<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.services.bizflow.model.BizFlowServiceProperties" %>
<%@ page import="com.bizflow.io.services.bizflow.util.BizFlowServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.message.util.I18NMessageUtil" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.StringWriter" %>
<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%
    String language = AcmManager.getInstance().getUserLanguage(request);
    String bizflowServiceName = BizFlowServiceConfigUtil.getProperty(BizFlowServiceProperties.ServiceName, "bizflow");

    int statusCode = 500;
    try {
        statusCode = pageContext.getErrorData().getStatusCode();
    } catch (Exception e) {
        statusCode = 500;
    }
%>
<style type="text/css">
    .maindiv a {
        color: #ffffff;
    }

    .maindiv .msgTitle {
        color: #ffffff;
        font-size: 30px;
        margin-bottom: 20px;
    }

    .maindiv .msgDetail {
        color: #9ef16b;
        font-size: 16px;
    }

    .maindiv .stacktrace {
        width: 100%;
        height: 100%;
        display: none;
        background-color: #dc3545;
        padding: 2px;
    }

    .maindiv .stacktrace textarea {
        width: 100%;
        height: 100%;
        background-color: #E8E8E8;
    }
</style>
<html>
<head>
    <style>
        .___error_wrap {
            padding: 10rem;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="___error_wrap">
    <div><img src="<%=request.getContextPath()%>/includes/images/error_text.png"></div>
    <br><br>
    <div class="text">
        <span style="color: red">
            <%if ((statusCode >= 401 && statusCode <= 404) || statusCode == 500) {%>
    <div class="msgTitle">
        <%=I18NMessageUtil.getMessage(bizflowServiceName, language, "exception.HTTP_" + statusCode + "_TITLE")%>
    </div>
    <div class="msgDetail">
        <%if(statusCode == 500) {%>
            <%=ExceptionUtil.getOriginalThrowable(exception).getMessage()%>
        <%}%>
        <%=I18NMessageUtil.getMessage(bizflowServiceName, language, "exception.HTTP_" + statusCode + "_TEXT")%>
        <%if (statusCode == 404) {%>
            <br>
            <%=pageContext.getErrorData().getRequestURI()%>
        <%} else if(statusCode == 500) {%>
            </span>
        <br><br>An unexpected error has occurred. If it still repeats, please contact your administrator.
    </div>
    <a href="#" onclick="document.getElementById('stacktrace').style.display = document.getElementById('stacktrace').style.display === 'block' ? '':'block'">Show details</a>
    <div class="maindiv">
        <div id="stacktrace" class="stacktrace">
	<textarea readonly>
	<%
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        out.print(stringWriter);
        printWriter.close();
        stringWriter.close();
    %>
	</textarea>
        </div>
        <%}%>
    </div>
    <%} else {%>
    <div class="msgTitle">
        ERROR CODE: <%=statusCode%>
    </div>
    <%}%>
</div>
</body>
</html>
