function initMap() {
    // Create a map object and specify the DOM element for display
    var map = new google.maps.Map(document.getElementById('map'), {
        center: geolocation$param$.mapLatLng,
        scrollwheel: geolocation$param$.mapScrollWheel,
        zoom: geolocation$param$.mapZoom || 13
    });

    // Create a marker and set its position.
    var marker = new google.maps.Marker({
        map: map,
        position: geolocation$param$.mapLatLng,
        title: geolocation$param$.mapTitle || 'My Location'
    });
}