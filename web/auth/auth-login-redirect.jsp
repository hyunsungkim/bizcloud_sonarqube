<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%
    try {
        if (!AcmManager.getInstance().isValidRequest(request)) {
            String contextPath = request.getContextPath();
            StringBuilder builder = new StringBuilder(request.getRequestURI()).append("?").append(request.getQueryString());
%>
<html>
<head>
    <script src="<%=contextPath%>/includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <script src="<%=contextPath%>/includes/node_modules/angular/angular.min.js"></script>
    <script>
        mars$require$.script("<%=contextPath%>/includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("<%=contextPath%>/includes/node_modules-ext/mars/mars.js");
    </script>
    <script>
        function _onload() {
            var url = "<%=builder.toString().replaceAll("\"", "%22")%>";
            document.getElementById("HOME").value = mars$cipher$.encipher(url);
            var form = document.forms[0];
            form.submit();
        }
    </script>
</head>
<body onload="_onload()">
<form method="post" action="<%=contextPath%>/auth/index.jsp">
    <input type="hidden" name="HOME" id="HOME">
</form>
</body>
</html>
<%
        return;
    }
} catch (Exception e) {
%>
<html>
<body>
<%=ExceptionUtil.getOriginalExceptionString(e)%>
</body>
</html>
<%
        return;

    }
%>