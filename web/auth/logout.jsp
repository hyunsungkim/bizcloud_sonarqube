<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<!DOCTYPE html>
<html ng-app="loginApp">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Welcome</title>
    <script src="../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../includes/node_modules/bootstrap/dist/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>

    <script src="../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../includes/node_modules/angular/angular.min.js"></script>
    <script src="../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../includes/node_modules/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>

    <script>
        mars$require$.link("../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../includes/node_modules-ext/angular/angular-ext-service.js");
        mars$require$.script("../includes/node_modules-ext/mars/mars.js");

        mars$require$.script("auth.js");
    </script>
</head>

<body ng-controller="logoutCtrl" class="background-transparent">
</body>
</html>
