var app = angular.module('loginApp', [
    'blockUI'
    , 'mars.angular.context'
    , 'mars.angular.ajax.service'
]).controller('loginAppController', ['$scope', '$window', '$timeout', 'blockUI', 'marsContext', 'marsService', function ($scope, $window, $timeout, blockUI, marsContext, marsService) {
    $scope.user = {};
    $scope.loginIdReady = false;
    $scope.blockUIMessage = "Sending...";

    function getCurrentUrl() {
        var href = "" + $window.top.location.href;
        var idx = href.lastIndexOf("#");
        if (-1 != idx) {
            href = href.substring(0, idx);
        }
        return href;
    }

    function getRedirectUrl(data) {
        var url;
        if (typeof (auth$option$) != "undefined" && auth$option$.home !== "") {
            url = auth$option$.home;
        } else if (data && data.moonUri) {
            url = marsContext.contextPath + "/" + data.moonUri;
        } else {
            url = getCurrentUrl();
        }

        return url;
    }

    function redirect(data) {
        $window.top.location = getRedirectUrl(data);
    }

    var queryParam = null;

    function focus() {
        $timeout(function () {
            try {
                var o = document.getElementById("pword");
                if (o) {
                    o.focus();
                }
            } catch (e) {
            }
        }, 200);
    }

    function login() {
        if (angular.isDefined($scope.user.i) && $scope.user.i != "" && angular.isDefined($scope.user.p) && $scope.user.p != "") {
            var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
            var config = {
                headers: {
                    "X-Client-TimeZone": timeZone
                }
            };
            marsService.login({
                success: function (o) {
                    if (angular.isDefined(o.data.credentialKey)) {
                        redirect(o.data)
                    } else {
                        angularExt.getBootboxObject().alert(invalidUserNameOrPassword, function () {
                            window.location.reload();
                        });
                    }
                },
                error: function (o) {
                    var faultCode = o.error.faultCode ? parseInt(o.error.faultCode) : 0;
                    if (faultCode == mars$faultcode$.AccountAlreadyUsed) {
                        angularExt.getBootboxObject().confirm({
                            message: o.error.data,
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-warning'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-default'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    queryParam = "force=true";
                                    $timeout(function () {
                                        $scope.login();
                                    }, 100);
                                }
                            }
                        });
                    } else if (faultCode == mars$faultcode$.InvalidCipherMessage) {
                        o.alert(cipherKeyReissuedMsg, function () {
                            window.location.reload();
                        });
                    } else {
                        o.alertExceptionOnly(function () {
                            window.location.reload();
                        });
                    }
                }
            }, $scope.user, queryParam, config);
        }
    }

    $scope.login = function () {
        var user = $scope.user;
        if (user.i && user.p) {
            login();
        } else if (user.i) {
            $scope.loginIdReady = true;
            $timeout(function () {
                document.getElementById("pword").focus();
            }, 100)
        }
    };

    $scope.enterUsername = function () {
        $scope.loginIdReady = false;
        $scope.user.p = undefined;
    };

    $scope.resetPassword = function () {
        $scope.user.redirect = getRedirectUrl();

        blockUI.start($scope.blockUIMessage);
        marsService.resetPasswordMail({
            success: function (o) {
                blockUI.stop();
                bootbox.alert("Reset password mail has been sent. Please check your email.");
            },
            error: function (o) {
                blockUI.stop();
                o.alertExceptionOnly(focus);
            }
        }, $scope.user);

    };
}]).controller('logoutCtrl', ['$scope', '$window', '$timeout', 'blockUI', 'marsContext', 'AjaxService', function ($scope, $window, $timeout, blockUI, marsContext, AjaxService) {
    var logoutApi = new AjaxService(marsContext.contextPath + '/services/acm/logout.json');

    function redirect() {
        var query = "";
        var idx = location.href.indexOf("?");
        if (-1 != idx) {
            query = location.href.substring(idx);
        }
        location.href = "redirect.jsp" + query;
    }

    logoutApi.get({
        success: function (o) {
            redirect();
        },
        error: function (o) {
            redirect();
        }
    });
}]);

function onLoadPage() {
    try {
        window.sessionStorage.clear();
        setTimeout(function () {
            var o = document.getElementById("loginId");
            if (o) {
                o.focus();
            }
        }, 100);
    } catch (e) {
    }
}
