<%@ page import="com.bizflow.io.core.lang.util.StringGetter" %>
<%@ page import="com.bizflow.io.core.net.util.RequestIdentifierKeeper" %>
<%@ page import="com.bizflow.io.core.security.SecurityCipher" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.message.util.I18NMessageUtil" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.moon.Moon" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.tenant.TenantManager" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.UnsupportedEncodingException" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.bizflow.io.services.core.model.ServiceConstant" %>
<%!
    String __getParameter(String url, String name) throws UnsupportedEncodingException {
        String value = StringGetter.get(url, "&" + name + "=", "&", false);
        if (null == value) {
            value = StringGetter.get(url, "?" + name + "=", "&", false);
        }

        return null != value ? URLDecoder.decode(value, "UTF-8") : null;
    }
%>
<%
    String contextPath = request.getContextPath();
    RequestIdentifierKeeper.setRequestIdentifier(null);
    HttpSession newSession = AcmManager.getInstance().initializeRequestSession(request);
    TenantManager.getInstance().setUserTenantIfNotSet(null, request);

    String home = null;
    String cipher = request.getParameter("$");
    if (null != cipher) {
        home = SecurityCipher.getInstance().decipherBase64(cipher);
    } else {
        home = ServletUtil.getParameterValue(request, "HOME", null);
        if (null == home) {
            home = "";
            String referrer = request.getHeader("Referer");
            if (null != referrer) {
                if (referrer.endsWith("/") || referrer.endsWith("index.html") || referrer.endsWith("index.jsp")) {
                    home = referrer;
                }
            } else {
                Map<String, Moon> map = MoonManager.getInstance().getMoonMap();
                if (map.size() == 1) {  // if there is ony one application, use it as default application
                    Moon moon = map.get(map.keySet().iterator().next());
                    home = contextPath + "/" + moon.getMoonUri();
                }
            }
        } else {
            String url = __getParameter(home, "$T");
            if (null != url) {
%>
<html>
<head>
    <script>
        <%if(url.startsWith(contextPath + "/") || url.equals(contextPath)) {%>
        if (parent) {
            parent.top.location = "<%=Encode.forJavaScript(url)%>";
        } else {
            top.location = "<%=Encode.forJavaScript(url)%>";
        }
        <%} else {%>
        alert('<%=I18NMessageUtil.getMessage(CoreServiceConfigUtil.getServiceName(), "security.invalidRedirectUrl")%>');
        <%}%>
    </script>
</head>
</html>
<%
                return;
            } else {
                url = __getParameter(home, "$H");
                if (null != url) {
                    if(url.startsWith(contextPath + "/") || url.equals(contextPath)) response.sendRedirect(url);
                    else {
%>
<html>
<head>
    <script>
        alert('<%=I18NMessageUtil.getMessage(CoreServiceConfigUtil.getServiceName(), "security.invalidRedirectUrl")%>');
    </script>
</head>
</html>
<%
                    }
                    return;
                } else {
                    url = CoreServiceConfigUtil.getProperty(CoreServiceProperties.SystemRedirectUrl);
                    if (null != url && url.trim().length() > 0) {
                        if (url.startsWith("/")) response.sendRedirect(contextPath + url);
                        else response.sendRedirect(url);
                        return;
                    }
                }
            }
        }
    }

    String appName = null;
    if (null != home) {
        appName = StringGetter.get(home, "/apps/", "/", false, false);
    }

    AcmManager.getInstance().setUserAppName(newSession, appName);
    boolean useResetPassword = CoreServiceConfigUtil.getConfiguration().getPropertyAsBoolean(CoreServiceProperties.AcmPasswordResetUse, false);
    String acmType = CoreServiceConfigUtil.getConfiguration().getProperty(CoreServiceProperties.AcmType);

    if (ServiceConstant.AcmTypeBizFlowAuthSP.equals(acmType)) {
        home = "bizauth/index.jsp?HOME=" + home;
%>
<html>
<head>
    <script>
        location.href = "<%=Encode.forJavaScript(home)%>";
    </script>
</head>
</html>
<%
    }

%>
<!DOCTYPE html>
<html ng-app="loginApp">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Welcome</title>
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <script src="../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>

    <script src="../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../includes/node_modules/angular/angular.min.js"></script>
    <script src="../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>

    <script>
        mars$require$.link("../includes/node_modules-ext/bootstrap4/css//bootstrap-ext.css");
        mars$require$.link("../includes/node_modules-ext/bizflow/css/theme/dark/bootstrap/bootstrap.css");
        mars$require$.link("../includes/node_modules-ext/bizflow/css/login.css");
        mars$require$.link("../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../includes/node_modules-ext/angular/angular-ext-service.js");
        mars$require$.script("../includes/node_modules-ext/mars/mars.js");
        mars$require$.script("../includes/node_modules-ext/mars/mars-util.js");

        mars$require$.script("auth.js");
    </script>

    <script language="javascript">
        var cipherKeyReissuedMsg = "<%=Encode.forJavaScriptBlock(I18NMessageUtil.getMessage(CoreServiceConfigUtil.getServiceName(), "security.cipherKeyReissuedOnLogin"))%>";
        var invalidUserNameOrPassword = "<%=Encode.forJavaScriptBlock(I18NMessageUtil.getMessage(CoreServiceConfigUtil.getServiceName(), "security.invalidUserNameOrPassword"))%>";
        (function (window, document) {
            'use strict';

            function AuthOption() {
                this.home = "<%=Encode.forJavaScriptBlock(home)%>";
            }

            var auth$option$ = window.auth$option$ || (window.auth$option$ = new AuthOption());
        })(window, document);
    </script>

    <style>
        .bootbox .modal-content {
            background-color: #343a40;
            border-color: #6c757d !important;
            color: #f8f9fa;
        }
    </style>
</head>

<body ng-controller="loginAppController" class="background-transparent" onload="onLoadPage()" onresize="onLoadPage()">
<div class="bf-login_wrap" style="z-index: 100;">
    <div class="login-box">
        <div class="logo"><img src="../includes/node_modules-ext/bizflow/img/logo_white.png" width="217" height="37" alt="BizFlow"></div>
        <div class="welcome">Welcome</div>
        <form method="POST">
            <div ng-if="!loginIdReady">
                <input ng-model="user.i" id="loginId" tabindex="1" type="text" name="loginId" maxlength="100" placeholder='User Name' value="" autofocus>
            </div>
            <div ng-if="!loginIdReady">
                <button ng-click="login()">NEXT</button>
            </div>
            <div ng-if="loginIdReady">
                <input ng-model="user.p" id="pword" tabindex="2" type="password" name="pword" size="18" maxlength="48" placeholder='Enter password for {{user.i}}'>
            </div>
            <div ng-if="loginIdReady">
                <button type="submit" class="btn btn-primary btn-sm" ng-click="login()">LOG IN</button>
            </div>
            <div class="line"></div>
            <div ng-if="!loginIdReady" class="footer">
                Your password will be asked in the next step
                <div ng-include="'../copyright-text.html'"></div>
            </div>
            <% if (useResetPassword) {%>
            <div style="color: #901c02; font-style: italic;padding-bottom: 10px;">
                <span>You can ask to reset password in the next strep</span>
            </div>
            <%}%>
            <div ng-if="loginIdReady" class="footer">
                <span ng-click="enterUsername();">Click here to change user name <i>{{user.i}}</i></span>
                <div ng-include="'../copyright-text.html'"></div>
            </div>
            <% if (useResetPassword) {%>
            <div style="color: #901c02; text-decoration: underline;">
                <span ng-click="resetPassword();">Click here to reset your password</span>
            </div>
            <%}%>
        </form>
    </div>
</div>
</body>
</html>
