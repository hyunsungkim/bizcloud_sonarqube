<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.bizflow.io.core.security.SecurityCipher" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%
    try {
        String d = request.getParameter("$");
        if (null != d) {
            AcmManager.getInstance().setAuthenticationKey(session, new JSONObject(SecurityCipher.getInstance().decipherBase64(d)));
%>
<!DOCTYPE html>
<html ng-app="resetPasswordApp">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Welcome</title>
    <script src="../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>

    <script src="../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>

    <script>
        mars$require$.link("../../includes/node_modules-ext/mars/css/mars-shadow.css");
        mars$require$.link("../../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext-service.js");
        mars$require$.script("../../includes/node_modules-ext/mars/mars.js");
        mars$require$.script("../../includes/node_modules-ext/mars/mars-util.js");

        mars$require$.script("reset-password.js");
    </script>
</head>

<body ng-controller="resetPasswordAppController" class="background-transparent" onload="onLoadPage()" onresize="onLoadPage()">
<div class="container position-center text-center">
    <div class="card center shadow-box shadow-black-bottom" id="formBox">
        <div class="card-header bg-danger" style="text-align: center;padding-bottom: 0;">
            <h3 class="card-title">Reset Password</h3>
        </div>
        <div class="card-body text-white bg-dark">
            <form>
                <table width="100%">
                    <tr ng-if="!loginIdReady">
                        <td style="width: 380px;">
                            <div class="form-group max-width">
                                <input ng-model="user.i" id="loginId" tabindex="1" type="text" name="loginId" size="18" maxlength="100" placeholder='User Name' value="" autofocus style="width: 350px;">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-sm" ng-click="resetPassword()">NEXT</button>
                            </div>
                        </td>
                    </tr>
                    <tr ng-if="loginIdReady">
                        <td style="width: 380px;">
                            <div class="form-group max-width">
                                <input ng-model="user.p" id="password" tabindex="1" type="password" name="password" size="18" maxlength="48" placeholder='Password' style="width: 350px;">
                            </div>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr ng-if="loginIdReady">
                        <td style="width: 380px;">
                            <div class="form-group max-width">
                                <input ng-model="user.p2" id="password2" tabindex="2" type="password" name="password2" size="18" maxlength="48" placeholder='Confirm Password' style="width: 350px;">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-sm" ng-click="resetPassword()" ng-disabled="!user.p || user.p != user.p2 || !isValid()">Reset</button>
                            </div>
                        </td>
                    </tr>
                    <tr ng-if="!loginIdReady">
                        <td colspan="2">
                            <div style="color: #909090; font-style: italic;padding-bottom: 10px;">
                                <small>Your new password will be asked in the next step</small>
                            </div>
                        </td>
                    </tr>
                    <tr ng-if="loginIdReady">
                        <td colspan="2">
                            <div class="text-left">
                                <ul>
                                    <li>The password must contain at least 1 lowercase alphabetical character.</li>
                                    <li>The password must contain at least 1 uppercase alphabetical character.</li>
                                    <li>The password must contain at least 1 numeric character.</li>
                                    <li>The password must contain at least one special character (!@#$%^&*).</li>
                                    <li>The password must be eight characters or longer.</li>
                                </ul>
                            </div>
                            <div style="color: #909090; font-style: italic;">
                                <small ng-click="enterUsername();">Click here to enter user name again</small>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<%
    }
} catch (Exception e) {
%>
<!DOCTYPE html>
<html ng-app="resetPasswordApp">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Welcome</title>
    <script src="../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
</head>
<body>
<div class="container">
    <div style="height: 100px;">&nbsp;</div>
    <div class="alert alert-danger text-center" role="alert">Invalid Request</div>
</div>
</body>
</html>
<%
    }
%>