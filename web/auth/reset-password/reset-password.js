var app = angular.module('resetPasswordApp', [
    'blockUI'
    , 'mars.angular.context'
    , 'mars.angular.ajax.service'
]).controller('resetPasswordAppController', ['$scope', '$window', '$timeout', 'blockUI', 'marsContext', 'marsService', function ($scope, $window, $timeout, blockUI, marsContext, marsService) {
    $scope.user = {};
    $scope.loginIdReady = false;

    function redirect(data) {
        var url = null;
        if (data && data.redirect) {
            url = data.redirect;
        } else if (data && data.moonUri) {
            url = marsContext.contextPath + "/" + data.moonUri;
        } else {
            url = marsContext.contextPath;
        }

        $window.top.location = url;
    }

    var queryParam = null;

    function focus() {
        $timeout(function () {
            try {
                var o = document.getElementById("password");
                if (o) {
                    o.focus();
                }
            } catch (e) {
            }
        }, 200);
    }

    function resetPassword() {
        if (angular.isDefined($scope.user.i) && $scope.user.i != "" && angular.isDefined($scope.user.p) && $scope.user.p != "") {
            marsService.resetPassword({
                success: function (o) {
                    if (o.data.result) {
                        bootbox.alert("Password has been reset successfully.", function () {
                            redirect(o.data);
                        });
                    }
                },
                error: function (o) {
                    o.alertExceptionOnly(focus);
                }
            }, $scope.user, queryParam);
        }
    }

    $scope.isValid = function () {
        return mars$password$.isMeetPasswordRules($scope.user.p);
    };

    $scope.resetPassword = function () {
        var user = $scope.user;
        if (user.i && user.p) {
            if ($scope.isValid()) {
                resetPassword();
            } else {
                bootbox.alert("The password does not meet the rules of a password.", function () {
                    $timeout(function () {
                        document.getElementById("password").focus();
                    }, 100)
                });
            }
        } else if (user.i) {
            $scope.loginIdReady = true;
            $timeout(function () {
                document.getElementById("password").focus();
            }, 100)
        }
    };

    $scope.enterUsername = function () {
        $scope.loginIdReady = false;
        $scope.user.p = undefined;
    };
}]);

function onLoadPage() {
    try {
        var formBox = $("#formBox");
        var c = ($(document).width() - formBox.width()) / 2 - 15;
        formBox.css("position", "absolute");
        formBox.css("left", c + "px");
        if (!window.frameElement) {
            var h = 200;
            formBox.css("top", h + "px");
        }
        setTimeout(function () {
            var o = document.getElementById("loginId");
            if (o) {
                o.focus();
            }
        }, 100);
    } catch (e) {
    }
}
