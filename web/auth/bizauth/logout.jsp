<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%
    String referrer = null;

    if (null != request.getParameter("HOME")) {
        referrer = new String(Base64.decodeBase64(request.getParameter("HOME")));
    }

    if (null == referrer) {
        referrer = AcmManager.getInstance().getReferrer(session);
        if (null == referrer) {
            referrer = request.getContextPath();
        }
    }

    try {
        session.invalidate();
    } catch (IllegalStateException ignore) {
    }

    if (null != referrer) {
        response.sendRedirect(referrer);
    }
%>