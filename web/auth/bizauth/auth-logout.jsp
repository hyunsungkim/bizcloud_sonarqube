<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%
    boolean validRequest = false;
    try {
        AcmManager.getInstance().checkAuthCredentialKey(request);
        validRequest = true;
    } catch (Exception e) {
    }

    String home = request.getParameter("HOME");
    home = new String(Base64.decodeBase64(home));

    String logoutUrl = CoreServiceConfigUtil.getProperty(CoreServiceProperties.AcmBizFlowAuthLogout, null);
    if (validRequest == false) {
        logoutUrl = "logout.jsp?HOME=" + request.getParameter("HOME");
        response.sendRedirect(logoutUrl);
    } else {
        AcmManager.getInstance().setReferrer(request.getSession(), home);
%>
<html>
<head>
    <meta name="referrer" content="unsafe-url"/>
    <script>
        this.location = "<%=Encode.forJavaScript(logoutUrl)%>";
    </script>
</head>
</html>
<%
    }
%>
