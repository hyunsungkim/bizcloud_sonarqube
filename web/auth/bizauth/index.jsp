<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="com.bizflow.auth.spclient.SPClientHelper" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String loginUrl = CoreServiceConfigUtil.getProperty(CoreServiceProperties.AcmBizFlowAuthLogin, null);
    String randomKey = (String) session.getAttribute("randomKey");
    String home = request.getParameter("HOME");

    if(randomKey == null){
        randomKey = SPClientHelper.generateInstanceId();
    }
    AcmManager.getInstance().setReferrer(request.getSession(), home);
    session.setAttribute("randomKey", randomKey);
%>
<html>
<meta name="referrer" content="unsafe-url" />​
<head>
    <script>
        function page_onload() {
            document.frm.submit();
        }
    </script>
</head>
<body onload="page_onload();">
<form name="frm" id="frm" action="<%=loginUrl%>" method="POST">
    <input type="hidden" name="ivp" value="<%=randomKey%>"/>
</form>
</body>
</html>
