<%@ page import="java.util.Map" %>
<%@ page import="com.bizflow.auth.spclient.SPClientHelper.*" %>
<%@ page import="com.bizflow.io.services.core.acm.UserSession" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.core.model.*" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.core.json.JSONArray" %>
<%@ page import="com.bizflow.io.services.core.acm.CredentialKeyType" %>
<%@ page import="static com.bizflow.io.services.core.bf.util.BizFlowUtil.getUserOrganization" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.net.URL" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    AcmManager acmManager = AcmManager.getInstance();
    String referrer = acmManager.getReferrer(request.getSession());
    if (null == referrer) {
        referrer = request.getContextPath();
    }

    String server = CoreServiceConfigUtil.getProperty(CoreServiceProperties.AcmBizFlowAuthServer, null);
    String secretKey = CoreServiceConfigUtil.getProperty(CoreServiceProperties.AcmBizFlowAuthSecretKey, null);
    String randomKey = (String) session.getAttribute("randomKey");
    String encAuthToken = request.getParameter("auth-token");

    TokenAssistant assistant = null;
    try {
        assistant = new TokenAssistantBuilder(randomKey, secretKey, encAuthToken)
                .setSPModuleBaseUrl(new URL(CoreServiceConfigUtil.getProperty(CoreServiceProperties.AcmBizFlowAuthBaseUrl, null)))
                .build();
    } catch (Exception e) {
%>
<html>
<head>
    <script>
        this.location = "index.jsp?HOME=<%=referrer%>";
    </script>
</head>
</html>
<%
        return;
    }

    Date now = new Date();
    Member member = new Member();
    member.setId((String) assistant.getUserInfo().get("id"));
    member.setLoginId((String) assistant.getUserInfo().get("loginid"));
    member.setRemoteAddress(ServletUtil.getClientIPAddress(request));
    member.setLoginDate(now);
    member.setLastAccessDate(now);
    member.setAuthenticationServer(server);
    member.setProperties(assistant.getUserInfo());
    member.setTimeZone(ServletUtil.getClientTimeZone(request));
    member.setLanguage(ServletUtil.getRequesterLanguage(request, ServletUtil.DefaultLanguage));
    member.setAuthCredentialKey(encAuthToken);
    member.setAuthCredentialRandomKey(randomKey);
    member.setAuthCredentialKeyType(CredentialKeyType.BizFlowAuthSP);

    // set BizFlow Credential Key
    if (assistant.getBpmSessionInfo() != null && assistant.getBpmSessionInfo().get("sessionInfoXML") != null) {
        member.setCredentialKeyType(CredentialKeyType.BizFlow);
        member.setCredentialKey((String) assistant.getBpmSessionInfo().get("sessionInfoXML"));
    }

    // Set License
    Map<String, License> licenseMap = new HashMap();

    for (Map<String, Object> license : assistant.getUserAuthList()) {
        String licenseId = (String) license.get("usergroupid");
        licenseMap.put(licenseId, new License(licenseId, CredentialKeyType.BizFlow));
    }

    Licenses licenses = new Licenses(licenseMap);
    acmManager.setUserBizFlowLicense(session, licenses);

    // Set Department
    if (assistant.getUserInfo().get("deptid") != null) {
        member.setDepartment(new Department((String) assistant.getUserInfo().get("deptid"), (String) assistant.getUserInfo().get("deptname"), getUserOrganization(), true));
    }

    // Set UserGroup
    JSONArray userGroups = new JSONArray();
    if (null != assistant.getUserGroupList()) {
        userGroups.putAll(assistant.getUserGroupList());
    }

    acmManager.setAuthCredentialKey(session, member);
    UserSession userSession = acmManager.setUserSessionMember(session, member);
    userSession.setGroups(new Groups(userGroups));
    userSession.setInitialized(true);
%>
<html>
<head>
    <script>
        this.location = "<%=referrer%>";
    </script>
</head>
</html>
