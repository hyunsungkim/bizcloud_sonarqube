<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%
    try {
        if (!AcmManager.getInstance().isValidRequest(request)) {
            out.print("<html><head><script>this.location=\"");
            out.print(request.getContextPath());
            out.print("/access-denied.jsp\";</script></head><body></body></html>");
            return;
        } else {
            boolean appDevMode = (CoreServiceConfigUtil.isExistFile("/apps/fbs/app"));
            if (appDevMode) {
                appDevMode = !CoreServiceProperties.SystemModeBIO.equalsIgnoreCase(CoreServiceConfigUtil.getProperty(CoreServiceProperties.SystemMode));
            }

            if (appDevMode) {
                String path = request.getServletPath();
                if (path.startsWith("/tools/api/")
                        || path.startsWith("/admin/tools/msg/")
                ) {
                    out.print("<html><head><script>this.location=\"");
                    out.print(request.getContextPath());
                    out.print("/access-denied.jsp\";</script></head><body></body></html>");
                    return;
                }
            }
        }
    } catch (Exception e) {
%>
<html>
<body>
<%=ExceptionUtil.getOriginalExceptionString(e)%>
</body>

</html>
<%
        return;

    }
%>