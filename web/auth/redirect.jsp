<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.moon.Moon" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%
    String referrer = null;
    String moonName = request.getParameter("m");
    if (null != moonName) {
        Moon moon = MoonManager.getInstance().get(moonName);
        if (null != moon) {
            referrer = request.getContextPath() + "/" + moon.getMoonUri();
        }
    }
    if (null == referrer) {
        referrer = AcmManager.getInstance().getReferrer(session);
        if (null == referrer) {
            referrer = request.getContextPath();
        }
    }

    try {
        session.invalidate();
    } catch (IllegalStateException ignore) {
    }

    if (null != referrer) {
        response.sendRedirect(referrer);
    }
%>