var app = angular.module('angularApp', [
    'blockUI'
    , 'jsonFormatter'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
]).filter('trustAsHtml', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]).controller('angularAppCtrl', ['$scope', 'AjaxService', 'marsContext', function ($scope, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);

    $scope.method = "POST";
    $scope.htmlStyle = parameter.getParameterValue("st", "flatly");

    $scope.format = "json";
    $scope.param = {
        id: parameter.getParameterValue("projectId", 1)
    };

    $scope.getTypeString = function (type) {
        if (type == 'D') return "Date";
        else if (type == 'N') return "Number";

        return "String";
    };

    var GetProjectContext = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.project-GetProjectContext@.json');
    GetProjectContext.post({
        success: function (o) {
            $scope.projectContext = o.data[0];
        },
        error: function (o) {
            o.alertException();
        }
    }, $scope.param);

}]);



