<%@include file="../../auth/auth.jsp" %>

<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%
    int processId = ServletUtil.getParameterIntValue(request, "processId", -1);
    String htmlStyle = ServletUtil.getParameterValue(request, "st", "flatly");
%>
<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project Information</title>
    <script src="../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <%if ("bootstrap".equals(htmlStyle)) {%>
    <link rel="stylesheet" href="../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <%} else {%>
    <link rel="stylesheet" href="../../includes/node_modules-ext/bootstrap/css/bootstrap.<%=Encode.forUriComponent(htmlStyle)%>.min.css" type="text/css"/>
    <%}%>
    <link rel="stylesheet" href="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../includes/node_modules/jsonformatter/dist/json-formatter.min.css"/>
    <link rel="stylesheet" href="../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../includes/node_modules/quill/dist/quill.core.css"/>

    <script src="../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../includes/node_modules/jsonformatter/dist/json-formatter.js"></script>
    <script src="../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>

    <script src="../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("project-info.js");
    </script>

    <style>
        .table {
            margin-bottom: 0;
        }
    </style>

    <script>
        function changeStyle() {
            var style = document.getElementById("htmlStyle");
            var styleName = style.options[style.selectedIndex].value;
            this.location.href = "project-info.jsp?processId=<%=processId%>&st=" + styleName;
        }
    </script>

</head>
<body ng-controller="angularAppCtrl" ng-cloak="true">
<div>
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools">
            <table class="max-width" style="font-size: medium;">
                <tr>
                    <td nowrap>
                        <h4>Project Information</h4>
                    </td>
                    <td width="max-width">
                    </td>
                    <td style="text-align: right;padding-right: 10px;">
                        <select id="htmlStyle" name="htmlStyle" ng-model="htmlStyle" style="color:black;" onchange="changeStyle()">
                            <option value="admin">admin</option>
                            <option value="bootstrap">bootstrap</option>
                            <option value="cerulean">cerulean</option>
                            <option value="cosmo">cosmo</option>
                            <option value="cyborg">cyborg</option>
                            <option value="darkly">darkly</option>
                            <option value="flatly">flatly</option>
                            <option value="journal">journal</option>
                            <option value="lumen">lumen</option>
                            <option value="readable">readable</option>
                            <option value="sandstone">sandstone</option>
                            <option value="simplex">simplex</option>
                            <option value="slate">slate</option>
                            <option value="spacelab">spacelab</option>
                            <option value="superhero">superhero</option>
                            <option value="united">united</option>
                            <option value="yeti">yeti</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="panel-body" ng-show="projectContext">
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading" data-toggle="collapse" data-target="#project">
                        <table style="width:100%;">
                            <tr>
                                <td nowrap>
                                    <h3 class="panel-title">Project</h3>
                                </td>
                                <td width="95%" style="text-align: right;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="project" class="collapse in">
                        <ul>
                            <li>Name: {{projectContext.name}}</li>
                            <li>Description: {{projectContext.description}}</li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading" data-toggle="collapse" data-target="#dataSchema">
                        <table style="width:100%;">
                            <tr>
                                <td nowrap>
                                    <h3 class="panel-title">Data</h3>
                                </td>
                                <td width="95%" style="text-align: right;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="dataSchema" class="collapse in">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Name</th>
                                <th>Variable</th>
                                <th>Type</th>
                            </tr>
                            <tr ng-repeat="(fieldName, field) in projectContext.dataSchema[0].content.field">
                                <td nowrap>{{field.name}}</td>
                                <td nowrap>{{fieldName}}</td>
                                <td nowrap>{{field.type}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading" data-toggle="collapse" data-target="#dataSchemaGroup">
                        <table style="width:100%;">
                            <tr>
                                <td nowrap>
                                    <h3 class="panel-title">Data Group</h3>
                                </td>
                                <td width="95%" style="text-align: right;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="dataSchemaGroup" class="collapse in">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Group Name</th>
                                <th>Variable</th>
                            </tr>
                            <tr ng-repeat="group in projectContext.dataSchema[0].content.group">
                                <td nowrap>
                                    {{group.name}}
                                </td>
                                <td>
                                    <ol>
                                        <li ng-repeat="item in group.item">
                                            {{item}}
                                        </li>
                                    </ol>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading" data-toggle="collapse" data-target="#forms">
                        <table style="width:100%;">
                            <tr>
                                <td nowrap>
                                    <h3 class="panel-title">Form</h3>
                                </td>
                                <td width="95%" style="text-align: right;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="forms" class="collapse in">
                        <div class="panel panel-default" ng-repeat="form in projectContext.form">
                            <div class="panel-heading" data-toggle="collapse" data-target="#form{{form.id}}">
                                <table style="width:100%;">
                                    <tr>
                                        <td nowrap>
                                            <h3 class="panel-title">{{form.name}}</h3>
                                        </td>
                                        <td class="max-width">
                                            &nbsp;&nbsp;({{form.description}})
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="form{{form.id}}" class="collapse in">
                                <div style="padding: 20px;">
                                    <div>
                                        <label>Process Variable Mapping:</label>
                                        <span ng-if="!form.pvContent || !form.pvContent.content"> None</span>
                                    </div>
                                    <div>
                                        <table class="table table-striped table-bordered" ng-if="form.pvContent.content">
                                            <tr>
                                                <th>Process Variable Name</th>
                                                <th>Form Variable</th>
                                                <th>Type</th>
                                            </tr>
                                            <tr ng-repeat="item in form.pvContent.content">
                                                <td nowrap>{{item.pv}}</td>
                                                <td nowrap>{{item.form}}</td>
                                                <td nowrap>{{getTypeString(item.type)}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
</body>
</html>