<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>

<%@include file="../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>
<%
    boolean editable = ServletUtil.getParameterBooleanValue(request, "e", false);

    String docBgColor = "light";
    String docTextColor = "dark";

%>
<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Document</title>
    <script src="../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../includes/node_modules/summernote/dist/summernote.css">
    <link rel="stylesheet" href="../../includes/node_modules/ui-select/dist/select.min.css"/>
    <link rel="stylesheet" href="../../includes/node_modules/ui-select/dist/select2.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../includes/node_modules/ui-select/dist/select.min.js"></script>

    <script src="../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("api-doc-viewer.js");
    </script>

    <style>
        #sessionName .select2-drop-active {
            width: 500px;
        }

        .select2-results {
            max-height: 600px !important;
        }

        .table {
            margin-bottom: 0;
        }

        .card-api {
            margin: 0 0 0 0;
            border-radius: 0;
        }

        .card-api .card-heading {
            padding: 2px 0 2px 0;
            border-radius: 0;
        }
    </style>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div>
    <div class="card text-<%=textColor%> bg-<%=bgColor%>">
        <div class="card-header" data-toggle="collapse" data-target="#GenTools">
            <table class="max-width" style="font-size: medium;">
                <tr>
                    <td nowrap>
                        <h4>API Documents</h4>
                    </td>
                    <td class="text-center" style="padding-top: 10px;">
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="sessionName">Session</label>&nbsp;&nbsp;
                                <div class="form-control form-control-xs text-left no-padding no-margin" style="color: black; width:250px;">
                                    <ui-select id="sessionName" ng-model="model.session" title="Choose a session" theme="select2" class="max-width" on-select="changeSession($item)" ng-click="fixChromeScroll();">
                                        <ui-select-match placeholder="Select a session in the list or search name...">{{$select.selected}}</ui-select-match>
                                        <ui-select-choices repeat="item in sessions | filter: $select.search">
                                            <div ng-bind-html="item | highlight: $select.search"></div>
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
                            </div>
                            <div class="form-group" style="padding-left: 10px;">
                                <label for="category">Category</label>&nbsp;&nbsp;
                                <div class="form-control form-control-xs text-left no-padding no-margin" style="color: black; width:250px;">
                                    <ui-select id="category" ng-model="model.category" title="Choose a category" theme="select2" class="max-width" ng-click="fixChromeScroll();">
                                        <ui-select-match placeholder="Select a category in the list or search name...">{{$select.selected}}</ui-select-match>
                                        <ui-select-choices repeat="item in categories | filter: $select.search">
                                            <div ng-bind-html="item | highlight: $select.search"></div>
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
                            </div>
                            <div ng-click="changeSession();">
                                &nbsp;<i class="fa fa-refresh fa-fw" aria-hidden="true"></i>
                            </div>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-body">
            <div class="card" ng-repeat="(category, apis) in apiDocs" ng-show="'ALL' === model.category || category === model.category">
                <div class="card-header bg-info" data-toggle="collapse" data-target="#card{{category | makeSafeId}}" style="padding-bottom: 0;">
                    <h5>{{category}}</h5>
                </div>
                <div id="card{{category | makeSafeId}}" class="collapse show">
                    <div class="card text-<%=textColor%> bg-<%=bgColor%>" ng-repeat="api in apis">
                        <div class="card-header no-padding no-margin" data-toggle="collapse" data-target="#card{{getApiIndexId(category,$index)}}" ng-click="clickPanel(category, $index);$event.stopPropagation()">
                            <div class="row">
                                <div class="col">
                                    <h6><i class="fa fa-caret-right fa-fw" aria-hidden="true"></i> {{api.name}}</h6>
                                </div>
                                <div class="col text-right">
                                    <%
                                        if (AcmManager.getInstance().isAdminSession(session)) {
                                    %>
                                    <button class="btn btn-danger btn-xs" ng-click="askDeleteApiDoc(api);$event.stopPropagation();">Delete</button>
                                    <%if (editable) {%>
                                    &nbsp;&nbsp;<button class="btn btn-info btn-xs" style="width: 80px;" ng-click="editApi(api);$event.stopPropagation()">Edit</button>&nbsp;&nbsp;
                                    <%
                                            }
                                        }
                                    %>
                                    <button class="btn btn-success btn-xs" style="width: 80px;" ng-click="testApi(api);$event.stopPropagation()">Test</button>
                                </div>
                            </div>
                        </div>
                        <div id="card{{getApiIndexId(category, $index)}}" class="{{getPanelState(category, $index) ? 'collapse show':'collapse'}} text-<%=docTextColor%> bg-<%=docBgColor%>" style="padding: 10px;">
                            <div style="padding-top: 10px; padding-bottom: 10px;">
                                <p class="lead">
                                    {{api.description}}
                                </p>
                            </div>
                            <div>
                                <div>
                                    <label>URL(s):</label>
                                    <ol ng-if="api.paginationUrl">
                                        <li>None Pagination: <i>{{api.url}}</i></li>
                                        <li ng-if="api.paginationUrl">Pagination: <i>{{api.paginationUrl}}</i></li>
                                        <div>{{api.urlDesc}}</div>
                                    </ol>
                                    <ul ng-if="!api.paginationUrl">
                                        <li><i>{{api.url}}</i></li>
                                        <div>{{api.urlDesc}}</div>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <label>Parameters:</label>
                                    <span ng-if="'undefined' != typeof(api.parameterList) || api.parameterList.length == 0"> {{api.parameterDesc || 'None'}}</span>
                                </div>
                                <div>
                                    <table class="table table-striped table-bordered" ng-if="api.parameterList.length > 0">
                                        <tr>
                                            <th>Name</th>
                                            <th>Mandatory</th>
                                            <th>Type</th>
                                            <th class="max-width">Description</th>
                                        </tr>
                                        <tr ng-repeat="param in api.parameterList">
                                            <td nowrap ng-if="isPublishedParameter(param)">{{param.name}}</td>
                                            <td nowrap ng-if="isPublishedParameter(param)">{{param.mandatory ? 'Yes' : 'No'}}</td>
                                            <td nowrap ng-if="isPublishedParameter(param)">{{param.type | toUpperCase}}<span ng-show="param.type && (param.type.indexOf('char') != -1 || param.type ==='number')">({{param.maxLength}})</span></td>
                                            <td ng-if="isPublishedParameter(param)">
                                                <p ng-bind-html="param.description"></p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div ng-if="api.notes">
                                <div class="ql-editor no-padding no-margin">
                                    <div ng-bind-html="api.notes | trustAsHtml" style="padding-top: 10px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<form id="testForm" action="../../admin/tools/api/api-test.jsp" target="_blank">
    <input type="hidden" name="a">
    <input type="hidden" name="s">
    <input type="hidden" name="n">
</form>
<form id="editForm" action="../../admin/tools/api/api-documentation.jsp" target="_blank">
    <input type="hidden" name="a">
    <input type="hidden" name="s">
    <input type="hidden" name="n">
</form>

<%@include file="../../copyright.html" %>

</body>
</html>