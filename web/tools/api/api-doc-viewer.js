var app = angular.module('angularApp', [
    'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'ui.select'
]).filter('trustAsHtml', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]).controller('angularAppCtrl', ['$scope', 'AjaxService', 'marsContext', function ($scope, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);

    $scope.model = {
        method: "POST",
        session: parameter.getParameterValue("s", ""),
        category: 'ALL'
    };

    if ('undefined' != typeof api) {
        $scope.apiUrl = api;
    }
    $scope.format = "json";
    $scope.result = {};
    $scope.resultString = "";
    $scope.categories = [];
    var preSession = null;

    $scope.changeSession = function () {
        if ($scope.model.session !== '') {
            if (preSession !== $scope.model.session) {
                $scope.model.category = 'ALL';
            }
            var api = new AjaxService(marsContext.contextPath + '/services/data/context/' + $scope.model.session + '/api-docs.json');
            api.get({
                success: function (o) {
                    $scope.apiDocs = o.data;

                    $scope.categories = ['ALL'];
                    for (var name in $scope.apiDocs) {
                        $scope.categories.push(name);
                    }
                },
                error: function (o) {
                    o.alertException();
                }
            });

            preSession = $scope.model.session;
        }
    };

    var sessionsApi = new AjaxService(marsContext.contextPath + '/services/data/context/sessions.json');
    sessionsApi.get({
        success: function (o) {
            $scope.sessions = o.data;
            $scope.changeSession();
        },
        error: function (o) {
            o.alertException();
        }
    });

    $scope.deleteApiDoc = function (api) {
        var api = new AjaxService(marsContext.contextPath + '/services/data/context/' + api.sessionName + '/api/' + api.name.replace(".", "-") + '.json');
        api.delete({
            success: function (o) {
                $scope.changeSession();
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.askDeleteApiDoc = function (api) {
        bootbox.confirm({
            message: "Are you sure to delete the API document for " + api.name + "?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    $scope.deleteApiDoc(api);
                }
            }
        });
    };

    $scope.testApi = function (api) {
        var testForm = document.getElementById("testForm");
        testForm.elements["a"].value = marsContext.contextPath + api.url;
        testForm.elements["s"].value = api.sessionName || '';
        testForm.elements["n"].value = api.name || '';
        testForm.submit();
    };

    $scope.editApi = function (api) {
        var editForm = document.getElementById("editForm");
        editForm.elements["a"].value = marsContext.contextPath + api.url;
        editForm.elements["s"].value = api.sessionName;
        editForm.elements["n"].value = api.name;
        editForm.submit();
    };

    $scope.isPublishedParameter = function (param) {
        return ("undefined" == typeof param.publication || param.publication) && "N/A" !== param.description;
    };

    $scope.panels = [];

    $scope.getApiIndexId = function (category, index) {
        return angularExt.getSafeId(category) + index;
    };

    $scope.clickPanel = function (category, idx) {
        var id = "card" + $scope.getApiIndexId(category, idx);
        $scope.panels[id] = !$scope.panels[id];
    };
    $scope.getPanelState = function (category, idx) {
        var id = "card" + $scope.getApiIndexId(category, idx);
        return $scope.panels[id];
    };
    $scope.fixChromeScroll = function () {
        setTimeout(function () {
            var div = document.createElement("div");
            div.innerHTML = "";
            document.body.appendChild(div);
            setTimeout(function () {
                document.body.removeChild(div);
            }, 100);
        }, 100);
    };
}]);



