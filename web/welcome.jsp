<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@include file="auth/auth.jsp" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="includes/node_modules-ext/bootstrap/dist/css/bootstrap.admin.min.css"/>
    <script src="includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="height: 100%;">
    <h1>Welcome: <%=ServletUtil.getClientIPAddress(request)%></h1>
    <p></p>
    <a href="admin">
        <%@include file="copyright.html" %>
    </a>
</div>
</body>
</html>
