<%@ page import="com.bizflow.io.core.util.UUIDGenerator" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.acm.util.AnonymousUtil" %>
<%@ page import="com.bizflow.io.services.core.model.Member" %>
<%@ page import="com.bizflow.io.services.custom.util.CustomServiceConfigUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.TimeZone" %>
<%
    String isfi = isfi1;

    Member member = null;
    String queryString = request.getQueryString();
    queryString = null == queryString ? "" : "&" + queryString;

    boolean iframeMode = ServletUtil.getParameterBooleanValue(request, "i", true);
    boolean validRequest = AcmManager.getInstance().isValidRequest(request) && null != AcmManager.getInstance().getUserSessionMember(session);
	/*
    boolean accessibility = null == request.getParameter("accessibility") && "accessibility".equalsIgnoreCase(CustomServiceConfigUtil.getProperty("VeteranPortal.DefaultMode", "accessibility"));

    if (accessibility) {
        queryString += "&accessibility=y";
    }
	*/

    if (!validRequest) {
        TimeZone timeZone = ServletUtil.getClientTimeZone(request);
        if (null != timeZone) {
            member = AnonymousUtil.createAnonymousCredential("BizCloud Training Portal Anonymous", "BizCloud Training Portal", timeZone, request);
            validRequest = null != member;
        } else {
%>
<html>
<head>
	<script>
			try {
				if(/Trident\/|MSIE/.test(window.navigator.userAgent) && window.document.documentMode) {
					top.opener = null;
				}
			}catch(e){}
	</script>
    <script>	
        var iframeMode = <%=iframeMode ? "true" : "false"%>;
        var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
        var url = '' + this.location.href;

        function _onload() {
            if (iframeMode) {
                var form = document.forms[0];
                form.tz.value = timeZone;
                form.submit();
            } else {
                url += (-1 == url.indexOf('?') ? '?' : '&') + 'tz=' + encodeURIComponent(timeZone);
                this.location.href = url;
            }
        }
    </script>
</head>
<body onload="_onload();">
<form method="GET">
    <input type="hidden" name="tz" value="">
    <%
        Enumeration<String> en = request.getParameterNames();
        while (en.hasMoreElements()) {
            String name = en.nextElement();
            String value = request.getParameter(name);
            if (null != value) {
    %>
    <input type="hidden" name="<%=Encode.forHtmlAttribute(name)%>" value="<%=Encode.forHtmlAttribute(value)%>">
    <%
            }
        }
    %>
</form>
</body>
</html>
<%
        }
    }

    if (validRequest) {
        member = null == member ? AcmManager.getInstance().getUserSessionMember(session) : member;
        if (!UUIDGenerator.ZeroId.equals(member.getId())) {
            isfi = isfi2;
        }

        String home = request.getRequestURI();
        if (iframeMode) {
%>
<html>
<head>
	<script>
			try {
				if(/Trident\/|MSIE/.test(window.navigator.userAgent) && window.document.documentMode) {
					top.opener = null;
				}
			}catch(e){}
	</script>
</head>
<body style="overflow: hidden;border: 0;padding: 0;margin: 0;">
<iframe id="frame" name="frame" style="width: 100%;height: 100%;border: 0;padding: 0;margin: 0;" frameborder="0" src="<%=request.getContextPath()%>/apps/fbs/app/?$T=<%=URLEncoder.encode(home, "UTF-8")%>&isfi=<%=isfi%><%=queryString%>"></iframe>
</body>
</html>
<%
} else {
%>
<html>
<head>
    <script>
        this.location = "<%=request.getContextPath()%>/apps/fbs/app/?$H=<%=URLEncoder.encode(home, "UTF-8")%>&isfi=<%=isfi%><%=queryString%>";
    </script>
</head>
</html>
<%
        }
    }
%>
