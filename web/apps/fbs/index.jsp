<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.acm.AuthCredentialKey" %>
<%@ page import="com.bizflow.io.services.core.acm.CredentialKeyType" %>
<%@ page import="com.bizflow.io.services.core.bf.security.BizFlowSecurity" %>
<%@ page import="com.bizflow.io.services.core.message.util.I18NMessageUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="com.bizflow.io.services.core.model.ServiceConstant" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%
    String errorMsg = null;
    boolean validRequest = AcmManager.getInstance().isValidRequest(request, false);
    if (validRequest) {
        AcmManager.getInstance().setUserAppName(session, CoreServiceConfigUtil.getAppDevServiceName());
        AuthCredentialKey credentialKey = AcmManager.getInstance().getAuthCredentialKey(session);
        if (CredentialKeyType.BizFlow.equals(credentialKey.getCredentialKeyType())) {
            if (!BizFlowSecurity.getInstance().checkUserLicense(credentialKey.getCredentialKey(), request.getSession())) {
                errorMsg = I18NMessageUtil.getMessage(CoreServiceConfigUtil.getServiceName(), "license.invalidLicense");
                validRequest = false;
            }
        }
    }
%>
<html>
<head>
    <script>
        <%if(null != errorMsg) {%>
        alert("<%=Encode.forJavaScript(errorMsg)%>");
        <%}%>

        <%
        String acmType = CoreServiceConfigUtil.getConfiguration().getProperty(CoreServiceProperties.AcmType);
        if (ServiceConstant.AcmTypeBizFlowAuthSP.equals(acmType) && null != errorMsg) {
        %>
        this.location = "../../auth/bizauth/auth-logout.jsp?HOME=<%=Base64.encodeBase64String(request.getRequestURI().getBytes())%>";
        <%} else if (validRequest) {%>
        this.location = "app/index.html?v=<%=MoonManager.getInstance().get("fbs").getVersion()%>";
        <%} else {%>
        this.location = "../../auth";
        <%} %>
    </script>
</head>
</html>
