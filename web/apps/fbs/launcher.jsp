<%@ page import="com.bizflow.io.core.bizflow.api.BizFlowBaseApi" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.acm.AuthCredentialKey" %>
<%@ page import="com.bizflow.io.services.core.acm.CredentialKey" %>
<%@ page import="com.bizflow.io.services.core.acm.CredentialKeyType" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="com.hs.bf.web.beans.HWSessionInfo" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%
    if (CoreServiceConfigUtil.getConfiguration().getPropertyAsBoolean(CoreServiceProperties.SecuritySecureResourceUse, true)) {
        if (!AcmManager.getInstance().isValidRequest(request)) {
            String sessionInfoXml = request.getParameter("sessioninfo");
            if (null != sessionInfoXml) {
                HWSessionInfo hwSessionInfo = BizFlowBaseApi.createHWSessionInfo(sessionInfoXml);
                String id = hwSessionInfo.getUserID();
                AuthCredentialKey authCredentialKey = new CredentialKey(id, id, sessionInfoXml, new Date(), ServletUtil.getRemoteAddress(request), CredentialKeyType.BizFlow);
                authCredentialKey.setIpAddressRestriction(Boolean.FALSE);
                AcmManager.getInstance().setAuthCredentialKey(session, authCredentialKey);
            }
        }
    }
%>
<html>
<script>
    function _onload() {
        document.forms[0].submit();
    }
</script>
<body onload="_onload();">
<form method="get" action="app/index.html">
    <%
        /*
         * Parameters:
         * isfi=cmZlOTVkYjhiZDdiMjQ0OGVhZDcxN2JmNWMxNWUxNzQyfHhmMTU3ZjA4MTJkYTE0MzhkOWIyNGY3ZmQ1YWJhMmNkMHxoYWJjZGI0YjhlNmY1NGU4ZTk3NzU1N2QxZTcyYWEzNmF8bGlnaHQ=
         * svrid=0000001001
         * procid=119
         * responsegroupid=1
         * workseq=134
         * actid=8
         * appidx=135
         * isarchive=false
         * readOnly=n
         * actseq=8
         * appseq=135
         * browser=NS
         * lang=en
         * sessioninfo=<SESSIONINFO KEY="10676_1614718091" USERID="0000000103" SERVERID="0000001001" IP="bizformdev2.bizflow.com" PORT="7201" DEPTID="0000000101" USERTYPE="U"  VERSION="14.0.0.0000.00" />
         * timeformat=MM/dd/yy HH:mm
         * timezone=America/New_York
         */

        Enumeration paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String name = (String) paramNames.nextElement();
            if (!"sessioninfo".equalsIgnoreCase(name)) {
                String[] values = request.getParameterValues(name);
                if (null != values) {
                    for (int v = 0; v < values.length; v++) {
                        String value = values[v];
    %>
    <input type="hidden" name="<%=Encode.forHtmlAttribute(name)%>" value="<%=Encode.forHtmlAttribute(value)%>">
    <%
                    }
                }
            }
        }
    %>
</form>
</body>
</html>
