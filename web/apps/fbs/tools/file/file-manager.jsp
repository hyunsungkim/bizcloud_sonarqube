<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.file.util.FileServiceConfigUtil" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AppDev File Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/lib/codemirror.css">
    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/addon/merge/merge.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../../includes/node_modules/angular-file-upload/dist/angular-file-upload.min.js"></script>
    <script src="../../../../includes/node_modules/codemirror/lib/codemirror.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/css/css.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/javascript/javascript.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="../../../../includes/node_modules/diff-match-patch/javascript/diff_match_patch.js"></script>
    <script src="../../../../includes/node_modules/codemirror/addon/merge/merge.js"></script>
    <script src="../../../../includes/node_modules/pdf.js/dist/build/pdf.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("file-manager.js");
    </script>
    <style>
        tr.file td {
            padding: 2px 5px 2px 5px;
        }

        tr.file2 td {
            padding: 2px 5px 2px 5px;
            border: 0;
            border-bottom: 1px solid #dee2e6;
        }
    </style>
    <script>
        var textColor = "<%=textColor%>";
        var maxFileSize = <%=FileServiceConfigUtil.getMaxFileSize()%>;
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>AppDev File Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row flex-nowrap" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div style="padding-bottom: 0;" id="leftPanelHeader">
                    <div class="bio-left-title">
                        <div class="title"><h5>File</h5></div>
                        <div class="i-btn">
                            <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshTree();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block" id="treeContainer">
                    <div class="form-group form-group-sm col-md-12">
                        <input type="text" ng-model="treeOptions.filter.name" class="searchinput" placeholder="Enter a name to filter" style="margin-bottom: 10px;">
                        <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                    </div>
                    <div id="treePanelContainer" class="panelContainer">
                        <tree nodes='treeRoot' options="treeOptions"></tree>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary col-md-12" id="rightPanelContainer">
                <div id="rightPanelHeader">
                    <div class="bio-right-title" ng-if="fileList">
                        <div class="title">
                            <div class="textbox">
                                <label for="title">Title</label>
                                <input type="text" id="title" ng-enter="searchFile()" ng-model="search.LIKE_title_LIKE">
                            </div>
                            <div class="textbox">
                                <label for="name">Name</label>
                                <input type="text" id="name" ng-enter="searchFile()" ng-model="search.LIKE_name_LIKE">
                            </div>
                            <div class="textbox">
                                <label for="type">Type</label>
                                <input type="text" id="type" ng-enter="searchFile()" ng-model="search.type">
                            </div>
                            <div class="textbox">
                                <button class="bf-button p-component bf-button-icon-only" ng-click="searchFile()" title="Search">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="bf-button p-component bf-button-icon-only" ng-if="selectedNode" ng-click="refreshList();$event.stopPropagation();" title="refresh">
                                    <i class="fa fa-repeat"></i>
                                </button>
                            </div>
                        </div>
                        <div class="i-btn">
                            <button class="p-button p-component p-button-success p-button-text-only" ng-if="selectedNode" ng-click="uploadFile();$event.stopPropagation();">Upload</button>
                            <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="showDeletedFile();$event.stopPropagation();">Recovery</button>
                        </div>
                    </div>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div id="DataValue" ng-if="fileList">
                        <div ng-if="fileList.data.length == 0">
                            <div class="text-center">There are no files</div>
                        </div>
                        <div class="bf-table-2">
                            <table class="table table-striped table-condensed table-font-small text-<%=textColor%>" ng-if="fileList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold">
                                    <th>#</th>
                                    <th ng-click="sortFileList('title')"><a>Title</a></th>
                                    <th class="w5" ng-click="sortFileList('type')"><a>Type</a></th>
                                    <th class="w5" ng-click="sortFileList('fileSize')" nowrap><a>Size</a></th>
                                    <th class="w5" ng-click="sortFileList('district')"><a>Level</a></th>
                                    <th ng-click="sortFileList('name')"><a>Name</a></th>
                                    <th class="w20"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in fileList.data" ng-click="selectFile(item)" id="{{item.id}}" class="file">
                                    <td>{{item.ROW_NUMBER}}</td>
                                    <td><a target="{{item.id}}" ng-href="{{getFileUrl(item, item.title)}}" title="{{item.title}}">{{item.title}}</a></td>
                                    <td>{{item.type}}</td>
                                    <td>{{item.fileSize | number:0}}</td>
                                    <td>{{item.district}}</td>
                                    <td><a target="{{item.id}}" ng-href="{{getFileUrl(item, item.name)}}" title="{{item.name}}">{{item.name}}</a></td>
                                    <td class="right-btn" style="vertical-align: middle;">
                                        <button type="button" class="p-button p-component p-button-info" ng-if="item.extension && item.extension.toLowerCase() == 'zip'" ng-click="showEntityList(item);$event.stopPropagation();"> Entity List</button>
                                        <button type="button" class="p-button p-component" ng-click="updateFile(item);$event.stopPropagation();">Edit</button>
                                        <button type="button" class="p-button p-component p-button-secondary" ng-click="copyUrl(item);$event.stopPropagation();">Copy URI</button>
                                        <button type="button" class="p-button p-component p-button-secondary" ng-click="copyUrl(item, 'full');$event.stopPropagation();">Copy Full URL</button>
                                        <button type="button" class="p-button p-component p-button-info p-button-secondary" ng-click="showFileHistory(item);$event.stopPropagation();">History</button>
                                        <button type="button" class="p-button p-component p-button-danger" ng-click="deleteFile(item);$event.stopPropagation();">Delete</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="fileList.totalCount > fileListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="fileList.totalCount" items-per-page="fileListPageSize" ng-model="$parent.$parent.fileListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changeFileListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>