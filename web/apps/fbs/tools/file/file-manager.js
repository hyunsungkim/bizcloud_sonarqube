var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
    , 'angularFileUpload'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$timeout', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', 'FileUploader', function ($scope, $timeout, $filter, $uibModal, blockUI, AjaxService, marsContext, FileUploader) {
    var parameter = new UrlParameterParser(location.href);
    $scope.projectId = parameter.getParameterValue("project_id", '');
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;
    // Constants
    var SystemFileTypeLookupDataTypeId = 'l7ba6654bac3b4d9ba24152ff9dbec297';
    var TreeRoot = 'TreeRoot';
    var Application = 'Application';
    var Workspace = 'Workspace';
    var Project = 'Project';
    var ApplicationNodeId = "A0";

    $scope.Workspace = Workspace;
    $scope.Project = Project;
    $scope.TreeRoot = TreeRoot;

    // Variables
    $scope.selectedNodeType = null;
    $scope.search = {};

    var blockUIStarted = false;

    function startBlockUI(msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function getObjectType(node) {
        var objectType = null;

        if ('undefined' != typeof node.projectCount) {
            objectType = Workspace;
        } else if ('undefined' != typeof node.formCount) {
            objectType = Project;
        } else if (node.nodeId === ApplicationNodeId) {
            objectType = Application;
        } else {
            objectType = TreeRoot;
        }

        return objectType;
    }

    function generateNodeId(node) {
        var objectType = getObjectType(node);
        var nodeId = "";
        if (Workspace === objectType) {
            nodeId = "W" + node.id;
        } else if (Project === objectType) {
            nodeId = "P" + node.id;
        } else {
            nodeId = ApplicationNodeId;
        }

        return nodeId;
    }

    function getNodeImage(node) {
        var image;
        var objectType = getObjectType(node);
        if (Workspace === objectType) {
            image = "../image/W.png";
        } else if (Project === objectType) {
            image = "../image/P.png";
        } else {
            image = "../image/A.png";
        }

        return image;
    }

    $scope.treeOptions = {
        titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
        generateNodeId: generateNodeId,
        getNodeImage: getNodeImage,
        onSelectNode: function (node, selectedByClick) {
            $scope.selectedNode = node;
            $scope.selectedNodeType = getObjectType(node);
            $scope.treeOptions.selectedNode = node;
            $scope.dataValueListOrderColumn = 'label';

            if (selectedByClick) {
                getFileList(1);
            }
        },
        onExpandNode: function (node) {
            $scope.expandedNode = node;
        }
    };

    function getWorkspaceTree() {
        var param = {
            LIKE_permission_LIKE: ["%M%", "P"]
        }
        if ($scope.projectId) param.projectId = $scope.projectId;
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.workspace-GetWorkspaceProjectTree@.json');
        api.post({
            success: function (o) {
                $scope.treeRoot = [{
                    name: "Root",
                    nodeId: "rootNode",
                    image: "../image/R.png",
                    children: o.data
                }];

                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.refreshTree = function () {
        getWorkspaceTree();
    };

    (function () {
        getWorkspaceTree();
        getDocumentType();
    })();

    $scope.fileListCurrentPage = 1;
    $scope.fileListPageSize = 15;
    $scope.fileListOrderColumn = 'name';
    $scope.fileListOrderAsc = true;

    $scope.setPageSize = function (size) {
        $scope.fileListPageSize = size;
    };

    function callFileListApi(param) {
        if (Application === $scope.selectedNodeType || TreeRoot === $scope.selectedNodeType) {
            param.district = 'Application';
        } else if (Workspace === $scope.selectedNodeType) {
            param.district = 'Workspace';
            param.workspaceId = $scope.selectedNode.id;
        } else if (Project === $scope.selectedNodeType) {
            param.district = 'Project';
            param.projectId = $scope.selectedNode.id;
        }

        var api = new AjaxService(marsContext.contextPath + '/services/file/run/fbs.file-GetFileList@.json');
        api.post({
            success: function (o) {
                $scope.fileList = o.data;
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function getFileList(pageNo, filter) {
        var param = {
            pageNo: pageNo || $scope.fileListCurrentPage,
            pageSize: $scope.fileListPageSize,
            ORDER_BY: $scope.fileListOrderColumn + ' ' + ($scope.fileListOrderAsc ? 'ASC' : 'DESC')
        };

        $scope.fileListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        callFileListApi(param);
    }

    $scope.searchFile = function () {
        getFileList(1, $scope.search);
    };

    $scope.changeFileListPage = function () {
        getFileList();
    };

    $scope.sortFileList = function (column) {
        if ($scope.fileListOrderColumn === column) {
            $scope.fileListOrderAsc = !$scope.fileListOrderAsc;
        } else $scope.fileListOrderAsc = true;
        $scope.fileListOrderColumn = column;
        getFileList($scope.fileListCurrentPage);
    };

    $scope.selectFile = function (file) {
        // if ($scope.selectedRow) {
        //     $scope.selectedRow.style.backgroundColor = $scope.selectedRow.getAttribute("oldBackgroundColor");
        // }
        // $scope.selectedFile = file;
        // $scope.selectedRow = document.getElementById(file.id);
        // $scope.selectedRow.setAttribute("oldBackgroundColor", $scope.selectedRow.style.backgroundColor);
        // $scope.selectedRow.style.backgroundColor = "#0d0d0d";
    };

    $scope.getFileUrl = function (item, fileName) {
        fileName = mars$util$.addFileExtension(fileName, item.extension);
        return marsContext.contextPath + "/services/file/get/fbs.file-DownloadFile@/" + item.id + "/" + fileName
            + "?etpl=/core/exception/ExceptionResponse.html";
    };

    $scope.refreshList = function (action) {
        getFileList(undefined, $scope.search);
        if ("update" === action) {
            showMessageInfo("Successfully Saved.");
        }
    };

    function deleteFile(item) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/run/fbs.file-DeleteFile@.json');
        api.post({
            success: function (o) {
                if ($scope.fileList.data.length === 1 && $scope.fileListCurrentPage > 1) {
                    $scope.fileListCurrentPage -= 1;
                }
                $scope.refreshList();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, {
            id: item.id
        });
    }

    $scope.deleteFile = function (item) {
        bootbox.confirm({
            message: "Are you sure to delete the file '" + item.title + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteFile(item);
                }
            }
        });
    };

    function getDocumentType() {
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.lookup-GetEvaluatedLookupDataValue.json');
        api.post({
            success: function (o) {
                $scope.documentTypes = o.data;
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, {
            typeId: SystemFileTypeLookupDataTypeId
        });
    }

    function beforeUploadFile(item) {
        var param = {
            type: item.type,
            name: item.file.name
        };

        if (item.title) {
            param.title = item.title;
        }

        if (item.id) {
            param.id = item.id;
        }

        if (Workspace === $scope.selectedNodeType) {
            param.workspaceId = $scope.selectedNode.id;
        } else if (Project === $scope.selectedNodeType) {
            param.projectId = $scope.selectedNode.id;
        }

        var formData = [param];

        Array.prototype.push.apply(item.formData, formData);
    }

    $scope.uploadFile = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/file/file-upload.html'),
            size: 'xl',
            controller: ['$scope',
                function ($scope) {
                    var config = $scope.config = {
                        title: "Upload File(s)",
                        multiple: true,
                        typeRequired: true
                    };

                    $scope.scope = scope;
                    $scope.documentTypes = scope.documentTypes;

                    var uploader = $scope.uploader = new FileUploader({
                        url: marsContext.contextPath + '/services/file/run/fbs.file-UploadFile@.json',
                        contentType: "multipart/form-data;charset=UTF-8"
                    });

                    uploader.onAfterAddingFile = function (item) {
                        try {
                            if (item.file.size > maxFileSize) {
                                item.error = item.error || {};
                                item.error.msg = "File Size exceeds the max file size " + (maxFileSize / 1024 / 1024).toFixed(3) + "MB";
                            }
                        } catch (e) {
                        }

                        try {
                            var mimeType = item.file.type;
                            if (mimeType.substring(0, 5) === "image") {
                                item.type = "Image";
                            } else if (mimeType === 'text/css') {
                                item.type = "CSS";
                            } else if (mimeType === 'text/html') {
                                item.type = "HTML";
                            } else if (mimeType === 'text/javascript') {
                                item.type = "Javascript";
                            } else {
                                item.type = "Other";
                            }
                        } catch (ignore) {
                        }
                    };

                    uploader.onBeforeUploadItem = function (item) {
                        beforeUploadFile(item);
                    };

                    function isErrorItems() {
                        for (var i = 0; i < uploader.queue.length; i++) {
                            var item = uploader.queue[i];
                            if (item.error) {
                                return true;
                            }
                        }

                        return false;
                    }

                    uploader.onErrorItem = function (fileItem, response, status, headers) {
                        fileItem.error = {
                            response: response,
                            status: status,
                            headers: headers
                        };

                        fileItem.error.msg = angularExt.makeAjaxExceptionString(mars$cipher$.decipher(response));
                    };

                    uploader.onCompleteAll = function () {
                        if (!isErrorItems()) {
                            uploader.clearQueue();
                            $scope.$dismiss();
                            scope.refreshList();
                        }
                    };

                    $scope.contextPath = marsContext.contextPath;

                    $scope.removeFile = function (fileItem) {
                        fileItem.remove();
                    };

                    $scope.isReadyToUpload = function () {
                        var ready = uploader.queue.length && uploader.queue.length > 0;

                        if (ready) {
                            for (var i = 0; i < uploader.queue.length; i++) {
                                var item = uploader.queue[i];
                                if ((config.typeRequired && angularExt.isInvalidObject(item.type)) || item.error) {
                                    ready = false;
                                    break;
                                }
                            }
                        }

                        return ready;
                    };

                    $scope.upload = function () {
                        if (uploader.queue.filter(function (el) {
                            return el.type == undefined
                        }).length > 0) {
                            angularExt.getBootboxObject().alert("Please enter the file type of all files");
                        } else {
                            uploader.uploadAll();
                        }
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close();
                    };
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    $scope.updateFile = function (item) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/file/file-update.html'),
            size: 'xl',
            controller: ['$scope',
                function ($scope) {
                    var config = $scope.config = {
                        title: "Update File",
                        multiple: false,
                        typeRequired: true,
                        uploadUrl: marsContext.contextPath + '/services/file/run/fbs.file-UpdateFile@.html?tpl=/core/ServiceResultCallback.html'
                    };

                    $scope.scope = scope;
                    $scope.document = angular.copy(item);
                    $scope.documentTypes = scope.documentTypes;

                    $scope.isReadyToUpload = function (id) {
                        var sizeOk = true;
                        try {
                            var input = document.getElementById(id);
                            sizeOk = input.files[0].size < maxFileSize;
                        } catch (e) {
                        }
                        return $scope.document.title && $scope.document.type && sizeOk;
                    };

                    $scope.upload = function (id) {
                        if(checkUploadFile(id)) {
                            mars$util$.createHiddenFrame(1);
                            $scope.uploading = true;
                            var form = document.forms[0];
                            if ($scope.document.workspaceId) {
                                mars$util$.createHiddenField(form, "workspaceId", "workspaceId", $scope.document.workspaceId);
                            }
                            if ($scope.document.projectId) {
                                mars$util$.createHiddenField(form, "projectId", "projectId", $scope.document.projectId);
                            }
                            form.action = config.uploadUrl;
                            startBlockUI('Uploading...');
                            form.submit();
                        }
                    };

                    $scope.serviceExceptionCallback = function () {
                        stopBlockUI();
                        $scope.uploading = false;
                        $scope.cancel();
                    };

                    $scope.serviceResultCallback = function () {
                        stopBlockUI();
                        scope.refreshList();
                        $scope.uploading = false;
                        $scope.cancel();
                        angularExt.getBootboxObject().alert("The file has been updated.");
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close();
                    };
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    function copyUrl(uri, mode) {
        if (mode === 'full') {
            mars$util$.copyToClipboard(mars$util$.getProtocolAndHost() + marsContext.contextPath + encodeURI(uri));
        } else {
            mars$util$.copyToClipboard(encodeURI(uri).substring(1));
        }
    }

    $scope.copyUrl = function (item, mode) {
        copyUrl(item.uri, mode);
    };

    function getEntityList(item, scope) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/getEntityList/fbs.file-GetEntityList@/' + item.id + '.json');
        api.get({
            success: function (o) {
                scope.entityList = o.data;
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    $scope.showEntityList = function (item) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/file/file-entity-list.html'),
            size: 'lg',
            controller: ['$scope',
                function ($scope) {
                    var config = $scope.config = {
                        title: item.name + " File Entity List"
                    };
                    $scope.scope = scope;
                    $scope.file = item;
                    $scope.textColor = textColor;

                    getEntityList(item, $scope);

                    $scope.getFileItemUrl = function (file, item) {
                        return "/services/file/get/fbs.file-DownloadFile@/" + file.id + "/" + file.name + "/" + item.name.replace('/', '|').replace('\\', '|');
                    };

                    $scope.getFileUrl = function (file, item) {
                        return marsContext.contextPath + "/services/file/get/fbs.file-DownloadFile@/" + file.id + "/" + file.name + "/" + item.name.replace('/', '|').replace('\\', '|')
                            + "?etpl=/core/exception/ExceptionResponse.html";
                    };

                    $scope.copyUrl = function (file, item, mode) {
                        copyUrl($scope.getFileItemUrl(file, item), mode);
                    };

                    $scope.setHeight = function () {
                        var clientSize = mars$util$.getClientSize();
                        var fileEntityListBody = document.getElementById("fileEntityListBody");
                        if (fileEntityListBody) fileEntityListBody.style.maxHeight = clientSize.height * 0.75 + "px";
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close();
                    };
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    function showMessageAlert(message) {
        showMessage(message, "Alert");
    }

    function showMessageInfo(message) {
        showMessage(message, "Info");
    }

    function showMessageWarning(message) {
        showMessage(message, "Warning");
    }

    function showMessage(message, type) {
        if (parent && parent.$B && parent.$B.ExceptionHandler) {
            if (type === "Alert") {
                parent.$B.ExceptionHandler.pin('Alert', message);
            } else if (type === "Info") {
                parent.$B.ExceptionHandler.toast('Info', message);
            } else if (type === "Warning") {
                parent.$B.ExceptionHandler.toast('Warning', message);
            }
        }
    }

    function getHistory(item, callback) {
        $scope.fileHistories = [];
        $scope.document = angular.copy(item);
        var param = {
            id: $scope.document.id
        };
        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.file-GetFileHistorySimpleList@.json');
        api.post({
            success: function (o) {
                $scope.fileHistories = o.data.data;
                if (callback) {
                    callback();
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.showFileHistory = function (item) {
        var callback = $scope.loadFileHistory;
        getHistory(item, callback);
    };

    $scope.loadFileHistory = function () {
        var scope = $scope;
        var leftEditor, rightEditor, mode;
        var pageSize = 10;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/file/file-history.html'),
            size: 'xl',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.document = scope.document;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = "File Versioning";
                    $scope.selectedNode = scope.selectedNode;
                    $scope.selectedNodeType = scope.selectedNodeType;
                    $scope.fileHistoriesLeft = scope.fileHistories;
                    $scope.fileHistoriesRight = scope.fileHistories;
                    $scope.selectedHistoryLeft = scope.fileHistories[0];
                    $scope.selectedHistoryRight = scope.fileHistories[0];
                    $scope.selectedNumLeft = 0;
                    $scope.selectedNumRight = -1;
                    $scope.contentLeft = {};
                    $scope.contentRight = {};
                    $scope.leftVersion = null;
                    $scope.rightVersion = null;

                    function initUI(view, value) {
                        if ($scope.document.type === 'CSS') {
                            mode = 'text/css';
                        } else if ($scope.document.type === 'HTML') {
                            mode = 'text/html';
                        } else {
                            mode = 'text/javascript';
                        }
                        if ('L' === view) {
                            leftEditor = CodeMirror(document.getElementById("codemirrorLeft"), {
                                value: value,
                                mode: mode,
                                lineNumbers: true,
                                readOnly: true
                            });
                        } else {
                            rightEditor = CodeMirror(document.getElementById("codemirrorRight"), {
                                value: value,
                                mode: mode,
                                lineNumbers: true,
                                readOnly: true
                            });
                        }
                    }

                    function previewContent(view, base64Content) {
                        if ($scope.document.type === 'Image') {
                            var type = $scope.document.extension.toLowerCase() === 'svg' ? 'svg+xml' : '*';
                            if ('L' === view) {
                                document.getElementById('imageLeft').src = 'data:image/' + type + ';base64,' + base64Content;
                            } else {
                                document.getElementById('imageRight').src = 'data:image/' + type + ';base64,' + base64Content;
                            }
                        } else if ($scope.document.type === 'CSS' || $scope.document.type === 'HTML' || $scope.document.type === 'Javascript') {
                            var s = atob(base64Content);
                            if ('L' === view) {
                                if (leftEditor !== undefined && leftEditor != null) {
                                    leftEditor.setValue(s);
                                } else {
                                    initUI('L', s);
                                }
                            } else {
                                if (rightEditor !== undefined && rightEditor != null) {
                                    rightEditor.setValue(s);
                                } else {
                                    initUI('R', s);
                                }
                            }
                        } else {
                            if ($scope.document.extension.toLowerCase() === 'pdf') {
                                $scope.document.type = 'pdf';
                                var pdfData = atob(base64Content);

                                pdfjsLib.GlobalWorkerOptions.workerSrc = '../../../../includes/node_modules/pdf.js/dist/build/pdf.worker.js';

                                var loadingTask = pdfjsLib.getDocument({data: pdfData});
                                loadingTask.promise.then(function (pdf) {

                                    // Fetch the first page
                                    var pageNumber = 1;
                                    pdf.getPage(pageNumber).then(function (page) {

                                        var scale = 0.5;
                                        var viewport = page.getViewport({scale: scale});

                                        // Prepare canvas using PDF page dimensions
                                        var canvas = 'L' === view ? document.getElementById('pdfLeft') : document.getElementById('pdfRight');
                                        var context = canvas.getContext('2d');
                                        canvas.height = viewport.height;
                                        canvas.width = viewport.width;

                                        // Render PDF page into canvas context
                                        var renderContext = {
                                            canvasContext: context,
                                            viewport: viewport
                                        };
                                        var renderTask = page.render(renderContext);
                                        renderTask.promise.then(function () {
                                            // console.log('Page rendered');
                                        });
                                    });
                                }, function (reason) {
                                    // PDF loading error
                                    console.error(reason);
                                });
                            } else {
                                $scope.otherType = true;
                            }
                        }
                        resize(view);
                    }

                    function getVersionContent(view, version) {
                        var param = {
                            id: $scope.document.id,
                            version: version
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.file-GetFileHistoryContent.json');
                        api.post({
                            success: function (o) {
                                if (view === 'L') {
                                    $scope.leftVersion = $scope.fileInfoLeft.version;
                                    $scope.contentLeft.content = o.data[0].content;
                                    $scope.contentLeft.isChanged = true;
                                    previewContent(view, $scope.contentLeft.content);
                                } else {
                                    $scope.rightVersion = $scope.fileInfoRight.version;
                                    $scope.contentRight.content = o.data[0].content;
                                    $scope.contentRight.isChanged = true;
                                    previewContent(view, $scope.contentRight.content);
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    function getHistoryContent(selectedData, selectedView, callback) {
                        $scope.contentLeft.isChanged = false;
                        $scope.contentRight.isChanged = false;
                        var param = {
                            id: selectedData.id,
                            transactionId: selectedData.transactionId
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.file-GetFileHistory.json');
                        api.post({
                            success: function (o) {
                                if (o.data) {
                                    var fileInfo = o.data[0];
                                    if ("L" === selectedView) {
                                        $scope.fileInfoLeft = fileInfo;
                                        if ($scope.leftVersion == null) $scope.leftVersion = fileInfo.version;
                                        if ($scope.fileInfoLeft.version !== $scope.leftVersion || $scope.contentLeft.content == null) {
                                            getVersionContent(selectedView, $scope.fileInfoLeft.version);
                                        } else {
                                            previewContent(selectedView, $scope.contentLeft.content);
                                        }
                                    } else if ("R" === selectedView) {
                                        $scope.fileInfoRight = fileInfo;
                                        if ($scope.rightVersion == null) $scope.rightVersion = fileInfo.version;
                                        if ($scope.fileInfoRight.version !== $scope.rightVersion || $scope.contentRight.content == null) {
                                            getVersionContent(selectedView, $scope.fileInfoRight.version);
                                        } else {
                                            previewContent(selectedView, $scope.contentRight.content);
                                        }
                                    }
                                    if (callback) {
                                        callback();
                                    }
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    $scope.selectHistoryLeft = function (idx, selectedData) {
                        $scope.selectedNumLeft = idx;
                        $scope.selectedHistoryLeft = idx===-1 ? $scope.fileHistoriesLeft[0] : selectedData;
                        getHistoryContent($scope.selectedHistoryLeft, 'L');
                    };
                    $scope.selectHistoryRight = function (idx, selectedData) {
                        $scope.selectedNumRight = idx;
                        $scope.selectedHistoryRight = idx===-1 ? $scope.fileHistoriesRight[0] : selectedData;
                        getHistoryContent($scope.selectedHistoryRight, 'R');
                    };

                    $scope.loadMore = function (view) {
                        pageSize = 10000;
                        $scope.getHistoryList(view);
                    };

                    $scope.getHistoryList = function (view, callback) {
                        var param = {
                            id: $scope.document.id,
                            pageSize: pageSize
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.file-GetFileHistorySimpleList@.json');
                        api.post({
                            success: function (o) {
                                $scope.fileHistoriesLeft = o.data.data;
                                $scope.fileHistoriesRight = o.data.data;

                                if (callback) {
                                    if (callback === initHistoryView) {
                                        $(".list-group").scrollTop(0);
                                        $(".content").scrollTop(0);
                                        $scope.selectedHistoryLeft = $scope.fileHistoriesLeft[0];
                                        $scope.selectedHistoryRight = $scope.fileHistoriesRight[0];
                                        $scope.selectedNumLeft = 0;
                                        $scope.selectedNumRight = -1;
                                        $scope.contentLeft = {};
                                        $scope.contentRight = {};
                                    }
                                    callback();
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    };

                    $scope.getFileUrl = function (item) {
                        if (item) {
                            var fileName = mars$util$.addFileExtension(item.name, item.extension);
                            return marsContext.contextPath + "/services/file/get/fbs.file-DownloadFile@/" + item.id + "/" + fileName
                                + "?version=" + item.version + "&etpl=/core/exception/ExceptionResponse.html";
                        }
                    };

                    $scope.isValueDiff = function (name) {
                        if (null != name && $scope.fileInfoLeft && $scope.fileInfoRight) {
                            return $scope.fileInfoLeft[name] !==  $scope.fileInfoRight[name];
                        }
                        return false;
                    };

                    function updateFile(fileData, callback) {
                        var api = new AjaxService(marsContext.contextPath + '/services/file/run/fbs.file-UpdateFileBase64@.json');
                        api.post({
                            success: function (o) {
                                scope.refreshList("update");
                                if (callback) {
                                    callback();
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, fileData);
                    }

                    $scope.revert = function (view) {
                        var file = 'L' === view ? $scope.fileInfoLeft : $scope.fileInfoRight;
                        var data = {
                            id: file.id,
                            title: file.title,
                            type: file.type
                        };
                        if (file.workspaceId) data.workspaceId = file.workspaceId;
                        if (file.projectId) data.projectId = file.projectId;

                        if (view === 'L' && $scope.contentLeft.isChanged) {
                            data.fileInputStream = $scope.contentLeft.content;
                            data.name = file.name;
                        } else if (view === 'R' && $scope.contentRight.isChanged) {
                            data.fileInputStream = $scope.contentRight.content;
                            data.name = file.name;
                        }
                        updateFile(data, resetHistory);
                    };

                    function resetHistory() {
                        var callback = initHistoryView;
                        $scope.getHistoryList("ALL", callback);
                    }

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };

                    function resize(view) {
                        var historyContainer = document.getElementById("historyContainer");
                        var historyContentHeader = document.getElementById("historyContentHeader");
                        var historyContent = document.getElementById("historyContent");
                        var contentHeight = historyContainer.offsetHeight - historyContentHeader.offsetHeight;

                        if (historyContent) historyContent.style.height = contentHeight + "px";

                        if ($scope.document.type === 'CSS' || $scope.document.type === 'HTML' || $scope.document.type === 'Javascript') {
                            var table1 = document.getElementById("table1");
                            var table2 = document.getElementById("table2");
                            if (view === 'L') {
                                leftEditor.setSize(null, contentHeight - table1.offsetHeight);
                            } else if (view === 'R') {
                                rightEditor.setSize(null, contentHeight - table2.offsetHeight);
                            } else {
                                leftEditor.setSize(null, contentHeight - table1.offsetHeight);
                                rightEditor.setSize(null, contentHeight - table2.offsetHeight);
                            }
                        }
                    }

                    $(window).on('resize', function () {
                        resize();
                    });

                    $scope.setHeight = function () {
                        setHeight();
                    };

                    function initHistoryView() {
                        var callback = getHistoryContent($scope.selectedHistoryRight, "R");
                        getHistoryContent($scope.selectedHistoryLeft, "L", callback);
                    }

                    (function () {
                        initHistoryView();
                    })();

                }
            ]
        }).result.then(function (fileData) {
        }, function () {
        });
    };

    $scope.showDeletedFile = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/file/file-recover.html'),
            size: 'lg',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.document = scope.document;
                    $scope.selectedNode = scope.selectedNode;
                    $scope.selectedNodeType = scope.selectedNodeType;
                    $scope.title = 'Deleted File List';

                    $scope.deletedListCurrentPage = 1;
                    $scope.deletedListPageSize = 10;
                    $scope.deletedListOrderColumn = 'name';
                    $scope.deletedListOrderAsc = false;

                    function getDeletedFileList(pageNo, callback) {
                        var param = {
                            pageNo: pageNo || $scope.deletedListCurrentPage,
                            pageSize: $scope.deletedListPageSize,
                            ORDER_BY: $scope.deletedListOrderColumn + ' ' + ($scope.deletedListOrderAsc ? 'ASC' : 'DESC')
                        };
                        if (Application === $scope.selectedNodeType || TreeRoot === $scope.selectedNodeType) {
                            param.district = 'Application';
                        } else if (Workspace === $scope.selectedNodeType) {
                            param.district = 'Workspace';
                            param.workspaceId = $scope.selectedNode.id;
                        } else if (Project === $scope.selectedNodeType) {
                            param.district = 'Project';
                            param.projectId = $scope.selectedNode.id;
                        }

                        $scope.deleListCurrentPage = param.pageNo;

                        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.file-GetDeletedFileList@.json');
                        api.post({
                            success: function (o) {
                                $scope.deletedList = o.data;
                                if (callback) {
                                    callback();
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    $scope.sortFileList = function (column) {
                        if ($scope.deletedListOrderColumn === column) {
                            $scope.deletedListOrderAsc = !$scope.deletedListOrderAsc;
                        } else $scope.deletedListOrderAsc = true;
                        $scope.deletedListOrderColumn = column;
                        getDeletedFileList($scope.historyListCurrentPage);
                    };

                    $scope.changeDeletedFileListPage = function () {
                        getDeletedFileList();
                    };

                    function insertFile(param) {
                        var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.file-InsertFile@.json');
                        api.post({
                            success: function (o) {
                                showMessageInfo("Successfully recovered.");
                                getDeletedFileList(undefined, scope.refreshList);
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    function getDeletedContent(file) {
                        var param = {
                            id: file.id,
                            version: file.version
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.file-GetFileHistoryContent.json');
                        api.post({
                            success: function (o) {
                                file.content = o.data[0].content;

                                delete file['transactionId'];
                                delete file['version'];
                                insertFile(file);
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    $scope.getFileUrl = function (item) {
                        if (item) {
                            var fileName = mars$util$.addFileExtension(item.name, item.extension);
                            return marsContext.contextPath + "/services/file/get/fbs.file-DownloadFile@/" + item.id + "/" + fileName
                                + "?version=" + item.version + "&etpl=/core/exception/ExceptionResponse.html";
                        }
                    };

                    $scope.recover = function (file) {
                        getDeletedContent(file);
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };

                    (function () {
                        getDeletedFileList();
                    })();
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    $scope.setHeight = function () {
        setHeight();
    };
}]);

function checkUploadFile(id) {
    var input = document.getElementById(id);
    try {
        if (input.files[0].size > maxFileSize) {
            var msg = "File Size exceeds the max file size " + (maxFileSize / 1024 / 1024).toFixed(3) + "MB";
            angularExt.getBootboxObject().alert(msg);
            return false;
        }
    } catch (e) {
    }

    return true;
}

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";

    var historyModal = document.getElementById("historyModal");
    if (historyModal) historyModal.style.height = (height * 0.9) + "px";
}
