var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
    , 'ng.jsoneditor'
    , 'xeditable'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$timeout', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext',
    function ($scope, $timeout, $filter, $uibModal, blockUI, AjaxService, marsContext) {
        var parameter = new UrlParameterParser(location.href);
        $scope.projectId = parameter.getParameterValue("project_id", '');
        // Constants
        var ZeroId = 'N00000000000000000000000000000000';
        var TreeRoot = 'TreeRoot';
        var Application = 'Application';
        var Workspace = 'Workspace';
        var Project = 'Project';
        var RawData = 'RawData';
        var ApplicationNodeId = "A0";

        $scope.Workspace = Workspace;
        $scope.Project = Project;
        $scope.RawData = RawData;
        $scope.TreeRoot = TreeRoot;

        // Variables
        $scope.addMode = false;
        $scope.selectedNodeType = null;
        $scope.authParams = {};
        $scope.mainParams = {};
        $scope.hiddenParams = {};
        $scope.schemaTypes = [
            'String',
            'Integer',
            'Number',
            'Date',
            'Any'
        ];

        var blockUIStarted = false;

        function startBlockUI() {
            blockUI.start();
            blockUIStarted = true;
        }

        function stopBlockUI() {
            blockUI.stop();
            blockUIStarted = false;
        }

        function JsonEditorOption() {
            var editor = null;

            this.mode = 'tree';
            this.modes = ['tree', 'code'];
            this.enableTransform = false;
            this.setEditor = function (_editor) {
                editor = _editor;
            };
            this.onChangeText = function (jsonString) {
                if (jsonString === '') {
                    editor.setText("{}");
                } else {
                    var lastStr = jsonString.substring(jsonString.length - 2);
                    if ("{}" === lastStr) {
                        editor.setText(jsonString.substring(0, jsonString.length - 2));
                    }
                }
            }
        }

        $scope.options4AuthContentBody = new JsonEditorOption();
        $scope.options4ContentBody = new JsonEditorOption();
        $scope.options4HiddenParameter = new JsonEditorOption();
        $scope.options4ReturnStructure = new JsonEditorOption();
        $scope.options4DataSchema = new JsonEditorOption();
        $scope.options4InPipeProcessorOption = new JsonEditorOption();
        $scope.options4OutPipeProcessorOption = new JsonEditorOption();

        function getObjectType(node) {
            var objectType = null;

            if ('undefined' != typeof node.projectCount) {
                objectType = Workspace;
            } else if ('undefined' != typeof node.formCount) {
                objectType = Project;
            } else if ('undefined' != typeof node.type && 'undefined' != typeof node.workspaceId && 'undefined' != typeof node.projectId) {
                objectType = RawData;
            } else if (node.nodeId === ApplicationNodeId) {
                objectType = Application;
            } else {
                objectType = TreeRoot;
            }

            return objectType;
        }

        function generateNodeId(node) {
            var objectType = getObjectType(node);
            var nodeId = "";
            if (Workspace === objectType) {
                nodeId = "W" + node.id;
            } else if (Project === objectType) {
                nodeId = "P" + node.id;
            } else if (RawData === objectType) {
                nodeId = "R" + node.id;
            } else {
                nodeId = ApplicationNodeId;
            }

            return nodeId;
        }

        function getNodeImage(node) {
            var image;
            var objectType = getObjectType(node);
            if (Workspace === objectType) {
                image = "../image/W.png";
            } else if (Project === objectType) {
                image = "../image/P.png";
            } else if (RawData === objectType) {
                image = "../image/R.png";
            } else {
                image = "../image/A.png";
            }

            return image;
        }

        function jsonToParamArray(json) {
            var objects = [];
            if (json) {
                for (var name in json) {
                    objects.push({
                            name: name,
                            value: json[name]
                        }
                    )
                }
            }
            return objects;
        }

        function jsonToSchemaArray(json) {
            var objects = [];
            if (json) {
                for (var name in json) {
                    objects.push({
                            name: name,
                            type: json[name]
                        }
                    )
                }
            }
            return objects;
        }

        function sortByKey(unordered) {
            if (unordered) {
                const ordered = {};
                Object.keys(unordered).sort().forEach(function (key) {
                    ordered[key] = unordered[key];
                });
                return ordered;
            } else return unordered;
        }

        function getName(node, id) {
            if (id !== ZeroId) {
                var parent = node.parent;
                while (parent) {
                    if (parent.id === id) {
                        return parent.name;
                    }
                    parent = parent.parent;
                }
            }
            return undefined;
        }

        $scope.treeOptions = {
            titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
            generateNodeId: generateNodeId,
            getNodeImage: getNodeImage,
            onSelectNode: function (node, selectedByClick) {
                $scope.selectedNode = node;
                $scope.selectedNodeType = getObjectType(node);
                $scope.treeOptions.selectedNode = node;
                $scope.dataValueListOrderColumn = 'label';

                if (selectedByClick) {
                    $scope.rawDataSource = {};
                    $scope.rawDataSourceList = undefined;
                    if (RawData === $scope.selectedNodeType) {
                        $scope.rawDataSource = {
                            id: node.id,
                            district: node.district,
                            workspaceId: node.workspaceId,
                            workspaceName: getName(node, node.workspaceId),
                            projectId: node.projectId,
                            projectName: getName(node, node.projectId),
                            type: node.type,
                            description: node.description,
                            url: node.url,
                            method: node.method,
                            content: node.content,
                            context: node.context,
                            dataSchema: sortByKey(node.dataSchema)
                        };

                        if ("undefined" === typeof $scope.rawDataSource.context.useAsLookupData) {
                            $scope.rawDataSource.context.useAsLookupData = false;
                        }

                        if ("undefined" === typeof $scope.rawDataSource.context.returnType) {
                            $scope.rawDataSource.context.returnType = "Text";
                        }

                        try {
                            $scope.authParams.param = jsonToParamArray($scope.rawDataSource.context.authentication.content.body);
                        } catch (e) {
                            $scope.authParams.param = [];
                        }
                        try {
                            $scope.authParams.headers = jsonToParamArray($scope.rawDataSource.context.authentication.content.headers);
                        } catch (e) {
                            $scope.authParams.headers = [];
                        }
                        try {
                            $scope.mainParams.param = jsonToParamArray($scope.rawDataSource.content.body);
                        } catch (e) {
                            $scope.mainParams.param = [];
                        }
                        try {
                            $scope.mainParams.headers = jsonToParamArray($scope.rawDataSource.content.headers);
                        } catch (e) {
                            $scope.mainParams.headers = [];
                        }
                        try {
                            $scope.hiddenParams.param = jsonToParamArray($scope.rawDataSource.content.hiddenParameter);
                        } catch (e) {
                            $scope.hiddenParams.param = [];
                        }

                        try {
                            $scope.dataSchema = jsonToSchemaArray($scope.rawDataSource.dataSchema);
                        } catch (e) {
                            $scope.dataSchema = [];
                        }

                    } else {
                        $scope.rawDataSource = null;
                        getRawDataSourceList(1);
                    }
                }
            },
            onExpandNode: function (node) {
                $scope.expandedNode = node;
            }
        };

        function getRawDataSourceTree() {
            var param = {
                LIKE_permission_LIKE: ["%M%", "P"]
            }
            if ($scope.projectId) param.projectId = $scope.projectId;

            var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.rawData-GetRawDataSourceTree@.json');
            api.post({
                success: function (o) {
                    $scope.rawDataSources = o.data;
                    $scope.treeRoot = [{
                        name: "APIs",
                        nodeId: "rootNode",
                        image: "../image/A.png",
                        children: $scope.rawDataSources
                    }];

                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        $scope.refreshRawData = function () {
            getRawDataSourceTree();
        };

        function createRawDataSource(rawData) {
            startBlockUI();

            if (rawData.district === Application) {
                rawData.workspaceId = ZeroId;
                rawData.projectId = ZeroId;
            } else if (rawData.district === Workspace) {
                rawData.projectId = ZeroId;
            } else if (rawData.district === Project) {
                rawData.workspaceId = ZeroId;
            }
            var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.rawData-CreateRawDataSource@.json');
            api.post({
                success: function (o) {
                    rawData.id = o.data.rawData.id;
                    $scope.refreshRawData();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, rawData);
        }

        function updateRowDataSource(rawData) {
            startBlockUI();
            if (rawData.district === Application) {
                rawData.workspaceId = ZeroId;
                rawData.projectId = ZeroId;
            } else if (rawData.district === Workspace) {
                rawData.projectId = ZeroId;
            } else if (rawData.district === Project) {
                rawData.workspaceId = ZeroId;
            }
            var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.rawData-UpdateRawDataSource@.json');
            api.post({
                success: function (o) {
                    $scope.refreshRawData();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, rawData);
        }

        function deleteRowDataSource(rawData) {
            startBlockUI();
            var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.rawData-DeleteRawDataSource@.json');
            api.post({
                success: function (o) {
                    $scope.rawDataSource = null;
                    $scope.addMode = false;
                    $scope.refreshRawData();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, rawData);
        }

        function getRawDataDistrict() {
            var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.rawData-GetRawDataDistrict@.json');
            api.get({
                success: function (o) {
                    $scope.rawDataDistrict = o.data;
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            });
        }

        $scope.addRawDataSource = function () {
            $scope.addMode = true;
            $scope.dataSchema = [];
            $scope.rawDataSource = {
                content: {
                    type: "application/json"
                },
                context: {
                    useAsLookupData: false,
                    returnStructure: {
                        mapping: {}
                    },
                    returnType: "Text"
                }
            };

            $scope.authParams = {};
            $scope.mainParams = {};
            $scope.hiddenParams = {};

            var objectType = $scope.selectedNodeType;
            if (Workspace === objectType) {
                $scope.rawDataSource.district = Workspace;
                $scope.rawDataSource.workspaceId = $scope.selectedNode.id;
            } else if (Project === objectType) {
                $scope.rawDataSource.district = Project;
                $scope.rawDataSource.projectId = $scope.selectedNode.id;
            }
        };

        $scope.cancelAddRawDataSource = function () {
            $scope.addMode = false;
            $scope.rawDataSource = null;
        };

        (function () {
            getRawDataDistrict();
            getRawDataSourceTree();
        })();

        $scope.testRawDataSource = function (rawData) {
            if (rawData) {
                var returnType = "Text";
                if ("string" == typeof rawData.context) {
                    returnType = JSON.parse(rawData.context).returnType;
                } else if ("object" == typeof rawData.context) {
                    returnType = rawData.context.returnType;
                }

                var testForm = document.getElementById("testForm");
                testForm.elements["a"].value = returnType === 'Text' ?
                    marsContext.contextPath + '/services/ais/run/fbs.rawData-GetRawDataSource/' + rawData.id + ".json" :
                    marsContext.contextPath + '/services/ais/download/fbs.rawData-GetRawDataSource/' + rawData.id + "/file";
                testForm.elements["s"].value = '';
                testForm.elements["n"].value = '';
                testForm.submit();
            }
        };

        $scope.copyDataSourceUri = function (id) {
            var uri = $scope.rawDataSource.context.returnType === 'Text' ?
               'services/ais/run/fbs.rawData-GetRawDataSource/' + id + ".json" :
                'services/ais/download/fbs.rawData-GetRawDataSource/' + id + "/file";
            mars$util$.copyToClipboard(uri);
        }

        $scope.saveRawDataSource = function () {
            if ($scope.rawDataSource.id) {
                updateRowDataSource($scope.rawDataSource);
            } else {
                createRawDataSource($scope.rawDataSource);
            }
        };

        $scope.copyRawDataSource = function () {
            var scope = $scope;
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/integration/api-integration-copy.html'),
                size: 'full',
                controller: ['$scope',
                    function ($scope) {
                        $scope.mode = 'Add';
                        $scope.rawDataDistrict = scope.rawDataDistrict;
                        $scope.contextPath = marsContext.contextPath;
                        $scope.rawDataSource = angular.copy(scope.rawDataSource);
                        $scope.rawDataSource.id = undefined;

                        $scope.isReadyData = function () {
                            if (Workspace === $scope.rawDataSource.district) {
                                return $scope.rawDataSource.workspaceId && $scope.rawDataSource.workspaceId !== ZeroId;
                            } else if (Project === $scope.rawDataSource.district) {
                                return $scope.rawDataSource.projectId && $scope.rawDataSource.projectId !== ZeroId;
                            }
                        };
                        $scope.selectWorkspace = function (selectedData, rawDataSource) {
                            if (angular.isDefined(selectedData)) {
                                rawDataSource.workspaceId = selectedData.originalObject.id;
                                rawDataSource.workspaceName = selectedData.originalObject.name;
                                rawDataSource.projectId = ZeroId;
                                rawDataSource.projectName = undefined;
                            } else {
                                rawDataSource.workspaceId = ZeroId;
                                rawDataSource.workspaceName = undefined;
                            }
                        };
                        $scope.selectProject = function (selectedData, rawDataSource) {
                            if (angular.isDefined(selectedData)) {
                                rawDataSource.projectId = selectedData.originalObject.id;
                                rawDataSource.projectName = selectedData.originalObject.name;
                                rawDataSource.workspaceId = ZeroId;
                                rawDataSource.workspaceName = undefined;
                            } else {
                                rawDataSource.projectId = ZeroId;
                                rawDataSource.projectName = undefined;
                            }
                        };
                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };
                        $scope.ok = function () {
                            $scope.$close($scope.rawDataSource);
                        };
                    }
                ]
            }).result.then(function (rawDataSource) {
                createRawDataSource(rawDataSource);
            }, function () {
            });
        }

        $scope.validateRawDataSource = function () {
            var rawDataSource = $scope.rawDataSource;
            return rawDataSource.type && rawDataSource.description && rawDataSource.url && rawDataSource.method
                && rawDataSource.context && rawDataSource.context.returnStructure && rawDataSource.context.returnStructure.mapping
                && (!rawDataSource.context.useAsLookupData || (rawDataSource.context.returnStructure.mapping.value && rawDataSource.context.returnStructure.mapping.label));
        };

        $scope.deleteRawDataSource = function () {
            bootbox.confirm("Are you sure to delete the raw data " + $scope.rawDataSource.type + "?", function (result) {
                if (result) {
                    var param = {
                        id: $scope.rawDataSource.id
                    };
                    deleteRowDataSource(param);
                }
            });
        };

        $scope.changeContentType = function (source, target) {
            target.param = jsonToParamArray(source);
        };

        $scope.addHeader = function (target) {
            $scope.newHeader = {
                name: '',
                value: ''
            };
            target.push($scope.newHeader);
        };

        function saveHeaders(target, headers) {
            var _headers = {};
            for (var i = 0; i < headers.length; i++) {
                var header = headers[i];
                _headers[header.name] = header.value;
            }

            target.headers = _headers;
        }

        $scope.removeHeader = function (target, headers, index) {
            headers.splice(index, 1);
            saveHeaders(target, headers);
        };

        $scope.saveHeaders = function (target, headers) {
            saveHeaders(target, headers);
        };

        $scope.addParam = function (target) {
            $scope.newParam = {
                name: '',
                value: ''
            };
            target.push($scope.newParam);
        };

        function saveParam(target, params, hidden) {
            var _params = {};
            for (var i = 0; i < params.length; i++) {
                var param = params[i];
                _params[param.name] = param.value;
            }

            if (hidden) {
                target.hiddenParameter = _params;
            } else {
                target.body = _params;
            }
        }

        $scope.removeParam = function (target, params, index, hidden) {
            params.splice(index, 1);
            saveParam(target, params, hidden);
        };

        $scope.saveParam = function (target, params, hidden) {
            saveParam(target, params, hidden);
        };

        $scope.addSchema = function (target) {
            $scope.newItem = {
                name: '',
                type: ''
            };
            target.push($scope.newItem);
        };

        function saveSchema(target, items) {
            var _items = {};
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                _items[item.name] = item.type;
            }

            target.dataSchema = _items;
        }

        $scope.saveSchema = function (target, params) {
            saveSchema(target, params);
        };

        $scope.removeSchema = function (target, items, index) {
            items.splice(index, 1);
            saveSchema(target, items);
        };

        $scope.changeSchemaFormat = function (jsonSchema) {
            if (!jsonSchema) {
                try {
                    $scope.dataSchema = jsonToSchemaArray($scope.rawDataSource.dataSchema);
                } catch (e) {
                    $scope.dataSchema = [];
                }
            }
        };

        $scope.rawDataSourceListCurrentPage = 1;
        $scope.rawDataSourceListPageSize = 15;
        $scope.rawDataSourceListOrderColumn = 'type';
        $scope.rawDataSourceListOrderAsc = true;

        function callRawDataSourceListApi(param) {
            var apiName = null;
            if (Workspace === $scope.selectedNodeType) {
                apiName = 'fbs.rawData-GetWorkspaceRawDataSource.json';
                param.workspaceId = $scope.selectedNode.id;
            } else if (Project === $scope.selectedNodeType) {
                apiName = 'fbs.rawData-GetProjectRawDataSource.json';
                param.projectId = $scope.selectedNode.id;
            }

            if (null != apiName) {
                var api = new AjaxService(marsContext.contextPath + '/services/data/getList/' + apiName);
                api.post({
                    success: function (o) {
                        $scope.rawDataSourceList = o.data;
                        stopBlockUI();
                    },
                    error: function (o) {
                        stopBlockUI();
                        o.alertException();
                    }
                }, param);
            }
        }

        function getRawDataSourceList(pageNo, filter) {
            var param = {
                pageNo: pageNo || $scope.rawDataSourceListCurrentPage,
                pageSize: $scope.rawDataSourceListPageSize,
                ORDER_BY: $scope.rawDataSourceListOrderColumn + ' ' + ($scope.rawDataSourceListOrderAsc ? 'ASC' : 'DESC')
            };

            $scope.rawDataSourceListCurrentPage = param.pageNo;

            if (filter) {
                angular.extend(param, filter);
            }

            callRawDataSourceListApi(param);
        }

        $scope.sortRawDataList = function (column) {
            if ($scope.rawDataSourceListOrderColumn === column) {
                $scope.rawDataSourceListOrderAsc = !$scope.rawDataSourceListOrderAsc;
            } else $scope.fileListOrderAsc = true;
            $scope.rawDataSourceListOrderColumn = column;
            getRawDataSourceList($scope.rawDataSourceListCurrentPage);
        };

        $scope.changeDataSourceListPage = function () {
            getRawDataSourceList();
        };

        $scope.editRawDataSource = function (item) {
            try {
                $("#R" + item.id).find("span.node-title")[0].click();
            } catch (e) {
            }
        };


        function getHistory(data, callback) {
            $scope.selectedDataValue = data;
            $scope.histories = [];
            var param = {
                id: data.id
            };

            var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.rawData-GetRawDataSourceHistory.json');
            api.post({
                success: function (o) {
                    var history = [];
                    for (var i in o.data.data) {
                        history.push(o.data.data[i]);
                    }
                    $scope.histories = history;
                    if (callback) {
                        callback();
                    }
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        $scope.showHistory = function (data) {
            var callback = $scope.loadHistory;
            getHistory(data, callback);
        };

        $scope.loadHistory = function () {
            var scope = $scope;
            var value, orig1, orig2, dv, panes = 2, highlight = true, connect = "align", collapse = true;
            // var $b = parent.$B;
            var pageSize = 10;
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/integration/api-integration-history.html'),
                windowClass: 'app-modal-window-11',
                controller: ['$scope',
                    function ($scope) {
                        $scope.scope = scope;
                        $scope.contextPath = marsContext.contextPath;
                        $scope.title = "API Integration Versioning";
                        $scope.selectedNode = scope.selectedNode;
                        $scope.selectedNodeType = scope.selectedNodeType;
                        $scope.selectedDataValue = scope.selectedDataValue;
                        $scope.historiesLeft = scope.histories;
                        $scope.historiesRight = scope.histories;
                        $scope.selectedHistoryLeft = scope.histories[0];
                        $scope.selectedHistoryRight = scope.histories[0];
                        $scope.selectedNumLeft = 0;
                        $scope.selectedNumRight = -1;

                        function initUI() {
                        }

                        $scope.jsonViewer = function (name, jsonName, pathInfo) {
                            var leftData;
                            var rightData;

                            if ("contentJson" == jsonName || "contextJson" == jsonName) {
                                leftData = JSON.stringify($scope.getJsonValue(name, $scope.selectedHistoryLeft[jsonName]), null, 2);
                                rightData = JSON.stringify($scope.getJsonValue(name, $scope.selectedHistoryRight[jsonName]), null, 2);
                            } else if (name == jsonName) {
                                leftData = JSON.stringify($scope.selectedHistoryLeft[jsonName], null, 2);
                                rightData = JSON.stringify($scope.selectedHistoryRight[jsonName], null, 2);
                            }
                            if ('undefined' != typeof leftData || 'undefined' != typeof rightData) {
                                $scope.openJsonViewer(leftData, rightData, pathInfo);
                            }
                        }

                        $scope.openJsonViewer = function (leftData, rightData, pathInfo) {

                            var scope = $scope;
                            var value = leftData, orig1, orig2 = rightData, dv, panes = 2, highlight = true, connect = "align", collapse = true;
                            $uibModal.open({
                                backdrop: false,
                                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/integration/api-integration-json-viewer.html'),
                                windowClass: 'app-modal-window-11',
                                controller: ['$scope',
                                    function ($scope) {
                                        $scope.scope = scope;
                                        $scope.contextPath = marsContext.contextPath;
                                        $scope.title = "JSON Viewer";
                                        $scope.pathInfo = pathInfo;
                                        $scope.selectedNode = scope.selectedNode;
                                        $scope.selectedDataValue = scope.selectedDataValue;

                                        function initUI() {
                                            if (value == null) value = "";
                                            var target = document.getElementById("view");

                                            target.innerHTML = "";
                                            dv = CodeMirror.MergeView(target, {
                                                value: value,
                                                origLeft: null,
                                                orig: orig2,
                                                lineNumbers: true,
                                                mode: "application/ld+json",
                                                highlightDifferences: highlight,
                                                collapseIdentical: collapse,
                                                readOnly: true,
                                                rtlMoveVisually: false
                                            });
                                        }

                                        function toggleDifferences() {
                                            dv.setShowDifferences(highlight = !highlight);
                                        }

                                        function mergeViewHeight(mergeView) {
                                            function editorHeight(editor) {
                                                if (!editor) return 0;
                                                return editor.getScrollInfo().height;
                                            }

                                            return Math.max(editorHeight(mergeView.leftOriginal()),
                                                editorHeight(mergeView.editor()),
                                                editorHeight(mergeView.rightOriginal()));
                                        }

                                        function resize(mergeView) {
                                            var height = mergeViewHeight(mergeView);
                                            for (; ;) {
                                                if (mergeView.leftOriginal())
                                                    mergeView.leftOriginal().setSize(null, height);
                                                mergeView.editor().setSize(null, height);
                                                if (mergeView.rightOriginal())
                                                    mergeView.rightOriginal().setSize(null, height);

                                                var newHeight = mergeViewHeight(mergeView);
                                                if (newHeight >= height) break;
                                                else height = newHeight;
                                            }
                                            mergeView.wrap.style.height = height + "px";
                                        }

                                        $scope.cancel = function () {
                                            $scope.$dismiss();
                                        };

                                        (function () {
                                            $timeout(function () {
                                                initUI();
                                            }, 0);
                                        })();
                                    }
                                ]
                            }).result.then(function (viewData) {

                            }, function () {

                            });
                        };

                        function getHistoryContent(selectedData, selectedView, callback) {

                            if ("L" === selectedView) {
                                value = selectedData.value;
                                $scope.selectedHistoryLeft.dataSchemaJson = parseJSON(selectedData.dataSchema);
                                $scope.selectedHistoryLeft.contentJson = parseJSON(selectedData.content);
                                $scope.selectedHistoryLeft.contextJson = parseJSON(selectedData.context);
                            } else if ("R" === selectedView) {
                                orig2 = selectedData.value;
                                $scope.selectedHistoryRight.dataSchemaJson = parseJSON(selectedData.dataSchema);
                                $scope.selectedHistoryRight.contentJson = parseJSON(selectedData.content);
                                $scope.selectedHistoryRight.contextJson = parseJSON(selectedData.context);
                            }
                            if (callback) {
                                callback();
                            }
                        }

                        function parseJSON(data) {
                            if (null == data) {
                                return {};
                            } else {
                                return JSON.parse(data);
                            }
                        }

                        $scope.selectHistoryLeft = function (idx, selectedData) {
                            $scope.selectedNumLeft = idx;
                            $scope.selectedHistoryLeft = idx===-1 ? $scope.historiesLeft[0] : selectedData;
                            getHistoryContent($scope.selectedHistoryLeft, "L");
                        };
                        $scope.selectHistoryRight = function (idx, selectedData) {
                            $scope.selectedNumRight = idx;
                            $scope.selectedHistoryRight = idx===-1 ? $scope.historiesRight[0] : selectedData;
                            getHistoryContent($scope.selectedHistoryRight, "R");
                        };

                        $scope.loadMore = function (view) {
                            pageSize = 10000;
                            $scope.getHistoryList(view);
                        };

                        $scope.getHistoryList = function (view, callback) {
                            var param = {
                                id: $scope.selectedDataValue.id,
                                pageSize: pageSize
                            };

                            var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.rawData-GetRawDataSourceHistory.json');
                            api.post({
                                success: function (o) {
                                    var history = [];
                                    for (var i in o.data.data) {
                                        history.push(o.data.data[i]);
                                    }
                                    $scope.historiesLeft = history;
                                    $scope.historiesRight = history;

                                    if (callback) {
                                        if (callback === initHistoryView) {
                                            $(".list-group").scrollTop(0);
                                            $scope.selectedHistoryLeft = history[0];
                                            $scope.selectedHistoryRight = history[0];
                                            $scope.selectedNumLeft = 0;
                                            $scope.selectedNumRight = -1;
                                        }
                                        callback();
                                    }
                                },
                                error: function (o) {
                                    stopBlockUI();
                                    o.alertException();
                                }
                            }, param);
                        };

                        $scope.isObject = function (data) {
                            return typeof (data) == 'object' ? true : false;
                        };

                        $scope.isValueDiff = function (name, type) {
                            if (null == type) {
                                return $scope.selectedHistoryLeft[name] !== $scope.selectedHistoryRight[name];
                            } else {
                                if (name.indexOf(".") > -1) {
                                    var leftValue = $scope.getJsonValue(name, $scope.selectedHistoryLeft[type]);
                                    var rightValue = $scope.getJsonValue(name, $scope.selectedHistoryRight[type]);
                                    return $scope.checkEqual(leftValue, rightValue) ? false : true;
                                } else {
                                    return $scope.checkEqual($scope.selectedHistoryLeft[type][name], $scope.selectedHistoryRight[type][name]) ? false : true;
                                }
                            }
                        }

                        $scope.checkEqual = function (data1, data2) {
                            if ($scope.isObject(data1) && $scope.isObject(data2)) {
                                return JSON.stringify(data1) == JSON.stringify(data2);
                            } else {
                                return data1 == data2;
                            }
                        }

                        $scope.getJsonValue = function (name, jsonObj) {
                            if (null == name || null == jsonObj) {
                                return;
                            }

                            if (Array.isArray(name)) {
                                if (name.length == 1) {
                                    return jsonObj[name[0]];
                                } else {
                                    for (var prop in jsonObj) {
                                        if (prop == name[0]) {
                                            name.shift();
                                            return $scope.getJsonValue(name, jsonObj[prop]);
                                        }
                                    }
                                    return;
                                }
                            } else if (name.indexOf(".") > -1) {
                                return $scope.getJsonValue(name.split("."), jsonObj);
                            } else {
                                return jsonObj[name];
                            }
                        }

                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };

                        function resize() {
                            var historyContainer = document.getElementById("historyContainer");
                            var historyContentHeader = document.getElementById("historyContentHeader");
                            var historyContent = document.getElementById("historyContent");
                            if (historyContent) historyContent.style.height = (historyContainer.offsetHeight - historyContentHeader.offsetHeight) + "px";
                        }

                        $(window).on('resize', function () {
                            resize();
                        });

                        $scope.setHeight = function () {
                            setHeight();
                        };

                        function initHistoryView() {
                            var callback = getHistoryContent($scope.selectedHistoryLeft, "R", initUI);
                            getHistoryContent($scope.selectedHistoryRight, "L", callback);
                        }

                        (function () {
                            initHistoryView();
                            $timeout(function () {
                                resize();
                            }, 100);
                        })();

                    }
                ]
            }).result.then(function (aisData) {

            }, function () {

            });
        };

        $scope.setHeight = function () {
            setHeight();
        };

        $scope.useRetrieveAPIFormatChanged = function () {
            if ($scope.rawDataSource.context.useRetrieveAPIFormat) {
                $scope.rawDataSource.context.returnStructure.pagination = {
                    "data": "data",
                    "pageNo": "pageNo",
                    "pageSize": "pageSize",
                    "totalCount": "totalCount"
                }
            } else {
                $scope.rawDataSource.context.returnStructure.pagination = undefined;
            }
        }
    }]);

app.run(function (editableOptions) {
    editableOptions.theme = 'bs3';
});

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";

    var historyModal = document.getElementById("historyModal");
    if (historyModal) historyModal.style.height = (height * 0.9) + "px";
}
