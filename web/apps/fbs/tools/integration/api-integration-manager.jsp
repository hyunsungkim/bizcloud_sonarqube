<%@ page import="com.bizflow.io.services.ais.model.ApiIntegrationConstant" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AppDev API Integration Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/jsoneditor/dist/jsoneditor.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-xeditable/dist/css/xeditable.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/lib/codemirror.css">
    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/addon/merge/merge.css">

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../../includes/node_modules-ext/jsoneditor/dist/jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/ng-jsoneditor/ng-jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/angular-xeditable/dist/js/xeditable.min.js"></script>

    <script src="../../../../includes/node_modules/codemirror/lib/codemirror.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/css/css.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/javascript/javascript.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="../../../../includes/node_modules/diff-match-patch/javascript/diff_match_patch.js"></script>
    <script src="../../../../includes/node_modules/codemirror/addon/merge/merge.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");
        mars$require$.link("api-integration-manager.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("api-integration-manager.js");
    </script>
    <style>
        tr.list td {
            padding: 2px 5px 2px 5px;
        }
    </style>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center" style="padding: 10px 0 10px 0">
    <h1>AppDev API Integration Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row flex-nowrap" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div style="padding-bottom: 0;" id="leftPanelHeader">
                    <div class="bio-left-title">
                        <div class="title"><h5>APIs</h5></div>
                        <div class="i-btn">
                            <button class="bf-button p-component bf-button-icon-only" title="Add" ng-click="addRawDataSource();$event.stopPropagation();" ng-disabled="selectedNodeType !== Workspace && selectedNodeType !== Project"><i class="fa fa-plus"></i></button>
                            <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshRawData();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block" id="treeContainer">
                    <div class="form-group form-group-sm col-md-12">
                        <input type="text" ng-model="treeOptions.filter.type" class="searchinput" placeholder="Enter type to filter" style="margin-bottom: 10px;">
                        <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                    </div>
                    <div id="treePanelContainer" class="panelContainer">
                        <tree nodes='treeRoot' options="treeOptions"></tree>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary col-md-12" id="rightPanelContainer">
                <div id="rightPanelHeader">
                    <div class="bio-right-title">
                        <div class="title">
                            <h3 ng-if="rawDataSource">{{rawDataSource.type || 'New'}}
                                <span class="id-wrap" ng-if="rawDataSource.id">ID&nbsp;:&nbsp;{{rawDataSource.id}}</span>
                            </h3>
                            <h3 ng-if="!rawDataSource">{{selectedNode.name}}
                                <span class="id-wrap" ng-if="selectedNode.id && selectedNodeType !== RawData">ID&nbsp;:&nbsp;{{selectedNode.id}}</span>
                            </h3>
                        </div>
                        <div class="i-btn">
                            <div class="space"></div>
                            <button class="p-button p-component p-button-text-only" ng-if="addMode" ng-click="cancelAddRawDataSource();$event.stopPropagation();">Cancel</button>
                            <button class="p-button p-component p-button-text-only" ng-if="rawDataSource && rawDataSource.id" ng-click="testRawDataSource(rawDataSource);$event.stopPropagation();">Test</button>
                            <button class="p-button p-component p-button-text-only" ng-if="rawDataSource" ng-click="saveRawDataSource();$event.stopPropagation();" ng-disabled="!validateRawDataSource()">Save</button>
                            <button class="p-button p-component p-button-secondary p-button-text-only" ng-if="rawDataSource && rawDataSource.id" ng-click="copyDataSourceUri(rawDataSource.id);$event.stopPropagation();">Copy URI</button>
                            <button class="p-button p-component p-button-secondary p-button-text-only" ng-if="rawDataSource && rawDataSource.id" ng-click="copyRawDataSource();$event.stopPropagation();" ng-disabled="!validateRawDataSource()">Copy</button>
                            <button class="p-button p-component p-button-info p-button-text-only" ng-if="rawDataSource && rawDataSource.id" ng-click="showHistory(rawDataSource);$event.stopPropagation();">History</button>
                            <button class="p-button p-component p-button-danger p-button-text-only" ng-if="rawDataSource && rawDataSource.id" ng-click="deleteRawDataSource();$event.stopPropagation();">Delete</button>
                        </div>
                    </div>
                </div>
                <div class="card-block panelContainer">
                    <div ng-if="rawDataSource">
                        <div class="form-group form-group-sm col-md-12">
                            <label for="type">Type</label><span class="mandatory">*</span>
                            <input type="text" ng-model="rawDataSource.type" class="form-control" name="type" id="type" placeholder="Type" required autofocus>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <label for="description">Description</label><span class="mandatory">*</span>
                            <textarea ng-model="rawDataSource.description" class="form-control" rows="5" name="description" id="description" placeholder="Description" maxlength="2000">
                        </textarea>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <label>URL / Query / Method</label><span class="mandatory">*</span>
                            <input type="text" ng-model="rawDataSource.url" class="form-control" name="url" id="url" placeholder="Enter an URL or a Query Full Path" required>
                            <div class="text-<%=textColor%>" style="border:1px solid #666565;padding-left: 10px;">
                                <div class="card-heading" data-toggle="collapse" data-target="#urlHelp">
                                    <label class="font-h7 no-margin">Click here to see help</label>
                                </div>
                                <div id="urlHelp" class="collapse panel-body">
                                    <br>
                                    <i style="color: #c0c0c0;">URL: </i>
                                    <div style="padding-left: 15px;">
                                        <i style="color: #c0c0c0;">In case of using configuration keywords in AppDev Configuration Management<br>
                                            <div style="padding-left: 15px;">
                                                Format:
                                                <ul>
                                                    <li>[[CONFIGURATION_TYPE::CONFIGURATION_NAME]]</li>
                                                </ul>
                                                Where:
                                                <ul>
                                                    <li>CONFIGURATION_TYPE: The type of a configuration</li>
                                                    <li>CONFIGURATION_NAME: The value of a configuration</li>
                                                </ul>
                                                Example:
                                                <ul>
                                                    <li>[[Server::AppDev1]]/api/test.json</li>
                                                </ul>
                                            </div>
                                        </i>
                                        <i style="color: #c0c0c0;">Reserved Keywords:
                                            <ul>
                                                <li>{ContextPath}: the context path. e.g. /bizflowappdev</li>
                                                <li>{Server:Port}: the server name and port number. e.g. appdev.bizflow.com:8080</li>
                                                <li>{BaseURL}: the base URL that starts with http:// or https:// and the server and port. e.g. http://appdev.bizflow.com:8080</li>
                                            </ul>
                                        </i>
                                    </div>

                                    <i style="color: #c0c0c0;">Query: </i>
                                    <div style="padding-left: 15px;">
                                        <i style="color: #c0c0c0;">
                                            <div style="padding-left: 15px;">
                                                Format:
                                                <ul>
                                                    <li>APPLICATION_NAME.NAMESPACE-QUERY_ID</li>
                                                </ul>
                                                Where:
                                                <ul>
                                                    <li>APPLICATION_NAME: The name of an BIO application</li>
                                                    <li>NAMESPACE: the namespace that a query belongs to</li>
                                                    <li>QUERY_ID: the id of a query to be executed</li>
                                                </ul>
                                                Example:
                                                <ul>
                                                    <li>store.customer-GetCustomerLis@</li>
                                                </ul>
                                            </div>
                                        </i>
                                    </div>

                                    <i style="color: #c0c0c0;">Method: </i>
                                    <div style="padding-left: 15px;">
                                        <i style="color: #c0c0c0;">
                                            <div style="padding-left: 15px;">
                                                Format:
                                                <ul>
                                                    <li>CLASS_PATH_AND_NAME::METHOD_NAME(PARAM1,PARAM2,...)</li>
                                                </ul>
                                                Where:
                                                <ul>
                                                    <li>CLASS_PATH_AND_NAME: The full path of a class. e.g. com.bizflow.io.services.ais.invoke.util.ApiInvokerUtil</li>
                                                    <li>METHOD_NAME: the name of a method to be executed. e.g. invokeApi</li>
                                                    <li>
                                                        PARAMx: the parameters of a method
                                                        <ul>
                                                            <li>When passing a constant value to a method parameter as a parameter, wrap it in a single quotation. e.g. 'numString'</li>
                                                            <li>If you want to specify the type for a constant value, indicate the type value after ::. e.g. 'numString'::String, '123'::int</li>
                                                            <li>If you want to pass as a method parameter among the parameters passed while calling the currently registered API, use the parameter name without a single quota. e.g. numString</li>
                                                        </ul>
                                                        Available Types:
                                                        <ul>
                                                            <li>boolean</li>
                                                            <li>byte</li>
                                                            <li>character</li>
                                                            <li>integer</li>
                                                            <li>short</li>
                                                            <li>long</li>
                                                            <li>float</li>
                                                            <li>double</li>
                                                            <li>Byte</li>
                                                            <li>Boolean</li>
                                                            <li>Character</li>
                                                            <li>Integer</li>
                                                            <li>Short</li>
                                                            <li>Long</li>
                                                            <li>Float</li>
                                                            <li>Double</li>
                                                            <li>String</li>
                                                            <li>Map</li>
                                                            <li>List</li>
                                                            <li>Collection</li>
                                                            <li>JSONObject</li>
                                                            <li>JSONArray</li>
                                                            <li>Object[]</li>
                                                            <li>String[]</li>
                                                            <li>Class Full Path. e.g. java.util.ArrayList</li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                                Reserved Parameter Keywords:
                                                <ul>
                                                    <li>$PARAMETER$: the whole parameters passed while calling the currently registered API</li>
                                                    <li>$HTTP_SERVLET_REQUEST$: current HttpServletRequest object</li>
                                                </ul>

                                                Example:
                                                <ul>
                                                    <li>com.bizflow.io.core.util.NumberUtil::parseInt(numString::String,'987'::int) <br>
                                                        ==> the value of a parameter called "numbString" is passed to the method parseInt as the first parameter<br>
                                                        ==> 987 is converted to int and passed to the method parseInt as the second parameter.
                                                    </li>
                                                    <li>com.bizflow.io.services.ais.invoke.util.ApiInvokerUtil::invokeApi('fbs','rawData-GetRawDataSource','z716824db298444d2b6c1f912f6c3d2b6',$PARAMETER$,$HTTP_SERVLET_REQUEST$)</li>
                                                </ul>
                                            </div>
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <label>Method</label><span class="mandatory">*</span>
                            <select name="method" id="district" ng-model="rawDataSource.method" class="form-control" required>
                                <option value="QUERY">QUERY</option>
                                <option value="INVOKE">INVOKE</option>
                                <option value="POST">POST</option>
                                <option value="GET">GET</option>
                            </select>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <div class="form-group form-inline">
                                <label for="useAsLookupData">Use as Lookup Data</label>&nbsp;
                                <input id="useAsLookupData" type="checkbox" ng-model="rawDataSource.context.useAsLookupData" ng-checked="rawDataSource.context.useAsLookupData" class="form-control">
                            </div>
                            <div ng-if="rawDataSource.context.useAsLookupData" class="card text-<%=textColor%> bg-secondary" style="padding: 10px;">
                                <div class="form-group form-group-sm col-md-12">
                                    <label for="valueField">Lookup Value Field</label><span class="mandatory">*</span>
                                    <input type="text" ng-model="rawDataSource.context.returnStructure.mapping.value" class="form-control" name="valueField" id="valueField"
                                           placeholder="Enter a field name for value" ng-required="rawDataSource.context.useAsLookupData">
                                </div>
                                <div class="form-group form-group-sm col-md-12">
                                    <label for="labelField">Lookup Label Field</label><span class="mandatory">*</span>
                                    <input type="text" ng-model="rawDataSource.context.returnStructure.mapping.label" class="form-control" name="labelField" id="labelField"
                                           placeholder="Enter a field name for label " ng-required="rawDataSource.context.useAsLookupData">
                                </div>
                                <div class="form-group form-group-sm col-md-12">
                                    <label for="labelI18NField">Lookup Label for I18N Field</label>
                                    <input type="text" ng-model="rawDataSource.context.returnStructure.mapping.i18n" class="form-control" name="labelI18NField" id="labelI18NField"
                                           placeholder="Enter a field name for label for I18N" ng-required="rawDataSource.context.useAsLookupData">
                                </div>
                            </div>
                            <div ng-if="rawDataSource.context.useAsLookupData" class="text-<%=textColor%>" style="border:1px solid #666565;padding-left: 10px;">
                                <div class="card-heading" data-toggle="collapse" data-target="#lookupHelp">
                                    <label class="font-h7 no-margin">Click here to see help</label>
                                </div>
                                <div id="lookupHelp" class="collapse panel-body">
                                    <br>
                                    <i style="color: #c0c0c0;">
                                        To use as lookup data, check "Use as Lookup Data" and enter the lookup field from the data that returns the results<br>
                                        Registered lookup data field is automatically entered into the "mapping" properties of the Return Content Structure.<br><br>
                                    </i>
                                    <i style="color: #c0c0c0;">Lookup Value Field:
                                        <div style="padding-left: 15px;">
                                            Register a lookup value field mapped in the result data.
                                            <div style="padding-left: 15px;">
                                                Example:
                                                <ul>
                                                    <li>lookupValue ==> "mapping": {"value": "lookupValue"} in the Return Content Structure</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </i>
                                    <i style="color: #c0c0c0;">Lookup Label Field:
                                        <div style="padding-left: 15px;">
                                            Register a lookup label field mapped in the result data.
                                                <div style="padding-left: 15px;">
                                                    Example:
                                                    <ul>
                                                        <li>lookupLabel ==> "mapping": {"label": "lookupLabel"} in the Return Content Structure</li>
                                                    </ul>
                                                </div>
                                        </div>
                                    </i>
                                    <i style="color: #c0c0c0;">Lookup Label for I18N Field:
                                        <div style="padding-left: 15px;">
                                            Register a lookup label for I18N field mapped in the result data.
                                                <div style="padding-left: 15px;">
                                                    Example:
                                                    <ul>
                                                        <li>lookupI18N ==> "mapping": {"i18n": "lookupI18N"} in the Return Content Structure</li>
                                                    </ul>
                                                </div>
                                        </div>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="text-<%=textColor%>" style="padding: 5px;border:1px solid;">
                                <div class="card-heading" data-toggle="collapse" data-target="#authInfo" style="padding-bottom: 0">
                                    <h5 class="no-margin" title="click here to configure authentication">Authentication</h5>
                                </div>
                                <div id="authInfo" class="collapse panel-body" ng-class="{'show': rawDataSource.context.authentication.url}">
                                    <div class="form-group form-group-sm col-md-12" style="padding-top: 5px;">
                                        <label for="authUrl">URL</label>
                                        <input type="text" ng-model="rawDataSource.context.authentication.url" class="form-control" id="authUrl" placeholder="Enter authentication URL">
                                        <div class="text-<%=textColor%>" style="border:1px solid #666565;padding-left: 10px;">
                                            <div class="card-heading" data-toggle="collapse" data-target="#authUrlHelp">
                                                <label class="font-h7 no-margin">Click here to see help</label>
                                            </div>
                                            <div id="authUrlHelp" class="collapse panel-body">
                                                <br>
                                                <i style="color: #c0c0c0;">In case of using configuration keywords in AppDev Configuration Management<br>
                                                    <div style="padding-left: 15px;">
                                                        Format:
                                                        <ul>
                                                            <li>[[CONFIGURATION_TYPE::CONFIGURATION_NAME]]</li>
                                                        </ul>
                                                        Where:
                                                        <ul>
                                                            <li>CONFIGURATION_TYPE: The type of a configuration</li>
                                                            <li>CONFIGURATION_NAME: The value of a configuration</li>
                                                        </ul>
                                                        Example:
                                                        <ul>
                                                            <li>[[Server::AppDev1]]/api/test.json</li>
                                                        </ul>
                                                    </div>
                                                </i>
                                                <i style="color: #c0c0c0;">Reserved Keywords:
                                                    <ul>
                                                        <li>{ContextPath}: the context path. e.g. /bizflowappdev</li>
                                                        <li>{Server:Port}: the server name and port number. e.g. appdev.bizflow.com:8080</li>
                                                        <li>{BaseURL}: the base URL that starts with http:// or https:// and the server and port. e.g. http://appdev.bizflow.com:8080</li>
                                                    </ul>
                                                </i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm col-md-12">
                                        <label>Method</label>
                                        <select ng-model="rawDataSource.context.authentication.method" class="form-control">
                                            <option value=""></option>
                                            <option value="POST">POST</option>
                                            <option value="GET">GET</option>
                                        </select>
                                    </div>
                                    <div class="form-group form-group-sm col-md-12">
                                        <label>Request Content Type</label>
                                        <select ng-model="rawDataSource.context.authentication.content.type" class="form-control"
                                                ng-change="changeContentType(rawDataSource.context.authentication.content.body, authParams)">
                                            <option></option>
                                            <option value="application/x-www-form-urlencoded">FORM</option>
                                            <option value="application/json">JSON</option>
                                        </select>
                                    </div>
                                    <div class="form-group form-group-sm col-md-12">
                                        <label>Request Header</label>
                                        <div class="bf-table-2">
                                            <table class="table table-bordered table-condensed table-font-small table-sm">
                                                <tr style="font-weight: bold">
                                                    <th style="width:35%">Name</th>
                                                    <th style="width:35%">Value</th>
                                                    <th style="width:30%"></th>
                                                </tr>
                                                <tr ng-if="!authParams.headers || authParams.headers.length == 0">
                                                    <td colspan="3" class="text-center">None</td>
                                                </tr>
                                                <tr ng-repeat="header in authParams.headers">
                                                    <td>
                                                        <span editable-text="header.name" e-class="form-control-sm" e-name="name" e-form="authParamForm">{{header.name || '' }}</span>
                                                    </td>
                                                    <td>
                                                        <span editable-text="header.value" e-class="form-control-sm" e-name="value" e-form="authParamForm">{{header.value || '' }}</span>
                                                    </td>
                                                    <td class="right-btn" style="white-space: nowrap;">
                                                        <form editable-form name="authParamForm" onaftersave="saveHeaders(rawDataSource.context.authentication.content, authParams.headers)" ng-show="authParamForm.$visible" class="mb0" shown="newHeader == header">
                                                            <button type="submit" ng-disabled="authParamForm.$waiting" class="p-button p-component p-button-secondary p-button-text-only">Save</button>
                                                            <button type="button" ng-disabled="authParamForm.$waiting" ng-click="authParamForm.$cancel()" class="p-button p-component p-button-secondary p-button-text-only">Cancel</button>
                                                        </form>
                                                        <div class="buttons" ng-show="!authParamForm.$visible">
                                                            <button type="button" class="p-button p-component p-button-text-only" ng-click="authParamForm.$show()">Edit</button>
                                                            <button type="button" class="p-button p-component p-button-danger p-button-text-only" ng-click="removeHeader(rawDataSource.context.authentication.content, authParams.headers, $index)">Del</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br>
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="addHeader(authParams.headers)">Add Header</button>
                                    </div>
                                    <div class="form-group form-group-sm col-md-12" ng-if="rawDataSource.context.authentication.content.type === 'application/json'">
                                        <label>Request Content</label>
                                        <div ng-jsoneditor="options4AuthContentBody.setEditor" ng-model="rawDataSource.context.authentication.content.body" options="options4AuthContentBody" class="background-white max-width" style="height: 250px;"></div>
                                        <div class="text-<%=textColor%>" style="border:1px solid #666565;padding-left: 10px;">
                                            <div class="card-heading" data-toggle="collapse" data-target="#requestParameterHelpForAuth">
                                                <label class="font-h7 no-margin">Click here to see help</label>
                                            </div>
                                            <div id="requestParameterHelpForAuth" class="collapse panel-body">
                                                <br>
                                                <i style="color: #c0c0c0;">To map a parameter, put a parameter name between {( and )}<br>
                                                    <div style="padding-left: 15px;">
                                                        Format:
                                                        <ul>
                                                            <li>API_PARAMETER_NAME: {(PARAMETER_PATH_AND_NAME)}</li>
                                                            <li>API_PARAMETER_NAME: {(PARAMETER_PATH_AND_NAME{'default': DEFAULT_VALUE})}</li>
                                                        </ul>
                                                        Where:
                                                        <ul>
                                                            <li>API_PARAMETER_NAME: The name of a parameter of an API to be called</li>
                                                            <li>PARAMETER_PATH_AND_NAME: the name and path of a parameter</li>
                                                        </ul>
                                                        Example:
                                                        <ul>
                                                            <li>"searchQuery": "{(user.firstName)}"</li>
                                                            <li>"pageNo": "{(pageNo{'default': 1}})}"</li>
                                                            <li>"ORDER_BY": "{(orderBy{'default': 'name desc'}})}"</li>
                                                        </ul>
                                                    </div>
                                                </i>
                                                <i style="color: #c0c0c0;">To select a specific object, use a selector<br>
                                                    <div style="padding-left: 15px;">
                                                        Format: OBJECT_NAME[@PROPERTY_NAME==PROPERTY_VALUE]<br>
                                                        Where:
                                                        <ul>
                                                            <li>OBJECT_NAME: The name of an object</li>
                                                            <li>PROPERTY_NAME: The name of a property of an object</li>
                                                            <li>PROPERTY_VALUE: The value of a property of an object</li>
                                                        </ul>
                                                        Example:
                                                        <ul>
                                                            <li>{(filters[@field=='lastName'].value)}: It returns the value property of an object that is {"field": "lastName", "value": value}</li>
                                                            <li>{(filters[@field=='lastName' && @matchMode=='startsWith'].value)}: It returns the value property of an object that is {"field": "lastName", "matchMode": "startsWith", "value": value}</li>
                                                        </ul>
                                                    </div>
                                                </i>
                                                <i style="color: #c0c0c0;">To covert an object to other type, use a helper function<br>
                                                    <div style="padding-left: 15px;">
                                                        Format:
                                                        <ul>
                                                            <li>OBJECT_NAME&lt;HELPER_FUNCTION></li>
                                                            <li>OBJECT_NAME_PATH::HELPER_FUNCTION></li>
                                                        </ul>
                                                        Where:
                                                        <ul>
                                                            <li>OBJECT_NAME: The name of an object</li>
                                                            <li>HELPER_FUNCTION: a helper function with arguments</li>
                                                            <li>OBJECT_NAME_PATH: The name and path of an object. e.g. query.option<#toMap('type')>.sorts</li>
                                                        </ul>
                                                        Helper Functions:
                                                        <ul>
                                                            <li>#join({(propertyName1)} {(propertyName2)} ... {(propertyName3)}, delimiter): Join arrays with properties and delimiter.</li>
                                                            <li>#split(REGULAR_EXPRESSION): Split a String object by the REGULAR_EXPRESSION</li>
                                                            <li>#toJSONObject(): Coverts an object to JSONObject</li>
                                                            <li>#toJSONArray(): Coverts an object to JSONArray</li>
                                                            <li>#toJSONString(): Coverts an object to JSON String</li>
                                                            <li>#toJSONString('DEFAULT_STRING]): Coverts an object to JSON String and if object is null, return the DEFAULT_STRING</li>
                                                            <li>#toString(): Coverts an object to a string</li>
                                                            <li>#toString('DEFAULT_STRING]): Coverts an object to a string and if object is null, return the DEFAULT_STRING</li>
                                                            <li>#toMap(): Coverts an object to a Map</li>
                                                            <li>#toMap('MAP_KEY'): Coverts an array to a Map by a MAP_KEY that a property name of each element in the array</li>
                                                            <li>#toList(): Coverts an object to a List</li>
                                                            <li>#toGMTDateString(originalFormat,outputFormat): Convert the format of a date string</li>
                                                            <li>#multiply(number,type): Multiply an object by a number. type is one of short, int, long, float, doubl</li>
                                                            <li>#divide(number,type): Divide an object by a number. type is one of short, int, long, float, double</li>
                                                            <li>#replaceAll(regex,replacement): Replace all occurrences of a substring (matching argument regex) with replacement string</li>
                                                            <li>#convertDateFormat(originalFormat,outputFormat): Convert the format of a date string</li>
                                                            <li>#getJSONObject(index): Get JSON object of an JSON array</li>
                                                            <li>#getJSONArrayObject(index): Get object of an JSON array</li>
                                                            <li>#getListObject(index): Get object of a list</li>
                                                            <li>#&#123;CLASS_PATH_AND_NAME::METHOD_NAME(PARAM1,PARAM2,...)}: Run an expression function. See the Method help. Use $VALUE$ as the name of a parameter for an object.</li>
                                                        </ul>
                                                        Example:
                                                        <ul>
                                                            <li>"ORDER_BY": "{(sorts::#join({(field)} {(order)},'\\,'))}"
                                                                ==> Join sorts object with field and order properties and , delimiter so the result seems like "firstName desc,lastName desc"
                                                            </li>
                                                            <li>"LIKE_lastName_LIKE": "{(filters<#toMap('field')>.lastName[@matchMode=='contains'].value)}"
                                                                ==> This converts filters object to a Map by using field property as the key of the map
                                                            </li>
                                                            <li>"testDate2": "{(filters[@field=='birthDate' && @matchMode=='between'].value[1]::#convertDateFormat('yyyy-MM-dd'T'HH:mm:ss.S'Z'','yyyy/MM/dd HH:mm:ss'))}"</li>
                                                            <li>"testDate3": "{(filters[@field=='birthDate' && @matchMode=='between'].value[1]::#toGMTDateString('yyyy-MM-dd'T'HH:mm:ss.S'Z'','yyyy/MM/dd HH:mm:ss'))}"</li>
                                                            <li>"testDate1": "{(filters[@field=='birthDate' && @matchMode=='between'].value[1]::#&#123;com.bizflow.io.core.util.DateUtil::convertDateFormat($VALUE$,'yyyy-MM-dd'T'HH:mm:ss.S'Z'','yyyy/MM/dd HH:mm:ss')})}"</li>
                                                            <li>"firstName": "{(content<#getJSONObject(0)>.firstName)}"</li>
                                                            <li>"content": "{(content<#getJSONObject(0)>)}"</li>
                                                        </ul>
                                                    </div>
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm col-md-12" ng-if="rawDataSource.context.authentication.content.type === 'application/x-www-form-urlencoded'">
                                        <label>Request Parameter</label>
                                        <div class="bf-table-2">
                                            <table class="table table-bordered table-hover table-condensed table-font-small table-sm">
                                                <tr style="font-weight: bold">
                                                    <th style="width:35%">Name</th>
                                                    <th style="width:35%">Value</th>
                                                    <th style="width:30%"></th>
                                                </tr>
                                                <tr ng-if="!authParams.param || authParams.param.length == 0">
                                                    <td colspan="3" class="text-center">None</td>
                                                </tr>
                                                <tr ng-repeat="param in authParams.param">
                                                    <td>
                                                        <span editable-text="param.name" e-class="form-control-sm" e-name="name" e-form="authParamForm">{{param.name || '' }}</span>
                                                    </td>
                                                    <td>
                                                        <span editable-text="param.value" e-class="form-control-sm" e-name="value" e-form="authParamForm">{{param.value || '' }}</span>
                                                    </td>
                                                    <td class="right-btn" style="white-space: nowrap">
                                                        <form editable-form name="authParamForm" onaftersave="saveParam(rawDataSource.context.authentication.content, authParams.param)" ng-show="authParamForm.$visible" class="mb0" shown="newParam == param">
                                                            <button type="submit" ng-disabled="authParamForm.$waiting" class="p-button p-component p-button-secondary p-button-text-only">Save</button>
                                                            <button type="button" ng-disabled="authParamForm.$waiting" ng-click="authParamForm.$cancel()" class="p-button p-component p-button-secondary p-button-text-only">Cancel</button>
                                                        </form>
                                                        <div class="buttons" ng-show="!authParamForm.$visible">
                                                            <button type="button" class="p-button p-component p-button-text-only" ng-click="authParamForm.$show()">Edit</button>
                                                            <button type="button" class="p-button p-component p-button-danger p-button-text-only" ng-click="removeParam(rawDataSource.context.authentication.content, authParams.param, $index)">Del</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br>
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="addParam(authParams.param)">Add Parameter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-md-12" style="padding-top: 10px;">
                            <label>Request Content Type</label>
                            <select ng-model="rawDataSource.content.type" class="form-control" ng-change="changeContentType(rawDataSource.content.body, mainParams)">
                                <option value="application/x-www-form-urlencoded">FORM</option>
                                <option value="application/json">JSON</option>
                            </select>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <label>Request Header</label>
                            <div class="bf-table-2">
                                <table class="table table-striped table-condensed text-white table-font-small table-sm">
                                    <tr style="font-weight: bold">
                                        <th style="width:35%">Name</th>
                                        <th style="width:35%">Value</th>
                                        <th style="width:30%"></th>
                                    </tr>
                                    <tr ng-if="!mainParams.headers || mainParams.headers.length == 0">
                                        <td colspan="3" class="text-center">None</td>
                                    </tr>
                                    <tr ng-repeat="header in mainParams.headers">
                                        <td>
                                            <span editable-text="header.name" e-class="form-control-sm" e-name="name" e-form="mainParamForm">{{header.name || '' }}</span>
                                        </td>
                                        <td>
                                            <span editable-text="header.value" e-class="form-control-sm" e-name="value" e-form="mainParamForm">{{header.value || '' }}</span>
                                        </td>
                                        <td class="right-btn" style="white-space: nowrap">
                                            <form editable-form name="mainParamForm" onaftersave="saveHeaders(rawDataSource.content, mainParams.headers)" ng-show="mainParamForm.$visible" class="mb0" shown="newHeader == header">
                                                <button type="submit" ng-disabled="mainParamForm.$waiting" class="p-button p-component p-button-secondary p-button-text-only">Save</button>
                                                <button type="button" ng-disabled="mainParamForm.$waiting" ng-click="mainParamForm.$cancel()" class="p-button p-component p-button-secondary p-button-text-only">Cancel</button>
                                            </form>
                                            <div class="buttons" ng-show="!mainParamForm.$visible">
                                                <button type="button" class="p-button p-component p-button-text-only" ng-click="mainParamForm.$show()">Edit</button>
                                                <button type="button" class="p-button p-component p-button-danger p-button-text-only" ng-click="removeHeader(rawDataSource.content, mainParams.headers, $index)">Del</button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br>
                            <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="addHeader(mainParams.headers)">Add Header</button>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <div class="text-<%=textColor%>" style="padding: 5px;border:1px solid;">
                                <div class="card-heading" data-toggle="collapse" data-target="#inPipeProcessor" style="padding-bottom: 0">
                                    <h5 class="no-margin" title="click here to configure authentication">In Hook Processor</h5>
                                </div>
                                <div id="inPipeProcessor" class="collapse panel-body" ng-class="{'show': rawDataSource.context.inPipeProcessor.classPath}">
                                    <div class="form-group form-group-sm col-md-12" style="padding-top: 5px;">
                                        <label for="authUrl">Processor Class</label>
                                        <input type="text" ng-model="rawDataSource.context.inPipeProcessor.classPath" class="form-control" id="inPipeProcessorClass" placeholder="Enter the full class path">
                                        <i>A processor class must be inherited from com.bizflow.io.services.data.processor.DataProcessor. For example, com.bizflow.io.services.form.processor.ExternalDataToGridDataProcessor</i>
                                    </div>
                                    <div class="form-group form-group-sm col-md-12" style="padding-top: 5px;">
                                        <label for="authUrl">Options</label>
                                        <div ng-jsoneditor="options4InPipeProcessorOption.setEditor" id="inPipeProcessorOption" ng-model="rawDataSource.context.inPipeProcessor.option" options="options4InPipeProcessorOption" class="background-white max-width" style="height: 250px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-md-12" ng-if="rawDataSource.content.type === 'application/json'">
                            <label for="requestContent">Request Content</label>
                            (Use Original Request Content <input type="checkbox" ng-model="rawDataSource.content.useOriginalContent" ng-checked="rawDataSource.content.useOriginalContent">)
                            <div ng-jsoneditor="options4ContentBody.setEditor" id="requestContent" ng-model="rawDataSource.content.body" options="options4ContentBody" ng-show="!rawDataSource.content.useOriginalContent" class="background-white max-width" style="height: 250px;"></div>
                            <div class="text-<%=textColor%>" style="border:1px solid #666565;padding-left: 10px;" ng-show="!rawDataSource.content.useOriginalContent">
                                <div class="card-heading" data-toggle="collapse" data-target="#requestParameterHelp">
                                    <label class="font-h7 no-margin">Click here to see help</label>
                                </div>
                                <div id="requestParameterHelp" class="collapse panel-body">
                                    <br>
                                    <i style="color: #c0c0c0;">To map a parameter, put a parameter name between {( and )}<br>
                                        <div style="padding-left: 15px;">
                                            Format:
                                            <ul>
                                                <li>API_PARAMETER_NAME: {(PARAMETER_PATH_AND_NAME)}</li>
                                                <li>API_PARAMETER_NAME: {(PARAMETER_PATH_AND_NAME{'default': DEFAULT_VALUE})}</li>
                                            </ul>
                                            Where:
                                            <ul>
                                                <li>API_PARAMETER_NAME: The name of a parameter of an API to be called</li>
                                                <li>PARAMETER_PATH_AND_NAME: the name and path of a parameter</li>
                                            </ul>
                                            Example:
                                            <ul>
                                                <li>"searchQuery": "{(user.firstName)}"</li>
                                                <li>"pageNo": "{(pageNo{'default': 1}})}"</li>
                                                <li>"ORDER_BY": "{(orderBy{'default': 'name desc'}})}"</li>
                                            </ul>
                                        </div>
                                    </i>
                                    <i style="color: #c0c0c0;">To select a specific object, use a selector<br>
                                        <div style="padding-left: 15px;">
                                            Format: OBJECT_NAME[@PROPERTY_NAME==PROPERTY_VALUE]<br>
                                            Where:
                                            <ul>
                                                <li>OBJECT_NAME: The name of an object</li>
                                                <li>PROPERTY_NAME: The name of a property of an object</li>
                                                <li>PROPERTY_VALUE: The value of a property of an object</li>
                                            </ul>
                                            Example:
                                            <ul>
                                                <li>{(filters[@field=='lastName'].value)}: It returns the value property of an object that is {"field": "lastName", "value": value}</li>
                                                <li>{(filters[@field=='lastName' && @matchMode=='startsWith'].value)}: It returns the value property of an object that is {"field": "lastName", "matchMode": "startsWith", "value": value}</li>
                                            </ul>
                                        </div>
                                    </i>
                                    <i style="color: #c0c0c0;">To covert an object to other type, use a helper function<br>
                                        <div style="padding-left: 15px;">
                                            Format:
                                            <ul>
                                                <li>OBJECT_NAME&lt;HELPER_FUNCTION></li>
                                                <li>OBJECT_NAME_PATH::HELPER_FUNCTION></li>
                                            </ul>
                                            Where:
                                            <ul>
                                                <li>OBJECT_NAME: The name of an object</li>
                                                <li>HELPER_FUNCTION: a helper function with arguments</li>
                                                <li>OBJECT_NAME_PATH: The name and path of an object. e.g. query.option<#toMap('type')>.sorts</li>
                                            </ul>
                                            Helper Functions:
                                            <ul>
                                                <li>#join({(propertyName1)} {(propertyName2)} ... {(propertyName3)}, delimiter): Join arrays with properties and delimiter.</li>
                                                <li>#split(REGULAR_EXPRESSION): Split a String object by the REGULAR_EXPRESSION</li>
                                                <li>#toJSONObject(): Coverts an object to JSONObject</li>
                                                <li>#toJSONArray(): Coverts an object to JSONArray</li>
                                                <li>#toJSONString(): Coverts an object to JSON String</li>
                                                <li>#toJSONString('DEFAULT_STRING]): Coverts an object to JSON String and if object is null, return the DEFAULT_STRING</li>
                                                <li>#toString(): Coverts an object to a string</li>
                                                <li>#toString('DEFAULT_STRING]): Coverts an object to a string and if object is null, return the DEFAULT_STRING</li>
                                                <li>#toMap(): Coverts an object to a Map</li>
                                                <li>#toMap('MAP_KEY'): Coverts an array to a Map by a MAP_KEY that a property name of each element in the array</li>
                                                <li>#toList(): Coverts an object to a List</li>
                                                <li>#toGMTDateString(originalFormat,outputFormat): Convert the format of a date string</li>
                                                <li>#multiply(number,type): Multiply an object by a number. type is one of short, int, long, float, doubl</li>
                                                <li>#divide(number,type): Divide an object by a number. type is one of short, int, long, float, double</li>
                                                <li>#replaceAll(regex,replacement): Replace all occurrences of a substring (matching argument regex) with replacement string</li>
                                                <li>#convertDateFormat(originalFormat,outputFormat): Convert the format of a date string</li>
                                                <li>#getJSONObject(index): Get JSON object of an JSON array</li>
                                                <li>#getJSONArrayObject(index): Get object of an JSON array</li>
                                                <li>#getListObject(index): Get object of a list</li>
                                                <li>#&#123;CLASS_PATH_AND_NAME::METHOD_NAME(PARAM1,PARAM2,...)}: Run an expression function. See the Method help. Use $VALUE$ as the name of a parameter for an object.</li>
                                            </ul>
                                            Example:
                                            <ul>
                                                <li>"ORDER_BY": "{(sorts::#join({(field)} {(order)},'\\,'))}"
                                                    ==> Join sorts object with field and order properties and , delimiter so the result seems like "firstName desc,lastName desc"
                                                </li>
                                                <li>"LIKE_lastName_LIKE": "{(filters<#toMap('field')>.lastName[@matchMode=='contains'].value)}"
                                                    ==> This converts filters object to a Map by using field property as the key of the map
                                                </li>
                                                <li>"testDate2": "{(filters[@field=='birthDate' && @matchMode=='between'].value[1]::#convertDateFormat('yyyy-MM-dd'T'HH:mm:ss.S'Z'','yyyy/MM/dd HH:mm:ss'))}"</li>
                                                <li>"testDate3": "{(filters[@field=='birthDate' && @matchMode=='between'].value[1]::#toGMTDateString('yyyy-MM-dd'T'HH:mm:ss.S'Z'','yyyy/MM/dd HH:mm:ss'))}"</li>
                                                <li>"testDate1": "{(filters[@field=='birthDate' && @matchMode=='between'].value[1]::#&#123;com.bizflow.io.core.util.DateUtil::convertDateFormat($VALUE$,'yyyy-MM-dd'T'HH:mm:ss.S'Z'','yyyy/MM/dd HH:mm:ss')})}"</li>
                                                <li>"firstName": "{(content<#getJSONObject(0)>.firstName)}"</li>
                                                <li>"content": "{(content<#getJSONObject(0)>)}"</li>
                                            </ul>
                                        </div>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-md-12" ng-if="rawDataSource.content.type === 'application/x-www-form-urlencoded'">
                            <label>Request Parameter</label>
                            <div class="bf-table-2">
                                <table class="table table-striped table-condensed text-white table-font-small table-sm">
                                    <thead>
                                    <tr style="font-weight: bold">
                                        <th style="width:35%">Name</th>
                                        <th style="width:35%">Value</th>
                                        <th style="width:30%"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-if="!mainParams.param || mainParams.param.length == 0">
                                        <td colspan="3" class="text-center">None</td>
                                    </tr>
                                    <tr ng-repeat="param in mainParams.param">
                                        <td>
                                            <span editable-text="param.name" e-class="form-control-sm" e-name="name" e-form="mainParamForm">{{param.name || '' }}</span>
                                        </td>
                                        <td>
                                            <span editable-text="param.value" e-class="form-control-sm" e-name="value" e-form="mainParamForm">{{param.value || '' }}</span>
                                        </td>
                                        <td class="right-btn" style="white-space: nowrap">
                                            <form editable-form name="mainParamForm" onaftersave="saveParam(rawDataSource.content, mainParams.param)" ng-show="mainParamForm.$visible" class="mb0" shown="newParam == param">
                                                <button type="submit" ng-disabled="mainParamForm.$waiting" class="p-button p-component p-button-secondary p-button-text-only">Save</button>
                                                <button type="button" ng-disabled="mainParamForm.$waiting" ng-click="mainParamForm.$cancel()" class="p-button p-component p-button-secondary p-button-text-only">Cancel</button>
                                            </form>
                                            <div class="buttons" ng-show="!mainParamForm.$visible">
                                                <button type="button" class="p-button p-component p-button-text-only" ng-click="mainParamForm.$show()">Edit</button>
                                                <button type="button" class="p-button p-component p-button-danger p-button-text-only" ng-click="removeParam(rawDataSource.content, mainParams.param, $index)">Del</button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="addParam(mainParams.param)">Add Parameter</button>
                        </div>
                        <div class="form-group form-group-sm col-md-12" ng-if="rawDataSource.content.type === 'application/json'">
                            <label>Hidden Parameter and Options</label>
                            <div ng-jsoneditor="options4HiddenParameter.setEditor" ng-model="rawDataSource.content.hiddenParameter" options="options4HiddenParameter" class="background-white max-width" style="height: 250px;"></div>
                        </div>
                        <div class="form-group form-group-sm col-md-12" ng-if="rawDataSource.content.type === 'application/x-www-form-urlencoded'">
                            <label>Hidden Parameter and Options</label>
                            <div class="bf-table-2">
                                <table class="table table-striped table-condensed text-white table-font-small table-sm">
                                    <thead>
                                    <tr style="font-weight: bold">
                                        <th style="width:35%">Name</th>
                                        <th style="width:35%">Value</th>
                                        <th style="width:30%"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-if="!hiddenParams.param || hiddenParams.param.length == 0">
                                        <td colspan="3" class="text-center">None</td>
                                    </tr>
                                    <tr ng-repeat="param in hiddenParams.param">
                                        <td>
                                            <span editable-text="param.name" e-class="form-control-sm" e-name="name" e-form="hiddenParamForm">{{param.name || '' }}</span>
                                        </td>
                                        <td>
                                            <span editable-text="param.value" e-class="form-control-sm" e-name="value" e-form="hiddenParamForm">{{param.value || '' }}</span>
                                        </td>
                                        <td class="right-btn" style="white-space: nowrap">
                                            <form editable-form name="hiddenParamForm" onaftersave="saveParam(rawDataSource.content, hiddenParams.param, true)" ng-show="hiddenParamForm.$visible" class="mb0" shown="newParam == param">
                                                <button type="submit" ng-disabled="hiddenParamForm.$waiting" class="p-button p-component p-button-secondary p-button-text-only">Save</button>
                                                <button type="button" ng-disabled="hiddenParamForm.$waiting" ng-click="hiddenParamForm.$cancel()" class="p-button p-component p-button-secondary p-button-text-only">Cancel</button>
                                            </form>
                                            <div class="buttons" ng-show="!hiddenParamForm.$visible">
                                                <button type="button" class="p-button p-component p-button-text-only" ng-click="hiddenParamForm.$show()">Edit</button>
                                                <button type="button" class="p-button p-component p-button-danger p-button-text-only" ng-click="removeParam(rawDataSource.content, hiddenParams.param, $index, true)">Del</button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="addParam(hiddenParams.param)">Add Hidden Parameter</button>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <label for="returnType">Return Type</label>
                            <select name="returnType" id="returnType" ng-model="rawDataSource.context.returnType" class="form-control" required>
                                <option value="<%=ApiIntegrationConstant.ReturnTypeText%>">JSON, XML or Text</option>
                                <option value="<%=ApiIntegrationConstant.ReturnTypeStream%>">Stream</option>
                                <option value="<%=ApiIntegrationConstant.ReturnTypeFile%>">File</option>
                                <option value="<%=ApiIntegrationConstant.ReturnTypeTransientFile%>">Transient File</option>
                            </select>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <div class="text-<%=textColor%>" style="padding: 5px;border:1px solid;">
                                <div class="card-heading" data-toggle="collapse" data-target="#outPipeProcessor" style="padding-bottom: 0">
                                    <h5 class="no-margin" title="click here to configure authentication">Out Hook Processor</h5>
                                </div>
                                <div id="outPipeProcessor" class="collapse panel-body" ng-class="{'show': rawDataSource.context.outPipeProcessor.classPath}">
                                    <div class="form-group form-group-sm col-md-12" style="padding-top: 5px;">
                                        <label for="authUrl">Processor Class</label>
                                        <input type="text" ng-model="rawDataSource.context.outPipeProcessor.classPath" class="form-control" id="outPipeProcessorClass" placeholder="Enter the full class path">
                                        <i>A processor class must be inherited from com.bizflow.io.services.data.processor.DataProcessor. For example, com.bizflow.io.services.form.processor.ExternalDataToGridDataProcessor</i>
                                    </div>
                                    <div class="form-group form-group-sm col-md-12" style="padding-top: 5px;">
                                        <label for="authUrl">Options</label>
                                        <div ng-jsoneditor="options4OutPipeProcessorOption.setEditor" id="outPipeProcessorOption" ng-model="rawDataSource.context.outPipeProcessor.option" options="options4OutPipeProcessorOption" class="background-white max-width" style="height: 250px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <label for="returnStructure">Return Content Structure</label>
                            (Use Retrieve API / Pagination Format <input type="checkbox" ng-model="rawDataSource.context.useRetrieveAPIFormat" ng-checked="rawDataSource.context.useRetrieveAPIFormat" ng-change="useRetrieveAPIFormatChanged()">)
                            <div ng-jsoneditor="options4ReturnStructure.setEditor" id="returnStructure" ng-model="rawDataSource.context.returnStructure" options="options4ReturnStructure" class="background-white max-width" style="height: 250px;"></div>
                        </div>
                        <div class="form-group form-group-sm col-md-12">
                            <label class="font-h5">Data Schema</label> (JSON Format <input type="checkbox" ng-model="jsonSchema" ng-change="changeSchemaFormat(jsonSchema)">)
                            <div ng-if="!jsonSchema" style="border: 1px solid;">
                                <div>
                                    <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%>">
                                        <thead>
                                        <tr style="font-weight: bold">
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-if="!dataSchema || dataSchema.length == 0">
                                            <td colspan="3" class="text-center">None</td>
                                        </tr>
                                        <tr ng-repeat="schema in dataSchema" class="list">
                                            <td>
                                                <span editable-text="schema.name" e-class="form-control-sm" e-name="name" e-form="dataSchemaForm">{{schema.name || '' }}</span>
                                            </td>
                                            <td>
                                                <span editable-select="schema.type" e-class="form-control-sm" e-name="type" e-form="dataSchemaForm" e-ng-options="item for item in schemaTypes">{{schema.type || '' }}</span>
                                            </td>
                                            <td class="text-right" style="white-space: nowrap">
                                                <form editable-form name="dataSchemaForm" onaftersave="saveSchema(rawDataSource, dataSchema)" ng-show="dataSchemaForm.$visible" class="mb0" shown="newItem== schema">
                                                    <button type="submit" class="p-button p-component p-button-secondary" ng-disabled="dataSchemaForm.$waiting">Save</button>
                                                    <button type="button" class="p-button p-component p-button-secondary" ng-disabled="dataSchemaForm.$waiting" ng-click="dataSchemaForm.$cancel()">Cancel</button>
                                                </form>
                                                <div class="buttons" ng-show="!dataSchemaForm.$visible">
                                                    <button type="button" class="p-button p-component p-button-secondary" ng-click="dataSchemaForm.$show()">Edit</button>
                                                    <button type="button" class="p-button p-component p-button-danger" ng-click="removeSchema(rawDataSource, dataSchema, $index)">Del</button>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center" style="border-top: 1px solid;padding: 5px;">
                                    <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="addSchema(dataSchema)">Add Field</button>
                                </div>
                            </div>
                            <div ng-if="jsonSchema">
                                <div ng-jsoneditor="options4DataSchema.setEditor" ng-model="rawDataSource.dataSchema" options="options4DataSchema" class="background-white max-width" style="height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="DataValue" ng-if="rawDataSourceList && !addMode">
                    <div ng-if="rawDataSourceList.data.length == 0">
                        <div class="text-center">No Available Raw Data Sources</div>
                    </div>
                    <div class="bf-table-2" ng-if="rawDataSourceList.data.length > 0">
                        <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%>">
                            <thead>
                            <tr style="font-weight: bold">
                                <th>#</th>
                                <th ng-click="sortRawDataList('type')" ng-if="selectedNodeType !== DataType"><a>Type</a></th>
                                <th ng-click="sortRawDataList('district')" ng-if="selectedNodeType !== DataType"><a>Level</a></th>
                                <th ng-click="sortRawDataList('url')"><a>URL</a></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="item in rawDataSourceList.data" title="ID={{item.id}}" class="list">
                                <td>{{item.ROW_NUMBER}}</td>
                                <td>{{item.type}}</td>
                                <td>{{item.district}}</td>
                                <td>{{item.url}}</td>
                                <td class="text-nowrap right-btn">
                                    <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="testRawDataSource(item);$event.stopPropagation();">Test</button>
                                    <button type="button" class="p-button p-component p-button-text-only" ng-click="editRawDataSource(item);$event.stopPropagation();">Edit</button>
                                    <button type="button" class="p-button p-component p-button-info p-button-text-only" ng-click="showHistory(item);$event.stopPropagation();">History</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="rawDataSourceList.totalCount > rawDataSourceListPageSize">
                        <ul class="uibPagination" uib-pagination total-items="rawDataSourceList.totalCount" items-per-page="rawDataSourceListPageSize"
                            ng-model="$parent.$parent.rawDataSourceListCurrentPage" max-size="10" boundary-links="true" rotate="false" ng-change="changeDataSourceListPage()"></ul>
                    </div>
                </div>
            </div>
        </div>
        <form id="testForm" action="<%=request.getContextPath()%>/admin/tools/api/api-test.jsp" target="_blank">
            <input type="hidden" name="a">
            <input type="hidden" name="s">
            <input type="hidden" name="n">
        </form>
    </div>
</div>
</div>
</body>
</html>