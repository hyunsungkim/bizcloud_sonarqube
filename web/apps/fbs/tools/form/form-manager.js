var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
    , 'ng.jsoneditor'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/search\//) || config.url.match(/\/search.json/)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', '$interval', '$timeout', 'blockUI', 'AjaxService', 'marsContext',
    function ($scope, $filter, $uibModal, $interval, $timeout, blockUI, AjaxService, marsContext) {
        var parameter = new UrlParameterParser(location.href);
        $scope.caller = parameter.getParameterValue("caller", '');
        // Constants
        var TreeRoot = 'TreeRoot';
        var Application = 'Application';
        var Workspace = 'Workspace';
        var Project = 'Project';
        var Form = 'Form';
        var Block = 'Block';
        var ApplicationNodeId = "A0";
        var WorkspaceAllProjects = "WorkspaceAllProjects";

        $scope.TreeRoot = TreeRoot;
        $scope.Workspace = Workspace;
        $scope.Project = Project;
        $scope.Form = Form;
        $scope.Block = Block;
        $scope.WorkspaceAllProjects = WorkspaceAllProjects;

        // Variables
        $scope.contextPath = marsContext.contextPath;
        $scope.selectedNodeType = null;
        $scope.search = {};

        var blockUIStarted = false;

        function startBlockUI(msg) {
            blockUI.start(msg);
            blockUIStarted = true;
        }

        function stopBlockUI() {
            blockUI.stop();
            blockUIStarted = false;
        }

        function getObjectType(node) {
            var objectType = null;

            if ('undefined' != typeof node.objectType) {
                objectType = node.objectType;
            } else if (node.nodeId === ApplicationNodeId) {
                objectType = Application;
            } else {
                objectType = TreeRoot;
            }

            return objectType;
        }

        function generateNodeId(node) {
            var objectType = getObjectType(node);
            var nodeId = "";
            if (Workspace === objectType) {
                nodeId = "W" + node.id;
            } else if (Project === objectType) {
                nodeId = "P" + node.id;
            } else if (Form === objectType) {
                nodeId = "F" + node.id;
            } else {
                nodeId = ApplicationNodeId;
            }

            return nodeId;
        }

        function getNodeImage(node) {
            var image;
            var objectType = getObjectType(node);
            if (Workspace === objectType) {
                image = "../image/W.png";
            } else if (Project === objectType) {
                image = "../image/P.png";
            } else if (Form === objectType) {
                image = "../image/F.png";
            } else {
                image = "../image/A.png";
            }

            return image;
        }

        $scope.treeOptions = {
            titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: true, multipleSelect: false, filter: {},
            generateNodeId: generateNodeId,
            getNodeImage: getNodeImage,
            onSelectNode: function (node, selectedByClick) {
                $scope.selectedNode = node;
                $scope.selectedNodeType = getObjectType(node);
                $scope.treeOptions.selectedNode = node;
                $scope.dataValueListOrderColumn = 'label';
                $scope.transactionIds = undefined;

                if (selectedByClick) {
                    if ($scope.selectedNodeType === Form) {
                        getFormIntegration($scope.selectedNode);
                        getFormPublicationList(1);
                    }
                }
            },
            onExpandNode: function (node) {
                $scope.expandedNode = node;
            }
        };

        function callApi(apiUrl, callback, param) {
            var api = new AjaxService(marsContext.contextPath + apiUrl);
            api.post({
                success: function (o) {
                    callback(o);
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        function getWorkspaceTree() {
            callApi('/services/data/get/fbs.workspace-GetWorkspaceProjectFormTree@.json',
                function (o) {
                    $scope.treeRoot = [{
                        name: "Root",
                        nodeId: "rootNode",
                        image: "../image/R.png",
                        children: o.data
                    }];

                    // $scope.treeOptions.selectedNode = {
                    //     nodeId: "rootNode"
                    // };

                    stopBlockUI();
                }, {
                    ALL: true
                });
        }

        $scope.refreshTree = function () {
            getWorkspaceTree();
        };

        (function () {
            getWorkspaceTree();
        })();

        $scope.setHeight = function () {
            setHeight();
        };

        ///-------------------------------------------------------------

        $scope.publicationListCurrentPage = 1;
        $scope.publicationListPageSize = 15;
        $scope.publicationListOrderColumn = 'transactionId';
        $scope.publicationListOrderAsc = false;

        function callFormPublicationList(param) {
            callApi('/services/data/getList/fbs.publication-GetPublicationFormInfoSimple.json',
                function (o) {
                    $scope.publicationList = o.data;
                    stopBlockUI();
                }, param);
        }

        function getFormPublicationList(pageNo, filter) {
            var param = {
                formId: $scope.selectedNode.id,
                pageNo: pageNo || $scope.publicationListCurrentPage,
                pageSize: $scope.publicationListPageSize,
                ORDER_BY: $scope.publicationListOrderColumn + ' ' + ($scope.publicationListOrderAsc ? 'ASC' : 'DESC'),
                SIMPLE: true,
                INCLUDE_INTEGRATION: true
            };

            $scope.publicationListCurrentPage = param.pageNo;

            if (filter) {
                angular.extend(param, filter);
            }

            callFormPublicationList(param);
        }

        $scope.sortFormPublicationList= function (column) {
            if ($scope.publicationListOrderColumn === column) {
                $scope.publicationListOrderAsc = !$scope.publicationListOrderAsc;
            } else $scope.publicationListOrderAsc = true;
            $scope.publicationListOrderColumn = column;
            getFormPublicationList($scope.publicationListCurrentPage);
        };

        function getSearchParam() {
            if ($scope.search.version === "") $scope.search.version = undefined;
            if ($scope.search.transactionId === "") $scope.search.transactionId = undefined;
            return $scope.search;
        }

        $scope.refreshFormPublicationList = function () {
            getFormPublicationList(undefined, getSearchParam());
        };

        $scope.changePublicationListPage = function () {
            getFormPublicationList();
        };

        $scope.searchPublication = function () {
            getFormPublicationList(1, getSearchParam());
        };

        function getFormIntegration(form) {
            var param = {
                formId: form.id,
                formPublicationIdIsNull: true,
                INCLUDE_RAW_DATA_SOURCE: true
            };

            callApi('/services/data/get/fbs.integration-GetIntegrationForm.json',
                function (o) {
                    if (o && o.data && o.data.length > 0) {
                        form.formIntegration = o.data[0];
                    } else {
                        form.formIntegration = undefined;
                    }
                }, param);
        }

        function deleteIntegration(formIntegrationId, type) {
            var param = {
                id: formIntegrationId
            };

            callApi('/services/data/create/fbs.integration-DeleteIntegrationForm.json',
                function (o) {
                    if (type === 'form') {
                        getFormIntegration($scope.selectedNode);
                    } else {
                        $scope.refreshFormPublicationList();
                    }
                }, param);
        }

        $scope.deleteIntegration = function (formIntegrationId, type) {
            bootbox.confirm({
                message: "Are you sure to delete the integration setting?",
                buttons: {
                    cancel: {
                        label: 'No',
                        className: 'btn btn-sm btn-secondary'
                    },
                    confirm: {
                        label: 'Yes',
                        className: 'btn btn-sm btn-primary'
                    }
                },
                callback: function (result) {
                    if (result) {
                        deleteIntegration(formIntegrationId, type);
                    }
                }
            });
        };

        function updateFormRowDataSource(formPublication, rawDataSource) {
            formPublication.rawDataSrcId = rawDataSource.id;
            formPublication.rawDataSrcType = rawDataSource.type;

            var param = {
                projectId: $scope.selectedNode.projectId,
                formId: formPublication.formId,
                formPublicationId: formPublication.id,
                rawDataSrcId: rawDataSource.id,
                runWhen: rawDataSource.runWhen
            };

            callApi('/services/data/create/fbs.integration-SaveIntegrationForm.json',
                function (o) {
                    if (formPublication.objectType === 'form') {
                        getFormIntegration($scope.selectedNode);
                    } else {
                        $scope.refreshFormPublicationList();
                    }
                }, param);
        }

        $scope.setIntegration = function (formPublication) {
            var scope = $scope;
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/form/api-integration-list.html'),
                windowClass: 'app-modal-window-10',
                controller: ['$scope',
                    function ($scope) {
                        $scope.bgColor = bgColor;
                        $scope.textColor = textColor;

                        function callRawDataSourceListApi(param) {
                            startBlockUI();
                            callApi('/services/data/getList/fbs.rawData-GetProjectRawDataSource.json',
                                function (o) {
                                    $scope.rawDataSourceList = o.data;
                                    stopBlockUI();
                                }, param);
                        }

                        $scope.formPublication = formPublication;

                        $scope.rawDataSourceListCurrentPage = 1;
                        $scope.rawDataSourceListPageSize = 15;
                        $scope.rawDataSourceListOrderColumn = 'type';
                        $scope.rawDataSourceListOrderAsc = true;

                        function getRawDataSourceList(pageNo, filter) {
                            var param = {
                                pageNo: pageNo || $scope.rawDataSourceListCurrentPage,
                                pageSize: $scope.rawDataSourceListPageSize,
                                ORDER_BY: $scope.rawDataSourceListOrderColumn + ' ' + ($scope.rawDataSourceListOrderAsc ? 'ASC' : 'DESC'),
                                formId: formPublication.formId,
                                SIMPLE: true
                            };

                            $scope.rawDataSourceListCurrentPage = param.pageNo;

                            if (filter) {
                                angular.extend(param, filter);
                            }

                            callRawDataSourceListApi(param);
                        }

                        $scope.adjustItem = function (item) {
                            if (!item._initDone && item.id == $scope.formPublication.rawDataSrcId) {
                                item.runWhen = $scope.formPublication.rawDataRunWhen;
                                item._initDone = true;
                            }
                            return item;
                        }

                        $scope.search = function () {
                            getRawDataSourceList(undefined, {
                                LIKE_type_LIKE: $scope.searchName
                            });
                        };

                        $scope.changeDataSourceListPage = function () {
                            getRawDataSourceList();
                        };

                        $scope.selectedRawDataSource = function (rawDataSource) {
                            $scope.rawDataSource = rawDataSource;
                        };

                        $scope.isValidSelected = function () {
                            return $scope.rawDataSource && $scope.rawDataSource.runWhen;
                        };

                        (function () {
                            getRawDataSourceList();
                        })();

                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };
                        $scope.ok = function () {
                            $scope.$close($scope.rawDataSource);
                        };
                    }
                ]
            }).result.then(function (rawDataSource) {
                updateFormRowDataSource(formPublication, rawDataSource);
            }, function () {
            });
        };

        function saveFormOption(formPublication, formOption) {
            formOption.projectId = $scope.selectedNode.projectId;
            formOption.formId = formPublication.formId;
            formOption.formPublicationId = formPublication.id;

            callApi('/services/data/create/fbs.form-SaveFormOption.json',
                function (o) {
                    formOption.originalRunWhen = formOption.runWhen;
                    angularExt.getBootboxObject().alert("Save was completed successfully.");
                }, formOption);
        }

        $scope.setOption = function (formPublication) {
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/form/form-option.html'),
                windowClass: 'app-modal-window-6',
                controller: ['$scope',
                    function ($scope) {
                        $scope.bgColor = bgColor;
                        $scope.textColor = textColor;
                        $scope.formPublication = formPublication;

                        function getFormOption(param) {
                            startBlockUI();
                            callApi('/services/data/get/fbs.form-GetFormOption.json',
                                function (o) {
                                    $scope.formOptions = o.data;
                                    $scope.allFormOptions = [
                                        {
                                            label: "Sync Form Data To Process Variables",
                                            type: "SyncFormDataToPV",
                                            originalRunWhen: (o.data && o.data.SyncFormDataToPV) ? o.data.SyncFormDataToPV.runWhen : "C",
                                            runWhen: (o.data && o.data.SyncFormDataToPV) ? o.data.SyncFormDataToPV.runWhen : "C"
                                        }
                                    ];
                                    stopBlockUI();
                                }, param);
                        }

                        $scope.saveOption = function (formOption) {
                            saveFormOption(formPublication, formOption);
                        };

                        (function () {
                            getFormOption({
                                formId: formPublication.formId,
                                formPublicationId: formPublication.id,
                                formPublicationIdIsNull: formPublication.formPublicationIdIsNull,
                                MAP_COLUMN: "type"
                            })
                        })();

                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };
                        $scope.ok = function () {
                            $scope.$dismiss();
                        };
                    }
                ]
            }).result.then(function (formOption) {
            }, function () {
            });
        }

        $scope.viewDataStructure = function (formPublication) {
            var scope = $scope;
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/form/form-data-structure.html'),
                windowClass: 'app-modal-window-11',
                controller: ['$scope',
                    function ($scope) {
                        $scope.bgColor = bgColor;
                        $scope.textColor = textColor;
                        $scope.jsonEditor = {
                            options: {
                                mode: 'code',
                                modes: ['tree', 'code'],
                                enableTransform: false,
                                onEditable: function () {
                                    return false;
                                }
                            }
                        };

                        $scope.formPublication = formPublication;
                        $scope.viewHeight = mars$util$.getClientSize().height - 250;

                        $scope.setJsonEditorInstance = function (instance) {
                            $scope.jsonEditor.instance = instance;
                        };

                        function setXmlData(xml) {
                            var formattedXml = new XmlBeautify().beautify(xml,
                                {
                                    indent: "    ",
                                    useSelfClosingElement: true
                                });

                            var textarea = document.getElementById("textarea");
                            textarea.innerText = formattedXml;
                        }

                        function callGetFormDataStructureApi(param) {
                            startBlockUI();
                            callApi('/services/data/run/fbs.form-GetFormDataStructure@.json',
                                function (o) {
                                    $scope.formDataStructure = o.data;
                                    $scope.jsonEditor.data = $scope.formDataStructure.jsonData;
                                    setXmlData($scope.formDataStructure.xmlData);
                                    stopBlockUI();
                                }, param);
                        }

                        function getFormDataStructureApi() {
                            callGetFormDataStructureApi({
                                projectPublicationId: formPublication.projectPublicationId
                            });
                        }

                        (function () {
                            getFormDataStructureApi();
                        })();

                        $scope.ok = function () {
                            $scope.$dismiss();
                        };
                    }
                ]
            }).result.then(function (rawDataSource) {
            }, function () {
            });
        };

        $scope.openForm = function (item) {
            var url = marsContext.contextPath + "/apps/fbs/app/?isfi=" + encodeURIComponent(item.isfi) + "&preview_mode=&procid=-2&actseq=-1&workseq=-1";
            window.open(url, item.isfi);
        };

        $scope.copyUrlForWIH = function (item) {
            var url = "/apps/fbs/launcher.jsp?isfi=" + encodeURIComponent(item.isfi);
            mars$util$.copyToClipboard(url);
        };

        $scope.showISFIClicked = function() {
            $scope.showISFI = !$scope.showISFI;
        };
    }]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";
}
