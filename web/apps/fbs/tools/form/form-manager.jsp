<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AppDev Form Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/font-bizflow/font-bizflow.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/jsoneditor/dist/jsoneditor.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../../includes/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../../includes/node_modules-ext/jsoneditor/dist/jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/ng-jsoneditor/ng-jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/xml-beautify/dist/XmlBeautify.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("form-manager.js");
    </script>
    <style>
        tr.publication td {
            padding: 2px 5px 2px 5px;
        }
    </style>
    <script>
        var bgColor = "<%=bgColor%>";
        var textColor = "<%=textColor%>";
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center" style="padding: 10px 0 10px 0">
    <h1>AppDev Form Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row flex-nowrap" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div style="padding-bottom: 0;" id="leftPanelHeader">
                    <div class="bio-left-title">
                        <div class="title"><h5>Workspaces</h5></div>
                        <div class="i-btn">
                            <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshTree();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block" id="treeContainer">
                    <div class="form-group form-group-sm col-md-12">
                        <input type="text" ng-model="treeOptions.filter.name" class="searchinput" placeholder="Enter a name to filter" style="margin-bottom: 10px;">
                        <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                    </div>
                    <div id="treePanelContainer" class="panelContainer">
                        <tree nodes='treeRoot' options="treeOptions"></tree>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary max-height col-md-12" id="rightPanelContainer">
                <div ng-show="selectedNodeType !== Form">
                    <div class="bio-right-title">
                        <div class="text-center">Select a form to manage integration.</div>
                    </div>
                </div>
                <div ng-show="selectedNodeType === Form">
                    <div class="bio-right-title">
                        <div class="title">
                            <h3>{{selectedNode.name}}</h3>
                        </div>
                    </div>
                    <div class="bio-right-title mt-0">
                        <div class="title">
                            <div ng-if="publicationList">
                                <div class="textbox">
                                    <label for="searchTitle">Version</label>
                                    <input type="text" id="searchTitle" ng-enter="searchPublication()" ng-model="search.version">
                                </div>
                                <div class="textbox">
                                    <label for="searchName">Transaction ID</label>
                                    <input type="text" id="searchName" ng-enter="searchPublication()" ng-model="search.transactionId">
                                </div>
                                <div class="textbox">
                                    <button class="bf-button p-component bf-button-icon-only" title="Search" ng-click="searchPublication()">
                                        <i class="fa fa-search"></i>
                                    </button>
                                    <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-if="selectedNode" ng-click="refreshFormPublicationList();$event.stopPropagation();">
                                        <i class="fa fa-repeat"></i>
                                    </button>
                                    <button class="p-button p-component p-button-danger p-button-text-only" ng-if="selectedNode" ng-show="selectedNode.formIntegration.id"
                                            ng-click="deleteIntegration(selectedNode.formIntegration.id, 'form');$event.stopPropagation();">Delete Integration
                                    </button>
                                    <button class="p-button p-component p-button-warning p-button-text-only" ng-if="selectedNode && caller !== 'bizflow'"
                                            ng-click="setIntegration({formId: selectedNode.id, rawDataSrcId: selectedNode.formIntegration.rawDataSrcId, rawDataRunWhen: selectedNode.formIntegration.runWhen, objectType:'form'});$event.stopPropagation();">Set Integration
                                    </button>
                                    <button class="p-button p-component p-button-text-only" ng-if="selectedNode && caller !== 'bizflow'"
                                            ng-click="setOption({formId: selectedNode.id, formPublicationIdIsNull: true});$event.stopPropagation();">Set Option
                                    </button>
                                </div>
                                <div class="textbox">
                                    <input type="checkbox" ng-model="showISFI" ng-change="showISFIClicked();$event.stopPropagation();"> <label for="searchTitle">Show ISFI</label>
                                </div>
                            </div>
                        </div>
                        <div class="i-btn" ng-if="caller !== 'bizflow'">
                            Form Default Integration:
                            <span class="badge badge-pill badge-info" style="font-size: small" ng-show="selectedNode.formIntegration.rawDataSrcType">{{selectedNode.formIntegration.rawDataSrcType}}</span>
                            <span class="badge badge-pill badge-secondary" style="font-size: small" ng-show="!selectedNode.formIntegration.rawDataSrcType">None</span>
                        </div>
                    </div>
                    <div class="card-block panelContainer">
                        <div ng-if="publicationList.data.length == 0">
                            <div class="text-center">There are no publications</div>
                        </div>
                        <div class="bf-table-2">
                            <table class="table table-striped table-condensed table-font-small text-<%=textColor%>" ng-if="publicationList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold">
                                    <th>#</th>
                                    <th ng-click="sortFormPublicationList('name')"><a>Name</a></th>
                                    <th ng-click="sortFormPublicationList('userName')"><a>Publisher</a></th>
                                    <th ng-click="sortFormPublicationList('actionDate')"><a>Publish Date</a></th>
                                    <th ng-click="sortFormPublicationList('version')"><a>Version</a></th>
                                    <th ng-click="sortFormPublicationList('transactionId')"><a>Transaction ID</a></th>
                                    <th ng-click="sortFormPublicationList('rawDataSrcType')"><a>Integration</a></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat-start="item in publicationList.data" id="{{item.id}}" class="publication">
                                    <td rowspan="2">{{item.ROW_NUMBER}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.userName}}</td>
                                    <td>{{item.actionDate | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm'}}</td>
                                    <td>{{item.version}}</td>
                                    <td>{{item.transactionId}}</td>
                                    <td>{{item.rawDataSrcType}}</td>
                                    <td class="right-btn" style="vertical-align: middle;">
                                        <button class="p-button p-component p-button-danger p-button-text-only" ng-click="deleteIntegration(item.formIntegrationId);$event.stopPropagation();" ng-show="item.formIntegrationId">Delete Integration</button>
                                        <button class="p-button p-component p-button-warning p-button-text-only" ng-click="setIntegration(item);$event.stopPropagation();" ng-if="caller !== 'bizflow'">Set Integration</button>
                                        <button class="p-button p-component p-button-primary p-button-text-only" ng-click="setOption(item);$event.stopPropagation();" ng-if="caller !== 'bizflow'">Set Option</button>
                                        <button class="p-button p-component p-button-info p-button-text-only" ng-click="viewDataStructure(item);$event.stopPropagation();">View Data Structure</button>
                                        <button class="p-button p-component p-button-success p-button-text-only" ng-click="openForm(item);$event.stopPropagation();">Open Form</button>
                                        <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="copyUrlForWIH(item);$event.stopPropagation();">Copy URL for WIH</button>
                                    </td>
                                </tr>
                                <tr ng-repeat-end>
                                    <td colspan="7" class="text-left" ng-show="showISFI">
                                        <div class="overflow-auto" style="font-size: 0.6rem;">
                                            ISFI: {{item.isfi}}
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="publicationList.totalCount > publicationListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="publicationList.totalCount" items-per-page="publicationListPageSize" ng-model="$parent.publicationListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changePublicationListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form name="downloadFile" target="frame" method="POST">
    <input type="hidden" name="param">
</form>
<iframe id="frame" name="frame" src="" style="width: 100%;height: 95%;border: 0;padding: 0;margin: 0;display: none;" frameborder="0"></iframe>
</body>
</html>