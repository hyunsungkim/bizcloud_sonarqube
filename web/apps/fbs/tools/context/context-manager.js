var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
    , 'ng.jsoneditor'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//) || config.url.match(/myTimeInfo.json/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$timeout', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext',
    function ($scope, $timeout, $filter, $uibModal, blockUI, AjaxService, marsContext) {
        var parameter = new UrlParameterParser(location.href);
        $scope.projectId = parameter.getParameterValue("project_id", '');
        var ZeroId = 'N00000000000000000000000000000000';
        // Constants
        var TreeRoot = 'TreeRoot';
        var Workspace = 'Workspace';
        var Project = 'Project';

        $scope.Workspace = Workspace;
        $scope.Project = Project;
        $scope.TreeRoot = TreeRoot;
        $scope.selectedNodeType = TreeRoot;

        // Variables
        var Original = 'Original';
        var Merged = 'Merged';

        $scope.Original = Original;
        $scope.Merged = Merged;
        $scope.showMode = Original;
        $scope.search = {};

        var blockUIStarted = false;
        var jsonHasNull = false;

        function startBlockUI() {
            blockUI.start();
            blockUIStarted = true;
        }

        function stopBlockUI() {
            blockUI.stop();
            blockUIStarted = false;
        }

        $scope.jsonEditor = {
            options: {
                mode: 'tree',
                modes: ['tree', 'code'],
                enableTransform: false,
                onEditable: function () {
                    return $scope.selectedNodeType !== TreeRoot && $scope.showMode === Original;
                }
            }
        };

        function getObjectType(node) {
            var objectType = null;

            if ('undefined' != typeof node.projectCount) {
                objectType = Workspace;
            } else if ('undefined' != typeof node.formCount) {
                objectType = Project;
            } else {
                objectType = TreeRoot;
            }

            return objectType;
        }

        function generateNodeId(node) {
            var objectType = getObjectType(node);
            var nodeId = "";
            if (Workspace === objectType) {
                nodeId = "W" + node.id;
            } else if (Project === objectType) {
                nodeId = "P" + node.id;
            } else {
                nodeId = TreeRoot;
            }

            return nodeId;
        }

        function getNodeImage(node) {
            var image;
            var objectType = getObjectType(node);
            if (Workspace === objectType) {
                image = "../image/W.png";
            } else if (Project === objectType) {
                image = "../image/P.png";
            } else {
                image = "../image/A.png";
            }

            return image;
        }

        $scope.treeOptions = {
            titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
            generateNodeId: generateNodeId,
            getNodeImage: getNodeImage,
            onSelectNode: function (node, selectedByClick) {
                var selectedNodeType = getObjectType(node);
                $scope.selectedNodeType = selectedNodeType;
                $scope.selectedNode = node;
                $scope.treeOptions.selectedNode = node;
                $scope.dataValueListOrderColumn = 'label';

                if (selectedByClick) {
                    getContext(node);
                }
            },
            onExpandNode: function (node) {
                $scope.expandedNode = node;
            }
        };

        function getWorkspaceTree() {
            var param = {
                LIKE_permission_LIKE: ["%M%", "P"]
            }
            if ($scope.projectId) param.projectId = $scope.projectId;
            var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.workspace-GetWorkspaceProjectTree@.json');
            api.post({
                success: function (o) {
                    $scope.treeRoot = [{
                        name: "Context Root",
                        nodeId: "rootNode",
                        image: "../image/R.png",
                        children: o.data
                    }];

                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        $scope.refreshTree = function () {
            getWorkspaceTree();
        };

        (function () {
            getWorkspaceTree();
        })();

        $scope.setJsonEditorInstance = function (instance) {
            $scope.jsonEditor.instance = instance;
        };

        function callGetMergedContext(namespace, param) {
            var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.' + namespace + '-GetContext@.json');
            api.post({
                success: function (o) {
                    $scope.context = o.data;
                    $scope.jsonEditor.instance.set($scope.context);
                    try {
                        $scope.jsonEditor.instance.expandAll();
                    } catch (e) {
                    }
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        function getMergedContext(item) {
            if (Workspace === $scope.selectedNodeType) {
                callGetMergedContext("workspace", {
                    id: item.id
                });
            } else if (Project === $scope.selectedNodeType) {
                callGetMergedContext("project", {
                    id: item.id
                });
            }

            $scope.showMode = Merged;
        }

        function callGetContext(param) {
            var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.context-GetContext.json');
            api.post({
                success: function (o) {
                    if(o.data.length > 0){
                        $scope.context = o.data[0];
                        $scope.isDefaultContent = false;
                    }else{
                        $scope.context = {content: '{}'};
                        $scope.isDefaultContent = true;
                    }
                    $scope.jsonEditor.instance.set(JSON.parse($scope.context.content));
                    try {
                        $scope.jsonEditor.instance.expandAll();
                    } catch (e) {
                    }
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        function getContext(item) {
            if (TreeRoot === $scope.selectedNodeType) {
                callGetContext({
                    workspaceIdIsNull: ZeroId,
                    projectIdIsNull: ZeroId
                });
            } else if (Workspace === $scope.selectedNodeType) {
                callGetContext({
                    workspaceId: item.id
                });
            } else if (Project === $scope.selectedNodeType) {
                callGetContext({
                    projectId: item.id
                });
            }

            $scope.showMode = Original;
        }

        function saveContext(data, callback) {
            startBlockUI();
            var api = new AjaxService(marsContext.contextPath + '/services/data/create/fbs.context-SaveContext.json');
            api.post({
                success: function (o) {
                    showMessageInfo("Successfully Saved.");
                    if($scope.isDefaultContent) getContext($scope.selectedNode);
                    if(callback){
                        getContext($scope.selectedNode);
                        callback();
                    }
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, data);
        }

        $scope.saveContext = function (content, callback) {
            jsonHasNull = false;
            checkJsonContent(content);
            if(!jsonHasNull) {
                if (Workspace === $scope.selectedNodeType) {
                    saveContext({
                        workspaceId: $scope.selectedNode.id,
                        content: content
                    }, callback);
                } else if (Project === $scope.selectedNodeType) {
                    saveContext({
                        projectId: $scope.selectedNode.id,
                        content: content
                    }, callback);
                }
            } else {
                bootbox.alert("There is one or more null values in the context. Please check.");
            }
        };

        function checkJsonContent(obj) {
            for(var key in obj){
                if (obj.hasOwnProperty(key)) {
                    //In the case of Undefined, NaN, and so on, json editor blocks it.
                    if (obj[key] === null) {
                        jsonHasNull = true;
                        break;
                    } else if (Object.prototype.toString.call(obj[key]) === '[object Object]') {
                        checkJsonContent(obj[key]);
                    } else if (Array.isArray(obj[key])) {
                        obj[key].forEach(element => checkJsonContent(element));
                    }
                }
            }
        }

        function callGetProjectStateVariableNames(param) {
            var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.project-GetStateVariableNames@.json');
            api.post({
                success: function (o) {
                    if (o.data) {
                        var variables = o.data;
                        var state;
                        var data = $scope.jsonEditor.instance.get();
                        if (data && data.state) {
                            state = data.state;
                        } else {
                            state = {};
                        }

                        if (state) {
                            for (var i = 0; i < variables.length; i++) {
                                if (!state[variables[i]]) {
                                    state[variables[i]] = "";
                                }
                            }
                        }
                        if (!(data && data.state)) {
                            data.state = state;
                        }

                        $scope.jsonEditor.instance.set(data);
                    }
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        $scope.getProjectStateVariableNames = function () {
            if (Project === $scope.selectedNodeType) {
                callGetProjectStateVariableNames({
                    projectId: $scope.selectedNode.id
                });
            }
        };

        $scope.changeContextShowMode = function (mode) {
            if (mode === Merged) {
                getMergedContext($scope.selectedNode);
            } else {
                getContext($scope.selectedNode);
            }
        };

        function showMessageAlert(message) {
            showMessage(message, "Alert");
        }

        function showMessageInfo(message) {
            showMessage(message, "Info");
        }

        function  showMessageWarning(message) {
            showMessage(message, "Warning");
        }

        function showMessage(message, type) {
            if (parent && parent.$B && parent.$B.ExceptionHandler) {
                if (type === "Alert") {
                    parent.$B.ExceptionHandler.pin('Alert', message);
                } else if (type === "Info") {
                    parent.$B.ExceptionHandler.toast('Info', message);
                } else if (type === "Warning") {
                    parent.$B.ExceptionHandler.toast('Warning', message);
                }
            }
        }

        function getHistory(callback) {
            $scope.contextHistories = [];
            var param = {};
            if (Workspace === $scope.selectedNodeType) {
                param = {
                    workspaceId: $scope.selectedNode.id
                };
            } else if (Project === $scope.selectedNodeType) {
                param = {
                    projectId: $scope.selectedNode.id
                };
            }
            var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.context-GetContextHistorySimpleList@.json');
            api.post({
                success: function (o) {
                    $scope.contextHistories = o.data.data;
                    if (callback) {
                        callback();
                    }
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        $scope.showContextHistory = function() {
            var callback = $scope.loadContextHistory;
            getHistory(callback);
        };

        $scope.loadContextHistory = function () {
            var scope = $scope;
            var value, orig, dv, highlight = true, connect = "align", collapse = true;
            var pageSize = 10;
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/context/context-history.html'),
                size: 'xl',
                controller: ['$scope',
                    function ($scope) {
                        $scope.scope = scope;
                        $scope.contextPath = marsContext.contextPath;
                        $scope.title = "Context Versioning";
                        $scope.selectedNode = scope.selectedNode;
                        $scope.selectedNodeType = scope.selectedNodeType;
                        $scope.contextHistoriesLeft = scope.contextHistories;
                        $scope.contextHistoriesRight = scope.contextHistories;
                        $scope.selectedHistoryLeft = scope.contextHistories[0];
                        $scope.selectedHistoryRight = scope.contextHistories[0];
                        $scope.selectedNumLeft = 0;
                        $scope.selectedNumRight = -1;
                        $scope.isChangedLeft = false;
                        $scope.isChangedRight = false;

                        function initUI() {
                            if (value == null) return;
                            var target = document.getElementById("view");
                            target.innerHTML = "";
                            dv = CodeMirror.MergeView(target, {
                                value: value,
                                origLeft: null,
                                orig: orig,
                                lineNumbers: true,
                                mode: "application/ld+json",
                                highlightDifferences: highlight,
                                allowEditingOriginals: true,
                                connect: connect,
                                collapseIdentical: collapse
                            });

                            dv.editor().on("change",function(cm,change){
                                $scope.setChanged("L", dv.editor().getValue() !== value ? true : false)
                            });

                            dv.rightOriginal().on("change",function(cm,change){
                                $scope.setChanged("R", dv.rightOriginal().getValue() !== orig ? true : false)
                            });

                            dv.editor().on("beforeChange",function(cm,change){
                                $scope.beforeChangeLeft = value;
                            });

                            dv.rightOriginal().on("beforeChange",function(cm,change){
                                $scope.beforeChangeRight = orig;
                            });

                            resize(dv);

                            $(window).on('resize', function () {
                                resize(dv);
                            });
                        }

                        $scope.setChanged = function (view, flag) {
                            if ("L" === view && $scope.isChangedLeft !== flag) {
                                $scope.isChangedLeft = flag;
                                $scope.$apply();
                            } else if ("R" === view && $scope.isChangedRight !== flag) {
                                $scope.isChangedRight = flag;
                                $scope.$apply();
                            }
                        };

                        function changeLeftViewData() {
                            $scope.isChangedLeft = false;
                            if (null == dv || undefined == dv) {
                                initUI();
                            } else {
                                dv.editor().setValue(value);
                            }
                        }

                        function changeRightViewData() {
                            $scope.isChangedRight = false;
                            if (null == dv || undefined == dv) {
                                initUI();
                            } else {
                                dv.rightOriginal().setValue(orig);
                            }
                        }

                        function resize(mergeView) {
                            var historyContainer = document.getElementById("historyContainer");
                            var historyContentHeader = document.getElementById("historyContentHeader");
                            if(historyContainer && historyContentHeader) {
                                var height = historyContainer.offsetHeight - historyContentHeader.offsetHeight;
                                mergeView.editor().setSize(null, height);
                                mergeView.rightOriginal().setSize(null, height);
                                mergeView.wrap.style.height = height + "px";
                            }
                        }

                        function getHistoryContent(selectedData, selectedView, callback) {
                            var param = {
                                id: selectedData.id,
                                transactionId: selectedData.transactionId
                            };
                            var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.context-GetContextHistory.json');
                            api.post({
                                success: function (o) {
                                    if (o.data && o.data[0].content) {
                                        var result = JSON.parse(o.data[0].content);
                                        var resultString = JSON.stringify(result, null, 2);

                                        if ("L" === selectedView) {
                                            value = resultString;
                                        } else if ("R" === selectedView) {
                                            orig = resultString;
                                        } else if ("ALL" === selectedView) {
                                            value = resultString;
                                            orig = resultString;
                                        }
                                        if (callback) {
                                            if (callback === initUI) {
                                                $timeout(function(){
                                                    callback();
                                                }, 100);
                                            } else {
                                                callback();
                                            }
                                        }
                                    }
                                },
                                error: function (o) {
                                    stopBlockUI();
                                    o.alertException();
                                }
                            }, param);
                        }

                        $scope.selectHistoryLeft = function (idx, selectedData) {
                            if($scope.isChangedLeft || $scope.isChangedRight){
                                showMessageWarning("Save/Revert Required.");
                            }else{
                                $scope.selectedNumLeft = idx;
                                $scope.selectedHistoryLeft = idx===-1 ? $scope.contextHistoriesLeft[0] : selectedData;
                                getHistoryContent($scope.selectedHistoryLeft, "L", changeLeftViewData);
                            }
                        };
                        $scope.selectHistoryRight = function (idx, selectedData){
                            if($scope.isChangedLeft || $scope.isChangedRight){
                                showMessageWarning("Save/Revert Required.");
                            }else{
                                $scope.selectedNumRight = idx;
                                $scope.selectedHistoryRight = idx===-1 ? $scope.contextHistoriesRight[0] : selectedData;
                                getHistoryContent($scope.selectedHistoryRight, "R", changeRightViewData);
                            }
                        };

                        $scope.loadMore = function(view) {
                            pageSize = 10000;
                            $scope.getHistoryList(view);
                        };

                        $scope.getHistoryList = function(view, callback){
                            var param = {
                                pageSize: pageSize
                            };
                            if (Workspace === $scope.selectedNodeType) {
                                param.workspaceId = $scope.selectedNode.id;
                            } else if (Project === $scope.selectedNodeType) {
                                param.projectId = $scope.selectedNode.id;
                            }
                            var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.context-GetContextHistorySimpleList@.json');
                            api.post({
                                success: function (o) {
                                    $scope.contextHistoriesLeft = o.data.data;
                                    $scope.contextHistoriesRight = o.data.data;

                                    if (callback) {
                                        if (callback === initHistoryView) {
                                            $(".list-group").scrollTop(0);
                                            $scope.selectedHistoryLeft = $scope.contextHistoriesLeft[0];
                                            $scope.selectedHistoryRight = $scope.contextHistoriesRight[0];
                                            $scope.selectedNumLeft = 0;
                                            $scope.selectedNumRight = -1;
                                            $scope.isChangedLeft = false;
                                            $scope.isChangedRight = false;
                                        }
                                        callback();
                                    }
                                },
                                error: function (o) {
                                    //stopBlockUI();
                                    o.alertException();
                                }
                            }, param);
                        };

                        function checkFormatJSON(obj) {
                            try {
                                JSON.parse(obj.getValue());
                                return true;
                            } catch(e) {
                                return false;
                            }
                        }

                        function getViewObj(view) {
                            var obj;
                            if ("L" === view) {
                                obj = dv.editor();
                            } else if ("R" === view) {
                                obj = dv.rightOriginal();
                            }
                            return obj;
                        }

                        $scope.revert = function (view) {
                            $scope.save(view);
                        };

                        $scope.save = function (view) {
                            var obj = getViewObj(view);
                            if (checkFormatJSON(obj)) {
                                scope.saveContext(obj.getValue(), resetHistory);
                            } else {
                                showMessageAlert("JSON Format Error");
                            }
                        };

                        $scope.reset = function (view){
                            if(view==='L') {
                                dv.editor().setValue($scope.beforeChangeLeft);
                            }else{
                                dv.rightOriginal().setValue($scope.beforeChangeRight);
                            }
                        };

                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };

                        $scope.setHeight = function () {
                            setHeight();
                        };

                        function resetHistory() {
                            var callback = initHistoryView;
                            $scope.getHistoryList("ALL", callback);
                        }

                        function initHistoryView() {
                            getHistoryContent($scope.selectedHistoryLeft, "ALL", initUI);
                        }

                        (function () {
                            initHistoryView();
                        })();

                    }
                ]
            }).result.then(function (contextData) {

            }, function () {

            });
        };

        $scope.setHeight = function () {
            setHeight();
        };
    }]);


function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";

    var rightPanelHeader = document.getElementById("rightPanelHeader");
    var jsonEditor = document.getElementById("jsonEditor");
    jsonEditor.style.height = (height - rightPanelHeader.offsetTop - rightPanelHeader.offsetHeight) + "px";

    var historyModal = document.getElementById("historyModal");
    if (historyModal) historyModal.style.height = (height * 0.9) + "px";
}

