<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AppDev Context Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/jsoneditor/dist/jsoneditor.min.css"/>
    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/lib/codemirror.css">
    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/addon/merge/merge.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../../includes/node_modules-ext/jsoneditor/dist/jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/ng-jsoneditor/ng-jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/codemirror/lib/codemirror.js"></script>
    <script src="../../../../includes/node_modules/codemirror/addon/merge/merge.js"></script>
    <script src="../../../../includes/node_modules/diff-match-patch/javascript/diff_match_patch.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("context-manager.js");
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>AppDev Context Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row flex-nowrap" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div style="padding-bottom: 0;" id="leftPanelHeader">
                    <div class="bio-left-title">
                        <div class="title"><h5>Projects</h5></div>
                        <div class="i-btn">
                            <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshTree();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block" id="treeContainer">
                    <div class="form-group form-group-sm col-md-12">
                        <input type="text" ng-model="treeOptions.filter.name" class="searchinput" placeholder="Enter name to filter" style="margin-bottom: 10px;">
                        <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                    </div>
                    <div id="treePanelContainer" class="panelContainer">
                        <tree nodes='treeRoot' options="treeOptions"></tree>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                <div id="rightPanelHeader">
                    <div class="bio-right-title bio-pd-15">
                        <div class="title">
                            <h3>
                                <span ng-if="selectedNode.id">{{selectedNode.name}}</span> Context
                            </h3>
                        </div>
                        <div class="i-btn">
                            <div class="space"></div>
                            <button class="p-button p-component p-button-text-only" ng-click="getProjectStateVariableNames();$event.stopPropagation();" ng-if="selectedNodeType === Project && showMode === Original">Collect State Variables</button>
                            <button class="p-button p-component p-button-text-only" ng-click="changeContextShowMode(Merged);$event.stopPropagation();" ng-if="selectedNodeType !== TreeRoot && showMode === Original">Show Merge Mode</button>
                            <button class="p-button p-component p-button-text-only" ng-click="changeContextShowMode(Original);$event.stopPropagation();" ng-if="selectedNodeType !== TreeRoot && showMode === Merged">Show Original Mode</button>
                            <button class="p-button p-component p-button-text-only" ng-click="saveContext(jsonEditor.data);$event.stopPropagation();" ng-if="selectedNodeType !== TreeRoot" ng-disabled="!(jsonEditor.data && showMode === Original)">Save</button>
                            <button class="p-button p-component p-button-info p-button-text-only" ng-click="showContextHistory();$event.stopPropagation();" ng-if="selectedNodeType !== TreeRoot" ng-disabled="!context || isDefaultContent">History</button>
                        </div>
                    </div>
                </div>
                <div class="card-block panelContainer overflow-hidden">
                    <div ng-show="!context" class="text-center" style="padding-top: 20px;">
                        Please select a project.
                    </div>
                    <div ng-show="context" style="background-color: white;height: 100%;">
                        <div ng-jsoneditor="setJsonEditorInstance" ng-model="jsonEditor.data" options="jsonEditor.options" class="max-width" id="jsonEditor"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>