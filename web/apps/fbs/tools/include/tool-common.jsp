<%@ page import="com.bizflow.io.core.lang.util.LangUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%
    boolean showTitle = LangUtil.isTrueObject(request.getParameter("t"));
    String bgColor;
    String textColor;
    String theme = ServletUtil.getParameterValue(request, "theme", "dark");
    if ("light".equalsIgnoreCase(theme)) {
        bgColor = "light";
        textColor = "dark";
    } else {
        theme = "dark";
        bgColor = org.owasp.encoder.Encode.forCssUrl(ServletUtil.getParameterValue(request, "bc", "dark"));
        textColor = org.owasp.encoder.Encode.forCssUrl(ServletUtil.getParameterValue(request, "tc", "white"));
    }
%>

<script>
    var themeBgColor = "<%=bgColor%>";
    var themeTextColor = "<%=textColor%>";
</script>
