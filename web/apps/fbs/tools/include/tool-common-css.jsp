<%if ("dark".equalsIgnoreCase(theme)) { %>
<style>
    .bootbox .modal-content {
        background-color: #343a40;
        border-color: #6c757d !important;
    }

    .uib-datepicker {
        background-color: #343a40;
    }

    ul.uib-datepicker-popup {
        background-color: #6c757d;
    }
</style>
<%}%>