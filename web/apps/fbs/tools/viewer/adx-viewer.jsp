<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>AppDev ADX Viewer</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/select2/dist/css/select2.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
    </script>
    <script>
        function _onload() {
            var clientSize = mars$util$.getClientSize();
            var frame = document.getElementById("frame");
            var height = clientSize.height - frame.offsetTop - 50;
            frame.style.height = height + "px";
        }

        function submitForm() {
            var form = document.forms[0];
            if (form.file.value == "") {
                bootbox.alert("Please select an ADX file");
                return;
            }

            form.submit();
        }
    </script>
</head>
<body onload="_onload()" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>AppDev ADX Viewer</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="container-fluid">
        <form name="form" method="post" target="frame" action="<%=request.getContextPath()%>/services/file/run/fbs.viewer-ViewADX@.json" enctype="multipart/form-data">
            <div class="row bg-<%=bgColor%>">
                <div class="form-group">
                    <label for="file1">
                        <h4>ADX file: </h4>
                    </label>&nbsp;
                    <input type="file" name="file" id="file1" accept=".adx" required>
                    <input type="button" class="p-button p-component p-button-secondary p-button-text-only ml-1" value="View" onclick="submitForm(this);">
                </div>
            </div>
        </form>
    </div>
    <iframe id="frame" name="frame" src="about:blank" style="width: 100%;height: 95%;border: 0;padding: 0;margin: 0;overflow: hidden;" frameborder="0"></iframe>
    <iframe name="hiddenGround" id="hiddenGround" src="about:blank" frameborder="0" width="5" height="5" style="visibility: hidden;"></iframe>
</div>
</body>
</html>