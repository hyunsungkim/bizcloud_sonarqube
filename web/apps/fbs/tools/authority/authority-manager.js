var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/search\//) || config.url.match(/\/search.json/)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', '$interval', '$timeout', 'blockUI', 'AjaxService', 'marsContext',
    function ($scope, $filter, $uibModal, $interval, $timeout, blockUI, AjaxService, marsContext) {
        var parameter = new UrlParameterParser(location.href);
        $scope.caller = parameter.getParameterValue("caller", '');
        // Constants
        var TreeRoot = 'TreeRoot';
        var Application = 'Application';
        var Workspace = 'Workspace';
        var Project = 'Project';
        var ApplicationNodeId = "A0";
        var WorkspaceAllProjects = "WorkspaceAllProjects";

        $scope.TreeRoot = TreeRoot;
        $scope.Workspace = Workspace;
        $scope.Project = Project;
        $scope.WorkspaceAllProjects = WorkspaceAllProjects;

        // Variables
        $scope.contextPath = marsContext.contextPath;
        $scope.selectedNodeType = null;
        $scope.search = {};


        var blockUIStarted = false;

        function startBlockUI(msg) {
            blockUI.start(msg);
            blockUIStarted = true;
        }

        function stopBlockUI() {
            blockUI.stop();
            blockUIStarted = false;
        }

        function getObjectType(node) {
            var objectType = null;

            if ('undefined' != typeof node.objectType) {
                objectType = node.objectType;
            } else if (node.nodeId === ApplicationNodeId) {
                objectType = Application;
            } else {
                objectType = TreeRoot;
            }

            return objectType;
        }

        function generateNodeId(node) {
            var objectType = getObjectType(node);
            var nodeId = "";
            if (Workspace === objectType) {
                nodeId = "W" + node.id;
            } else if (Project === objectType) {
                nodeId = "P" + node.id;
            } else {
                nodeId = ApplicationNodeId;
            }

            return nodeId;
        }

        function getNodeImage(node) {
            var image;
            var objectType = getObjectType(node);
            if (Workspace === objectType) {
                image = "../image/W.png";
            } else if (Project === objectType) {
                image = "../image/P.png";
            } else {
                image = "../image/A.png";
            }

            return image;
        }

        $scope.treeOptions = {
            titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
            generateNodeId: generateNodeId,
            getNodeImage: getNodeImage,
            onSelectNode: function (node, selectedByClick) {
                $scope.selectedNode = node;
                $scope.selectedNodeType = getObjectType(node);
                $scope.treeOptions.selectedNode = node;
                $scope.dataValueListOrderColumn = 'label';
                $scope.transactionIds = undefined;
                $scope.permissions = [];

                if (selectedByClick) {
                    $scope.permissionMode = $scope.selectedNodeType;
                    if ($scope.selectedNodeType === Workspace || $scope.selectedNodeType === Project) {
                        getWorkspacePermission();
                    }
                }
            },
            onExpandNode: function (node) {
                $scope.expandedNode = node;
            }
        };

        function callApi(apiUrl, callback, param) {
            var api = new AjaxService(marsContext.contextPath + apiUrl);
            api.post({
                success: function (o) {
                    callback(o);
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        function getWorkspaceTree() {
            callApi('/services/data/get/fbs.workspace-GetWorkspaceProjectTree@.json',
                function (o) {
                    $scope.treeRoot = [{
                        name: "Root",
                        nodeId: "rootNode",
                        image: "../image/R.png",
                        children: o.data
                    }];

                    // $scope.treeOptions.selectedNode = {
                    //     nodeId: "rootNode"
                    // };

                    stopBlockUI();
                }, {
                    ALL: true
                });
        }

        $scope.refreshTree = function () {
            getWorkspaceTree();
        };

        (function () {
            getWorkspaceTree();
        })();

        $scope.setHeight = function () {
            setHeight();
        };

        function getWorkspacePermission() {
            callApi('/services/data/get/fbs.permission-GetMemberPermission.json',
                function (o) {
                    $scope.permissions = o.data;
                    stopBlockUI();
                }, {
                    objectId: $scope.selectedNode.id,
                    objectType: $scope.selectedNodeType,
                    ORDER_BY: "memberName"
                });
        }

        $scope.setAllWorkspaceProjectsPermission = function () {
            $scope.permissions = [];
            $scope.permissionMode = WorkspaceAllProjects;
        };

        $scope.setWorkspacePermission = function () {
            $scope.permissionMode = $scope.selectedNodeType;
            getWorkspacePermission();
        };

        function checkExistMember(obj) {
            for (var i = 0; i < $scope.permissions.length; i++) {
                var perm = $scope.permissions[i];
                if (perm.memberId === obj.memberId && perm.memberType === obj.memberType) return true;
            }

            return false;
        }

        $scope.selectUser = function (user) {
            if (user && user.originalObject) {
                var obj = user.originalObject;
                var permission = {
                    deptName: obj.deptName,
                    email: obj.email,
                    loginId: obj.loginid,
                    memberId: obj.id,
                    memberName: obj.name,
                    memberType: obj.type,
                    objectId: $scope.selectedNode.id,
                    objectType: $scope.selectedNodeType,
                    permission: ""
                };

                if (!checkExistMember(permission)) {
                    $scope.permissions.push(permission);
                    if ($scope.permissionMode !== Workspace && $scope.permissionMode !== Project) {
                        var permission2 = {};
                        angular.copy(permission, permission2);
                        permission2.permission = "A,M,R";
                        $scope.permissions.push(permission2);
                    }
                }
                $scope.$broadcast('angucomplete-alt:clearInput', 'user');
            }
        };

        $scope.selectDept = function (dept) {
            if (dept && dept.originalObject) {
                var obj = dept.originalObject;
                var permission = {
                    memberId: obj.id,
                    memberName: obj.name,
                    memberType: obj.type,
                    objectId: $scope.selectedNode.id,
                    objectType: $scope.selectedNodeType,
                    permission: ""
                };

                if (!checkExistMember(permission)) {
                    $scope.permissions.push(permission);
                    if ($scope.permissionMode !== Workspace && $scope.permissionMode !== Project) {
                        var permission2 = {};
                        angular.copy(permission, permission2);
                        permission2.permission = "A,M,R";
                        $scope.permissions.push(permission2);
                    }
                }
                $scope.$broadcast('angucomplete-alt:clearInput', 'dept');
            }
        };

        $scope.selectUserGroup = function (group) {
            if (group && group.originalObject) {
                var obj = group.originalObject;
                var permission = {
                    memberId: obj.id,
                    memberName: obj.name,
                    memberType: obj.type,
                    objectId: $scope.selectedNode.id,
                    objectType: $scope.selectedNodeType,
                    permission: ""
                };

                if (!checkExistMember(permission)) {
                    $scope.permissions.push(permission);
                    if ($scope.permissionMode !== Workspace && $scope.permissionMode !== Project) {
                        var permission2 = {};
                        angular.copy(permission, permission2);
                        permission2.permission = "A,M,R";
                        $scope.permissions.push(permission2);
                    }
                }
                $scope.$broadcast('angucomplete-alt:clearInput', 'userGroup');
            }
        };

        function addPermission(permission, type, deleteInfo) {
            var data = {
                id: $scope.selectedNode.id,
                memberId: permission.memberId,
                memberName: permission.memberName,
                memberType: permission.memberType,
                permission: type
            };
            var param = null, url = null;

            if (deleteInfo) {
                param = deleteInfo;
                param.push(data);
            } else {
                param = data;
            }
            if ($scope.selectedNodeType === TreeRoot) {
                url = '/services/data/run/fbs.permission-CreateAllWorkspacesAndProjectsPermission@.json';
            } else if ($scope.permissionMode === WorkspaceAllProjects) {
                url = '/services/data/run/fbs.permission-CreateWorkspaceAndAllProjectsPermission@.json';
            } else {
                url = '/services/data/run/fbs.' + $scope.selectedNodeType.toLocaleLowerCase() + '-CreatePermission@.json';
            }

            if (null != url) {
                callApi(url, function (o) {
                    stopBlockUI();
                    if ($scope.permissionMode === Workspace || $scope.permissionMode === Project) {
                        getWorkspacePermission();
                    }
                }, param);
            } else alert("Wrong URL");
        }

        function deletePermission(permission, type) {
            var url = null;
            if ($scope.selectedNodeType === TreeRoot) {
                url = '/services/data/run/fbs.permission-DeleteAllWorkspacesAndProjectsPermission@.json';
            } else if ($scope.permissionMode === WorkspaceAllProjects) {
                url = '/services/data/run/fbs.permission-DeleteWorkspaceAndAllProjectsPermission@.json';
            } else {
                url = '/services/data/run/fbs.' + $scope.selectedNodeType.toLocaleLowerCase() + '-DeletePermission@.json';
            }
            if (null != url) {
                callApi(url, function (o) {
                    stopBlockUI();
                    if ($scope.permissionMode === Workspace || $scope.permissionMode === Project) {
                        getWorkspacePermission();
                    }
                }, {
                    id: $scope.selectedNode.id,
                    memberId: permission.memberId,
                    memberName: permission.memberName,
                    memberType: permission.memberType,
                    permission: type
                });
            } else alert("Wrong URL");
        }

        function checkPermission(permission, type) {
            if ($scope.selectedNodeType === TreeRoot || $scope.permissionMode === WorkspaceAllProjects) {
                addPermission(permission, type);
            } else {
                var deleteUser = {
                    id: $scope.selectedNode.id,
                    memberId: permission.memberId,
                    memberName: permission.memberName,
                    memberType: permission.memberType,
                    DEDICATED_QUERY: { query: $scope.selectedNodeType.toLocaleLowerCase() + '-DeletePermission@', action: 'execute-one' }
                };
                var deleteInfo = [];

                if (type === 'R') {
                    if (permission.permission.indexOf('M') !== -1) {
                        var deleteM = {...deleteUser};
                        deleteM.permission = 'M';
                        deleteInfo.push(deleteM);
                    }
                    if (permission.permission.indexOf('A') !== -1) {
                        var deleteA = {...deleteUser};
                        deleteA.permission = 'A';
                        deleteInfo.push(deleteA);
                    }
                    addPermission(permission, type, deleteInfo);
                    permission.permission = 'R';
                } else {
                    if (permission.permission.indexOf('R') !== -1) {
                        deleteUser.permission = 'R';
                        deleteInfo.push(deleteUser);
                        addPermission(permission, type, deleteInfo);
                        permission.permission = permission.permission.replace('R,', '');
                    } else {
                        addPermission(permission, type);
                    }
                }
            }
        }

        $scope.changePermission = function (permission, type) {
            if (permission.permission.indexOf(type) === -1) {
                if (permission.permission.length > 0) {
                    permission.permission += "," + type;
                    checkPermission(permission, type);
                } else {
                    addPermission(permission, type);
                    permission.permission = type;
                }
            } else {
                deletePermission(permission, type);
                permission.permission = permission.permission.replace(type + ',', '').replace(type, '');
            }
        };
    }]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";
}
