<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AppDev Authority Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/font-bizflow/font-bizflow.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../../includes/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("authority-manager.js");
    </script>
    <style>
        tr.publication td {
            padding: 2px 5px 2px 5px;
        }

        i.permission {
            font-size: 0.45rem;
            vertical-align: top;
            padding: 0.1rem;
            cursor: default;
        }
    </style>
    <script>
        var bgColor = "<%=bgColor%>";
        var textColor = "<%=textColor%>";
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>AppDev Authority Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row flex-nowrap" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div style="padding-bottom: 0;" id="leftPanelHeader">
                    <div class="bio-left-title">
                        <div class="title"><h5>Workspaces</h5></div>
                        <div class="i-btn">
                            <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshTree();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block" id="treeContainer">
                    <div class="form-group form-group-sm col-md-12">
                        <input type="text" ng-model="treeOptions.filter.name" class="searchinput" placeholder="Enter a name to filter" style="margin-bottom: 10px;">
                        <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                    </div>
                    <div id="treePanelContainer" class="panelContainer">
                        <tree nodes='treeRoot' options="treeOptions"></tree>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary max-height col-md-12" id="rightPanelContainer">
                <div class="max-height">
                    <div class="bio-right-title">
                        <div class="title">
                            <h3>{{selectedNode.name}}</h3>
                        </div>
                        <div class="i-btn">
                            <button ng-show="permissionMode === Workspace" class="p-button p-component p-button-text-only" ng-click="setAllWorkspaceProjectsPermission();$event.stopPropagation();">Change to Current Workspace and All Projects of Current Workspace Permission Mode</button>
                            <button ng-show="permissionMode === WorkspaceAllProjects" class="p-button p-component p-button-text-only" ng-click="setWorkspacePermission();$event.stopPropagation();">Change to Current Workspace Permission Mode</button>
                        </div>
                    </div>
                    <div class="bg-info" ng-show="selectedNodeType === TreeRoot" style="padding: 5px 0 5px 100px;">
                        Search and select a user, a department or a user group to change <b>the permission of all workspaces and projects</b>.
                        <ul style="margin: 0">
                            <li>Click colored R, M, or A icon to remove a permission</li>
                            <li>Click gray R, M, or A icon to assign a permission</li>
                        </ul>
                    </div>
                    <div class="bg-info" ng-show="selectedNodeType === Workspace || selectedNodeType === Project" style="padding: 5px 0 5px 100px;">
                        Search and select a user, a department or a user group
                        <ul style="margin: 0">
                            <li>Click colored R, M, or A icon to remove a permission</li>
                            <li>Click gray R, M, or A icon to assign a permission</li>
                        </ul>
                        <i ng-show="permissionMode === Workspace">
                            Change to "All Projects of Current Workspace Permission Mode" to change the permission of all projects of current workspace as well as the permission of current workspace.
                        </i>
                        <i ng-show="permissionMode === WorkspaceAllProjects">
                            Change to "Current Workspace Permission Mode" to change the permission of current workspace only.
                        </i>
                    </div>
                    <div>
                        <div ng-show="permissionMode === Project" class="max-width badge badge-warning" style="font-size: 14px;">Current Project Permission</div>
                        <div ng-show="permissionMode === Workspace" class="max-width badge badge-warning" style="font-size: 14px;">Current Workspace Permission</div>
                        <div ng-show="permissionMode === WorkspaceAllProjects" class="max-width badge badge-warning" style="font-size: 14px;">Current Workspace and All Projects of Current Workspace Permission</div>
                        <div ng-show="selectedNodeType === TreeRoot" class="max-width badge badge-warning" style="font-size: 14px;">All Workspaces and All Projects Permission</div>
                    </div>
                    <div ng-show="selectedNodeType" class="row card-block panelContainer max-height" style="padding: 10px;">
                        <div class="col-md-4">
                            <label for="user"><h5>User</h5></label>
                            <div angucomplete-alt
                                 id="user"
                                 placeholder="Enter a user name"
                                 pause="100"
                                 selected-object="selectUser"
                                 remote-url="{{contextPath}}/services/org/user/search/appdev.json?LIKE_name_LIKE="
                                 title-field="name"
                                 search-field="name"
                                 field-required="true"
                                 initial-value=""
                                 minlength="2"
                                 input-class="form-control form-control-small"
                                 name="" required>
                            </div>
                            <div class="p-listbox-list-wrapper border-secondary" style="padding-top: 20px;">
                                <ul class="p-listbox-list">
                                    <li class="p-listbox-item" tabindex="0" ng-repeat="item in permissions | filter:{memberType: 'U'} | orderBy: 'memberName'">
                                        <div class="p-clearfix position-relative" style="cursor: default;">
                                            <div class="bf-noBr" style="width: calc(100% - 65px);">{{item.memberName}}</div>
                                            <div class="position-absolute bf-text-theme" style="top: 0px; right: 0px;">
                                                <i class="ml-1 p-1 bf-icon-r p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-info': -1 != item.permission.indexOf('R')}" ng-click="changePermission(item, 'R')"></i>
                                                <i class="ml-1 p-1 bf-icon-m p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-warning': -1 != item.permission.indexOf('M')}" ng-click="changePermission(item, 'M')"></i>
                                                <i class="ml-1 p-1 bf-icon-a p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-violet': -1 != item.permission.indexOf('A')}" ng-click="changePermission(item, 'A')"></i>
                                                <span class="d-none">
                                                    <i class="bf-division-line" style="height: 1rem;"></i><i class="p-button p-button-secondary pi pi-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="user"><h5>Department</h5></label>
                            <div angucomplete-alt
                                 id="dept"
                                 placeholder="Enter a department name"
                                 pause="100"
                                 selected-object="selectDept"
                                 remote-url="{{contextPath}}/services/org/department/search.json?LIKE_name_LIKE="
                                 title-field="name"
                                 search-field="name"
                                 field-required="true"
                                 initial-value=""
                                 minlength="2"
                                 input-class="form-control form-control-small"
                                 name="" required>
                            </div>
                            <div class="p-listbox-list-wrapper border-secondary" style="padding-top: 20px;">
                                <ul class="p-listbox-list">
                                    <li class="p-listbox-item" tabindex="0" ng-repeat="item in permissions | filter:{memberType: 'D'} | orderBy: 'memberName'">
                                        <div class="p-clearfix position-relative" style="cursor: default;">
                                            <div class="bf-noBr" style="width: calc(100% - 65px);">{{item.memberName}}</div>
                                            <div class="position-absolute bf-text-theme" style="top: 0px; right: 0px;">
                                                <i class="ml-1 p-1 bf-icon-r p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-info': -1 != item.permission.indexOf('R')}" ng-click="changePermission(item, 'R')"></i>
                                                <i class="ml-1 p-1 bf-icon-m p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-warning': -1 != item.permission.indexOf('M')}" ng-click="changePermission(item, 'M')"></i>
                                                <i class="ml-1 p-1 bf-icon-a p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-violet': -1 != item.permission.indexOf('A')}" ng-click="changePermission(item, 'A')"></i>
                                                <span class="d-none">
                                                    <i class="bf-division-line" style="height: 1rem;"></i><i class="p-button p-button-secondary pi pi-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="user"><h5>User Group</h5></label>
                            <div angucomplete-alt
                                 id="userGroup"
                                 placeholder="Enter a user group name"
                                 pause="100"
                                 selected-object="selectUserGroup"
                                 remote-url="{{contextPath}}/services/org/group/search.json?LIKE_name_LIKE="
                                 title-field="name"
                                 search-field="name"
                                 field-required="true"
                                 initial-value=""
                                 minlength="2"
                                 input-class="form-control form-control-small"
                                 name="" required>
                            </div>
                            <div class="p-listbox-list-wrapper border-secondary" style="padding-top: 20px;">
                                <ul class="p-listbox-list">
                                    <li class="p-listbox-item" tabindex="0" ng-repeat="item in permissions | filter:{memberType: 'G'} | orderBy: 'memberName'">
                                        <div class="p-clearfix position-relative" style="cursor: default;">
                                            <div class="bf-noBr" style="width: calc(100% - 65px);">{{item.memberName}}</div>
                                            <div class="position-absolute bf-text-theme" style="top: 0px; right: 0px;">
                                                <i class="ml-1 p-1 bf-icon-r p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-info': -1 != item.permission.indexOf('R')}" ng-click="changePermission(item, 'R')"></i>
                                                <i class="ml-1 p-1 bf-icon-m p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-warning': -1 != item.permission.indexOf('M')}" ng-click="changePermission(item, 'M')"></i>
                                                <i class="ml-1 p-1 bf-icon-a p-button p-button-rounded p-button-secondary permission border-0" ng-class="{'p-button-violet': -1 != item.permission.indexOf('A')}" ng-click="changePermission(item, 'A')"></i>
                                                <span class="d-none">
                                                    <i class="bf-division-line" style="height: 1rem;"></i><i class="p-button p-button-secondary pi pi-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form name="downloadFile" target="frame" method="POST">
    <input type="hidden" name="param">
</form>
<iframe id="frame" name="frame" src="" style="width: 100%;height: 95%;border: 0;padding: 0;margin: 0;display: none;" frameborder="0"></iframe>
</body>
</html>