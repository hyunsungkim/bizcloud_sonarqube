var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'blockUI'
    , 'ngSanitize'
    , 'ui.select'
]).controller('angularAppCtrl', ['$scope', '$filter', '$timeout', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $filter, $timeout, $uibModal, blockUI, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);
    var _channelName = sanitizeMessage(parameter.getParameterValue("c"));
    var ANTEROOM = '@ANTEROOM';
    var NOTIFICATION_ROOM = '@NOTIFICATION';

    var defaultColor = $('#userTitle').css("color");
    $scope.websocket = {};
    $scope.connected = false;
    $scope.channelName = null;
    $scope.NOTIFICATION_ROOM = NOTIFICATION_ROOM;

    if ("undefined" != typeof _channelName) {
        $scope.websocket.channel = _channelName;
        $scope.singleMode = true;
    }

    var MessageType = {
        Register: "Register",
        Connection: "Connection",
        Disconnection: "Disconnection",
        Broadcast: "Broadcast",
        Notification: "Notification",
        System: "System",
        Alert: "Alert",
        Chat: "Chat"
    };

    var maxHistory = 200;
    var protocol = location.protocol === "https:" ? "wss:" : "ws:";
    var hostPort = location.host;
    var socket = atmosphere;
    var channel = null;
    var transport = 'websocket';
    var fallbackTransport = 'long-polling';

    var content = $('#content');
    var input = $('#input');

    function cleanMessage() {
        if (content.children().length > maxHistory) {
            $('#content div:first').remove();
        }
    }

    function getTimeString(datetime) {
        return (datetime.getHours() < 10 ? '0' + datetime.getHours() : datetime.getHours()) + ':' + (datetime.getMinutes() < 10 ? '0' + datetime.getMinutes() : datetime.getMinutes());
    }

    function sanitizeMessage(message) {
        return message ? message.replace(/</g, "&lt;") : message;
    }

    function showMessage(sender, message, datetime, type) {
        if (parent && parent.$B && parent.$B.ExceptionHandler) {
            var msg = sender + ' @ ' + getTimeString(datetime) + ": " + sanitizeMessage(message);

            if (type === MessageType.Broadcast) {
                parent.$B.ExceptionHandler.pin('Alert', msg);
            } else {
                parent.$B.ExceptionHandler.toast('Info', msg);
            }
        }
    }

    function addMessage(sender, message, color, datetime, type, showMsg) {
        content.append('<div><span style="color:' + color + '">' + sender + '</span> @ ' + getTimeString(datetime) + ': ' + sanitizeMessage(message) + '</div>');
        cleanMessage();
        gotoBottom();
        if (showMsg) {
            showMessage(sender, message, datetime, type);
        }
    }

    function addSystemMessage(type, message, datetime, color) {
        message = '###### ' + message + ' @ ' + getTimeString(datetime) + ' ######';
        color = color || '#17a2b8';
        content.append('<div style="color:' + color + '">' + message + '</div>');
        gotoBottom();
    }

    function addCommentMessage(message, color) {
        color = color || '#b85900';
        content.append('<div style="color:' + color + '">' + message + '</div>');
        gotoBottom();
    }

    function getUserInfo(user) {
        return user ? user.name + " (" + user.loginId + ")" : "Anonymous";
    }

    function disconnect() {
        try {
            if (channel) {
                if ($scope.websocket.myInfo) {
                    addSystemMessage(MessageType.Disconnection, getUserInfo($scope.websocket.myInfo) + " left " + ($scope.channelName || $scope.websocket.myChannel.name), new Date());
                }
                socket.unsubscribe();
                channel = null;
                $scope.connected = false;
                $scope.websocket.myChannel = null;
            }
        } catch (e) {
            console.error("Error on disconnecting", e);
        }
    }

    function makeUserList(myChannel) {
        var userList = {};
        if (myChannel && myChannel.users) {
            for (var key in myChannel.users) {
                var user = myChannel.users[key];
                var listUser = userList[user.id];
                if (listUser) {
                    listUser.connectionCount = listUser.connectionCount + 1;
                } else {
                    user.connectionCount = 1;
                    userList[user.id] = user;
                }
            }
        }

        return userList;
    }

    function isValidConnection() {
        return 'undefined' !== typeof $scope.websocket.myInfo;
    }

    function connect(channelName) {
        var request = {
            url: marsContext.contextPath + '/msg/ws/' + channelName,
            webSocketUrl: protocol + "//" + hostPort + marsContext.contextPath + '/msg/ws/' + channelName,
            contentType: "application/json;charset=utf-8",
            logLevel: 'warn',
            transport: transport,
            trackMessageLength: true,
            reconnectInterval: 5000,
            fallbackTransport: fallbackTransport
        };

        request.onOpen = function (response) {
            if (channelName) addSystemMessage(MessageType.System, 'Channel connected using ' + response.transport, new Date(), '#6c757d');
            transport = response.transport;
        };

        request.onReopen = function (response) {
            if (channelName) addSystemMessage(MessageType.System, 'Channel re-connected using ' + response.transport, new Date(), '#6c757d');
        };

        request.onMessage = function (response) {
            try {
                var message = JSON.parse(response.responseBody);
                message.time = new Date(parseInt(message.time || new Date().getTime()));

                if (message.messageType === MessageType.Register) {
                    if (message.myInfo) {
                        $scope.websocket.myInfo = message.myInfo;
                        // console.log(channelName + ".myInfo", $scope.websocket.myInfo);
                    } else {
                        $timeout(function () {
                            reconnect(channelName);
                        }, 50);
                    }
                } else if (message.messageType === MessageType.Connection) {
                    if (message.channels) {
                        $scope.websocket.channels = message.channels;
                    }
                    if (message.myChannel) {
                        $scope.websocket.myChannel = message.myChannel;
                        $scope.websocket.myChannel.uniqueUsers = makeUserList($scope.websocket.myChannel);
                        $scope.$apply();
                    } else {
                        $timeout(function () {
                            reconnect(channelName);
                        }, 50);
                    }
                    if (message.myChannel) {
                        addSystemMessage(MessageType.Connection, getUserInfo(message.sender) + " joined " + channelName, message.time);
                        if (typeof welcomeMessage != "undefined") {
                            addCommentMessage(welcomeMessage);
                        }
                    }
                } else if (message.messageType === MessageType.Disconnection) {
                    addSystemMessage(MessageType.Disconnection, getUserInfo(message.sender) + " left " + channelName, message.time);
                } else if (message.messageType === MessageType.Notification) {
                    if (isValidConnection()) {
                        if (parent["BizFormPrimeReactAppTool"] && parent["BizFormPrimeReactAppTool"].SocketListenerHandlerService) {

                            // console.log("sendByMe=" + (message.sender.userAgentId === parent.BizFormCore.DataStorage.uniqueAppName  && message.sender.id === $scope.websocket.myInfo.id),
                            //     ", message.sender.userAgentId=" + message.sender.userAgentId,
                            //     ", parent.BizFormCore.DataStorage.uniqueAppName =" + parent.BizFormCore.DataStorage.uniqueAppName ,
                            //     ", {sender, myInfo} ==> ", message.sender, $scope.websocket.myInfo);

                            parent["BizFormPrimeReactAppTool"].SocketListenerHandlerService.message(
                                channelName,
                                message,
                                message.sender.userAgentId === parent.BizFormCore.DataStorage.uniqueAppName && message.sender.id === $scope.websocket.myInfo.id,
                                $scope.websocket.myInfo,
                                $scope.websocket.myChannel);
                        }
                        if (message.sourceChannel === channelName) {
                            var msg = message.message || "Event: " + message.event + " by " + getUserInfo(message.sender);
                            addSystemMessage(MessageType.Notification, msg, message.time, "#ffc107");
                        }
                    } else {
                        reconnect(channelName);
                    }
                } else if (message.messageType === MessageType.System) {
                    if (parent && parent.$B && parent.$B.ExceptionHandler) {
                        parent.$B.ExceptionHandler.pin('Warning', 'System @ ' + getTimeString(message.time) + ": " + message.message);
                    } else {
                        alert(message.message);
                    }
                } else if (message.messageType === MessageType.Alert) {
                    if (parent && parent.$B && parent.$B.ExceptionHandler) {
                        parent.$B.ExceptionHandler.alert('Alert', getUserInfo(message.sender) + ' @ ' + getTimeString(message.time) + ": " + message.message);
                    } else {
                        alert(getUserInfo(message.sender) + ": " + message.message);
                    }
                } else if (message.message) {
                    if (isValidConnection()) {
                        if ($scope.channelName !== NOTIFICATION_ROOM) {
                            var showMsg = true;
                            var color = defaultColor;
                            if (message.sender && message.sender.webSocketUserId) {
                                color = message.sender.webSocketUserId === $scope.websocket.myInfo.webSocketUserId ? "#28a745" : defaultColor;
                                showMsg = message.sender.webSocketUserId !== $scope.websocket.myInfo.webSocketUserId;
                            }
                            addMessage(getUserInfo(message.sender), message.message, color, message.time, message.messageType, showMsg);
                        }
                    } else {
                        reconnect(channelName);
                    }
                }
            } catch (e) {
                console.error(e);
            }
        };

        request.onClose = function (response) {
            if (channel && $scope.websocket.me) {
                var msg = $scope.websocket.me.name + " (" + $scope.websocket.me.loginId + ") left " + channelName;
                channel.push(JSON.stringify({message: msg}));
            }
        };

        request.onError = function (response) {
            if (channelName) addSystemMessage(MessageType.System, 'Sorry, but there\'s some problem with your socket or the server is down', new Date(), '#dc3545');
            console.error("onError", response);
        };

        request.onReconnect = function (request, response) {
            if (channelName) addSystemMessage(MessageType.System, 'Connection lost, trying to reconnect. Trying to reconnect ' + request.reconnectInterval, new Date(), '#fd7e14');
            // console.log("onReconnect", request, response);
        };

        channel = socket.subscribe(request);
        $scope.connected = true;
        $scope.channelName = channelName;
        input.removeAttr('disabled').focus();
    }

    input.keydown(function (e) {
        if (e.keyCode === 13) {
            var msg = $(this).val();
            $(this).val('');
            input.removeAttr('disabled').focus();
            channel.push(msg);
        }
    });

    function reconnect(channelName) {
        // console.log("reconnecting...", channelName);
        disconnect();
        connect(channelName);
    }

    $scope.joinChannel = function () {
        disconnect();
        connect($scope.websocket.channel);
    };

    $scope.disconnectChannel = function () {
        disconnect();
    };

    (function () {
        if ($scope.websocket.channel) {
            connect($scope.websocket.channel);
        } else {
            connect(ANTEROOM);
        }
    })();

    $scope.setHeight = function () {
        setHeight();
    };

}]);

function onUnload() {
    var scope = angularExt.getAngularScope("body");
    scope.disconnectChannel();
}

function gotoBottom() {
    mars$util$.gotoScrollBottom("content");
    mars$util$.gotoScrollBottom("contentBox");
}

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    if (panelContainer) {
        var height = clientSize.height - panelContainer.offsetTop;
        var rightContainer = document.getElementById("rightPanelContainer");
        if (rightContainer) rightContainer.style.height = height + "px";
        var leftContainer = document.getElementById("leftPanelContainer");
        if (leftContainer) leftContainer.style.height = rightContainer.style.height;
        gotoBottom();
    }
}