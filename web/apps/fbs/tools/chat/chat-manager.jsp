<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<%
    boolean hideLeftPanel = ServletUtil.getParameterBooleanValue(request, "l", false);
    String welcomeMessage = request.getParameter("m");
%>
<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Chat Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/fontawesome-free/css/all.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/ui-select/dist/select.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/ui-select/dist/select2.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../../includes/node_modules/ui-select/dist/select.min.js"></script>
    <script src="../../../../includes/node_modules-ext/websocket/atmosphere-min.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("chat-manager.js");
    </script>
    <style>
        ::-webkit-scrollbar {
            width: 0.6rem;
            height: 0.6rem;
        }

        ::-webkit-scrollbar-thumb {
            background: rgba(127, 130, 140, 0.6);
        }

        ::-webkit-scrollbar-corner {
            background: rgba(127, 130, 140, 0.6);
        }

        .select2-container .select2-choice {
            height: auto;
        }

        a:not([href]):not([tabindex]) {
            color: black;
        }
    </style>
    <script>
        <%if(null != welcomeMessage) {%>
        var welcomeMessage = "<%=Encode.forJavaScriptBlock(welcomeMessage)%>";
        <%}%>
    </script>

</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>" onbeforeunload="onUnload();">
<div ng-show="channelName !== NOTIFICATION_ROOM">
    <%if (showTitle) {%>
    <div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
        <h1>Chat Management</h1>
    </div>
    <%}%>

    <div class="container-fluid" id="baseContainer">
        <div class="row" id="panelContainer">
            <%if (!hideLeftPanel) {%>
            <div class="col-sm-3" style="padding:0 1px 0 0">
                <div class="card text-<%=textColor%> bg-<%=bgColor%>" id="leftPanelContainer">
                    <div class="card-header card-header-sm" id="leftPanelHeader" ng-if="!singleMode">
                        <table width="100%">
                            <tr>
                                <td>
                                    <h3>Channels</h3>
                                </td>
                                <td class="text-right" style="padding-ddttom: 5px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-block panelContainer" id="treeContainer">
                        <div class="form-group form-group-sm" ng-if="!singleMode">
                            <ui-select id="channelName" ng-model="websocket.channel" title="Choose a channel" theme="select2" class="max-width">
                                <ui-select-match placeholder="Select a channel in the list or search channel...">{{$select.selected}}</ui-select-match>
                                <ui-select-choices repeat="item in websocket.channels | filter: $select.search">
                                    <div ng-bind-html="item | highlight: $select.search"></div>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div ng-if="!singleMode">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <input class="form-control form-control-xs" placeholder="Enter a room name" ng-model="websocket.channel" style="width:100%;" ng-enter="joinChannel()">
                                    </td>
                                    <td style="width: 40px;">
                                        <button class="btn btn-primary btn-xs" ng-disabled="!websocket.channel" ng-click="joinChannel();">Enter</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div ng-show="connected">
                            <%if (showTitle) {%>
                            <div class="card-header card-header-sm">
                                <h3 id="userTitle">Users</h3>
                            </div>
                            <%}%>
                            <div style="padding: 10px;" ng-show="websocket.myChannel">
                                <div ng-repeat="(key, user) in websocket.myChannel.uniqueUsers">
                                    <i class="fas fa-user" aria-hidden="true"></i>&nbsp;<span>{{user.name}} ({{user.loginId}})</span><span ng-if="user.connectionCount > 1" style="font-size: smaller"> [{{user.connectionCount}}]</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%}%>
            <div class='col-sm-<%=hideLeftPanel ? "12" : "9"%>' style="padding: 0 0 0 1px;" id="rightPanelContainer">
                <div class="card text-<%=textColor%> bg-<%=bgColor%>" style="height: 100%;">
                    <%if (showTitle) {%>
                    <div class="card-header card-header-sm" style="padding-bottom: 0;" id="rightPanelHeader">
                        <table width="100%">
                            <tr>
                                <td nowrap>
                                    <h3>{{websocket.myChannel.name}}</h3>
                                </td>
                                <td style="text-align: right;">
                                    <button class="btn btn-secondary btn-xs" ng-show="connected" ng-click="disconnectChannel();">Log Out</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%}%>
                    <div class="card-block panelContainer" style="width: 100%; height: 100%; overflow: auto;" id="contentBox">
                        <table cellpadding="0" cellspacing="0" style="width:100%;height: 100%;">
                            <tr>
                                <td style="vertical-align: top;">
                                    <div id="content" style="height: 100%;overflow: auto;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px;">
                                    <input ng-show="connected" placeholder="Enter a message" id="input" style="width: 100%;"></input>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>