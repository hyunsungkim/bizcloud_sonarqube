<%@ page import="com.bizflow.io.core.json.JSONArray" %>
<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.bizflow.io.core.util.NumberUtil" %>
<%@ page import="com.bizflow.io.core.web.MultipartFile" %>
<%@ page import="com.bizflow.io.core.web.MultipartFormData" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.service.AdhocService" %>
<%@ page import="com.bizflow.io.services.core.util.CipherFileUtil" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlTransactionDAOManager" %>
<%@ page import="com.bizflow.io.services.data.dao.util.SqlTransactionDAOUtil" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Excel - Importing</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
    </script>
</head>
<body class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="container" style="margin-top: 15%; auto;">
    <div class="card text-white bg-dark border-secondary">
        <div class="card-header bg-info">Importing Lookup Data</div>
        <div class="progress">
            <div id="progress" class="progress-bar progress-bar-striped progress-bar-animated"
                 role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;">0
            </div>
        </div>
        <div id="alertSuccess" class="alert alert-success" role="alert" style="display: none;margin:0;">All lookup data have been imported. It took <span id="howLong"></span>ms.</div>
    </div>

    <%
        Date date_s = new Date();
        String errorMessage = null;
        AdhocService.getInstance().initializeRequest(request, response);

        MultipartFormData formData = new MultipartFormData(request);
        MultipartFile multipartFile = formData.getFile("file");
        String filename = multipartFile.getName();
        File file = multipartFile.getFile();
        if (file.exists()) {
            String contents = CipherFileUtil.getFileContents(file, filename);
            JSONObject jsonObject = new JSONObject(contents);
            JSONObject exportObject = jsonObject.optJSONObject("exportObject");
            if (null != exportObject) {
                if ("LookupData".equals(exportObject.getString("exportObjectType"))) {
                    int currentIndex = 0;
                    JSONObject lookupJson = null;
                    final String sessionName = "fbs";
                    SqlTransactionDAOManager sqlDaoManager = new SqlTransactionDAOManager(sessionName);

                    try {
                        Map param = new HashMap();
                        param.put("moduleName", "ImportLookupData");
                        param.put("action", "I");
                        SqlTransactionDAOUtil.runQuery(sqlDaoManager, sessionName, "app.CreateTransactionLog", param);

                        long transactionId = NumberUtil.parseLong(param.get("transactionId"), -1);
                        final JspWriter writer = out;

                        JSONObject iepActionLog = jsonObject.getJSONObject("iepActionLog");
                        param = iepActionLog.toMap();
                        param.put("transactionId", transactionId);
                        SqlTransactionDAOUtil.runQuery(sqlDaoManager, sessionName, "app.InsertOrUpdateIEPActionLog", param);

                        Map param2 = new HashMap();
                        param2.put("originalId", iepActionLog.getString("id"));
                        param2.put("objectType", "LookupData");
                        param2.put("iepAction", "I");
                        param2.put("transactionId", transactionId);
                        SqlTransactionDAOUtil.runQuery(sqlDaoManager, sessionName, "app.CreateIEPActionLog", param2);

                        JSONArray lookupDataArray = exportObject.getJSONArray("lookupData");
                        int oldPercent = 0;
                        int totalNumber = lookupDataArray.length();
                        for (int i = 0; i < totalNumber; i++) {
                            currentIndex = i + 1;
                            lookupJson = lookupDataArray.getJSONObject(i);
                            Map param3 = lookupJson.toMap();
                            param3.put("transactionId", transactionId);
                            SqlTransactionDAOUtil.runQuery(sqlDaoManager, sessionName, "lookup-InsertOrUpdateLookupData@", param3);
                            int percent = ((currentIndex * 100) / totalNumber);
                            if (oldPercent != percent) {
                                oldPercent = percent;
                                try {
                                    writer.print("<script>");
                                    writer.print("$('#progress').attr('aria-valuenow', ");
                                    writer.print(percent);
                                    writer.print(").css('width', '");
                                    writer.print(percent);
                                    writer.print("%').html('");
                                    writer.print(percent);
                                    writer.print("%');");
                                    writer.println("</script>");
                                    writer.flush();
                                } catch (IOException e) {
                                }
                            }
                        }
                        sqlDaoManager.commitAllSessions();
                    } catch (Throwable e) {
                        sqlDaoManager.rollbackAllSessions();
                        StringBuilder builder = new StringBuilder();
                        builder.append("CurrentIndex=").append(currentIndex).append("\n");
                        builder.append("LookupData=").append(null != lookupJson ? lookupJson.toString() : "{}").append("\n");
                        builder.append(ExceptionUtil.getOriginalThrowable(e).getMessage());
                        errorMessage = builder.toString();
                    } finally {
                        sqlDaoManager.closeAllSessions();
                    }
                } else {
                    errorMessage = "The file is not lookup data export file";
                }
            } else {
                errorMessage = "The file is not export file";
            }
        } else {
            errorMessage = "File does not exist";
        }
    %>

    <%if (null != errorMessage) {%>
    <div id="alertError" class="alert alert-sm alert-danger" role="alert" style="max-height: 300px; overflow: auto;font-size: 12px;">
        <%=HtmlUtil.getHtmlText(errorMessage).replaceAll("\n", "<br>")%>
    </div>
    <%} else {%>
    <script>
        $("#howLong").html(<%=System.currentTimeMillis() - date_s.getTime()%>);
        setTimeout(function () {
            $("#alertSuccess").css("display", "");
        }, 50);
    </script>
    <%}%>

    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-sm btn-secondary" onclick="parent.progressiveRunDoneOk();">Ok</button>
    </div>
</div>
<br><br><br>
</body>
</html>