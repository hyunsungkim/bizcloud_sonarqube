<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AppDev Migration Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../../includes/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../../includes/node_modules/angular-file-upload/dist/angular-file-upload.min.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("migration-manager.js");
    </script>
    <script>
        var textColor = "<%=textColor%>";
        var bgColor = "<%=bgColor%>";
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%> overflow-hidden">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>AppDev Migration Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row flex-nowrap" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div style="padding-bottom: 0;" id="leftPanelHeader">
                    <div class="bio-left-title">
                        <div class="title"><h5>Projects</h5></div>
                        <div class="i-btn">
                            <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshTree();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block" id="treeContainer">
                    <div class="form-group form-group-sm col-md-12">
                        <input type="text" ng-model="treeOptions.filter.name" class="searchinput" placeholder="Enter a name to filter" style="margin-bottom: 10px;">
                        <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                    </div>
                    <div id="treePanelContainer" class="panelContainer">
                        <tree nodes='treeRoot' options="treeOptions"></tree>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary col-md-12" id="rightPanelContainer" style="overflow-y: auto">
                <div id="actionPanel">
                    <div id="rightPanelHeader" ng-show="selectedNodeType">
                        <div class="bio-right-title">
                            <div class="title">
                                <h3>
                                    {{selectedNode.name}}
                                    <span class="id-wrap" ng-if="selectedNode.id">ID&nbsp;:&nbsp;{{selectedNode.id}}</span>
                                </h3>
                            </div>
                            <div class="i-btn">
                                <div class="space"></div>
                                <button class="p-button p-component p-button-warning p-button-text-only" ng-if="selectedNodeType == Workspace" ng-click="moveWorkspaceResources();$event.stopPropagation();">Move Resources</button>
                                <button class="p-button p-component p-button-info p-button-text-only" ng-if="selectedNodeType == TreeRoot || selectedNodeType == Project" ng-click="showMigrationHistory();$event.stopPropagation();">History</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-block panelContainer" style="overflow-x: hidden;margin-top: 20%;" ng-show="!selectedNodeType">
                        <div class="row justify-content-md-center" style="padding:10px 0 10px 0;">
                            <h5>
                                <ul>
                                    <li>Click Root to import a project or project publication.</li>
                                    <li>Click Root to export global Lookup Data.</li>
                                    <li>Click a workspace to copy a project.</li>
                                    <li>Click a project to export a project, the data model of a project, or project publications.</li>
                                    <li>Click a project to import the data model of a project.</li>
                                </ul>
                            </h5>
                        </div>
                    </div>
                    <div ng-show="selectedNodeType == TreeRoot">
                        <div class="card bg-info">
                            <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#importOnRoot" aria-expanded="false" aria-controls="#importOnRoot">
                                <h5 class="card-title pd5">
                                    Import
                                </h5>
                            </div>
                            <div class="card-block collapse show text-<%=textColor%> bg-<%=bgColor%>" id="importOnRoot" style="padding-bottom: 20px;">
                                <form method="POST" target="frame" name="ProjectForm" id="ProjectForm" action="../blank.html" enctype="multipart/form-data">
                                    <div class="row justify-content-md-center">
                                        <div class="form-group" style="padding: 5px;">
                                            <label for="file2">
                                                <h6>Please choose a file to import</h6>
                                            </label>
                                            <ul>
                                                <li>A project</li>
                                                <li>A project publication</li>
                                                <li>lookup data</li>
                                                <li>files</li>
                                            </ul>
                                            <input type="file" name="file" id="file2" required ng-model="file.project">
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="importProject();$event.stopPropagation();">Import Project</button>
                                        <div style="padding-right: 10px"></div>
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="importProjectPublication();$event.stopPropagation();">Import Project Publication</button>
                                        <div style="padding-right: 10px"></div>
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="importProjectFromPublication();$event.stopPropagation();">Import Project from Publication</button>
                                        <div style="padding-right: 10px"></div>
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="importLookupData();$event.stopPropagation();">Import Lookup Data</button>
                                        <div style="padding-right: 10px"></div>
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="importFile();$event.stopPropagation();">Import File</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div style="padding: 10px;"></div>
                        <div class="card bg-primary">
                            <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#exportOnRoot" aria-expanded="false" aria-controls="#exportOnRoot">
                                <h5 class="card-title pd5">
                                    Export
                                </h5>
                            </div>
                            <div class="card-block collapse show text-<%=textColor%> bg-<%=bgColor%>" id="exportOnRoot" style="padding-bottom: 20px;">
                                <div class="row justify-content-md-center" style="padding: 5px;">
                                    <h6>Please click one of buttons below to export</h6>
                                </div>
                                <div class="row justify-content-md-center">
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportLookupData();$event.stopPropagation();">Export Root Lookup Data</button>
                                    <div style="padding-right: 10px"></div>
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportFile();$event.stopPropagation();">Export Root File</button>
                                </div>
                                <%
                                    if ("true".equals(request.getParameter("j"))) {
                                %>
                                <div class="row justify-content-md-center" style="padding-top: 20px;">
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportLookupData('.json');$event.stopPropagation();">Export Root Lookup Data in JSON</button>
                                    <div style="padding-right: 10px"></div>
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportFile('.json');$event.stopPropagation();">Export Root File in JSON</button>
                                </div>
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <div ng-show="selectedNodeType == Workspace">
                        <div class="card bg-info">
                            <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#copyOnWorkspace" aria-expanded="false" aria-controls="#copyOnWorkspace">
                                <h5 class="card-title pd5">
                                    Copy
                                </h5>
                            </div>
                            <div class="card-block collapse show text-<%=textColor%> bg-<%=bgColor%>" id="copyOnWorkspace" style="padding: 20px;">
                                <form method="POST" target="frame" name="CopyProjectForm" id="CopyProjectForm" action="../blank.html" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <div>
                                            <h6>Please select a project to be copied.</h6>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-md-3 col-form-label text-nowrap">Source Project<span class="mandatory">*</span></label>
                                            <div class="col-md-9">
                                                <select name="source" ng-model="param.sourceId" class="form-control form-control-sm">
                                                    <option ng-repeat="item in selectedNode.children" value="{{item.id}}">{{item.name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-md-3 col-form-label">Name<span class="mandatory">*</span></label>
                                            <div class="col-md-9">
                                                <input type="text" ng-model="param.name" name="name" class="form-control form-control-sm" id="inputName" placeholder="Enter New Project Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputDesc" class="col-md-3 col-form-label text-nowrap">Description<span class="mandatory">*</span></label>
                                            <div class="col-md-9">
                                                <textarea type="text" ng-model="param.description" name="description" class="form-control form-control-sm" id="inputDesc" placeholder="Enter New Project Description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="copyProject(selectedNode.id);$event.stopPropagation();" ng-disabled="!param.sourceId || !param.name || !param.description">Copy Project</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div ng-show="selectedNodeType == Project">
                        <div class="card bg-info">
                            <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#importOnProject" aria-expanded="false" aria-controls="#importOnProject">
                                <h5 class="card-title pd5">
                                    Import
                                </h5>
                            </div>
                            <div class="card-block collapse show text-<%=textColor%> bg-<%=bgColor%>" id="importOnProject" style="padding-bottom: 20px;">
                                <form method="POST" target="frame" name="ModelForm" id="ModelForm" action="../blank.html" enctype="multipart/form-data">
                                    <div class="row justify-content-md-center">
                                        <div class="form-group" style="padding: 5px;">
                                            <label for="file1">
                                                <h6>Please choose a file to import</h6>
                                            </label>
                                            <ul>
                                                <li>A project data model</li>
                                            </ul>
                                            <input type="file" name="file" id="file1" required ng-model="file.model">
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <button type="button" class="p-button p-component p-button-secondary p-button-text-only" ng-click="importDataModel();$event.stopPropagation();">Import Data Model</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div style="padding: 10px;"></div>
                        <div class="card bg-primary">
                            <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#exportOnProject" aria-expanded="false" aria-controls="#exportOnProject">
                                <h5 class="card-title pd5">
                                    Export Project
                                </h5>
                            </div>
                            <div class="card-block collapse show text-<%=textColor%> bg-<%=bgColor%>" id="exportOnProject" style="padding-bottom: 20px;">
                                <div class="row justify-content-md-center" style="padding: 5px;">
                                    <h6>Please click one of buttons below to export</h6>
                                </div>
                                <div class="row justify-content-md-center">
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportDataModel();$event.stopPropagation();">Export Data Model</button>
                                    <div style="padding-right: 10px"></div>
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportProject();$event.stopPropagation();">Export Project</button>
                                </div>
                                <%
                                    if ("true".equals(request.getParameter("j"))) {
                                %>
                                <div class="row justify-content-md-center" style="padding-top: 20px;">
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportDataModel('.json');$event.stopPropagation();">Export Data Model in JSON</button>
                                    <div style="padding-right: 10px"></div>
                                    <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportProject('.json');$event.stopPropagation();">Export Project in JSON</button>
                                </div>
                                <%}%>
                            </div>
                        </div>
                        <div style="padding: 10px;"></div>
                        <div class="card bg-success">
                            <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#expoertOnPublication" aria-expanded="false" aria-controls="#expoertOnPublication">
                                <h5 class="card-title pd5">
                                    Export Project Publication
                                </h5>
                            </div>
                            <div class="card-block collapse show text-<%=textColor%> bg-<%=bgColor%> col-md-12" id="expoertOnPublication" style="padding-bottom: 20px;">
                                <div>
                                    <div class="card-header">
                                        <div class="row justify-content-md-center">
                                            <form class="form-inline">
                                                <label for="searchTitle" style="padding: 0 5px 0 2px;">Tag</label>
                                                <input type="text" class="form-control form-control-sm" id="searchTitle" ng-enter="searchPublication()" ng-model="search.LIKE_tag_LIKE">
                                                <div style="padding-left: 10px;"></div>
                                                <label for="searchName" style="padding: 0 5px 0 2px;">Transaction ID</label>
                                                <input type="text" class="form-control form-control-sm" id="searchName" ng-enter="searchPublication()" ng-model="search.transactionId">
                                                <div style="padding-left: 10px;"></div>
                                                <div style="padding-left: 10px;"></div>
                                                <button type="button" class="bf-button p-component bf-button-icon-only" title="Search" ng-click="searchPublication()"><i class="fa fa-search"></i></button>
                                                <div style="padding-left: 10px;"></div>
                                                <button type="button" class="bf-button p-component bf-button-icon-only" title="Refresh" ng-if="selectedNode" ng-click="refreshPublicationList();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                                                <div style="padding-left: 10px;"></div>
                                                <button type="button" class="p-button p-component p-button-secondary p-button-text-only" title="RecoveryHistory" ng-if="selectedNode" ng-click="showPublicationHistory();$event.stopPropagation();">History & Undelete</button>
                                            </form>
                                        </div>
                                        <div class="row" style="padding-top: 5px;">
                                            <div class="col-md-2 text-right" style="padding-right: 0;">
                                                <label for="isfi" style="padding: 5px 5px 0 0;">ISFI</label>
                                            </div>
                                            <div class="col-md-8" style="padding-left: 0;">
                                                <input type="text" class="form-control form-control-sm max-width" id="isfi" ng-enter="searchPublication()" ng-model="search.isfi">
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-block panelContainer" style="overflow-x: hidden;">
                                        <div ng-if="publicationList.data.length == 0">
                                            <div class="text-center">There are no publications</div>
                                        </div>
                                        <div class="bf-table-2">
                                            <form method="POST" target="frame" name="RecoveryProjectForm" id="RecoveryProjectForm" action="../blank.html">
                                            </form>
                                            <table class="table table-striped table-condensed table-sm text-<%=textColor%>" ng-if="publicationList.data.length > 0">
                                                <thead>
                                                <tr style="font-weight: bold">
                                                    <th>#</th>
                                                    <th ng-click="sortPublicationList('userName')"><a>Publisher</a></th>
                                                    <th ng-click="sortPublicationList('actionDate')"><a>Publish Date</a></th>
                                                    <th ng-click="sortPublicationList('tag')"><a>Tag</a></th>
                                                    <th ng-click="sortPublicationList('version')"><a>Version</a></th>
                                                    <th ng-click="sortPublicationList('transactionId')"><a>Transaction ID</a></th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat-start="item in publicationList.data" id="{{item.id}}" class="publication">
                                                    <td rowspan="2">{{item.ROW_NUMBER}}</td>
                                                    <td>{{item.userName}}</td>
                                                    <td>{{item.actionDate | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm'}}</td>
                                                    <td>{{item.tag}}</td>
                                                    <td>{{item.version}}</td>
                                                    <td>{{item.transactionId}}</td>
                                                    <td class="text-nowrap text-right" style="vertical-align: middle;">
                                                        <button class="p-button p-component p-button-danger p-button-text-only" ng-click="deletePublication(item);$event.stopPropagation();">Delete Publication</button>
                                                        <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="recoveryProjectFromPublication(item);$event.stopPropagation();">Recovery Project</button>
                                                        <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportPublication(item);$event.stopPropagation();">Export Publication</button>
                                                        <%if ("true".equals(request.getParameter("j"))) {%>
                                                        <button class="p-button p-component p-button-secondary p-button-text-only" ng-click="exportPublication(item, '.json');$event.stopPropagation();">Export Publication in JSON</button>
                                                        <%}%>
                                                    </td>
                                                </tr>
                                                <tr ng-repeat-end>
                                                    <td colspan="6">
                                                        <div class="overflow-auto" style="height: 100px;">
                                                            <textarea class="max-width max-height text-<%=textColor%> bg-<%=bgColor%>" style="border: 0;" readonly>{{item.description}}</textarea>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="publicationList.totalCount > publicationListPageSize">
                                            <ul class="uibPagination" uib-pagination total-items="publicationList.totalCount" items-per-page="publicationListPageSize" ng-model="$parent.publicationListCurrentPage"
                                                max-size="10" boundary-links="true" rotate="false" ng-change="changePublicationListPage()"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <iframe id="frame" name="frame" src="" style="width: 100%;height: 100%;border: 0;padding: 0;margin: 0;display: none;" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
<form name="hiddenForm" target="frame" method="POST">
    <input type="hidden" name="param">
</form>
</body>
</html>