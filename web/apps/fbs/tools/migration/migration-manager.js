var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
    , 'angularFileUpload'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', '$interval', '$timeout', 'blockUI', 'AjaxService', 'marsContext', 'FileUploader', function ($scope, $filter, $uibModal, $interval, $timeout, blockUI, AjaxService, marsContext, FileUploader) {
    var ZeroId = 'N00000000000000000000000000000000';
    // Constants
    var TreeRoot = 'TreeRoot';
    var Application = 'Application';
    var Workspace = 'Workspace';
    var Project = 'Project';
    var ApplicationNodeId = "A0";
    var _timer = null;

    $scope.param = {};
    $scope.file = {};
    $scope.Workspace = Workspace;
    $scope.Project = Project;
    $scope.TreeRoot = TreeRoot;

    // Variables
    $scope.selectedNodeType = null;
    $scope.search = {};

    var blockUIStarted = false;

    function startBlockUI(msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    $scope.stopBlockUI = function () {
        $scope.$apply(function () {
            stopBlockUI();
        });
    }

    function getObjectType(node) {
        var objectType = null;

        if ('undefined' != typeof node.projectCount) {
            objectType = Workspace;
        } else if ('undefined' != typeof node.formCount) {
            objectType = Project;
        } else if (node.nodeId === ApplicationNodeId) {
            objectType = Application;
        } else {
            objectType = TreeRoot;
        }

        return objectType;
    }

    function generateNodeId(node) {
        var objectType = getObjectType(node);
        var nodeId = "";
        if (Workspace === objectType) {
            nodeId = "W" + node.id;
        } else if (Project === objectType) {
            nodeId = "P" + node.id;
        } else {
            nodeId = ApplicationNodeId;
        }

        return nodeId;
    }

    function getNodeImage(node) {
        var image;
        var objectType = getObjectType(node);
        if (Workspace === objectType) {
            image = "../image/W.png";
        } else if (Project === objectType) {
            image = "../image/P.png";
        } else {
            image = "../image/A.png";
        }

        return image;
    }

    $scope.treeOptions = {
        titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
        generateNodeId: generateNodeId,
        getNodeImage: getNodeImage,
        onSelectNode: function (node, selectedByClick) {
            $scope.selectedNode = node;
            $scope.selectedNodeType = getObjectType(node);
            $scope.treeOptions.selectedNode = node;
            $scope.dataValueListOrderColumn = 'label';
            $scope.transactionIds = undefined;

            if (selectedByClick) {
                if ($scope.selectedNodeType === Project) {
                    // getPublicationVersion($scope.selectedNode.id);
                    getPublicationList(1);
                }
            }
        },
        onExpandNode: function (node) {
            $scope.expandedNode = node;
        }
    };

    function callApi(apiUrl, callback, param) {
        var api = new AjaxService(marsContext.contextPath + apiUrl);
        api.post({
            success: function (o) {
                callback(o);
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function getWorkspaceTree() {
        callApi('/services/data/get/fbs.workspace-GetWorkspaceProjectTree@.json',
            function (o) {
                $scope.treeRoot = [{
                    name: "Root",
                    nodeId: "rootNode",
                    image: "../image/R.png",
                    children: o.data
                }];

                // $scope.treeOptions.selectedNode = {
                //     nodeId: "rootNode"
                // };

                stopBlockUI();
            }, {
                LIKE_permission_LIKE: ["%M%", "P"]
            });
    }

    function getPublicationVersion(projectId) {
        callApi('/services/data/get/fbs.publication-GetPublicationProjectHistoryInfoSimple.json',
            function (o) {
                $scope.transactionIds = o.data;
                stopBlockUI();
            }, {
                projectId: projectId,
                "ARRAY_COLUMN": "transactionId",
                "ORDER_BY": "transactionId DESC"
            });
    }

    $scope.refreshTree = function () {
        getWorkspaceTree();
    };

    (function () {
        getWorkspaceTree();
    })();

    //----------
    function callHistoryList(callback) {
        var param = {};
        if (TreeRoot === $scope.selectedNodeType) {
            param.objectIdIsNull = ZeroId;
        } else {
            param.objectId = $scope.selectedNode.id;
        }
        param.ORDER_BY = "actionDate DESC";

        $scope.migrationHistories = [];
        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.app-GetIEPActionLog.json');
        api.post({
            success: function (o) {
                $scope.migrationHistories = o.data;
                if (callback) {
                    callback();
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.showMigrationHistory = function () {
        var callback = $scope.loadMigrationHistory;
        callHistoryList(callback);
    };

    $scope.loadMigrationHistory = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/migration/migration-history.html'),
            windowClass: 'app-modal-window-10',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.selectedNode = scope.selectedNode;
                    $scope.selectedNodeType = scope.selectedNodeType;
                    $scope.migrationHistories = scope.migrationHistories;
                    $scope.title = "Migration History";

                    $scope.historyListCurrentPage = 1;
                    $scope.historyListPageSize = 10;
                    $scope.historyListOrderColumn = 'actionDate';
                    $scope.historyListOrderAsc = false;

                    function getHistoryList(pageNo) {
                        var param = {
                            pageNo: pageNo || $scope.historyListCurrentPage,
                            pageSize: $scope.historyListPageSize,
                            ORDER_BY: $scope.historyListOrderColumn + ' ' + ($scope.historyListOrderAsc ? 'ASC' : 'DESC')
                        };
                        if (TreeRoot === $scope.selectedNodeType) {
                            param.objectIdIsNull = ZeroId;
                        } else {
                            param.objectId = $scope.selectedNode.id;
                        }

                        $scope.historyListCurrentPage = param.pageNo;

                        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.app-GetIEPActionLog.json');
                        api.post({
                            success: function (o) {
                                $scope.migrationHistories = o.data;
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    $scope.sortHistoryList = function (column) {
                        if ($scope.historyListOrderColumn === column) {
                            $scope.historyListOrderAsc = !$scope.historyListOrderAsc;
                        } else $scope.historyListOrderAsc = true;
                        $scope.historyListOrderColumn = column;
                        getHistoryList($scope.historyListCurrentPage);
                    };

                    $scope.changeHistoryListPage = function () {
                        getHistoryList();
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                }
            ]
        }).result.then(function (historyData) {
        }, function () {
        });
    };

    //----------
    $scope.publicationListCurrentPage = 1;
    $scope.publicationListPageSize = 5;
    $scope.publicationListOrderColumn = 'transactionId';
    $scope.publicationListOrderAsc = false;

    function callPublicationList(param) {
        callApi('/services/data/getList/fbs.publication-GetPublicationProjectInfoSimple.json',
            function (o) {
                $scope.publicationList = o.data;
                stopBlockUI();
            }, param);
    }

    function getPublicationList(pageNo, filter) {
        var param = {
            projectId: $scope.selectedNode.id,
            pageNo: pageNo || $scope.publicationListCurrentPage,
            pageSize: $scope.publicationListPageSize,
            ORDER_BY: $scope.publicationListOrderColumn + ' ' + ($scope.publicationListOrderAsc ? 'ASC' : 'DESC')
        };

        $scope.publicationListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        callPublicationList(param);
    }

    $scope.sortPublicationList = function (column) {
        if ($scope.publicationListOrderColumn === column) {
            $scope.publicationListOrderAsc = !$scope.publicationListOrderAsc;
        } else $scope.publicationListOrderAsc = true;
        $scope.publicationListOrderColumn = column;
        getPublicationList($scope.publicationListCurrentPage);
    };

    function getSearchParam() {
        if ($scope.search.LIKE_tag_LIKE === "") $scope.search.LIKE_tag_LIKE = undefined;
        if ($scope.search.transactionId === "") $scope.search.transactionId = undefined;
        if ($scope.search.isfi === "") $scope.search.isfi = undefined;
        return $scope.search;
    }

    $scope.refreshPublicationList = function () {
        getPublicationList(undefined, getSearchParam());
    };

    $scope.changePublicationListPage = function () {
        getPublicationList();
    };

    $scope.searchPublication = function () {
        getPublicationList(1, getSearchParam());
    };

    //----------

    function getAdjustUrl(url) {
        if (-1 !== url.indexOf("services/data/get/")) {
            url = url.replace(/services\/data\/get\//, "services/data/getWithParam/");
        } else if (-1 !== url.indexOf("services/data/getList/")) {
            url = url.replace(/services\/data\/getList\//, "services/data/getListWithParam/");
        } else if (-1 !== url.indexOf("services/data/create/")) {
            url = url.replace(/services\/data\/create\//, "services/data/createWithParam/");
        } else if (-1 !== url.indexOf("services/data/update/")) {
            url = url.replace(/services\/data\/update\//, "services/data/updateWithParam/");
        } else if (-1 !== url.indexOf("services/data/delete/")) {
            url = url.replace(/services\/data\/delete\//, "services/data/deleteWithParam/");
        } else if (-1 !== url.indexOf("services/data/run/")) {
            url = url.replace(/services\/data\/run\//, "services/data/runWithParam/");
        } else if (-1 !== url.indexOf("services/data/download/")) {
            url = url.replace(/services\/data\/download\//, "services/data/downloadWithParam/");
        } else if (-1 !== url.indexOf("services/file/download/")) {
            url = url.replace(/services\/file\/download\//, "services/file/downloadWithParam/");
        }

        return url;
    }

    function downloadFile(url, param) {
        var form = document.forms['hiddenForm'];
        form.action = getAdjustUrl(url);
        form.param.value = mars$cipher$.encipher(JSON.stringify(param));
        form.submit();
    }

    var _fileName = null;
    var _reqKey = "";

    function isWaitingFile() {
        var cookieName = "DOWNLOAD-FILE-DONE" + _reqKey;
        var loadState = mars$util$.getCookie(cookieName);
        if (loadState === _fileName) {
            mars$util$.deleteCookie(cookieName, marsContext.contextPath);
            _fileName = null;
            $interval.cancel(_timer);
            stopBlockUI();
        }

        cookieName = "BIO-EXCEPTION" + _reqKey;
        var bioException = mars$util$.getCookie(cookieName);
        if (bioException && -1 !== bioException.indexOf(_fileName)) {
            mars$util$.deleteCookie(cookieName, marsContext.contextPath);
            $interval.cancel(_timer);
            stopBlockUI();
            var frameDoc = mars$util$.getFrameDocument("frame");
            try {
                var errorMsg = JSON.parse(frameDoc.body.innerText);
                var a = new AjaxService();
                a.alert(errorMsg.faultString);
            } catch (e) {
                console.error(e);
            }
        }
    }

    function exportData(projectId, projectName, type, fileExt, param) {
        _reqKey = "";
        _fileName = projectName + "-" + type;
        var uri = null;
        if (type === 'Model') {
            uri = '/services/file/download/fbs.export-ProjectDataModel@.json';
        } else if (type === 'Project') {
            uri = '/services/file/download/fbs.export-Project@.json';
        } else if (type === 'Publication') {
            uri = '/services/file/download/fbs.export-ProjectPublication@.json';
            if (param.tag) {
                _fileName += "-" + param.tag.replace(/\s+/g, '_').replace(/\W+/g, '_');
            }
            _fileName += "-" + param.transactionId;
        } else if (type === 'LookupData') {
            uri = '/services/file/download/fbs.export-LookupData@.json';
        } else if (type === 'File') {
            uri = '/services/file/download/fbs.export-File@.json';
        }
        _fileName += "-" + $filter('date')(new Date(), "yyyyMMdd_HHmmss") + fileExt;
        if (null != uri) {
            _reqKey = "-" + new Date().getTime();
            startBlockUI("Exporting...");
            downloadFile(marsContext.contextPath + uri + '/' + encodeURIComponent(_fileName) + "?$RK=" + _reqKey,
                param ||
                {
                    "projectId": projectId
                });
            _timer = $interval(isWaitingFile, 1000);
        }
    }

    $scope.exportDataModel = function (fileExt) {
        exportData($scope.selectedNode.id, $scope.selectedNode.name, "Model", fileExt || ".adx");
    };

    function uploadFile(fileType, jsonParam, formName) {
        var form = document.getElementById((formName || fileType) + "Form");
        if (form) {
            var inputs = form.getElementsByTagName("input");
            if (inputs) {
                for (var i = inputs.length - 1; i >= 0; i--) {
                    var input = inputs[i];
                    var type = input.getAttribute("type");
                    if (null == type || "file" != type) {
                        form.removeChild(input);
                    }
                }
            }

            var params = mars$util$.jsonToParamArray(jsonParam);
            if (params) {
                for (var i = 0; i < params.length; i++) {
                    var param = params[i];
                    var input = document.createElement("input");
                    input.setAttribute("type", "hidden");
                    input.setAttribute("name", param.name);
                    input.setAttribute("value", param.value);
                    form.appendChild(input);
                }
            }

            var uri = null;
            if (fileType === 'Model') {
                uri = marsContext.contextPath + "/services/file/run/fbs.import-ProjectDataModel@.html";
            } else if (fileType === 'Project') {
                uri = marsContext.contextPath + "/services/file/run/fbs.import-Project@.html";
            } else if (fileType === 'ProjectPublication') {
                uri = marsContext.contextPath + "/services/file/run/fbs.import-ProjectPublication@.html";
            } else if (fileType === 'ProjectFromPublication') {
                uri = marsContext.contextPath + "/services/file/run/fbs.import-ProjectFromPublication@.html";
            } else if (fileType === 'ProjectAsNew') {
                uri = marsContext.contextPath + "/services/file/run/fbs.import-CopyProject@.html";
            } else if (fileType === 'LookupData') {
                uri = marsContext.contextPath + "/services/file/run/fbs.import-LookupData@.html";
            } else if (fileType === 'File') {
                uri = marsContext.contextPath + "/services/file/run/fbs.import-File@.html";
            } else if (fileType === 'RecoveryProjectFromPublication') {
                uri = marsContext.contextPath + "/services/data/runWithParam/fbs.project-RecoveryProjectFromPublication@.html";
            }
            if (null != uri) {
                var frame = document.getElementById("frame");
                frame.style.display = "";
                var actionPanel = document.getElementById("actionPanel");
                actionPanel.style.display = "none";
                form.action = uri;
                if (fileType !== 'RecoveryProjectFromPublication') {
                    startBlockUI("It is being uploaded. It may take some time.")
                }
                form.submit();
            }
        } else alert(fileType + "Form NOT found");
    }

    $scope.importStarted = function () {
        $scope.stopBlockUI();
    }

    $scope.importDone = function () {
        $scope.refreshTree();
    };

    $scope.importError = function (msg, faultResult) {
        $timeout(stopBlockUI, 50);
        bootbox.alert(msg);
    };

    function importData(type, param, formName) {
        var form = document.getElementById((formName || type) + "Form");
        if (form.file.value === "") {
            bootbox.alert("Please select an ADX file");
            return;
        }

        var msg = "Are you sure to import?";
        if ('ProjectAsNew' === type) msg = "Are you sure to copy a project?";
        else if ("Project" === type) msg = "Are you sure to import a project?";
        else if ("ProjectPublication" === type) msg = "Are you sure to import a project publication?";
        else if ("ProjectFromPublication" === type) msg = "If a project exist, the existing project will be overwritten. Please export current project first if you need to back to current project later.<br><br>Are you sure to recovery a project from a project publication?";

        bootbox.confirm({
            message: msg,
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    uploadFile(type, param, formName);
                }
            }
        });
    }

    $scope.importDataModel = function () {
        var param = {
            projectId: $scope.selectedNode.id
        };

        importData("Model", param);
    };

    $scope.exportProject = function (fileExt) {
        exportData($scope.selectedNode.id, $scope.selectedNode.name, 'Project', fileExt || ".adx");
    };

    $scope.importProject = function () {
        var param = {};
        importData('Project', param);
    };

    $scope.importProjectPublication = function () {
        var param = {};
        importData('ProjectPublication', param, 'Project');
    };

    $scope.importProjectFromPublication = function () {
        var param = {};
        importData('ProjectFromPublication', param, 'Project');
    };

    $scope.importProjectAsNew = function () {
        importData('ProjectAsNew', param);
    };

    $scope.importLookupData = function () {
        var param = {};
        importData('LookupData', param, 'Project');
    };

    $scope.importFile = function () {
        var param = {};
        importData('File', param, 'Project');
    };

    $scope.copyProject = function (workspaceId) {
        $scope.param.workspaceId = workspaceId;
        var frame = document.getElementById("frame");
        frame.style.display = "";
        var actionPanel = document.getElementById("actionPanel");
        actionPanel.style.display = "none";
        var form = document.forms['hiddenForm'];
        form.action = getAdjustUrl(marsContext.contextPath + '/services/data/run/fbs.project-CopyProject@.html');
        form.param.value = mars$cipher$.encipher(JSON.stringify($scope.param));
        form.submit();
    };

    $scope.copyProject_old = function (workspaceId) {
        $scope.param.workspaceId = workspaceId;
        callApi('/services/data/run/fbs.project-CopyProject@.json',
            function (o) {
                stopBlockUI();
                $scope.refreshTree();
                bootbox.alert("Coping a project completed successfully.");
            }, $scope.param);
    };

    $scope.setHeight = function () {
        setHeight();
    };

    function deletePublication(item) {
        startBlockUI();
        callApi('/services/data/run/fbs.publication-DeleteProjectPublication@.json',
            function (o) {
                getPublicationList(1);
            }, {
                id: item.id
            });
    }

    $scope.deletePublication = function (item) {
        bootbox.confirm({
            message: "Are you sure to delete this project publication?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deletePublication(item);
                }
            }
        });
    }

    $scope.exportPublication = function (item, fileExt) {
        exportData($scope.selectedNode.id, $scope.selectedNode.name, "Publication", fileExt || ".adx", {
            projectId: $scope.selectedNode.id,
            id: item.id,
            transactionId: item.transactionId,
            tag: item.tag
        });
    };

    $scope.recoveryProjectFromPublication = function (item) {
        bootbox.confirm({
            message: "The existing project will be overwritten. Please export current project first if you need to back to current project later.<br><br>Are you sure to recovery a project from this project publication?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    uploadFile("RecoveryProjectFromPublication", {
                        param: mars$cipher$.encipher(JSON.stringify({
                            projectPublicationId: item.id
                        }))
                    }, "RecoveryProject");
                }
            }
        });
    }

    $scope.showPublicationHistory = function () {

        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/migration/migration-publication-history.html'),
            windowClass: 'app-modal-window-10',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.selectedNode = scope.selectedNode;
                    $scope.selectedNodeType = scope.selectedNodeType;
                    $scope.publicationHistoryList = scope.publicationHistoryList;
                    $scope.title = "Publication History";

                    $scope.searchPublicationHistory = {};
                    $scope.publicationHistoryListCurrentPage = 1;
                    $scope.publicationHistoryListPageSize = 3;
                    $scope.publicationHistoryListOrderColumn = 'transactionId';
                    $scope.publicationHistoryListOrderAsc = false;
                    $scope.isUndelete = false;

                    function callPublicationHistoryList(param) {
                        callApi('/services/data/getList/fbs.publication-GetPublicationProjectHistoryInfoSimple.json',
                            function (o) {
                                $scope.publicationHistoryList = o.data;
                                stopBlockUI();
                            }, param);
                    }

                    function getPublicationHistoryList(pageNo, filter) {
                        var param = {
                            projectId: $scope.selectedNode.id,
                            pageNo: pageNo || $scope.publicationHistoryListCurrentPage,
                            pageSize: $scope.publicationHistoryListPageSize,
                            ORDER_BY: $scope.publicationHistoryListOrderColumn + ' ' + ($scope.publicationHistoryListOrderAsc ? 'ASC' : 'DESC'),
                            RANKING: 1
                        };

                        $scope.publicationHistoryListCurrentPage = param.pageNo;

                        if (filter) {
                            angular.extend(param, filter);
                        }

                        callPublicationHistoryList(param);
                    }

                    $scope.sortPublicationHistoryList = function (column) {
                        if ($scope.publicationHistoryListOrderColumn === column) {
                            $scope.publicationHistoryListOrderAsc = !$scope.publicationHistoryListOrderAsc;
                        } else $scope.publicationHistoryListOrderAsc = true;
                        $scope.publicationHistoryListOrderColumn = column;
                        getPublicationHistoryList($scope.publicationHistoryListCurrentPage);
                    };

                    function getSearchHistoryParam() {
                        if ($scope.searchPublicationHistory.LIKE_tag_LIKE === "") $scope.searchPublicationHistory.LIKE_tag_LIKE = undefined;
                        if ($scope.searchPublicationHistory.transactionId === "") $scope.searchPublicationHistory.transactionId = undefined;
                        if ($scope.searchPublicationHistory.isfi === "") $scope.searchPublicationHistory.isfi = undefined;
                        return $scope.searchPublicationHistory;
                    }

                    $scope.refreshPublicationHistoryList = function () {
                        getPublicationHistoryList(undefined, getSearchHistoryParam());
                    };

                    $scope.changePublicationHistoryListPage = function () {
                        getPublicationHistoryList();
                    };

                    $scope.searchPublicationHistory = function () {
                        getPublicationHistoryList(1, getSearchHistoryParam());
                    };

                    function undeletePublication(item) {
                        startBlockUI();
                        callApi('/services/data/run/fbs.publication-UndeleteProjectPublication@.json',
                            function (o) {
                                $scope.isUndelete = true;
                                getPublicationHistoryList(1);
                            }, {
                                id: item.id,
                                transactionId2: item.transactionId2
                            });
                    }

                    $scope.undeletePublication = function (item) {
                        if (null === item.transactionId2 || 0 === item.transactionId2) {
                            bootbox.alert("Invalid transaction information for deleted Publication");
                            return;
                        }

                        bootbox.confirm({
                            message: "Are you sure to undelete this project publication?",
                            buttons: {
                                cancel: {
                                    label: 'No',
                                    className: 'btn btn-sm btn-secondary'
                                },
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn btn-sm btn-primary'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    undeletePublication(item);
                                }
                            }
                        });
                    };

                    $scope.cancel = function () {
                        $scope.$close($scope.isUndelete);
                    };

                    (function () {
                        getPublicationHistoryList();
                    })();
                }
            ]
        }).result.then(function (data) {
            if (data) {
                getPublicationList(1);
            }
        }, function () {
        });
    };

    $scope.exportLookupData = function (fileExt) {
        exportData($scope.selectedNode.id, $scope.selectedNode.name, "LookupData", fileExt || ".adx", {
            "district": "Application"
        });
    };

    $scope.exportFile = function (fileExt) {
        exportData($scope.selectedNode.id, $scope.selectedNode.name, "File", fileExt || ".adx", {
            "district": "Application"
        });
    };

    function moveWorkspaceResources(param) {
        startBlockUI("Moving resources...");
        callApi('/services/data/run/fbs.move-workspaceResources@.json',
            function (o) {
                stopBlockUI();
                $scope.refreshTree();
            }, param);
    }

    $scope.moveWorkspaceResources = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/migration/migration-move-resources.html'),
            size: 'full',
            controller: ['$scope',
                function ($scope) {
                    $scope.clientSize = mars$util$.getClientSize();
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.selectedNode = scope.selectedNode;
                    $scope.selectedNodeType = scope.selectedNodeType;
                    $scope.title = "Move Resources of " + $scope.selectedNode.name
                    $scope.textColor = textColor;
                    $scope.bgColor = bgColor;
                    $scope.workspaceId = $scope.selectedNode.id;
                    $scope.workspace = {
                        id: $scope.selectedNode.id
                    };

                    $scope.moveResources = {
                        from: {
                            workspaceId: $scope.selectedNode.id
                        },
                        to: {},
                        i18n: [],
                        lookupData: [],
                        rawDataSource: [],
                        file: [],
                        context: [],
                        project: []
                    };

                    $scope.stopBlockUI = function () {
                        if ($scope.workspace.i18n && $scope.workspace.project && $scope.workspace.rawDataSource
                            && $scope.workspace.lookupData && $scope.workspace.file && $scope.workspace.context) {
                            stopBlockUI();
                        }
                    };

                    $scope.init = function () {
                        callApi('/services/data/get/fbs.i18n-GetI18N.json',
                            function (o) {
                                $scope.workspace.i18n = o.data;
                                $scope.stopBlockUI();
                            }, {
                                workspaceId: $scope.workspace.id,
                                WITHOUT_CONTENT: true
                            });

                        callApi('/services/data/get/fbs.project-GetProject.json',
                            function (o) {
                                $scope.workspace.project = o.data;
                                $scope.stopBlockUI();
                            }, {
                                workspaceId: $scope.workspace.id
                            });

                        callApi('/services/data/get/fbs.rawData-GetRawDataSource.json',
                            function (o) {
                                $scope.workspace.rawDataSource = o.data;
                                $scope.stopBlockUI();
                            }, {
                                workspaceId: $scope.workspace.id
                            });

                        callApi('/services/data/get/fbs.lookup-GetLookupDataType.json',
                            function (o) {
                                $scope.workspace.lookupData = o.data;
                                $scope.stopBlockUI();
                            }, {
                                workspaceId: $scope.workspace.id
                            });

                        callApi('/services/data/get/fbs.file-GetFile.json',
                            function (o) {
                                $scope.workspace.file = o.data;
                                $scope.stopBlockUI();
                            }, {
                                workspaceId: $scope.workspace.id
                            });

                        callApi('/services/data/get/fbs.context-GetContext.json',
                            function (o) {
                                $scope.workspace.context = o.data;
                                $scope.stopBlockUI();
                            }, {
                                workspaceId: $scope.workspace.id,
                                WITHOUT_CONTENT: true
                            });
                    };

                    (function () {
                        startBlockUI();
                        $scope.init();
                    })();

                    $scope.removeItem = function (target, item) {
                        var index = target.indexOf(item);
                        if (index >= 0) {
                            target.splice(index, 1);
                        }
                    }

                    $scope.addItem = function (target, item) {
                        var index = target.indexOf(item);
                        if (index === -1) {
                            target.push(item);
                        }
                    }

                    $scope.checkItem = function (target, item, checked) {
                        if ("undefined" != typeof checked) item.checked = checked;
                        else item.checked = !item.checked;
                        if (item.checked) $scope.addItem(target, item);
                        else $scope.removeItem(target, item);
                    }

                    $scope.checkAll = function (target, items) {
                        angular.forEach(items, function (item) {
                            $scope.checkItem(target, item, true);
                        });
                    }

                    $scope.uncheckAll = function (target, items) {
                        angular.forEach(items, function (item) {
                            $scope.checkItem(target, item, false);
                        });
                    }

                    function selectWorkspace(selectedData, target) {
                        if (angular.isDefined(selectedData)) {
                            target.workspaceId = selectedData.originalObject.id;
                        } else {
                            target.workspaceId = undefined;
                        }
                    }

                    $scope.selectWorkspace = function (selectedData, target) {
                        selectWorkspace(selectedData, target);
                    };

                    $scope.isReadyData = function () {
                        return $scope.moveResources.to.workspaceId && (
                            $scope.moveResources.i18n.length > 0 || $scope.moveResources.lookupData.length > 0 ||
                            $scope.moveResources.rawDataSource.length > 0 || $scope.moveResources.file.length > 0 ||
                            $scope.moveResources.context.length > 0 || $scope.moveResources.project.length > 0);
                    }

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.move = function () {
                        if ($scope.moveResources.to.workspaceId === $scope.workspaceId) {
                            angularExt.alert("The source workspace and target workspace are the same. Please select a different workspace.");
                        } else {
                            bootbox.confirm({
                                message: "Are you sure to move the selected resources?",
                                buttons: {
                                    cancel: {
                                        label: 'No',
                                        className: 'btn btn-sm btn-secondary'
                                    },
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn btn-sm btn-primary'
                                    }
                                },
                                callback: function (result) {
                                    if (result) {
                                        $scope.$close($scope.moveResources);
                                    }
                                }
                            });
                        }
                    };
                }
            ]
        }).result.then(function (moveResources) {
            if (moveResources) {
                moveWorkspaceResources(moveResources)
            }
        }, function () {
        });
    };
}]);

function serviceErrorCallback(msg, faultResult) {
    var scope = angularExt.getAngularScope("body");
    scope.importError(msg, faultResult);
}

function progressiveRunError(faultResult) {
    var scope = angularExt.getAngularScope("body");
    scope.importStarted();
}

function progressiveRunStart() {
    var scope = angularExt.getAngularScope("body");
    scope.importStarted();
}

function progressiveRunDone() {
    var scope = angularExt.getAngularScope("body");
    scope.importDone();
}

function progressiveRunDoneOk() {
    var frame = document.getElementById("frame");
    frame.src = "../blank.html";
    frame.style.display = "none";
    var actionPanel = document.getElementById("actionPanel");
    actionPanel.style.display = "";
}

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";
}
