var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//) || config.url.match(/myTimeInfo.json/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$timeout', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $timeout, $filter, $uibModal, blockUI, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);
    $scope.projectId = parameter.getParameterValue("project_id", '');
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;
    // Constants
    var ZeroId = 'N00000000000000000000000000000000';
    var TreeRoot = 'TreeRoot';
    var Application = 'Application';
    var Workspace = 'Workspace';
    var Project = 'Project';
    var DataType = 'DataType';
    var ApplicationNodeId = "A0";
    var Yes = 'Y';
    var No = 'N';

    $scope.DataType = DataType;
    $scope.TreeRoot = TreeRoot;
    $scope.Yes = Yes;
    $scope.No = No;
    $scope.OriginalListMode = "O";
    $scope.EvaluatedListMode = "C";

    // Variables
    $scope.selectedNodeType = null;

    $scope.listMode = $scope.OriginalListMode;
    $scope.dataValueListCurrentPage = 1;
    $scope.dataValueListPageSize = 15;
    $scope.dataValueListOrderColumn = 'displayOrder';
    $scope.dataValueListOrderAsc = true;

    var blockUIStarted = false;

    $scope.setDataValueListPageSize = function (size) {
        $scope.dataValueListPageSize = size;
    };

    function startBlockUI() {
        blockUI.start();
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function callApi(apiUrl, callback, param) {
        var api = new AjaxService(marsContext.contextPath + apiUrl);
        api.post({
            success: function (o) {
                callback(o);
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function getObjectType(node) {
        var objectType = null;

        if ('undefined' != typeof node.projectCount) {
            objectType = Workspace;
        } else if ('undefined' != typeof node.formCount) {
            objectType = Project;
        } else if ('undefined' != typeof node.type && 'undefined' != typeof node.workspaceId && 'undefined' != typeof node.projectId) {
            objectType = DataType;
        } else if (node.nodeId === ApplicationNodeId) {
            objectType = Application;
        } else {
            objectType = TreeRoot;
        }

        return objectType;
    }

    function generateNodeId(node) {
        var objectType = getObjectType(node);
        var nodeId = "";
        if (Workspace === objectType) {
            nodeId = "W" + node.id;
        } else if (Project === objectType) {
            nodeId = "P" + node.id;
        } else if (DataType === objectType) {
            nodeId = "T" + node.id;
        } else {
            nodeId = ApplicationNodeId;
        }

        return nodeId;
    }

    function getNodeImage(node) {
        var image;
        var objectType = getObjectType(node);
        if (Workspace === objectType) {
            image = "../image/W.png";
        } else if (Project === objectType) {
            image = "../image/P.png";
        } else if (DataType === objectType) {
            image = "../image/T.png";
        } else {
            image = "../image/A.png";
        }

        return image;
    }

    $scope.treeOptions = {
        titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
        generateNodeId: generateNodeId,
        getNodeImage: getNodeImage,
        onSelectNode: function (node, selectedByClick) {
            $scope.selectedNode = node;
            $scope.selectedNodeType = getObjectType(node);
            $scope.treeOptions.selectedNode = node;
            $scope.dataValueListOrderColumn = 'displayOrder';

            if (selectedByClick) {
                if ($scope.selectedNode.rawDataSrcId) {
                    getRawDataSource();
                    $scope.lookupDataValueList = $scope.lookupDataValueList || {};
                    $scope.lookupDataValueList.data = [];
                    $scope.lookupDataValueList.totalCount = 0;
                } else {
                    $scope.changeDataValueListPage(1);
                }
            }
        },
        onExpandNode: function (node) {
            $scope.expandedNode = node;
        }
    };

    function getRawDataSource() {
        callApi('/services/data/get/fbs.rawData-GetRawDataSource.json',
            function (o) {
                if (o && o.data && o.data.length > 0) {
                    $scope.rawDataSource = o.data[0];
                } else {
                    $scope.rawDataSource = undefined;
                }
                stopBlockUI();
            }, {
                id: $scope.selectedNode.rawDataSrcId
            });
    }

    function getLookupDataTypes() {
        var param = {
            LIKE_permission_LIKE: ["%M%", "P"]
        }
        if ($scope.projectId) param.projectId = $scope.projectId;
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.lookup-GetLookupDataTypeTree@.json');
        api.post({
            success: function (o) {
                $scope.lookupDataTypes = o.data;
                $scope.treeRoot = [{
                    name: "Lookup Data",
                    nodeId: "rootNode",
                    image: "../image/D.png",
                    children: $scope.lookupDataTypes
                }];

                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.refreshDataTypes = function () {
        getLookupDataTypes();
    };

    function createLookupDataType(lookupDataType) {
        startBlockUI();

        if (lookupDataType.district === Application) {
            lookupDataType.workspaceId = ZeroId;
            lookupDataType.projectId = ZeroId;
        } else if (lookupDataType.district === Workspace) {
            lookupDataType.projectId = ZeroId;
        } else if (lookupDataType.district === Project) {
            lookupDataType.workspaceId = ZeroId;
        }
        var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.lookup-CreateLookupDataType@.json');
        api.post({
            success: function (o) {
                $scope.refreshDataTypes();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, lookupDataType);
    }

    function updateLookupDataType(lookupDataType) {
        if (lookupDataType.district === Application) {
            lookupDataType.workspaceId = ZeroId;
            lookupDataType.projectId = ZeroId;
        } else if (lookupDataType.district === Workspace) {
            lookupDataType.projectId = ZeroId;
        } else if (lookupDataType.district === Project) {
            lookupDataType.workspaceId = ZeroId;
        }
        var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.lookup-UpdateLookupDataType@.json');
        api.post({
            success: function (o) {
                $scope.refreshDataTypes();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, lookupDataType);
    }

    function getLookupDataDistrict() {
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.lookup-GetLookupDataDistrict@.json');
        api.get({
            success: function (o) {
                $scope.lookupDataDistrict = o.data;
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    function selectWorkspace(selectedData, lookupDataType) {
        if (angular.isDefined(selectedData)) {
            lookupDataType.workspaceId = selectedData.originalObject.id;
        } else {
            lookupDataType.workspaceId = ZeroId;
        }
    }

    function selectProject(selectedData, lookupDataType) {
        if (angular.isDefined(selectedData)) {
            lookupDataType.projectId = selectedData.originalObject.id;
        } else {
            lookupDataType.projectId = ZeroId;
        }
    }

    function isReadyData(lookupDataType) {
        if ("Workspace" === lookupDataType.district) {
            return lookupDataType.workspaceId && lookupDataType.workspaceId != ZeroId;
        } else if ("Project" === lookupDataType.district) {
            return lookupDataType.workspaceId && lookupDataType.workspaceId != ZeroId && lookupDataType.projectId && lookupDataType.projectId != ZeroId;
        }
        return true;
    }

    $scope.addDataType = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/lookup/lookup-data-type.html'),
            size: 'full',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.mode = 'Add';
                    $scope.lookupDataDistrict = scope.lookupDataDistrict;
                    $scope.title = "Add Lookup Data Type";
                    $scope.lookupDataType = {
                        workspaceId: 0,
                        projectId: 0
                    };
                    if (scope.selectedNode) {
                        if (scope.selectedNodeType === Workspace) {
                            $scope.lookupDataType.district = Workspace;
                            $scope.lookupDataType.workspaceId = scope.selectedNode.id;
                            $scope.lookupDataType.workspaceName = scope.selectedNode.name;
                        } else if (scope.selectedNodeType === Project) {
                            $scope.lookupDataType.district = Project;
                            $scope.lookupDataType.projectId = scope.selectedNode.id;
                            $scope.lookupDataType.projectName = scope.selectedNode.name;
                            $scope.lookupDataType.workspaceId = scope.selectedNode.parent.id;
                            $scope.lookupDataType.workspaceName = scope.selectedNode.parent.name;
                        } else if (scope.selectedNodeType === DataType && scope.selectedNode.district === Workspace) {
                            $scope.lookupDataType.district = Workspace;
                            $scope.lookupDataType.workspaceId = scope.selectedNode.parent.id;
                            $scope.lookupDataType.workspaceName = scope.selectedNode.parent.name;
                        } else if (scope.selectedNodeType === DataType && scope.selectedNode.district === Project) {
                            $scope.lookupDataType.district = Project;
                            $scope.lookupDataType.projectId = scope.selectedNode.parent.id;
                            $scope.lookupDataType.projectName = scope.selectedNode.parent.name;
                            $scope.lookupDataType.workspaceId = scope.selectedNode.parent.parent.id;
                            $scope.lookupDataType.workspaceName = scope.selectedNode.parent.parent.name;
                        } else {
                            $scope.lookupDataType.district = Application;
                        }
                    } else {
                        $scope.lookupDataType.district = Application;
                    }

                    //check if it is called by iframe
                    $scope.isParamProjectId = function () {
                        return scope.projectId !== '';
                    };
                    $scope.isReadyData = function () {
                        return isReadyData($scope.lookupDataType);
                    };
                    $scope.selectWorkspace = function (selectedData, lookupDataType) {
                        selectWorkspace(selectedData, lookupDataType);
                    };
                    $scope.selectProject = function (selectedData, lookupDataType) {
                        selectProject(selectedData, lookupDataType);
                    };
                    $scope.searchProjectParams = function (string) {
                        return {
                            workspaceId: $scope.lookupDataType.workspaceId,
                            LIKE_name_LIKE: string
                        };
                    };
                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.lookupDataType);
                    };
                }
            ]
        }).result.then(function (lookupDataType) {
            createLookupDataType(lookupDataType);
        }, function () {
        });
    };

    $scope.editDataType = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/lookup/lookup-data-type.html'),
            size: 'full',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.mode = 'Modify';
                    $scope.lookupDataDistrict = scope.lookupDataDistrict;
                    $scope.title = "Modify Lookup Data Type";
                    $scope.lookupDataType = {
                        id: scope.selectedNode.id,
                        workspaceId: scope.selectedNode.workspaceId,
                        projectId: scope.selectedNode.projectId,
                        type: scope.selectedNode.type,
                        description: scope.selectedNode.description,
                        district: scope.selectedNode.district
                    };

                    if (Project === $scope.lookupDataType.district) {
                        $scope.lookupDataType.projectName = scope.selectedNode.parent.name;
                        $scope.lookupDataType.workspaceId = scope.selectedNode.parent.parent.id;
                        $scope.lookupDataType.workspaceName = scope.selectedNode.parent.parent.name;
                    } else if (Workspace === $scope.lookupDataType.district) {
                        $scope.lookupDataType.workspaceId = scope.selectedNode.parent.id;
                        $scope.lookupDataType.workspaceName = scope.selectedNode.parent.name
                    }

                    //check if it is called by iframe
                    $scope.isParamProjectId = function () {
                        return scope.projectId !== '';
                    };
                    $scope.isReadyData = function () {
                        return isReadyData($scope.lookupDataType);
                    };
                    $scope.selectWorkspace = function (selectedData, lookupDataType) {
                        selectWorkspace(selectedData, lookupDataType);
                    };
                    $scope.selectProject = function (selectedData, lookupDataType) {
                        selectProject(selectedData, lookupDataType);
                    };
                    $scope.searchProjectParams = function (string) {
                        return {
                            workspaceId: $scope.lookupDataType.workspaceId,
                            LIKE_name_LIKE: string
                        };
                    };
                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.lookupDataType);
                    };
                }
            ]
        }).result.then(function (lookupDataType) {
            updateLookupDataType(lookupDataType);
        }, function () {
        });
    };


    (function () {
        getLookupDataDistrict();
        getLookupDataTypes();
    })();

    //****************** LookupDataValue ************************
    function getMyTimeInfo() {
        var api = new AjaxService(marsContext.contextPath + '/services/acm/myTimeInfo.json');
        api.get({
            success: function (o) {
                $scope.myTimeInfo = o.data;
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    function callEvaluatedLookupDataValueListApi(param) {
        var apiName = null;
        if (TreeRoot === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetEvaluatedLookupData.json';
        } else if (Application === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetEvaluatedApplicationLookupData.json';
        } else if (Workspace === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetEvaluatedWorkspaceLookupData.json';
            param.workspaceId = $scope.selectedNode.id;
        } else if (Project === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetEvaluatedProjectLookupData.json';
            param.projectId = $scope.selectedNode.id;
        } else if (DataType === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetEvaluatedLookupDataValue.json';
            param.typeId = $scope.selectedNode.id;
        }

        if (null != apiName) {
            var api = new AjaxService(marsContext.contextPath + '/services/data/getList/' + apiName);
            api.post({
                success: function (o) {
                    $scope.lookupDataValueList = o.data;
                    getMyTimeInfo();
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        } else {
            bootbox.alert("selected node type not found");
        }
    }

    function getEvaluatedLookupDataValueList(pageNo, filter) {
        var param = {
            pageNo: pageNo || $scope.dataValueListCurrentPage,
            pageSize: $scope.dataValueListPageSize,
            ORDER_BY: $scope.dataValueListOrderColumn + ' ' + ($scope.dataValueListOrderAsc ? 'ASC' : 'DESC')
        };

        $scope.dataValueListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        callEvaluatedLookupDataValueListApi(param);
    }

    function callLookupDataValueListApi(param) {
        var apiName = null;
        if (TreeRoot === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetLookupData.json';
        } else if (Application === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetApplicationLookupData.json';
        } else if (Workspace === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetWorkspaceLookupData.json';
            param.workspaceId = $scope.selectedNode.id;
        } else if (Project === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetProjectLookupData.json';
            param.projectId = $scope.selectedNode.id;
        } else if (DataType === $scope.selectedNodeType) {
            apiName = 'fbs.lookup-GetLookupData.json';
            param.typeId = $scope.selectedNode.id;
        }

        if (null != apiName) {
            var api = new AjaxService(marsContext.contextPath + '/services/data/getList/' + apiName);
            api.post({
                success: function (o) {
                    $scope.lookupDataValueList = o.data;
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        } else {
            bootbox.alert("selected node type not found");
        }
    }

    function getLookupDataValueList(pageNo, filter) {
        var param = {
            pageNo: pageNo || $scope.dataValueListCurrentPage,
            pageSize: $scope.dataValueListPageSize,
            ORDER_BY: $scope.dataValueListOrderColumn + ' ' + ($scope.dataValueListOrderAsc ? 'ASC' : 'DESC')
        };

        $scope.dataValueListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        callLookupDataValueListApi(param);
    }

    $scope.changeDataValueListPage = function (pageNo) {
        $scope.myTimeInfo = undefined;

        if ($scope.listMode === $scope.EvaluatedListMode) {
            getEvaluatedLookupDataValueList(pageNo);
        } else {
            getLookupDataValueList(pageNo);
        }
    };

    $scope.openSubType = function (dataValue) {
        try {
            $("#T" + dataValue.subTypeId).find("span.node-title")[0].click();
        } catch (e) {
        }
    };

    $scope.viewEvaluatedDataValue = function () {
        $scope.listMode = $scope.EvaluatedListMode;
        $scope.changeDataValueListPage($scope.dataValueListCurrentPage);
    };

    $scope.viewOriginalDataValue = function () {
        $scope.listMode = $scope.OriginalListMode;
        $scope.changeDataValueListPage($scope.dataValueListCurrentPage);
    };

    $scope.refreshDataValueList = function () {
        $scope.changeDataValueListPage($scope.dataValueListCurrentPage);
    };

    function createLookupDataValue(lookupDataValue) {
        startBlockUI();

        var api = new AjaxService(marsContext.contextPath + '/services/data/create/fbs.lookup-CreateLookupDataValue.json');
        api.post({
            success: function (o) {
                $scope.refreshDataValueList();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, lookupDataValue);
    }

    function getUTCDate(date, hour) {
        var dateString = $filter('date')(date, 'yyyy/MM/dd') + " " + hour + " UTC";
        return mars$util$.makeDate(dateString);
    }

    function getUTCDateString(date, hour) {
        if (date) return $filter('date')(date, 'yyyy-MM-dd') + " " + hour;
        else return null;
    }

    function updateLookupDataValue(lookupDataValue) {
        if (angular.isDate(lookupDataValue.effectiveDate)) {
            lookupDataValue.effectiveDate = getUTCDateString(lookupDataValue.effectiveDate, "00:00:00");
        } else {
            lookupDataValue.effectiveDate = null;
            lookupDataValue.effectiveDateIsNull = "true";
        }

        if (angular.isDate(lookupDataValue.expirationDate)) {
            lookupDataValue.expirationDate = getUTCDateString(lookupDataValue.expirationDate, "23:59:59");
        } else {
            lookupDataValue.expirationDate = null;
            lookupDataValue.expirationDateIsNull = "true";
        }

        if (null == lookupDataValue.subTypeId) {
            lookupDataValue.subTypeIdIsNull = "true";
        }

        if (null == lookupDataValue.i18n) {
            lookupDataValue.i18nIsNull = "true";
        }

        var api = new AjaxService(marsContext.contextPath + '/services/data/create/fbs.lookup-UpdateLookupDataValue.json');
        api.post({
            success: function (o) {
                $scope.refreshDataValueList();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, lookupDataValue);
    }

    $scope.editDataValue = function (mode, dataValue) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/lookup/lookup-data-value.html'),
            size: 'full',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.Yes = Yes;
                    $scope.No = No;
                    $scope.mode = mode;
                    if ("Add" == $scope.mode) {
                        $scope.title = "Add Lookup Data Value";
                        $scope.lookupDataValue = {
                            typeId: scope.selectedNode.id,
                            active: Yes,
                            readonly: No,
                            displayOrder: 0
                        };
                        var selectedNode = scope.selectedNode.parent;
                        $scope.lookupDataValue.district = getObjectType(selectedNode);
                        if (Workspace === $scope.lookupDataValue.district) {
                            $scope.lookupDataValue.workspaceId = selectedNode.id;
                        } else if (Project === $scope.lookupDataValue.district) {
                            $scope.lookupDataValue.projectId = selectedNode.id;
                        }
                    } else if ("Update" == $scope.mode) {
                        $scope.title = "Update Lookup Data Value";
                        $scope.lookupDataValue = {};
                        angular.extend($scope.lookupDataValue, dataValue);
                        $scope.lookupDataValue.transactionId = undefined;
                        if ($scope.lookupDataValue.effectiveDate) {
                            $scope.lookupDataValue.effectiveDate = mars$util$.makeDate($scope.lookupDataValue.effectiveDate);
                        }
                        if ($scope.lookupDataValue.expirationDate) {
                            $scope.lookupDataValue.expirationDate = mars$util$.makeDate($scope.lookupDataValue.expirationDate);
                        }
                    }

                    $scope.resetSubType = function () {
                        $scope.lookupDataValue.subType = null;
                        $scope.lookupDataValue.subTypeId = null;
                    };

                    $scope.getSubType = function (dataValue, district, param, callback) {
                        var apiName = null;
                        if (Application === district) {
                            apiName = 'fbs.lookup-GetAllApplicationLookupDataType@.json';
                        } else if (Workspace === district) {
                            apiName = 'fbs.lookup-GetWorkspaceLookupDataType@.json';
                            param.workspaceId = dataValue.workspaceId;
                        } else if (Project === district) {
                            apiName = 'fbs.lookup-GetAllWorkspaceLookupDataType@.json';
                            param.projectId = dataValue.projectId;
                        }
                        if (null != apiName) {
                            var api = new AjaxService(marsContext.contextPath + '/services/data/get/' + apiName);
                            api.post({
                                success: function (o) {
                                    callback(o);
                                    stopBlockUI();
                                },
                                error: function (o) {
                                    stopBlockUI();
                                    o.alertException();
                                }
                            }, param);
                        } else {
                            bootbox.alert("selected node type not found");
                        }
                    };

                    $scope.selectSubType = function (dataValue) {
                        var district = dataValue.district;
                        var param = {
                            LIKE_permission_LIKE: ["%M%", "P"]
                        };
                        $scope.getSubType(dataValue, district, param, function (o) {
                            $uibModal.open({
                                backdrop: false,
                                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/lookup/lookup-data-value-link.html'),
                                size: 'md',
                                controller: ['$scope',
                                    function ($scope) {
                                        $scope.scope = scope;
                                        var clientSize = mars$util$.getClientSize();
                                        var lookupModal = document.getElementById("lookupModal");
                                        var lookupModalBody = document.getElementById("lookupModalBody");
                                        $scope.title = "Select a Lookup Data Type to be linked to " + dataValue.label;
                                        // if the space is enough, select bigger
                                        $scope.modalBodyHeight = clientSize.height * 0.9 > lookupModal.offsetHeight ? clientSize.height * 0.8 : lookupModalBody.offsetHeight;
                                        $scope.treeRoot = [];

                                        if (Application == dataValue.district) {
                                            $scope.treeRoot = [{
                                                name: "Lookup Data",
                                                nodeId: "rootNode",
                                                image: "../image/D.png",
                                                children: [{
                                                    name: "Application",
                                                    children: o.data.children
                                                }]
                                            }];
                                        } else if (Workspace == dataValue.district || Project == dataValue.district) {
                                            $scope.treeRoot = [{
                                                name: "Lookup Data",
                                                nodeId: "rootNode",
                                                image: "../image/D.png",
                                                children: o.data
                                            }];
                                        }

                                        $scope.treeOptions = {
                                            titleField: ['name', 'type', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
                                            generateNodeId: generateNodeId,
                                            getNodeImage: getNodeImage,
                                            onSelectNode: function (node) {
                                                $scope.selectedNode = node;
                                            }
                                        };

                                        $scope.cancel = function () {
                                            $scope.$dismiss();
                                        };
                                        $scope.ok = function () {
                                            $scope.$close($scope.selectedNode);
                                        };
                                    }
                                ]
                            }).result.then(function (selectedNode) {
                                if ('undefined' != typeof selectedNode && selectedNode.type) {
                                    dataValue.subTypeId = selectedNode.id;
                                    dataValue.subType = selectedNode.type;
                                } else {
                                    bootbox.alert("The selected one was not a type. Please select a type");
                                }
                            }, function () {
                            });
                        });
                    };

                    $scope.resetI18N = function () {
                        $scope.lookupDataValue.i18n = null;
                    };

                    $scope.getI18NData = function (dataValue, district, param, callback) {
                        var apiName = null;
                        if (Application === district) {
                            apiName = 'fbs.app-GetI18N@.json';
                        } else if (Workspace === district) {
                            apiName = 'fbs.workspace-GetI18N@.json';
                            param.workspaceId = dataValue.workspaceId;
                        } else if (Project === district) {
                            apiName = 'fbs.project-GetI18N@.json';
                            param.projectId = dataValue.projectId;
                        }
                        if (null != apiName) {
                            var api = new AjaxService(marsContext.contextPath + '/services/data/get/' + apiName);
                            api.post({
                                success: function (o) {
                                    callback(o);
                                    stopBlockUI();
                                },
                                error: function (o) {
                                    stopBlockUI();
                                    o.alertException();
                                }
                            }, param);
                        } else {
                            bootbox.alert("selected node type not found");
                        }
                    };

                    $scope.selectI18N = function (dataValue) {
                        var district = dataValue.district;
                        var param = {
                            baseLang: "en",
                            lang: "en"
                        };
                        $scope.getI18NData(dataValue, district, param, function (o) {
                            if (!o.data || o.data.length == 0) {
                                bootbox.alert(district + " I18N data not found");
                                return;
                            }
                            var i18nData = o.data[param.lang].content;
                            $uibModal.open({
                                backdrop: false,
                                templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/lookup/lookup-data-value-i18n.html'),
                                size: 'md',
                                controller: ['$scope',
                                    function ($scope) {
                                        $scope.scope = scope;
                                        var clientSize = mars$util$.getClientSize();
                                        var lookupModal = document.getElementById("lookupModal");
                                        var lookupModalBody = document.getElementById("lookupModalBody");
                                        $scope.title = "Select a I18N for " + dataValue.label;
                                        // if the space is enough, select bigger
                                        $scope.modalBodyHeight = clientSize.height * 0.9 > lookupModal.offsetHeight ? clientSize.height * 0.8 : lookupModalBody.offsetHeight;
                                        $scope.treeRoot = [];

                                        $scope.i18nMessage = i18nData;
                                        $scope.i18nMessages = [];

                                        $scope.isObject = function (data) {
                                            return typeof (data) == 'object' ? true : false;
                                        };

                                        $scope.i18nTreeObject = function (nodeId, source, target) {
                                            for (var key in source) {
                                                if ($scope.isObject(source[key])) {
                                                    target.push({
                                                        nodeId: (nodeId) ? nodeId + "." + key : key,
                                                        name: key,
                                                        children: []
                                                    });
                                                    $scope.i18nTreeObject(target[target.length - 1].nodeId, source[key], target[target.length - 1].children);
                                                } else {
                                                    target.push({
                                                        nodeId: (nodeId) ? nodeId + "." + key : key,
                                                        name: key + " (" + source[key] + ")"
                                                    });
                                                }
                                            }
                                        };

                                        $scope.i18nTreeObject("", $scope.i18nMessage, $scope.i18nMessages);

                                        $scope.treeRoot = [{
                                            name: "I18N Message",
                                            nodeId: "rootNode",
                                            image: "../image/M.png",
                                            children: $scope.i18nMessages
                                        }];
                                        $scope.treeOptions = {
                                            titleField: ['name'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
                                            generateNodeId: generateNodeId,
                                            onSelectNode: function (node) {
                                                $scope.selectedNode = node;
                                            }
                                        };

                                        $scope.cancel = function () {
                                            $scope.$dismiss();
                                        };
                                        $scope.ok = function () {
                                            $scope.$close($scope.selectedNode);
                                        };
                                    }
                                ]
                            }).result.then(function (selectedNode) {
                                if ('undefined' != typeof selectedNode) {
                                    if (selectedNode.nodeId && !selectedNode.children) {
                                        dataValue.i18n = selectedNode.nodeId;
                                    } else if (selectedNode.nodeId && selectedNode.children) {
                                        bootbox.alert("The selected one was not a leaf node. Please select a leaf node");
                                    }
                                } else {
                                    bootbox.alert("Please select a node");
                                }
                            }, function () {
                            });
                        });
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.lookupDataValue);
                    };
                }
            ]
        }).result.then(function (lookupDataValue) {
            if ("Add" == mode) {
                lookupDataValue.effectiveDate = getUTCDateString(lookupDataValue.effectiveDate, "00:00:00");
                lookupDataValue.expirationDate = getUTCDateString(lookupDataValue.expirationDate, "23:59:59");
                createLookupDataValue(lookupDataValue);
            } else if ("Update" == mode) {
                updateLookupDataValue(lookupDataValue);
            }
        }, function () {
        });
    };

    $scope.makeDataValueInactive = function (dataValue) {
        dataValue.active = No;
        updateLookupDataValue(dataValue);
    };

    $scope.makeDataValueActive = function (dataValue) {
        dataValue.active = Yes;
        updateLookupDataValue(dataValue);
    };

    $scope.sortDataValueList = function (column) {
        if ($scope.dataValueListOrderColumn === column) {
            $scope.dataValueListOrderAsc = !$scope.dataValueListOrderAsc;
        } else $scope.dataValueListOrderAsc = true;
        $scope.dataValueListOrderColumn = column;
        getLookupDataValueList($scope.dataValueListCurrentPage);
    };


    function getHistory(data, callback) {
        $scope.selectedDataValue = data;
        $scope.histories = [];
        var param = {
            id: data.id,
            ORDER_BY: "transactionId desc"
        };

        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.lookup-GetLookupHistory.json');
        api.post({
            success: function (o) {
                var history = [];
                for (var i in o.data.data) {
                    history.push(o.data.data[i]);
                }
                $scope.histories = history;
                if (callback) {
                    callback();
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.showHistory = function (data) {
        var callback = $scope.loadHistory;
        getHistory(data, callback);
    };

    $scope.loadHistory = function () {
        var scope = $scope;
        var value, orig1, orig2, dv, panes = 2, highlight = true, connect = "align", collapse = true;
        // var $b = parent.$B;
        var pageSize = 10;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/lookup/lookup-data-history.html'),
            windowClass: 'app-modal-window-11',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = "Lookup Data Versioning";
                    $scope.selectedNode = scope.selectedNode;
                    $scope.selectedNodeType = scope.selectedNodeType;
                    $scope.selectedDataValue = scope.selectedDataValue;
                    $scope.historiesLeft = scope.histories;
                    $scope.historiesRight = scope.histories;
                    $scope.selectedHistoryLeft = scope.histories[0];
                    $scope.selectedHistoryRight = scope.histories[0];
                    $scope.selectedNumLeft = 0;
                    $scope.selectedNumRight = -1;

                    function initUI() {
                    }

                    function getHistoryContent(selectedData, selectedView, callback) {
                        if ("L" === selectedView) {
                            value = selectedData.value;
                        } else if ("R" === selectedView) {
                            orig2 = selectedData.value;
                        }
                        if (callback) {
                            callback();
                        }
                    }

                    $scope.selectHistoryLeft = function (idx, selectedData) {
                        $scope.selectedNumLeft = idx;
                        $scope.selectedHistoryLeft = idx === -1 ? $scope.historiesLeft[0] : selectedData;
                        getHistoryContent($scope.selectedHistoryLeft, "L");
                    };
                    $scope.selectHistoryRight = function (idx, selectedData) {
                        $scope.selectedNumRight = idx;
                        $scope.selectedHistoryRight = idx === -1 ? $scope.historiesRight[0] : selectedData;
                        getHistoryContent($scope.selectedHistoryRight, "R");
                    };

                    $scope.loadMore = function (view) {
                        pageSize = 10000;
                        $scope.getHistoryList(view);
                    };

                    $scope.getHistoryList = function (view, callback) {
                        var param = {
                            id: $scope.selectedDataValue.id,
                            ORDER_BY: "transactionId desc",
                            pageSize: pageSize
                        };

                        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.lookup-GetLookupHistory.json');
                        api.post({
                            success: function (o) {
                                var history = [];
                                for (var i in o.data.data) {
                                    history.push(o.data.data[i]);
                                }
                                $scope.historiesLeft = history;
                                $scope.historiesRight = history;

                                if (callback) {
                                    if (callback === initHistoryView) {
                                        $(".list-group").scrollTop(0);
                                        $scope.selectedHistoryLeft = history[0];
                                        $scope.selectedHistoryRight = history[0];
                                        $scope.selectedNumLeft = 0;
                                        $scope.selectedNumRight = -1;
                                    }
                                    callback();
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    };

                    $scope.isValueDiff = function (name) {
                        if (null != name) {
                            return $scope.selectedHistoryLeft[name] !== $scope.selectedHistoryRight[name];
                        }
                        return false;
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };

                    function resize() {
                        var historyContainer = document.getElementById("historyContainer");
                        var historyContentHeader = document.getElementById("historyContentHeader");
                        var historyContent = document.getElementById("historyContent");
                        if (historyContent) historyContent.style.height = (historyContainer.offsetHeight - historyContentHeader.offsetHeight) + "px";
                    }

                    $(window).on('resize', function () {
                        resize();
                    });

                    $scope.setHeight = function () {
                        setHeight();
                    };

                    function initHistoryView() {
                        var callback = getHistoryContent($scope.selectedHistoryLeft, "R", initUI());
                        getHistoryContent($scope.selectedHistoryRight, "L", callback);
                    }

                    (function () {
                        initHistoryView();
                        $timeout(function () {
                            resize();
                        }, 100);
                    })();

                }
            ]
        }).result.then(function (i18nData) {

        }, function () {

        });
    };

    $scope.setHeight = function () {
        setHeight();
    };

    $scope.importLookupValue = function () {
        var url = "import-lookup-select.jsp?typeId=" + encodeURIComponent($scope.selectedNode.id);
        angularExt.openModalWindow(url, 'bootbox-dialog-frame-95 bootbox-dialog-overflow-hidden');
    }

}]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";

    var historyModal = document.getElementById("historyModal");
    if (historyModal) historyModal.style.height = (height * 0.9) + "px";
}
