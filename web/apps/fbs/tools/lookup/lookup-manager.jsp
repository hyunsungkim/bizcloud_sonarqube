<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AppDev Lookup Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("lookup-manager.js");
    </script>
    <style>
        tr.list td {
            padding: 2px 5px 2px 5px;
        }

        .i18n-wrapper, .subtype-wrapper {
            position: relative;
        }

        .i18n-clear-icon {
            position: absolute;
            color: #848484;
            top: 8px;
            right: 90px;
        }

        .subtype-clear-icon {
            position: absolute;
            color: #848484;
            top: 8px;
            right: 120px;
        }

        .clear-icon-show {
            display: block;
            cursor: pointer;
        }

        .clear-icon-hide {
            display: none;
        }

        .bootbox-dialog-frame-95 .modal-dialog {
            width: 80% !important;
            height: 600px!important;
        }
    </style>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>AppDev Lookup Data Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row flex-nowrap" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div style="padding-bottom: 0;" id="leftPanelHeader">
                    <div class="bio-left-title">
                        <div class="title"><h5>Types</h5></div>
                        <div class="i-btn">
                            <button class="bf-button p-component bf-button-icon-only" title="Add" ng-click="addDataType();$event.stopPropagation();"><i class="fa fa-plus"></i></button>
                            <button class="bf-button p-component bf-button-icon-only" title="Edit" ng-click="editDataType();$event.stopPropagation();" ng-disabled="selectedNodeType !== DataType"><i class="fa fa-pencil"></i></button>
                            <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshDataTypes();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-block" id="treeContainer">
                    <div class="form-group form-group-sm col-md-12">
                        <input type="text" ng-model="treeOptions.filter.type" class="searchinput" placeholder="Enter type to filter" style="margin-bottom: 10px;">
                        <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                    </div>
                    <div id="treePanelContainer" class="panelContainer">
                        <tree nodes='treeRoot' options="treeOptions"></tree>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary col-md-12" id="rightPanelContainer">
                <div id="rightPanelHeader">
                    <div class="bio-right-title">
                        <div class="title">
                            <h3 ng-if="!lookupDataValueList">{{listMode === EvaluatedListMode ? 'Evaluated ' : ''}}Values</h3>
                            <h3 ng-if="lookupDataValueList && selectedNodeType === DataType">
                                {{listMode === EvaluatedListMode ? 'Evaluated ' : ''}}Values for <span title="ID: {{selectedNode.id || 'None'}}">{{selectedNode.type}}</span>
                                <%--on <span title="ID: {{selectedNode.parent.id || 'None'}}">{{selectedNode.parent.name}}</span>--%>
                                <span class="id-wrap" ng-if="selectedNode.id && listMode !== EvaluatedListMode">ID&nbsp;:&nbsp;{{selectedNode.id}}</span>
                            </h3>
                            <h3 ng-if="lookupDataValueList && selectedNodeType !== DataType">
                                All {{listMode === EvaluatedListMode ? 'Evaluated ' : ''}}Values for <span title="ID: {{selectedNode.id || 'None'}}">{{selectedNode.name}}</span>
                                <span class="id-wrap" ng-if="selectedNode.id && listMod !== EvaluatedListMode">ID&nbsp;:&nbsp;{{selectedNode.id}}</span>
                            </h3>
                        </div>
                        <div class="i-btn" ng-if="lookupDataValueList && !selectedNode.rawDataSrcId">
                            <div class="space"></div>
                            <button class="p-button p-component p-button-text-only p-button-success" ng-click="importLookupValue();$event.stopPropagation();"
                                    ng-if="selectedNodeType === DataType && listMode !== EvaluatedListMode" ng-disabled="!lookupDataValueList">Import
                            </button>
                            <button class="p-button p-component p-button-text-only" ng-click="editDataValue('Add');$event.stopPropagation();"
                                    ng-if="selectedNodeType === DataType && listMode !== EvaluatedListMode" ng-disabled="!lookupDataValueList">Add Value
                            </button>
                            <button class="p-button p-component p-button-text-only" ng-if="listMode !== EvaluatedListMode" ng-click="viewEvaluatedDataValue();$event.stopPropagation();">Evaluated Values</button>
                            <button class="p-button p-component p-button-text-only" ng-if="listMode === EvaluatedListMode" ng-click="viewOriginalDataValue();$event.stopPropagation();">Original Values</button>
                            <button class="p-button p-component p-button-text-only p-button-secondary" ng-click="refreshDataValueList();$event.stopPropagation();"
                                    ng-if="selectedNodeType === DataType && listMode !== EvaluatedListMode" ng-disabled="!lookupDataValueList">Refresh
                            </button>
                        </div>
                        <div ng-if="lookupDataValueList && !selectedNode.rawDataSrcId">
                            <div class="bf-clearfix" ng-if="myTimeInfo">Base Time: {{myTimeInfo.clientDate | makeDate | date:'MM/dd/yyyy HH:mm:ss'}}</div>
                        </div>
                    </div>
                    <div ng-if="lookupDataValueList && !selectedNode.rawDataSrcId">
                        <div class="bio-total" ng-if="lookupDataValueList">Total: {{lookupDataValueList.totalCount}}</div>
                    </div>
                </div>
                <div class="card-block panelContainer" style="overflow-x: auto;">
                    <div ng-if="!lookupDataValueList">
                        <div class="text-center">Please select a type to see lookup data values</div>
                    </div>
                    <div id="DataValue" ng-if="lookupDataValueList">
                        <div ng-if="lookupDataValueList.data.length == 0">
                            <div class="card text-white bg-dark border-secondary" ng-if="selectedNode.rawDataSrcId">
                                <div class="card-header card-header-xs bg-info">
                                    <h5 class="panel-title pd5">External Lookup Data</h5>
                                </div>
                                <div class="bf-table-3 pd20">
                                    <table ng-if="rawDataSource" class="table table-striped table-condensed">
                                        <tr>
                                            <th>Type</th>
                                            <td>{{rawDataSource.type}}</td>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <td>{{rawDataSource.description}}</td>
                                        </tr>
                                        <tr>
                                            <th>Method</th>
                                            <td>{{rawDataSource.method}}</td>
                                        </tr>
                                        <tr>
                                            <th>URL/Query</th>
                                            <td>{{rawDataSource.url}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="alert alert-warning alert-xs" role="alert" ng-if="selectedNode.rawDataSrcId" style="margin-top: 10px;">
                                Please use API Integration Management to change this external lookup data setting.
                            </div>
                            <div class="text-center" ng-if="!selectedNode.rawDataSrcId">No values</div>
                        </div>
                        <div class="bf-table-2">
                            <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%>" ng-if="lookupDataValueList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold">
                                    <th>#</th>
                                    <th ng-click="sortDataValueList('district')" ng-if="selectedNodeType !== DataType">Level</th>
                                    <th ng-click="sortDataValueList('type')" ng-if="selectedNodeType !== DataType">Type</th>
                                    <th ng-click="sortDataValueList('label')">Label</th>
                                    <th ng-click="sortDataValueList('i18n')">Label for I18N</th>
                                    <th ng-click="sortDataValueList('value')">Value</th>
                                    <th ng-click="sortDataValueList('active')">Active</th>
                                    <%--<th ng-click="sortDataValueList('readonly')">Readonly</th>--%>
                                    <th ng-click="sortDataValueList('displayOrder')">Display Order</th>
                                    <th ng-click="sortDataValueList('effectiveDate')">Effective Date</th>
                                    <th ng-click="sortDataValueList('expirationDate')">Expiration Date</th>
                                    <th ng-click="sortDataValueList('subType')" nowrap>Sub Type</th>
                                    <th ng-click="sortDataValueList('transactionDate')" nowrap>Last Modified</th>
                                    <th ng-if="listMode !== EvaluatedListMode"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="dataValue in lookupDataValueList.data" title="ID={{dataValue.id}}" class="list">
                                    <td>{{dataValue.ROW_NUMBER}}</td>
                                    <td ng-if="selectedNodeType !== DataType">{{dataValue.district}}</td>
                                    <td ng-if="selectedNodeType !== DataType">{{dataValue.type}}</td>
                                    <td>{{dataValue.label}}</td>
                                    <td>{{dataValue.i18n}}</td>
                                    <td>{{dataValue.value}}</td>
                                    <td>{{dataValue.active == Yes ? 'Active' : 'Inactive'}}</td>
                                    <%--<td>{{dataValue.readonly == Yes ? 'Yes' : 'No'}}</td>--%>
                                    <td>{{dataValue.displayOrder}}</td>
                                    <td>{{dataValue.effectiveDate | makeDate | date:'MM/dd/yyyy'}}</td>
                                    <td>{{dataValue.expirationDate | makeDate | date:'MM/dd/yyyy'}}</td>
                                    <td><a ng-click="openSubType(dataValue);$event.stopPropagation();">{{dataValue.subType}}</a></td>
                                    <td>{{dataValue.transactionDate | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm:ss'}}</td>
                                    <td class="text-nowrap right-btn" ng-if="listMode !== EvaluatedListMode">
                                        <button type="button" class="p-button p-component p-button-text-only" ng-click="editDataValue('Update', dataValue);$event.stopPropagation();">Edit</button>
                                        <button type="button" class="p-button p-component p-button-warning p-button-text-only" ng-if="dataValue.active === Yes" ng-click="makeDataValueInactive(dataValue);$event.stopPropagation();">Inactive</button>
                                        <button type="button" class="p-button p-component p-button-warning p-button-text-only" ng-if="dataValue.active !== Yes" ng-click="makeDataValueActive(dataValue);$event.stopPropagation();">Active</button>
                                        <button type="button" class="p-button p-component p-button-info p-button-text-only" ng-click="showHistory(dataValue);$event.stopPropagation();">History</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="lookupDataValueList.totalCount > dataValueListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="lookupDataValueList.totalCount" items-per-page="dataValueListPageSize" ng-model="$parent.$parent.dataValueListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changeDataValueListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>