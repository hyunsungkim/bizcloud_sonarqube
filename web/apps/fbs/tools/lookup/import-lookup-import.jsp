<%@ page import="com.bizflow.io.core.db.mybatis.session.SqlSessionFactoryManager" %>
<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.core.file.processor.CsvProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.ExcelProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.FileProcessor" %>
<%@ page import="com.bizflow.io.core.json.util.JSONUtil" %>
<%@ page import="com.bizflow.io.core.lang.LongData" %>
<%@ page import="com.bizflow.io.core.lang.ObjectData" %>
<%@ page import="com.bizflow.io.core.util.NumberUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.service.AdhocService" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlTransactionDAO" %>
<%@ page import="com.bizflow.io.services.data.processor.RecordDBProcessor" %>
<%@ page import="org.apache.commons.csv.CSVFormat" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.bizflow.io.core.json.JSONArray" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlDAO" %>
<%@ page import="com.bizflow.io.core.json.JSONMap" %>
<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="importApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Excel - Importing</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../../tools/include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js")
        mars$require$.script("import-lookup-import.js");
    </script>
</head>
<%
    AdhocService.getInstance().initializeRequest(request, response);

    final String filename = request.getParameter("filename");
    String filePath = request.getParameter("file");
    File file = new File(filePath);
    if (file.exists()) {
        String lowercaseFilename = filename.toLowerCase();

        FileProcessor fileProcessor = null;
        String csvFormat = ServletUtil.getParameterValue(request, "csvFormat", "DEFAULT");
        if (lowercaseFilename.endsWith(".csv")) {
            if ("TDF".equalsIgnoreCase(csvFormat)) {
                fileProcessor = new CsvProcessor(file, CSVFormat.TDF);
            } else {
                fileProcessor = new CsvProcessor(file, CSVFormat.DEFAULT);
            }
        } else {
            fileProcessor = new ExcelProcessor(file);
        }

        final String typeId = request.getParameter("typeId");
        HashMap param = new HashMap();
        param.put("id", typeId);
        JSONMap lookupInfo = (JSONMap) SqlDAO.selectOne("fbs", "lookup.GetLookupDataType", param);
        if (null != lookupInfo) {
            JSONObject _label = new JSONObject();
            _label.put("name", "label");
            _label.put("type", "String");
            _label.put("mandatory", true);
            JSONObject _value = new JSONObject();
            _value.put("name", "value");
            _value.put("type", "String");
            _value.put("mandatory", true);
            JSONObject _active = new JSONObject();
            _active.put("name", "active");
            _active.put("type", "String");
            _active.put("defaultValue", "Y");
            JSONObject _readonly = new JSONObject();
            _readonly.put("name", "readonly");
            _readonly.put("type", "String");
            _readonly.put("defaultValue", "N");
            JSONObject _effectiveDate = new JSONObject();
            _effectiveDate.put("name", "effectiveDate");
            _effectiveDate.put("type", "Date");
            _effectiveDate.put("format", "yyyy-MM-dd");
            JSONObject _expirationDate = new JSONObject();
            _expirationDate.put("name", "expirationDate");
            _expirationDate.put("type", "Date");
            _expirationDate.put("format", "yyyy-MM-dd");

            JSONArray _dataSchema = new JSONArray();
            _dataSchema.put(_label);
            _dataSchema.put(_value);
            _dataSchema.put(_active);
            _dataSchema.put(_readonly);
            _dataSchema.put(_effectiveDate);
            _dataSchema.put(_expirationDate);

            JSONObject lookupContent = new JSONObject();
            lookupContent.put("dataSchema", _dataSchema);
            lookupInfo.put("content", lookupContent);

            try {
%>
<body ng-controller="importAppCtrl" onload="_onload()" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="container">
    <div class="text-center">
        <h1>Import Data</h1>
    </div>
    <div class="card text-<%=textColor%> bg-<%=bgColor%>">
        <div class="card-header">
            <h3 class="card-title"><span class="glyphicon glyphicon-th-large"></span>Importing...</h3>
        </div>
        <div class="progress" style="margin-bottom: 0;">
            <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;">0</div>
        </div>
        <div id="alertSuccess" class="alert alert-success" role="alert" style="display: none;margin:0;">All data have been imported. It took <span id="howLong"></span>ms.</div>
        <%
            String errorMessage = null;
            Date date_s = new Date();
            final LongData currentRecordNumber = new LongData();
            final ObjectData objectData = new ObjectData();
            final String dbSession = "fbs";
            final String dbQuery = "lookup.InsertOrUpdateLookupDataValueByValue";
            final JSONArray dataSchema = lookupContent.getJSONArray("dataSchema");
            final Map<String, String> columnMapping = new HashMap<>();
            final int dataSchemaLen = dataSchema.length();
            for (int i = 0; i < dataSchemaLen; i++) {
                String name = dataSchema.getJSONObject(i).getString("name");
                columnMapping.put(name, request.getParameter("schema-" + name));
            }

            if (SqlSessionFactoryManager.getInstance().isExistSessionFactory(dbSession)) {
                final SqlTransactionDAO sqlDao = new SqlTransactionDAO(dbSession);
                try {
                    final JspWriter writer = out;
                    final boolean csvFile = fileProcessor instanceof CsvProcessor;
                    fileProcessor.process(new RecordDBProcessor(false) {
                        int oldPercent = 0;
                        int count = 1;

                        @Override
                        public String getMapColumnName(String name) {
                            return name;
                        }

                        private Map getParamMap(Map dataMap) {
                            JSONObject content = new JSONObject();
                            for (int i = 0; i < dataSchemaLen; i++) {
                                JSONObject schema = dataSchema.getJSONObject(i);
                                String name = schema.getString("name");
                                String type = schema.getString("type");
                                String column = columnMapping.get(name);
                                if (StringUtil.isNotNullBlank(column)) {
                                    if ("$index".equalsIgnoreCase(column)) {
                                        content.put(name, count);
                                    } else {
                                        Object value = dataMap.get(column);
                                        if (null != value) {
                                            if ("Integer".equalsIgnoreCase(type)) {
                                                try {
                                                    value = NumberUtil.parseInt(value);
                                                } catch (NumberFormatException e) {
                                                }
                                            } else if ("Date".equalsIgnoreCase(type)) {
                                                if (value.toString().length() >= 10) value += " 00:00:00";
                                                else value = null;
                                            }
                                            content.put(name, value);
                                        }
                                    }
                                }
                            }

                            Map<String, Object> map = new HashMap<>();
                            map.put("typeId", typeId);
                            map.put("label", String.valueOf(content.get("label")));
                            map.put("value", String.valueOf(content.get("value")));
                            map.put("active", content.optString("active", "Y"));
                            map.put("readonly", content.optString("readonly", "N"));
                            map.put("effectiveDate", content.opt("effectiveDate"));
                            map.put("expirationDate", content.opt("expirationDate"));
                            map.put("content", content);
                            map.put("displayOrder", count++);

                            return map;
                        }

                        @Override
                        public void processRecord(long recordNumber, long totalRecordNumber) {
                            currentRecordNumber.setLongData(recordNumber);
                            Map dataMap = getRecordDataMap();
                            objectData.setObject(dataMap);

                            Map paramMap = getParamMap(dataMap);
                            sqlDao.insert(dbQuery, paramMap);

                            int percent = (int) (((csvFile ? (recordNumber - 1) : recordNumber) * 100) / totalRecordNumber);

                            if (oldPercent != percent) {
                                oldPercent = percent;
                                try {
                                    writer.print("<script>");
                                    writer.print("$('#progress').attr('aria-valuenow', ");
                                    writer.print(percent);
                                    writer.print(").css('width', '");
                                    writer.print(percent);
                                    writer.print("%').html('");
                                    writer.print(percent);
                                    writer.print("%');");
                                    writer.print("</script>");
                                    writer.flush();
                                } catch (IOException e) {
                                }
                            }
                        }
                    });

                    sqlDao.commit();
                } catch (Exception e) {
                    sqlDao.rollback();
                    StringBuilder builder = new StringBuilder();
                    builder.append("RecordNumber=").append(currentRecordNumber.getLongData()).append("\r\n, ");
                    builder.append("Record=").append(null != objectData.getObject() ? JSONUtil.mapToJsonString((Map) objectData.getObject()) : "").append("\r\n, ");
                    builder.append(ExceptionUtil.getOriginalExceptionString(e));
                    errorMessage = builder.toString();
                } finally {
                    sqlDao.close();
                }
            } else {
                errorMessage = "The DB Session \"" + dbSession + "\" does not exist";
            }
        %>
    </div>

    <%if (null != errorMessage) {%>
    <div id="alertError" class="alert alert-danger" role="alert"><%=HtmlUtil.getHtmlText(errorMessage)%>
    </div>
    <%} else {%>
    <script>
        $("#howLong").html(<%=System.currentTimeMillis() - date_s.getTime()%>);
        setTimeout(function () {
            $("#alertSuccess").css("display", "");
        }, 1000);
    </script>
    <%}%>

    <div class="col-md-12 text-center" style="padding-top: 20px;">
        <div id="btnGroup1" style="display: none;">
            <button type="button" class="bf-button p-component p-button p-button-text-only" onclick="done()">Ok</button>
        </div>
        <div id="btnGroup2" style="display: none;">
            <button type="button" class="bf-button p-component p-button p-button-text-only p-button-secondary" onclick="closeWindow()">Close</button>
        </div>
    </div>
</div>
<br><br><br>
</body>
<%
        } finally {
            fileProcessor.close();
            file.delete();
        }
    } else {
        throw new RuntimeException("Lookup Type does not exist");
    }
} else {
%>
<body onload="_onload()">
<div class="container">
    <div class="page-header text-center">
        <h1>Import Data</h1>
    </div>
    <div class="card" class="text-<%=textColor%> bg-<%=bgColor%>">
        <div class="card-header">Importing Data</div>
        <div class="card-body">
            <div class="alert alert-danger" role="alert">File <%=file.getName()%> does not exist.</div>
        </div>
    </div>

    <div class="col-md-12 text-center">
        <div id="btnGroup1" style="display: none;">
            <button type="button" class="bf-button p-component p-button p-button-text-only" onclick="done()">Ok</button>
        </div>
        <div id="btnGroup2" style="display: none;">
            <button type="button" class="bf-button p-component p-button p-button-text-only p-button-secondary" onclick="closeWindow()">Close</button>
        </div>
    </div>
</div>
<br><br><br>
</body>
<%
    }
%>
</html>