<%@ page import="com.bizflow.io.core.file.processor.CsvProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.ExcelProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.FileProcessor" %>
<%@ page import="com.bizflow.io.core.web.MultipartFile" %>
<%@ page import="com.bizflow.io.core.web.MultipartFormData" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.apache.commons.csv.CSVFormat" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlDAO" %>
<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.bizflow.io.core.json.JSONMap" %>
<%@ page import="com.bizflow.io.core.json.JSONArray" %>
<%@include file="../include/tool-common.jsp" %>

<%
    MultipartFormData formData = new MultipartFormData(request);
    MultipartFile multipartFile = formData.getFile("uploadFile");
    String filename = multipartFile.getName();
    File file = multipartFile.getFile();
    String mode = formData.getParameterValue("mode", "");
    String typeId = formData.getParameterValue("id");

    HashMap param = new HashMap();
    param.put("id", typeId);
    JSONMap lookupInfo = (JSONMap) SqlDAO.selectOne("fbs", "lookup.GetLookupDataType", param);
    if (null != lookupInfo) {
        JSONObject _label = new JSONObject();
        _label.put("name", "label");
        _label.put("type", "String");
        _label.put("mandatory", true);
        JSONObject _value = new JSONObject();
        _value.put("name", "value");
        _value.put("type", "String");
        _value.put("mandatory", true);
        JSONObject _active = new JSONObject();
        _active.put("name", "active");
        _active.put("type", "String");
        _active.put("defaultValue", "Y");
        JSONObject _readonly = new JSONObject();
        _readonly.put("name", "readonly");
        _readonly.put("type", "String");
        _readonly.put("defaultValue", "N");
        JSONObject _effectiveDate = new JSONObject();
        _effectiveDate.put("name", "effectiveDate");
        _effectiveDate.put("type", "Date");
        _effectiveDate.put("format", "yyyy-MM-dd");
        JSONObject _expirationDate = new JSONObject();
        _expirationDate.put("name", "expirationDate");
        _expirationDate.put("type", "Date");
        _expirationDate.put("format", "yyyy-MM-dd");

        JSONArray _dataSchema = new JSONArray();
        _dataSchema.put(_label);
        _dataSchema.put(_value);
        _dataSchema.put(_active);
        _dataSchema.put(_readonly);
        _dataSchema.put(_effectiveDate);
        _dataSchema.put(_expirationDate);

        JSONObject lookupContent = new JSONObject();
        lookupContent.put("dataSchema", _dataSchema);
        lookupInfo.put("content", lookupContent);

        FileProcessor fileProcessor = null;
        String lowercaseFilename = filename.toLowerCase();
        String csvFormat = formData.getParameterValue("csvFormat", "DEFAULT");
        if (lowercaseFilename.endsWith(".csv")) {
            if ("TDF".equalsIgnoreCase(csvFormat)) {
                fileProcessor = new CsvProcessor(file, CSVFormat.TDF);
            } else {
                fileProcessor = new CsvProcessor(file, CSVFormat.DEFAULT);
            }
        } else {
            fileProcessor = new ExcelProcessor(file);
        }

        try {
%>

<!DOCTYPE html>
<html ng-app="importApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Excel - Review</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../../tools/include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js")
        mars$require$.script("import-lookup-review.js");
    </script>
    <style>
        div.title {
            min-width: 200px;
        }
    </style>
</head>
<body ng-controller="importAppCtrl" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="container">
    <div class="text-center">
        <h1>Mapping</h1>
    </div>
    <div class="card text-<%=textColor%> bg-<%=bgColor%>">
        <div class="card-body">
            <form name="form" action="import-lookup-import.jsp" method="post" enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
                <input type="hidden" name="file" value="<%=file.getAbsolutePath()%>">
                <input type="hidden" name="filename" value="<%=Encode.forHtmlAttribute(filename)%>">
                <input type="hidden" name="csvFormat" value="<%=Encode.forHtmlAttribute(csvFormat)%>">
                <input type="hidden" name="mode" value="<%=Encode.forHtmlAttribute(mode)%>">
                <input type="hidden" name="typeId" value="<%=typeId%>">
                <div class="card text-<%=textColor%> bg-<%=bgColor%>">
                    <div>
                        <h5>Column Mapping</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%>">
                            <tr>
                                <th>Lookup Column</th>
                                <th>Excel Column</th>
                                <th>Note</th>
                            </tr>
                            <%
                                JSONArray dataSchema = lookupContent.getJSONArray("dataSchema");
                                int dataSchemaLen = dataSchema.length();
                                for (int i = 0; i < dataSchemaLen; i++) {
                                    JSONObject schema = dataSchema.getJSONObject(i);
                                    String schemaName = schema.getString("name");
                                    boolean mandatory = schema.optBoolean("mandatory", false);
                                    String defaultValue = schema.optString("defaultValue", null);
                                    String format = schema.optString("format", null);
                            %>
                            <tr>
                                <td><%=schemaName%> <%=mandatory ? "*" : ""%>
                                </td>
                                <td style="width: 50%;">
                                    <select name="schema-<%=schemaName%>" style="width:90%;"
                                            <%if (mandatory) {%>
                                            ng-model="mapping.<%=schemaName%>" ng-required="<%=mandatory%>"
                                            <%}%>>
                                        <option></option>
                                        <%
                                            List<String> columnNameList = fileProcessor.getColumnNameList();
                                            for (String columnName : columnNameList) {
                                                boolean theSame = columnName.equalsIgnoreCase(schemaName);
                                        %>
                                        <option value="<%=columnName%>" <%=theSame ? "selected" : ""%> ><%=columnName%>
                                        </option>
                                        <%
                                            }
                                        %>
                                        <option value="$index">$index</option>
                                    </select>
                                </td>
                                <td>
                                    <% if (null != defaultValue) {%>
                                    Default Value: <%=defaultValue%>
                                    <%}%>
                                    <% if (null != format) { %>
                                    Format: <%=format%>
                                    <%}%>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                            <tr>
                                <td colspan="3" style="text-align: right">
                                    *: Mandatory
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="text-center" style="padding-top: 10px;">
                    <button type="button" class="bf-button p-component p-button p-button-secondary p-button-text-only" ng-click="goBack();">Go back</button>
                    <button type="submit" class="bf-button p-component p-button p-button-text-only" ng-disabled="!isReadyToImport()">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
<br><br><br>
</body>
</html>
<%
        } finally {
            fileProcessor.close();
        }
    } else {
        throw new RuntimeException("Lookup Type does not exist");
    }
%>