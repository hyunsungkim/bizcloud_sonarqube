<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="../include/tool-common.jsp" %>

<%
    String mode = ServletUtil.getParameterValue(request, "mode", "");
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Excel - Select</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../../tools/include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js")
        mars$require$.script("import-lookup-select.js");
    </script>
</head>
<body onload="_onload();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="container">
    <div class="text-center">
        <h1>Import Lookup</h1>
    </div>
    <div class="card text-<%=textColor%> bg-<%=bgColor%>">
        <div class="card-header">
            <h3 class="card-title"><span class="glyphicon glyphicon-th-large"></span>&nbsp;Select File<br></h3>
            <span style="color: #7a7a7a;">Please select a csv with comma separated format or an excel file that has records on the first work sheet. The first row of the file must be column headers.</span>
        </div>
        <div class="card-body">
            <div class="max-width no-border">
                <form action="import-lookup-review.jsp" method="post" enctype="multipart/form-data" accept-charset="UTF-8">
                    <input type="file" name="uploadFile" id="uploadFile" style="padding-bottom: 40px;" accept=".xlsx,.xls,.csv" class="form-control btn btn-default max-width" onchange="isReadExcelFile();" required/>
                    <input type="hidden" name="mode" value="<%=Encode.forHtmlAttribute(mode)%>">
                    <input type="hidden" name="id" value='<%=Encode.forHtmlAttribute(request.getParameter("typeId"))%>'>
                    <div style="padding: 5px"></div>
                    <div>
                        CSV Format:
                        <select name="csvFormat">
                            <option>Select one for CSV</option>
                            <option value="DEFAULT" selected>Comma</option>
                            <option value="TDF">Tab</option>
                        </select>
                    </div>
                    <div class="text-center" style="padding-top: 10px;">
                        <button type="submit" class="bf-button p-component p-button p-button-text-only">Upload</button>
                        <button type="button" class="bf-button p-component p-button p-button-text-only p-button-secondary" id="btnGroup1" style="display: none;" onclick="closeWindow()">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br><br><br>
</body>
</html>