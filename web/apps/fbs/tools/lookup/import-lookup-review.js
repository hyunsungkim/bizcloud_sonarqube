var app = angular.module('importApp', []).controller('importAppCtrl', ['$scope', function ($scope) {
    $scope.mapping = {};

    $scope.goBack = function () {
        window.history.back();
    }

    $scope.isReadyToImport = function () {
        return $scope.mapping.label && $scope.mapping.value;
    };
}]);

