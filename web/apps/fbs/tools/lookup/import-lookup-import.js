var app = angular.module('importApp', []).controller('importAppCtrl', ['$scope', function ($scope) {
}]);

function done() {
}

function closeWindow() {
    var scope = angularExt.getFrameParentAngularScope('body');
    if (scope && scope.refreshDataValueList) {
        scope.refreshDataValueList();
    }
    angularExt.closeModalWindow();
}

function _onload() {
    if (window.frameElement && window.frameElement.contentWindow) {
        document.getElementById("btnGroup2").style.display = "";
    } else {
        document.getElementById("btnGroup1").style.display = "";
    }
}

