var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
    , 'ng.jsoneditor'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//) || config.url.match(/myTimeInfo.json/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$timeout', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $timeout, $filter, $uibModal, blockUI, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);
    $scope.projectId = parameter.getParameterValue("project_id", '');
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;
    // Constants
    var ZeroId = 'N00000000000000000000000000000000';
    var TreeRoot = 'TreeRoot';
    var Application = 'Application';
    var Workspace = 'Workspace';
    var Project = 'Project';
    var I18N = 'I18N';
    var ApplicationNodeId = "A0";
    var Original = 'Original';
    var Merged = 'Merged';

    var historyIndex = -1;
    $scope.Original = Original;
    $scope.Merged = Merged;

    $scope.I18N = I18N;
    $scope.i18nDataMode = Original;
    $scope.i18nBaseLang = "en";

    $scope.jsonEditor = {
        options: {
            mode: 'tree',
            modes: ['tree', 'code'],
            enableTransform: false,
            change: function () {
                try {
                    historyIndex = $scope.jsonEditor.instance.history.index;
                } catch (e) {
                    historyIndex = -1;
                }
            }
        }
    };

    var blockUIStarted = false;
    var jsonHasNull = false;

    $scope.setJsonEditorInstance = function (instance) {
        $scope.jsonEditor.instance = instance;
    };

    function startBlockUI() {
        blockUI.start();
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function getObjectType(node) {
        var objectType = null;

        if ('undefined' != typeof node.projectCount) {
            objectType = Workspace;
        } else if ('undefined' != typeof node.formCount) {
            objectType = Project;
        } else if ('undefined' != typeof node.lang) {
            objectType = I18N;
        } else if (node.nodeId === ApplicationNodeId) {
            objectType = Application;
        } else {
            objectType = TreeRoot;
        }

        return objectType;
    }

    function generateNodeId(node) {
        var objectType = getObjectType(node);
        var nodeId = "";
        if (Workspace === objectType) {
            nodeId = "W" + node.id;
        } else if (Project === objectType) {
            nodeId = "P" + node.id;
        } else if (I18N === objectType) {
            nodeId = "I" + node.id;
        } else {
            nodeId = ApplicationNodeId;
        }

        return nodeId;
    }

    function getNodeImage(node) {
        var image;
        var objectType = getObjectType(node);
        if (Workspace === objectType) {
            image = "../image/W.png";
        } else if (Project === objectType) {
            image = "../image/P.png";
        } else if (I18N === objectType) {
            image = "../image/I.png";
        } else {
            image = "../image/A.png";
        }

        return image;
    }

    $scope.treeOptions = {
        titleField: ['name', 'lang', 'district'], showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
        generateNodeId: generateNodeId,
        getNodeImage: getNodeImage,
        allowSelectNode: function () {
            var allowed = true;
            if (historyIndex >= 0) {
                allowed = confirm("The content has been modified. Do you want to discard the changes?");
            }
            return allowed;
        },
        onSelectNode: function (node, selectedByClick) {
            historyIndex = -1;
            $scope.i18nDataMode = Original;
            $scope.selectedNode = node;
            $scope.selectedNodeType = getObjectType(node);
            if (node.content) {
                $scope.jsonEditor.data = node.content;
                $scope.jsonEditor.instance.set(node.content);
                try {
                    $scope.jsonEditor.instance.expandAll();
                } catch (e) {
                }
            } else {
                $scope.jsonEditor.data = undefined;
            }
            $scope.treeOptions.selectedNode = node;
        },
        onExpandNode: function (node) {
            $scope.expandedNode = node;
        }
    };

    function getI18NTree(action) {
        var param = {
            jsonColumn: "content",
            LIKE_permission_LIKE: ["%M%", "P"]
        }
        if ($scope.projectId) param.projectId = $scope.projectId;
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.i18n-GetI18NTree@.json');
        api.post({
            success: function (o) {
                $scope.i18nData = o.data;
                $scope.treeRoot = [{
                    name: "I18N Message",
                    nodeId: "rootNode",
                    image: "../image/M.png",
                    children: $scope.i18nData
                }];

                stopBlockUI();
                if ("update" == action) {
                    showMessageInfo("Successfully Saved.");
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function get18NData(selectedNode, selectedNodeType, param, callback) {
        var apiName = null;
        if (TreeRoot === selectedNodeType) {
            apiName = 'fbs.i18n-GetI18NExt@.json';
        } else if (Application === selectedNodeType) {
            apiName = 'fbs.app-GetI18N@.json';
        } else if (Workspace === selectedNodeType) {
            apiName = 'fbs.workspace-GetI18N@.json';
            param.workspaceId = selectedNode.id;
        } else if (Project === selectedNodeType) {
            apiName = 'fbs.project-GetI18N@.json';
            param.projectId = selectedNode.id;
        } else if (I18N === selectedNodeType) {
            apiName = 'fbs.i18n-GetI18NExt@.json';
            param.id = selectedNode.id;
        }

        if (null != apiName) {
            var api = new AjaxService(marsContext.contextPath + '/services/data/get/' + apiName);
            api.post({
                success: function (o) {
                    callback(o);
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        } else {
            bootbox.alert("selected node type not found");
        }

    }

    function getLookupDataDistrict() {
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.lookup-GetLookupDataDistrict@.json');
        api.get({
            success: function (o) {
                $scope.lookupDataDistrict = o.data;
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    function getI18NLanguage() {
        var param = {
            "ARRAY_COLUMN": "lang"
        };
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.i18n-GetI18NLanguage.json');
        api.post({
            success: function (o) {
                $scope.i18nLanguages = o.data;
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    (function () {
        getI18NLanguage();
        getLookupDataDistrict();
        getI18NTree();
    })();

    $scope.refreshI18NData = function (action) {
        getI18NTree(action);
    };

    function selectWorkspace(selectedData, i18nData) {
        if (angular.isDefined(selectedData)) {
            i18nData.workspaceId = selectedData.originalObject.id;
        } else {
            i18nData.workspaceId = ZeroId;
        }
    }

    function selectProject(selectedData, i18nData) {
        if (angular.isDefined(selectedData)) {
            i18nData.projectId = selectedData.originalObject.id;
        } else {
            i18nData.projectId = ZeroId;
        }
    }

    function getAjaxConfig() {
        var config = undefined;
        if (parent && parent.$B) {
            config = {
                headers: {
                    "X-User-Agent-ID": parent.$B.DataStorage().uniqueAppName
                }
            }
        }
        return config;
    }

    function createI18NData(i18nData) {
        startBlockUI();
        if (i18nData.district === Application) {
            i18nData.workspaceId = ZeroId;
            i18nData.projectId = ZeroId;
        } else if (i18nData.district === Workspace) {
            i18nData.projectId = ZeroId;
        } else if (i18nData.district === Project) {
            i18nData.workspaceId = ZeroId;
        }
        var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.i18n-CreateI18N@.json');
        api.post({
            success: function (o) {
                $scope.refreshI18NData();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, i18nData, undefined, getAjaxConfig());
    }

    function updateI18NData(i18nData, callback) {
        startBlockUI();
        if (i18nData.district === Application) {
            i18nData.workspaceId = ZeroId;
            i18nData.projectId = ZeroId;
        } else if (i18nData.district === Workspace) {
            i18nData.projectId = ZeroId;
        } else if (i18nData.district === Project) {
            i18nData.workspaceId = ZeroId;
        }
        var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.i18n-UpdateI18N@.json');
        api.post({
            success: function (o) {
                $scope.refreshI18NData("update");
                if (callback) {
                    callback();
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, i18nData, undefined, getAjaxConfig());
    }

    function deleteI18NData(param) {
        var api = new AjaxService(marsContext.contextPath + '/services/data/run/fbs.i18n-DeleteI18N@.json');
        api.post({
            success: function (o) {
                $scope.refreshI18NData();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param, undefined, getAjaxConfig());
    }

    $scope.deleteI18NLanguage = function (selectedNode) {
        bootbox.confirm({
            message: "Are you sure to delete the language " + selectedNode.lang + "?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    var param = {
                        id: selectedNode.id,
                        projectId: selectedNode.projectId
                    };
                    deleteI18NData(param);
                }
            }
        });
    };

    $scope.addI18NLanguage = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/i18n/i18n-data.html'),
            size: 'full',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.mode = 'Add';
                    $scope.lookupDataDistrict = scope.lookupDataDistrict;
                    $scope.title = "Add I18N Language";
                    $scope.i18nData = {
                        workspaceId: 0,
                        projectId: 0
                    };
                    if (scope.selectedNode) {
                        if (scope.selectedNodeType === Workspace) {
                            $scope.i18nData.district = Workspace;
                            $scope.i18nData.workspaceId = scope.selectedNode.id;
                            $scope.i18nData.workspaceName = scope.selectedNode.name;
                        } else if (scope.selectedNodeType === Project) {
                            $scope.i18nData.district = Project;
                            $scope.i18nData.projectId = scope.selectedNode.id;
                            $scope.i18nData.projectName = scope.selectedNode.name;
                            $scope.i18nData.workspaceId = scope.selectedNode.parent.id;
                            $scope.i18nData.workspaceName = scope.selectedNode.parent.name;
                        } else {
                            $scope.i18nData.district = Application;
                        }
                    } else {
                        $scope.i18nData.district = Application;
                    }

                    //check if it is called by iframe
                    $scope.isParamProjectId = function () {
                        return scope.projectId !== '';
                    };
                    $scope.selectWorkspace = function (selectedData, i18nData) {
                        selectWorkspace(selectedData, i18nData);
                    };
                    $scope.selectProject = function (selectedData, i18nData) {
                        selectProject(selectedData, i18nData);
                    };
                    $scope.searchProjectParams = function (string) {
                        return {
                            workspaceId: $scope.i18nData.workspaceId,
                            LIKE_name_LIKE: string
                        };
                    };
                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.i18nData);
                    };
                }
            ]
        }).result.then(function (i18nData) {
            i18nData.content = {};
            createI18NData(i18nData);
        }, function () {
        });
    };

    $scope.editI18NLanguage = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/i18n/i18n-data.html'),
            size: 'full',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.mode = 'Modify';
                    $scope.lookupDataDistrict = scope.lookupDataDistrict;
                    $scope.title = "Update I18N Language";
                    $scope.i18nData = {
                        id: scope.selectedNode.id,
                        workspaceId: scope.selectedNode.workspaceId,
                        projectId: scope.selectedNode.projectId,
                        lang: scope.selectedNode.lang,
                        district: scope.selectedNode.district
                    };

                    if (Project === $scope.i18nData.district) {
                        $scope.i18nData.projectName = scope.selectedNode.parent.name;
                        $scope.i18nData.workspaceId = scope.selectedNode.parent.parent.id;
                        $scope.i18nData.workspaceName = scope.selectedNode.parent.parent.name;
                    } else if (Workspace === $scope.i18nData.district) {
                        $scope.i18nData.workspaceId = scope.selectedNode.parent.id;
                        $scope.i18nData.workspaceName = scope.selectedNode.parent.name
                    }

                    //check if it is called by iframe
                    $scope.isParamProjectId = function () {
                        return scope.projectId !== '';
                    };
                    $scope.selectWorkspace = function (selectedData, i18nData) {
                        selectWorkspace(selectedData, i18nData);
                    };
                    $scope.selectProject = function (selectedData, i18nData) {
                        selectProject(selectedData, i18nData);
                    };
                    $scope.searchProjectParams = function (string) {
                        return {
                            workspaceId: $scope.i18nData.workspaceId,
                            LIKE_name_LIKE: string
                        };
                    };
                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.i18nData);
                    };
                }
            ]
        }).result.then(function (i18nData) {
            var data = {
                id: i18nData.id,
                workspaceId: i18nData.workspaceId,
                projectId: i18nData.projectId,
                lang: i18nData.lang,
                district : i18nData.district
            };
            updateI18NData(data);
        }, function () {
        });
    };

    $scope.loadI18NData = function (mode) {
        $scope.i18nDataMode = mode;
        if (mode === Merged) {
            var selectedNode = $scope.selectedNode.parent;
            var selectedNodeType = getObjectType(selectedNode);
            var param = {
                baseLang: $scope.i18nBaseLang,
                lang: $scope.selectedNode.lang
            };
            get18NData(selectedNode, selectedNodeType, param, function (o) {
                $scope.jsonEditor.instance.set(o.data[param.lang].content);
                try {
                    $scope.jsonEditor.instance.expandAll();
                } catch (e) {
                }
            });
        } else {
            $scope.jsonEditor.instance.set($scope.selectedNode.content);
            try {
                $scope.jsonEditor.instance.expandAll();
            } catch (e) {
            }
        }
    };

    $scope.changeJsonEditorMode = function (mode) {
        $scope.jsonEditor.options.mode = mode;
    };

    $scope.saveI18NData = function (content) {
        jsonHasNull = false;
        checkJsonContent(content);
        if(!jsonHasNull) {
            var data = {
                id: $scope.selectedNode.id,
                workspaceId: $scope.selectedNode.workspaceId,
                projectId: $scope.selectedNode.projectId,
                lang: $scope.selectedNode.lang,
                content: content
            };
            updateI18NData(data);
        } else {
            bootbox.alert("There is one or more null values in the message. Please check.");
        }
    };

    function checkJsonContent(obj) {
        for(var key in obj){
            if (obj.hasOwnProperty(key)) {
                //In the case of Undefined, NaN, and so on, json editor blocks it.
                if (obj[key] === null) {
                    jsonHasNull = true;
                    break;
                } else if (Object.prototype.toString.call(obj[key]) === '[object Object]') {
                    checkJsonContent(obj[key]);
                } else if (Array.isArray(obj[key])) {
                    obj[key].forEach(element => checkJsonContent(element));
                }
            }
        }
    }

    function showMessageAlert(message) {
        showMessage(message, "Alert");
    }

    function showMessageInfo(message) {
        showMessage(message, "Info");
    }

    function showMessageWarning(message) {
        showMessage(message, "Warning");
    }

    function showMessage(message, type) {
        if (parent && parent.$B && parent.$B.ExceptionHandler) {
            if (type === "Alert") {
                parent.$B.ExceptionHandler.pin('Alert', message);
            } else if (type === "Info") {
                parent.$B.ExceptionHandler.toast('Info', message);
            } else if (type === "Warning") {
                parent.$B.ExceptionHandler.toast('Warning', message);
            }
        }
    }

    function getHistory(callback) {
        $scope.i18nHistories = [];
        var param = {
            id: $scope.selectedNode.id,
            lang: $scope.selectedNode.lang
        };
        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.i18n-GetI18NHistorySimpleList@.json');
        api.post({
            success: function (o) {
                $scope.i18nHistories = o.data.data;
                if (callback) {
                    callback();
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.showI18NHistory = function () {
        var callback = $scope.loadI18NHistory;
        getHistory(callback);
    };

    $scope.loadI18NHistory = function () {
        var scope = $scope;
        var value, orig, dv, highlight = true, connect = "align", collapse = true;
        var pageSize = 10;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/i18n/i18n-history.html'),
            size: 'xl',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = "I18N Versioning";
                    $scope.selectedNode = scope.selectedNode;
                    $scope.i18nHistoriesLeft = scope.i18nHistories;
                    $scope.i18nHistoriesRight = scope.i18nHistories;
                    $scope.selectedHistoryLeft = scope.i18nHistories[0];
                    $scope.selectedHistoryRight = scope.i18nHistories[0];
                    $scope.selectedNumLeft = 0;
                    $scope.selectedNumRight = -1;
                    $scope.isChangedLeft = false;
                    $scope.isChangedRight = false;

                    function initUI() {
                        if (value == null) return;
                        var target = document.getElementById("view");
                        target.innerHTML = "";
                        dv = CodeMirror.MergeView(target, {
                            value: value,
                            origLeft: null,
                            orig: orig,
                            lineNumbers: true,
                            mode: "application/ld+json",
                            highlightDifferences: highlight,
                            allowEditingOriginals: true,
                            connect: connect,
                            collapseIdentical: collapse
                        });

                        dv.editor().on("change", function (cm, change) {
                            $scope.setChanged("L", dv.editor().getValue() !== value ? true : false)
                        });

                        dv.rightOriginal().on("change", function (cm, change) {
                            $scope.setChanged("R", dv.rightOriginal().getValue() !== orig ? true : false)
                        });

                        dv.editor().on("beforeChange", function (cm, change) {
                            $scope.beforeChangeLeft = value;
                        });

                        dv.rightOriginal().on("beforeChange", function (cm, change) {
                            $scope.beforeChangeRight = orig;
                        });

                        resize(dv);

                        $(window).on('resize', function () {
                            resize(dv);
                        });
                    }

                    $scope.setChanged = function (view, flag) {
                        if ("L" === view && $scope.isChangedLeft !== flag) {
                            $scope.isChangedLeft = flag;
                            $scope.$apply();
                        } else if ("R" === view && $scope.isChangedRight !== flag) {
                            $scope.isChangedRight = flag;
                            $scope.$apply();
                        }
                    };

                    function changeLeftViewData() {
                        $scope.isChangedLeft = false;
                        if (null == dv || undefined == dv) {
                            initUI();
                        } else {
                            dv.editor().setValue(value);
                        }
                    }

                    function changeRightViewData() {
                        $scope.isChangedRight = false;
                        if (null == dv || undefined == dv) {
                            initUI();
                        } else {
                            dv.rightOriginal().setValue(orig);
                        }
                    }

                    function resize(mergeView) {
                        var historyContainer = document.getElementById("historyContainer");
                        var historyContentHeader = document.getElementById("historyContentHeader");
                        if(historyContainer && historyContentHeader) {
                            var height = historyContainer.offsetHeight - historyContentHeader.offsetHeight;
                            mergeView.editor().setSize(null, height);
                            mergeView.rightOriginal().setSize(null, height);
                            mergeView.wrap.style.height = height + "px";
                        }
                    }

                    function getHistoryContent(selectedData, selectedView, callback) {
                        var param = {
                            id: selectedData.id,
                            lang: selectedData.lang,
                            transactionId: selectedData.transactionId
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/get/fbs.i18n-GetI18NHistory.json');
                        api.post({
                            success: function (o) {
                                if (o.data && o.data[0].content) {
                                    var result = JSON.parse(o.data[0].content);
                                    var resultString = JSON.stringify(result, null, 2);

                                    if ("L" === selectedView) {
                                        value = resultString;
                                    } else if ("R" === selectedView) {
                                        orig = resultString;
                                    } else if ("ALL" === selectedView) {
                                        value = resultString;
                                        orig = resultString;
                                    }
                                    if (callback) {
                                        if (callback === initUI) {
                                            $timeout(function () {
                                                callback();
                                            }, 100);
                                        } else {
                                            callback();
                                        }
                                    }
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    $scope.selectHistoryLeft = function (idx, selectedData) {
                        if ($scope.isChangedLeft || $scope.isChangedRight) {
                            showMessageWarning("Save/Revert Required.");
                        } else {
                            $scope.selectedNumLeft = idx;
                            $scope.selectedHistoryLeft = idx === -1 ? $scope.i18nHistoriesLeft[0] : selectedData;
                            getHistoryContent($scope.selectedHistoryLeft, "L", changeLeftViewData);
                        }
                    };
                    $scope.selectHistoryRight = function (idx, selectedData) {
                        if ($scope.isChangedLeft || $scope.isChangedRight) {
                            showMessageWarning("Save/Revert Required.");
                        } else {
                            $scope.selectedNumRight = idx;
                            $scope.selectedHistoryRight = idx === -1 ? $scope.i18nHistoriesRight[0] : selectedData;
                            getHistoryContent($scope.selectedHistoryRight, "R", changeRightViewData);
                        }
                    };

                    $scope.loadMore = function (view) {
                        pageSize = 10000;
                        $scope.getHistoryList(view);
                    };

                    $scope.getHistoryList = function (view, callback) {
                        var param = {
                            id: $scope.selectedNode.id,
                            lang: $scope.selectedNode.lang,
                            pageSize: pageSize
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/fbs.i18n-GetI18NHistorySimpleList@.json');
                        api.post({
                            success: function (o) {
                                $scope.i18nHistoriesLeft = o.data.data;
                                $scope.i18nHistoriesRight = o.data.data;

                                if (callback) {
                                    if (callback === initHistoryView) {
                                        $(".list-group").scrollTop(0);
                                        $scope.selectedHistoryLeft = $scope.i18nHistoriesLeft[0];
                                        $scope.selectedHistoryRight = $scope.i18nHistoriesRight[0];
                                        $scope.selectedNumLeft = 0;
                                        $scope.selectedNumRight = -1;
                                        $scope.isChangedLeft = false;
                                        $scope.isChangedRight = false;
                                    }
                                    callback();
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    };

                    function checkFormatJSON(obj) {
                        try {
                            JSON.parse(obj.getValue());
                            return true;
                        } catch (e) {
                            return false;
                        }
                    }

                    function getViewObj(view) {
                        var obj;
                        if ("L" === view) {
                            obj = dv.editor();
                        } else if ("R" === view) {
                            obj = dv.rightOriginal();
                        }
                        return obj;
                    }

                    $scope.revert = function (view) {
                        $scope.save(view);
                    };

                    $scope.save = function (view) {
                        var obj = getViewObj(view);
                        if (checkFormatJSON(obj)) {
                            var data = {
                                id: $scope.selectedNode.id,
                                workspaceId: $scope.selectedNode.workspaceId,
                                projectId: $scope.selectedNode.projectId,
                                lang: $scope.selectedNode.lang,
                                content: obj.getValue()
                            };
                            updateI18NData(data, resetHistory);
                        } else {
                            showMessageAlert("JSON Format Error");
                        }
                    };

                    $scope.reset = function (view) {
                        if (view === 'L') {
                            dv.editor().setValue($scope.beforeChangeLeft);
                        } else {
                            dv.rightOriginal().setValue($scope.beforeChangeRight);
                        }
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };

                    $scope.setHeight = function () {
                        setHeight();
                    };

                    function resetHistory() {
                        var callback = initHistoryView;
                        $scope.getHistoryList("ALL", callback);
                    }

                    function initHistoryView() {
                        getHistoryContent($scope.selectedHistoryLeft, "ALL", initUI);
                    }

                    (function () {
                        initHistoryView();
                    })();

                }
            ]
        }).result.then(function (i18nData) {

        }, function () {

        });
    };

    $scope.setHeight = function () {
        setHeight();
    };
}]);


function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;

    var treeContainer = document.getElementById("treePanelContainer");
    var l = mars$util$.getElementPosition(leftContainer);
    var t = mars$util$.getElementPosition(treeContainer);
    treeContainer.style.height = (height - (t.y - l.y + 5)) + "px";

    var rightPanelHeader = document.getElementById("rightPanelHeader");
    var jsonEditor = document.getElementById("jsonEditor");
    jsonEditor.style.height = (height - rightPanelHeader.offsetTop - rightPanelHeader.offsetHeight) + "px";

    var historyModal = document.getElementById("historyModal");
    if (historyModal) historyModal.style.height = (height * 0.9) + "px";
}