<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AppDev I18N Message Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/angular-tree-widget/angular-tree-widget-ext-custom.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/jsoneditor/dist/jsoneditor.min.css"/>
    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/lib/codemirror.css">
    <link rel=stylesheet href="../../../../includes/node_modules/codemirror/addon/merge/merge.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../../includes/node_modules-ext/jsoneditor/dist/jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/ng-jsoneditor/ng-jsoneditor.min.js"></script>
    <script src="../../../../includes/node_modules/codemirror/lib/codemirror.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/css/css.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/javascript/javascript.js"></script>
    <script src="../../../../includes/node_modules/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="../../../../includes/node_modules/diff-match-patch/javascript/diff_match_patch.js"></script>
    <script src="../../../../includes/node_modules/codemirror/addon/merge/merge.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("i18n-message-manager.js");
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
    <%if (showTitle) {%>
    <div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
        <h1>AppDev I18N Message Management</h1>
    </div>
    <%}%>
    <div class="container-fluid" id="baseContainer">
        <div class="row flex-nowrap" id="panelContainer">
            <div class="col-md-3" style="padding:0 1px 0 0">
                <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                    <div style="padding-bottom: 0;" id="leftPanelHeader">
                        <div class="bio-left-title">
                            <div class="title"><h5>I18N</h5></div>
                            <div class="i-btn">
                                <button class="bf-button p-component bf-button-icon-only" title="Add" ng-click="addI18NLanguage();$event.stopPropagation();" ng-disabled="selectedNodeType === I18N" disabled="disabled"><i class="fa fa-plus"></i></button>
                                <button class="bf-button p-component bf-button-icon-only" title="Edit" ng-click="editI18NLanguage();$event.stopPropagation();" ng-disabled="selectedNodeType !== I18N"><i class="fa fa-pencil"></i></button>
                                <button class="bf-button p-component bf-button-icon-only" title="Delete" ng-click="deleteI18NLanguage(selectedNode);$event.stopPropagation();" ng-disabled="selectedNodeType !== I18N"><i class="fa fa-times"></i></button>
                                <button class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshI18NData();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="card-block" id="treeContainer">
                        <div class="form-group form-group-sm col-md-12">
                            <input type="text" ng-model="treeOptions.filter.name" class="searchinput" placeholder="Enter a name to filter" style="margin-bottom: 10px;">
                            <div style="top: 5px; right: 25px;" class="position-absolute"><span class="searchbtn mt-1"><i class="fa fa-search"></i></span></div>
                        </div>
                        <div id="treePanelContainer" class="panelContainer">
                            <tree nodes='treeRoot' options="treeOptions"></tree>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9" style="padding: 0 0 0 1px;">
                <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                    <div id="rightPanelHeader">
                        <div class="bio-right-title bio-pd-15">
                            <div class="title">
                                <h3>
                                    I18N Messages
                                    <span class="id-wrap" ng-if="selectedNode.id">ID&nbsp;:&nbsp;{{selectedNode.id}}</span>
                                </h3>
                            </div>
                            <div class="i-btn">
                                <div class="space"></div>
                                <button class="p-button p-component p-button-text-only" ng-click="loadI18NData(Merged);$event.stopPropagation();" ng-if="jsonEditor.data && i18nDataMode !== Merged">Merge Mode</button>
                                <button class="p-button p-component p-button-text-only" ng-click="loadI18NData(Original);$event.stopPropagation();" ng-if="jsonEditor.data && i18nDataMode !== Original">Original Mode</button>
                                <button class="p-button p-component p-button-text-only" ng-click="saveI18NData(jsonEditor.data);$event.stopPropagation();" ng-if="jsonEditor.data">Save</button>
                                <button class="p-button p-component p-button-info p-button-text-only" ng-click="showI18NHistory();$event.stopPropagation();" ng-if="jsonEditor.data">History</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-block panelContainer overflow-hidden">
                        <div ng-show="!selectedNode.content" class="text-center" style="padding-top: 20px;">
                            Please select an language.
                        </div>
                        <div ng-show="selectedNode.content" style="background-color: white;height: 100%;">
                            <div ng-jsoneditor="setJsonEditorInstance" ng-model="jsonEditor.data" options="jsonEditor.options" class="max-width" id="jsonEditor"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>