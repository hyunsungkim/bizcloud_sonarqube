<%@ page import="com.bizflow.io.core.security.SecurityCipher" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.bf.security.BizFlowSecurity" %>
<%@ page import="com.bizflow.io.services.core.service.AdhocService" %>
<%@ page import="java.util.TimeZone" %>
<%
    final String key128 = "QwERtYuIjHgFCdV8"; // 128 bit key
    final String initVector = "BvCdRtY9PAsAcRtY"; // 16 bytes IV

    AdhocService.getInstance().initializeRequestIdentifier(request, response);
    boolean validRequest = AcmManager.getInstance().isValidAdminRequest(request);

    if (!validRequest) {
        TimeZone timeZone = ServletUtil.getClientTimeZone(request);
        if (null != timeZone) {
            String sessionInfoXml = request.getParameter("$");
            if (null != sessionInfoXml) {
                sessionInfoXml = SecurityCipher.getInstance().decipherBase64(key128, initVector, sessionInfoXml);
                BizFlowSecurity.getInstance().initializeBizFlowSession(request, sessionInfoXml);
                validRequest = true;
            }
        } else {
%>
<html>
<head>
    <script>
        var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
        var url = '' + this.location.href;
        this.location.href = url + (-1 == url.indexOf('?') ? '?' : '&') + 'tz=' + encodeURIComponent(timeZone);
    </script>
</head>
</html>
<%
        }
    }
%>

<%
    if (validRequest) {
%>
<html>
<script>
    this.location = "<%=request.getContextPath()%>/apps/fbs/tools/form/form-manager.jsp?theme=light&caller=bizflow";
</script>
</html>
<%
    }
%>