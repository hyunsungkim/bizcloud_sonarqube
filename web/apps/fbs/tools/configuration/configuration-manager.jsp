<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../../auth/auth.jsp" %>
<%@include file="../include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AppDev Configuration Management</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../include/tool-common-css.jsp" %>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/bootbox/bootbox.min.js"></script>

    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../include/tool-common.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("../../../../admin/tools/configuration/configuration-manager.js");
    </script>
    <script>
        var appName = "fbs";
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center" style="padding: 10px 0 10px 0">
    <h1>AppDev Configuration Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                <div class="card-header" id="rightPanelHeader" style="min-height: 46px;">
                    <div class="row justify-content-md-center" style="padding:10px 0 10px 0;" ng-if="confList">
                        <form class="form-inline">
                            <label for="searchType" style="padding: 0 5px 0 2px;">Type</label>
                            <input type="text" class="form-control form-control-sm" id="searchType" ng-enter="searchConf()" ng-model="search.LIKE_type_LIKE">
                            <div style="padding-left: 10px;"></div>
                            <label for="searchName" style="padding: 0 5px 0 2px;">Name</label>
                            <input type="text" class="form-control form-control-sm" id="searchName" ng-enter="searchConf()" ng-model="search.LIKE_name_LIKE">
                            <div style="padding-left: 10px;"></div>
                            <button type="button" class="bf-button p-component bf-button-icon-only" title="Search" ng-click="searchConf()"><i class="fa fa-search"></i></button>
                            <div style="padding-left: 5px;"></div>
                            <button type="button" class="bf-button p-component bf-button-icon-only" title="Refresh" ng-click="refreshList();$event.stopPropagation();"><i class="fa fa-repeat"></i></button>
                        </form>
                    </div>
                    <button class="p-button p-component p-button-success p-button-text-only float-right" ng-click="addConf();$event.stopPropagation();">Add</button>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div class="col-md-12" id="DataValue" ng-if="confList">
                        <div ng-if="confList.data.length == 0">
                            <div class="text-center">There are no configurations</div>
                        </div>
                        <div class="bf-table-2">
                            <table class="table table-striped table-condensed table-font-small no-margin table-sm text-<%=textColor%>" ng-if="confList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold" class="conf">
                                    <th>#</th>
                                    <th ng-click="sortConfList('type')"><a>Type</a></th>
                                    <th ng-click="sortConfList('name')"><a>Name</a></th>
                                    <th ng-click="sortConfList('value')"><a>Value</a></th>
                                    <th ng-click="sortConfList('description')"><a>Description</a></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in confList.data" id="{{item.id}}" class="conf">
                                    <td>{{item.ROW_NUMBER}}</td>
                                    <td>{{item.type}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.value}}</td>
                                    <td>{{item.description}}</td>
                                    <td class="text-nowrap right-btn" style="vertical-align: middle;">
                                        <button type="button" class="p-button p-component p-button-text-only" ng-click="updateConf(item);$event.stopPropagation();">Edit</button>
                                        <button type="button" class="p-button p-component p-button-info p-button-text-only" ng-click="showConfHistory(item);$event.stopPropagation();">History</button>
                                        <button type="button" class="p-button p-component p-button-danger p-button-text-only" ng-click="deleteConf(item);$event.stopPropagation();">Delete</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="confList.totalCount > confListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="confList.totalCount" items-per-page="confListPageSize" ng-model="$parent.$parent.confListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changeConfListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>