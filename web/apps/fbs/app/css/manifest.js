var _themeType = ["dark", "light"];
var _themeManifest = {
  "bootstrap": {
    "theme": true,
    "folder": "bootstrap$",
    "version": "0.0.1",
    "files": [
      "bootstrap.css"
    ],
    "dirs": [
      "img"
    ]
  },
  "fontawesome-free": {
    "theme": false,
    "folder": "fontawesome-free",
    "version": "5.15.1",
    "files": [
      "css/all.css"
    ],
    "dirs": [
      "sprites",
      "sprites",
      "svgs",
      "webfonts"
    ]
  },
  "fullcalendar": {
    "theme": true,
    "folder": "fullcalendar$",
    "version": "4.0.0-alpha.4",
    "files": [
      "fullcalendar.css"
    ]
  },
  "primeflex": {
    "theme": false,
    "folder": "primeflex",
    "version": "0.0.2",
    "files": [
      "primeflex.css"
    ]
  },
  "primeicons": {
    "theme": false,
    "folder": "primeicons",
    "version": "0.0.2",
    "files": [
      "primeicons.css"
    ],
    "dirs": [
      "fonts"
    ]
  },
  "primereact": {
    "theme": true,
    "folder": "primereact",
    "version": "0.0.3",
    "files": [
      "primereact.css",
      "themes/theme.css"
    ],
    "dirs": [
      "images",
      "themes/bizflow$"
    ]
  },
  "quill": {
    "theme": true,
    "folder": "quill$",
    "version": "1.3.7",
    "files": [
      "quill.core.css",
      "quill.snow.css"
    ]
  },
  "font-bizflow": {
    "theme": false,
    "folder": "font-bizflow",
    "version": "2.3.6",
    "files": [
      "font-bizflow.css"
    ],
    "dirs": [
      "font",
      "svg"
    ]
  }
};