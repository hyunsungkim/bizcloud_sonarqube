// helper
var $B = (function() {
    var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
    var _tooltip = [];
    var _customValidation = [];
    var _config = {
        popupValidation: true,
        landingPage: null,
        ajaxHook: null,
        accessibility: {
            modal: true,
            popupValidation: true,
            restoreFocus: true,
        },
        step: "normal"
    };
    return {
        path: window.location.pathname.replace(/(^\/[^/]+).*/, "$1"),
        timeZone: timeZone,
        timeZoneHeader: {
            "X-Client-TimeZone": timeZone
        },
        basicWIHActionClient: null,
        getQueryString: function(key) {
            var match = RegExp("[?&]" + key + "=([^&]*)").exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ""));
        },
        storage: {
            // user storage
        },
        extend: function(funcName, func) {
            $B.custom[funcName] = func;
        },
        custom: {
            // user define function
        },
        customHTML: function(bfCustomId, func, maxTry) {
            // try maxTry(default 10) per 100ms
            // func(doms: Array<HTMLElement>, success: boolean)
            window["BizFormHelper"].DomHelper.customHTML(bfCustomId, func, maxTry);
        },
        loader: {
            javascript: function(filename, version, successCallback, errorCallback, abortCallback, w) {
                w = w ? w : window;
                var script = w.document.createElement("script");
                script.setAttribute("src", filename + (version ? "?v=" + version : ""));
                script.setAttribute("type", "text/javascript");
                $B.loader.loader(script, successCallback, errorCallback, abortCallback, w);
            },
            css: function(filename, version, successCallback, errorCallback, abortCallback, w) {
                w = w ? w : window;
                var css = w.document.createElement("link");
                css.setAttribute("href", filename + (version ? "?v=" + version : ""));
                css.setAttribute("rel", "stylesheet");
                $B.loader.loader(css, successCallback, errorCallback, abortCallback, w);
            },
            loader: function(loader, successCallback, errorCallback, abortCallback, w) {
                w = w ? w : window;
                if (successCallback) {
                    loader.addEventListener("load", successCallback);
                }
                if (errorCallback) {
                    loader.addEventListener("error", errorCallback);
                }
                if (abortCallback) {
                    loader.addEventListener("abort", abortCallback);
                }
                w.document.getElementsByTagName("head")[0].appendChild(loader);
            }
        },
        labelAlias: {
            component: {},
            label: {}
        },
        getApplicationInfo: function(dom) {
            var info = {
                applicationId: null,
                formId: null,
                globalIndex: null,
                isReadonlyMode: null
            };
            if (dom) {
                var infoFromDom = window["BizFormHelper"].DomHelper.getInformationFromDom(dom);
                if (infoFromDom && infoFromDom.applicationId) {
                    info.applicationId = infoFromDom.applicationId;
                    info.formId = infoFromDom.formId;
                    info.globalIndex = infoFromDom.globalIndex;
                    info.isReadonlyMode = infoFromDom.isReadonlyMode;
                }
            }
            return info;
        },
        tooltip: function(targetDom, text, eventName, position) {
            // eventName => "hover" or "focus"
            // position => "top", "right", "bottom" or "left"
            // text => text or HTML (only ' in tag attribute)
            var tooltipAttribute = {
                event: eventName ? eventName : "hover",
                position: position ? position : "bottom",
                text: text
            };
            window["BizFormHelper"].ComponentHelper.tooltip(targetDom, tooltipAttribute, _tooltip);
        },
        getRootApplicationDom: function(applicationId, globalIndex) {
            if (applicationId && globalIndex) {
                return window["BizFormHelper"].DomHelper.getRootApplicationDom(applicationId, globalIndex);
            }
            return null;
        },
        config: {
            getRedirectHome: function() {
                return encodeURIComponent(location.pathname + location.search);
            },
            getLandingPage: function() {
                return _config.landingPage;
            },
            setLandingPage: function(valueOrFunction) {
                _config.landingPage = valueOrFunction;
            },
            // ${home} => replace automatically $B.config.getRedirectHome()
            goLandingPage: function() {
                if (_config.landingPage) {
                    $B.BlockHandler.block();
                    var func = function() {
                        if (this.contentDocument && this.contentDocument.body) {
                            var url;
                            if (typeof _config.landingPage === "function") {
                                url = _config.landingPage();
                            } else {
                                url = _config.landingPage;
                            }
                            if (url) {
                                location.replace(url.replace(/\$\{home}/, $B.config.getRedirectHome()));
                            }
                            func.__proto__ = null;
                            func = null;
                        } else {
                            setTimeout(func, 1000);
                        }
                    }
                    var iframe = document.createElement("iframe");
                    iframe.src = window["BizFormCore"].DataStorage.logoutPath;
                    iframe.style.display = "none";
                    iframe.onload = func;
                    document.body.appendChild(iframe);
                    return true;
                }
                return false;
            },
            getPopupValidation: function() {
                return _config.popupValidation;
            },
            setPopupValidation: function(on) {
                _config.popupValidation = on;
            },
            callAjaxSuccessHook: function() {
                if (typeof _config.ajaxHook === "function") {
                    _config.ajaxHook();
                }
            },
            setAjaxSuccessHook: function(hook) {
                _config.ajaxHook = hook;
            },
            lang: function() {
                return window["BizFormCore"].I18N.getLanguage();
            },
            accessibility: {
                setAutoModalRead: function(turnOn) {
                    _config.accessibility.modal = turnOn;
                },
                getAutoModalRead: function() {
                    return _config.accessibility.modal;
                },
                setAutoPopupValidationRead: function(turnOn) {
                    _config.accessibility.popupValidation = turnOn;
                },
                getAutoPopupValidationRead: function() {
                    return _config.accessibility.popupValidation;
                },
                setRestoreFocus: function(restoreFocus) {
                    _config.restoreFocus = restoreFocus;
                },
                getRestoreFocus: function() {
                    return _config.restoreFocus;
                }
            },
            step: {
                strict: function() {
                    _config.step = "strict";
                },
                normal: function() {
                    _config.step = "normal";
                },
                flexible: function() {
                    _config.step = "flexible";
                },
                getMode: function() {
                    return _config.step;
                }
            }
        },
        session: {
            setTTL: function(minutes) {
                window["BizFormCore"].FormConfigService.setTTL(minutes);
            },
            getTTL: function() {
                return window["BizFormCore"].FormConfigService.getTTL();
            },
            getFirstAccessTime: function() {
                return window["BizFormCore"].FormConfigService.getFirstAccessTime();
            },
            getLastAccessTime: function() {
                return window["BizFormCore"].FormConfigService.getLastAccessTime();
            },
            updateLastAccessTime: function() {
                return window["BizFormCore"].FormConfigService.updateLastAccessTime();
            },
            getDiffAccessTime: function() {
                return window["BizFormCore"].FormConfigService.getDiffAccessTime();
            }
        },
        state: {
            isDesignMode: function() {
                return window["___applicationId"] ? false : true;
            },
            getFormStateData: function(applicationId, formId) {
                return window["BizFormCore"].FormStateService.getFormStateData(applicationId, formId);
            },
            getFormStateDataByKeys: function(applicationId, formId, stateKeys, readonlyStateKeys) {
                return window["BizFormCore"].FormStateService.getFormStateDataByKeys(applicationId, formId, stateKeys, readonlyStateKeys);
            },
            getQueryString: function(applicationId, formId) {
                return window["BizFormCore"].FormStateService.getQueryString(applicationId, formId);
            },
            mergeState: function(applicationId, formId, state) {
                window["BizFormCore"].FormStateService.mergeState(applicationId, formId, null, state);
            },
            mergeStateNoNotify: function(applicationId, formId, state) {
                window["BizFormCore"].FormStateService.mergeStateNoNotify(applicationId, formId, null, state);
            },
            removeStateDataKeys: function(applicationId, formId, stateKeys) {
                window["BizFormCore"].FormStateService.removeStateDataKeys(applicationId, formId, null, stateKeys);
            },
            removeStateDataKeysNoNotify: function(applicationId, formId, stateKeys) {
                window["BizFormCore"].FormStateService.removeStateDataKeysNoNotify(applicationId, formId, null, stateKeys);
            }
        },
        Inner: {},
        Outer: {},
        DataStorage: function() {
            return window["BizFormCore"].DataStorage;
        },
        I18N: {
            getData: function() {
                return $I;
            },
            translate: function(value, option, defaultValue) {
                return window["BizFormCore"].I18N.options(value, option, defaultValue);
            }
        },
        Ajax: {
            get: function(url, headers) {
                return window["BizFormCore"].AjaxService.get(url, headers);
            },
            post: function(url, body, headers) {
                return window["BizFormCore"].AjaxService.post(url, body, headers);
            },
            // rxjs
            // refer to rx/js Ajax manual
            ajax: function() {
                return window["BizFormCore"].AjaxService.getAjax();
            }
        },
        CryptoJS: function() {
            // CryptoJS
            // refer to CryptoJS manual
            return window["BizFormHelper"].CipherHelper.getCryptoJS();
        },
        Lodash: function() {
            // _
            // refer to lodash manual
            return window["BizFormHelper"].ObjectHelper.getLodash();
        },
        Moment: function() {
            // refer to Moment Timezone Documentation
            return {
                // moment()
                func: moment(),
                // moment
                obj: moment
            };
        },
        ModalHandler: {
            setTitle: function(title) {
                window["BizFormCore"].ModalHandler.setTitle(title);
            },
            setOnOffEsc: function(on) {
                // true esc on / false esc off / default on
                window["BizFormCore"].ModalHandler.setOnOffEsc(on);
            },
            close: function(dom) {
                var modalDom = window["BizFormCore"].DomHelper.getModalContentDomFromDom(dom);
                if (modalDom) {
                    var closeDom = modalDom.querySelector("[bf-modal-button-close]");
                    if (closeDom) {
                        closeDom.click();
                    }
                }
            },
            apply: function(dom) {
                var modalDom = window["BizFormCore"].DomHelper.getModalContentDomFromDom(dom);
                if (modalDom) {
                    var applyDom = modalDom.querySelector("[bf-modal-button-apply]");
                    if (applyDom) {
                        applyDom.click();
                    }
                }
            }
        },
        BlockHandler: {
            block: function() {
                window["BizFormCore"].BlockHandler.block();
            },
            unblock: function() {
                window["BizFormCore"].BlockHandler.unblock();
            },
            forcedUnblock: function(value) {
                window["BizFormCore"].BlockHandler.forcedUnblock(value);
            },
            load: function() {
                window["BizFormCore"].BlockHandler.load();
            },
            unload: function() {
                window["BizFormCore"].BlockHandler.unload();
            }
        },
        ExceptionHandler: {
            // type: Alert / Warning / Error / Info / Exception
            // message: "string" or {summary: "string", detail: "string"}
            toast: function(type, message) {
                window["BizFormCore"].ExceptionHandler.toast(type, message);
            },
            pin: function(type, message) {
                window["BizFormCore"].ExceptionHandler.pin(type, message);
            },
            // type: Alert / Warning / Error / Info / Exception
            // message: "string"
            alert: function(type, message, callback) {
                window["BizFormCore"].ExceptionHandler.alert(type, message, callback);
            },
            confirm: function(type, message, callback) {
                window["BizFormCore"].ExceptionHandler.confirm(type, message, callback);
            },
            interrupt: function(type, message) {
                if (!$B.state.isDesignMode()) {
                    window["BizFormCore"].ExceptionHandler.interrupt(type, message);
                } else {
                    $B.ExceptionHandler.toast(type, message);
                }
            }
        },
        Date: function() {
            // formatter(date: Date, format: string): string
            // guessTimeZone(ignoreCache: boolean = true): string
            // getOffset(timezone?: string): number
            // convertUTCServerToFront(timeISOString: string, timezone?: string): string
            // convertUTCFrontToServer(timeISOString: string, timezone?: string): string;
            return window["BizFormHelper"].DateHelper;
        },
        LookUp: function(applicationId, lookUpId, callback, isLazyLoading, value, requestBody) {
            var rCallback;
            if (callback) {
                rCallback = function() {
                    rCallback.__proto__= null;
                    callback(arguments[1]);
                }
            }
            window["BizFormCore"].LookupService.getEvaluatedLookupDataMap(applicationId, lookUpId, value, null, isLazyLoading, rCallback, requestBody)
        },
        LookUpHelper: {
            getLookupDataUsingI18N: function(lookupData) {
                // lookupData
                // [{
                //     label: default label value,
                //     i18n: i18n key,
                //     value: value
                // }]
                // =>
                // [{
                //     label: translated label using i18n,
                //     i18n: i18n key,
                //     value: value
                //     ___label: default label value
                // }]
                return window["BizFormCore"].LookupService.getLookupDataUsingI18N(lookupData);
            },
            getLookupValueUsingI18N: function(value) {
                // value
                // {
                //     label: translated label using i18n,
                //     i18n: i18n key,
                //     value: value
                //     ___label: default label value
                // ]
                // =>
                // {
                //     label: default label value,
                //     i18n: i18n key,
                //     value: value
                // ]
                return window["BizFormCore"].LookupService.getLookupValueUsingI18N(value);
            }
        },
        String: {
            randomString: function(length) {
                return window["BizFormHelper"].StringHelper.randomString(length);
            }
        },
        Signature: {
            getInfo: function(value) {
                try {
                    if (value) {
                        var v = value.split("@")[1];
                        if (v) {
                            return JSON.parse(atob(v));
                        }
                    }
                } catch (e) {
                }
            },
            getImage: function(value) {
                try {
                    if (value && value.split) {
                        var v = value.split("@")[0];
                        if (v) {
                            return v;
                        }
                    }
                } catch (e) {
                }
            }
        },
        Validation: {
            complete: {
                turnOn: function() {
                    window["BizFormCore"].WihActionService.on();
                },
                turnOff: function() {
                    window["BizFormCore"].WihActionService.off();
                }
            },
            setMessage: function(value) {
                window["BizFormCore"].FormValidationService.setMessage(value);
            },
            doValidation: function(formId, callback, rootDom, noPopup, forced) {
                // rootDom is optional.
                // forced is true => run custom Validation / grid custom validation if not complete
                // if there is callback, then error print and popup validation doesn't work
                // return false: no error / true: has error
                return window["BizFormCore"].FormValidationService.markValidation(formId, rootDom, callback, noPopup, null, forced);
                // there are validation faults.
                // callback(false);
                // else
                // callback(true);
            },
            checkStepValidation: function(applicationId, formId) {
                // return false: complete step / true: no complete step
                return window["BizFormCore"].FormValidationService.checkStepValidation(applicationId, formId);
            },
            resetRequired: function(rootDom) {
                rootDom = rootDom ? rootDom : document.body;
                window["BizFormCore"].FormValidationService.resetRequired(rootDom);
            },
            resetValidation: function(rootDom) {
                rootDom = rootDom ? rootDom : document.body;
                window["BizFormCore"].FormValidationService.resetValidation(rootDom);
            }
        },
        CustomValidation: {
            extend: function(funcName, func) {
                // custom: function funcName(contents, applicationId, formId, state, rState, i18nOption, bypassOption) { // contents }
                // grid custom: function funcName(contents, applicationId, formId, state, rState, i18nOption, bypassOption, formDataList, page, total, firstFormDataList, firstPage, firstTotal, linkId, listId, accessAlias) { // contents }
                // => ex:) bypassOption.pk = xxx => CallUpdate
                $B.CustomValidation.custom[funcName] = func;
            },
            custom: {
                // user define function for custom validation
            },
            get: function() {
                return _customValidation;
            },
            register: function(fieldName, type, validationFunction, callbackFunction, vArgs, cArgs) {
                fieldName = fieldName ? fieldName.trim() : null;
                validationFunction = typeof validationFunction === "function" ?  validationFunction : null;
                callbackFunction = typeof callbackFunction === "function" ?  callbackFunction : null;
                var errMessage = "";
                if (!fieldName) {
                    errMessage = "FieldName";
                }
                if (errMessage) {
                    errMessage += " should be required.";
                }
                if (!validationFunction || !callbackFunction) {
                    var fErr = "";
                    if (!validationFunction || !callbackFunction) {
                        fErr = "Validation and Callback"
                    } else if (!validationFunction) {
                        fErr = "Validation"
                    } else {
                        fErr = "Callback"
                    }
                    errMessage = (errMessage ? " And " : "") + fErr +" function should be function.";
                }
                if (errMessage) {
                    $B.ExceptionHandler.alert("Alert", errMessage);
                } else {
                    _customValidation.push({
                        fieldName: fieldName,
                        type: type ? type : "", // type: Focus, Move, ...
                        validationFunction: validationFunction, // return invalid message
                        callbackFunction: callbackFunction, // if to have invalid message
                        vArgs: vArgs, // for validationFunction
                        cArgs: cArgs // for callbackFunction
                    });
                }
            }
        },
        Layout: {
            dom: {
                /**
                 [key]: {
                        dom: Custom Component Layout
                        label:
                        component:
                        validation:

                        // for validation
                        fieldName:
                        focusFunction:
                        args:
                    }
                 */
            },
            layout: {
                FlowX: "_XkH2suMpDx37zCTywsa9TPDhT_JOeKs",
                FlowXL: "_M8iNUZo0oA014fu8bQelSDo5DNN7XJJ",
                FlowY: "_TNtwnrru7yu0krTGLUEK1ImgUgqXUdl",
                OnlyComponentElement: "_B30ouiXDulMJ3FtOZfxcDkqTetzToOK",
                WithoutLabel: "_HTlFZLdJVmG5DneJWkpAQoAgZJchSw7",
            },
            init: function(layout, key, labelWidth, componentWidth, validationWidth) {
                var d = window["BizFormCore"].PluginService.getComponentLayoutTemplate(layout).init();
                $B.Layout.dom[key] = {
                    dom: d,
                    label: d.querySelector("[bf-label]"),
                    component: d.querySelector("[bf-component-element]"),
                    validation: d.querySelector("[bf-validation]"),
                    dirty: false,
                };
                $B.Layout.dom[key].label.style.width = labelWidth;
                $B.Layout.dom[key].component.style.width = componentWidth;
                $B.Layout.dom[key].validation.style.width = validationWidth;
                $B.Layout.dom[key].label.setAttribute("for", key);
                $B.Layout.dom[key].component.setAttribute("for", key);
                $B.Layout.dom[key].validation.setAttribute("for", key);
                return d;
            },
            attachFromCustomId: function(bfCustomId, key) {
                var func = function(dom) {
                    if (dom && dom[0]) {
                        $B.Layout.attachFromDom(dom[0], key);
                    }
                    func.__proto__ = null;
                    func = null;
                };
                $B.customHTML(bfCustomId, func);
            },
            attachFromDom: function(dom, key) {
                if (dom && $B.Layout.dom[key]) {
                    dom.append($B.Layout.dom[key].dom);
                }
            },
            setLabel: function(key, value) {
                if ($B.Layout.dom[key]) {
                    var d = $B.Layout.dom[key].label;
                    if (d) {
                        d.innerHTML = value;
                    }
                }
            },
            setFieldName: function(key, value) {
                if ($B.Layout.dom[key]) {
                    $B.Layout.dom[key].fieldName = value;
                }
            },
            setFocusFunction: function(key, value, args) {
                if ($B.Layout.dom[key]) {
                    $B.Layout.dom[key].focusFunction = value;
                    $B.Layout.dom[key].args = args;
                }
            },
            setComponent: function(key, component) {
                if ($B.Layout.dom[key]) {
                    var d = $B.Layout.dom[key].component;
                    if (d) {
                        d.innerHTML = component;
                    }
                }
            },
            setValidation: function(key, value, dirty) {
                if ($B.Layout.dom[key]) {
                    value = value ? value.trim() : "";
                    var d = $B.Layout.dom[key].validation;
                    if (d && value) {
                        d.setAttribute("bf-validation-error", "");
                        d.innerHTML = "<span>" + value + "</span>";
                    } else if (d) {
                        d.removeAttribute("bf-validation-error");
                        d.innerHTML = "";
                    }
                    d.style.display = dirty ? "inline" : "none";
                    $B.Layout.dom[key].dirty = dirty ? true : false;
                }
            }
        },
        Schema: {
            getProjectDataSchemaData: function(applicationId) {
                return window["BizFormCore"].FormDataSchemaService.getProjectDataSchemaData(applicationId);
            },
            getFormDataSchemaData: function(applicationId, formId) {
                return window["BizFormCore"].FormDataSchemaService.getFormDataSchemaData(applicationId, formId);
            },
            getProjectDataSchemaFieldData: function(applicationId, entityId) {
                return window["BizFormCore"].FormDataSchemaService.getProjectDataSchemaFieldData(applicationId, entityId);
            },
            getFormDataSchemaFieldData: function(applicationId, formId, entityId) {
                return window["BizFormCore"].FormDataSchemaService.getFormDataSchemaFieldData(applicationId, formId, entityId);
            }
        },
        // Grid data is not supported => Use $B.Grid
        Data: {
            getValue: function(applicationId, formId, accessKey) {
                if (applicationId && formId && accessKey) {
                    return window["BizFormCore"].FormDataService.getValue(applicationId, formId, accessKey);
                }
            },
            setValue: function(applicationId, formId, accessKey, value) {
                if (applicationId && formId && accessKey) {
                    return window["BizFormCore"].FormDataService.setValue(applicationId, formId, accessKey, value);
                }
            },
            getFormData: function(applicationId, formId) {
                return window["BizFormCore"].FormDataService.getFormData(formId);
            }
        },
        Grid: {
            storage: {
                // user storage only for grid
            },
            extend: function(funcName, func) {
                $B.Grid.custom[funcName] = func;
            },
            custom: {
                // user define function for grid
            },
            reload: function(listId) {
                window["BizFormCore"].GridActionService.setPage(listId, null, true);
            },
            setPage: function(listId, page, forced) {
                window["BizFormCore"].GridActionService.setPage(listId, page, forced);
            },
            getListDataFromServer: function(applicationId, formId, accessAlias, filters, sorts, pageNo, pageSize, successCallback, errorCallback) {
                window["BizFormCore"].GridDataService.getListDataFromServer(applicationId, formId, accessAlias, filters, sorts, pageNo, pageSize, successCallback, errorCallback);
            },
            getListDataFromList: function(applicationId, formId, accessAlias, linkId, listId) {
                return window["BizFormCore"].GridDataService.getListDataFromList(applicationId, formId, accessAlias, linkId, listId);
            },
            getTotalListDataFromList: function(applicationId, formId, accessAlias, linkId, listId) {
                return window["BizFormCore"].GridDataService.getTotalListDataFromList(applicationId, formId, accessAlias, linkId, listId);
            },
            getUpdateListDataFromList: function(applicationId, formId, accessAlias, linkId, listId) {
                return window["BizFormCore"].GridDataService.getUpdateListDataFromList(applicationId, formId, accessAlias, linkId, listId);
            },
            getListDataFromListPk: function(applicationId, formId, accessAlias, linkId, listId, pk) {
                return window["BizFormCore"].GridDataService.getListDataFromList(applicationId, formId, accessAlias, linkId, listId, pk);
            },
            getUpdateListDataFromListPk: function(applicationId, formId, accessAlias, linkId, listId, pk) {
                return window["BizFormCore"].GridDataService.getUpdateListDataFromList(applicationId, formId, accessAlias, linkId, listId, pk);
            },
            deleteData: function(applicationId, formId, accessAlias, data, successCallback, errorCallback) {
                /**
                    data = [
                        {
                            "_pk": primary value1,
                            "_epk": external primary value1
                        }, {
                            "_pk": primary value2,
                            "_epk": external primary value2
                        }, {
                            "_pk": primary value3,
                            "_epk": external primary value2
                        },
                        ...
                    ]
                 */
                window["BizFormCore"].GridDataService.deleteData(applicationId, formId, accessAlias, data, successCallback, errorCallback);
            },
            updateData: function(applicationId, formId, accessAlias, updateDataList, successCallback, errorCallback) {
                window["BizFormCore"].GridDataService.updateData(applicationId, formId, accessAlias, updateDataList, successCallback, errorCallback);
            },
            createData: function(applicationId, formId, accessAlias, createDataList, successCallback, errorCallback) {
                window["BizFormCore"].GridDataService.createData(applicationId, formId, accessAlias, createDataList, successCallback, errorCallback);
            },
            getLoginUser: function() {
                return window["BizFormCore"].FormStateService.getLoginUser();
            },
            initPk: function() {
                return window["BizFormCore"].GridDataService.initPk();
            },
            getNow: function() {
                return new Date().toISOString();
            },
            selectedItems: function(rootDom) {
                return window["BizFormCore"].GridActionService.selectedItems(rootDom);
            }
        },
        WIH: {
            getPvMap: function() {
                if ($B.basicWIHActionClient) {
                    var _ = $B.Lodash();
                    var wihContext = window["BizFormHelper"].ObjectHelper.jsonCloneDeep(parent._workItemHandlerService.workitem);
                    var pv = _.get(wihContext, "variables");
                    if (pv && pv.length) {
                        return _.keyBy(pv, "name");
                    }
                }
                return null;
            },
            getPvByName: function(name) {
                var pv = $B.WIH.getPvMap(name);
                return pv ? pv[name] : null;
            },
            getResponsesInfo: function() {
                if ($B.basicWIHActionClient && typeof parent.getResponsesInfo === "function") {
                    return parent.getResponsesInfo();
                }
                return null;
            },
            getCurrentResponseId: function() {
                if ($B.basicWIHActionClient && typeof parent.getCurrentResponseId === "function") {
                    return parent.getCurrentResponseId();
                }
                return null;
            },
            getSelectedResponse: function() {
                if ($B.basicWIHActionClient && typeof parent.getSelectedResponse === "function") {
                    return parent.getSelectedResponse();
                }
                return null;
            }
        },
        ___clear: function() {
            $B.timeZone = null;
            delete($B.timeZoneHeader);
            timeZone = null;
            $B.basicWIHActionClient = null;
            $B.getQueryString.__proto__ = null;
            if (_tooltip.length) {
                window["BizFormHelper"].ComponentHelper.unbindTooltip(_tooltip);
                _tooltip = null;
            }
            if (_customValidation.length) {
                for (var i = 0; i < _customValidation.length; i++) {
                    if (typeof _customValidation[i].validationFunction === "function") {
                        _customValidation[i].validationFunction.__proto__ = null;
                    }
                    if (typeof _customValidation[i].callbackFunction === "function") {
                        _customValidation[i].callbackFunction.__proto__ = null;
                    }
                    _customValidation[i] = null;
                }
            }
            if ($B.Layout.dom) {
                var keys = Object.keys($B.Layout.dom);
                for (var i = 0; i < keys.length; i++) {
                    if (typeof $B.Layout.dom[keys[i]].focusFunction === "function") {
                        $B.Layout.dom[keys[i]].focusFunction.__proto__ = null;
                    }
                    $B.Layout.dom[keys[i]].focusFunction = null;
                }
            }
            var type = ["custom", "Inner", "Outer"];
            for (var i = 0; i < type.length; i++) {
                if ($B[type[i]]) {
                    var keys = Object.keys($B[type[i]]);
                    for (var j = 0; j < keys.length; j++) {
                        if (typeof $B[type[i]][keys[j]] === "function") {
                            $B[type[i]][keys[j]].__proto__ = null;
                        }
                        $B[type[i]][keys[j]] = null;
                    }
                }
                $B[type[i]] = null;
            }
            $B.__proto__ = null;
        }
    };
})();

// i18n
var $I = {};
