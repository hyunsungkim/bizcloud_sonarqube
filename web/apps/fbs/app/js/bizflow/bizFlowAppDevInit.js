// load main
var ___loadMain = (function() {
    var ___module = {
        ___design: false,
        ___documentation: false,
        ___modeler: false,
        ___studio: false,
        ___componentManifest: false
    };
    return function(module) {
        ___module[module] = true;
        var keys = Object.keys(___module);
        for (var i = 0; i < keys.length; i++) {
            if (!___module[keys[i]]) {
                return;
            }
        }
        try {
            window["BizFormApp"].AppManager.init();
            window["___loadMain"].__proto__ = null;
            window["___loadMain"] = null;
        } catch (e) {
            ___logout();
        }
    }
})();

// load common
var ___modules;
function ___loadCommon() {
    // modeler
    if (window["___applicationId"] === "_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U") {
        window["___loadMain"]("___studio");
        document.getElementById("___modeler").setAttribute("src", ___modules.___modeler);
     // studio
    } else if (!window["___applicationId"]) {
        window["___loadMain"]("___modeler");
        document.getElementById("___studio").setAttribute("src", ___modules.___studio);
    }
    ___modules = null;
    window["___loadCommon"].__proto__ = null;
    window["___loadCommon"] = null;
}

// init application
function _initApplication(module) {
    if (window["___applicationId"] && !window["___applicationId"].startsWith("_")) {
        document.body.setAttribute("style", "margin: 0; padding: 0; position: absolute; top: 0; right: 0; bottom: 0; left: 0; ");
    }

    // modeler or studio
    if (!window["___applicationId"] || window["___applicationId"] === "_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U") {
        window["___loadMain"]("___documentation");
        ___modules = module;
        document.getElementById("___common").setAttribute("src", module.___common);
     // documentation
    } else if (window["___applicationId"] === "_oXxyfTpnCFf5NJTcf8HUaRyVcSHpDc1") {
        window["___loadMain"]("___modeler");
        window["___loadMain"]("___studio");
        document.getElementById("___documentation").setAttribute("src", module.___documentation);
    } else {
        window["___loadMain"]("___documentation");
        window["___loadMain"]("___modeler");
        window["___loadMain"]("___studio");
    }

    // design
    if (window["___designMode"] || window["___versionMode"] || !window["___applicationId"]) {
        document.getElementById("___design").setAttribute("src", module.___design);
    } else {
        window["___loadMain"]("___design");
    }

    // component
    if (window["___designMode"] || !window["___applicationId"] || window["___applicationId"] === "_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U") {
        document.getElementById("___componentManifest").setAttribute("src", module.___componentManifest);
    } else {
        window["___loadMain"]("___componentManifest");
    }

    if (_setPreviewMode) {
        _setPreviewMode();
    }
    
    window["_initApplication"].__proto__ = null;
    window["_initApplication"] = null;
}

var ___resizePreviewMode;
var ___resizeTimer;
var ___resizeDiv;
var ___resizeBfForm;
var ___resizeWatch;

function _resizePreviewMode() {
    var width = window.outerWidth;
    var height = window.outerHeight < 600 ? 600 : window.outerHeight;
    var mode = ___resizePreviewMode;
    if (width < 392) {
        width = 392;
        mode = "mobile";
    } else if (width < 687) {
        mode = "mobile";
    } else if (width < 1100) {
        mode = "tablet";
    } else {
        mode = "desktop";
        if (window.outerHeight < 750) {
            height = 750;
        }
    }
    if (width !== window.outerWidth || height !== window.outerHeight) {
        if (___resizeTimer) {
            clearTimeout(___resizeTimer);
        }
        ___resizeTimer = setTimeout(function() {
            window.resizeTo(width, height);
            if (mode !== ___resizePreviewMode) {
                _drawPreviewMode();
            }
        }, 300);
    }
    if (mode !== ___resizePreviewMode) {
        _drawPreviewMode();
    }
}

function _drawPreviewMode() {
    var scrollTop;
    var id = "___previewAppDevForm";
    if (___resizeDiv) {
        scrollTop = ___resizeDiv.querySelector("#" + id).parentElement.scrollTop;
        ___resizeBfForm = ___resizeBfForm.parentElement.removeChild(___resizeBfForm);
        ___resizeDiv.parentElement.removeChild(___resizeDiv);
    }
    var div = document.createElement("div");
    var pDiv = "<div id='" + id + "'></div>";
    if (window.outerWidth < 687) {
        ___resizePreviewMode = "mobile";
    } else if (window.outerWidth < 1100) {
        ___resizePreviewMode = "tablet";
    } else {
        ___resizePreviewMode = "desktop";
    }
    switch (___resizePreviewMode) {
        case "mobile":
            ___resizeWatch.style.display = "block";
            div.innerHTML =  ""
                + "<div style='position: fixed; left: 4px; top:0px; width: 282px; height: 88px; background-image: url(\"css/preview/mobile_top_left.png\"); z-index: 1;'></div>"
                + "<div style='position: fixed; left: 286px; right: 91px; top:0px; height: 87px; background-image: url(\"css/preview/mobile_top_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 4px; top: 0px; width: 87px; height: 88px; background-image: url(\"css/preview/mobile_top_right.png\"); background-color: #ffffff; z-index: 2;'></div>"
                + "<div style='position: fixed; left:0px; right: 0px; top: 27px; height: 10px; background-image: url(\"css/preview/mobile_top_camera.png\"); background-repeat: no-repeat; background-position-x: center; z-index: 3;'>&nbsp;</div>"
                + "<div style='position: fixed; left: 0px; top: 87px; bottom: 59px; width: 62px; background-image: url(\"css/preview/mobile_left_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; left: 0px; top: 87px; width: 62px; height: 397px; background-image: url(\"css/preview/mobile_left_top.png\"); z-index: 1;'></div>"
                + "<div style='overflow: auto; position: fixed; left: 30px; right: 25px; top: 87px; bottom: 59px; z-index: 2;'>"
                + pDiv
                + "</div>"
                + "<div style='position: fixed; right: 0px; top: 87px; bottom: 59px; width: 91px; background-image: url(\"css/preview/mobile_right_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 0px; top: 87px; width: 91px; height: 182px; background-image: url(\"css/preview/mobile_right_top.png\"); z-index: 1;'></div>"
                + "<div style='position: fixed; left: 0px; bottom:0px; width: 62px; height: 59px; background-image: url(\"css/preview/mobile_bottom_left.png\"); background-color: #ffffff; z-index: 1;'></div>"
                + "<div style='position: fixed; position: fixed; left: 62px; right: 91px; bottom: 0px; height: 59px; background-image: url(\"css/preview/mobile_bottom_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 0px; bottom: 0px; width: 91px; height: 59px; background-image: url(\"css/preview/mobile_bottom_right.png\"); background-color: #ffffff; z-index: 2;'></div>";
            break;
        case "tablet":
            ___resizeWatch.style.display = "none";
            div.innerHTML =  ""
                + "<div style='position: fixed; left: 0px; top:0px; width: 570px; height: 38px; background-image: url(\"css/preview/tablet_top_left.png\"); z-index: 1;'></div>"
                + "<div style='position: fixed; left: 570px; right: 102px; top:0px; height: 38px; background-image: url(\"css/preview/tablet_top_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 0px; top: 0px; width: 102px; height: 38px; background-image: url(\"css/preview/tablet_top_right.png\"); background-color: #ffffff; z-index: 2;'></div>"
                + "<div style='position: fixed; left: 0px; top: 38px; bottom: 47px; width: 48px; background-image: url(\"css/preview/tablet_left_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; left: 0px; top: 38px; width: 48px; height: 400px; background-image: url(\"css/preview/tablet_left_top.png\"); z-index: 1;'></div>"
                + "<div style='overflow: auto; position: fixed; left: 35px; right: 38px; top: 42px; bottom: 145px; z-index: 2;'>"
                + pDiv
                + "</div>"
                + "<div style='position: fixed; left:0px; right: 0px; bottom: 45px; height: 93px; background-image: url(\"css/preview/tablet_app.png\"); background-repeat: no-repeat; background-position-x: center;'>&nbsp;</div>"
                + "<div style='position: fixed; right: 0px; top: 38px; bottom: 47px; width: 51px; background-image: url(\"css/preview/tablet_right_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 0px; top: 38px; width: 51px; height: 127px; background-image: url(\"css/preview/tablet_right_top.png\"); z-index: 1;'></div>"
                + "<div style='position: fixed; left: 0px; bottom:0px; width: 48px; height: 47px; background-image: url(\"css/preview/tablet_bottom_left.png\"); background-color: #ffffff; z-index: 1;'></div>"
                + "<div style='position: fixed; position: fixed; left: 48px; right: 51px; bottom: 0px; height: 47px; background-image: url(\"css/preview/tablet_bottom_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 2px; bottom: 0px; width: 49px; height: 47px; background-image: url(\"css/preview/tablet_bottom_right.png\"); background-color: #ffffff; z-index: 2;'></div>";
            break;
        case "desktop":
            ___resizeWatch.style.display = "none";
            div.innerHTML =  ""
                + "<div style='position: fixed; left: 0px; top:0px; width: 859px; height: 115px; background-image: url(\"css/preview/desktop_top_left.png\"); z-index: 1;'></div>"
                + "<div style='position: fixed; left: 196px; right: 196px; top:0px; height: 115px; background-image: url(\"css/preview/desktop_top_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 0px; top: 0px; width: 196px; height: 115px; background-image: url(\"css/preview/desktop_top_right.png\"); background-color: #ffffff; z-index: 2;'></div>"
                + "<div style='position: fixed; left:0px; right: 0px; top: 22px; height: 10px; background-image: url(\"css/preview/desktop_top_camera.png\"); background-repeat: no-repeat; background-position-x: center; z-index: 3;'>&nbsp;</div>"
                + "<div style='position: fixed; left: 0px; top: 115px; bottom: 58px; width: 51px; background-image: url(\"css/preview/desktop_left_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; left: 0px; top: 115px; width: 57px; height: 513px; background-image: url(\"css/preview/desktop_left_top.png\"); z-index: 1;'></div>"
                + "<div style='overflow: auto; position: fixed; left: 48px; right: 48px; top: 120px; bottom: 58px; z-index: 2;'>"
                + pDiv
                + "</div>"
                + "<div style='position: fixed; right: 0px; top: 38px; bottom: 58px; width: 57px; background-image: url(\"css/preview/desktop_right_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; left: 0px; bottom:0px; width: 57px; height: 58px; background-image: url(\"css/preview/desktop_bottom_left.png\"); background-color: #ffffff; z-index: 1;'></div>"
                + "<div style='position: fixed; position: fixed; left: 57px; right: 57px; bottom: 0px; height: 58px; background-image: url(\"css/preview/desktop_bottom_bg.png\"); z-index: 0;'></div>"
                + "<div style='position: fixed; right: 0px; bottom: 0px; width: 57px; height: 58px; background-image: url(\"css/preview/desktop_bottom_right.png\"); background-color: #ffffff; z-index: 2;'></div>";
            break;
    }
    document.body.appendChild(div);

    if (!___resizeBfForm) {
        ___resizeBfForm = document.body.querySelector("BfForm[bf-application-id]");
        ___resizeBfForm.style.display = "block";
        document.querySelector("#" + id).appendChild(___resizeBfForm.parentElement.removeChild(___resizeBfForm));
    } else {
        document.querySelector("#" + id).appendChild(___resizeBfForm);
        if (scrollTop) {
            div.querySelector("#" + id).parentElement.scrollTop = scrollTop;
        }
    }

    ___resizeDiv = div;
}

// set preview mode
function _setPreviewMode() {
    if (window["___previewMode"]) {
        document.body.style.padding = "0 !important";
        document.body.style.margin = "0 !important";

        if (!___resizeWatch) {
            ___resizeWatch = document.createElement("div");
            ___resizeWatch.style.position = "fixed";
            ___resizeWatch.style.top = "65px";
            ___resizeWatch.style.left = "35px";
            ___resizeWatch.style.fontWeight = "bold";
            ___resizeWatch.style.zIndex = "10";
            ___resizeWatch.style.display = "none";
            ___resizeWatch.setAttribute("id", "___watch");
            document.body.appendChild(___resizeWatch);
            var timer = function () {
                var date = new Date();
                ___resizeWatch.innerHTML = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                setTimeout(timer, 1000);
            };
            timer();
        }

        _drawPreviewMode();
        window.addEventListener("resize", _resizePreviewMode);
    }
}

// intro or loading
function _setIntroOrLoading() {
    if (!window["___designMode"]) {
        if (!window["___applicationId"] || window["___applicationId"].startsWith("_")) {
            document.getElementById("___intro").style.display = "block";
        } else {
            document.getElementById("___loading").style.display = "block";
        }
    }
    window["_setIntroOrLoading"].__proto__ = null;
    window["_setIntroOrLoading"] = null;
}

// theme
function _setThemeManifest() {
    var styles = 0;

    function pageInit() {
        styles--;
        if (styles === 0) {
            document.querySelector("BfForm").style.display = "block";
            window["_setThemeManifest"].__proto__ = null;
            window["_setThemeManifest"] = null;
        }
    }

    themeName = window["___tempTheme"];
    if (window["_themeCustomManifest"]) {
        if (!window["___rootMode"] || applyApp === "true") {
            _themeCustomManifest.theme = true;
            _themeCustomManifest.folder = "_custom";
        }
        _themeManifest._themeCustomManifest = _themeCustomManifest;
    }
    if (_themeManifest) {
        var _themes = Object.keys(_themeManifest);
        for (var i = 0; i < _themes.length; i++) {
            var theme = _themeManifest[_themes[i]];
            if (Array.isArray(theme.files)) {
                for (var j = 0; j < theme.files.length; j++) {
                    if (theme.folder) {
                        styles++;
                        var file = (theme.theme ? "theme/" + themeName + "/" : "") + theme.folder.replace(/\$/, "") + "/" + theme.files[j];
                        $B.loader.css("css/" + file, theme.version, pageInit, pageInit, pageInit);
                    }
                }
            }
        }
    }
}

// init
function ___init() {
    window["___rootMode"] = null;
    window["___runtimeMode"] = null;
    window["___previewString"] = $B.getQueryString("preview_mode");
    window["___designMode"] = $B.getQueryString("design_mode") ? true : false;
    window["___popupMode"] = $B.getQueryString("popup_mode") ? true : false;
    window["___previewMode"] = window["___previewString"] && window["___previewString"] !== "document" ? true : false;
    window["___versionMode"] = $B.getQueryString("version_mode");
    window["___runtimeMode"] = $B.getQueryString("isfi");
    window["___accessibility"] = $B.getQueryString("accessibility") === "y" ? true : ($B.getQueryString("accessibility") === "n" ? false : null);
    window["___customComponent"] = $B.getQueryString("custom_component");

    if (window["___runtimeMode"]) {
        var information = atob(decodeURIComponent(window["___runtimeMode"])).split("|");
        window["___applicationId"] = information[0];
        window["___publishId"] = information[1];
        window["___formId"] = information[2];
        window["___tempTheme"] = information[3];
        information = null;
    } else {
        window["___applicationId"] = $B.getQueryString("application_id");
        window["___formId"] = $B.getQueryString("form_id");
        window["___tempTheme"] = $B.getQueryString("theme");
    }

    var favicon;
    if (window["___applicationId"] === "_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U") {
        document.title = "BizFlow AppDev Modeler";
        favicon = "modeler";
    } else if (window["___applicationId"] === "_oXxyfTpnCFf5NJTcf8HUaRyVcSHpDc1") {
        document.title = "BizFlow AppDev Project Document";
        favicon = "modeler";
    } else {
        document.title = "BizFlow AppDev Studio";
        favicon = "studio";
    }
    document.getElementById("___favicon").href = "ico/" + favicon + "/favicon.ico?v=" + window["___version"];

    if (window["___previewMode"]) {
        window["___procid"] = -1;
        window["___workseq"] = -1;
        window["___actseq"] = -1;
    } else {
        window["___procid"] = $B.getQueryString("procid");
        window["___workseq"] = $B.getQueryString("workseq");
        window["___actseq"] = $B.getQueryString("actseq");
        window["_setPreviewMode"].__proto__ = null;
        window["_drawPreviewMode"].__proto__ = null;
        window["_resizePreviewMode"].__proto__ = null;
        window["_setPreviewMode"] = null;
        window["_drawPreviewMode"] = null;
        window["_resizePreviewMode"] = null;
    }

    window["___tempTheme"] = window["___tempTheme"] ? window["___tempTheme"] : (!window["___applicationId"] ? "dark" : "light");

    if (!window["___designMode"] && !window["___popupMode"] && !window["___previewMode"]) {
        window["___rootMode"] = true;
    }

    window["___init"].__proto__ = null;
    window["___init"] = null;
}
___init();