// WIH Action Callback
function ___onWorkitemReload(basicWIHAction, unblock) {
    basicWIHAction.lastAction = "";
    basicWIHAction.lastEvent = "";
    basicWIHAction.continueOption = "continue";
    basicWIHAction.skipEvent = false;
    if (unblock) {
        setTimeout(function() {
            parent.BizFlowHelper.unblock();
        });
    }
}

var ___complete = false;
function onWorkitemComplete(basicWIHAction) {
    if (basicWIHAction) {
        if (___complete) {
            ___complete = false;
            ___onWorkitemReload(basicWIHAction);
            return true;
        }
        setTimeout(function() {
            parent.BizFlowHelper.block();
        });
        ___complete = true;
        function callback(failed) {
            if (failed) {
                ___complete = false;
                ___onWorkitemReload(basicWIHAction, true);
            } else {
                basicWIHAction.complete();
            }
        }
        window["BizFormCore"].WihActionService.onWorkitemComplete(___applicationId, ___formId, ___procid, ___workseq, ___actseq, callback);
        return false;
    }
}

var ___save = false;
function onWorkitemSave(basicWIHAction) {
    if (basicWIHAction) {
        if (___save) {
            ___save = false;
            ___onWorkitemReload(basicWIHAction);
            return true;
        }
        setTimeout(function() {
            parent.BizFlowHelper.block();
        });
        ___save = true;
        function callback(failed) {
            if (failed) {
                ___save = false;
                ___onWorkitemReload(basicWIHAction, true);
            } else {
                basicWIHAction.save();
            }
        }
        window["BizFormCore"].WihActionService.onWorkitemSave(___applicationId, ___formId, ___procid, ___workseq, ___actseq, callback);
        return false;
    }
}

var ___exit = false;
function onWorkitemExit() {
    if ($B.basicWIHActionClient) {
        if (___exit) {
            ___exit = false;
            ___onWorkitemReload($B.basicWIHActionClient);
            return true;
        }
        setTimeout(function() {
            parent.BizFlowHelper.block();
        });
        ___exit = true;
        function callback(failed) {
            if ($B.basicWIHActionClient) {
                if (failed) {
                    ___exit = false;
                    ___onWorkitemReload($B.basicWIHActionClient, true);
                } else {
                    $B.basicWIHActionClient.exit({silentExit: true});
                }
            }
        }
        window["BizFormCore"].WihActionService.onWorkitemExit(___applicationId, ___formId, ___procid, ___workseq, ___actseq, callback);
        return false;
    }
}
