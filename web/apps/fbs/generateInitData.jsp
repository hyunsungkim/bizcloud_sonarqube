
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.hs.bf.web.beans.HWSession" %>
<%@ page import="com.hs.bf.web.beans.HWSessionFactory" %>

<%
    String sessionInfoXML1 = AcmManager.getInstance().getAuthCredentialKey(session).getCredentialKey();
    String serverIP = ServletUtil.getParameterValue(request, "serverIP", "bizcloud.bizflow.com");
    int serverPort = ServletUtil.getParameterIntValue(request, "serverPort", 7201);
    String loginID = ServletUtil.getParameterValue(request, "loginID", "hkim");
    String password = ServletUtil.getParameterValue(request, "password", "1");

    HWSessionFactory hwSessionFactory = new HWSessionFactory();
    hwSessionFactory.setDefaultImplClassName(HWSessionFactory.TCPImpl);
    HWSession hwSession = hwSessionFactory.newInstance();
    String sessionInfoXML = null;
    if(sessionInfoXML1 == null || sessionInfoXML1.equals("")){
        sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, password,true);
    } else {
        System.out.println("sessionInfoXML1!~~~~");
        sessionInfoXML = sessionInfoXML1;
        out.println("-----");
    }

%>
<html>
<head>
    <script src="../../includes/node_modules/jquery/dist/jquery.min.js"></script>

    <script>
        var workspaceId = "";
        var projectId = "";
        var formId = "";
        function initSession() {
            var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
            var url = "/bizflowappdev/services/bizflow/auth/loginByToken.json?tz=" + timeZone;

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    <%--"TOKEN": "<%=sessionInfoXML.replaceAll("\"", "\\\\\"")%>"--%>
                    "TOKEN": "<%=sessionInfoXML.replaceAll("\"", "\\\\\"")%>"
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                    console.log('status', status);
                    console.log('xhr', xhr);
                    console.log(xhr.getResponseHeader("JSESSIONID"));
                    console.log(xhr.getAllResponseHeaders());
            $.ajax({
                url: "/bizflowappdev/services/data/create/fbs.workspace-CreateWorkspace.json",
                type: "POST",
                data: {
                    "name": "SampleWorkspace2",
                    "description": "Initial Sample Workspace2"
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                    workspaceId = response.id;
                    updateWorkspaceAuth('A');
                    updateWorkspaceAuth('M');
                    updateWorkspaceAuth('R');
                    updateProject();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function getData() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/get/fbs.project-GetProject.json",
                type: "GET",
                success: function (response, status, xhr) {
                    console.log('response', response);
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateWorkspace() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/create/fbs.workspace-CreateWorkspace.json",
                type: "POST",
                data: {
                    "name": "SampleWorkspace",
                    "description": "Initial Sample Workspace"
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                    workspaceId = response.id;
                    updateWorkspaceAuth('A');
                    updateWorkspaceAuth('M');
                    updateWorkspaceAuth('R');
                    updateProject();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("workspaceId="+workspaceId);
        }

        function updateWorkspaceAuth(permission) {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/run/fbs.workspace-CreatePermission@.json",
                type: "POST",
                data: {
                    "permission": permission,
                    "memberType": "D",
                    "memberName": "Root",
                    "memberId": "9000000000",
                    "id": workspaceId
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("workspaceId="+workspaceId);
        }

        function updateProject() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/bizflowappdev/services/data/create/fbs.project-CreateProject.json",
                type: "POST",
                data: {
                    "workspaceId": workspaceId,
                    "name": "SampleProject",
                    "description": "Initial Sample Project"
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                    projectId = response.id;
                    updateProjectAuth('A');
                    updateProjectAuth('M');
                    updateProjectAuth('R');
                    updateForm();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("projectId="+projectId);
        }

        function updateProjectAuth(permission) {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/run/fbs.project-CreatePermission@.json",
                type: "POST",
                data: {
                    "permission": permission,
                    "memberType": "D",
                    "memberName": "Root",
                    "memberId": "9000000000",
                    "id": projectId
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateForm() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/create/fbs.form-CreateForm.json",
                type: "POST",
                data: {
                    "projectId": projectId,
                    "name": "SampleForm",
                    "description": "Initial Sample Form"
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                    formId = response.id;
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("formId="+formId);
        }

        function _onload() {
            initSession();
            getData();
        }
    </script>
</head>
<body onload="_onload();">
</body>
</html>
