<%@ page import="com.bizflow.io.core.net.util.RequestIdentifierKeeper" %>
<%@ page import="com.bizflow.io.core.util.UUIDGenerator" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.acm.util.AnonymousUtil" %>
<%@ page import="com.bizflow.io.services.core.model.Member" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.TimeZone" %>
<%
    /**
     * The ISFI of the first AppDev form to be loaded before a user logs in AppDev. Change this with the ISFI of your form
     */
    final String isfi1 = "bDdmM2Q4MjdlYjZhMDQyNTRiMWMwNjYxM2RjMDA5ZmE1fHQ4YTA3ZTRlYTQ2OWM0ZTlhOTk5ZmJlZWU4NzI4NTBkN3xyZjYwM2JjNDg3ZDViNGYzNmI1ZmJmZGMxMGM4ZTFhYjl8bGlnaHQ%3D";

    /**
     * The ISFI of the second AppDev form the be loaded after a user logs in AppDev. Change this with the ISFI of your form
     */
    final String isfi2 = "bDdmM2Q4MjdlYjZhMDQyNTRiMWMwNjYxM2RjMDA5ZmE1fHQ4YTA3ZTRlYTQ2OWM0ZTlhOTk5ZmJlZWU4NzI4NTBkN3xyZjYwM2JjNDg3ZDViNGYzNmI1ZmJmZGMxMGM4ZTFhYjl8bGlnaHQ%3D";

    /**
     * Anonymous Application Name. Change this with your application name + Anonymous, for example, "myApp Anonymous"
     */
    final String anonymousName = "AppDev Anonymous";

    /**
     * The organization name for Anonymous. Change this with the organization name for anonymous users
     */
    final String anonymousOrganizationName = "Anonymous";

    /**
     * You don't need to update below codes unless you need to add new features or change existing features.
     */
    String isfi = isfi1;
    Member member = null;
    String queryString = request.getQueryString();
    queryString = null == queryString ? "" : "&" + queryString;

    RequestIdentifierKeeper.setRequestIdentifier(null);
    TimeZone timeZone = ServletUtil.getClientTimeZone(request);
    boolean iframeMode = ServletUtil.getParameterBooleanValue(request, "i", true);
    boolean validRequest = AcmManager.getInstance().isValidRequest(request) && null != AcmManager.getInstance().getUserSessionMember(session);

    if (!validRequest) {
        if (null != timeZone) {
            member = AnonymousUtil.createAnonymousCredential(anonymousName, anonymousOrganizationName, timeZone, request);
            validRequest = null != member;
        } else {
%>
<html>
<head>
    <script>
        try {
            if (/Trident\/|MSIE/.test(window.navigator.userAgent) && window.document.documentMode) {
                top.opener = null;
            }
        } catch (e) {
        }
    </script>
    <script>
        var iframeMode = <%=iframeMode ? "true" : "false"%>;
        var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
        var url = '' + this.location.href;

        function _onload() {
            if (iframeMode) {
                var form = document.forms[0];
                form.tz.value = timeZone;
                form.submit();
            } else {
                url += (-1 == url.indexOf('?') ? '?' : '&') + 'tz=' + encodeURIComponent(timeZone);
                this.location.href = url;
            }
        }
    </script>
</head>
<body onload="_onload();">
<form method="GET">
    <input type="hidden" name="tz" value="">
    <%
        Enumeration<String> en = request.getParameterNames();
        while (en.hasMoreElements()) {
            String name = en.nextElement();
            String value = request.getParameter(name);
            if (null != value) {
    %>
    <input type="hidden" name="<%=Encode.forHtmlAttribute(name)%>" value="<%=Encode.forHtmlAttribute(value)%>">
    <%
            }
        }
    %>
</form>
</body>
</html>
<%
        }
    }

    if (validRequest) {
        member = null == member ? AcmManager.getInstance().getUserSessionMember(session) : member;
        if (!UUIDGenerator.ZeroId.equals(member.getId())) {
            isfi = isfi2;
        }

        String home = request.getRequestURI();
        if (iframeMode) {
%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<head>
    <script>
        try {
            if (/Trident\/|MSIE/.test(window.navigator.userAgent) && window.document.documentMode) {
                top.opener = null;
            }
        } catch (e) {
        }
    </script>
</head>
<body style="overflow: hidden;border: 0;padding: 0;margin: 0;">
<iframe id="frame" name="frame" style="width: 100%;height: 100%;border: 0;padding: 0;margin: 0;" frameborder="0"
        src="<%=request.getContextPath()%>/apps/fbs/app/?$T=<%=URLEncoder.encode(home, "UTF-8")%>&isfi=<%=isfi%><%=queryString%>"></iframe>
</body>
</html>
<%
} else {
%>
<html>
<head>
    <script>
        this.location = "<%=request.getContextPath()%>/apps/fbs/app/?$H=<%=URLEncoder.encode(home, "UTF-8")%>&isfi=<%=isfi%><%=queryString%>";
    </script>
</head>
</html>
<%
        }
    }
%>
