<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AuthCredentialKey" %>
<%@ page import="com.bizflow.io.services.core.model.Tenant" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
    String contextPath = request.getContextPath();
    boolean noBrand = "n".equalsIgnoreCase(request.getParameter("b"));

    AuthCredentialKey authCredentialKey = AcmManager.getInstance().getAuthCredentialKey(session);
    String loginId = null != authCredentialKey ? authCredentialKey.getLoginId() : "";

    String bgColor = "dark";
    String textColor = "white";
    String listBgColor = "light";
    String theme = ServletUtil.getParameterValue(request, "theme", "dark");
    if ("light".equalsIgnoreCase(theme)) {
        bgColor = "light";
        textColor = "dark";
        listBgColor = "light";
    }

    final String _theme = Encode.forHtmlAttribute(theme);
    final String fbsVersion = MoonManager.getInstance().get("fbs").getVersion();
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <title>BizFlow AppDev Management</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/fontawesome-free/css/all.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
    </script>
    <style>
        .card-header h5 {
            margin: 0;
        }

        a.list-group-item {
            color: #343a40;
        }
    </style>
</head>

<body onload="mars$util$.resizeFrameWindowHeight()" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="page-header text-center" style="padding: 15px 0 10px 0;">
    <h1>BizFlow AppDev Management</h1>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#apps" aria-expanded="false" aria-controls="apps"><h5>Application</h5></div>
                <div class="card-block collapse show" id="apps">
                    <div class="list-group list-group-flush">
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/app/index.html")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-ad" href="<%=contextPath%>/apps/fbs/app/?v=<%=fbsVersion%>"><i class="fas fa-layer-group fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Studio</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-am" href="<%=contextPath%>/apps/fbs/app/?v=<%=fbsVersion%>&application_id=_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U&form_id=_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U&theme=dark"><i class="fa fa-table fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Modeler</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/i18n/i18n-message-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-i18n" href="<%=contextPath%>/apps/fbs/tools/i18n/i18n-message-manager.jsp?theme=<%=_theme%>&t=true"><i class="fa fa-comment-dots fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev I18N Message Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/lookup/lookup-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/lookup/lookup-manager.jsp?theme=<%=_theme%>&t=true"><i class="fa fa-table fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Lookup Data Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/integration/api-integration-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/integration/api-integration-manager.jsp?theme=<%=_theme%>&t=true"><i class="fab fa-asymmetrik fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev API Integration Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/file/file-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/file/file-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-file fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev File Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/file/attachment-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/file/attachment-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-paperclip fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Attachment Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/context/context-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/context/context-manager.jsp?theme=<%=_theme%>&t=true"><i class="fab fa-contao fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Context Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/migration/migration-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/migration/migration-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-plane-departure fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Migration Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/form/form-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/form/form-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-newspaper fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Form Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/authority/authority-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/authority/authority-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-users-cog fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Authority Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/configuration/configuration-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/configuration/configuration-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-tools fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Configuration Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/component/component-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/component/component-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-object-group fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Component Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/viewer/adx-viewer.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/viewer/adx-viewer.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-microscope fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev ADX Viewer</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/chat/chat-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-cm" href="<%=contextPath%>/apps/fbs/tools/chat/chat-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-comment-dots fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Chat</a>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2"></div>
    </div>

    <br>

    <div class="row justify-content-md-center">
        <!--
        <form method="POST" name="form1">
            <div class="input-group">
                <select name="theme" onchange="document.form1.submit();" class="form-control form-control-sm btn-outline-secondary">
                    <option value="dark" <%="dark".equalsIgnoreCase(theme) ? "selected" : ""%>>Dark Theme</option>
                    <option value="light" <%="light".equalsIgnoreCase(theme) ? "selected" : ""%>>Light Theme</option>
                </select>
            </div>
        </form>
        -->
        <div style="padding-left: 10px;">
            <%
                Tenant tenant = AcmManager.getInstance().getUserSessionTenant(session);
                if (null != tenant) {
            %>
            <a class="btn btn-sm btn-outline-info" href="<%=contextPath%>/admin/logout.jsp?r=<%=contextPath%>/welcome/<%=tenant.getIdentifier()%>/apps/fbs/admin">
                logout <%=loginId%>@<%=tenant.getName()%>(<%=tenant.getIdentifier()%>)
            </a>
            <%} else {%>
            <a class="btn btn-sm btn-outline-info" href="<%=contextPath%>/admin/logout.jsp?r=<%=contextPath%>/apps/fbs/admin">
                logout <%=loginId%>
            </a>
            <%}%>
        </div>
    </div>
</div>

<br>
<br>
<%if (!noBrand) { %>
<%@include file="../../../copyright.html" %>
<%}%>

</body>
</html>