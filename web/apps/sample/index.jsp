<%@ page import="com.bizflow.io.core.util.UUIDGenerator" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.acm.AuthCredentialKey" %>
<%@ page import="com.bizflow.io.services.core.model.Department" %>
<%@ page import="com.bizflow.io.services.core.model.Member" %>
<%@ page import="com.bizflow.io.services.core.model.Organization" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.TimeZone" %>
<%
    TimeZone timeZone = ServletUtil.getClientTimeZone(request);
    if (null != timeZone) {
        if (!AcmManager.getInstance().isValidRequest(request)) {
            String id = UUIDGenerator.ZeroId;
            String credentialKey = "PUBLIC-ACCESS";

            Member member = new Member(id, id, credentialKey, new Date(), ServletUtil.getRemoteAddress(request), AuthCredentialKey.CredentialKeyType.Other);
            member.addProperty("name", "IT-Automation");
            member.setSystemRole("Anonymous");
            member.setDepartment(new Department("Anonymous", new Organization("TriWest Veteran Portal")));
            member.setTimeZone(timeZone);
            member.setLanguage(ServletUtil.getRequesterLanguage(request, ServletUtil.DefaultLanguage));
            member.setIpAddressRestriction(Boolean.FALSE);

            AcmManager.getInstance().setAuthCredentialKey(session, member);
            AcmManager.getInstance().setUserSessionMember(session, member);
        }
%>
<html>
<head>
    <script>
        this.location = "<%=request.getContextPath()%>/apps/fbs/app/?isfi=ZzVmNzU1YTk2MTZjYjRhNmJhZTA4N2RhMTdkZmZiNjlkfHExODk0YmY0MWQzZDc0MWM3YjZiMTBmNWY0MDQzOTQzOXxoMmJlNzMyNDc5ODAxNDNmZGEwYzU5OTUwMmYwZWYyM2Z8bGlnaHQ%3D&preview_mode";
    </script>
</head>
</html>
<%
} else {
%>
<html>
<head>
    <script>
        var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
        this.location.href = this.location.href + "?tz=" + encodeURIComponent(timeZone);
    </script>
</head>
</html>
<%
    }
%>
