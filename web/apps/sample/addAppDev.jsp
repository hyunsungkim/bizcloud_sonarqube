<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*" %>
<%@ page import="com.hs.bf.web.beans.*" %>
<%@ page import="com.hs.bf.web.xmlrs.*" %>
<%!

%>
<%

    boolean forceCheckOut = true;

    HWSessionFactory hwSessionFactory = new HWSessionFactory();
    hwSessionFactory.setDefaultImplClassName(HWSessionFactory.TCPImpl);
    HWSession hwSession = hwSessionFactory.newInstance();
    String sessionInfoXML = hwSession.logIn(serverIP, serverPort, "administrator", password,true);

    HWSessionInfo hwSessionInfo = new HWSessionInfo();
    hwSessionInfo.reset (sessionInfoXML);

    // Check out
    HWInteger reasonNumber = new HWInteger();
    hwSession.checkOutOrg(sessionInfoXML, forceCheckOut, "9000000000", reasonNumber);

    // User info
    XMLResultSetImpl xrsUsers = new XMLResultSetImpl();
    xrsUsers.createResultSet("HWUsers", "HWUSER");

    xrsUsers.add();
    xrsUsers.setFieldValueAt(0, "ID", "_USER00001");    // temp id. this should have the prefix '_USER'.
    xrsUsers.setFieldValueAt(0, "LICENSETYPE", "N");    // Named
    xrsUsers.setFieldValueAt(0, "INHERITTYPE", "P");    // InheritP
    xrsUsers.setFieldValueAt(0, "NAME", "AppDev User");
    xrsUsers.setFieldValueAt(0, "MANAGERID", "0000000000");
    xrsUsers.setFieldValueAt(0, "EMAIL", "");
    xrsUsers.setFieldValueAt(0, "EMPLOYEENUMBER", "");
    xrsUsers.setFieldValueAt(0, "JOBTITLEID", "9999999999");
    xrsUsers.setFieldValueAt(0, "JOBTITLEIDA", "");
    xrsUsers.setFieldValueAt(0, "JOBTITLEIDB", "");
    xrsUsers.setFieldValueAt(0, "DEPTID", "9000000000");
    xrsUsers.setFieldValueAt(0, "SERVERID", (String) hwSessionInfo.get("ServerID"));
    xrsUsers.setFieldValueAt(0, "ISABSENT", "F");
    xrsUsers.setFieldValueAt(0, "STATE", "N");  // Normal
    xrsUsers.setFieldValueAt(0, "JOBTITLENAME", "Administrator");
    xrsUsers.setFieldValueAt(0, "JOBTITLENAMEA", "");
    xrsUsers.setFieldValueAt(0, "JOBTITLENAMEB", "");
    xrsUsers.setFieldValueAt(0, "DEPTNAME", "Root");
    xrsUsers.setFieldValueAt(0, "SHORTNAME", "AppDev User");
    xrsUsers.setFieldValueAt(0, "LOGINID", loginID);
    xrsUsers.setFieldValueAt(0, "TYPE", "U");
    xrsUsers.setFieldValueAt(0, "DISPORDER", "0");
    xrsUsers.setFieldValueAt(0, "PASSWORD", "224155018038064014231021037079227131083195211000");
    xrsUsers.setFieldValueAt(0, "CHANGEPASSWD", "F");
    xrsUsers.setFieldValueAt(0, "PASSWDFAILCOUNT", "0");

    // License Group
    XMLResultSetImpl xrsLicenseGroups = new XMLResultSetImpl();
    xrsLicenseGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");

    // licsgrp001: System Administrator , licsgrp002: General Administrator , licsgrp003: Designer,
    // licsgrp004: User                 , licsgrp005: OfficeEngine User     , licsgrp006: Report Designer,
    // licsgrp007: Report User          , licsgrp008: WebMaker Designer     , licsgrp009: AppDev Designer

    int objectRow = 0;
    for(int i=0 ; i < auth9.length(); i ++){
        char theAuth = auth9.charAt(i);
        if(theAuth != '0'){
            objectRow = xrsLicenseGroups.add();
            xrsLicenseGroups.setFieldValueAt(objectRow, "USERGROUPID", "licsgrp00" + (i+1));   // 'System Administrator ' license
            xrsLicenseGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001"); // temp id. This should have the prefix '_USER'.
            xrsLicenseGroups.setFieldValueAt(objectRow, "TYPE", "U");
            xrsLicenseGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
        }
    }

    // Authority Group
    XMLResultSetImpl xrsAuthGroups = new XMLResultSetImpl();
    xrsAuthGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");

    for(int i=0 ; i < auth9.length(); i ++) {
        char theAuth = auth9.charAt(i);
        if (theAuth != '0') {
            if( i < 4 ) {
                objectRow = xrsAuthGroups.add();
                xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 1));
                xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");    // temp id. This should have the prefix '_USER'.
                xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
            }

            if( i > 2 ){
                objectRow = xrsAuthGroups.add();
		if(i < 8){
                    xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 2));
		} else {
                    xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp0" + (i + 2));
		}
                xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");    // temp id. This should have the prefix '_USER'.
                xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
            }
        }
    }

    // Add user
    hwSession.updateOrg(sessionInfoXML, null, xrsUsers.toByteArray(),
            xrsAuthGroups.toByteArray(), xrsLicenseGroups.toByteArray(), null);
    hwSession.checkInOrg(sessionInfoXML, "9000000000");
    System.out.println("Test user '"+ loginID + "' was successfully added.");
    hwSession.logOut(sessionInfoXML);

%>

