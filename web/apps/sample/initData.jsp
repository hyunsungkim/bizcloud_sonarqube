<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.hs.bf.web.beans.HWSession" %>
<%@ page import="com.hs.bf.web.beans.HWSessionFactory" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>


<%
    // String sessionInfoXML1 = AcmManager.getInstance().getAuthCredentialKey(session).getCredentialKey();
    String serverIP = ServletUtil.getParameterValue(request, "serverIP", "bizcloud.bizflow.com");
    int serverPort = ServletUtil.getParameterIntValue(request, "serverPort", 7201);
    String loginID = ServletUtil.getParameterValue(request, "loginID", "appdev");
    String password = ServletUtil.getParameterValue(request, "password", "s");
    String workspaceName = ServletUtil.getParameterValue(request, "wsName", "SampleWorkspace");
    String auth9 = ServletUtil.getParameterValue(request, "auth9", "111000001");
    System.out.println("loginID="+loginID);
    System.out.println("password="+password);
    System.out.println("workspaceName="+workspaceName);
    System.out.println("auth9="+auth9);
%>

<%@include file="addAppDev.jsp" %>

<%
    hwSession = hwSessionFactory.newInstance();
    System.out.println("Login appdev user to create sample workspaces");
    hwSessionFactory = new HWSessionFactory();
    hwSession = hwSessionFactory.newInstance();
    System.out.println("serverIP="+serverIP);
    System.out.println("serverPort="+serverPort);
    System.out.println("loginID="+loginID);
    sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, password,true);
%>
<html>
<head>
    <meta name="referrer" content="no-referrer-when-downgrade" />

    <script src="../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <script src="../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../includes/node_modules/angular/angular.min.js"></script>

    <script>

        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext.js");

        var workspaceId = "";
        var projectId = "";
        var formId = "";
        function initSession() {
            var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
            var url = "/bizflowappdev/services/bizflow/auth/loginByToken.json?tz=" + timeZone;

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    <%--"TOKEN": "<%=sessionInfoXML.replaceAll("\"", "\\\\\"")%>"--%>
                    "TOKEN": "<%=sessionInfoXML.replaceAll("\"", "\\\\\"")%>"
                },
                success: function (response, status, xhr) {
                    console.log('status', status);
                    console.log('xhr', xhr);
                    console.log(xhr.getAllResponseHeaders());
                    $.ajax({
                        url: "/bizflowappdev/services/data/create/fbs.workspace-CreateWorkspace.json",
                        type: "POST",
                        data: mars$cipher$.encipher(JSON.stringify({
                            "name": "<%= workspaceName %>",
                            "description": "Initial Sample Workspace2"
                        })),
                        success: function (response, status, xhr) {
                            console.log('Create Workspace Start');
                            var dresponse = mars$cipher$.decipher(response);
                            workspaceId = dresponse.id;
                            console.log('response', mars$cipher$.decipher(response));
                            console.log('workspaceId', workspaceId);
                            updateWorkspaceAuth('A');
                            updateWorkspaceAuth('M');
                            updateWorkspaceAuth('R');
                            updateProject();
                        },
                        error: function (response) {
                            console.error('error', response);
                        }
                    });
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateWorkspaceAuth(permission) {
            $.ajax({
                url: "/bizflowappdev/services/data/run/fbs.workspace-CreatePermission@.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "permission": permission,
                    "memberType": "D",
                    "memberName": "Root",
                    "memberId": "9000000000",
                    "id": workspaceId
                })),
                success: function (response, status, xhr) {
                    console.log('Create Workspace Auth Start');
                    console.log('response', mars$cipher$.decipher(response));
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateProject() {
            $.ajax({
                url: "/bizflowappdev/services/data/create/fbs.project-CreateProject.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "workspaceId": workspaceId,
                    "name": "SampleProject",
                    "description": "Initial Sample Project"
                })),
                success: function (response, status, xhr) {
                    console.log('Create Project Start');
                    var dresponse = mars$cipher$.decipher(response);
                    projectId = dresponse.id;
                    console.log('response', mars$cipher$.decipher(response));
                    updateProjectAuth('A');
                    updateProjectAuth('M');
                    updateProjectAuth('R');
                    updateForm();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateProjectAuth(permission) {
            $.ajax({
                url: "/bizflowappdev/services/data/run/fbs.project-CreatePermission@.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "permission": permission,
                    "memberType": "D",
                    "memberName": "Root",
                    "memberId": "9000000000",
                    "id": projectId
                })),
                success: function (response, status, xhr) {
                    console.log('Create Project Auth Start');
                    console.log('response', mars$cipher$.decipher(response));
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateForm() {
            $.ajax({
                url: "/bizflowappdev/services/data/create/fbs.form-CreateForm.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "projectId": projectId,
                    "name": "SampleForm",
                    "description": "Initial Sample Form"
                })),
                success: function (response, status, xhr) {
                    console.log('Create Form Start');
                    console.log('response', mars$cipher$.decipher(response));
                    formId = response.id;
                    updateDataModel();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateDataModel() {
            $.ajax({
                url: "/bizflowappdev/services/data/create/fbs.model-CreateProjectDataModel.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "version": "606",
                    "projectId": projectId,
                    "content": "{\"globalOption\":{\"condition\":{\"readonly\":null,\"printable\":null,\"description\":\"\"},\"componentLayout\":{\"marginTop\":\"0\",\"marginRight\":\"0\",\"marginBottom\":\"0\",\"marginLeft\":\"0\",\"paddingTop\":\"5px\",\"paddingRight\":\"5px\",\"paddingBottom\":\"5px\",\"paddingLeft\":\"5px\"},\"theme\":{\"name\":\"light\",\"baseFontSize\":\"12px\"}},\"schema\":{\"structure\":{\"_QUzD0ntqh6dsURwIUMBmAKHMabV78kK\":{},\"_SzC_dw_EtmtEZuGZqB7UJQ5_aQg8A4S\":{}},\"entity\":{\"_QUzD0ntqh6dsURwIUMBmAKHMabV78kK\":{\"name\":\"FullName\",\"description\":\"Sample Entity 2\",\"label\":\"Full Name\",\"readonly\":false,\"lookup\":null,\"transient\":false,\"type\":\"String\",\"dataLength\":100,\"validation\":{\"maxLength\":{\"use\":true,\"value\":100,\"message\":\"This fields can be entered maximum of 100 character(s).\"}}},\"_SzC_dw_EtmtEZuGZqB7UJQ5_aQg8A4S\":{\"name\":\"Age\",\"description\":\"Sample Entity 2\",\"label\":\"Age\",\"readonly\":false,\"lookup\":null,\"transient\":false,\"type\":\"Integer\",\"validation\":{\"pattern\":{\"use\":true,\"preDefined\":\"DIGITAL\",\"message\":\"This fields can be entered only integer type.\"}}}},\"group\":{}},\"relationship\":{},\"rawDataRelationship\":{},\"pvMap\":{\"structure\":[],\"procVar\":{}},\"version\":606,\"isPublished\":false}"
                })),
                success: function (response, status, xhr) {
                    console.log('Create Project Data Model Start');
                    console.log('response', mars$cipher$.decipher(response));
                    publishDataModel();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function publishDataModel() {
            $.ajax({
                url: "/bizflowappdev/services/data/run/fbs.model-PublishProjectDataModel@.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "projectId": projectId,
                    "content": {"globalOption":{"condition":{"readonly":null,"printable":null,"description":""},"componentLayout":{"marginTop":"0","marginRight":"0","marginBottom":"0","marginLeft":"0","paddingTop":"5px","paddingRight":"5px","paddingBottom":"5px","paddingLeft":"5px"},"theme":{"name":"light","baseFontSize":"12px"}},"schema":{"structure":{"_QUzD0ntqh6dsURwIUMBmAKHMabV78kK":{},"_SzC_dw_EtmtEZuGZqB7UJQ5_aQg8A4S":{}},"entity":{"_QUzD0ntqh6dsURwIUMBmAKHMabV78kK":{"name":"FullName","description":"Sample Entity 2","label":"Full Name","readonly":false,"lookup":null,"transient":false,"type":"String","dataLength":100,"validation":{"maxLength":{"use":true,"value":100,"message":"This fields can be entered maximum of 100 character(s)."}}},"_SzC_dw_EtmtEZuGZqB7UJQ5_aQg8A4S":{"name":"Age","description":"Sample Entity 2","label":"Age","readonly":false,"lookup":null,"transient":false,"type":"Integer","validation":{"pattern":{"use":true,"preDefined":"DIGITAL","message":"This fields can be entered only integer type."}}}},"group":{}},"relationship":{},"rawDataRelationship":{},"pvMap":{"structure":[],"procVar":{}},"version":606,"isPublished":false}

                })),
                success: function (response, status, xhr) {
                    console.log('Publish Project Data Model Start');
                    console.log('response', mars$cipher$.decipher(response));
                },
                error: function (response) {
                    console.log('response', mars$cipher$.decipher(response));
                    console.error('error', response);
                }
            });
        }

        function _onload() {
            initSession();
        }
    </script>
</head>
<body onload="_onload();">
</body>
</html>

