<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.hs.bf.web.beans.HWSession" %>
<%@ page import="com.hs.bf.web.beans.HWSessionFactory" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>


<%
    // String sessionInfoXML1 = AcmManager.getInstance().getAuthCredentialKey(session).getCredentialKey();
    String serverIP = ServletUtil.getParameterValue(request, "serverIP", "bizcloud.bizflow.com");
    int serverPort = ServletUtil.getParameterIntValue(request, "serverPort", 7201);
    String workspaceName = ServletUtil.getParameterValue(request, "wsName", "SampleWorkspace5");
%>

<%@include file="addAppDev.jsp" %>

<%
    loginID = "appdev";
    hwSession = hwSessionFactory.newInstance();
System.out.println("Login appdev user to create sample workspaces");
    sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, password,true);
%>
<% out.println("sessionInfoXML="+ sessionInfoXML);%>
<html>
<head>
    <script src="../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <script src="../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../includes/node_modules/angular/angular.min.js"></script>

    <script>
        mars$require$.script("../includes/node_modules-ext/angular/angular-ext.js");

        var workspaceId = "";
        var projectId = "";
        var formId = "";
        function initSession() {
            var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
            var url = "http://bizcloud.bizflow.com/bizflowappdev/services/bizflow/auth/loginByToken.json?tz=" + timeZone;

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    <%--"TOKEN": "<%=sessionInfoXML.replaceAll("\"", "\\\\\"")%>"--%>
                    "TOKEN": "<%=sessionInfoXML.replaceAll("\"", "\\\\\"")%>"
                },
                success: function (response, status, xhr) {
                    console.log('response', response);
                    console.log('status', status);
                    console.log('xhr', xhr);
                    console.log(xhr.getResponseHeader("JSESSIONID"));
                    console.log(xhr.getAllResponseHeaders());
                    $.ajax({
                        url: "http://<%=serverIP%>/bizflowappdev/services/data/create/fbs.workspace-CreateWorkspace.json",
                        type: "POST",
                        data: mars$cipher$.encipher(JSON.stringify({
                            "name": "<%= workspaceName %>",
                            "description": "Initial Sample Workspace2"
                        })),
                        success: function (response, status, xhr) {
                            console.log('response', response);
                            var dresponse = mars$cipher$.decipher(response);
                            workspaceId = dresponse.id;
                            console.log('workspaceId', workspaceId);
                            updateWorkspaceAuth('A');
                            updateWorkspaceAuth('M');
                            updateWorkspaceAuth('R');
                            updateProject();
                        },
                        error: function (response) {
                            console.error('error', response);
                        }
                    });
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function getData() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/get/fbs.project-GetProject.json",
                type: "GET",
                success: function (response, status, xhr) {
                    console.log('response', response);
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateWorkspace() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/create/fbs.workspace-CreateWorkspace.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "name": "SampleWorkspace2",
                    "description": "Initial Sample Workspace2"
                })),
                success: function (response, status, xhr) {
                    console.log('response', response);
                    workspaceId = response.id;
                    updateWorkspaceAuth('A');
                    updateWorkspaceAuth('M');
                    updateWorkspaceAuth('R');
                    // updateProject();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("workspaceId="+workspaceId);
        }

        function updateWorkspaceAuth(permission) {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/run/fbs.workspace-CreatePermission@.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "permission": permission,
                    "memberType": "D",
                    "memberName": "Root",
                    "memberId": "9000000000",
                    "id": workspaceId
                })),
                success: function (response, status, xhr) {
                    console.log('response', response);
                    console.log('response-decipher', mars$cipher$.decipher(response));
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("workspaceId="+workspaceId);
        }

        function updateProject() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/create/fbs.project-CreateProject.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "workspaceId": workspaceId,
                    "name": "SampleProject",
                    "description": "Initial Sample Project"
                })),
                success: function (response, status, xhr) {
                    console.log('response', response);
                    var dresponse = mars$cipher$.decipher(response);
                    projectId = dresponse.id;
                    updateProjectAuth('A');
                    updateProjectAuth('M');
                    updateProjectAuth('R');
                    updateForm();
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("projectId="+projectId);
        }

        function updateProjectAuth(permission) {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/run/fbs.project-CreatePermission@.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "permission": permission,
                    "memberType": "D",
                    "memberName": "Root",
                    "memberId": "9000000000",
                    "id": projectId
                })),
                success: function (response, status, xhr) {
                    console.log('response', response);
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
        }

        function updateForm() {
            $.ajax({
                url: "http://<%=serverIP%>/bizflowappdev/services/data/create/fbs.form-CreateForm.json",
                type: "POST",
                data: mars$cipher$.encipher(JSON.stringify({
                    "projectId": projectId,
                    "name": "SampleForm",
                    "description": "Initial Sample Form"
                })),
                success: function (response, status, xhr) {
                    console.log('response', response);
                    formId = response.id;
                },
                error: function (response) {
                    console.error('error', response);
                }
            });
            console.log("formId="+formId);
        }

        function _onload() {
            initSession();
        }
    </script>
</head>
<body onload="_onload();">
</body>
</html>
