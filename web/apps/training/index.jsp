<%@ page import="com.bizflow.io.core.net.util.RequestIdentifierKeeper" %>
<%@ page import="com.bizflow.io.core.util.UUIDGenerator" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.acm.util.AnonymousUtil" %>
<%@ page import="com.bizflow.io.services.core.model.Member" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.TimeZone" %>
<%
    /**
     * The ISFI of the first AppDev form to be loaded before a user logs in AppDev. Change this with the ISFI of your form
     */
    // final String isfi1 = "djI0OWExNDEwNjc0ZTQ0MzE4NjkxY2JmM2M1OTg2ZjQyfHNhMmFmNjIwNjgwYTI0YzczODkxNmI1ZTg2Y2ZjZDg4ZnxqYzVmZmMzMjdiZDAyNDgwYjk3YWViNGJlYzRjZDhkNzl8bGlnaHQ%3D";
	final String isfi1 = "djI0OWExNDEwNjc0ZTQ0MzE4NjkxY2JmM2M1OTg2ZjQyfHJhNTY2OThmMmMwMzU0YTdlODNlYjY0ZGQxZTYwNDg3OXxlNDU0MWVjMjUwMjhhNDgzNzk4ODUxMmYwNWM0Mzc2NDh8bGlnaHQ%3D";
    /**
     * The ISFI of the second AppDev form the be loaded after a user logs in AppDev. Change this with the ISFI of your form
     */
    // final String isfi2 = "djI0OWExNDEwNjc0ZTQ0MzE4NjkxY2JmM2M1OTg2ZjQyfHNhMmFmNjIwNjgwYTI0YzczODkxNmI1ZTg2Y2ZjZDg4ZnxqYzVmZmMzMjdiZDAyNDgwYjk3YWViNGJlYzRjZDhkNzl8bGlnaHQ%3D";
	final String isfi2 = "djI0OWExNDEwNjc0ZTQ0MzE4NjkxY2JmM2M1OTg2ZjQyfHJhNTY2OThmMmMwMzU0YTdlODNlYjY0ZGQxZTYwNDg3OXxlNDU0MWVjMjUwMjhhNDgzNzk4ODUxMmYwNWM0Mzc2NDh8bGlnaHQ%3D";
    /**
     * Anonymous Application Name. Change this with your application name + Anonymous, for example, "myApp Anonymous"
     */
    final String anonymousName = "NetSub Portal Anonymous";

    /**
     * The organization name for Anonymous. Change this with the organization name for anonymous users
     */
    final String anonymousOrganizationName = "Anonymous";

    /**
     * You don't need to update below codes unless you need to add new features or change existing features.
     */
    String isfi = isfi1;
    Member member = null;
    String queryString = request.getQueryString();
    queryString = null == queryString ? "" : "&" + queryString;

    RequestIdentifierKeeper.setRequestIdentifier(null);
    TimeZone timeZone = ServletUtil.getClientTimeZone(request);
    boolean iframeMode = ServletUtil.getParameterBooleanValue(request, "i", true);
    boolean validRequest = AcmManager.getInstance().isValidRequest(request) && null != AcmManager.getInstance().getUserSessionMember(session);
    if (null == request.getParameter("procid")) queryString += "&procid=-1&workseq=-1&actseq=-1";
    if (!validRequest) {
        if (null != timeZone) {
            member = AnonymousUtil.createAnonymousCredential(anonymousName, anonymousOrganizationName, timeZone, request);
            validRequest = null != member;
        } else {
%>
<html>
<head>
    <script>
        try {
            if (/Trident\/|MSIE/.test(window.navigator.userAgent) && window.document.documentMode) {
                top.opener = null;
            }
        } catch (e) {
        }
    </script>
    <script>
        var iframeMode = <%=iframeMode ? "true" : "false"%>;
        var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
        var url = '' + this.location.href;

        function _onload() {
            if (iframeMode) {
                var form = document.forms[0];
                form.tz.value = timeZone;
                form.submit();
            } else {
                url += (-1 == url.indexOf('?') ? '?' : '&') + 'tz=' + encodeURIComponent(timeZone);
                this.location.href = url;
            }
        }
    </script>
</head>
<body onload="_onload();">
<form method="GET">
    <input type="hidden" name="tz" value="">
    <%
        Enumeration<String> en = request.getParameterNames();
        while (en.hasMoreElements()) {
            String name = en.nextElement();
            String value = request.getParameter(name);
            if (null != value) {
    %>
    <input type="hidden" name="<%=Encode.forHtmlAttribute(name)%>" value="<%=Encode.forHtmlAttribute(value)%>">
    <%
            }
        }
    %>
</form>
</body>
</html>
<%
        }
    }

    if (validRequest) {
        member = null == member ? AcmManager.getInstance().getUserSessionMember(session) : member;
        if (!UUIDGenerator.ZeroId.equals(member.getId())) {
//            isfi = isfi2;
System.out.println("isfi2~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + request.getParameter("action"));
            if (null == request.getParameter("action") || !"login".equals(request.getParameter("action"))) {
              isfi = isfi2;
            }
        }

        String home = request.getRequestURI();
        if (iframeMode) {
%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<head>
    <script>
        try {
            if (/Trident\/|MSIE/.test(window.navigator.userAgent) && window.document.documentMode) {
                top.opener = null;
            }
        } catch (e) {
        }
    </script>
</head>
<body style="overflow: hidden;border: 0;padding: 0;margin: 0;">
<iframe id="frame" name="frame" style="width: 100%;height: 100%;border: 0;padding: 0;margin: 0;" frameborder="0"
        src="<%=request.getContextPath()%>/apps/fbs/app/?$T=<%=URLEncoder.encode(home, "UTF-8")%>&isfi=<%=isfi%><%=queryString%>&accessibility=y"></iframe>
</body>
</html>
<%
} else {
%>
<html>
<head>
    <script>
        if (isfi == isfi1) {
	console.log("isfi1");
        this.location = "<%=request.getContextPath()%>/apps/fbs/app/?$H=<%=URLEncoder.encode(home, "UTF-8")%>&isfi=<%=isfi%><%=queryString%>&accessibility=y"; }
        else {
           console.log("isfi", isfi == isfi2);
        }
    </script>
</head>
</html>
<%
        }
    }
%>
