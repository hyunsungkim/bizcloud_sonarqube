<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.core.model.ServiceConstant" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="com.bizflow.io.services.core.message.util.I18NMessageUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%
    AcmManager.getInstance().setReferrer(request, true);
%>
<html>
<head>
    <script>
<%
    String acmType = CoreServiceConfigUtil.getConfiguration().getProperty(CoreServiceProperties.AcmType);
    if (ServiceConstant.AcmTypeBizFlowAuthSP.equals(acmType)) {

        String errorMsg = null;
        boolean validRequest = AcmManager.getInstance().isValidRequest(request, false);

        if (validRequest) {
            if (AcmManager.getInstance().isValidAdminRequest(request)) {
%>
                this.location = "main.jsp";
<%
            } else {
                errorMsg = I18NMessageUtil.getMessage(CoreServiceConfigUtil.getServiceName(), "license.invalidLicense");
%>
                <%if(null != errorMsg) {%>
                alert("<%=Encode.forJavaScript(errorMsg)%>");
                <%}%>
                this.location = "../auth/bizauth/auth-logout.jsp?HOME=<%=Base64.encodeBase64String(request.getRequestURI().getBytes())%>";
<%
            }
        } else {
%>
            this.location = "../auth";
<%
        }
    } else {
%>
        this.location = "login/";
<%
    }
%>
    </script>
</head>
</html>