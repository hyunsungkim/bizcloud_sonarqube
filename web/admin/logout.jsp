<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.moon.Moon" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.service.AcmServiceSync" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.model.ServiceConstant" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%
    String referrer = request.getParameter("r");
    if (null == referrer) {
        String moonName = request.getParameter("m");
        if (null != moonName) {
            Moon moon = MoonManager.getInstance().get(moonName);
            if (null != moon) {
                referrer = request.getContextPath() + "/" + moon.getMoonUri();
            }
        }
        if (null == referrer) {
            referrer = AcmManager.getInstance().getReferrer(session);
            if (null == referrer) {
                referrer = request.getContextPath();
            }
        }
    }

    AcmServiceSync acmService = new AcmServiceSync();
    acmService.logOut("json", request, response);

    String acmType = CoreServiceConfigUtil.getConfiguration().getProperty(CoreServiceProperties.AcmType);
    if (ServiceConstant.AcmTypeBizFlowAuthSP.equals(acmType)) {
        request.setAttribute("HOME", referrer);
        response.sendRedirect(request.getContextPath() + "/auth/bizauth/auth-logout.jsp?HOME=" + Base64.encodeBase64String(referrer.getBytes()));
    } else {
        try {
            session.invalidate();
        } catch (IllegalStateException ignore) {
        }

        if (null != referrer) {
            response.sendRedirect(referrer);
        }
    }
%>