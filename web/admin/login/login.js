var app = angular.module('loginApp', [
    'blockUI'
    , 'mars.angular.context'
    , 'mars.angular.ajax'
]).controller('loginAppController', ['$scope', '$window', '$timeout', 'blockUI', 'marsContext', 'AjaxService', function ($scope, $window, $timeout, blockUI, marsContext, AjaxService) {
    var loginApi = new AjaxService(marsContext.contextPath + '/services/acm/login.json');
    $scope.user = {};
    $scope.loginIdReady = false;

    var queryParam = null;

    function focus() {
        $timeout(function () {
            try {
                var o = document.getElementById("pword");
                if (o) {
                    o.focus();
                }
            } catch (e) {
            }
        }, 200);
    }

    function login() {
        var timeZone = new Date().toString().match(/([A-Z]+[\+-][0-9]+)/)[1];
        var config = {
            headers: {
                "X-Client-TimeZone": timeZone
            }
        };
        if (angular.isDefined($scope.user.i) && $scope.user.i != "" && angular.isDefined($scope.user.p) && $scope.user.p != "") {
            loginApi.post({
                success: function (o) {
                    if (angular.isDefined(o.data.credentialKey)) {
                        $window.location = '../main.jsp';
                    } else {
                        angularExt.getBootboxObject().alert("User name and/or password is not correct.", focus);
                    }
                },
                error: function (o) {
                    var faultCode = o.error.faultCode ? parseInt(o.error.faultCode) : 0;
                    if (faultCode == mars$faultcode$.AccountAlreadyUsed) {
                        angularExt.getBootboxObject().confirm({
                            message: o.error.data,
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-warning'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-default'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    queryParam = "force=true";
                                    $timeout(function () {
                                        $scope.login();
                                    }, 100);
                                }
                            }
                        });
                    } else {
                        o.alertExceptionOnly(focus);
                    }
                }
            }, $scope.user, queryParam, config);
        }
    }

    $scope.login = function () {
        var user = $scope.user;
        if (user.i && user.p) {
            login();
        } else if (user.i) {
            $scope.loginIdReady = true;
            $timeout(function () {
                document.getElementById("pword").focus();
            }, 100)
        }
    };

    $scope.enterUsername = function () {
        $scope.loginIdReady = false;
        $scope.user.p = undefined;
    }
}]);

function onLoadPage() {
    try {
        window.sessionStorage.clear();
        setTimeout(function () {
            var o = document.getElementById("loginId");
            if (o) {
                o.focus();
            }
        }, 100);
    } catch (e) {
    }
}
