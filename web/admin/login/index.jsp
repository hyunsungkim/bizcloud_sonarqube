<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.core.net.util.RequestIdentifierKeeper" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.tenant.TenantManager" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%
    RequestIdentifierKeeper.setRequestIdentifier(null);
    String errorMsg = null;
    AcmManager.getInstance().setReferrer(request, true);
    try {
        HttpSession newSession = AcmManager.getInstance().initializeRequestSession(request);
        TenantManager.getInstance().setUserTenantIfNotSet(null, request);
        AcmManager.getInstance().setUserAppName(newSession, CoreServiceConfigUtil.getServiceName());
    } catch (Throwable t) {
        errorMsg = ExceptionUtil.getOriginalThrowable(t).getMessage();
    }
%>
<!DOCTYPE html>
<html ng-app="loginApp">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Welcome</title>
    <link rel="shortcut icon" href="../../favicon.ico" type="image/x-icon">
    <script src="../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>

    <script src="../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../includes/node_modules/bootbox/bootbox.js"></script>
    <script src="../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>

    <script>
        mars$require$.link("../../includes/node_modules-ext/bootstrap4/css//bootstrap-ext.css");
        mars$require$.link("../../includes/node_modules-ext/bizflow/css/theme/dark/bootstrap/bootstrap.css");
        mars$require$.link("../../includes/node_modules-ext/bizflow/css/login.css");
        mars$require$.link("../../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../includes/node_modules-ext/mars/mars.js");
        mars$require$.script("../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("login.js");
    </script>

    <style>
        .bootbox .modal-content {
            background-color: #343a40;
            border-color: #6c757d !important;
            color: #f8f9fa;
        }
    </style>

    <script>
        function showErrorMsg() {
            <%if(null != errorMsg) {%>
            setTimeout(function () {
                alert("<%=errorMsg.replaceAll("\"", "\\\"")%>");
            }, 100);
            <%}%>
        }
    </script>
</head>

<body ng-controller="loginAppController" onload="onLoadPage();showErrorMsg();" onresize="onLoadPage()">
<div class="bf-login_wrap" style="z-index: 100;">
    <div class="login-box">
        <div class="logo"><img src="../../includes/node_modules-ext/bizflow/img/logo_white.png" width="217" height="37" alt="BizFlow"></div>
        <div class="welcome">Welcome</div>
        <form method="POST">
            <div ng-if="!loginIdReady">
                <input ng-model="user.i" id="loginId" tabindex="1" type="text" name="loginId" maxlength="100" placeholder='User Name' value="" autofocus>
            </div>
            <div ng-if="!loginIdReady">
                <button ng-click="login()">NEXT</button>
            </div>
            <div ng-if="loginIdReady">
                <input ng-model="user.p" id="pword" tabindex="2" type="password" name="pword" size="18" maxlength="48" placeholder='Enter password for {{user.i}}'>
            </div>
            <div ng-if="loginIdReady">
                <button type="submit" class="btn btn-primary btn-sm" ng-click="login()">LOG IN</button>
            </div>
            <div class="line"></div>
            <div ng-if="!loginIdReady" class="footer">
                Your password will be asked in the next step
                <div ng-include="'../../copyright-text.html'"></div>
            </div>
            <div ng-if="loginIdReady" class="footer">
                <span ng-click="enterUsername();">Click here to change user name <i>{{user.i}}</i></span>
                <div ng-include="'../../copyright-text.html'"></div>
            </div>
        </form>
    </div>
</div>
</body>
</html>