<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String userLang = AcmManager.getInstance().getUserLanguage(request);
%>

<html>
<head>
    <link rel="stylesheet" href="../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <script src="../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
        function page_onload() {
            try {
                if (parent) {
                    parent.accessDenied();
                }
            } catch (e) {
            }
        }
    </script>
</head>
<body onload="page_onload();">
<div class="container" style="height: 100%;">
    <%if ("ko".equals(userLang)) {%>
    <h1>접근 금지: <%=ServletUtil.getClientIPAddress(request)%>
    </h1>
    아래의 이유등으로 접근이 금지되었습니다.
    <br><br>
    <ul>
        <li>로그인 없이 접근하였습니다.</li>
        <li>섹션이 무효화 되었습니다.</li>
        <li>이 페이지를 접근할 권한이 없습니다.</li>
    </ul>
    <br> <br>
    다시 <a href="<%=request.getContextPath()%>/admin/login">로그인</a> 하십시오.
    <br> <br>
    <%} else {%>
    <h1>Access Denied: <%=ServletUtil.getClientIPAddress(request)%>
    </h1>
    Because one or several reason of the following reasons
    <br><br>
    <ul>
        <li>Credential is not initialized.</li>
        <li>Your session was expired.</li>
        <li>You do not have a permission to access.</li>
    </ul>
    <br> <br>
    Please <a href="<%=request.getContextPath()%>/admin/login">log in</a> first.
    <br> <br>
    <%}%>
    <%@include file="../copyright.html" %>
</div>
</body>
</html>
