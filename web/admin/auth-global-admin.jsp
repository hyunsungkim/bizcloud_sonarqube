<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%
    if (!AcmManager.getInstance().isValidGlobalAdminRequest(request)) {
%>
<script>
    this.location = "<%=request.getContextPath()%>/admin/access-denied.jsp";
</script>
<%
        return;
    }
%>