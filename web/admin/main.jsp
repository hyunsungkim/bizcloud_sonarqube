<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.AuthCredentialKey" %>
<%@ page import="com.bizflow.io.services.core.model.Tenant" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="auth-admin.jsp" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
    String contextPath = request.getContextPath();
    boolean noBrand = "n".equalsIgnoreCase(request.getParameter("b"));

    AuthCredentialKey authCredentialKey = AcmManager.getInstance().getAuthCredentialKey(session);
    String loginId = null != authCredentialKey ? authCredentialKey.getLoginId() : "";

    String bgColor = "dark";
    String textColor = "white";
    String listBgColor = "light";
    String theme = ServletUtil.getParameterValue(request, "theme", "dark");
    if ("light".equalsIgnoreCase(theme)) {
        bgColor = "light";
        textColor = "dark";
        listBgColor = "light";
    }

    final String _theme = Encode.forHtmlAttribute(theme);
    final String fbsVersion = MoonManager.getInstance().get("fbs").getVersion();
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <title>BizFlow Integration Orchestrator</title>
    <script src="../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../includes/node_modules/fontawesome-free/css/all.min.css"/>

    <script src="../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.script("../includes/node_modules-ext/mars/mars-util.js");
    </script>
    <style>
        .card-header h5 {
            margin: 0;
        }

        a.list-group-item {
            color: #343a40;
        }
    </style>
</head>

<body onload="mars$util$.resizeFrameWindowHeight()" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="page-header text-center" style="padding: 15px 0 10px 0;">
    <%if (appDevMode) {%>
    <h1>BizFlow AppDev Management</h1>
    <%} else {%>
    <h1>BizFlow Integration Orchestrator</h1>
    <%}%>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg">
            <%if (!appDevMode) {%>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#apiMgmt" aria-expanded="false" aria-controls="apiMgmt"><h5>API Management</h5></div>
                <div class="card-block collapse show" id="apiMgmt">
                    <div class="list-group list-group-flush">
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/db/query-manager.jsp?theme=<%=_theme%>"><i class="fa fa-list-alt fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Query Management</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/db/sql-browser.jsp?theme=<%=_theme%>"><i class="fa fa-table fa-2x fa-fw" aria-hidden="true"></i>&nbsp; SQL Browser</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/api/api-documentation.jsp?theme=<%=_theme%>"><i class="fa fa-edit fa-2x fa-fw" aria-hidden="true"></i>&nbsp; API Documentation</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/tools/api/api-doc-viewer.jsp?theme=<%=_theme%>"><i class="fa fa-book fa-2x fa-fw" aria-hidden="true"></i>&nbsp; API Document</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/api/api-access.jsp?theme=<%=_theme%>&t=true"><i class="fab fa-keycdn fa-2x fa-fw" aria-hidden="true"></i>&nbsp; API Access Control</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/api/api-access-monitor.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-chart-bar fa-2x fa-fw" aria-hidden="true"></i>&nbsp; API Access Monitor</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/api/api-test.jsp?theme=<%=_theme%>"><i class="fa fa-rocket fa-2x fa-fw" aria-hidden="true"></i>&nbsp; API Test</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/api/test-hello.jsp?theme=<%=_theme%>"><i class="fa fa-handshake fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Test Handshaking</a>
                    </div>
                </div>
            </div>
            <%}%>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#sysMgmt" aria-expanded="false" aria-controls="sysMgmt"><h5>System Management</h5></div>
                <div class="card-block collapse show" id="sysMgmt">
                    <div class="list-group list-group-flush">
                        <%if (!appDevMode) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-mm" href="<%=contextPath%>/admin/tools/application/application-manager.jsp?theme=<%=_theme%>"><i class="fa fa-moon fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Application Management</a>
                        <%}%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-mp" href="<%=contextPath%>/admin/tools/mgmt/configuration-manager.jsp?theme=<%=_theme%>"><i class="fa fa-cog fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Configuration Management</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-rc" href="<%=contextPath%>/admin/tools/mgmt/reload-config-menu.jsp?theme=<%=_theme%>"><i class="fa fa-sync fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Reload Configuration</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/log/view-logs.jsp?theme=<%=_theme%>"><i class="fa fa-eye fa-2x fa-fw" aria-hidden="true"></i>&nbsp; View Logs</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-dl" href="<%=contextPath%>/admin/tools/log/zip-logs.jsp?theme=<%=_theme%>"><i class="fa fa-download fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Download Logs</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-cpm" href="<%=contextPath%>/admin/tools/db/db-connection-pool-monitor.jsp?theme=<%=_theme%>"><i class="fas fa-database fa-2x fa-fw" aria-hidden="true"></i>&nbsp; DB Connection Pool Monitor</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-syi" href="<%=contextPath%>/admin/tools/system/system-info.jsp?theme=<%=_theme%>"><i class="fas fa-info fa-2x fa-fw" aria-hidden="true"></i>&nbsp; System Information</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-sei" href="<%=contextPath%>/admin/tools/util/show-environment.jsp?theme=<%=_theme%>"><i class="fa fa-info-circle fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Environment Information</a>
                        <%--                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-qjs" href="<%=contextPath%>/admin/tools/job/quartz-manager.jsp?theme=<%=_theme%>"><i class="fa fa-tasks fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Job Scheduler State</a>--%>
                    </div>
                </div>
            </div>

            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#tenantMgmt" aria-expanded="false" aria-controls="tenantMgmt"><h5>Operation Management</h5></div>
                <div class="card-block collapse show" id="tenantMgmt">
                    <div class="list-group list-group-flush">
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-tm" href="<%=contextPath%>/admin/tools/tenant/tenant-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-building fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Tenant Management</a>
                        <%if (!appDevMode) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/server/server-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-server fa-2x fa-fw" aria-hidden="true"></i>&nbsp; System Server Management</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/configuration/configuration-manager.jsp?theme=<%=_theme%>&t=true"><i class="fab fa-contao fa-2x fa-fw" aria-hidden="true"></i>&nbsp; System Configuration Management</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/distribution/distribution-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-truck-pickup fa-2x fa-fw" aria-hidden="true"></i>&nbsp; System Distribution Management</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/websocket/websocket-manager.jsp?theme=<%=_theme%>&t=true"><i class="fab fa-rocketchat fa-2x fa-fw" aria-hidden="true"></i>&nbsp; System WebSocket Management</a>
                        <%}%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-wc" href="<%=contextPath%>/admin/tools/websocket/notification-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-bell fa-2x fa-fw" aria-hidden="true"></i>&nbsp; System Notification Management</a>
                    </div>
                </div>
            </div>

            <%if (!appDevMode) {%>
        </div>

        <div class="col-lg">
            <%}%>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#perform" aria-expanded="false" aria-controls="perform"><h5>Performance</h5></div>
                <div class="card-block collapse show" id="perform">
                    <div class="list-group list-group-flush">
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-rp" href="<%=contextPath%>/admin/tools/performance/performance-response.jsp?theme=<%=_theme%>"><i class="fa fa-clock fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Realtime Response Performance</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-fp" href="<%=contextPath%>/admin/tools/performance/performance-view.jsp?theme=<%=_theme%>&type=Basic&prefix=root-performance"><i class="fab fa-superpowers fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Basic Service
                            Performance</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-dp" href="<%=contextPath%>/admin/tools/performance/performance-view.jsp?theme=<%=_theme%>"><i class="fa fa-database fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Data Service Performance</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-fp" href="<%=contextPath%>/admin/tools/performance/performance-view.jsp?theme=<%=_theme%>&type=File&prefix=file-performance"><i class="fa fa-file fa-2x fa-fw" aria-hidden="true"></i>&nbsp; File Service Performance</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-fmp" href="<%=contextPath%>/admin/tools/performance/performance-view.jsp?theme=<%=_theme%>&type=Form&prefix=form-performance"><i class="fab fa-wpforms fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Form Service Performance</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-bp" href="<%=contextPath%>/admin/tools/performance/performance-view.jsp?theme=<%=_theme%>&type=BizFlow&prefix=bizflow-performance&logKey=Operator"><i class="fa fa-server fa-2x fa-fw" aria-hidden="true"></i>&nbsp;
                            BizFlow Service Performance</a>
                        <%if (!appDevMode) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-mp" href="<%=contextPath%>/admin/tools/performance/performance-view.jsp?theme=<%=_theme%>&type=Message&prefix=msg-performance"><i class="fab fa-telegram fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Message Service
                            Performance</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-wp" href="<%=contextPath%>/admin/tools/performance/performance-ws.jsp?theme=<%=_theme%>"><i class="fa fa-cloud fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Web Service Performance</a>
                        <%}%>
                    </div>
                </div>
            </div>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#apps" aria-expanded="false" aria-controls="apps"><h5>Application</h5></div>
                <div class="card-block collapse show" id="apps">
                    <div class="list-group list-group-flush">
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/app/index.html")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-ad" href="<%=contextPath%>/apps/fbs/app/?v=<%=fbsVersion%>"><i class="fas fa-layer-group fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Studio</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-am" href="<%=contextPath%>/apps/fbs/app/?v=<%=fbsVersion%>&application_id=_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U&form_id=_tH1AJ4hLqGsOL0RUrpaN7UzvsXiTy5U&theme=dark"><i class="fa fa-table fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Modeler</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/i18n/i18n-message-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-i18n" href="<%=contextPath%>/apps/fbs/tools/i18n/i18n-message-manager.jsp?theme=<%=_theme%>&t=true"><i class="fa fa-comment-dots fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev I18N Message Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/lookup/lookup-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/lookup/lookup-manager.jsp?theme=<%=_theme%>&t=true"><i class="fa fa-table fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Lookup Data Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/integration/api-integration-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/integration/api-integration-manager.jsp?theme=<%=_theme%>&t=true"><i class="fab fa-asymmetrik fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev API Integration Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/file/file-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/file/file-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-file fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev File Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/file/attachment-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/file/attachment-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-paperclip fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Attachment Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/context/context-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/context/context-manager.jsp?theme=<%=_theme%>&t=true"><i class="fab fa-contao fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Context Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/migration/migration-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/migration/migration-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-plane-departure fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Migration Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/form/form-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/form/form-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-newspaper fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Form Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/authority/authority-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/authority/authority-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-users-cog fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Authority Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/configuration/configuration-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/configuration/configuration-manager.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-tools fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Configuration Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/component/component-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/component/component-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-object-group fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Component Management</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/viewer/adx-viewer.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/apps/fbs/tools/viewer/adx-viewer.jsp?theme=<%=_theme%>&t=true"><i class="fas fa-microscope fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev ADX Viewer</a>
                        <%}%>
                        <% if (CoreServiceConfigUtil.isExistFile("/apps/fbs/tools/chat/chat-manager.jsp")) {%>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-cm" href="<%=contextPath%>/apps/fbs/tools/chat/chat-manager.jsp?theme=<%=_theme%>&t=true"><i class="far fa-comment-dots fa-2x fa-fw" aria-hidden="true"></i>&nbsp; AppDev Chat</a>
                        <%}%>
                    </div>
                </div>
            </div>
            <%if (!appDevMode) {%>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#utils" aria-expanded="false" aria-controls="utils"><h5>Utility</h5></div>
                <div class="card-block collapse" id="utils">
                    <div class="list-group list-group-flush">
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-et" href="<%=contextPath%>/admin/tools/util/encrypt.jsp?theme=<%=_theme%>"><i class="fa fa-key fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Encrypt Tool</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/import/excel/select-file.jsp?theme=<%=_theme%>"><i class="fa fa-file-excel fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Import Excel File</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/import/access/select-file.jsp?theme=<%=_theme%>"><i class="fa fa-file-archive fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Import/View Access File</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="_blank" href="<%=contextPath%>/admin/tools/util/html-editor.jsp?theme=<%=_theme%>"><i class="fas fa-pencil-alt fa-2x fa-fw" aria-hidden="true"></i>&nbsp; HTML Editor</a>
                        <a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-sm1" href="<%=contextPath%>/admin/tools/msg/send-mail.jsp?theme=<%=_theme%>"><i class="fa fa-envelope fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Send Mail</a>
                        <%--<a class="list-group-item list-group-item-action list-group-item-<%=listBgColor%>" target="t-sm2" href="<%=contextPath%>/admin/tools/msg/send-mail2.jsp?theme=<%=_theme%>"><i class="fa fa-envelope fa-2x fa-fw" aria-hidden="true"></i>&nbsp; Send Mail 2</a>--%>
                    </div>
                </div>
            </div>
            <%}%>
        </div>

        <div class="col-lg-2"></div>
    </div>

    <br>

    <div class="row justify-content-md-center">
        <!--
        <form method="POST" name="form1">
            <div class="input-group">
                <select name="theme" onchange="document.form1.submit();" class="form-control form-control-sm btn-outline-secondary">
                    <option value="dark" <%="dark".equalsIgnoreCase(theme) ? "selected" : ""%>>Dark Theme</option>
                    <option value="light" <%="light".equalsIgnoreCase(theme) ? "selected" : ""%>>Light Theme</option>
                </select>
            </div>
        </form>
        -->
        <div style="padding-left: 10px;">
            <%
                Tenant tenant = AcmManager.getInstance().getUserSessionTenant(session);
                if (null != tenant) {
            %>
            <a class="btn btn-sm btn-outline-info" href="<%=contextPath%>/admin/logout.jsp?r=<%=contextPath%>/welcome/<%=tenant.getIdentifier()%>/admin">
                logout <%=loginId%>@<%=tenant.getName()%>(<%=tenant.getIdentifier()%>)
            </a>
            <%} else {%>
            <a class="btn btn-sm btn-outline-info" href="<%=contextPath%>/admin/logout.jsp?r=<%=contextPath%>/admin">
                logout <%=loginId%>
            </a>
            <%}%>
        </div>
    </div>
</div>

<br>
<br>
<%if (!noBrand) { %>
<%@include file="../copyright.html" %>
<%}%>

</body>
</html>