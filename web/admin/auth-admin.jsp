<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@ page import="com.bizflow.io.services.core.model.CoreServiceProperties" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%
    boolean appDevMode = (CoreServiceConfigUtil.isExistFile("/apps/fbs/app"));
    if (appDevMode) {
        appDevMode = !CoreServiceProperties.SystemModeBIO.equalsIgnoreCase(CoreServiceConfigUtil.getProperty(CoreServiceProperties.SystemMode));
    }

    if (AcmManager.getInstance().isValidAdminRequest(request)) {
        if (appDevMode) {
            String path = request.getServletPath();
            if (path.startsWith("/admin/tools/api/")
                    || path.startsWith("/admin/tools/application/")
                    || path.startsWith("/admin/tools/configuration/")
                    || path.startsWith("/admin/tools/distribution/")
                    || path.startsWith("/admin/tools/db/")
                    || path.startsWith("/admin/tools/ex/")
                    || path.startsWith("/admin/tools/import/")
                    || path.startsWith("/admin/tools/job/")
                    || path.startsWith("/admin/tools/mq/")
                    || path.startsWith("/admin/tools/msg/")
                    || path.startsWith("/admin/tools/websocket/websocket")
                    || (path.startsWith("/admin/tools/util/") && !path.startsWith("/admin/tools/util/show-environment.jsp"))
            ) {
                out.print("<html><head><script>this.location=\"");
                out.print(request.getContextPath());
                out.print("/access-denied.jsp\";</script></head><body></body></html>");
                return;
            }
        }
    } else {
        out.print("<html><head><script>this.location=\"");
        out.print(request.getContextPath());
        out.print("/access-denied.jsp\";</script></head><body></body></html>");
        return;
    }
%>