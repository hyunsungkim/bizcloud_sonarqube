var app = angular.module('memberApp', [
    'blockUI'
    , 'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.context'
    , 'mars.angular.mars'
    , 'mars.angular.filter'
    , 'mars.angular.ajax.service'
    , 'ui.mask'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/-LookUp/gi) || config.url.match(/-Search/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
});

app.controller('memberAppController', ['$scope', '$uibModal', '$filter', '$timeout', 'blockUI', '$mars', 'marsContext', 'marsService', 'marsGageService', 'marsMemberService',
    function ($scope, $uibModal, $filter, $timeout, blockUI, $mars, marsContext, marsService, marsGageService, marsMemberService) {
        var parameter = new UrlParameterParser(location.href);

        // var mode = parameter.getParameterValue("m");
        $scope.gageId = parameter.getParameterValue("gid");
        $scope.globalDistrict = marsContext.globalDistrict;
        $scope.currentPage = 1;
        $scope.pageSize = 15;
        $scope.orderColumn = 'name';
        $scope.orderAsc = true;

        $scope.data = {};

        function getGageLicense() {
            if ($scope.gage) {
                marsGageService.getGageLicense({
                    success: function (o) {
                        $scope.gage.license = o.data;
                    },
                    error: function (o) {
                        o.alertException();
                    }
                }, $scope.gage.district, $scope.gage.name);
            }
        }

        function getGage() {
            marsGageService.getGage({
                success: function (o) {
                    if (o.isValidData(o)) {
                        $scope.gage = o.data[0];
                        getGageLicense();
                    }
                },
                error: function (o) {
                    o.alertException();
                }
            }, {id: $scope.gageId});
        }

        if (null != $scope.gageId) {
            getGage();
        }

        function getMoonList() {
            marsService.getMoons({
                success: function (o) {
                    if (o.isValidData(o)) {
                        $scope.moons = o.data;
                        $scope.moonMap = new Map();
                        for (var i = 0; i < $scope.moons.length; i++) {
                            var moon = $scope.moons[i];
                            $scope.moonMap.set(moon.name, moon);
                        }
                    }
                },
                error: function (o) {
                    o.alertException();
                }
            });
        }

        var __admin = null;
        $scope.isAdmin = function () {
            if (null == __admin) {
                __admin = false;
                if ($scope.me) {
                    __admin = angularExt.equalsIgnoreCase($scope.me.sysRole, 'admin');
                }
            }

            return __admin;
        };

        var __globalAdmin = null;
        $scope.isGlobalAdmin = function () {
            if (null == __globalAdmin) {
                __globalAdmin = false;
                if ($scope.isAdmin()) {
                    __globalAdmin = null == $scope.me.district || angularExt.equalsIgnoreCase($scope.me.district, $scope.globalDistrict);
                }
            }

            return __globalAdmin;
        };

        marsService.whoAmI({
            success: function (o) {
                $scope.me = o.data;
                if (angularExt.isInvalidObject($scope.gageId)) {
                    if (!$mars.isGlobalAdmin($scope.me)) {
                        $scope.gageId = $scope.me.gageId;
                        if ($scope.gageId) {
                            getGage();
                        }
                    }
                }
                getMemberList();
                if ($mars.isGlobalAdmin($scope.me)) {
                    getMoonList();
                }
            },
            error: function (o) {
                o.alertException();
            }
        });

        function updateScreen() {
            $timeout(function () {
                mars$util$.resizeFrameWindowHeight(mars$util$.getClientSize().height);
            }, 500);
        }

        function callGetMemberListApi(formData) {
            $scope.currentFormData = formData;
            marsMemberService.getMemberList({
                success: function (o) {
                    if (o.isValidData(o.data)) {
                        $scope.memberList = o.data;
                        updateScreen();
                    } else {
                        $scope.memberList = {};
                    }
                },
                error: function (o) {
                    o.alertException();
                }
            }, formData);
        }

        function getMemberList(page, pageSize, filter) {
            var formData = {
                gageId: 0 != $scope.gageId ? $scope.gageId : null,
                pageNo: page || $scope.currentPage,
                pageSize: pageSize || $scope.pageSize,
                ORDER_BY: $scope.orderColumn + ' ' + ($scope.orderAsc ? 'ASC' : 'DESC')
            };

            if (filter) {
                angular.extend(formData, filter);
            }

            if (!$mars.isGlobalAdmin($scope.me)) {
                formData.district = $scope.me.district;
            }

            $scope.pageSize = formData.pageSize;

            callGetMemberListApi(formData);
        }

        $scope.pageChanged = function () {
            getMemberList($scope.currentPage);
        };

        $scope.searchUsersByName = function () {
            var filter = {
                LIKE_name_LIKE: $scope.searchNameValue
            };

            getMemberList(1, $scope.pageSize, filter);
        };

        $scope.sort = function (column) {
            $scope.orderAsc = !$scope.orderAsc;
            $scope.orderColumn = column;
            getMemberList($scope.currentPage);
        };

        $scope.refresh = function () {
            callGetMemberListApi($scope.currentFormData);
        };

        function createMember(member) {
            marsMemberService.createMember({
                success: function (o) {
                    getGageLicense();
                    $scope.refresh();
                },
                error: function (o) {
                    o.alertException();
                }
            }, member);
        }

        function checkMember(member, callback) {
            if (member.loginId) {
                var param = {
                    loginId: member.loginId
                };
                marsMemberService.getMember({
                    success: function (o) {
                        callback(o.data);
                    },
                    error: function (o) {
                        o.alertException();
                    }
                }, param);
            }
        }

        function searchGageParams(string) {
            return {query: string};
        }

        function selectGage(selectedData, member) {
            if (angular.isDefined(selectedData)) {
                member.gageId = selectedData.originalObject.id;
                member.gageName = selectedData.originalObject.name;
            } else {
                member.gageId = null;
                member.gageName = null;
            }
        }

        function checkEnoughLicense(type) {
            if (!$mars.isGlobalAdmin($scope.me)) {
                if ("member" == type) {
                    return mars$mml$.isEnoughUserLicense($scope.gage.license);
                }
            }

            return true;
        }

        function getMemberRoles(scope, district) {
            scope.memberRoles = [];
            marsMemberService.getMemberRoles({
                success: function (o) {
                    if (o.isValidData(o)) {
                        scope.memberRoles = o.data;
                    }

                    if (scope.memberRoles.length == 0) {
                        scope.memberRoles = [
                            {
                                id: 0,
                                role: 'None'
                            }
                        ]
                    }
                },
                error: function (o) {
                    o.alertException();
                }
            }, {
                district: district
            });
        }

        function setManger(selectedData, member) {
            if (angular.isDefined(selectedData)) {
                member.managerId = selectedData.originalObject.value;
                member.managerName = selectedData.originalObject.text;
            } else {
                member.managerId = undefined;
                member.managerName = undefined;
            }
        }

        $scope.addMember = function () {
            if (!checkEnoughLicense("member")) {
                if ($scope.gage.license.maxUserCount) {
                    angularExt.getBootboxObject().alert("Your license can create only " + $scope.gage.license.maxUserCount + " users including admin accounts");
                } else {
                    angularExt.getBootboxObject().alert("You do not have a valid license. Please email to <a href='mailto:sales.bf.rhythmos@gmail.com'>sales.bf.rhythmos@gmail.com</a> to purchase a license.");
                }
                return;
            }
            var scope = $scope;
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/admin/org/member/member-form.html'),
                size: 'full',
                controller: ['$scope',
                    function ($scope) {
                        $scope.gage = scope.gage;
                        $scope.newMode = true;
                        $scope.moons = scope.moons;
                        $scope.title = "Add Member" + (scope.gage ? " to " + scope.gage.name : "");
                        $scope.member = {
                            gageId: scope.gageId,
                            gageName: scope.gage ? scope.gage.name : null,
                            role: 'clerk',
                            sysRole: 'user',
                            active: true,
                            district: scope.me.district
                        };
                        $scope.setManager = function (selectedData, member) {
                            setManger(selectedData, member);
                        };
                        $scope.getMemberRoles = function () {
                            getMemberRoles($scope, $scope.member.district);
                        };
                        $scope.isValidMember = function () {
                            return angularExt.isValidObject($scope.member.gageId) && angularExt.isValidObject($scope.member.gageName);
                        };
                        $scope.searchGageParams = function (string) {
                            return searchGageParams(string);
                        };
                        $scope.selectGage = function (selectedData, member) {
                            selectGage(selectedData, member);
                        };
                        $scope.checkLoginId = function () {
                            checkMember($scope.member, function (data) {
                                if (data && data.length > 0) {
                                    $scope.loginIdExist = true;
                                } else {
                                    $scope.loginIdExist = false;
                                }
                            });
                        };
                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };
                        $scope.ok = function () {
                            $scope.$close($scope.member);
                        };

                        $scope.getMemberRoles();
                    }
                ]
            }).result.then(function (member) {
                createMember(member);
            }, function () {
            });
        };

        $scope.editMember = function (member) {
            var scope = $scope;
            var _member = angular.copy(member);
            if (null == _member.district || _member.district == "") {
                _member.district = $scope.globalDistrict;
            }
            _member.password = '';
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/admin/org/member/member-form.html'),
                size: 'full',
                controller: ['$scope',
                    function ($scope) {
                        $scope.moons = scope.moons;
                        $scope.gage = scope.gage;
                        $scope.title = "Edit Member";
                        $scope.member = _member;

                        $scope.checkLoginId = function () {
                            $scope.loginIdExist = false;
                            if ($scope.member.loginId != member.loginId) {
                                checkMember($scope.member, function (data) {
                                    if (data && data.length > 0) {
                                        $scope.loginIdExist = true;
                                    } else {
                                        $scope.loginIdExist = false;
                                    }
                                });
                            }
                        };
                        $scope.getMemberRoles = function () {
                            getMemberRoles($scope, $scope.member.district);
                        };
                        $scope.isValidMember = function () {
                            return angularExt.isValidObject($scope.member.gageId) && angularExt.isValidObject($scope.member.gageName);
                        };
                        $scope.searchGageParams = function (string) {
                            return searchGageParams(string);
                        };
                        $scope.selectGage = function (selectedData, member) {
                            selectGage(selectedData, member);
                        };
                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };
                        $scope.ok = function () {
                            $scope.$close($scope.member);
                        };

                        $scope.getMemberRoles();
                    }
                ]
            }).result.then(function (_member) {
                updateMember(_member);
            }, function () {
            });

        };

        function deleteMember(member) {
            marsMemberService.deleteMember({
                success: function (o) {
                    getGageLicense();
                    getMemberList();
                },
                error: function (o) {
                    o.alertException();
                }
            }, member);
        }

        function updateMember(member) {
            marsMemberService.updateMember({
                success: function (o) {
                    $scope.refresh();
                },
                error: function (o) {
                    o.alertException();
                }
            }, member);
        }

        $scope.deleteMember = function (member) {
            angularExt.getBootboxObject().confirm("Do you want to delete " + member.name + "?", function (result) {
                if (result) {
                    deleteMember(member);
                }
            });
        };

        $scope.showOrgChart = function () {
            var url = '../chart/org-chart.jsp';
            angularExt.openModalWindow(url, 'bootbox-dialog-full-frame');
        };

        //member-select.html
        $scope.selectMember = function (member) {
            if (-1 == $scope.selectedMembers.indexOf(member)) {
                $scope.selectedMembers.push(member);
                member._selected = true;
            } else {
                $scope.removeSelectedMember(member);
            }
        };

        $scope.removeSelectedMember = function (member) {
            var idx = $scope.selectedMembers.indexOf(member);
            if (-1 != idx) {
                member._selected = undefined;
                $scope.selectedMembers.splice(idx, 1);
            }
        };

        $scope.isSelectedMember = function (member) {
            for (var i = 0; i < $scope.selectedMembers.length; i++) {
                var m = $scope.selectedMembers[i];
                if (m.id == member.id) {
                    member._selected = true;
                    $scope.selectedMembers[i] = member;
                    break;
                }
            }
        };

        $scope.selectCancel = function () {
            $scope.$dismiss();
        };

        $scope.selectOk = function () {
            $scope.$close($scope.selectedMembers);
        };
    }]);
