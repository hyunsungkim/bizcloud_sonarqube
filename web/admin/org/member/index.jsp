<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.acm.AcmManager" %>
<%@include file="../../auth-admin.jsp" %>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<!DOCTYPE html>
<html ng-app="memberApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Management</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../includes/node_modules/angular-ui-mask/dist/mask.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/angucomplete-alt/angucomplete-alt-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-service.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");

        mars$require$.script("member.js");
    </script>
</head>

<body ng-controller="memberAppController" ng-cloak="true">

<div class="page-header text-center no-margin">
    <h1 class="no-margin">{{gage.name || ''}} User Management</h1>
    <small>- <%=AcmManager.getInstance().getSessionMember(session).getName()%> -</small>
</div>

<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-no-padding">
            <h3 class="panel-title">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            &nbsp;<span class="glyphicon glyphicon-user"></span>&nbsp;Users
                        </td>
                        <td style="padding-right: 50px;">&nbsp;</td>
                        <td>
                            <form ng-submit="searchUsersByName();">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search name for..." ng-model="data.searchNameValue" ng-click="$event.stopPropagation();">
                                    <span class="input-group-btn"><button class="btn btn-default" type="button" ng-click="searchUsersByName(); $event.stopPropagation();"><span class="glyphicon glyphicon-search"></span></button></span>
                                </div>
                            </form>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <button type="button" class="btn btn-default navbar-btn" ng-click="addMember();$event.stopPropagation()">Create User</button>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <button type="button" class="btn btn-default navbar-btn" ng-click="refresh();$event.stopPropagation()">Refresh</button>
                            <%--<a type="button" target="orgChart" class="btn btn-default navbar-btn" href="../chart/org-chart.jsp">Org Chart</a>--%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </h3>
        </div>
        <div id="Member" class="collapse in table-responsive">
            <table class="table table-striped table-condensed" ng-table="usersTable" show-filter="true">
                <tr style="font-weight: bold">
                    <th ng-click="sort('name')"><a>Name</a></th>
                    <th ng-click="sort('gageName')"><a>Gage Name</a></th>
                    <th ng-click="sort('loginId')"><a>Login ID</a></th>
                    <th ng-click="sort('email')"><a>EMail</a></th>
                    <th ng-click="sort('phone')"><a>Phone</a></th>
                    <th ng-click="sort('creationDTime')"><a>Creation Date</a></th>
                    <th ng-click="sort('Manager')"><a>Manager</a></th>
                    <th ng-click="sort('role')"><a>Role</a></th>
                    <th ng-click="sort('sysRole')"><a>System Role</a></th>
                    <th ng-click="sort('active')"><a>Active</a></th>
                    <th ng-click="sort('lastAccessDTime')"><a>Last Access</a></th>
                    <th ng-click="sort('ipAddress')"><a>IP</a></th>
                    <th ng-if="moons" ng-click="sort('district')"><a>District</a></th>
                    <th></th>
                </tr>
                <tr ng-repeat="member in memberList.data" ng-click="editMember(member);">
                    <td>{{member.name}}</td>
                    <td>{{member.gageName}}</td>
                    <td>{{member.loginId}}</td>
                    <td>{{member.email}}</td>
                    <td>{{member.phone1}} {{member.phone2}}</td>
                    <td>{{member.creationDTime | makeDate | date:'MM/dd/yyyy hh:mm a'}}</td>
                    <td>{{member.managerName}}</td>
                    <td>{{member.role}}</td>
                    <td>{{member.sysRole}}</td>
                    <td>{{member.active == 1 ? 'Active' : 'Inactive'}}</td>
                    <td>{{member.lastAccessDTime | makeDate | date:'MM/dd/yyyy hh:mm a'}}</td>
                    <td>{{member.ipAddress}}</td>
                    <td ng-if="moons">{{moonMap.get(member.district).title || globalDistrict}}</td>
                    <td class="col-md-1 text-nowrap">
                        <button type="button" class="btn btn-xs btn-default" ng-click="editMember(member);$event.stopPropagation();">Edit</button>
                        &nbsp;&nbsp;
                        <button ng-hide="member.loginId=='admin'" type="button" class="btn btn-xs btn-default" ng-click="deleteMember(member);$event.stopPropagation();">Delete</button>
                    </td>
                </tr>
            </table>
            <div class="text-center" style="padding-top: 20px;" ng-if="memberList.totalCount > pageSize">
                <ul style="margin:0" uib-pagination total-items="memberList.totalCount" items-per-page="pageSize" ng-model="$parent.currentPage"
                    max-size="10" boundary-links="true" rotate="false" ng-change="pageChanged()"></ul>
            </div>
        </div>
    </div>
</div>
<%@include file="../../../copyright.html" %>
</body>
</html>