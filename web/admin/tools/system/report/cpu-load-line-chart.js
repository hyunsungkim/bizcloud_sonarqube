angular.module('bio.angular.report', [
    'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
]).directive('cpuLoadLineChart', ['marsContext', function (marsContext) {
    return {
        restrict: 'EA',
        transclude: true,
        scope: {
            id: "=",
            param: "<",
            refreshInterval: "<",
            height: "<",
            margin: "<",
            maxRecords: "<",
            callback: "="
        },
        link: function (scope, element, attrs) {
        },
        template: '<nvd3 id="id" options="options" data="data" class="with-3d-shadow with-transitions" ng-show="data"></nvd3>',
        controller: 'cpuLoadLineChartController'
    };
}]).controller('cpuLoadLineChartController', ['$scope', '$timeout', 'AjaxService', 'marsContext', function ($scope, $timeout, AjaxService, marsContext) {
    if ("undefined" === typeof $scope.margin) {
        $scope.margin = {};
    }
    if ("undefined" === typeof $scope.maxRecords) {
        $scope.maxRecords = 50;
    }

    $scope.options = {
        chart: {
            type: 'lineChart',
            height: $scope.height || 450,
            margin: {
                top: $scope.margin.top || 50,
                right: $scope.margin.right || 35,
                bottom: $scope.margin.bottom || 50,
                left: $scope.margin.left || 70
            },
            x: function (d) {
                return d.x;
            },
            y: function (d) {
                return d.y;
            },
            useInteractiveGuideline: true,
            dispatch: {
                stateChange: function (e) {
                    if ($scope.callback && $scope.callback.stateChange) $scope.callback.stateChange(e);
                },
                changeState: function (e) {
                    if ($scope.callback && $scope.callback.changeState) $scope.callback.changeState(e);
                },
                tooltipShow: function (e) {
                    if ($scope.callback && $scope.callback.tooltipShow) $scope.callback.tooltipShow(e);
                },
                tooltipHide: function (e) {
                    if ($scope.callback && $scope.callback.tooltipHide) $scope.callback.tooltipHide(e);

                },
                renderEnd: function (e) {
                    if ($scope.callback && $scope.callback.renderEnd) $scope.callback.renderEnd(e);
                }
            },
            xAxis: {
                axisLabel: 'Date & Time',
                tickFormat: function (d) {
                    return d3.time.format('%H:%M:%S')(mars$util$.convertUTCDateToLocalDate(new Date(d)));
                }
            },
            yAxis: {
                axisLabel: 'Load',
                tickFormat: function (d) {
                    return d3.format('.02f')(d * 100);
                },
                axisLabelDistance: 5
            },
            callback: function (chart) {
                if ($scope.callback && $scope.callback.onReady) $scope.callback.onReady(chart, $scope);
            }
        },
        title: {
            enable: true,
            text: 'CPU Load'
        }
    };

    var series = [];
    var cores = [];

    function makeReportData(response) {
        var cpuLoad = response.cpuLoad;
        var processors = cpuLoad.cpuLoadPerProcessor.processors;

        var coreData = cores[0];
        if (null == coreData || "undefined" === coreData) {
            coreData = [];
            cores[0] = coreData;
            series.push({
                values: coreData,
                key: 'Processor',
                strokeWidth: 4,
                classed: 'dashed',
                color: mars$util$.getColor(series.length)
            })
        }
        coreData.push({
            x: mars$util$.makeDate(cpuLoad.currentTimeMillis),
            y: cpuLoad.cpuLoad
        })

        if (coreData.length > $scope.maxRecords) {
            coreData.shift();
        }

        for (var i = 1; i <= processors.length; i++) {
            var coreData = cores[i];
            if (null == coreData || "undefined" === coreData) {
                coreData = [];
                cores[i] = coreData;
                series.push({
                    values: coreData,
                    key: 'Core' + i,
                    color: mars$util$.getColor(series.length)
                })
            }
            coreData.push({
                x: mars$util$.makeDate(cpuLoad.currentTimeMillis),
                y: processors[i - 1]
            })
            if (coreData.length > $scope.maxRecords) {
                coreData.shift();
            }
        }

        return series;
    }

    $scope.getReportData = function () {
        var api = new AjaxService(marsContext.contextPath + '/services/sys/info.json?report=yes');
        api.post({
            success: function (o) {
                $scope.data = makeReportData(o.data);
            },
            error: function (o) {
                o.alertException();
            }
        }, {
            type: 'cpuLoad'
        });

        if ($scope.refreshInterval && $scope.refreshInterval > 0) {
            $timeout(function () {
                $scope.getReportData();
            }, $scope.refreshInterval);
        }
    };

    // (function () {
    //     $scope.getReportData();
    // })();
}]);
