var app = angular.module('angularApp', [
    'nvd3'
    , 'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'bio.angular.report'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/sys\/info.json\?report=yes/)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$interval', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $interval, blockUI, AjaxService, marsContext) {
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;
    var blockUIStarted = false;

    function startBlockUI(msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    $scope.startBlockUI = function (msg) {
        startBlockUI(msg);
    }

    $scope.stopBlockUI = function () {
        $scope.$apply(function () {
            stopBlockUI();
        });
    }

    function getSystemInfo(type) {
        var param = {};
        if (type) param.type = type;

        var api = new AjaxService(marsContext.contextPath + '/services/sys/info.json');
        startBlockUI("Loading System Information...")
        api.post({
            success: function (o) {
                stopBlockUI();
                if (type) angular.extend($scope.systemInfo, o.data);
                else $scope.systemInfo = o.data;
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    (function () {
        getSystemInfo();
    })();

    $scope.refresh = function (type) {
        getSystemInfo(type);
    }

    $scope.networkTcp = function (items) {
        var result = {};
        angular.forEach(items, function (value, key) {
            if (value.hasOwnProperty('connectionsEstablished')) {
                result[key] = value;
            }
        });
        return result;
    }

    $scope.networkUdp = function (items) {
        var result = {};
        angular.forEach(items, function (value, key) {
            if (!value.hasOwnProperty('connectionsEstablished')) {
                result[key] = value;
            }
        });
        return result;
    }

    // Download file {
    var _fileName = null;
    var _reqKey;

    function isWaitingFile() {
        var cookieName = "DOWNLOAD-FILE-DONE" + _reqKey;
        var loadState = mars$util$.getCookie(cookieName);
        console.log("cookieName", cookieName, loadState, _fileName);
        if (loadState === _fileName) {
            mars$util$.deleteCookie(cookieName, marsContext.contextPath);
            _fileName = null;
            $interval.cancel(_timer);
            stopBlockUI();
        }

        cookieName = "BIO-EXCEPTION" + _reqKey;
        var bioException = mars$util$.getCookie(cookieName);
        if (bioException && -1 !== bioException.indexOf(_fileName)) {
            mars$util$.deleteCookie(cookieName, marsContext.contextPath);
            $interval.cancel(_timer);
            stopBlockUI();
            var frameDoc = mars$util$.getFrameDocument("hiddenGround");
            var errorMsg = JSON.parse(frameDoc.body.innerText);
            angularExt.getBootboxObject().alert(errorMsg.faultMessage || errorMsg.faultString);
        }
    }

    var _timer = null;
    $scope.downloadSystemInfo = function (appName) {
        $scope.startBlockUI("Download...");
        _fileName = "systemInfo.json";
        _reqKey = "-" + new Date().getTime();
        document.getElementById('hiddenGround').src = marsContext.contextPath + '/services/sys/download/info.json/' + encodeURIComponent(_fileName) + "?$RK=" + _reqKey;
        _timer = $interval(isWaitingFile, 1000);
    }
    // } Download File

    // CPU Load Report
    $scope.param = {};
    $scope.intervalTime = '5';
    $scope.refreshInterval = parseInt($scope.intervalTime) * 1000;
    $scope.cpuLoadReport = null;
    $scope.cpuLoadReportOn = false;

    $scope.chartCallback = {
        onReady: function (chart, scope) {
            $scope.cpuLoadReportScope = scope;
        }
    };
    $scope.loadCpuLoadChart = function (on) {
        $scope.cpuLoadReportOn = on;
        if (on) {
            $scope.cpuLoadReportScope.refreshInterval = $scope.refreshInterval;
            $scope.cpuLoadReportScope.getReportData();
        } else {
            $scope.cpuLoadReportScope.refreshInterval = -1;
        }
    }
    $scope.intervalChanged = function () {
        $scope.refreshInterval = parseInt($scope.intervalTime) * 1000;
    }
}]);

