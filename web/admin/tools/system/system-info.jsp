<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>System Information</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/nvd3/build/nv.d3.min.css"/>
    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/d3/d3.min.js"></script>
    <script src="../../../includes/node_modules/nvd3/build/nv.d3.js"></script>
    <script src="../../../includes/node_modules/angular-nvd3/dist/angular-nvd3.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("../performance/report/performance-response-line-chart.js");
        mars$require$.script("report/cpu-load-line-chart.js");
        mars$require$.script("system-info.js");
    </script>
    <style>
        .label {
            padding-left: 25px;
            font-weight: bold;
        }

        .nvtooltip.xy-tooltip {
            background-color: #0c0a10;
        }
    </style>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="page-header" style="text-align: center;">
    <h1>System Information</h1>
</div>

<div class="container-fluid">
    <div class="text-right" style="padding-bottom: 10px;">
        <div style="text-align: center;">
            <div class="btn-group" role="group">
                <a class="btn btn-info btn-xs" role="button" href="#operatingSystemPanel">Operating System</a>
                <a class="btn btn-info btn-xs" role="button" href="#computerSystemPanel">Computer System</a>
                <a class="btn btn-info btn-xs" role="button" href="#processorPanel">Processor</a>
                <a class="btn btn-info btn-xs" role="button" href="#cpuLoadPanel">CPU Load</a>
                <a class="btn btn-info btn-xs" role="button" href="#memoryPanel">Memory</a>
                <a class="btn btn-info btn-xs" role="button" href="#processesPanel">Process</a>
                <a class="btn btn-info btn-xs" role="button" href="#networkPanel">Network</a>
                <a class="btn btn-info btn-xs" role="button" href="#peripheralDevicePanel">Peripheral Device</a>
            </div>
            <button type="button" class="btn btn-primary btn-xs" ng-click="downloadSystemInfo();$event.stopPropagation();">Download System Info</button>
            <button type="button" class="btn btn-secondary btn-xs" ng-click="refresh();$event.stopPropagation();">Refresh</button>
        </div>
    </div>
    <%--CPU Load Chart--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="cpuLoadLineChartPanel" ng-show="systemInfo">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#cpuLoadLineChart" aria-expanded="false" aria-controls="#cpuLoadLineChart">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            CPU Load Line Chart
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-show="!cpuLoadReportOn" ng-click="loadCpuLoadChart(true);$event.stopPropagation();">Start</button>
                        <button type="button" class="btn btn-xs2 btn-warning" ng-show="cpuLoadReportOn" ng-click="loadCpuLoadChart(false);$event.stopPropagation();">Stop</button>
                        <select id="statusInterval" ng-model="intervalTime" ng-click="$event.stopPropagation();" ng-change="intervalChanged();" style="color:black;">
                            <option value="3">3s</option>
                            <option value="5">5s</option>
                            <option value="10">10s</option>
                            <option value="30">30s</option>
                            <option value="60">1m</option>
                            <option value="120">2m</option>
                            <option value="300">5m</option>
                            <option value="600">10m</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="cpuLoadLineChart">
            <cpu-load-line-chart id="'lineChart1'" param="param" refresh-interval="refreshInterval" callback="chartCallback"/>
        </div>
    </div>
    <%--cpuLoad--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="cpuLoadPanel" ng-show="systemInfo.cpuLoad">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#cpuLoad" aria-expanded="false" aria-controls="#cpuLoad">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            CPU Load
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('cpuLoad');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="cpuLoad">
            <div>
                <div class="row">
                    <label class="col-md-3 label">CPU Load</label>
                    <div class="col-md-9">{{systemInfo.cpuLoad.cpuLoadPercentage}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">CPU Load Averages</label>
                    <div class="col-md-9">{{systemInfo.cpuLoad.cpuLoadAverages}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">CPU Load per Core</label>
                    <div class="col-md-9">{{systemInfo.cpuLoad.cpuLoadPerProcessor.all}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">CPU, IOWait, and IRQ ticks @ 0 sec</label>
                    <div class="col-md-7">{{systemInfo.cpuLoad.cpuIOWaitAndIRQTicks0Sec}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">CPU, IOWait, and IRQ ticks @ 1 sec</label>
                    <div class="col-md-7">{{systemInfo.cpuLoad.cpuIOWaitAndIRQTicks1Sec}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">User</label>
                    <div class="col-md-9">{{systemInfo.cpuLoad.user}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Vendor Frequency</label>
                    <div class="col-md-9">{{systemInfo.cpuLoad.vendorFrequency}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Max Frequency</label>
                    <div class="col-md-9">{{systemInfo.cpuLoad.maxFrequency}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Current Frequencies per Core</label>
                    <div class="col-md-9">{{systemInfo.cpuLoad.currentFrequencies.all}}</div>
                </div>
            </div>
        </div>
    </div>
    <%--operatingSystem--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="operatingSystemPanel" ng-show="systemInfo.operatingSystem">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#operatingSystem" aria-expanded="false" aria-controls="#operatingSystem">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            Operating System
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('operatingSystem');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="operatingSystem">
            <div>
                <div class="row">
                    <label class="col-md-3 label">Name</label>
                    <div class="col-md-9">{{systemInfo.operatingSystem.operatingSystem}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Booted</label>
                    <div class="col-md-9">{{systemInfo.operatingSystem.booted | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm:ss'}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Uptime</label>
                    <div class="col-md-9">{{systemInfo.operatingSystem.uptime}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Thread Count</label>
                    <div class="col-md-9">{{systemInfo.operatingSystem.threadCount}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Evaluated</label>
                    <div class="col-md-9">{{systemInfo.operatingSystem.evaluated}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Family</label>
                    <div class="col-md-9">{{systemInfo.operatingSystem.family}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Manufacturer</label>
                    <div class="col-md-9">{{systemInfo.operatingSystem.manufacturer}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Sessions</label>
                    <div class="col-md-9">
                        <table class="table table-sm table-striped table-condensed table-font-small">
                            <tr>
                                <th>User Name</th>
                                <th>Terminal Device</th>
                                <th>Host</th>
                                <th>Login Time</th>
                                <th>Session</th>
                            </tr>
                            <tr ng-repeat="item in systemInfo.operatingSystem.sessions">
                                <td>{{item.userName}}</td>
                                <td>{{item.terminalDevice}}</td>
                                <td>{{item.host}}</td>
                                <td ng-if="item.loginTime">{{item.loginTime | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm:ss'}}</td>
                                <td ng-if="!item.loginTime"></td>
                                <td>{{item.session}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--computerSystem--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="computerSystemPanel" ng-show="systemInfo.computerSystem">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#computerSystem" aria-expanded="false" aria-controls="#computerSystem">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            Computer System
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('computerSystem');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="computerSystem">
            <div>
                <div class="row">
                    <label class="col-md-3 label">Manufacture</label>
                    <div class="col-md-9">{{systemInfo.computerSystem.manufacture}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Model</label>
                    <div class="col-md-9">{{systemInfo.computerSystem.model}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Serial Number</label>
                    <div class="col-md-9">{{systemInfo.computerSystem.serialNumber}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Firmware</label>
                    <div class="col-md-9">{{systemInfo.computerSystem.firmware.firmware}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Baseboard</label>
                    <div class="col-md-9">{{systemInfo.computerSystem.baseboard.baseboard}}</div>
                </div>
            </div>
        </div>
    </div>
    <%--processor--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="processorPanel" ng-show="systemInfo.processor">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#processor" aria-expanded="false" aria-controls="#processor">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            Processor
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('processor');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="processor">
            <div>
                <div class="row">
                    <label class="col-md-3 label">Name</label>
                    <div class="col-md-9">{{systemInfo.processor.processorIdentifier.name}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Identifier</label>
                    <div class="col-md-9">{{systemInfo.processor.processorIdentifier.identifier}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">ID</label>
                    <div class="col-md-9">{{systemInfo.processor.processorIdentifier.processorID}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Micro Architecture</label>
                    <div class="col-md-9">{{systemInfo.processor.processorIdentifier.microarchitecture}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Vendor</label>
                    <div class="col-md-9">{{systemInfo.processor.processorIdentifier.vendor}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">64 Bit</label>
                    <div class="col-md-9">{{systemInfo.processor.processorIdentifier.cpu64bit}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Frequency</label>
                    <div class="col-md-9">{{systemInfo.processor.processorIdentifier.vendorFreq}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Physical Package Count</label>
                    <div class="col-md-9">{{systemInfo.processor.physicalPackageCount}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Physical Processor Count</label>
                    <div class="col-md-9">{{systemInfo.processor.physicalProcessorCount}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Logical Processor Count</label>
                    <div class="col-md-9">{{systemInfo.processor.logicalProcessorCount}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Context Switches</label>
                    <div class="col-md-9">{{systemInfo.processor.contextSwitches}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Interrupts</label>
                    <div class="col-md-9">{{systemInfo.processor.interrupts}}</div>
                </div>
            </div>
        </div>
    </div>
    <%--memory--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="memoryPanel" ng-show="systemInfo.memory">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#memory" aria-expanded="false" aria-controls="#memory">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            Memory
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('memory');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="memory">
            <div>
                <div class="row">
                    <label class="col-md-3 label">Total</label>
                    <div class="col-md-9">{{systemInfo.memory.total}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Available</label>
                    <div class="col-md-9">{{systemInfo.memory.available}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Physical Memory</label>
                    <div class="col-md-9">{{systemInfo.memory.memory}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Virtual Memory</label>
                    <div class="col-md-9">{{systemInfo.memory.virtualMemory.virtualMemory}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Memory Banks</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.memory.physicalMemory">{{item.physicalMemory}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--processes--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="processesPanel" ng-show="systemInfo.processes">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#processes" aria-expanded="false" aria-controls="#processes">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            Process
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('processes,services');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="processes">
            <div>
                <div class="row">
                    <label class="col-md-3 label">My ID</label>
                    <div class="col-md-9">{{systemInfo.processes.processIdWithAffinity}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Current Working Directory</label>
                    <div class="col-md-9">{{systemInfo.processes.currentWorkingDirectory}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Path</label>
                    <div class="col-md-9">{{systemInfo.processes.path}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">User</label>
                    <div class="col-md-9">{{systemInfo.processes.user}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Start Time</label>
                    <div class="col-md-9">{{systemInfo.processes.startTime | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm:ss'}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Up Time</label>
                    <div class="col-md-9">{{systemInfo.processes.upTime/1000/60/60 | number: 2}} Hours</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">User Time</label>
                    <div class="col-md-9">{{systemInfo.processes.userTime}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Kernel Time</label>
                    <div class="col-md-9">{{systemInfo.processes.kernelTime}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">State</label>
                    <div class="col-md-9">{{systemInfo.processes.state}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Priority</label>
                    <div class="col-md-9">{{systemInfo.processes.priority}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Thread Count</label>
                    <div class="col-md-9">{{systemInfo.processes.threadCount}}</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Virtual Size</label>
                    <div class="col-md-9">{{systemInfo.processes.virtualSize | makeMBSize}}MB</div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Processes</label>
                    <div class="col-md-9">
                        <table class="table table-sm table-striped table-condensed table-font-small">
                            <tr>
                                <th>PID</th>
                                <th>Name</th>
                                <th>%CPU</th>
                                <th>%MEM</th>
                                <th>VSZ</th>
                                <th>RSS</th>
                                <th>State</th>
                                <th>User</th>
                                <th>Start Time</th>
                                <th>Path</th>
                            </tr>
                            <tr ng-repeat="item in systemInfo.processes.processes">
                                <td>{{item.processID}}</td>
                                <td>{{item.name}}</td>
                                <td>{{item.cpuUsage | number:1}}</td>
                                <td>{{item.memoryUsage * 100 | number: 1}}</td>
                                <td title="{{item.virtualSize}}">{{item.virtualSizeBytes}}</td>
                                <td title="{{item.residentSetSize}}">{{item.residentSetSizeBytes}}</td>
                                <td>{{item.state}}</td>
                                <td>{{item.user}}</td>
                                <td>{{item.startTime | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm:ss'}}</td>
                                <td title='{{item.commandLine}}'>{{item.path}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Services</label>
                    <div class="col-md-9">
                        <table class="table table-sm table-striped table-condensed table-font-small">
                            <tr>
                                <th>PID</th>
                                <th>Name</th>
                                <th>State</th>
                            </tr>
                            <tr ng-repeat="item in systemInfo.services">
                                <td>{{item.processID}}</td>
                                <td>{{item.name}}</td>
                                <td>{{item.state}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--network--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="networkPanel" ng-show="systemInfo">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#network" aria-expanded="false" aria-controls="#network">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            Network
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('networkInterfaces,networkParameters,internetProtocolStats');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="network">
            <div>
                <div class="row">
                    <label class="col-md-3 label">Network Interfaces</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.networkInterfaces">
                            <pre>{{item.networkInterface}}</pre>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Network Parameters</label>
                    <div class="col-md-9">
                        <pre>{{systemInfo.networkParameters.networkParameter}}</pre>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Internet Protocols</label>
                    <div class="col-md-9">
                        <table class="table table-sm table-striped table-condensed table-font-small">
                            <tr>
                                <th>Protocol</th>
                                <th>Connections Established</th>
                                <th>Connections Active</th>
                                <th>Connections Passive</th>
                                <th>Connections Failures</th>
                                <th>Connections Reset</th>
                                <th>Segments Sent</th>
                                <th>Segments Received</th>
                                <th>Segments Retransmitted</th>
                                <th>In Errors</th>
                                <th>Out Resets</th>
                            </tr>
                            <tr ng-repeat="(key, item) in networkTcp(systemInfo.internetProtocolStats)">
                                <td>{{key}}</td>
                                <td>{{item.connectionsEstablished}}</td>
                                <td>{{item.connectionsActive}}</td>
                                <td>{{item.connectionsPassive}}</td>
                                <td>{{item.connectionFailures}}</td>
                                <td>{{item.connectionsReset}}</td>
                                <td>{{item.segmentsSent}}</td>
                                <td>{{item.segmentsReceived}}</td>
                                <td>{{item.segmentsRetransmitted}}</td>
                                <td>{{item.inErrors}}</td>
                                <td>{{item.outResets}}</td>
                            </tr>
                        </table>
                        <table class="table table-sm table-striped table-condensed table-font-small">
                            <tr>
                                <th>Protocol</th>
                                <th>Datagrams Sent</th>
                                <th>Datagrams Received</th>
                                <th>Datagrams No Port</th>
                                <th>Datagrams Received Errors</th>
                            </tr>
                            <tr ng-repeat="(key, item) in networkUdp(systemInfo.internetProtocolStats)">
                                <td>{{key}}</td>
                                <td>{{item.datagramsSent}}</td>
                                <td>{{item.datagramsReceived}}</td>
                                <td>{{item.datagramsNoPort}}</td>
                                <td>{{item.datagramsReceivedErrors}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- peripheral device--%>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="peripheralDevicePanel" ng-show="systemInfo">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#peripheralDevice" aria-expanded="false" aria-controls="#peripheralDevice">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            Peripheral Device
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="refresh('sensors,powerSources,disks,fileSystem,usbDevices,soundCards,graphicCards,displays');$event.stopPropagation();">Refresh</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="peripheralDevice">
            <div>
                <div class="row">
                    <label class="col-md-3 label">Sensors</label>
                    <div class="col-md-9">
                        <pre>{{systemInfo.sensors.sensors}}</pre>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Power Sources</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.powerSources">
                            <pre>{{item.powerSource}}</pre>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Disks</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.disks">
                            <pre>{{item.disk}}</pre>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">File System</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.fileSystem.fileStores">
                            <pre>{{item.fileStore}}</pre>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">USB Devices</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.usbDevices">
                            <pre>{{item.usbDevice}}</pre>
                        </div>
                        <div ng-if="systemInfo.usbDevices.length == 0">
                            None
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Sound Cards</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.soundCards">
                            <pre>{{item.soundCard}}</pre>
                        </div>
                        <div ng-if="systemInfo.soundCards.length == 0">
                            None
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Graphic Cards</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.graphicCards">
                            <pre>{{item.graphicCard}}</pre>
                        </div>
                        <div ng-if="systemInfo.graphicCards.length == 0">
                            None
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 label">Displays</label>
                    <div class="col-md-9">
                        <div ng-repeat="item in systemInfo.displays">
                            <pre>{{item}}</pre>
                        </div>
                        <div ng-if="systemInfo.displays.length == 0">
                            None
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe name="hiddenGround" id="hiddenGround" src="../blank.html" frameborder="0" width="5" height="5" style="visibility: hidden;"></iframe>
<%@include file="../../../copyright.html" %>
</body>
</html>