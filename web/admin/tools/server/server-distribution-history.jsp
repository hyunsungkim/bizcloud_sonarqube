<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Server Distribution History</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>

    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("server-distribution-history.js");
    </script>
    <style>
        a:not([href]):not([tabindex]), a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
            color: #444;
        }
    </style>
    <script>
        var textColor = "<%=textColor%>";
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>Server Distribution History</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                <div class="card-header" id="rightPanelHeader" style="min-height: 46px;">
                    <div class="row justify-content-md-center" style="padding:10px 0 10px 0;">
                        <form class="form-inline">
                            <label for="searchName" style="padding: 0 5px 0 2px;">Name</label>
                            <input type="text" class="form-control form-control-sm" id="searchName" ng-enter="searchServer()" ng-model="search.LIKE_name_LIKE">
                            <div style="padding-left: 10px;"></div>
                            <label for="searchlabel" style="padding: 0 5px 0 2px;">Label</label>
                            <input type="text" class="form-control form-control-sm" id="searchLabel" ng-enter="searchServer()" ng-model="search.LIKE_label_LIKE">
                            <div style="padding-left: 10px;"></div>
                            <button type="button" class="btn btn-sm btn-secondary" ng-click="searchServer()">Search</button>
                            <div style="padding-left: 10px;"></div>
                            <button class="btn btn-sm btn-secondary" ng-click="refreshList();$event.stopPropagation();">Refresh</button>
                        </form>
                    </div>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div id="DataValue" ng-if="historyList">
                        <div ng-if="historyList.data.length == 0">
                            <div class="text-center">There is no distribution history</div>
                        </div>
                        <div class="table-responsive" style="border-bottom: 1px solid #dee2e6;">
                            <table class="table table-striped table-condensed text-<%=textColor%>" ng-if="historyList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold">
                                    <th>#</th>
                                    <th ng-click="sortHistoryList('userName')">Who</th>
                                    <th ng-click="sortHistoryList('transactionId')">When</th>
                                    <th ng-click="sortHistoryList('appName')">App</th>
                                    <th ng-click="sortHistoryList('name')">Name</th>
                                    <th ng-click="sortHistoryList('label')">Label</th>
                                    <th ng-click="sortHistoryList('type')">Type</th>
                                    <th ng-click="sortHistoryList('fileSize')" nowrap>Size</th>
                                    <th>Auto</th>
                                    <th ng-click="sortHistoryList('method')">Method</th>
                                    <th ng-click="sortHistoryList('priority')">Priority</th>
                                    <th ng-click="sortHistoryList('location')">Location</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat-start="item in historyList.data" id="{{item.id}}" class="dist">
                                    <td rowspan="2">{{item.ROW_NUMBER}}</td>
                                    <td>{{item.userName}}</td>
                                    <td>{{item.transactionDate | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm:ss'}}</td>
                                    <td>{{item.appName}}</td>
                                    <td><a target="{{item.id}}" ng-href="{{getDistributionUrl(item, item.name)}}" title="{{item.name}}">{{item.name}}</a></td>
                                    <td>{{item.label}}</td>
                                    <td>{{item.type}}</td>
                                    <td>{{item.fileSize|number:0}}</td>
                                    <td>{{item.auto}}</td>
                                    <td>{{item.method ? item.method : "COPY"}}</td>
                                    <td>{{item.priority ? item.priority : "0"}}</td>
                                    <td>{{item.location}}</td>
                                </tr>
                                <tr ng-repeat-end>
                                    <td colspan="11">
                                        <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                            {{item.description}}
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="historyList.totalCount > historyListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="historyList.totalCount" items-per-page="historyListPageSize" ng-model="$parent.$parent.historyListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changeHistoryListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>