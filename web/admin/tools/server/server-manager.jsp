<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>System Server Management</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>

    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("server-manager.js");
    </script>
    <style>
        a:not([href]):not([tabindex]), a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
            color: #444;
        }
    </style>
    <script>
        var textColor = "<%=textColor%>";
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>System Server Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                <div class="card-header" id="rightPanelHeader" style="min-height: 46px;">
                    <div class="row justify-content-md-center" style="padding:10px 0 10px 0;">
                        <form class="form-inline">
                            <label for="searchName" style="padding: 0 5px 0 2px;">Name</label>
                            <input type="text" class="form-control form-control-sm" id="searchName" ng-enter="searchServer()" ng-model="search.LIKE_name_LIKE">
                            <div style="padding-left: 10px;"></div>
                            <label for="searchlabel" style="padding: 0 5px 0 2px;">IP Address</label>
                            <input type="text" class="form-control form-control-sm" id="searchLabel" ng-enter="searchServer()" ng-model="search.ipAddress">
                            <div style="padding-left: 10px;"></div>
                            <button type="button" class="btn btn-sm btn-secondary" ng-click="searchServer()">Search</button>
                            <div style="padding-left: 10px;"></div>
                            <button class="btn btn-sm btn-secondary" ng-click="refreshList();$event.stopPropagation();">Refresh</button>
                        </form>
                    </div>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div id="DataValue" ng-if="serverList">
                        <div ng-if="serverList.data.length == 0">
                            <div class="text-center">There are no servers</div>
                        </div>
                        <div class="table-responsive" style="border-bottom: 1px solid #dee2e6;">
                            <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%>" ng-if="serverList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold">
                                    <th>#</th>
                                    <th ng-click="sortServerList('name')">Name</th>
                                    <th ng-click="sortServerList('ipAddress')">IP Address</th>
                                    <th ng-click="sortServerList('state')">State</th>
                                    <th>URL</th>
                                    <th>BIO Version</th>
                                    <th ng-click="sortServerList('bootDate')">Boot Date</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in serverList.data" ng-click="selectFile(item)" id="{{item.id}}" class="file">
                                    <td>{{item.ROW_NUMBER}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.ipAddress}}</td>
                                    <td>{{getStateDesc(item.state)}}</td>
                                    <td>{{item.url}}</td>
                                    <td>{{item.version}}</td>
                                    <td>{{item.bootDate | makeDate | makeLocalDate | date:'MM/dd/yyyy hh:mm:ss a'}}</td>
                                    <td>{{item.description}}</td>
                                    <td class="text-nowrap text-right" style="vertical-align: middle;">
                                        <button class="btn btn-xs2 btn-info " ng-click="showServerDistributionHistory(item);$event.stopPropagation();">Show History</button>
                                        <%if (!appDevMode) {%>
                                        <button class="btn btn-xs2 btn-warning" ng-click="runAutoDistributions(item);$event.stopPropagation();">Run Auto Distribution</button>
                                        <button class="btn btn-xs2 btn-secondary" ng-click="backupSystemApplication(item);$event.stopPropagation();">Backup</button>
                                        <%}%>
                                        <button class="btn btn-xs2 btn-primary" ng-click="updateServer(item);$event.stopPropagation();">Edit</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="serverList.totalCount > serverListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="serverList.totalCount" items-per-page="serverListPageSize" ng-model="$parent.$parent.serverListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changeServerListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>