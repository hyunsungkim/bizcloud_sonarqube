var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'blockUI'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//) || config.url.match(/myTimeInfo.json/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext',
    function ($scope, $filter, $uibModal, blockUI, AjaxService, marsContext) {
        var parameter = new UrlParameterParser(location.href);
        $scope.serverId = parameter.getParameterValue("sid");

        var blockUIStarted = false;

        function startBlockUI(msg) {
            blockUI.start(msg);
            blockUIStarted = true;
        }

        function stopBlockUI() {
            blockUI.stop();
            blockUIStarted = false;
        }

        $scope.historyListCurrentPage = 1;
        $scope.historyListPageSize = 10;
        $scope.historyListOrderColumn = 'transactionId';
        $scope.historyListOrderAsc = false;

        $scope.setPageSize = function (size) {
            $scope.historyListPageSize = size;
        };

        function callHistoryListApi(param) {
            var api = new AjaxService(marsContext.contextPath + '/services/file/run/bio.distribution-GetDistributionHistoryInfoList@.json');
            api.post({
                success: function (o) {
                    $scope.historyList = o.data;
                    stopBlockUI();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        function getHistoryList(pageNo, filter) {
            var param = {
                serverId: $scope.serverId,
                pageNo: pageNo || $scope.historyListCurrentPage,
                pageSize: $scope.historyListPageSize,
                ORDER_BY: $scope.historyListOrderColumn + ' ' + ($scope.historyListOrderAsc ? 'ASC' : 'DESC'),
            };

            $scope.historyListCurrentPage = param.pageNo;

            if (filter) {
                angular.extend(param, filter);
            }

            callHistoryListApi(param);
        }

        $scope.searchServer = function () {
            $scope.search.LIKE_name_LIKE = $scope.search.LIKE_name_LIKE === '' ? undefined : $scope.search.LIKE_name_LIKE;
            $scope.search.LIKE_label_LIKE = $scope.search.LIKE_label_LIKE === '' ? undefined : $scope.search.LIKE_label_LIKE;

            getHistoryList(1, $scope.search);
        };

        $scope.changeHistoryListPage = function () {
            getHistoryList();
        };

        $scope.sortHistoryList = function (column) {
            if ($scope.historyListOrderColumn === column) {
                $scope.historyListOrderAsc = !$scope.historyListOrderAsc;
            } else $scope.historyListOrderAsc = true;
            $scope.historyListOrderColumn = column;
            getHistoryList($scope.historyListCurrentPage);
        };

        $scope.refreshList = function () {
            getHistoryList(undefined, $scope.search);
        };

        $scope.setHeight = function () {
            setHeight();
        };

        (function () {
            getHistoryList(1);
        })();

        $scope.getDistributionUrl = function (item, distName) {
            distName = mars$util$.addFileExtension(distName, item.extension);
            return marsContext.contextPath + "/services/file/get/bio.distribution-DownloadDistribution@/" + item.distributionId + "/" + distName
                + "?etpl=/core/exception/ExceptionResponse.html&version=" + item.distributionVersion;
        };
    }]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = height + "px";
}
