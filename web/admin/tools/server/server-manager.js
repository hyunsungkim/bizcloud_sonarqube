var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'blockUI'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//) || config.url.match(/myTimeInfo.json/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $filter, $uibModal, blockUI, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);
    $scope.theme = parameter.getParameterValue("theme", "dark");
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;

    var blockUIStarted = false;

    function startBlockUI(msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    var states = [
        "R",
        "D",
        "M",
        "T"
    ];

    $scope.serverListCurrentPage = 1;
    $scope.serverListPageSize = 15;
    $scope.serverListOrderColumn = 'name';
    $scope.serverListOrderAsc = true;

    $scope.setPageSize = function (size) {
        $scope.serverListPageSize = size;
    };

    function callServerListApi(param) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/run/bio.server-GetServerList@.json');
        api.post({
            success: function (o) {
                $scope.serverList = o.data;
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function getServerList(pageNo, filter) {
        var param = {
            pageNo: pageNo || $scope.serverListCurrentPage,
            pageSize: $scope.serverListPageSize,
            ORDER_BY: $scope.serverListOrderColumn + ' ' + ($scope.serverListOrderAsc ? 'ASC' : 'DESC'),
            appName: $scope.appName
        };

        $scope.serverListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        callServerListApi(param);
    }

    $scope.searchServer = function () {
        $scope.search.LIKE_name_LIKE = $scope.search.LIKE_name_LIKE === '' ? undefined : $scope.search.LIKE_name_LIKE;
        $scope.search.ipAddress = $scope.search.ipAddress === '' ? undefined : $scope.search.ipAddress;

        getServerList(1, $scope.search);
    };

    $scope.changeServerListPage = function () {
        getServerList();
    };

    $scope.sortServerList = function (column) {
        if ($scope.serverListOrderColumn === column) {
            $scope.serverListOrderAsc = !$scope.serverListOrderAsc;
        } else $scope.serverListOrderAsc = true;
        $scope.serverListOrderColumn = column;
        getServerList($scope.serverListCurrentPage);
    };

    $scope.selectServer = function (server) {
    };

    $scope.refreshList = function () {
        getServerList(undefined, $scope.search);
    };

    $scope.getStateDesc = function (state) {
        var stateDesc = "Unknown";
        if ("R" === state) {
            stateDesc = "Running";
        } else if ("D" === state) {
            stateDesc = "Discharged";
        } else if ("M" === state) {
            stateDesc = "Maintenance"
        } else if ("T" === state) {
            stateDesc = "Temporary"
        }
        return stateDesc;
    }

    function updateServer(server) {
        var api = new AjaxService(marsContext.contextPath + '/services/data/run/bio.server-UpdateServer.json');
        api.post({
            success: function (o) {
                $scope.refreshList();
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, server);
    }

    $scope.updateServer = function (server) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/server/server-edit.html'),
            windowClass: 'app-modal-window-8',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.mode = 'Edit';
                    $scope.title = "Edit Server";
                    $scope.states = states;
                    $scope.server = angular.copy(server);

                    $scope.getStateDesc = scope.getStateDesc;
                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.server);
                    };
                }
            ]
        }).result.then(function (server) {
            updateServer(server);
        }, function () {
        });
    };

    function runAutoDistributions(item) {
        var api = new AjaxService(item.url + '/services/dist/run/autoDistributions.json');
        api.get({
            success: function (o) {
                stopBlockUI();
                var num = o && o.data && o.data.length ? o.data.length : 0;
                if (num > 0) {
                    angularExt.getBootboxObject().alert(num > 1 ? num + " distributions are deployed" : "One distribution is deployed");
                } else {
                    angularExt.getBootboxObject().alert("There is no auto distribution for the server");
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    function backupSystemApplication(item) {
        var api = new AjaxService(item.url + '/services/dist/backup/system.json');
        startBlockUI("Backup...")
        api.get({
            success: function (o) {
                stopBlockUI();
                angularExt.getBootboxObject().alert("System application backup is completed.");
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    $scope.runAutoDistributions = function (item) {
        if (item.url) runAutoDistributions(item);
        else angularExt.getBootboxObject().alert("Please update URL of the server to run auto distributions");
    }

    $scope.backupSystemApplication = function (item) {
        if (item.url) backupSystemApplication(item);
        else angularExt.getBootboxObject().alert("Please update URL of the server to run auto distributions");
    }

    $scope.setHeight = function () {
        setHeight();
    };

    (function () {
        getServerList(1);
    })();

    $scope.showServerDistributionHistory = function (item) {
        var url = "server-distribution-history.jsp?theme=" + encodeURIComponent($scope.theme) + "&t=true&sid=" + item.id;
        angularExt.openModalWindow(url, 'bootbox-dialog-frame-95 bootbox-dialog-overflow-hidden');
    };
}]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = height + "px";
}
