<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../../auth/auth.jsp" %>
<!DOCTYPE html>
<html id="ng-app" ng-app="sendMailApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Send Mail</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/quill/dist/quill.core.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/quill/dist/quill.snow.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/quill/dist/quill.bubble.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../includes/node_modules/angular-file-upload/dist/angular-file-upload.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../includes/node_modules/angular-ui-mask/dist/mask.min.js"></script>
    <script src="../../../includes/node_modules/quill/dist/quill.min.js"></script>
    <script src="../../../includes/node_modules/ng-quill/dist/ng-quill.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/angucomplete-alt/angucomplete-alt-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-service.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");

        mars$require$.script("../../org/member/member.js");
        mars$require$.script("send-mail.js");
    </script>
</head>

<body ng-controller="sendMailAppController">

<div class="container-fluid" ng-if="me">
    <div class="page-header text-center">
        <h1 class="no-margin">Send Mail</h1>
    </div>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading panel-heading-small">
                <h3 class="panel-title">
                    <table cellpadding="0" cellspacing="0" class="max-width">
                        <tr>
                            <td nowrap>
                                <span class="glyphicon glyphicon-edit"></span>&nbsp;Compose New Mail
                            </td>
                            <td style="padding-right: 50px;">&nbsp;</td>
                            <td class="text-right">
                                <button type="button" class="btn btn-default btn-xs" ng-click="sendEmail(); $event.stopPropagation();" ng-disabled="!isValid();">
                                    &nbsp;Send&nbsp;
                                </button>
                            </td>
                        </tr>
                    </table>
                </h3>
            </div>
            <div class="panel-body">
                <div class="input-group">
                    <span class="input-group-addon" ng-click="selectToRecipients()"><a>To</a></span>
                    <textarea class="form-control" aria-describedby="mailTo" ng-model="data.mailTo"></textarea>
                </div>
                <div style="padding-top: 5px;"></div>
                <div class="input-group">
                    <span class="input-group-addon" ng-click="selectCcRecipients()"><a>Cc</a></span>
                    <textarea class="form-control" aria-describedby="mailCc" ng-model="data.mailCc"></textarea>
                </div>
                <div style="padding-top: 5px;"></div>
                <div class="input-group">
                    <span class="input-group-addon" ng-click="selectBccRecipients()"><a>Bcc</a></span>
                    <textarea class="form-control" aria-describedby="mailBcc" ng-model="data.mailBcc"></textarea>
                </div>
                <div style="padding-top: 5px;"></div>
                <div class="input-group">
                    <span class="input-group-addon">From</span>
                    <input type="text" class="form-control" aria-describedby="sender" ng-model="mail.sender">
                </div>
                <div style="padding-top: 5px;"></div>
                <div class="input-group">
                    <span class="input-group-addon">Reply To</span>
                    <input type="text" class="form-control" aria-describedby="replyTo" ng-model="data.replyTo">
                </div>
                <div style="padding-top: 5px;"></div>
                <div class="input-group">
                    <span class="input-group-addon">Subject</span>
                    <input type="text" class="form-control" id="subject" aria-describedby="subject" ng-model="mail.subject">
                </div>
                <div style="padding-top: 5px;"></div>
                <div class="input-group max-width">
                    <ng-quill-editor ng-model="mail.contents" toolbar="true" link-tooltip="true" image-tooltip="true" class="mail-contents"
                                     editor-required="true" required="" error-class="input-error">
                    </ng-quill-editor>
                </div>
                <div style="padding-top: 5px;"></div>
                <div class="text-center col-md-12">
                    <input class="btn btn-primary" type="button" value="Send" ng-click="sendEmail()" ng-disabled="!isValid();">
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>


