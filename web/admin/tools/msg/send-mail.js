'use strict';

angular.module('sendMailApp', [
    , 'blockUI'
    , 'ui.bootstrap'
    , 'ngSanitize'
    , 'mars.angular.ajax'
    , 'mars.angular.ajax.service'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'memberApp'
    , 'ngQuill'
]).config(['ngQuillConfigProvider', function (ngQuillConfigProvider) {

    var config = {
            modules: {
                toolbar: [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block'],

                    // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
                    [{ 'direction': 'rtl' }],                         // text direction

                    // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                    [{ 'font': [] }],
                    [{ 'align': [] }],

                    ['clean'],                                         // remove formatting button

                    ['link', 'image', 'video']                         // link and image, video
                ]
            },
            theme: 'snow',
            placeholder: 'Insert mail contents here ...',
            readOnly: false,
            bounds: document.body
    };

    ngQuillConfigProvider.set(config);

}]).controller('sendMailAppController', ['$scope', '$uibModal', 'marsContext', 'marsService', 'marsMessageService', 'blockUI', function ($scope, $uibModal, marsContext, marsService, marsMessageService, blockUI) {
    $scope.mail = {};
    $scope.editButtonOn = false;
    $scope.toRecipients = [];
    $scope.ccRecipients = [];
    $scope.bccRecipients = [];
    $scope.blockUIMessage = "Sending...";
    $scope.data = {};

    $scope.getMyEmailAddress = function () {
        return '"' + $scope.me.name + '" <' + $scope.me.email + '>';
    };

    (function initialize() {
        marsService.whoAmI({
            success: function (o) {
                $scope.me = o.data;
                if ($scope.me.email) {
                    $scope.data.replyTo = $scope.getMyEmailAddress();
                }
                if ($scope.initialize) {
                    $scope.initialize();
                }
            },
            error: function (o) {
                o.alertException();
            }
        });
    })();

    function getMailAddresses(members) {
        var addresses = "";
        for (var i = 0; i < members.length; i++) {
            var m = members[i];
            if (m.email) {
                addresses += '"' + m.name + '" <' + m.email + '>;';
            }
        }

        return addresses;
    }

    $scope.isSelectableMember = function (member) {
        return member.email;
    };

    $scope.selectRecipients = function (users, type) {
        var scope = $scope;
        $scope.selectedMembers = angular.copy(users);
        var uibModal = angularExt.getTopUibModal() || $uibModal;
        uibModal.open({
            scope: $scope,
            templateUrl: marsContext.getTemplateUrl('/admin/org/member/member-select.html'),
            size: 'dialog-95',
            controller: 'memberAppController'
        }).result.then(function (selectedMembers) {
            if ("to" == type) {
                scope.toRecipients = selectedMembers;
                scope.data.mailTo = getMailAddresses(scope.toRecipients);
            } else if ("cc" == type) {
                scope.ccRecipients = selectedMembers;
                scope.data.mailCc = getMailAddresses(scope.ccRecipients);
            } else if ("bcc" == type) {
                scope.bccRecipients = selectedMembers;
                scope.data.mailBcc = getMailAddresses(scope.bccRecipients);
            }
        }, function () {
        });
    };

    $scope.selectToRecipients = function () {
        $scope.selectRecipients($scope.toRecipients, "to");
    };

    $scope.selectCcRecipients = function () {
        $scope.selectRecipients($scope.ccRecipients, "cc");
    };

    $scope.selectBccRecipients = function () {
        $scope.selectRecipients($scope.bccRecipients, "bcc");
    };

    $scope.removeSelectedMember = function (member, recipients) {
        var idx = recipients.indexOf(member);
        if (-1 != idx) {
            member._selected = undefined;
            recipients.splice(idx, 1);
            if (recipients === $scope.toRecipients) {
                $scope.data.mailTo = getMailAddresses(recipients);
            } else if (recipients === $scope.ccRecipients) {
                $scope.data.mailCc = getMailAddresses(recipients);
            } else if (recipients === $scope.bccRecipients) {
                $scope.data.mailBcc = getMailAddresses(recipients);
            }
        }
    };

    $scope.isValid = function () {
        return $scope.mail.subject && ($scope.data.mailTo || $scope.data.mailCc || $scope.data.mailBcc || $scope.toRecipients.length > 0 || $scope.ccRecipients.length > 0 || $scope.bccRecipients.length > 0 );
    };

    $scope.sendEmail = function (callback) {
        if ($scope.data.mailTo) {
            $scope.mail.to = $scope.data.mailTo.split(";");
        } else {
            var mailTo = getMailAddresses($scope.toRecipients);
            if (mailTo.length > 0) {
                $scope.mail.to = mailTo.split(";");
            }
        }

        if ($scope.data.mailCc) {
            $scope.mail.cc = $scope.data.mailCc.split(";");
        } else {
            var mailCc = getMailAddresses($scope.ccRecipients);
            if (mailCc.length > 0) {
                $scope.mail.cc = mailCc.split(";");
            }
        }

        if ($scope.data.mailBcc) {
            $scope.mail.bcc = $scope.data.mailBcc.split(";");
        } else {
            var mailBcc = getMailAddresses($scope.bccRecipients);
            if (mailBcc.length > 0) {
                $scope.mail.bcc = mailBcc.split(";");
            }
        }

        if ($scope.data.replyTo) {
            $scope.mail.replyTo = $scope.data.replyTo.split(";");
        }
        
        $scope.mail.quillMessage = true;

        blockUI.start($scope.blockUIMessage);
        marsMessageService.sendMail({
            success: function (o) {
                blockUI.stop();
                $scope.result = o.data;
                if (callback && callback.success) {
                    callback.success(o);
                } else {
                    bootbox.alert("Mail has been sent");
                }
            },
            error: function (o) {
                blockUI.stop();
                if (callback && callback.error) {
                    callback.error(o);
                } else {
                    o.alertExceptionOnly();
                }
            }
        }, $scope.mail);
    }
}]);

