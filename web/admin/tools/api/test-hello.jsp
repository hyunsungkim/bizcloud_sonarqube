<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>

<!DOCTYPE html>
<html ng-app="TestApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Hello</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/jsonformatter/dist/json-formatter.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/jsonformatter/dist/json-formatter.js"></script>
    <script src="../../../includes/node_modules/cryptojs/rollups/aes.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("test-hello.js");
    </script>
</head>
<body ng-controller="TestAppCtrl" ng-cloak="true">
<div class="page-header text-center">
    <h1>Test Hello~~</h1>
</div>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools"><h3 class="panel-title">Hello</h3>
        </div>
        <div class="panel-body">
            <div class="form-group col-md-10">
                <label for="apiUrl">API URL</label>
                <div ng-class="{ 'has-error': apiUrlError}">
                    <input type="text" name="apiUrl" id="apiUrl" ng-model="apiUrl" placeholder="Enter API URL" class="form-control">
                </div>
            </div>
            <div class="form-group col-md-2">
                <label for="apiMethod">Method</label>
                <div>
                    <select id="apiMethod" ng-model="method" class="form-control max-width">
                        <option value="POST">POST</option>
                        <option value="GET">GET</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label for="param">Encrypt Key (16 characters)</label>
                <input type="text" name="key" id="key" ng-model="key" placeholder="Enter Encrypt Key (must be 16 characters)" maxlength="16" class="form-control">
            </div>
            <div class="form-group col-md-12">
                <label for="param">Session Info XML</label> (A session info XML must start with "&lt;SESSIONINFO ")
                <textarea name="param" id="param" ng-model="param" placeholder="Enter a session info XML string" class="form-control" style="width: 100%; height: 150px;"></textarea>
            </div>
            <div class="text-center col-md-12">
                <input class="btn btn-primary" type="button" value="Submit" ng-click="doSubmit()">
            </div>
        </div>
    </div>
    <div class="panel panel-info" ng-show="result">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools"><h3 class="panel-title">Result</h3></div>
        <div class="panel-body">
            <div ng-if="format=='json'" style="background-color: #282828;">
                <json-formatter open="1" json="result" class="json-formatter-dark" style="overflow:scroll;min-height: 150px;padding-bottom: 15px;"></json-formatter>
            </div>
            <div ng-if="format != 'json'">
                <textarea class="form-control" style="width: 100%; height: 300px;" ng-disabled="true">{{result}}</textarea>
            </div>
        </div>
    </div>
</div>
<form name="downloadForm" method="POST">
    <input type="hidden" name="param">
</form>
</body>
</html>