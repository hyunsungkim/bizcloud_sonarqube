var app = angular.module('TestApp', [
    'blockUI'
    , 'jsonFormatter'
    , 'mars.angular.ajax'
]).controller('TestAppCtrl', ['$scope', '$http', function ($scope, $http) {
    var parameter = new UrlParameterParser(location.href);

    $scope.method = "GET";
    $scope.apiUrl = angularExt.getContextPath() + "/services/acm/hello.json";
    $scope.key = "QaSdFgBHyTerSxDf";
    $scope.param = parameter.getParameterValue("s");

    function string2Hex(tmp) {
        var str = '';
        for (var i = 0; i < tmp.length; i++) {
            str += tmp[i].charCodeAt(0).toString(16);
        }
        return str;
    }

    function getToken(info, hexKey) {
        var enc = mars$cipher$.encipherBase64(info, hexKey, hexKey);
        var token = enc + "*" + hexKey;
        var tokenBase64 = btoa(token); // make base64 string

        return tokenBase64;
    }

    function decipher(data, key, iv) {
        var result;
        try {
            result = mars$cipher$.decipherBase64(data, key, iv);
            try {
                result = JSON.parse(result);
            } catch (e) {
            }
        } catch (e) {
        }

        return result || data;
    }

    function getResult(data, hexKey) {
        var data2 = decipher(data, hexKey, hexKey);
        var result = {
            originalData: data,
            returnData: typeof data2 == 'string' ? JSON.parse(data2) : data2
        };

        return result;
    }

    function getErrorMessage(msg) {
        if (msg.faultMessage) {
            msg = msg.faultMessage;
        }
        return msg;
    }

    $scope.doSubmit = function () {
        if ("undefined" != typeof (mars$security$)) {
            mars$security$.cipher = false;
        }

        if ($scope.apiUrl) {
            $scope.format = "json";
            $scope.apiUrlError = false;
            $scope.result = undefined;
            var hexKey = string2Hex($scope.key);
            var token = getToken($scope.param, hexKey);

            var config = {
                headers: {
                    "ACM.TOKEN": token
                }
            };
            if ($scope.method == "POST") {
                $http.post($scope.apiUrl, null, config).then(function (result, status, headers, config) {
                    $scope.result = getResult(result.data, hexKey);
                }, function (error, status, headers, config) {
                    var msg = decipher(error.data, hexKey, hexKey);
                    alert(getErrorMessage(msg));
                });
            } else {
                $http.get($scope.apiUrl, config).then(function (result, status, headers, config) {
                    $scope.result = getResult(result.data, hexKey);
                }, function (error, status, headers, config) {
                    var msg = decipher(error.data, hexKey, hexKey);
                    alert(getErrorMessage(msg));
                });
            }
        } else {
            $scope.apiUrlError = true;
        }
    }
}]);



