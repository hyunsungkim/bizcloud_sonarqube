var app = angular.module('angularApp', [
    'blockUI'
    , 'jsonFormatter'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'ngSanitize'
    , 'summernote'
    , 'ui.select'
]).controller('angularAppCtrl', ['$scope', 'AjaxService', 'marsContext', function ($scope, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);

    $scope.method = "POST";
    $scope.htmlStyle = parameter.getParameterValue("st", "bootstrap");

    $scope.manual = {};
    $scope.model = {
        service: "data",
        session: ""
    };

    function getService(api) {
        var prefix = "/services/";
        var idx = api.indexOf(prefix);
        if (-1 != idx) {
            var idx1 = idx + prefix.length;
            var idx2 = api.indexOf("/", idx1);
            if (-1 != idx2) {
                return api.substring(idx1, idx2);
            }
        }

        return "data";
    }

    var paramLoad = false;

    var api = parameter.getParameterValue("a");
    if ('undefined' != typeof api) {
        if (!api.startsWith(marsContext.contextPath)) {
            api = marsContext.contextPath + api;
        }

        var name = parameter.getParameterValue("n");
        var namespace = name.split("-")[0];

        $scope.model.service = getService(api);
        $scope.model.apiUrl = api;
        $scope.model.api = {
            url: api
        };
        $scope.model.session = parameter.getParameterValue("s", undefined);
        $scope.model.namespace = namespace;
        paramLoad = true;
    }

    $scope.format = "json";
    $scope.result = {};
    $scope.resultString = "";

    function getContextServiceUrl(service) {
        var path;
        if ("data" === service) {
            path = '/services/' + service + "/context/";
        } else {
            path = "/services/context/" + service + "/";
        }

        return path;
    }

    function getQueryName() {
        if ($scope.manual.queryName) {
            return $scope.manual.queryName;
        } else {
            var strings = $scope.model.api.url.split("/");
            var names = strings[strings.length - 1].split(".");
            return names[1];
        }
    }

    function hideAlert() {
        document.getElementById("savedAlert").style.display = 'none';
        document.getElementById("deletedAlert").style.display = 'none';
    }

    $scope.alertOk = function () {
        hideAlert();
    };

    $scope.deleteApiDoc = function () {
        hideAlert();
        var queryName = getQueryName();
        var api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/api/' + queryName + '.json');
        api.delete({
            success: function (o) {
                if (o.data) {
                    document.getElementById("deletedAlert").style.display = '';
                }
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.updateQueryName = function () {
        $scope.queryInfo.name = $scope.manual.queryName;
    }

    $scope.getApiDoc = function () {
        hideAlert();
        var queryName = getQueryName();
        var api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/doc/' + queryName + '.json');
        api.get({
            success: function (o) {
                $scope.queryInfo = o.data || {};
                $scope.queryInfo.name = $scope.manual.queryName;
                $scope.model.notes = $scope.queryInfo.notes;
                $scope.genApiDoc();
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.saveApiDoc = function () {
        hideAlert();
        if ($scope.model.notes) {
            $scope.queryInfo.notes = $scope.model.notes;
        }
        var queryName = getQueryName();
        var api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/api/' + queryName + '.json');
        api.post({
            success: function (o) {
                if (o.data) {
                    document.getElementById("savedAlert").style.display = '';
                }
            },
            error: function (o) {
                o.alertException();
            }
        }, $scope.queryInfo);
    };

    $scope.genApiDoc = function () {
        $scope.result = {
            description: $scope.queryInfo.description || ""
        };

        var len = $scope.queryInfo.parameterList ? $scope.queryInfo.parameterList.length : 0;
        if (len > 0) {
            var param = {};
            for (var i = 0; i < len; i++) {
                var parameter = $scope.queryInfo.parameterList[i];
                param[parameter.name] = {
                    queryParameter: parameter.queryParameter,
                    mandatory: parameter.mandatory,
                    publication: parameter.publication,
                    type: parameter.type,
                    maxLength: parameter.maxLength,
                    description: parameter.description || ''
                };
            }

            $scope.result.param = param;
        }

        $scope.resultString = JSON.stringify($scope.result, null, 2);
    };

    $scope.selectApi = function (item) {
        $scope.manual = {};
        var queryName = getQueryName();
        var api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/api/' + queryName + '.json');
        api.get({
            success: function (o) {
                $scope.queryInfo = o.data;
                $scope.model.notes = $scope.queryInfo.notes;
                $scope.genApiDoc();
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.changeNamespace = function () {
        var api;
        if ($scope.model.namespace === "ALL") {
            api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/api-list.json');
        } else {
            api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/' + $scope.model.namespace + '/api-list.json');
        }
        api.get({
            success: function (o) {
                $scope.apis = o.data;
                if (paramLoad) {
                    $scope.selectApi();
                    paramLoad = false;
                } else {
                    $scope.model.api = undefined;
                }
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.changeSession = function () {
        if ($scope.model.session !== '') {
            var api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/namespaces.json');
            api.get({
                success: function (o) {
                    $scope.namespaces = o.data;
                    if (!paramLoad) {
                        $scope.namespaces.push("ALL");
                        $scope.model.namespace = "ALL";
                    }
                    $scope.changeNamespace();
                },
                error: function (o) {
                    o.alertException();
                }
            });
        }
    };

    $scope.changeService = function () {
        var sessionsApi = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + 'sessions.json');
        sessionsApi.get({
            success: function (o) {
                $scope.sessions = o.data;
                $scope.changeSession();
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.getServices = function () {
        var sessionsApi = new AjaxService(marsContext.contextPath + '/services/context/services.json');
        sessionsApi.get({
            success: function (o) {
                $scope.services = o.data;
                if (!paramLoad) {
                    $scope.model.service = $scope.services[0];
                }
                $scope.changeService();
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    (function () {
        $scope.getServices();
    })();

    $scope.fixChromeScroll = function () {
        setTimeout(function () {
            var div = document.createElement("div");
            div.innerHTML = "";
            document.body.appendChild(div);
            setTimeout(function () {
                document.body.removeChild(div);
            }, 100);
        }, 100);
    };
}]);



