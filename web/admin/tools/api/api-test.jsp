<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="../../auth-admin.jsp" %>
<%
    String htmlStyle = ServletUtil.getParameterValue(request, "st", "bootstrap");
%>
<!DOCTYPE html>
<html ng-app="apiTestApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Test</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <%if ("bootstrap".equals(htmlStyle)) {%>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <%} else {%>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.<%=Encode.forUriComponent(htmlStyle)%>.min.css" type="text/css"/>
    <%}%>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/jsonformatter/dist/json-formatter.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select2.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/jsonformatter/dist/json-formatter.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../includes/node_modules/ui-select/dist/select.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <!--[if lt IE 9]>
    <script>
        document.createElement('ui-select');
        document.createElement('ui-select-match');
        document.createElement('ui-select-choices');
    </script>
    <![endif]-->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("api-test.js");
    </script>

    <style>
        #sessionName .select2-drop-active {
            width: 500px;
        }

        #apiUrls .select2-drop-active {
            width: 630px;
        }

        .table {
            margin-bottom: 0;
        }

        .select2-results {
            max-height: 600px !important;
        }
    </style>

    <script>
        function changeStyle() {
            var style = document.getElementById("htmlStyle");
            var styleName = style.options[style.selectedIndex].value;
            this.location.href = "api-test.jsp?st=" + styleName;
        }
    </script>

</head>
<body ng-controller="apiTestAppCtrl" ng-cloak="true">
<div>
    <table class="max-width">
        <tr>
            <td class="max-width text-center" style="padding-left: 100px;">
                <h1>API Test</h1>
            </td>
            <td style="text-align: right;padding-right: 10px;">
                <select id="htmlStyle" name="htmlStyle" ng-model="htmlStyle" style="color:black;" onchange="changeStyle()">
                    <option value="admin">admin</option>
                    <option value="bootstrap">bootstrap</option>
                    <option value="cerulean">cerulean</option>
                    <option value="cosmo">cosmo</option>
                    <option value="cyborg">cyborg</option>
                    <option value="darkly">darkly</option>
                    <option value="flatly">flatly</option>
                    <option value="journal">journal</option>
                    <option value="lumen">lumen</option>
                    <option value="readable">readable</option>
                    <option value="sandstone">sandstone</option>
                    <option value="simplex">simplex</option>
                    <option value="slate">slate</option>
                    <option value="spacelab">spacelab</option>
                    <option value="superhero">superhero</option>
                    <option value="united">united</option>
                    <option value="yeti">yeti</option>
                </select>
            </td>
        </tr>
    </table>
</div>
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Test</h3>
                    </td>
                    <td style="text-align: right; width: 95%;">
                        <button class="btn btn-info btn-xs" style="width: 200px;" ng-click="openAPITestWithFile();$event.stopPropagation()">API Test With File</button>
                        <form target="_blank" action="api-test-with-file.jsp" id="apiTestWithFile">
                            <input type="hidden" name="url" ng-model="model.apiUrl">
                        </form>
                    </td>
                </tr>
            </table>
        </div>
        <div class="panel-body">
            <div class="form-group col-md-2">
                <label for="serviceName">Service</label>
                <ui-select id="serviceName" ng-model="model.service" title="Choose a service" theme="select2" class="max-width" on-select="changeService($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a service in the list or search service...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in services | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-2">
                <label for="sessionName">Session</label>
                <ui-select id="sessionName" ng-model="model.session" title="Choose a session" theme="select2" class="max-width" on-select="changeSession($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a session in the list or search name...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in sessions | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-2">
                <label for="namespaces">Type</label>
                <ui-select id="namespaces" ng-model="model.namespace" title="Choose a type" theme="select2" class="max-width" on-select="changeNamespace($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a type in the list or search name...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in namespaces | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-5">
                <label for="apiUrl">API URLs</label>
                <ui-select id="apiUrls" ng-model="model.api" title="Choose a URL" theme="select2" class="max-width" on-select="selectApi($item)" autofocus ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a URL in the list or search name...">{{$select.selected.actualUrl||$select.selected.url}}</ui-select-match>
                    <ui-select-choices repeat="item in apis | filter: $select.search">
                        <div ng-bind-html="item.actualUrl||item.url | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-1">
                <label for="apiMethod">Method</label>
                <div>
                    <select id="apiMethod" ng-model="model.method" class="max-width">
                        <option value="POST">POST</option>
                        <option value="GET">GET</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12">
                <input type="text" name="apiUrl" id="apiUrl" ng-model="model.apiUrl" placeholder="Enter API URL" class="max-width">
            </div>
            <div class="col-md-12" ng-if="model.resolvedApiUrl">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td nowrap>
                            <label for="apiMethod">Resolved URL:</label>
                        </td>
                        <td style="width: 95%; padding-left: 10px;">
                            <div class="max-width">{{model.resolvedApiUrl}}</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="form-group col-md-12">
                <label for="param">Parameter (JSON String)</label>
                <textarea name="param" id="param" ng-model="model.param" placeholder="Enter json parameter" class="form-control" style="width: 100%; height: 200px;"></textarea>
            </div>
            <div class="text-center col-md-12">
                <input class="btn btn-primary btn-xs" type="button" value="Submit" ng-click="doSubmit()">
            </div>
        </div>
    </div>
    <div class="panel panel-info" ng-show="queryInfo">
        <div class="panel-heading" data-toggle="collapse" data-target="#parameters">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Parameters</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-primary btn-xs" style="width: 200px;" ng-click="genParam();$event.stopPropagation()">Generate Parameter</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="parameters" class="collapse in">
            <span ng-if="queryInfo.parameterList.length == 0" style="padding-left: 10px;">None</span>
            <table class="table table-bordered table-striped table-condensed" ng-if="queryInfo.parameterList.length > 0">
                <tr>
                    <th>Name</th>
                    <th nowrap>Query Parameter</th>
                    <th>Mandatory</th>
                    <th>Publication</th>
                    <th>Type</th>
                    <th class="max-width">Value</th>
                </tr>
                <tr ng-repeat="param in queryInfo.parameterList">
                    <td>{{param.name}}</td>
                    <td nowrap>{{param.queryParameter ? 'Yes' : 'No'}}</td>
                    <td nowrap>{{param.mandatory ? 'Yes' : 'No'}}</td>
                    <td nowrap>{{param.publication ? 'Yes' : 'No'}}</td>
                    <td nowrap>{{param.type | toUpperCase}}<span ng-show="param.type && (param.type.indexOf('char') != -1 || param.type ==='number')">({{param.maxLength}})</span></td>
                    <td><textarea ng-model="param.value" class="max-width" rows="1" ng-change="genParam()"></textarea></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-success" ng-show="result && format=='json'">
        <div class="panel-heading" data-toggle="collapse" data-target="#result">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Result</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <span ng-show="responseTime">({{responseTime}} ms)</span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="result" class="collapse in panel-body" style="padding: 0;">
            <div style="background-color: #282828;">
                <json-formatter open="1" json="result" class="json-formatter-dark" style="overflow:scroll;padding-bottom: 15px;"></json-formatter>
            </div>
        </div>
    </div>
    <div class="panel panel-default" ng-show="resultString">
        <div class="panel-heading" data-toggle="collapse" data-target="#result2">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Result (Text)</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <span ng-show="responseTime">({{responseTime}} ms)</span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="result2" class="collapse panel-body" style="padding: 0;">
            <textarea class="form-control" style="width: 100%; height: 600px;" readonly>{{resultString}}</textarea>
        </div>
    </div>
</div>
<form name="downloadForm" method="POST">
    <input type="hidden" name="param">
</form>
<form name="downloadFile" target="_blank">
    <input type="hidden" name="param">
</form>
<br>
<br>
<br>
<%@include file="../../../copyright.html" %>
</body>
</html>