<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="../../auth-admin.jsp" %>
<%
    String htmlStyle = ServletUtil.getParameterValue(request, "st", "bootstrap");
%>
<!DOCTYPE html>
<html ng-app="apiTestApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Test With File</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <%if ("bootstrap".equals(htmlStyle)) {%>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <%} else {%>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.<%=Encode.forUriComponent(htmlStyle)%>.min.css" type="text/css"/>
    <%}%>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/jsonformatter/dist/json-formatter.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select2.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/jsonformatter/dist/json-formatter.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../includes/node_modules/ui-select/dist/select.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <!--[if lt IE 9]>
    <script>
        document.createElement('ui-select');
        document.createElement('ui-select-match');
        document.createElement('ui-select-choices');
    </script>
    <![endif]-->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("api-test-with-file.js");
    </script>

    <style>
        .table {
            margin-bottom: 0;
        }

        .select2-results {
            max-height: 600px !important;
        }
    </style>

    <script>
        function changeStyle() {
            var style = document.getElementById("htmlStyle");
            var styleName = style.options[style.selectedIndex].value;
            this.location.href = "api-test-with-file.jsp?st=" + styleName;
        }

        function beforeSubmit() {
            document.getElementById("param").name = document.getElementById("paramName").value;
        }
    </script>
</head>
<body ng-controller="apiTestAppCtrl" ng-cloak="true" onload="_onload();">
<div>
    <table class="max-width">
        <tr>
            <td class="max-width text-center" style="padding-left: 100px;">
                <h1>API Test With File</h1>
            </td>
            <td style="text-align: right;padding-right: 10px;">
                <select id="htmlStyle" name="htmlStyle" ng-model="htmlStyle" style="color:black;" onchange="changeStyle()">
                    <option value="admin">admin</option>
                    <option value="bootstrap">bootstrap</option>
                    <option value="cerulean">cerulean</option>
                    <option value="cosmo">cosmo</option>
                    <option value="cyborg">cyborg</option>
                    <option value="darkly">darkly</option>
                    <option value="flatly">flatly</option>
                    <option value="journal">journal</option>
                    <option value="lumen">lumen</option>
                    <option value="readable">readable</option>
                    <option value="sandstone">sandstone</option>
                    <option value="simplex">simplex</option>
                    <option value="slate">slate</option>
                    <option value="spacelab">spacelab</option>
                    <option value="superhero">superhero</option>
                    <option value="united">united</option>
                    <option value="yeti">yeti</option>
                </select>
            </td>
        </tr>
    </table>
</div>
<div class="container-fluid">
    <div class="text-right" id="titleBox">
        <a href="api-test.jsp">API Test</a>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools"><h3 class="panel-title">API Test</h3></div>
        <div class="panel-body">
            <div class="form-group col-md-10">
                <label for="apiUrl">API URL</label>
                <input type="text" name="apiUrl" id="apiUrl" ng-model="apiUrl" placeholder="Enter API URL" class="max-width" autofocus>
            </div>
            <div class="form-group col-md-2">
                <label for="apiMethod">Method</label>
                <div>
                    <select id="apiMethod" ng-model="method" class="max-width">
                        <option value="POST">POST</option>
                        <option value="GET">GET</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label for="param">Parameter (JSON String)</label>
                <textarea id="param" ng-model="param" placeholder="Enter json parameter" class="form-control" style="width: 100%; height: 200px;"></textarea>
            </div>
            <div class="form-group col-md-12">
                <form method="POST" target="frame" action="../blank.html" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" name="file" id="file">
                    </div>
                </form>
            </div>
            <div class="form-group col-md-12 text-center">
                <input class="btn btn-primary btn-xs" type="button" value="Submit" ng-click="doSubmit()">
            </div>
        </div>
    </div>
</div>

<iframe id="frame" name="frame" src="../blank.html" style="width: 100%;height: 95%;border: 0;padding: 0;margin: 0;" frameborder="0"></iframe>
<%@include file="../../../copyright.html" %>
</body>
</html>