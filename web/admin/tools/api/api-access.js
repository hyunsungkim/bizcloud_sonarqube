var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'blockUI'
    , 'angucomplete-alt',

]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/search\//) || config.url.match(/\/search.json/)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAccessCtrl', ['$scope', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', '$timeout',
    function ($scope, $filter, $uibModal, blockUI, AjaxService, marsContext) {
        var blockUIStarted = false;
        var paramLoad = false;

        $scope.bgColor = themeBgColor;
        $scope.textColor = themeTextColor;
        $scope.contextPath = marsContext.contextPath;

        $scope.model = {};
        $scope.popup = {};
        $scope.newMembers = [];
        $scope.permissionType = null;

        $scope.users = ['user', 'department', 'group'];
        $scope.userType = null;

        $scope.tenants = [];

        function startBlockUI(msg) {
            blockUI.start(msg);
            blockUIStarted = true;
        }

        function stopBlockUI() {
            blockUI.stop();
            blockUIStarted = false;
        }

        function callApi2(apiUrl, callback, param) {
            var api = new AjaxService(marsContext.contextPath + apiUrl);
            api.get({
                success: function (o) {
                    callback(o);
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        function callApi(apiUrl, callback, param) {
            var api = new AjaxService(marsContext.contextPath + apiUrl);
            api.post({
                success: function (o) {
                    callback(o);
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            }, param);
        }

        function callAccessList(apiUrl, param) {
            getServices();
            callApi(apiUrl,
                function (o) {
                    $scope.accessList = o.data;
                    stopBlockUI();
                }, param);
        }

        function getAccessList(pageNo, filter) {
            var param = {};
            if (filter) {
                angular.extend(param, filter);
            }
            callAccessList('/services/data/get/bio.api-GetAPIAccess@.json', param);
        }

        function getAllAccessList(){
            callAccessList('/services/data/get/bio.api-GetAPIAccess@.json');
        }

        function getTenant(){
            callApi('/services/data/get/bio.tenant-GetTenant.json', function (o1) {
                if (o1.data) {
                    $scope.tenants = o1.data;
                }
            });
        }

        (function () {
            getTenant();
            getAllAccessList();
        })();

        $scope.getQueryName = function (name) {
            if(null != name && name.indexOf('@') === -1) {
                return name.replace('.', '-');
            }
            return name;
        }

        function getContextServiceUrl(service) {
            var path;
            if ("data" === service) {
                path = '/services/' + service + "/context/";
            } else {
                path = "/services/context/" + service + "/";
            }

            return path;
        }

        $scope.selectApi = function () {
            if ($scope.model.query !== '' && $scope.model.query != null) {
                $scope.accessType = 'url';
                $scope.model.queryName = $scope.getQueryName($scope.model.query);
                $scope.model.url = 'services/' + $scope.model.service +'/' + $scope.model.session + '.' + $scope.model.queryName;
            }else{
                if ($scope.accessType === 'url') {
                    $scope.accessType = 'namespace';
                    $scope.model.url = 'services/' + $scope.model.service +'/' + $scope.model.session + '.' + $scope.model.namespace + '-*';
                }
            }
        };

        $scope.changeNamespace = function () {
            var url;
            if($scope.model.namespace !== '' && $scope.model.namespace != null){
                if ($scope.model.namespace === "ALL") {
                    url = getContextServiceUrl($scope.model.service) + $scope.model.session + '/api-list.json';
                }else{
                    $scope.accessType = 'namespace';
                    $scope.model.url = 'services/' + $scope.model.service +'/' + $scope.model.session + '.' + $scope.model.namespace + '-*';
                    url = getContextServiceUrl($scope.model.service) + $scope.model.session + '/' + $scope.model.namespace + '/api-list.json';
                }
                callApi2(url,
                    function (o) {
                        $scope.apis = o.data;
                        if (paramLoad) {
                            paramLoad = false;
                        }
                    });
            }else{
                if ($scope.accessType === 'namespace') {
                    $scope.accessType = 'session';
                    $scope.model.url = 'services/' + $scope.model.service +'/' + $scope.model.session + '.*';
                }
                $scope.apis = [];
            }
        };

        $scope.changeSession = function () {
            if ($scope.model.session !== '' && $scope.model.session != null) {
                $scope.accessType = 'session';
                $scope.model.url = 'services/' + $scope.model.service +'/' + $scope.model.session + '.*';
                callApi2(getContextServiceUrl($scope.model.service) + $scope.model.session + '/namespaces.json',
                    function (o) {
                        $scope.namespaces = o.data;
                        $scope.apis = [];
                        if (!paramLoad) {
                            $scope.namespaces.push("ALL");
                        }
                    });
            }else{
                if ($scope.accessType === 'session') {
                    $scope.accessType = 'service';
                    $scope.model.url = 'services/' + $scope.model.service +'/*';
                }
                $scope.accessType = 'service';
                $scope.model.url = 'services/' + $scope.model.service +'/*';
                $scope.namespaces = [];
                $scope.apis = [];
            }
        };

        function containsTenantId(value) {
            for (var i in $scope.tenants) {
                if (value.endsWith($scope.tenants[i].id)) {
                    return true;
                }
            }
            return false;
        }

        $scope.changeService = function () {
            if ($scope.model.service !== '' && $scope.model.service != null) {
                $scope.accessType = 'service';
                $scope.model.url = 'services/' + $scope.model.service +'/*';
                callApi2(getContextServiceUrl($scope.model.service) + 'sessions.json',
                    function (o) {
                        $scope.sessions = [];
                        if (o.data) {
                            for (var i in o.data) {
                                if (containsTenantId(o.data[i]) == false ) {
                                    $scope.sessions.push(o.data[i]);
                                }
                            }
                        }
                        $scope.namespaces = [];
                        $scope.apis = [];
                    });
            }else{
                $scope.popup = {};
                $scope.model.url = '';
                $scope.sessions = [];
                $scope.namespaces = [];
                $scope.apis = [];
            }
        };

        function getServices () {
            if($scope.accessType==='' || $scope.accessType==null) {
                $scope.accessType='service';
            }
            callApi2('/services/context/services.json',
                function(o){
                    $scope.services = o.data;
                });
        }

        $scope.searchUser = function(user) {
            if (user !== undefined) {
                var obj = user.originalObject;
                $scope.searchUserInfo = {
                    memberId: obj.id,
                    memberType: obj.type,
                };
            }
        };

        $scope.clearBar = function(){
            $scope.searchUserInfo = {};
            $scope.$broadcast('angucomplete-alt:clearInput', $scope.userType);
        };

        $scope.searchAccess = function () {
            $scope.api= {};
            if($scope.model.service!=='' && $scope.model.service != null) $scope.api.serviceName = $scope.model.service;
            if($scope.model.session!=='' && $scope.model.session != null) $scope.api.sessionName = $scope.model.session;
            if($scope.model.namespace!=='' && $scope.model.namespace != null && $scope.model.namespace !=='ALL') $scope.api.namespace = $scope.model.namespace;
            if($scope.model.query!=='' && $scope.model.query != null) $scope.api.queryName = $scope.getQueryName($scope.model.query);
            if($scope.searchUserInfo!=='' && $scope.searchUserInfo!=null) angular.extend($scope.api, $scope.searchUserInfo);
            getAccessList(1, $scope.api);
        };

        $scope.reset = function () {
            $scope.api = {};
            $scope.model = {};
            $scope.changeService();
            $scope.clearBar();
            $scope.userType = null;
            getAccessList(undefined, $scope.api);

        };

        $scope.refreshAccessList = function () {
            getAccessList(undefined, $scope.api);
        };

        function deleteAccess(access) {
            callApi('/services/data/run/bio.api-DeleteAPIAccess@.json',
                function (o) {
                    $scope.refreshAccessList();
                }, {
                    id: access.id,
                });
        }

        $scope.deleteAcc = function (access) {
            bootbox.confirm({
                message: "Are you sure to delete?",
                buttons: {
                    cancel: {
                        label: 'No',
                        className: 'btn btn-sm btn-secondary'
                    },
                    confirm: {
                        label: 'Yes',
                        className: 'btn btn-sm btn-primary'
                    }
                },
                callback: function (result) {
                    if (result) {
                        deleteAccess(access);
                    }
                }
            });
        };

        function createAccess(scope, newMembers, members) {
            var param = scope;

            var mergeMember = [...newMembers, ...members];
            param.permissions = mergeMember;
            callApi('/services/data/run/bio.api-CreatePermissions@.json',
                function () {
                    $scope.refreshAccessList();
                }, param);

            $scope.permissions = [];
            $scope.newMembers = [];
            $scope.conf = [];
        }

        function testAccess(scope, testMember) {
            var param = testMember;

            if(scope.serviceName!=='' && scope.serviceName != null) param.serviceName = scope.serviceName;
            if(scope.sessionName!=='' && scope.sessionName != null) param.sessionName = scope.sessionName;
            if (scope.namespace!=='' && scope.namespace != null && scope.namespace !=='ALL') param.namespace = scope.namespace;
            if(scope.queryName!=='' && scope.queryName != null) param.queryName = scope.queryName;

            callApi('/services/acm/apiAccess/test.json',
                function (o) {
                    if (o != null && o.data != null) {
                        if (o.data.accessYN === "N") {
                            bootbox.alert({
                                message: "API Access Denied",
                                size: 'small',
                                centerVertical: true
                            });
                        } else {
                            bootbox.alert({
                                message: "API Access Allowed",
                                size: 'small',
                                centerVertical: true
                            });
                        }
                    }
                }, param);

            $scope.permissions = [];
            $scope.popup = [];
        }

        function getPermission(mode, param, editInfo) {
            callApi('/services/data/get/bio.api-GetAPIAccess@.json',
                function (o) {
                    var permissionArray = [];
                    if(o.data.length>0) {
                        $scope.permissionType = o.data[0].permissionType;
                        for (var i in o.data[0].permissions) {
                            permissionArray.push(o.data[0].permissions[i]);
                        }
                        $scope.permissions = permissionArray;
                        popup(mode, editInfo);
                        stopBlockUI();
                    }
                }, param);

        }

        $scope.accessPopup = function(mode, info){
            $scope.permissions = [];
            $scope.members = [];
            $scope.newMembers = [];
            $scope.permissionType = null;
            if(mode==='Add'){
                popup(mode);
            }else if(mode==='Edit' || mode==='Test'){
                var param = {
                    id: info.id
                };
                getPermission(mode, param, info);
            }
        };

        function popup(mode, info) {
            var scope = $scope;
            $uibModal.open({
                backdrop: false,
                templateUrl: marsContext.getTemplateUrl('/admin/tools/api/api-access-popup.html'),
                windowClass: 'app-modal-window-9',
                controller: ['$scope',
                    function ($scope) {
                        $scope.scope = scope;
                        $scope.mode = mode;
                        $scope.contextPath = marsContext.contextPath;

                        $scope.model = scope.model;
                        $scope.editModel = info;
                        $scope.permissions = scope.permissions;
                        $scope.beforePermissions = scope.permissions;
                        $scope.newMembers = scope.newMembers;
                        $scope.services = scope.services;
                        $scope.sessions = scope.sessions;
                        $scope.namespaces = scope.namespaces;
                        $scope.apis = scope.apis;
                        $scope.users = scope.users;

                        $scope.popup = scope.popup;
                        $scope.popup.accessType = scope.accessType;
                        $scope.popup.queryName = scope.accessType === 'url' ? scope.model.queryName : null;
                        $scope.permissionType = scope.permissionType;
                        $scope.beforePermissionType = scope.permissionType;

                        $scope.accessScope = {};
                        $scope.members = [];
                        $scope.testUserType = null;
                        $scope.checkPermission = false;
                        $scope.changeExisting = false;

                        $scope.getQueryName = scope.getQueryName;

                        $scope.selectTestUser = function(select) {
                            $scope.testUser = {
                                memberId: select.id,
                                memberName: select.name,
                                memberType: select.type,
                                permission: select.permission
                            };
                            $scope.$apply();
                        };

                        $scope.selectApi = function () {
                            if ($scope.popup.query !== '' && $scope.popup.query != null) {
                                $scope.popup.accessType = 'url';
                                $scope.popup.queryName = $scope.getQueryName($scope.popup.query);
                                $scope.popup.url = 'services/' + $scope.popup.serviceName +'/' + $scope.popup.sessionName + '.'+ $scope.popup.queryName;
                                $scope.searchPermission();
                            }else{
                                delete $scope.popup['url'];
                                if ($scope.popup.namespace === 'ALL') {
                                    $scope.popup.accessType = 'session';
                                    $scope.popup.url = 'services/' + $scope.popup.serviceName +'/' + $scope.popup.sessionName + '.*';
                                }else {
                                    $scope.popup.accessType = 'namespace';
                                    $scope.popup.url = 'services/' + $scope.popup.serviceName +'/' + $scope.popup.sessionName + '.'+ $scope.popup.namespace + '-*';
                                }
                            }
                        };

                        $scope.changeNamespace = function () {
                            if($scope.checkPermission){
                                $scope.checkPermission = false;
                            }
                            var url;
                            if($scope.popup.namespace !=='' && $scope.popup.namespace != null){
                                if ($scope.popup.namespace === "ALL") {
                                    url = getContextServiceUrl($scope.popup.serviceName) + $scope.popup.sessionName + '/api-list.json';
                                }else{
                                    $scope.popup.accessType = 'namespace';
                                    $scope.popup.url = 'services/' + $scope.popup.serviceName +'/' + $scope.popup.sessionName + '.'+ $scope.popup.namespace + '-*';
                                    url = getContextServiceUrl($scope.popup.serviceName) + $scope.popup.sessionName + '/' + $scope.popup.namespace + '/api-list.json';
                                }
                                callApi2(url,
                                    function (o) {
                                        $scope.apis = o.data;
                                        if (paramLoad) {
                                            paramLoad = false;
                                        }
                                    });
                            }else{
                                $scope.apis = [];
                                delete $scope.popup['namespace'];
                                $scope.popup.accessType = 'session';
                                $scope.popup.url = 'services/' + $scope.popup.serviceName +'/' + $scope.popup.sessionName + '.*';
                            }
                        };

                        $scope.changeSession = function () {
                            if($scope.checkPermission){
                                $scope.checkPermission = false;
                            }
                            if ($scope.popup.sessionName !== '' && $scope.popup.sessionName != null) {
                                $scope.popup.accessType = 'session';
                                $scope.popup.url = 'services/' + $scope.popup.serviceName +'/' + $scope.popup.sessionName + '.*';
                                callApi2(getContextServiceUrl($scope.popup.serviceName) + $scope.popup.sessionName + '/namespaces.json',
                                    function (o) {
                                        $scope.namespaces = o.data;
                                        $scope.apis = [];
                                        if (!paramLoad) {
                                            $scope.namespaces.push("ALL");
                                        }
                                    });
                            }else{
                                $scope.namespaces = [];
                                $scope.apis = [];
                                delete $scope.popup['sessionName'];
                                $scope.popup.accessType = 'service';
                                $scope.popup.url = 'services/' + $scope.popup.serviceName +'/*';
                            }
                        };

                        $scope.changeService = function () {
                            if($scope.checkPermission){
                                $scope.checkPermission = false;
                            }
                            if ($scope.popup.serviceName !== '' && $scope.popup.serviceName != null) {
                                $scope.popup.accessType = 'service';
                                $scope.popup.url = 'services/' + $scope.popup.serviceName +'/*';
                                callApi2(getContextServiceUrl($scope.popup.serviceName) + 'sessions.json',
                                    function (o) {
                                        $scope.sessions = [];
                                        if (o.data) {
                                            for (var i in o.data) {
                                                if (containsTenantId(o.data[i]) == false ) {
                                                    $scope.sessions.push(o.data[i]);
                                                }
                                            }
                                        }
                                        $scope.namespaces = [];
                                        $scope.apis = [];
                                    });
                            }else{
                                $scope.sessions = [];
                                $scope.namespaces = [];
                                $scope.apis = [];
                                $scope.popup = {};
                            }
                        };

                        function getPermission(param) {
                            //get only when a user selects the access scope.
                            $scope.permissions = [];
                            $scope.newMembers = [];
                            $scope.permissionType = null;
                            if(param.accessType!=='' && param.accessType!=null) {
                                callApi('/services/data/get/bio.api-GetAPIAccess@.json',
                                    function (o) {
                                        if(o.data.length>0) {
                                            var permissionArray = [];
                                            for (var i in o.data[0].permissions) {
                                                permissionArray.push(o.data[0].permissions[i]);
                                            }
                                            bootbox.confirm({
                                                message: "There are already registered access controls. Would you like to edit it?",
                                                buttons: {
                                                    cancel: {
                                                        label: 'No',
                                                        className: 'btn btn-sm btn-secondary'
                                                    },
                                                    confirm: {
                                                        label: 'Yes',
                                                        className: 'btn btn-sm btn-primary'
                                                    }
                                                },
                                                callback: function (result) {
                                                    if(result) {
                                                        $scope.checkPermission = true;
                                                        $scope.mode='Edit';
                                                        $scope.permissionType = o.data[0].permissionType;
                                                        $scope.beforePermissionType = o.data[0].permissionType;
                                                        $scope.permissions = permissionArray;
                                                        $scope.beforePermissions = permissionArray;
                                                        $scope.editModel = $scope.popup;
                                                        $scope.$apply();
                                                    }
                                                }
                                            });
                                        }else{
                                            $scope.checkPermission = true;
                                        }
                                        stopBlockUI();
                                    }, param);
                            }
                        }

                        function setScope(scope, param){
                            param.accessType = scope.accessType;
                            param.permissionType = $scope.permissionType;

                            if(scope.serviceName!=='' && scope.serviceName!=null){
                                param.serviceName = scope.serviceName;
                            }
                            if(scope.sessionName!=='' && scope.sessionName!=null){
                                param.sessionName = scope.sessionName;
                            }
                            if(scope.namespace!=='' && scope.namespace!=null && scope.namespace!=='ALL'){
                                param.namespace = scope.namespace;
                            }
                            if(scope.queryName!=='' && scope.queryName!=null){
                                param.queryName = scope.queryName;
                            }
                            if(scope.url!=='' && scope.url!=null){
                                param.url = scope.url;
                            }
                            return param;
                        }

                        $scope.searchPermission = function() {
                            var param = {};
                            getPermission(setScope($scope.popup, param));
                        };

                        $scope.detectPermissionChange = function(type) {
                            if($scope.permissionType==null){
                                $scope.permissionType=type;
                                if($scope.newMembers.length>0){
                                    $scope.newMembers.forEach(function (arr) {
                                        arr.permission = $scope.permissionType;
                                    });
                                    $scope.accessScope.permissionType = $scope.permissionType;
                                }
                            }else{
                                if ($scope.permissionType !== type && !($scope.mode === 'Add' && $scope.newMembers.length === 0)) {
                                    bootbox.confirm({
                                        message: "Are you sure you want to change current permissions?",
                                        buttons: {
                                            cancel: {
                                                label: 'No',
                                                className: 'btn btn-sm btn-secondary'
                                            },
                                            confirm: {
                                                label: 'Yes',
                                                className: 'btn btn-sm btn-primary'
                                            }
                                        },
                                        callback: function (result) {
                                            if (result) {
                                                if ($scope.permissionType === 'A') {
                                                    document.getElementById("radioAllow").checked = false;
                                                }else{
                                                    document.getElementById("radioDeny").checked = false;
                                                }
                                                $scope.permissionType = type;

                                                if ($scope.newMembers.length > 0) {
                                                    for (var i = $scope.newMembers.length - 1; i >= 0; i--) {
                                                        $scope.newMembers[i].permission = type;
                                                    }
                                                }
                                                if ($scope.permissions.length > 0) {
                                                    for (var j = $scope.permissions.length - 1; j >= 0; j--) {
                                                        $scope.permissions[j].permission = type;
                                                    }
                                                    $scope.id = $scope.permissions[0].id;
                                                    $scope.changeExisting = $scope.beforePermissionType !== type;
                                                }
                                                if ($scope.mode === 'Add') {
                                                    $scope.accessScope = setScope($scope.popup, $scope.accessScope);
                                                } else {
                                                    $scope.accessScope = setScope($scope.editModel, $scope.accessScope);
                                                }
                                                $scope.$apply();
                                            } else {
                                                if ($scope.permissionType === 'A') {
                                                    document.getElementById("radioAllow").checked = true;
                                                    document.getElementById("radioDeny").checked = false;
                                                }else{
                                                    document.getElementById("radioDeny").checked = true;
                                                    document.getElementById("radioAllow").checked = false;
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        };

                        $scope.deletePermission = function (idx, permission, type, mode) {
                            $scope.id = null;
                            if($scope.mode === 'Edit'){
                                $scope.accessScope = setScope($scope.editModel, $scope.accessScope);
                            }else{
                                $scope.accessScope = setScope($scope.popup, $scope.accessScope);
                            }
                            bootbox.confirm({
                                message: "Are you sure you want to delete?",
                                buttons: {
                                    cancel: {
                                        label: 'No',
                                        className: 'btn btn-sm btn-secondary'
                                    },
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn btn-sm btn-primary'
                                    }
                                },
                                callback: function (result) {
                                    if(result){
                                        if(mode === 'New') {
                                            for(var i = 0; i < $scope.newMembers.length; i++) {
                                                if($scope.newMembers[i].memberId === permission.memberId){
                                                    $scope.newMembers.splice(i, 1);
                                                }
                                            }
                                        }else if(mode === 'Existing'){
                                            $scope.changeExisting = true;
                                            for(var i = 0; i < $scope.permissions.length; i++) {
                                                if($scope.permissions[i].memberId === permission.memberId){
                                                    $scope.id = $scope.permissions[i].id
                                                    $scope.permissions.splice(i, 1);
                                                }
                                            }
                                        }
                                        $scope.$apply();
                                    }
                                }
                            });
                        };

                        function checkExistMember(member) {
                            if($scope.newMembers !=='' && $scope.newMembers !== undefined) {
                                for (var i = 0; i < $scope.newMembers.length; i++) {
                                    var newMember = $scope.newMembers[i];
                                    if (newMember.memberId === member.memberId && newMember.memberType === member.memberType) return true;
                                }
                            }
                            if($scope.permissions !=='' && $scope.permissions !== undefined) {
                                for (var j = 0; j < $scope.permissions.length; j++) {
                                    var perm = $scope.permissions[j];
                                    if (perm.memberId === member.memberId && member.memberType === member.memberType) return true;
                                }
                            }
                            return false;
                        }

                        $scope.selectUser = function (user) {
                            if (user !== undefined) {
                                var obj = user.originalObject;
                                var memberInfo = {
                                    memberId: obj.id,
                                    memberType: obj.type,
                                };
                                if($scope.mode !== 'Test'){
                                    memberInfo.permission = $scope.permissionType;
                                    memberInfo.memberName = obj.name;
                                    if (!checkExistMember(memberInfo)) {
                                        if ($scope.mode === 'Edit') {
                                            $scope.accessScope = setScope($scope.editModel, $scope.accessScope);
                                        } else {
                                            $scope.accessScope = setScope($scope.popup, $scope.accessScope);
                                        }
                                        $scope.newMembers.push(memberInfo);
                                    } else {
                                        alert("Already registered on the white list or black list.");
                                    }
                                    $scope.$broadcast('angucomplete-alt:clearInput', 'user');
                                }else{
                                    $scope.testUser = memberInfo;
                                }
                            }
                        };


                        $scope.selectDept = function (dept) {
                            if (dept !== undefined) {
                                var obj = dept.originalObject;
                                var memberInfo = {
                                    memberId: obj.id,
                                    memberName: obj.name,
                                    memberType: obj.type,
                                    permission: $scope.permissionType
                                };

                                if (!checkExistMember(memberInfo)) {
                                    if ($scope.mode === 'Edit') {
                                        $scope.accessScope = setScope($scope.editModel, $scope.accessScope);
                                    } else {
                                        $scope.accessScope = setScope($scope.popup, $scope.accessScope);
                                    }
                                    $scope.newMembers.push(memberInfo);
                                } else {
                                    alert("Already registered on the white list or black list.");
                                }

                                $scope.$broadcast('angucomplete-alt:clearInput', 'dept');
                            }
                        };

                        $scope.selectUserGroup = function (group) {
                            if (group !== undefined) {
                                var obj = group.originalObject;
                                var memberInfo = {
                                    memberId: obj.id,
                                    memberName: obj.name,
                                    memberType: obj.type,
                                    permission: $scope.permissionType
                                };

                                if (!checkExistMember(memberInfo)) {
                                    if ($scope.mode === 'Edit') {
                                        $scope.accessScope = setScope($scope.editModel, $scope.accessScope);
                                    } else {
                                        $scope.accessScope = setScope($scope.popup, $scope.accessScope);
                                    }
                                    $scope.newMembers.push(memberInfo);
                                } else {
                                    alert("Already registered on the white list or black list.");
                                }

                                $scope.$broadcast('angucomplete-alt:clearInput', 'userGroup');
                            }
                        };


                        $scope.clearBar = function(){
                            $scope.$broadcast('angucomplete-alt:clearInput', $scope.testUserType);
                        };

                        function setMember(){
                            $scope.members = [];
                            $scope.permissions.forEach(function (arr) {
                                $scope.members.push({
                                    memberId: arr.memberId,
                                    memberName: arr.memberName,
                                    memberType: arr.memberType,
                                    permission: arr.permission
                                })
                            });
                        }

                        $scope.check = function () {
                            if($scope.mode === 'Add'){
                                return $scope.permissionType === null || $scope.newMembers.length === 0;
                            }else if($scope.mode === 'Edit') {
                                if ($scope.newMembers.length === 0) {
                                    if ($scope.beforePermissions.length >0 && $scope.beforePermissions.length === $scope.permissions.length) {
                                        return !$scope.changeExisting;
                                    }
                                }
                            }
                        };

                        $scope.cancel = function () {
                            $scope.$dismiss();
                        };
                        $scope.ok = function () {
                            if($scope.newMembers === undefined){
                                $scope.newMembers = [];
                            }
                            setMember();
                            $scope.$close({
                                id: $scope.id,
                                accessScope: $scope.accessScope,
                                new: $scope.newMembers,
                                members: $scope.members,
                                isChanged: $scope.changeExisting
                            });
                        };
                        $scope.test = function () {
                            // TODO
                            // $scope.$close({accessScope: $scope.accessScope, new: $scope.newMembers, members: $scope.members, id:$scope.id});
                            $scope.accessScope = setScope($scope.editModel, $scope.accessScope);
                            testAccess($scope.accessScope, $scope.testUser);
                        };
                    }
                ]
            }).result.then(function(transferData) {
                if(($scope.newMembers.length + transferData.members.length === 0) && transferData.isChanged){
                    if(transferData.id!=null) {
                        var param = {
                            id: transferData.id
                        };
                        deleteAccess(param);
                    }else{
                        console.log("id is null.");
                    }
                }else{
                    if(mode!=='Test'){
                        if(transferData.new.length>0 || transferData.isChanged) {
                            createAccess(transferData.accessScope, transferData.new, transferData.members);
                        }else{
                            console.log("nothing to update.")
                        }
                    }
                }
            }, function () {
            });
        }

        $scope.setPageSize = function (size) {
            $scope.confListPageSize = size;
        };

        $scope.setHeight = function () {
            setHeight();
        };
    }]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = height + "px";
}
