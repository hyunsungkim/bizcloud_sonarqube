var app = angular.module('apiTestApp', [
    'blockUI'
    , 'jsonFormatter'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'ui.select'
]).controller('apiTestAppCtrl', ['$scope', 'AjaxService', 'marsContext', function ($scope, AjaxService, marsContext) {
    var parameter = new UrlParameterParser(location.href);

    $scope.htmlStyle = parameter.getParameterValue("st", "bootstrap");
    $scope.apiUrl = "/bizflowio/services/file/run/fbs.file-UploadFile@.json";
    $scope.method = "POST";

    var url = parameter.getParameterValue("url");
    if ("undefined" != typeof url) {
        $scope.apiUrl = url;
    }

    $scope.doSubmit = function () {
        var frame = document.getElementById("frame");
        frame.src = '../blank.html';

        var form = document.forms[0];
        var url = $scope.apiUrl;

        if (-1 !== url.indexOf("services/data/get/")) {
            url = url.replace(/services\/data\/get\//, "services/data/getWithParam/");
        } else if (-1 !== url.indexOf("services/data/getList/")) {
            url = url.replace(/services\/data\/getList\//, "services/data/getListWithParam/");
        } else if (-1 !== url.indexOf("services/data/create/")) {
            url = url.replace(/services\/data\/create\//, "services/data/createWithParam/");
        } else if (-1 !== url.indexOf("services/data/update/")) {
            url = url.replace(/services\/data\/update\//, "services/data/updateWithParam/");
        } else if (-1 !== url.indexOf("services/data/delete/")) {
            url = url.replace(/services\/data\/delete\//, "services/data/deleteWithParam/");
        } else if (-1 !== url.indexOf("services/data/download/")) {
            url = url.replace(/services\/data\/download\//, "services/data/downloadWithParam/");
        } else if (-1 !== url.indexOf("services/file/download/")) {
            url = url.replace(/services\/file\/download\//, "services/file/downloadWithParam/");
        }

        var inputs = form.getElementsByTagName("input");
        if (inputs) {
            for (var i = inputs.length - 1; i >= 0; i--) {
                var input = inputs[i];
                var type = input.getAttribute("type");
                if (null == type || "file" != type) {
                    form.removeChild(input);
                }
            }
        }

        var jsonParam = JSON.parse($scope.param || "{}");
        var params = mars$util$.jsonToParamArray(jsonParam);
        if (params) {
            for (var i = 0; i < params.length; i++) {
                var param = params[i];
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", param.name);
                input.setAttribute("value", param.value);
                form.appendChild(input);
            }
        }

        form.action = url;
        form.method = $scope.method;
        form.submit();
    }
}]);


function _onload() {
    var titleBox = document.getElementById("titleBox");
    var height = document.body.clientHeight - titleBox.offsetHeight - 5;
    var frame = document.getElementById("frame");
    frame.style.height = height + "px";
}

