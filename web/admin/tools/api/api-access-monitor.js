var app = angular.module('angularApp', [
    'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'

]).controller('angularAccessLogCtrl', [ '$scope', 'blockUI', 'AjaxService', 'marsContext',  function($scope, blockUI, AjaxService, marsContext){

    var blockUIStarted = false;
    $scope.startDate = new Date();
    $scope.endDate = new Date();

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function reload() {
        this.location.reload(true);
    }

    function initTotalTable() {
        $('#totalTable').bootstrapTable('destroy').bootstrapTable({
            columns: [
                {
                    title: 'URI',
                    field: 'requestPath'
                }, {
                    title: 'Service Name',
                    field: 'serviceName',
                    sortable: true
                }, {
                    title: 'Session Name',
                    field: 'sessionName',
                    valign: 'middle',
                    sortable: true
                }, {
                    title: 'Namespace',
                    field: 'namespace',
                    sortable: true
                }, {
                    title: 'Query Name',
                    field: 'queryName',
                    sortable: true
                }, {
                    title: 'Total Count',
                    field: 'totalCount',
                    align: 'center',
                    sortable: true
                }, {
                    title: 'Number of Allowed',
                    field: 'allowedCount',
                    align: 'center',
                    sortable: true
                }, {
                    title: 'Number of Denied',
                    field: 'deniedCount',
                    align: 'center',
                    sortable: true
                }
            ]
        })
    }

    function initAllowTable() {
        $('#allowTable').bootstrapTable('destroy').bootstrapTable({
            columns: [
                {
                    title: 'URI',
                    field: 'requestPath'
                }, {
                    title: 'Date',
                    field: 'logDate',
                    sortable: true
                }, {
                    title: 'User',
                    field: 'loginId',
                    sortable: true
                }, {
                    title: 'User Address',
                    field: 'remoteAddress',
                    sortable: true
                }, {
                    title: 'Check Auth',
                    field: 'checkAuthYN',
                    sortable: true
                }
            ]
        })
    }

    function initDenyTable() {
        $('#denyTable').bootstrapTable('destroy').bootstrapTable({
            columns: [
                {
                    title: 'URI',
                    field: 'requestPath'
                }, {
                    title: 'Date',
                    field: 'logDate',
                    sortable: true
                }, {
                    title: 'User',
                    field: 'loginId',
                    sortable: true
                }, {
                    title: 'User Address',
                    field: 'remoteAddress',
                    sortable: true
                }, {
                    title: 'Check Auth',
                    field: 'checkAuthYN',
                    sortable: true
                }
            ]
        })
    }

    $scope.filterDate = function() {
        if($scope.startDate > $scope.endDate) {
            bootbox.alert({
                message: "The date period is not valid.",
                size: 'small',
                centerVertical: true
            });
            return;
        }
        initTotalTable();
        initAllowTable();
        initDenyTable();
    };

    $scope.downloadFile = function() {
        var today = new Date().toISOString().slice(0,10);
        var form = document.forms['downloadForm'];
        var param = {};

        param.EXCEL = {"FILE_NAME": "accesslog_" + today + ".xlsx"};
        if ($scope.startDate != null) {
            param.startDate = formatDate($scope.startDate, "start");
        }
        if ($scope.endDate != null) {
            param.endDate = formatDate($scope.endDate, "end");
        }

        form.action = marsContext.contextPath + "/services/data/getWithParam/bio.api-GetAPIAccessLog.xlsx";
        form.method = "POST";
        form.param.value = mars$cipher$.encipher(JSON.stringify(param));
        form.submit();
    }

    angular.element(document).ready(function () {
        $scope.filterDate();
    });

}]);

window.bootstrapTableAjaxOptions = {
    beforeSend: function (xhr, settings) {
        if ("POST" == settings.type) {
            settings.data = mars$cipher$.encipher(settings.data);
        }
    },
    dataFilter: function (data, type) {
        var result;
        if (data && mars$cipher$.isUseCipher()) {
            try {
                result = JSON.stringify(mars$cipher$.decipher(data));
            } catch (e) {
            }
        }
        return result || data;
    }
}

function formatDate(date, type) {
    if (typeof date === 'object') {
        var month = '' + (date.getMonth() + 1),
            day = '' + date.getDate(),
            year = date.getFullYear();
        date = [year, month, day].join('-');
    }
    var time = ("end" == type) ? '23:59:59.999' : '00:00:00.000';
    var isoDate = new Date(date + ' ' + time);
    return isoDate.toISOString();
}

function setParam(params) {
    var startDate = $('#date1').val();
    var endDate = $('#date2').val();

    if (startDate != null) {
        params.startDate = formatDate(startDate, "start");
    }
    if (endDate != null) {
        params.endDate = formatDate(endDate, "end");
    }

    if (params.sort != null) {
        params.ORDER_BY = params.sort + " " + params.order;
    }
    params.START_ROW_NUMBER = params.offset + 1;
    params.END_ROW_NUMBER = params.offset + params.limit;

    return params
}

function totalTableQueryParams(params) {
    return setParam(params);
}

function allowTableQueryParams(params) {
    params = setParam(params);
    params.accessYN = "Y";
    return params;
}

function denyTableQueryParams(params) {
    params = setParam(params);
    params.accessYN = "N";
    return params;
}
