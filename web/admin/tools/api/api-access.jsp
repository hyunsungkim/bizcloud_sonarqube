<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>

<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Access Control</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/theme/dark/bootstrap/bootstrap.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/theme/light/bootstrap/bootstrap.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/font-bizflow/font-bizflow.css"/>

    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("api-access.js");
    </script>
</head>
<body ng-controller="angularAccessCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>API Access Control</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                <div class="card-header" id="rightPanelHeader" style="min-height: 46px;">
                    <div class="row justify-content-md-center" style="padding:10px 0 10px 0;">
                        <div class="form-group col-md-1">
                            <label for="searchService">Service</label>
                            <select class="form-control form-control-sm max-width bf-font-12" id="searchService" ng-model="model.service" ng-change="changeService()">
                                <option></option>
                                <option ng-repeat="item in services" ng-value="item">{{item}}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="searchSession">Session</label>
                            <select class="form-control form-control-sm max-width bf-font-12" id="searchSession" ng-model="model.session" ng-change="changeSession()">
                                <option></option>
                                <option ng-repeat="item in sessions" ng-value="item">{{item}}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="searchType">Namespace</label>
                            <select class="form-control form-control-sm max-width bf-font-12" id="searchType" ng-model="model.namespace" ng-change="changeNamespace()">
                                <option></option>
                                <option ng-repeat="item in namespaces" ng-value="item">{{item}}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="searchAPI">API URLs</label>
                            <select class="form-control form-control-sm max-width bf-font-12" id="searchAPI" ng-model="model.query" ng-change="selectApi()">
                                <option></option>
                                <option ng-repeat="item in apis" ng-value="item.name">{{item.actualUrl||item.url}}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="searchUser">User Type</label>
                            <select class="form-control form-control-sm max-width bf-font-12" id="searchUser" ng-model="userType" ng-change="clearBar()">
                                <option></option>
                                <option ng-repeat="item in users" ng-value="item">{{item}}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-1">
                            <div ng-if="userType==null || userType===''">
                                <label for="userDefault">Name</label>
                                <input class="form-control form-control-sm bf-font-12" id="userDefault" onclick="alert('Select a user type.')">
                            </div>
                            <div ng-if="userType=='user'">
                                <label for="user">Name</label>
                                <div angucomplete-alt
                                     id="user"
                                     pause="100"
                                     selected-object="searchUser"
                                     remote-url="{{contextPath}}/services/org/user/search/appdev.json?LIKE_name_LIKE="
                                     title-field="name"
                                     search-field="name"
                                     field-required="true"
                                     initial-value=""
                                     minlength="2"
                                     input-class="form-control form-control-sm bf-font-12"
                                     name="" required>
                                </div>
                            </div>
                            <div ng-if="userType=='department' || userType=='group'">
                                <label for="{{userType}}">Name</label>
                                <div angucomplete-alt
                                     id="{{userType}}"
                                     pause="100"
                                     selected-object="searchUser"
                                     remote-url="{{contextPath}}/services/org/{{userType}}/search.json?LIKE_name_LIKE="
                                     title-field="name"
                                     search-field="name"
                                     field-required="true"
                                     initial-value=""
                                     minlength="2"
                                     input-class="form-control form-control-sm bf-font-12"
                                     name="" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 text-center mb-2" style="margin-top:25px;">
                            <button type="button" class="btn btn-sm btn-secondary bf-font-12 mr-2" ng-click="reset();$event.stopPropagation();">Reset</button>
                            <button type="button" class="btn btn-sm btn-secondary bf-font-12 mr-2" ng-click="searchAccess();$event.stopPropagation();">Search</button>
                            <button type="button" class="btn btn-sm btn-primary bf-font-12" ng-click="accessPopup('Add');$event.stopPropagation();">Create</button>
                        </div>
                    </div>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div id="DataValue">
                        <div ng-if="accessList.length == 0">
                            <div class="text-center">There are no access control list.</div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%>" ng-if="accessList.length > 0">
                                <thead>
                                <tr style="font-weight: bold;" class="bg-info conf">
                                    <th>#</th>
                                    <th><a>Service</a></th>
                                    <th><a>Session</a></th>
                                    <th><a>Namespace</a></th>
                                    <th><a>API URLs</a></th>
                                    <th><a>Permission Type</a></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="(num, i) in accessList" class="conf">
                                    <td>{{num+1}}</td>
                                    <td>{{i.serviceName}}</td>
                                    <td>{{i.sessionName}}</td>
                                    <td>{{i.namespace}}</td>
                                    <td>{{i.url}}</td>
                                    <td>
                                        <span ng-if="i.permissionType=='A'">Allow List</span>
                                        <span ng-if="i.permissionType=='D'">Deny List</span>
                                    </td>
                                    <td class="text-nowrap text-right" style="vertical-align: middle;">
                                        <button class="btn btn-xs2 btn-success" ng-click="accessPopup('Test', i);$event.stopPropagation();">Test</button>
                                        <button class="btn btn-xs2 btn-primary" ng-click="accessPopup('Edit', i);$event.stopPropagation();">Edit</button>
                                        <button class="btn btn-xs2 btn-danger" ng-click="deleteAcc(i);$event.stopPropagation();">Delete</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>