var app = angular.module('apiTestApp', [
    'blockUI'
    , 'jsonFormatter'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'ui.select'
]).controller('apiTestAppCtrl', ['$scope', 'AjaxService', 'marsContext', 'blockUI', function ($scope, AjaxService, marsContext, blockUI) {
    var parameter = new UrlParameterParser(location.href);

    $scope.htmlStyle = parameter.getParameterValue("st", "bootstrap");
    $scope.model = {
        method: "POST",
        session: ""
    };

    function getService(api) {
        var prefix = "/services/";
        var idx = api.indexOf(prefix);
        if (-1 !== idx) {
            var idx1 = idx + prefix.length;
            var idx2 = api.indexOf("/", idx1);
            if (-1 !== idx2) {
                return api.substring(idx1, idx2);
            }
        }

        return "data";
    }

    var paramLoad = false;

    var api = parameter.getParameterValue("a");
    if ('undefined' != typeof api) {
        if (!api.startsWith(marsContext.contextPath)) {
            api = marsContext.contextPath + api;
        }

        var name = parameter.getParameterValue("n");
        var namespace = name.split("-")[0];

        $scope.model.service = getService(api);
        $scope.model.apiUrl = api;
        $scope.model.api = {
            url: api
        };
        $scope.model.session = parameter.getParameterValue("s", undefined);
        $scope.model.namespace = namespace;
        paramLoad = true;
    }

    function getFormat(url) {
        var idx0 = url.indexOf("?");
        if (-1 !== idx0) {
            url = url.substring(0, idx0);
        }
        var idx = url.lastIndexOf(".");
        if (-1 !== idx) {
            return url.substring(idx + 1);
        }
    }

    function isDownloadUrl(url) {
        return -1 !== url.indexOf("services/data/download/") || -1 !== url.indexOf("services/file/download/");
    }

    function getAdjustUrl(url) {
        if (-1 !== url.indexOf("services/data/get/")) {
            url = url.replace(/services\/data\/get\//, "services/data/getWithParam/");
        } else if (-1 !== url.indexOf("services/data/getList/")) {
            url = url.replace(/services\/data\/getList\//, "services/data/getListWithParam/");
        } else if (-1 !== url.indexOf("services/data/create/")) {
            url = url.replace(/services\/data\/create\//, "services/data/createWithParam/");
        } else if (-1 !== url.indexOf("services/data/update/")) {
            url = url.replace(/services\/data\/update\//, "services/data/updateWithParam/");
        } else if (-1 !== url.indexOf("services/data/delete/")) {
            url = url.replace(/services\/data\/delete\//, "services/data/deleteWithParam/");
        } else if (-1 !== url.indexOf("services/data/run/")) {
            url = url.replace(/services\/data\/run\//, "services/data/runWithParam/");
        } else if (-1 !== url.indexOf("services/data/download/")) {
            url = url.replace(/services\/data\/download\//, "services/data/downloadWithParam/");
        } else if (-1 !== url.indexOf("services/file/download/")) {
            url = url.replace(/services\/file\/download\//, "services/file/downloadWithParam/");
        } else if (-1 !== url.indexOf("services/ais/download/")) {
            url = url.replace(/services\/ais\/download\//, "services/ais/downloadWithParam/");
        }

        return url;
    }

    function resolveUrl(url) {
        var json = JSON.parse($scope.model.param || "{}");

        for (var name in json) {
            url = url.replace(new RegExp('\\{' + name + '\\}', 'g'), json[name]);
        }
        return url;
    }

    function downloadExcel(url) {
        var form = document.forms['downloadForm'];
        var param = $scope.model.param || "{}";

        form.action = getAdjustUrl(url);
        form.method = $scope.model.method;
        form.param.value = mars$cipher$.encipher(param);
        form.submit();
    }

    function downloadFile(url) {
        var form = document.forms['downloadFile'];
        var param = $scope.model.param || "{}";

        form.action = getAdjustUrl(url);
        form.method = $scope.model.method;
        form.param.value = mars$cipher$.encipher(param);
        form.submit();
    }

    function getContextServiceUrl(service) {
        var path;
        if ("data" === service) {
            path = '/services/' + service + "/context/";
        } else {
            path = "/services/context/" + service + "/";
        }

        return path;
    }

    function getQueryName(url) {
        var strings = $scope.model.api.url.split("/");
        for (var i = strings.length - 1; i >= 0; i--) {
            if (-1 !== strings[i].indexOf(".")) return strings[i];
        }

        return "";
    }

    $scope.selectApi = function () {
        $scope.result = undefined;
        $scope.resultString = undefined;
        $scope.model.param = '';

        $scope.model.apiUrl = $scope.model.api.actualUrl || $scope.model.api.url;

        var names = getQueryName($scope.model.api.url).split(".");
        var queryName = names[1];
        var api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/api/' + queryName + '.json');
        api.get({
            success: function (o) {
                unfoldParameterSection();
                $scope.queryInfo = o.data;
            },
            error: function (o) {
                o.alertException();
            }
        });

        document.title = queryName;
    };

    function getParamValue(value) {
        if (value) {
            var jsonSyntax = "$JSON$(";
            if (jsonSyntax === value.substring(0, jsonSyntax.length) && ")" === value.substring(value.length - 1)) {
                value = JSON.parse(value.substring(jsonSyntax.length, value.length - 1));
            }

            return value;

        } else return undefined;
    }

    $scope.genParam = function () {
        var param = {};
        var len = $scope.queryInfo.parameterList.length;
        for (var i = 0; i < len; i++) {
            var parameter = $scope.queryInfo.parameterList[i];
            var names = parameter.name.split(".");
            var target = param;
            for (var j = 0; j < names.length - 1; j++) {
                var _name = names[j];
                var idx = _name.indexOf("[]");
                if (-1 !== idx) {
                    _name = _name.substring(0, idx);
                }
                if ("undefined" == typeof target[_name]) {
                    if (-1 !== idx) {
                        target[_name] = [];
                        target[_name][0] = {};
                    } else {
                        target[_name] = {};
                    }
                }

                target = -1 !== idx ? target[_name][0] : target[_name];
            }
            var currentValue = target[names[names.length - 1]];
            var value = getParamValue(parameter.value);
            if ((null == currentValue || "undefined" === typeof currentValue) && "" !== value) {
                target[names[names.length - 1]] = value;
            }
        }

        $scope.model.param = JSON.stringify(param, null, 2);

        var resolvedApiUrl = resolveUrl($scope.model.apiUrl);
        if (resolvedApiUrl !== $scope.model.apiUrl) {
            $scope.model.resolvedApiUrl = resolvedApiUrl;
        }
    };

    function unfoldParameterSection() {
        $("#parameters").addClass("in");
    }

    function foldParameterSection() {
        $("#parameters").removeClass("in");
    }

    function getParam(param) {
        try {
            return JSON.parse(param);
        } catch (e) {
            return param;
        }
    }

    function stopBlockUI() {
        blockUI.stop();
    }

    $scope.doSubmit = function () {
        var start = new Date();
        if ($scope.model.apiUrl) {
            var resolvedApiUrl = resolveUrl($scope.model.apiUrl);
            if (resolvedApiUrl !== $scope.model.apiUrl) {
                $scope.model.resolvedApiUrl = resolvedApiUrl;
            }
            $scope.format = getFormat(resolvedApiUrl);
            if ($scope.format == "xlsx" || $scope.format == 'xls' || $scope.format == 'csv' || $scope.format == 'accdb' || $scope.format == 'mdb' || isDownloadUrl(resolvedApiUrl)) {
                downloadExcel(resolvedApiUrl);
            } else if ($scope.format == "json" || $scope.format == 'xml' || $scope.format == 'file') {
                $scope.model.apiUrlError = false;
                $scope.result = undefined;
                $scope.resultString = undefined;
                var service = new AjaxService(resolvedApiUrl);
                if ($scope.model.method == "POST") {
                    var param = $scope.model.param ? getParam($scope.model.param) : {};
                    service.post({
                        success: function (o) {
                            foldParameterSection();
                            $scope.responseTime = new Date().getTime() - start.getTime();
                            $scope.result = o.data;
                            $scope.resultString = JSON.stringify($scope.result, null, 2);
                        },
                        error: function (o) {
                            stopBlockUI();
                            o.alertExceptionOnly();
                        }
                    }, param);
                } else {
                    var param = $scope.model.param ? $scope.model.param : "";
                    service.get({
                        success: function (o) {
                            $scope.responseTime = new Date().getTime() - start.getTime();
                            $scope.result = o.data;
                            $scope.resultString = JSON.stringify($scope.result, null, 2);
                        },
                        error: function (o) {
                            stopBlockUI();
                            o.alertExceptionOnly();
                        }
                    }, param);
                }
            } else {
                downloadFile(resolvedApiUrl);
            }
        } else {
            $scope.model.apiUrlError = true;
        }
    };

    $scope.changeNamespace = function () {
        var api;
        if ($scope.model.namespace === "ALL") {
            api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/api-list.json');
        } else if ($scope.model.namespace !== '') {
            api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/' + $scope.model.namespace + '/api-list.json');
        }
        if (api) {
            api.get({
                success: function (o) {
                    $scope.apis = o.data;
                    if (paramLoad) {
                        $scope.selectApi();
                        paramLoad = false;
                    } else {
                        $scope.model.api = undefined;
                    }
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            });
        }
    };

    $scope.changeSession = function () {
        if ($scope.model.session !== '') {
            var api = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + $scope.model.session + '/namespaces.json');
            api.get({
                success: function (o) {
                    $scope.namespaces = o.data;
                    if (!paramLoad) {
                        $scope.namespaces.push("ALL");
                        $scope.model.namespace = "ALL";
                    }
                    $scope.changeNamespace();
                },
                error: function (o) {
                    stopBlockUI();
                    o.alertException();
                }
            });
        }
    };

    $scope.changeService = function () {
        var sessionsApi = new AjaxService(marsContext.contextPath + getContextServiceUrl($scope.model.service) + 'sessions.json');
        sessionsApi.get({
            success: function (o) {
                $scope.sessions = o.data;
                $scope.changeSession();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    };

    $scope.getServices = function () {
        var sessionsApi = new AjaxService(marsContext.contextPath + '/services/context/services.json');
        sessionsApi.get({
            success: function (o) {
                $scope.services = o.data;
                if (!paramLoad) {
                    $scope.model.service = $scope.services[0];
                }
                $scope.changeService();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    };

    (function () {
        $scope.getServices();
    })();

    $scope.openAPITestWithFile = function () {
        var form = document.getElementById("apiTestWithFile");
        form.url.value = resolveUrl($scope.model.apiUrl) || "";
        form.submit();
    };

    $scope.fixChromeScroll = function () {
        setTimeout(function () {
            var div = document.createElement("div");
            div.innerHTML = "";
            document.body.appendChild(div);
            setTimeout(function () {
                document.body.removeChild(div);
            }, 100);
        }, 100);
    };
}]);



