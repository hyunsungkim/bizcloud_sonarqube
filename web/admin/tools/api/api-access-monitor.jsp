<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="java.io.File" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>

<%
    String prefix = ServletUtil.getParameterValue(request, "prefix", "api-access");
    String status = ServletUtil.getParameterValue(request, "status", "OK");
%>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>API Access Monitor</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("api-access-monitor.js");
    </script>
    <script>
        function reload() {
            this.location.reload(true);
        }
    </script>
</head>
<body ng-controller="angularAccessLogCtrl" ng-cloak="true">
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>API Access Monitor</h1>
</div>
<%
    String performFileName = null;

    File toolDir = new File(application.getRealPath("/admin"));
    File rootDir = toolDir.getParentFile();
    File docDir = new File(rootDir, "doc");
    if (!docDir.exists()) {
        docDir.mkdirs();
    }
%>
<div class="container-fluid">
    <div class="text-center">
        <div class="btn-group" role="group" aria-label="...">
            <div type="button" class="btn btn-default btn-xs"><a href="#AccessTotalPanel">The Total Access</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#AccessAllowPanel">The Allowed Access</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#AccessDenyPanel">The Denied Access</a></div>
        </div>
        <a class="btn btn-default btn-xs" ng-click="downloadFile();">Download api-access</a>
        <a class="btn btn-default btn-xs" href="#" onclick="reload();">Refresh</a>
    </div>
    <br>
    <div class="text-right form-inline" role="form">
        <div class="form-group">
            <span>From&nbsp;</span>
            <input class="form-control w70" type="date" id="date1" name="date1" ng-model="startDate">
        </div>
        <div class="form-group">
            <span>&nbsp;To&nbsp;</span>
            <input class="form-control w70" type="date" id="date2" name="date2" ng-model="endDate">
        </div>
        <button id="ok" type="submit" class="btn btn-primary" ng-click="filterDate()">Search</button>
    </div>
    <br>
    <div class="panel panel-primary" id="AccessTotalPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#Total"><h3 class="panel-title">The Total Access</h3></div>
        <div id="Total" class="collapse in overflow-auto">
            <table id="totalTable" class="table table-striped table-condensed" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true"
                   data-pagination="true" data-side-pagination="server" data-method="post" data-ajax-options="bootstrapTableAjaxOptions"
                   data-url="<%=request.getContextPath()%>/services/data/getList/bio.api-GetAPIAccessReport.json" data-query-params="totalTableQueryParams"
                   data-total-field="totalCount" data-data-field="data">
                <thead>
                <tr>
                    <th data-field='requestPath' data-sortable='true' class="header text-nowrap">URI</th>
                    <th data-field='serviceName' data-sortable='true' class="header" data-visible="false">Service Name</th>
                    <th data-field='sessionName' data-sortable='true' class="header" data-visible="false">Session Name</th>
                    <th data-field='namespace' data-sortable='true' class="header" data-visible="false">Namespace</th>
                    <th data-field='queryName' data-sortable='true' class="header" data-visible="false">Query Name</th>
                    <th data-field='totalCount' data-sortable='true' class="header">Total Count</th>
                    <th data-field='allowedCount' data-sortable='true' class="header">Number of Allowed</th>
                    <th data-field='deniedCount' data-sortable='true' class="header">Number of Denied</th>
                </tr>
                </thead>
                <tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-primary" id="AccessAllowPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#Allow"><h3 class="panel-title">The Allowed Access</h3></div>
        <div id="Allow" class="collapse in overflow-auto">
            <table id="allowTable" class="table table-striped table-condensed" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true"
                   data-pagination="true" data-side-pagination="server" data-method="post" data-ajax-options="bootstrapTableAjaxOptions"
                   data-url="<%=request.getContextPath()%>/services/data/getList/bio.api-GetAPIAccessLog.json" data-query-params="allowTableQueryParams"
                   data-total-field="totalCount" data-data-field="data">
                <thead>
                <tr>
                    <th data-field='requestPath' data-sortable='true' class="header text-nowrap">URI</th>
                    <th data-field='logDate' data-sortable='true' class="header">Date</th>
                    <th data-field='loginId' data-sortable='true' class="header">User</th>
                    <th data-field='remoteAddress' data-sortable='true' class="header">User Address</th>
                    <th data-field='checkAuthYN' data-sortable='true' class="header">Check Auth</th>
                </tr>
                </thead>
                <tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-primary" id="AccessDenyPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#Deny"><h3 class="panel-title">The Denied Access</h3></div>
        <div id="Deny" class="collapse in overflow-auto">
            <table id="denyTable" class="table table-striped table-condensed" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true"
                   data-pagination="true" data-side-pagination="server" data-method="post" data-ajax-options="bootstrapTableAjaxOptions"
                   data-url="<%=request.getContextPath()%>/services/data/getList/bio.api-GetAPIAccessLog.json" data-query-params="denyTableQueryParams"
                   data-total-field="totalCount" data-data-field="data">
                <thead>
                <tr>
                    <th data-field='requestPath' data-sortable='true' class="header text-nowrap">URI</th>
                    <th data-field='logDate' data-sortable='true' class="header">Date</th>
                    <th data-field='loginId' data-sortable='true' class="header">User</th>
                    <th data-field='remoteAddress' data-sortable='true' class="header">User Address</th>
                    <th data-field='checkAuthYN' data-sortable='true' class="header">Check Auth</th>
                </tr>
                </thead>
                <tbody>
            </table>
        </div>
    </div>
</div>
</div>
<form name="downloadForm" method="POST">
    <input type="hidden" name="param">
</form>
</body>
</html>