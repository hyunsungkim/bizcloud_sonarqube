<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="../../auth-admin.jsp" %>
<%
    String htmlStyle = ServletUtil.getParameterValue(request, "st", "bootstrap");
%>
<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Documentation</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <%if ("bootstrap".equals(htmlStyle)) {%>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <%} else {%>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.<%=Encode.forUriComponent(htmlStyle)%>.min.css" type="text/css"/>
    <%}%>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/jsonformatter/dist/json-formatter.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/summernote/dist/summernote.css">
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select2.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/jsonformatter/dist/json-formatter.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../includes/node_modules/ui-select/dist/select.min.js"></script>
    <script src="../../../includes/node_modules/summernote/dist/summernote.min.js"></script>
    <script src="../../../includes/node_modules/angular-summernote/dist/angular-summernote.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <!--[if lt IE 9]>
    <script>
        document.createElement('ui-select');
        document.createElement('ui-select-match');
        document.createElement('ui-select-choices');
    </script>
    <![endif]-->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("api-documentation.js");
    </script>

    <style>
        .table {
            margin-bottom: 0;
        }

        .text-dark-bg-light {
            color: #343a40;
            background-color: #f8f9fa;
        }

        .html-contents .ql-container {
            min-height: 200px;
        }

        .select2-results {
            max-height: 600px !important;
        }
    </style>

    <script>
        function changeStyle() {
            var style = document.getElementById("htmlStyle");
            var styleName = style.options[style.selectedIndex].value;
            this.location.href = "api-documentation.jsp?st=" + styleName;
        }
    </script>

</head>
<body ng-controller="angularAppCtrl" ng-cloak="true">
<div>
    <table class="max-width">
        <tr>
            <td class="max-width text-center" style="padding-left: 100px;">
                <h1>API Documentation</h1>
            </td>
            <td style="text-align: right;padding-right: 10px;">
                <select id="htmlStyle" name="htmlStyle" ng-model="htmlStyle" style="color:black;" onchange="changeStyle()">
                    <option value="admin">admin</option>
                    <option value="bootstrap">bootstrap</option>
                    <option value="cerulean">cerulean</option>
                    <option value="cosmo">cosmo</option>
                    <option value="cyborg">cyborg</option>
                    <option value="darkly">darkly</option>
                    <option value="flatly">flatly</option>
                    <option value="journal">journal</option>
                    <option value="lumen">lumen</option>
                    <option value="readable">readable</option>
                    <option value="sandstone">sandstone</option>
                    <option value="simplex">simplex</option>
                    <option value="slate">slate</option>
                    <option value="spacelab">spacelab</option>
                    <option value="superhero">superhero</option>
                    <option value="united">united</option>
                    <option value="yeti">yeti</option>
                </select>
            </td>
        </tr>
    </table>
</div>
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools"><h3 class="panel-title">API Info</h3></div>
        <div class="panel-body">
            <div class="form-group col-md-1">
                <label for="serviceName">Service</label>
                <ui-select id="serviceName" ng-model="model.service" title="Choose a service" theme="select2" class="max-width" on-select="changeService($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a service in the list or search service...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in services | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-2">
                <label for="sessionName">Session</label>
                <ui-select id="sessionName" ng-model="model.session" title="Choose a session" theme="select2" class="max-width" on-select="changeSession($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a session in the list or search session...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in sessions | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-2">
                <label for="namespaces">Type</label>
                <ui-select id="namespaces" ng-model="model.namespace" title="Choose a type" theme="select2" class="max-width" on-select="changeNamespace($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a type in the list or search type...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in namespaces | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-7">
                <label for="apiUrls">API URLs</label>
                <ui-select id="apiUrls" ng-model="model.api" title="Choose a URL" theme="select2" class="max-width" on-select="selectApi($item)" focus-me="true" autofocus ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a URL in the list or search URL...">{{$select.selected.url}}</ui-select-match>
                    <ui-select-choices repeat="item in apis | filter: $select.search">
                        <div ng-bind-html="item.url | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-12">
                <form class="form-inline">
                    <div class="form-group">
                        <input class="form-control form-control-xs" placeholder="Query Name" ng-model="manual.queryName" style="width: 400px;" ng-change="updateQueryName()">
                    </div>
                    <button class="btn btn-default btn-xs" ng-click="getApiDoc();">Get Document</button>
                </form>
            </div>
            <div class="form-group col-md-12" ng-show="manual.queryName">
                <input type="text" ng-model="queryInfo.url" placeholder="Enter API URL" class="max-width">
            </div>
            <div class="form-group col-md-12" ng-show="queryInfo && queryInfo.queryString">
                <label for="queryString">Query</label>
                <textarea id="queryString" ng-model="queryInfo.queryString" class="form-control" readonly style="width: 100%; height: 200px;"></textarea>
            </div>
        </div>
    </div>
    <div class="alert alert-success alert-sm" role="alert" id="savedAlert" style="display: none;">
        The API document was saved.
        <button type="button" class="close" aria-label="Close" ng-click="alertOk()"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="alert alert-success alert-sm" role="alert" id="deletedAlert" style="display: none;">
        The API document was deleted.
        <button type="button" class="close" aria-label="Close" ng-click="alertOk()"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="panel panel-info" ng-show="queryInfo">
        <div class="panel-heading" data-toggle="collapse" data-target="#apiDoc">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Document</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-danger btn-xs" style="width: 200px;" ng-click="deleteApiDoc();$event.stopPropagation();">Delete</button>
                        <button class="btn btn-default btn-xs" style="width: 200px;" ng-click="saveApiDoc();$event.stopPropagation();" accesskey="s">Save</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="apiDoc" class="collapse panel-body" style="padding: 0;">
            <div id="jsonWindow" style="background-color: #282828;">
                <json-formatter open="50" json="result" class="json-formatter-dark" style="overflow:scroll;padding-bottom: 15px;"></json-formatter>
            </div>
            <%--<div>--%>
            <%--<textarea id="textArea" class="form-control" htmlStyle="width: 100%; height: 100%;" ng-disabled="true">{{resultString}}</textarea>--%>
            <%--</div>--%>
        </div>
    </div>
    <div class="panel panel-primary" ng-show="queryInfo">
        <div class="panel-heading" data-toggle="collapse" data-target="#apiCategory">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Category</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-default btn-xs" style="width: 200px;" ng-click="saveApiDoc();$event.stopPropagation()" accesskey="s">Save</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="apiCategory" class="collapse in">
            <input ng-model="queryInfo.category" class="form-control" rows="2" placeholder="Enter the category. For example, I18N, Project, or Form" style="width: 100%;" ng-change="genApiDoc()" autofocus>
        </div>
    </div>
    <div class="panel panel-info" ng-show="queryInfo">
        <div class="panel-heading" data-toggle="collapse" data-target="#apiDesc">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Description</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-default btn-xs" style="width: 200px;" ng-click="saveApiDoc();$event.stopPropagation()" accesskey="s">Save</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="apiDesc" class="collapse in">
            <textarea ng-model="queryInfo.description" class="form-control" rows="2" placeholder="Enter the description" style="width: 100%;" ng-change="genApiDoc()"></textarea>
        </div>
    </div>
    <div class="panel panel-success" ng-show="queryInfo">
        <div class="panel-heading" data-toggle="collapse" data-target="#apiUrl">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API URL(s)</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-default btn-xs" style="width: 200px;" ng-click="saveApiDoc();$event.stopPropagation()" accesskey="s">Save</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="apiUrl" class="collapse in">
            <ol ng-if="queryInfo.paginationUrl">
                <li>None Pagination: <i>{{queryInfo.url}}</i></li>
                <li ng-if="queryInfo.paginationUrl">Pagination: <i>{{queryInfo.paginationUrl}}</i></li>
            </ol>
            <ul ng-if="!queryInfo.paginationUrl">
                <li><i>{{queryInfo.url}}</i></li>
            </ul>
        </div>
    </div>
    <div class="panel panel-primary" ng-show="queryInfo">
        <div class="panel-heading" data-toggle="collapse" data-target="#apiParameters">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Parameters</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-default btn-xs" style="width: 200px;" ng-click="saveApiDoc();$event.stopPropagation()" accesskey="s">Save</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="apiParameters" class="collapse in text-dark-bg-light">
            <span ng-if="queryInfo.parameterList.length == 0" style="padding-left: 10px;">None</span>
            <table class="table table-bordered table-condensed text-dark-bg-light" ng-if="queryInfo.parameterList.length > 0">
                <tr>
                    <th>Name</th>
                    <th nowrap>Query Parameter</th>
                    <th>Mandatory</th>
                    <th>Publication</th>
                    <th>Type</th>
                    <th nowrap>Max Length</th>
                    <th class="max-width">Description</th>
                </tr>
                <tr ng-repeat="param in queryInfo.parameterList" class="text-dark-bg-light">
                    <td>{{param.name}}</td>
                    <td>
                        <select ng-model="param.queryParameter" class="max-width">
                            <option ng-value="true">Yes</option>
                            <option ng-value="false">No</option>
                        </select>
                    </td>
                    <td>
                        <select ng-model="param.mandatory" class="max-width">
                            <option ng-value="true">Yes</option>
                            <option ng-value="false">No</option>
                        </select>
                    </td>
                    <td>
                        <select ng-model="param.publication" class="max-width">
                            <option ng-value="true">Yes</option>
                            <option ng-value="false">No</option>
                        </select>
                    </td>
                    <td>
                        <select ng-model="param.type">
                            <option value="">UNKNOWN</option>
                            <option value="bigint">BIGINT</option>
                            <option value="binary">BINARY</option>
                            <option value="bit">BIT</option>
                            <option value="blob">BLOB</option>
                            <option value="boolean">BOOLEAN</option>
                            <option value="char">CHAR</option>
                            <option value="clob">CLOB</option>
                            <option value="date">DATE</option>
                            <option value="datetime">DATETIME</option>
                            <option value="decimal">DECIMAL</option>
                            <option value="double">DOUBLE</option>
                            <option value="file">FILE</option>
                            <option value="float">FLOAT</option>
                            <option value="integer">INTEGER</option>
                            <option value="json">JSON</option>
                            <option value="longvarbinary">LONGVARBINARY</option>
                            <option value="longvarchar">LONGVARCHAR</option>
                            <option value="nchar">NCHAR</option>
                            <option value="nclob">NCLOB</option>
                            <option value="numeric">NUMERIC</option>
                            <option value="number">NUMBER</option>
                            <option value="nvarchar">NVARCHAR</option>
                            <option value="nvarchar2">NVARCHAR2</option>
                            <option value="real">REAL</option>
                            <option value="smallint">SMALLINT</option>
                            <option value="struct">STRUCT</option>
                            <option value="time">TIME</option>
                            <option value="timestamp">TIMESTAMP</option>
                            <option value="tinyint">TINYINT</option>
                            <option value="uuid33">UUID(33)</option>
                            <option value="varbinary">VARBINARY</option>
                            <option value="varchar">VARCHAR</option>
                            <option value="varchar2">VARCHAR2</option>
                            <option value="xml">XML</option>
                            <option value="xmltype">XMLTYPE</option>
                            <option value="array">ARRAY</option>
                            <option value="other">OTHER</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" ng-model="param.maxLength" class="max-width" ng-show="param.type && (param.type.indexOf('char') != -1 || param.type ==='number')">
                    </td>
                    <td><textarea ng-model="param.description" class="max-width no-border no-border-color" rows="1" ng-change="genApiDoc()"></textarea></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="panel panel-info" ng-show="queryInfo || manual.apiUrl">
        <div class="panel-heading" data-toggle="collapse" data-target="#apiNotes">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Notes</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-default btn-xs" style="width: 200px;" ng-click="saveApiDoc();$event.stopPropagation();" accesskey="s">Save</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="apiNotes" class="collapse in max-width text-dark-bg-light">
            <summernote ng-model="model.notes " class="html-contents" placeholder="'Enter notes here...'"></summernote>
        </div>
    </div>
</div>
<br>
<br>
<%@include file="../../../copyright.html" %>
</body>
</html>