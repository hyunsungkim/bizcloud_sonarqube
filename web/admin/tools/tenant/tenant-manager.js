var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'ngSanitize'
    , 'TreeWidget'
    , 'blockUI'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $filter, $uibModal, blockUI, AjaxService, marsContext) {
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;
    // Constants
    var RootNodeId = "rootNode";
    var TreeRoot = 'TreeRoot';
    var Group = 'Group';
    var Tenant = 'Tenant';

    $scope.createMode = "Register";

    $scope.TreeRoot = TreeRoot;
    $scope.Group = Group;
    $scope.Tenant = Tenant;

    // Variables
    $scope.selectedNodeType = null;
    $scope.search = {};

    var blockUIStarted = false;

    function startBlockUI() {
        blockUI.start();
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function getObjectType(node) {
        var objectType = null;

        if (node.nodeId === RootNodeId) {
            objectType = TreeRoot;
        } else if ('undefined' != typeof node.identifier) {
            objectType = Tenant;
        } else if ('undefined' != typeof node.name) {
            objectType = Group;
        }

        return objectType;
    }

    function generateNodeId(node) {
        var objectType = getObjectType(node);
        var nodeId = "";
        if (Tenant === objectType) {
            nodeId = "T" + node.id;
        } else if (Group === objectType) {
            nodeId = "G" + node.id;
        } else {
            nodeId = RootNodeId;
        }

        return nodeId;
    }

    function getNodeImage(node) {
        var image;
        var objectType = getObjectType(node);
        if (Tenant === objectType) {
            image = "image/tenant.png";
        } else if (Group === objectType) {
            image = "image/building.png";
        } else {
        }

        return image;
    }

    function onNodeSelected(node, selectedByClick) {
        $scope.selectedNode = node;
        $scope.selectedNodeType = getObjectType(node);
        $scope.treeOptions.selectedNode = node;
        $scope.dataValueListOrderColumn = 'name';

        if (selectedByClick) {
            if ($scope.selectedNodeType === Tenant) {
                $scope.tenantList = undefined;
                refreshBizFlowServerList();
                refreshDatabaseList();
            } else {
                getTenantList();
            }
        }
    }

    $scope.selectNode = function (node) {
        onNodeSelected(node, true);
    };

    $scope.treeOptions = {
        titleField: 'name', showIcon: true, expandOnClick: false, multipleSelect: false, filter: {},
        generateNodeId: generateNodeId,
        getNodeImage: getNodeImage,
        onSelectNode: function (node, selectedByClick) {
            onNodeSelected(node, selectedByClick);
        },
        onExpandNode: function (node) {
            $scope.expandedNode = node;
        }
    };

    function resetSelectedNode() {
        $scope.selectedNode = undefined;
        $scope.selectedNodeType = undefined;
    }

    function callApi(apiUrl, callback, param) {
        var api = new AjaxService(marsContext.contextPath + apiUrl);
        api.post({
            success: function (o) {
                callback(o);
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function getTenantTree() {
        callApi('/services/data/get/bio.tenant-GetTenantTree@.json',
            function (o) {
                $scope.treeRoot = [{
                    name: "Tenants",
                    nodeId: RootNodeId,
                    image: "image/root.png",
                    children: o.data
                }];

                stopBlockUI();
            });
    }

    $scope.refreshTree = function () {
        getTenantTree();
    };

    (function () {
        getTenantTree();
        getDefaultTenant();
    })();

    function createTenantGroup(param) {
        startBlockUI();
        callApi('/services/data/create/bio.tenant-CreateTenantGroup.json', function (o) {
            getTenantTree()
        }, param);
    }

    function searchTenantGroup(param, callback) {
        callApi('/services/data/search/bio.tenant-GetTenantGroup.json', function (o) {
            callback(o);
        }, param);
    }

    function updateTenantGroup(param) {
        startBlockUI();
        callApi('/services/data/update/bio.tenant-UpdateTenantGroup.json', function (o) {
            getTenantTree()
        }, param);
    }

    function deleteTenantGroup(param) {
        startBlockUI();
        callApi('/services/data/run/bio.tenant-DeleteTenantGroup@.json', function (o) {
            resetSelectedNode();
            getTenantTree()
        }, param);
    }

    $scope.tenantListCurrentPage = 1;
    $scope.tenantListPageSize = 10;
    $scope.tenantListOrderColumn = 'name';
    $scope.tenantListOrderAsc = true;

    function callTenantList(param) {
        callApi('/services/data/getList/bio.tenant-GetTenant.json', function (o) {
            $scope.tenantList = o.data;
        }, param);
    }

    function getTenantList(pageNo, filter) {
        var param = {
            pageNo: pageNo || $scope.tenantListCurrentPage,
            pageSize: $scope.tenantListPageSize,
            ORDER_BY: $scope.tenantListOrderColumn + ' ' + ($scope.tenantListOrderAsc ? 'ASC' : 'DESC')
        };

        if ($scope.selectedNodeType === Group) {
            param.groupId = $scope.selectedNode.id;
        }

        $scope.tenantListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        callTenantList(param);
    }

    function createTenant(param) {
        startBlockUI();
        callApi('/services/data/create/bio.tenant-CreateTenant.json', function (o) {
            getTenantTree();
            getTenantList();
        }, param);
    }

    function searchTenant(param, callback) {
        callApi('/services/data/search/bio.tenant-GetTenant.json', function (o) {
            callback(o);
        }, param);
    }

    function updateTenant(param) {
        startBlockUI();
        callApi('/services/data/update/bio.tenant-UpdateTenant.json', function (o) {
            getTenantTree();
            getTenantList();
        }, param);
    }

    function deleteTenant(param) {
        startBlockUI();
        callApi('/services/data/run/bio.tenant-DeleteTenant@.json', function (o) {
            resetSelectedNode();
            getTenantTree();
            getTenantList();
        }, param);
    }

    function editTenantGroup(tenantGroup, mode) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/tenant/tenant-group.html'),
            size: 'lg',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.mode = mode;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = mode + " Tenant Group";
                    $scope.tenantGroup = tenantGroup;
                    $scope.error = {};

                    $scope.checkGroupName = function () {
                        $scope.error.name = undefined;
                        if ($scope.tenantGroup.name != "") {
                            searchTenantGroup({name: $scope.tenantGroup.name}, function (o) {
                                if (o.data.length > 0) {
                                    $scope.error.name = "The name already exists.";
                                }
                            })
                        }
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.tenantGroup);
                    };
                }
            ]
        }).result.then(function (tenantGroup) {
            if ("Create" === mode) {
                createTenantGroup(tenantGroup);
            } else {
                updateTenantGroup(tenantGroup);
            }
        }, function () {
        });
    }

    $scope.createTenantGroup = function () {
        editTenantGroup({}, "Create");
    };

    $scope.modifyTenantGroup = function (item) {
        editTenantGroup({
            id: item.id,
            name: item.name,
            description: item.description
        }, "Modify");
    };

    $scope.deleteTenantGroup = function (item) {
        bootbox.confirm({
            message: "Are you sure to delete the group '" + item.name + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteTenantGroup({id: item.id});
                }
            }
        });
    };

    function editTenant(tenant, mode) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/tenant/tenant.html'),
            size: 'lg',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.mode = mode;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = mode + " Tenant";
                    $scope.tenant = tenant;
                    $scope.error = {};

                    $scope.checkName = function () {
                        $scope.error.name = undefined;
                        if ($scope.tenant.name != "") {
                            searchTenant({name: $scope.tenant.name}, function (o) {
                                if (o.data.length > 0) {
                                    $scope.error.name = "The name already exists.";
                                }
                            })
                        }
                    };

                    $scope.checkIdentifier = function () {
                        searchTenant({identifier: $scope.tenant.identifier}, function (o) {
                            $scope.error.identifier = undefined;
                            if (o.data.length > 0) {
                                $scope.error.identifier = "The identifier already exists.";
                            }
                        })
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.tenant);
                    };
                }
            ]
        }).result.then(function (tenant) {
            if (mode === "Create") {
                createTenant(tenant);
            } else {
                updateTenant(tenant);
            }
        }, function () {
        });
    }

    $scope.createTenant = function () {
        editTenant({
            groupId: $scope.selectedNode.id
        }, "Create");
    };

    $scope.modifyTenant = function (item) {
        editTenant({
            id: item.id,
            name: item.name,
            identifier: item.identifier,
            active: item.active,
            description: item.description
        }, "Modify");
    };

    $scope.deleteTenant = function (item) {
        bootbox.confirm({
            message: "Are you sure to delete the tenant '" + item.name + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteTenant({
                        id: item.id
                    });
                }
            }
        });
    };

    function refreshBizFlowServerList() {
        getBizFlowServer({
            tenantId: $scope.selectedNode.id
        });
    }

    function createBizFlowServer(param) {
        startBlockUI();
        callApi('/services/data/create/bio.tenant-CreateBizFlowServer.json', function (o) {
            refreshBizFlowServerList();
        }, param);
    }

    function getBizFlowServer(param) {
        callApi('/services/data/get/bio.tenant-GetBizFlowServer.json', function (o) {
            $scope.bfServerList = o.data;
            stopBlockUI();
        }, param);
    }

    function updateBizFlowServer(param) {
        startBlockUI();
        callApi('/services/data/update/bio.tenant-UpdateBizFlowServer.json', function (o) {
            refreshBizFlowServerList();
        }, param);
    }

    function deleteBizFlowServer(param) {
        startBlockUI();
        callApi('/services/data/execOne/bio.tenant-DeleteBizFlowServer.json', function (o) {
            refreshBizFlowServerList();
        }, param);
    }

    function editBizFlowServer(bfServer, mode) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/tenant/tenant-bizflow-server.html'),
            size: 'lg',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.mode = mode;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = mode + " BizFlow Server";
                    $scope.data = angular.copy(bfServer);

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.data);
                    };
                }
            ]
        }).result.then(function (data) {
            if (mode === $scope.createMode) {
                createBizFlowServer(data);
            } else {
                updateBizFlowServer(data);
            }
        }, function () {
        });
    }

    $scope.createBizFlowServer = function () {
        editBizFlowServer({tenantId: $scope.selectedNode.id, port: 7201}, $scope.createMode);
    };

    $scope.modifyBizFlowServer = function (item) {
        editBizFlowServer(item, "Modify");
    };

    $scope.deleteBizFlowServer = function (item) {
        bootbox.confirm({
            message: "Are you sure to delete the BizFlow Server '" + item.name + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteBizFlowServer({id: item.id});
                }
            }
        });
    };

    function refreshDatabaseList() {
        getDatabase({
            tenantId: $scope.selectedNode.id
        });
    }

    function createDatabase(param) {
        startBlockUI();
        callApi('/services/data/create/bio.tenant-CreateDatabase.json', function (o) {
            refreshDatabaseList();
        }, param);
    }

    function getDatabase(param) {
        callApi('/services/data/get/bio.tenant-GetDatabase.json', function (o) {
            $scope.databaseList = o.data;
            stopBlockUI();
        }, param);
    }

    function updateDatabase(param) {
        startBlockUI();
        callApi('/services/data/update/bio.tenant-UpdateDatabase.json', function (o) {
            refreshDatabaseList();
        }, param);
    }

    function deleteDatabase(param) {
        startBlockUI();
        callApi('/services/data/execOne/bio.tenant-DeleteDatabase.json', function (o) {
            refreshDatabaseList();
        }, param);
    }

    function editDatabase(dbServer, mode) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/tenant/tenant-database-server.html'),
            size: 'lg',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.mode = mode;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = mode + " Database Server";
                    $scope.data = angular.copy(dbServer);
                    $scope.data.password = undefined;

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.data);
                    };
                }
            ]
        }).result.then(function (data) {
            if (mode === $scope.createMode) {
                createDatabase(data);
            } else {
                updateDatabase(data);
            }
        }, function () {
        });
    }

    $scope.createDatabase = function () {
        editDatabase({tenantId: $scope.selectedNode.id}, $scope.createMode);
    };

    $scope.modifyDatabase = function (item) {
        editDatabase(item, "Modify");
    };

    $scope.deleteDatabase = function (item) {
        bootbox.confirm({
            message: "Are you sure to delete the Database Server '" + item.name + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteDatabase({id: item.id});
                }
            }
        });
    };

    function saveConfiguration(param, callback) {
        startBlockUI();
        callApi('/services/data/create/bio.conf-SaveConfiguration.json', function (o) {
            callback(o);
            stopBlockUI();
        }, param);
    }

    function getConfiguration(param, callback) {
        callApi('/services/data/get/bio.conf-GetConfiguration.json', function (o) {
            callback(o);
        }, param);
    }

    function updateConfiguration(param) {
        startBlockUI();
        callApi('/services/data/update/bio.conf-UpdateConfiguration.json', function (o) {
            stopBlockUI();
        }, param);
    }

    function deleteConfiguration(param) {
        startBlockUI();
        callApi('/services/data/run/bio.conf-DeleteConfiguration@.json', function (o) {
            stopBlockUI();
        }, param);
    }

    function getDefaultTenant() {
        callApi('/services/data/get/bio.tenant-GetDefaultTenant.json', function (o) {
            if (o.data.length === 1) {
                $scope.defaultTenant = o.data[0];
            }
        });
    }

    function setDefaultTenant(param) {
        startBlockUI();
        callApi('/services/data/run/bio.tenant-SetDefaultTenant@.json', function (o) {
            stopBlockUI();
            if (o.data.length === 1) {
                $scope.defaultTenant = o.data[0];
            }
        }, {
            value: param.id,
            appName: "BIO",
            type: "Tenant",
            name: "DefaultTenant.Id"
        });
    }

    $scope.makeDefaultTenant = function (item) {
        bootbox.confirm({
            message: "Are you sure to make the tenant '" + item.name + "' default tenant?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    setDefaultTenant(item);
                }
            }
        });
    };

    function insertTenant(tenantId, param) {
        callApi('/services/data/run/bio-' + tenantId + '.tenant-syncTenantGroup@.json', function (o) {
        }, param);
    }

    function syncTenant() {
        startBlockUI();
        callApi('/services/data/get/bio.tenant-GetTenant.json', function (o1) {
            var tenants = o1.data;
            callApi('/services/data/get/bio.tenant-GetTenantTreeContent@.json', function (o2) {
                var len = tenants.length;
                for (var i = 0; i < len; i++) {
                    var tenant = tenants[i];
                    if (tenant.defaultTenant !== 'Y') {
                        insertTenant(tenant.id, o2.data);
                    }
                }
                stopBlockUI();
            });
        });
    }

    $scope.syncTenant = function () {
        syncTenant();
    };

    $scope.setHeight = function () {
        setHeight();
    };
}]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var leftContainer = document.getElementById("leftPanelContainer");
    leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = leftContainer.style.height;
}
