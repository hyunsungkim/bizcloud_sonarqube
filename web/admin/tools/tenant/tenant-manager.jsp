<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<%
    String contentPath = request.getContextPath();
%>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tenant Manager</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/fontawesome-free/css/all.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.css"/>

    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../includes/node_modules-ext/angular-tree-widget/angular-tree-widget-ext.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("tenant-manager.js");
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>Tenant Manager</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col-md-3" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div class="card-header" style="padding-bottom: 0;" id="leftPanelHeader">
                    <table width="100%">
                        <tr>
                            <td>
                                <h3>Tenants</h3>
                            </td>
                            <td class="text-right" style="padding-bottom: 5px;">
                                <button class="btn btn-warning btn-xs" ng-click="syncTenant();$event.stopPropagation();">Sync</button>
                                <button class="btn btn-primary btn-xs" ng-show="selectedNodeType==TreeRoot" ng-click="createTenantGroup();$event.stopPropagation();">Create Group</button>
                                <button class="btn btn-secondary btn-xs" ng-click="refreshTree();$event.stopPropagation();">Refresh</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card-block panelContainer" id="treeContainer">
                    <div class="form-group form-group-sm">
                        <input type="text" ng-model="treeOptions.filter.name" class="form-control" placeholder="Enter a name to filter" style="margin-bottom: 10px;">
                    </div>
                    <tree nodes='treeRoot' options="treeOptions"></tree>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 0 0 1px;" id="rightPanelContainer">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" ng-show="selectedNodeType === Group" style="height: 100%;">
                <div class="card-header" id="rightPanelHeader" style="min-height: 46px;">
                    <table width="100%">
                        <tr>
                            <td nowrap>
                                <h3 class="card-title text-nowrap"> {{selectedNode.name}}</h3>
                                Default tenant: <span class="badge-pill badge-success font-12">{{defaultTenant.name}}</span>
                            </td>
                            <td style="text-align: right;">
                                <button class="btn btn-primary btn-xs" ng-show="selectedNodeType==Group" ng-click="createTenant();$event.stopPropagation();">Create Tenant</button>
                                <button class="btn btn-info btn-xs" ng-show="selectedNodeType==Group" ng-click="modifyTenantGroup(selectedNode);$event.stopPropagation();">Modify Group</button>
                                <button class="btn btn-danger btn-xs" ng-show="selectedNodeType==Group" ng-click="deleteTenantGroup(selectedNode);$event.stopPropagation();">Delete Group</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div ng-if="tenantList.totalCount == 0">
                        <div class="text-center">There are no tenants</div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%> bg-<%=bgColor%>" ng-if="tenantList.data.length > 0">
                            <thead>
                            <tr style="font-weight: bold">
                                <th>#</th>
                                <th>Name</th>
                                <th>Identifier</th>
                                <th>Description</th>
                                <th>AppDev URL</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="item in tenantList.data" title="ID={{item.id}}" id="{{item.id}}">
                                <td>{{item.ROW_NUMBER}}</td>
                                <td>{{item.name}}</td>
                                <td>{{item.identifier}}</td>
                                <td>{{item.description}}</td>
                                <td><a href="<%=contentPath%>/welcome/{{item.identifier}}/apps/fbs" target="_blank"><%=contentPath%>/welcome/{{item.identifier}}/apps/fbs</a></td>
                                <td class="text-nowrap" width="80px;">
                                    <button class="btn btn-xs btn-info" ng-click="selectNode(item);$event.stopPropagation();">View</button>
                                    <button class="btn btn-xs btn-danger" ng-click="deleteTenant(item);$event.stopPropagation();">Delete</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="tenantList.totalCount > tenantListPageSize">
                        <ul class="uibPagination" uib-pagination total-items="tenantList.totalCount" items-per-page="fileListPageSize" ng-model="$parent.$parent.tenantListCurrentPage"
                            max-size="10" boundary-links="true" rotate="false" ng-change="changeTenantListPage()"></ul>
                    </div>
                </div>
            </div>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" ng-show="selectedNodeType == Tenant" style="height: 100%;">
                <div class="card-header" style="min-height: 46px;">
                    <table width="100%">
                        <tr>
                            <td>
                                <h3 class="card-title">{{selectedNode.name}}
                                    <span class="badge badge-pill badge-secondary font-10" ng-if="selectedNode.id">ID: {{selectedNode.id}}</span>
                                </h3>
                            </td>
                            <td class="text-nowrap; text-right">
                                <button class="btn btn-warning btn-xs" ng-click="makeDefaultTenant(selectedNode);$event.stopPropagation();" ng-show="defaultTenant.id !== selectedNode.id">Make Default Tenant</button>
                                <button class="btn btn-info btn-xs" ng-click="modifyTenant(selectedNode);$event.stopPropagation();">Modify Tenant</button>
                                <button class="btn btn-danger btn-xs" ng-click="deleteTenant(selectedNode);$event.stopPropagation();">Delete Tenant</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div class="form-group col-md-12">
                        <span ng-if="defaultTenant.id === selectedNode.id" class="badge-pill badge-success">This is default tenant</span>
                        <span ng-if="defaultTenant.id !== selectedNode.id" class="badge-pill badge-secondary">This is not default tenant</span>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="name">Name</label>
                        <div id="name" class="alert alert-secondary">
                            {{selectedNode.name}}
                        </div>
                    </div>
                    <div class="form-group sm col-md-12">
                        <label for="identifier">Identifier</label>
                        <div id="identifier" class="alert alert-secondary">
                            {{selectedNode.identifier}}
                        </div>
                    </div>
                    <div class="form-group sm col-md-12">
                        <label for="identifier">AppDev URL</label>
                        <div id="appDevUrl" class="alert alert-secondary">
                            <a href="<%=contentPath%>/welcome/{{selectedNode.identifier}}/apps/fbs" target="_blank"><%=contentPath%>/welcome/{{selectedNode.identifier}}/apps/fbs</a>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="desc">Description</label>
                        <div id="desc" class="alert alert-secondary">
                            {{selectedNode.description}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card text-<%=textColor%> bg-secondary">
                            <div class="card-header" data-toggle="collapse" data-target="#bfServerPanel" style="padding-bottom: 0">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <h6>BizFlow Server</h6>
                                        </td>
                                        <td style="text-align: right; padding-bottom: 5px;">
                                            <button class="btn btn-primary btn-xs" ng-if="!bfServerList || bfServerList.length < 1" ng-click="createBizFlowServer();$event.stopPropagation();">Add</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="bfServerPanel" class="collapse show panel-body">
                                <div ng-if="bfServerList.length == 0" class="bg-<%=bgColor%>">
                                    <div class="text-center">There are no BizFlow Servers</div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%> bg-<%=bgColor%>" ng-if="bfServerList.length > 0">
                                        <thead>
                                        <tr style="font-weight: bold">
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Host</th>
                                            <th>Port</th>
                                            <th>Description</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="item in bfServerList" title="ID={{item.id}}" id="{{item.id}}">
                                            <td>{{item.ROW_NUMBER}}</td>
                                            <td>{{item.name}}</td>
                                            <td>{{item.host}}</td>
                                            <td>{{item.port}}</td>
                                            <td>{{item.description}}</td>
                                            <td class="text-nowrap" width="80px;">
                                                <button class="btn btn-xs2 btn-info" ng-click="modifyBizFlowServer(item);$event.stopPropagation();">Edit</button>
                                                <button class="btn btn-xs2 btn-danger" ng-click="deleteBizFlowServer(item);$event.stopPropagation();">Delete</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="padding: 10px;"></div>
                    <div class="col-md-12">
                        <div class="card text-<%=textColor%> bg-secondary">
                            <div class="card-header" data-toggle="collapse" data-target="#databasePanel" style="padding-bottom: 0">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <h6>Database Server</h6>
                                        </td>
                                        <td style="text-align: right; padding-bottom: 5px;">
                                            <button class="btn btn-primary btn-xs" ng-if="!databaseList || databaseList.length < 1" ng-click="createDatabase();$event.stopPropagation();">Add</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="databasePanel" class="collapse show panel-body">
                                <div ng-if="databaseList.length == 0" class="bg-<%=bgColor%>">
                                    <div class="text-center">There are no database servers</div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%> bg-<%=bgColor%>" ng-if="databaseList.length > 0">
                                        <thead>
                                        <tr style="font-weight: bold">
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Host</th>
                                            <th>Port</th>
                                            <th>Type</th>
                                            <th>Description</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="item in databaseList" title="ID={{item.id}}" id="{{item.id}}">
                                            <td>{{item.ROW_NUMBER}}</td>
                                            <td>{{item.name}}</td>
                                            <td>{{item.host}}</td>
                                            <td>{{item.port}}</td>
                                            <td>{{item.sourceType}}</td>
                                            <td>{{item.description}}</td>
                                            <td class="text-nowrap" width="80px;">
                                                <button class="btn btn-xs2 btn-info" ng-click="modifyDatabase(item);$event.stopPropagation();">Edit</button>
                                                <button class="btn btn-xs2 btn-danger" ng-click="deleteDatabase(item);$event.stopPropagation();">Delete</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>