<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.file.util.FileIOUtil" %>
<%@ page import="com.bizflow.io.core.file.util.FileUtil" %>
<%@ page import="com.bizflow.io.core.security.util.SecurityEncryptUtil" %>
<%@ page import="com.bizflow.io.core.util.LinkedProperties" %>
<%@ page import="com.bizflow.io.core.util.PropertyUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Properties" %>

<%!
    private Map<String, File> getPropertyFiles(ServletContext application, String subFolder) {
        Map<String, File> fileList = new LinkedHashMap();
        File webInfDir = new File(application.getRealPath(subFolder));
        File[] listFiles = webInfDir.listFiles();
        for (File file : listFiles) {
            if (file.getName().endsWith(".properties")) {
                fileList.put(subFolder + "/" + file.getName(), file);
            }
        }

        return fileList;
    }
%>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);

    File propertyFile = null;
    Map<String, File> propertyFileMap = getPropertyFiles(application, "/WEB-INF");
    propertyFileMap.putAll(getPropertyFiles(application, "/WEB-INF/classes"));
    String propertyFilename = ServletUtil.getParameterValue(request, "file", "/WEB-INF/core.properties");
    if (null != propertyFilename) {
        propertyFile = propertyFileMap.get(propertyFilename);
    }

    if (null == propertyFile) {
        Iterator<String> it = propertyFileMap.keySet().iterator();
        if (it.hasNext()) {
            propertyFilename = it.next();
            propertyFile = propertyFileMap.get(propertyFilename);
        }
    }

    int keyDepth = ServletUtil.getParameterIntValue(request, "groupLevel", -1);
    if (-1 == keyDepth) {
        keyDepth = 1;
        if ("/WEB-INF/log4j.properties".equalsIgnoreCase(propertyFilename)) {
            keyDepth = 3;
        } else if ("/WEB-INF/classes/log4j2.properties".equalsIgnoreCase(propertyFilename)) {
            keyDepth = 2;
        } else if ("/WEB-INF/classes/quartz.properties".equalsIgnoreCase(propertyFilename)) {
            keyDepth = 3;
        } else if ("/WEB-INF/classes/quartz_all.properties".equalsIgnoreCase(propertyFilename)) {
            keyDepth = 3;
        }
    }

    boolean saved = false;
    boolean undo = false;
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        Properties properties = new LinkedProperties();
        int lastParameterIndex = ServletUtil.getParameterIntValue(request, "#lastParameterIndex", 0);

        for (int i = 0; i <= lastParameterIndex; i++) {
            String name = request.getParameter("#" + i);
            if (name.startsWith("#newPropertyName@")) {
                String keyName = name.substring("#newPropertyName@".length());
                String newName = request.getParameter(name);
                if (null != newName && newName.trim().length() > 0) {
                    String newValue = request.getParameter("#newPropertyValue@" + keyName);
                    String encrypt = request.getParameter("#newPropertyEncrypt@" + keyName);
                    if ("yes".equalsIgnoreCase(encrypt)) {
                        newValue = SecurityEncryptUtil.encryptToBase64(newValue);
                    }
                    properties.setProperty(newName, newValue);
                }
            } else if (!name.startsWith("#newPropertyValue@") && !name.endsWith("_{DELETE}")) {
                String delete = request.getParameter(name + "_{DELETE}");
                if (!"yes".equalsIgnoreCase(delete)) {
                    String value = request.getParameter(name);
                    String encrypt = request.getParameter(name + "_{ENCRYPT}");
                    if ("yes".equalsIgnoreCase(encrypt)) {
                        value = SecurityEncryptUtil.encryptToBase64(value);
                    }
                    properties.setProperty(name, value);
                }
            }
        }

        File bakFile = new File(propertyFile.getParent(), propertyFile.getName() + ".undo");
        FileIOUtil.copyTo(propertyFile, bakFile);

        FileOutputStream outputStream = new FileOutputStream(propertyFile);
        try {
            properties.store(outputStream, propertyFilename);
        } finally {
            outputStream.close();
        }
        saved = true;
    } else {
        String action = request.getParameter("action");
        if ("undo".equalsIgnoreCase(action)) {
            File bakFile = new File(propertyFile.getParent(), propertyFile.getName() + ".undo");
            if (bakFile.exists()) {
                FileIOUtil.copyTo(bakFile, propertyFile);
                undo = true;
            }
        }
    }

    File descFile = new File(propertyFile.getParentFile(), FileUtil.getFilenameOnly(propertyFile.getName()) + ".descriptions." + FileUtil.getFileExtension(propertyFile.getName()));
    Properties descProperties = new Properties();
    if (null != descFile && descFile.exists()) {
        descProperties.load(new FileReader(descFile));
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Configuration Management</title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <style>
        td {
            border: 0 !important;
        }

        .container {
            padding: 0 !important;
        }

        .table {
            margin-bottom: 0px;
        }
    </style>
    <script>
        function undone() {
            alert("<%=Encode.forJavaScriptAttribute(propertyFilename)%> has been undone successfully.");
            this.location.href = "configuration-manager.jsp?file=<%=Encode.forUriComponent(propertyFilename)%>";
        }

        function changeFile() {
            var keyDepth = 1;
            var file = document.forms[0].file.options[document.forms[0].file.selectedIndex].value;
            if ("/WEB-INF/log4j.properties" == file) {
                keyDepth = 3;
            } else if ("/WEB-INF/log4j2.properties" == file) {
                keyDepth = 2;
            } else if ("/WEB-INF/classes/quartz.properties" == file) {
                keyDepth = 3;
            } else if ("/WEB-INF/classes/quartz_all.properties" == file) {
                keyDepth = 3;
            }

            document.forms[0].groupLevel.selectedIndex = (keyDepth - 1);
        }

        function executeAction(action) {
            this.location.href = "configuration-manager.jsp?file=" + document.forms[0].file.options[document.forms[0].file.selectedIndex].value + "&action=" + action
                + "&groupLevel=" + document.forms[0].groupLevel.options[document.forms[0].groupLevel.selectedIndex].value;
        }

        function apply() {
            var hiddenGround = document.getElementById('hiddenGround');
            hiddenGround.src = 'reload-config.jsp?service=<%=Encode.forUriComponent(propertyFilename)%>';
        }

        function reloadDone() {
            var saveAlert = document.getElementById("saveAlert");
            if (saveAlert) {
                saveAlert.style.display = 'none';
            }
            document.getElementById("applyAlert").style.display = '';
        }

        function applyOk() {
            document.getElementById("applyAlert").style.display = 'none';
        }

        function page_onload() {
            <% if(undo) {%>
            undone();
            <%}%>
        }
    </script>
</head>
<body onload="page_onload();">
<div class="page-header" style="text-align: center;">
    <h1>Configuration Management</h1>
</div>
<div class="container-fluid">
    <% if (saved) {%>
    <div class="alert alert-success alert-dismissible" role="alert" id="saveAlert">
        <table style="width: 100%;">
            <tr>
                <td>
                    <%=Encode.forHtmlContent(propertyFilename)%> has been saved successfully.
                </td>
                <td style="text-align: right;">
                    <input type="button" class="btn btn-warning btn-xs" value="Undo" onclick="executeAction('undo');" style="width: 100px;">
                    <input type="button" class="btn btn-danger btn-xs" value="Apply" onclick="apply();" style="width: 100px;">
                </td>
                <td>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </td>
            </tr>
        </table>
    </div>
    <%}%>
    <div class="alert alert-success" role="alert" id="applyAlert" style="display: none;">
        <%=Encode.forHtmlContent(propertyFilename)%> has been applied successfully.
        <button type="button" class="close" aria-label="Close" onclick="applyOk()"><span aria-hidden="true">&times;</span></button>
    </div>
    <form method="post">
        <div class="well">
            <select name="file" style="width:400px;height: 30px;color:black;" onchange="changeFile();executeAction('edit');">
                <%
                    Iterator<String> it = propertyFileMap.keySet().iterator();
                    while (it.hasNext()) {
                        String name = it.next();
                        String selected = name.equals(propertyFilename) ? "selected" : "";
                %>
                <option value="<%=name%>" <%=selected%>><%=name%>
                </option>
                <%
                    }
                %>
            </select>
            &nbsp;&nbsp;
            Group Level:
            <select name="groupLevel">
                <%for (int gl = 1; gl < 10; gl++) {%>
                <option value="<%=gl%>" <%=gl == keyDepth ? "selected" : ""%>><%=gl%>
                </option>
                <%}%>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" class="btn btn-default btn-xs" value="Edit" onclick="executeAction('edit');" style="width: 100px;">&nbsp;&nbsp;
            <input type="button" class="btn btn-danger btn-xs" value="Apply" onclick="apply();" style="width: 100px;">

        </div>

        <div class="panel panel-primary">
            <div class="panel-heading" data-toggle="collapse" data-target="#NewProperty">
                <table style="width:100%;">
                    <tr>
                        <td nowrap>
                            <h3 class="panel-title">New Property</h3>
                        </td>
                        <td width="95%" style="text-align: right;">
                            <button class="btn btn-default btn-xs" type="submit" value="Save" style="width: 100px;" accesskey="s">Save</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="NewProperty" class="collapse in">
                <div class="panel-body">
                    Enter a new property name and value to be added.
                </div>
                <table class="table table-condensed">
                    <tr>
                        <td style="width:49%;white-space: nowrap;">
                            <input type="hidden" name="#0" value="#newPropertyName@_NEW">
                            <input type="text" name="#newPropertyName@_NEW" style="width:100%;" placeholder="Enter the name of new property" class="max-width">
                        </td>
                        <td style="width:49%"><input type="text" name="#newPropertyValue@_NEW" value="" placeholder="Enter the value of new property" class="max-width"></td>
                        <td nowrap>
                            Encrypt? <input type="checkbox" name="#newPropertyEncrypt@_NEW" value="yes">
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <%
            int keyIndex = 0;
            int parameterIndex = 1;
            Map<String, Map> map = PropertyUtil.getPropertyKeyMap(propertyFile, keyDepth);

            for (String keyName : map.keySet()) {
                keyIndex++;
        %>
        <div class="panel panel-primary">
            <div class="panel-heading" data-toggle="collapse" data-target="#panel<%=keyIndex%>">
                <div class="row">
                    <div class="col-md-10"><h3 class="panel-title"><%=keyName%>
                    </h3></div>
                    <div class="col-md-2 text-right">
                        <button class="btn btn-default btn-xs" type="submit" value="Save" style="width: 100px;" accesskey="s">Save</button>
                    </div>
                </div>
            </div>
            <div id="panel<%=keyIndex%>" class="collapse in">
                <table class="table table-striped table-condensed">
                    <%
                        Map<String, String> keyMap = map.get(keyName);
                        for (String name : keyMap.keySet()) {
                            String value = keyMap.get(name);
                            String desc = descProperties.getProperty(name);
                            if (null == desc) {
                                desc = "";
                            }
                    %>
                    <tr>
                        <td class="property" style="width: 100px;white-space: nowrap;">
                            <input type="hidden" name="#<%=parameterIndex++%>" value="<%=name%>">
                            <label id="<%=name%>"><%=name%>
                            </label>
                        </td>
                        <td class="property" width="50%"><input type="text" name="<%=name%>" value="<%=HtmlUtil.getHtmlInput(value)%>" style="width:100%;"></td>
                        <td class="description"><%=desc%>
                        </td>
                        <td class="property" style="width:60px;">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><span class="action">Encrypt?&nbsp;</span></td>
                                    <td><input type="checkbox" name="<%=name%>_{ENCRYPT}" value="yes"></td>
                                    <td style="width: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                    <td style="width: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><span class="action">Delete?&nbsp;</span></td>
                                    <td><input type="checkbox" name="<%=name%>_{DELETE}" value="yes"></td>
                                    <td style="width: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
            </div>
        </div>
        <%
            }
        %>
        <input type="hidden" name="#lastParameterIndex" value="<%=parameterIndex-1%>">
    </form>
</div>
<iframe name="hiddenGround" id="hiddenGround" src="../blank.html" frameborder="0" width="5" height="5" style="visibility: hidden;"></iframe>
<%@include file="../../../copyright.html" %>
</body>
</html>