var app = angular.module('angularApp', [
    'blockUI'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        return true;
    };
}).controller('angularAppCtrl', ['$scope', 'blockUI', function ($scope, blockUI) {
    var blockUIStarted = false;

    $scope.startBlockUI = function (msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    $scope.stopBlockUI = function () {
        $scope.$apply(function () {
            blockUI.stop();
            blockUIStarted = false;
        });
    }


    $scope.reloadConf = function (service, serviceTitle) {
        $scope.startBlockUI("Reloading...");
        reloadConf(service, serviceTitle);
    }
}]);

