function reloadConf(service, serviceTitle, baseUrl, param) {
    hideAlert();
    document.getElementById("serviceName").innerHTML = serviceTitle || service;
    document.getElementById('hiddenGround').src = (baseUrl || '') + 'reload-config.jsp?service=' + service + (param ? '&' + param : '');
}

function clearWSCache() {
    hideAlert();
    document.getElementById("serviceName").innerHTML = "Clearing Web Service cache";
    document.getElementById('hiddenGround').src = contextPath + '/services/ws/-/ClearCache';
}

function reloadDone() {
    document.getElementById("applyAlert").style.display = '';
    try {
        angularExt.getAngularScope("body").stopBlockUI();
    } catch (e) {
    }
    try {
        reloadPage();
    } catch (e) {
    }
}

function reloadError() {
    try {
        angularExt.getAngularScope("body").stopBlockUI();
    } catch (e) {
    }
}

function hideAlert() {
    try {
        document.getElementById("applyAlert").style.display = 'none';
        document.getElementById("accessDenied").style.display = 'none';
    } catch (e) {
    }
}

function applyOk() {
    hideAlert();
    try {
        applyPage();
    } catch (e) {
    }
}

function accessDenied() {
    document.getElementById("accessDenied").style.display = '';
}
