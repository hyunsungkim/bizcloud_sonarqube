<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.db.mybatis.session.SqlSessionFactoryManager" %>
<%@ page import="com.bizflow.io.core.expression.ExpressionMethodInvoker" %>
<%@ page import="com.bizflow.io.core.io.util.ResourceManager" %>
<%@ page import="com.bizflow.io.services.ais.invoke.ApiInvoker" %>
<%@ page import="com.bizflow.io.services.bizflow.util.BizFlowServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.core.acm.auth.AuthCredentialKeyCheckerInvoker" %>
<%@ page import="com.bizflow.io.services.core.message.I18NMessageManager" %>
<%@ page import="com.bizflow.io.services.core.message.util.TemplateUtil" %>
<%@ page import="com.bizflow.io.services.core.service.CoreService" %>
<%@ page import="com.bizflow.io.services.core.servlet.InitializeServiceServlet" %>
<%@ page import="com.bizflow.io.services.core.system.SystemServerManager" %>
<%@ page import="com.bizflow.io.services.custom.util.CustomServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.data.processor.DataProcessorInvoker" %>
<%@ page import="com.bizflow.io.services.data.util.DataServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.data.util.DataSessionFactoryUtil" %>
<%@ page import="com.bizflow.io.services.file.session.FileServiceSessionBuilder" %>
<%@ page import="com.bizflow.io.services.file.util.FileServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.form.util.FormServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.message.util.MessageServiceConfigUtil" %>
<%@ page import="com.bizflow.io.services.ws.util.WSServiceConfigUtil" %>
<%@ page import="org.apache.logging.log4j.LogManager" %>
<%@ page import="org.apache.logging.log4j.Logger" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.services.core.util.ClearUtil" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceUtil" %>
<%
    Logger logger = LogManager.getLogger(CoreService.class);
    String service = request.getParameter("service");
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <script>
        var loaded = false;
        var error = false;

        function page_onload() {
            try {
                if (parent) {
                    if (loaded) {
                        parent.reloadDone();
                    } else {
                        if (!error) {
                            alert("Failed to reload <%=Encode.forJavaScriptAttribute(service)%>");
                        }
                        parent.reloadError();
                    }
                }
            } catch (e) {
            }
        }
    </script>
</head>
<body onload="page_onload();">
<%
    String errorMsg = null;
    try {
        if ("Core".equalsIgnoreCase(service) || "/WEB-INF/core.properties".equalsIgnoreCase(service)) {
            CoreServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading Core Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("File".equalsIgnoreCase(service) || "/WEB-INF/file.properties".equalsIgnoreCase(service)) {
            FileServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading File Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("Data".equalsIgnoreCase(service) || "DB".equalsIgnoreCase(service) || "/WEB-INF/data.properties".equalsIgnoreCase(service)) {
            DataServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading DB Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("BizFlow".equalsIgnoreCase(service) || "/WEB-INF/bizflow.properties".equalsIgnoreCase(service)) {
            BizFlowServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading BizFlow Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("WS".equalsIgnoreCase(service) || "/WEB-INF/ws.properties".equalsIgnoreCase(service)) {
            WSServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading WS Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("MyBatis".equalsIgnoreCase(service)) {
            SqlSessionFactoryManager.getInstance().rebuildExistingSqlSessionFactories();
            logger.info("Reloading MyBatis Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("Moon-DB-MyBatis".equalsIgnoreCase(service)) {
            DataServiceConfigUtil.getConfiguration().reload();
            DataSessionFactoryUtil.rebuildSqlSessionFactory();
            logger.info("Reloading MyBatis SQL Session Factory is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("Moons".equalsIgnoreCase(service)) {
            String moon = request.getParameter("moon");
            if (null != moon) DataSessionFactoryUtil.rebuildMoonSqlSessionFactories(moon);
            else DataSessionFactoryUtil.rebuildSqlSessionFactories();
            logger.info("Reloading Moons is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("Message".equalsIgnoreCase(service) || "/WEB-INF/message.properties".equalsIgnoreCase(service)) {
            MessageServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading Message Service Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("Custom".equalsIgnoreCase(service) || "/WEB-INF/custom.properties".equalsIgnoreCase(service)) {
            CustomServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading Custom Service Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("Form".equalsIgnoreCase(service) || "/WEB-INF/form.properties".equalsIgnoreCase(service)) {
            FormServiceConfigUtil.getConfiguration().reload();
            logger.info("Reloading Form Service Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("log4j".equalsIgnoreCase(service) || "/WEB-INF/classes/log4j2.properties".equalsIgnoreCase(service)) {
            InitializeServiceServlet.reloadLog4j();
            logger.info("Reloading log4j Configuration is done successfully");
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("MyBatis-Cache".equalsIgnoreCase(service)) {
            String sessionName = request.getParameter("sessionName");
            if (null != sessionName) {
                SqlSessionFactoryManager.getInstance().clearSqlSessionCaches(sessionName);
            } else {
                SqlSessionFactoryManager.getInstance().clearAllSqlSessionCaches();
            }
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("File-Service".equalsIgnoreCase(service)) {
            FileServiceSessionBuilder.getInstance().buildServiceSessions();
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("ClearClassCache".equalsIgnoreCase(service)) {
            ClearUtil.clearClassCache();
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("ClearTemplateCache".equalsIgnoreCase(service)) {
            ResourceManager.getInstance().clear();
            TemplateUtil.clearTemplates();
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("LoadSystemMessage".equalsIgnoreCase(service)) {
            I18NMessageManager.getInstance().loadSystemMessages();
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("LoadMoonMessage".equalsIgnoreCase(service)) {
            I18NMessageManager.getInstance().loadMoonMessages();
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        } else if ("RegisterServer".equalsIgnoreCase(service)) {
            SystemServerManager.getInstance().initialize();
            out.println("Done");
            out.println("<script>loaded=true;</script>");
        }
    } catch (Throwable t) {
        errorMsg = ExceptionUtil.getOriginalExceptionString(t);
        logger.error(t);
    }
%>
<% if (null != errorMsg) { %>
<script>
    error = true;
    alert("<%=Encode.forJavaScript(errorMsg)%>")
</script>
<%}%>
</body>
</html>
