<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Reload Configuration</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/fontawesome-free/css/all.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("reload-config.js");
        mars$require$.script("reload-config-menu.js");
    </script>

    <script>
        var contextPath = "<%=request.getContextPath()%>";
    </script>
    <style>
        .card-header h5 {
            margin: 0;
        }

        button.list-group-item {
            text-align: left;
        }

        button span {
            color: #6c757d;
            font-size: 0.8rem;
        }
    </style>
</head>

<body ng-controller="angularAppCtrl" onload="mars$util$.resizeFrameWindowHeight()" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="page-header text-center" style="padding: 15px 0 10px 0;">
    <h1>Reload Configuration</h1>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md">
            <div class="alert alert-success" role="alert" id="applyAlert" style="display: none;">
                <span id="serviceName"></span> is done successfully.
                <button type="button" class="close" aria-label="Close" onclick="applyOk()"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="alert alert-danger" role="alert" id="accessDenied" style="display: none;">
                <h1>Access Denied: <%=ServletUtil.getClientIPAddress(request)%>
                </h1>Credential is not initialized or your session was expired or you do not have a permission to access
                <br>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#confMenu" aria-expanded="false" aria-controls="confMenu"><h5>Configurations</h5></div>
                <div class="card-block collapse show" id="confMenu">
                    <div class="list-group list-group-flush">
                        <button type="button" class="list-group-item" ng-click="reloadConf('Core', 'Reloading Core Configuration')"><strong>Core Configuration</strong><span> - reload core.properties</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('DB', 'Reloading Data Configuration')"><strong>DB Configuration</strong><span> - reload data.properties</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('File', 'Reloading File Configuration')"><strong>File Configuration</strong><span> - reload file.properties</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('WS', 'Reloading Web Service Configuration')"><strong>Web Service Configuration</strong><span> - reload ws.properties</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('Message', 'Reloading Message Configuration')"><strong>Message Configuration</strong><span> - reload message.properties</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('Custom', 'Reloading Custom Configuration')"><strong>Custom Configuration</strong><span> - reload custom.properties</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('log4j', 'Reloading log4j Configuration')"><strong>LOG4J Configuration</strong><span> - reload lo4j.properties</span></button>
                    </div>
                </div>
            </div>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#perform" aria-expanded="false" aria-controls="message"><h5>Message</h5></div>
                <div class="card-block collapse show" id="message">
                    <div class="list-group list-group-flush">
                        <button type="button" class="list-group-item" ng-click="reloadConf('LoadSystemMessage','Load System Message')"><strong>System Message</strong><span> - reload all system messages</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('LoadMoonMessage','Load Moon Message')"><strong>Application Message</strong><span> - reload all application messages</span></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#sysMgmt" aria-expanded="false" aria-controls="sysMgmt"><h5>Services</h5></div>
                <div class="card-block collapse show" id="sysMgmt">
                    <div class="list-group list-group-flush">
                        <button type="button" class="list-group-item" ng-click="reloadConf('Moons', 'Reloading Applications')"><strong>Application</strong><span> - reload applications</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('MyBatis', 'Reloading Data Service Configuration')"><strong>Data Service</strong><span> - reload existing Data Service Configurations</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('Moon-DB-MyBatis', 'Reloading all applications, DB and Data Service Configuration')">
                            <strong>Application & Data Services</strong><span> - reload all applications, DB and Data Service Configurations like web server is restarted.</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('File-Service','Reloading File Service')"><strong>File Service</strong><span> - reload File service options and virtual services</span></button>
                    </div>
                </div>
            </div>
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#perform" aria-expanded="false" aria-controls="perform"><h5>Cache</h5></div>
                <div class="card-block collapse show" id="perform">
                    <div class="list-group list-group-flush">
                        <button type="button" class="list-group-item" ng-click="reloadConf('MyBatis-Cache','Cleaning Data Service caches')"><strong>Data Service Caches</strong><span> - clear all Data Service caches</span></button>
                        <button type="button" class="list-group-item" onclick="clearWSCache();"><strong>Web Service Cache</strong><span> - clear Web Service caches such as request and response xml</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('ClearClassCache', 'Clearing resource caches');"><strong>Clear Class Cache</strong><span> - clear API Invoker, Data Processor, Expression Method classes</span></button>
                        <button type="button" class="list-group-item" ng-click="reloadConf('ClearTemplateCache', 'Clear template caches')"><strong>Clear Templates Cache</strong><span> - Clear templates and resources loaded from class path</span></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-1"></div>
    </div>
</div>
<iframe name="hiddenGround" id="hiddenGround" src="../blank.html" frameborder="0" width="5" height="5" style="visibility: hidden;"></iframe>
<br><br>
<%@include file="../../../copyright.html" %>
</body>
</html>