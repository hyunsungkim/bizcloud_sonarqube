<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="java.io.File" %>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
    String rootLog = request.getContextPath().substring(1) + "-root.log";
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>View Log</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <script>
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("view-logs.js");
    </script>
    <style>
        select, input, .button {
            font-size: 14px;
        }
    </style>
</head>
<body onload="_onload();">
<div id="logNameBox" class="fixedElement" style="height: 40px;border: 1px solid #202020;vertical-align: middle;">
    <form target="tailLogFrame" action="tail-log.jsp">
        <table width="100%">
            <tr>
                <td>
                    <span style="font-size: small; padding-left: 8px; padding-bottom: 5px;">
                        <b>Log Directory:</b> <i><%=CoreServiceConfigUtil.getLogDir()%></i>
                    </span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <table id="logNameBoxTable" cellspacing="0" cellpadding="0">
                        <tr>
                            <td nowrap>
                                <select name="logName" style="color:black;">
                                    <%
                                        File logDir = new File(CoreServiceConfigUtil.getLogDir());
                                        File[] files = logDir.listFiles();
                                        for (int i = 0; i < files.length; i++) {
                                            File _file = files[i];
                                            if (_file.isFile()) {
                                                String fileName = _file.getName();
                                                if (fileName.endsWith(".log")) {
                                    %>
                                    <option value="<%=fileName%>" <%=fileName.equalsIgnoreCase(rootLog) ? "selected" : ""%>><%=fileName%>
                                    </option>
                                    <%
                                                }
                                            }
                                        }
                                    %>
                                </select>
                            </td>
                            <td nowrap>
                                <select name="mode" style="color:black;">
                                    <option value="tail">Tail Mode</option>
                                    <option value="view">View Mode</option>
                                </select>
                            </td>
                            <td nowrap>
                                <select name="style" style="color:black;">
                                    <option value="admin">admin</option>
                                    <option value="cerulean">cerulean</option>
                                    <option value="cosmo">cosmo</option>
                                    <option value="cyborg" selected>cyborg</option>
                                    <option value="darkly">darkly</option>
                                    <option value="flatly">flatly</option>
                                    <option value="journal">journal</option>
                                    <option value="lumen">lumen</option>
                                    <option value="readable">readable</option>
                                    <option value="sandstone">sandstone</option>
                                    <option value="simplex">simplex</option>
                                    <option value="slate">slate</option>
                                    <option value="spacelab">spacelab</option>
                                    <option value="superhero">superhero</option>
                                    <option value="united">united</option>
                                    <option value="yeti">yeti</option>
                                </select>
                            </td>
                            <td nowrap="">
                                <select name="logLevel" style="color:black;">
                                    <option value="(PERFORMANCE |ERROR |WARN |INFO |DEBUG )" selected>All</option>
                                    <option value="(PERFORMANCE |ERROR |WARN |INFO )">Above INFO</option>
                                    <option value="(PERFORMANCE |ERROR |WARN )">Above WARN</option>
                                    <option value="(ERROR )">ERROR only</option>
                                    <option value="(PERFORMANCE )">PERFORMANCE only</option>
                                    <option value="(WARN )">WARN only</option>
                                    <option value="(INFO )">INFO only</option>
                                    <option value="(DEBUG )">DEBUG only</option>
                                </select>
                            </td>
                            <td nowrap style="width: 90%">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td nowrap="true">&nbsp;filter:</td>
                                        <td width="100%">
                                            <input type="text" id="filter" name="filter" value="" style="width: 100%;" placeholder="Enter regular expression to filter, for example, ^((?!Job).)*$">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td nowrap>
                                <input class="button" type="button" value="Go" onclick="viewLog();">
                                <input class="button" type="button" value="Stop Tail" onclick="stopTail();">
                                <input class="button" type="button" value="Start Tail" onclick="startTail();">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>
<iframe id="tailLogFrame" name="tailLogFrame" src="tail-log.jsp?logName=<%=rootLog%>" style="width: 100%;height: 100%;border: 0;padding: 0;margin: 0;" frameborder="0"></iframe>
<script>
    _onload();
</script>
</body>
</html>