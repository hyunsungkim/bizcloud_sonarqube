var _timer = null;
var gotoScrollBottom = true;

function setTimer() {
    _timer = setTimeout("setScrollBottom()", 1000);
}

function setScrollBottom() {
    if (null != _timer) {
        clearTimeout(_timer);
        _timer = null;
    }

    try {
        document.body.scrollTop = document.body.scrollHeight;
        window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    }
    catch (e) {
    }

    if (gotoScrollBottom) {
        setTimer();
    }
}

function stopTail() {
    if (null != _timer) {
        clearTimeout(_timer);
        _timer = null;
    }

    gotoScrollBottom = false;
}

function startTail() {
    gotoScrollBottom = true;
    setTimer();
}
