<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.io.util.IOResourceUtil" %>
<%@ page import="com.bizflow.io.core.net.util.MimeUtil" %>
<%@ page import="com.bizflow.io.core.util.DateUtil" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.OutputStream" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.zip.ZipEntry" %>
<%@ page import="java.util.zip.ZipOutputStream" %>
<%
    File logDir = new File(CoreServiceConfigUtil.getLogDir());

    String zipFileName = "LOG" + DateUtil.convert(new Date(), "yyyyMMddHHmmss") + ".zip";

    StringBuilder builder = new StringBuilder();
    builder.append("attachment; ").append("filename=\"").append(zipFileName).append("\"");
    response.setContentType(MimeUtil.getContentType(zipFileName));
    response.setHeader("Content-Disposition", builder.toString());
    OutputStream os = response.getOutputStream();

    ZipOutputStream zout = new ZipOutputStream(os);
    try {
        File[] files = logDir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                String zipEntryName = file.getName();
                zout.putNextEntry(new ZipEntry(zipEntryName));
                IOResourceUtil.copy(file, zout);
                zout.closeEntry();
            }
        }
    } finally {
        zout.close();
    }
%>