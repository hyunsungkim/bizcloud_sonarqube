function _onload() {
    var logNameBox = document.getElementById("logNameBox");
    var height = document.body.clientHeight - logNameBox.offsetHeight - 10;
    var tailLogFrame = document.getElementById("tailLogFrame");
    tailLogFrame.style.height = height + "px";
}

function stopTail() {
    var tailLogFrame = document.getElementById("tailLogFrame");
    tailLogFrame.contentWindow.stopTail();
}

function startTail() {
    var tailLogFrame = document.getElementById("tailLogFrame");
    tailLogFrame.contentWindow.startTail();
}

function viewLog() {
    var form = document.forms[0];
    var logName = form.logName.options[form.logName.selectedIndex].value;
    var mode = form.mode.options[form.mode.selectedIndex].value;
    var style = form.style.options[form.style.selectedIndex].value;
    var logLevel = form.logLevel.options[form.logLevel.selectedIndex].value;
    var filter = form.filter.value;
    var url = "tail-log.jsp?logName=" + logName + "&mode=" + mode + "&style=" + style + "&logLevel=" + encodeURIComponent(logLevel) + "&filter=" + encodeURIComponent(filter);

    var frame = document.getElementById("tailLogFrame");
    frame.src = url;
}