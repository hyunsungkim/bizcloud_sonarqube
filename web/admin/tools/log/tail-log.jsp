<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.file.tail.FileTailer" %>
<%@ page import="com.bizflow.io.core.file.tail.FileTailerException" %>
<%@ page import="com.bizflow.io.core.file.tail.FileTailerListener" %>
<%@ page import="com.bizflow.io.core.io.OutputStreamWriterEx" %>
<%@ page import="com.bizflow.io.core.lang.util.StringReplacer" %>
<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.UnsupportedEncodingException" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page import="org.owasp.encoder.Encode" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);

    String contextPath = request.getContextPath();
    String logName = ServletUtil.getParameterValue(request, "logName", contextPath + "-root.log");
    File logFile = new File(CoreServiceConfigUtil.getLogDir(), logName);

    final String errorPattern = ServletUtil.getParameterValue(request, "errorPattern", "(PERFORMANCE |ERROR |WARN |INFO |DEBUG )");
    final String mode = ServletUtil.getParameterValue(request, "mode", "tail");
    final String style = ServletUtil.getParameterValue(request, "style", "cyborg");
    final String logLevel = ServletUtil.getParameterValue(request, "logLevel", "(PERFORMANCE |ERROR |WARN |INFO |DEBUG )");
    final String filter = ServletUtil.getParameterValue(request, "filter", null);

    final boolean tailMode = "tail".equalsIgnoreCase(mode);

    // Use OutputStream to catch org.apache.catalina.connector.ClientAbortException: java.io.IOException: Failed to send AJP message or ClientAbortException:  java.net.SocketException: Connection reset by peer: socket write error
    final OutputStreamWriterEx htmlWriter = new OutputStreamWriterEx(response.getOutputStream(), StandardCharsets.UTF_8);
    htmlWriter.write("<!DOCTYPE html><html><head>");
    htmlWriter.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
    htmlWriter.write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>");
    htmlWriter.write("<title>").write(Encode.forHtmlContent(logName)).write("</title>");
    htmlWriter.write("<link href=\"../../../includes/node_modules-ext/bootstrap/css/bootstrap." + Encode.forUriComponent(style) + ".min.css\" rel=\"stylesheet\" type=\"text/css\"/>");
    htmlWriter.write("<link href=\"tail-log.css\" rel=\"stylesheet\" type=\"text/css\"/>");
    htmlWriter.write("<script src=\"tail-log.js\"></script>");
    htmlWriter.write("<script>");
    if (tailMode) {
        htmlWriter.write("startTail();");
    }
    htmlWriter.write("</script>");
    htmlWriter.write("</head>");
    htmlWriter.write("<body>");
    htmlWriter.safelyFlush();

    final Map<String, String> classMap = new HashMap();
    classMap.put("performance", "alert alert-success log");
    classMap.put("error", "alert alert-danger log");
    classMap.put("warn", "alert alert-warning log");
    classMap.put("info", "alert alert-info log");
    classMap.put("debug", "alert alert-default log");

    if ((null != logFile && logFile.exists())) {
        final FileTailer tailer = new FileTailer(logFile, 500, !tailMode);
        tailer.setStartFileOffset(ServletUtil.getParameterIntValue(request, "offset", null != filter ? 1000000 : 200000));
        tailer.addFileTailerListener(new FileTailerListener() {
            Map<String, Integer> patternIndexMap = new HashMap();
            Pattern logPattern = Pattern.compile("(^PERFORMANCE |^ERROR |^WARN |^INFO |^DEBUG )");
            Pattern logLevelPattern = Pattern.compile(logLevel);
            Pattern filterPattern = StringUtil.isNotNullBlank(filter) ? Pattern.compile(filter) : null;
            Pattern pattern = Pattern.compile(errorPattern);
            boolean showNewLogs = false;
            boolean logLinePrint = false;
            int i = 0;

            public void processStartOfFile(File file) {
            }

            private void printLog(String line) throws IOException {
                boolean matched = false;
                String group = "";
                Integer patternIndex = null;

                try {
                    Matcher matcher = pattern.matcher(line);
                    matched = matcher.find();
                    group = matcher.group().trim();

                    patternIndex = patternIndexMap.get(group);
                    if (null == patternIndex) {
                        patternIndex = new Integer(0);
                        patternIndexMap.put(group, patternIndex);
                    }

                    patternIndex++;
                    patternIndexMap.put(group, patternIndex);
                } catch (Exception e) {
                    matched = false;
                    group = "";
                }

                htmlWriter.write("<div class='").write(matched ? classMap.get(group.toLowerCase().trim()) : (i++ % 2 == 0) ? "alert log" : "alert log").write("'>");
                htmlWriter.write("<span>");
                htmlWriter.write(StringReplacer.replace(HtmlUtil.getHtmlText(line), new String[]{"\t", " "}, new String[]{"&nbsp;&nbsp;&nbsp;&nbsp;", "&nbsp;"}));
                htmlWriter.write("</span>");
                htmlWriter.write("</div>");

                htmlWriter.safelyFlush();
            }

            public void processNewLine(String line) throws FileTailerException {
                if (line.length() > 0) {
                    try {
                        line = new String(line.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                        Matcher logMatcher = logPattern.matcher(line);
                        boolean logLine = logMatcher.find();

                        if (!logLine && logLinePrint) {
                            printLog(line);
                        } else {
                            logLinePrint = false;
                            boolean filterMatched = true;

                            if (null != filterPattern) {
                                Matcher matcher = filterPattern.matcher(line);
                                filterMatched = matcher.find();
                            }

                            if (filterMatched) {
                                boolean logLevelMatched = true;

                                if (null != logLevelPattern) {
                                    Matcher matcher = logLevelPattern.matcher(line);
                                    logLevelMatched = matcher.find();
                                }

                                if (logLevelMatched) {
                                    printLog(line);
                                    logLinePrint = true;
                                }
                            } else if (tailMode) {
                                setNextFile();
                            } else {
                                tailer.stopTailing();
                            }
                        }
                    } catch (UnsupportedEncodingException e) {
                        throw new FileTailerException(e);
                    } catch (IOException e) {
                        throw new FileTailerException(e);
                    }
                }
            }

            public void processEndOfFile(long filePointer) throws FileTailerException {
                if (filePointer == 0) {
                    try {
                        htmlWriter.write("");
                        htmlWriter.safelyFlush();
                    } catch (IOException e) {
                        throw new FileTailerException(e);
                    }
                }
                showNewLogs = true;
                if (!tailMode) {
                    tailer.stopTailing();
                }
            }

            private void setNextFile() {
            }

            public void reloadFile() throws FileTailerException {
                try {
                    htmlWriter.write("");
                    htmlWriter.safelyFlush();
                } catch (IOException e) {
                    throw new FileTailerException(e);
                }
            }
        });

        tailer.run();
    } else {
        htmlWriter.write("<div>");
        htmlWriter.write("Log file does not exist. path=").write(logFile.getCanonicalPath());
        htmlWriter.write("<div>");
        htmlWriter.safelyFlush();
    }
    htmlWriter.write("</body>");
    htmlWriter.write("</html>");
    htmlWriter.safelyFlush();
%>

