var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'blockUI'
    , 'ngSanitize'
]).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $filter, $uibModal, blockUI, AjaxService, marsContext) {
    $scope.model = {
        channel: "@NOTIFICATION"
    };

    $scope.message = {
        messageType: "System"
    };

    (function () {
    })();

    var blockUIStarted = false;

    function startBlockUI() {
        blockUI.start();
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function callApi(apiUrl, callback, param) {
        var api = new AjaxService(marsContext.contextPath + apiUrl);
        api.post({
            success: function (o) {
                callback(o);
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function sendMessage(model) {
        callApi('/services/msg/ws/' + encodeURIComponent(model.channel) + '.json',
            function (o) {
                console.log(o);
                stopBlockUI();
            }, JSON.parse(model.message));
    }

    $scope.sendMessage = function () {
        sendMessage($scope.model);
    };

    $scope.genMessage = function () {
        $scope.model.message = JSON.stringify($scope.message, null, 2);
    }
}]);
