<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Notification Manager</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/fontawesome-free/css/all.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("notification-manager.js");
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>Notification Manager</h1>
</div>
<%}%>

<div class="container" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col-md-12" style="padding:0 1px 0 0">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="leftPanelContainer">
                <div class="card-header card-header-sm bg-secondary" id="leftPanelHeader" ng-if="!singleMode">
                    <table width="100%">
                        <tr>
                            <td>
                                <h3>Notification</h3>
                            </td>
                            <td class="text-right" style="padding-bottom: 5px;">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card-block panelContainer" id="treeContainer">
                    <form name="notiForm">
                        <div class="panel panel-info" style="padding: 5px;">
                            <div class="row no-margin">
                                <div class="form-group form-group-sm col-md-6">
                                    <label for="message">Channel</label>
                                    <input name="channel" class="form-control form-control-xs" placeholder="Enter a channel name" ng-model="model.channel" style="width:100%;" required>
                                </div>
                                <div class="form-group form-group-sm col-md-6">
                                    <label for="messageType">Message Type</label>
                                    <select id="messageType" ng-model="message.messageType" class="form-control form-control-sm" ng-change="genMessage();">
                                        <option value="Alert">Alert</option>
                                        <option value="Broadcast">Broadcast</option>
                                        <option value="Chat">Chat</option>
                                        <option value="Notification">Notification</option>
                                        <option value="System">System</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm col-md-12">
                                <label for="message">Text Message</label>
                                <textarea ng-model="message.message" class="form-control form-control-sm" style="height: 200px;" id="message" placeholder="Enter a text message" ng-change="genMessage();" autofocus></textarea>
                            </div>
                            <div class="form-group form-group-sm col-md-12">
                                <label for="message">JSON Message</label>
                                <textarea name="message" class="form-control form-control-sm" placeholder="Enter a JSON message" ng-model="model.message" id="input" style="width: 100%;height:300px;" required></textarea>
                            </div>
                            <div class="text-center col-md-12">
                                <button class="btn btn-primary btn-xs" ng-disabled="notiForm.$invalid" ng-click="sendMessage();">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br>
<%@include file="../../../copyright.html" %>
</body>
</html>