var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'angucomplete-alt'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'ngSanitize'
    , 'blockUI'
    , 'angularFileUpload'
    , 'ui.select'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//) || config.url.match(/myTimeInfo.json/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', 'FileUploader', function ($scope, $filter, $uibModal, blockUI, AjaxService, marsContext, FileUploader) {
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;
    if ("undefined" != typeof appName) $scope.appName = appName;
    if ("undefined" != typeof baseLocation) $scope.baseLocation = baseLocation;
    if ("undefined" != typeof defaultLocation) $scope.defaultLocation = defaultLocation;
    if ("undefined" != typeof hideLocation) $scope.hideLocation = hideLocation;
    else $scope.hideLocation = false;
    if ("undefined" != typeof jsType) $scope.jsType = jsType;
    if ("undefined" != typeof distType) $scope.distType = distType;

    var blockUIStarted = false;

    function startBlockUI(msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function getAppNames() {
        var api = new AjaxService(marsContext.contextPath + '/services/moon/getMoonNames.json');
        api.get({
            success: function (o) {
                $scope.appNames = o.data;
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    function getDistTypeFilters() {
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/bio.distribution-GetDistributionTypes.json');
        api.get({
            success: function (o) {
                $scope.distTypeFilters = o.data;
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    function getDirList() {
        var param = $scope.baseLocation ? "?path=" + encodeURIComponent($scope.baseLocation) : "";
        var api = new AjaxService(marsContext.contextPath + '/services/util/directory/list.json' + param);
        api.get({
            success: function (o) {
                $scope.dirList = o.data;
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    var distTypes = [
        "APPLICATION",
        "COMPONENT",
        "CSS",
        "HTML",
        "IMAGE",
        "JS",
        "ZIP",
        "OTHER"
    ];

    var distMethods = [
        "COPY",
        "UNZIP",
        "UNZIP-DELETE",
        "DELETE"
    ];

    var distAutos = [
        "N",
        "Y"
    ];

    $scope.distListCurrentPage = 1;
    $scope.distListPageSize = 15;
    $scope.distListOrderColumn = 'appName, label, priority DESC, name, transactionId';
    $scope.distListOrderAsc = false;

    $scope.setPageSize = function (size) {
        $scope.distListPageSize = size;
    };

    function callDistributionListApi(param) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/run/bio.distribution-GetDistributionList@.json');
        api.post({
            success: function (o) {
                $scope.distList = o.data;
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function getDistributionList(pageNo, filter) {
        var param = {
            pageNo: pageNo || $scope.distListCurrentPage,
            pageSize: $scope.distListPageSize,
            ORDER_BY: $scope.distListOrderColumn + ' ' + ($scope.distListOrderAsc ? 'ASC' : 'DESC'),
            appName: $scope.appName
        };

        $scope.distListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        getDistTypeFilters();
        callDistributionListApi(param);
    }

    $scope.searchDistribution = function () {
        $scope.search.appName = $scope.search.appName === '' ? undefined : $scope.search.appName;
        $scope.search.LIKE_name_LIKE = $scope.search.LIKE_name_LIKE === '' ? undefined : $scope.search.LIKE_name_LIKE;
        $scope.search.label = $scope.search.label === '' ? undefined : $scope.search.label;
        $scope.search.type = $scope.search.type === '' ? undefined : $scope.search.type;

        getDistributionList(1, $scope.search);
    };

    $scope.changeDistributionListPage = function () {
        getDistributionList();
    };

    $scope.sortDistributionList = function (column) {
        if ($scope.distListOrderColumn === column) {
            $scope.distListOrderAsc = !$scope.distListOrderAsc;
        } else $scope.distListOrderAsc = true;
        $scope.distListOrderColumn = column;
        getDistributionList($scope.distListCurrentPage);
    };

    $scope.selectDistribution = function (file) {
    };

    $scope.getDistributionUrl = function (item, distName) {
        distName = mars$util$.addFileExtension(distName, item.extension);
        return marsContext.contextPath + "/services/file/get/bio.distribution-DownloadDistribution@/" + item.id + "/" + distName
            + "?etpl=/core/exception/ExceptionResponse.html";
    };

    $scope.refreshList = function () {
        getDistributionList(undefined, $scope.search);
    };

    function deleteDistribution(item) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/run/bio.distribution-DeleteDistribution@.json');
        api.post({
            success: function (o) {
                if ($scope.distList.data.length === 1 && $scope.distListCurrentPage > 1) {
                    $scope.distListCurrentPage -= 1;
                }
                $scope.refreshList();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, {
            id: item.id
        });
    }

    $scope.deleteDistribution = function (item) {
        bootbox.confirm({
            message: "Are you sure to delete the distribution '" + item.name + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteDistribution(item);
                }
            }
        });
    };

    function beforeUploadDistribution(item) {
        var param = {
            type: item.type,
            name: item.file.name,
            location: ($scope.baseLocation ? $scope.baseLocation : "") + item.location,
            appName: item.appName,
            auto: item.auto
        };

        if (item.type === 'APPLICATION') {
            param.location = '/apps/' + item.appName;
        }

        if (item.distMethod) {
            param.method = item.distMethod;
        }

        if (item.description) {
            param.description = item.description;
        }

        if (item.label) {
            param.label = item.label;
        }

        if (item.priority) {
            param.priority = item.priority;
        }

        if (item.id) {
            param.id = item.id;
        }

        var formData = [param];

        Array.prototype.push.apply(item.formData, formData);
    }

    function adjustAppName(appName) {
        return appName ? appName.replace(/\s/g, '').replace(/-/g, '').replace(/\./g, '') : appName;
    }

    $scope.uploadDistribution = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/distribution/distribution-upload.html'),
            size: 'xl',
            controller: ['$scope',
                function ($scope) {
                    var config = $scope.config = {
                        title: "Upload Distribution(s)",
                        multiple: true,
                        typeRequired: true
                    };

                    $scope.scope = scope;
                    $scope.baseUrl = location.protocol + "://" + location.host + marsContext.contextPath;
                    $scope.distAutos = distAutos;
                    $scope.distTypes = distTypes;
                    $scope.distMethods = distMethods;
                    $scope.locations = scope.dirList;
                    $scope.appNames = scope.appNames;
                    $scope.appName = scope.appName;
                    $scope.baseLocation = scope.baseLocation;
                    $scope.hideLocation = scope.hideLocation;

                    var uploader = $scope.uploader = new FileUploader({
                        url: marsContext.contextPath + '/services/file/run/bio.distribution-UploadDistribution@.json',
                        contentType: "multipart/form-data;charset=UTF-8"
                    });

                    uploader.onAfterAddingFile = function (item) {
                        try {
                            if ("undefined" != typeof scope.distType) {
                                item.type = "APPLICATION";
                            } else {
                                var mimeType = item.file.type;
                                if (mimeType.substring(0, 5) === "image") {
                                    item.type = "IMAGE";
                                } else if (mimeType === 'text/css') {
                                    item.type = "CSS";
                                } else if (mimeType === 'text/html') {
                                    item.type = "HTML";
                                } else if (mimeType === 'text/javascript') {
                                    item.type = scope.jsType || "JS";
                                } else if (mimeType === 'application/zip') {
                                    item.type = "ZIP";
                                } else {
                                    item.type = "OTHER";
                                }
                            }

                            if ("undefined" != typeof $scope.appName) {
                                item.appName = $scope.appName;
                            }

                            item.distMethod = "COPY";
                            item.location = scope.defaultLocation;
                        } catch (ignore) {
                        }
                    };

                    uploader.onBeforeUploadItem = function (item) {
                        beforeUploadDistribution(item);
                    };

                    function isErrorItems() {
                        for (var i = 0; i < uploader.queue.length; i++) {
                            var item = uploader.queue[i];
                            if (item.error) {
                                return true;
                            }
                        }

                        return false;
                    }

                    uploader.onErrorItem = function (fileItem, response, status, headers) {
                        fileItem.error = {
                            response: response,
                            status: status,
                            headers: headers
                        };

                        fileItem.error.msg = angularExt.makeAjaxExceptionString(mars$cipher$.decipher(response));
                    };

                    uploader.onCompleteAll = function () {
                        stopBlockUI();
                        if (!isErrorItems()) {
                            uploader.clearQueue();
                            $scope.$dismiss();
                            scope.refreshList();
                        }
                    };

                    var _method = null;

                    $scope.typeChanged = function (item) {
                        if (item.type === 'APPLICATION') {
                            _method = item.distMethod;
                            item.distMethod = 'UNZIP';
                        } else if (_method) {
                            item.distMethod = _method;
                        }
                    }

                    $scope.adjustAppName = function (item) {
                        item.appName = adjustAppName(item.appName);
                    }

                    $scope.searchStartsWith = function (actual, expected) {
                        var lowerStr = (actual + "").toLowerCase();
                        return lowerStr.indexOf(expected.toLowerCase()) === 0;
                    }

                    $scope.contextPath = marsContext.contextPath;

                    $scope.removeDistribution = function (fileItem) {
                        fileItem.remove();
                    };

                    $scope.isReadyToUpload = function () {
                        var ready = uploader.queue.length && uploader.queue.length > 0;

                        if (ready) {
                            for (var i = 0; i < uploader.queue.length; i++) {
                                var item = uploader.queue[i];
                                if ((config.typeRequired && angularExt.isInvalidObject(item.type))
                                    || (item.type !== 'APPLICATION' && angularExt.isInvalidObject(item.location))
                                    || angularExt.isInvalidObject(item.appName)
                                    || angularExt.isInvalidObject(item.auto)
                                    || item.error) {

                                    ready = false;
                                    break;
                                }
                            }
                        }

                        return ready;
                    };

                    $scope.upload = function () {
                        if (uploader.queue.filter(function (el) {
                            return el.type == undefined
                        }).length > 0) {
                            angularExt.getBootboxObject().alert("Please enter the file type of all files");
                        } else {
                            startBlockUI("It is being uploaded. It may take some time.");
                            uploader.uploadAll();
                        }
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close();
                    };
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    $scope.updateDistribution = function (item) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/distribution/distribution-update.html'),
            size: 'xl',
            controller: ['$scope',
                function ($scope) {
                    var config = $scope.config = {
                        title: "Update Distribution",
                        multiple: false,
                        typeRequired: true,
                        uploadUrl: marsContext.contextPath + '/services/file/run/bio.distribution-UpdateDistribution@.html?tpl=/core/ServiceResultCallback.html'
                    };

                    $scope.scope = scope;
                    var relativePath = false;
                    $scope.item = angular.copy(item);
                    if (scope.baseLocation) {
                        if (0 === $scope.item.location.indexOf(scope.baseLocation)) {
                            $scope.item.location = $scope.item.location.substring(scope.baseLocation.length);
                            relativePath = true;
                        }
                    }

                    $scope.baseUrl = location.protocol + "://" + location.host + marsContext.contextPath;
                    $scope.distAutos = distAutos;
                    $scope.distTypes = distTypes;
                    $scope.distMethods = distMethods;
                    $scope.locations = scope.dirList;
                    $scope.appNames = scope.appNames;
                    $scope.appName = scope.appName;
                    $scope.baseLocation = scope.baseLocation;
                    $scope.hideLocation = scope.hideLocation;

                    $scope.adjustAppName = function (_item) {
                        _item.appName = adjustAppName(_item.appName);
                    }

                    $scope.typeChanged = function (_item) {
                        if (item.type === 'APPLICATION') {
                            if (_item.type !== 'APPLICATION') {
                                _item.appName = undefined;
                                _item.location = undefined;
                            } else {
                                _item.appName = item.appName;
                                _item.location = item.location;
                            }
                        }
                    }

                    $scope.isReadyToUpload = function () {
                        return $scope.item.name && $scope.item.type && $scope.item.location && $scope.item.appName;
                    };

                    $scope.upload = function () {
                        mars$util$.createHiddenFrame(1);
                        $scope.uploading = true;
                        var form = document.forms[0];
                        if (item.type === 'APPLICATION') {
                            form.location.value = "/apps/" + $scope.item.appName;
                        } else {
                            form.location.value = ((relativePath || item.location !== $scope.item.location) && scope.baseLocation ? scope.baseLocation : "") + $scope.item.location;
                        }
                        form.action = config.uploadUrl;
                        startBlockUI('It is being uploaded. It may take some time.');
                        form.submit();
                    };

                    $scope.serviceExceptionCallback = function () {
                        stopBlockUI();
                        $scope.uploading = false;
                        $scope.cancel();
                    }

                    $scope.serviceResultCallback = function () {
                        scope.refreshList();
                        $scope.uploading = false;
                        $scope.cancel();
                        angularExt.getBootboxObject().alert("The distribution has been updated.");
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close();
                    };
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    function copyUrl(uri, mode) {
        if (mode === 'full') {
            mars$util$.copyToClipboard(mars$util$.getProtocolAndHost() + marsContext.contextPath + uri);
        } else {
            mars$util$.copyToClipboard(uri.substring(1));
        }
    }

    $scope.copyUrl = function (item, mode) {
        copyUrl(item.uri, mode);
    };

    function getEntityList(item, scope) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/getEntityList/bio.distribution-GetEntityList@/' + item.id + '.json');
        api.get({
            success: function (o) {
                scope.entityList = o.data;
                stopBlockUI();
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    $scope.showEntityList = function (item) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/distribution/distribution-entity-list.html'),
            size: 'lg',
            controller: ['$scope',
                function ($scope) {
                    var config = $scope.config = {
                        title: item.name + " Distribution Entity List"
                    };
                    $scope.scope = scope;
                    $scope.file = item;
                    $scope.textColor = textColor;

                    getEntityList(item, $scope);

                    $scope.getDistributionItemUrl = function (file, item) {
                        return "/services/file/get/bio.distribution-DownloadDistribution@/" + file.id + "/" + file.name + "/" + item.name.replace('/', '|').replace('\\', '|');
                    };

                    $scope.getDistributionUrl = function (file, item) {
                        return marsContext.contextPath + "/services/file/get/bio.distribution-DownloadDistribution@/" + file.id + "/" + file.name + "/" + item.name.replace('/', '|').replace('\\', '|')
                            + "?etpl=/core/exception/ExceptionResponse.html";
                    };

                    $scope.copyUrl = function (file, item, mode) {
                        copyUrl($scope.getDistributionItemUrl(file, item), mode);
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close();
                    };
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    $scope.deployDistribution = function (item) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/run/bio.distribution-Deploy@.json');
        api.post({
            success: function (o) {
                stopBlockUI();
                angularExt.getBootboxObject().alert("Deployment is complete.");
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, {
            id: item.id
        });
    }

    function getHistory(dist, callback) {
        $scope.distHistories = [];
        $scope.dist = dist;
        var param = {
            id: dist.id,
            appName: dist.appName
        };
        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/bio.distribution-GetComponentHistoryInfoList@.json');
        api.post({
            success: function (o) {
                $scope.distHistories = o.data.data;
                if (callback) {
                    callback();
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.showDistributionHistory = function(dist) {
        var callback = $scope.loadDistributionHistory;
        getHistory(dist, callback);
    };

    $scope.loadDistributionHistory = function () {
        var scope = $scope;
        var pageSize = 10;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/component/component-history.html'),
            windowClass: 'app-modal-window-11',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = "Component Versioning";
                    $scope.dist = scope.dist;
                    $scope.distHistoriesLeft = scope.distHistories;
                    $scope.distHistoriesRight = scope.distHistories;
                    $scope.selectedHistoryLeft = scope.distHistories[0];
                    $scope.selectedHistoryRight = scope.distHistories[0];
                    $scope.selectedNumLeft = 0;
                    $scope.selectedNumRight = -1;
                    $scope.contentLeft = {};
                    $scope.contentRight = {};
                    $scope.leftVersion = null;
                    $scope.rightVersion = null;

                    function getVersionContent(view, version){
                        var param ={
                            id: $scope.dist.id,
                            version: version
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/get/bio.distribution-GetSingleDistributionBase64Content.json');
                        api.post({
                            success: function (o) {
                                if(view==='L'){
                                    $scope.leftVersion = $scope.distInfoLeft.version;
                                    $scope.contentLeft.content = o.data[0].content;
                                    $scope.contentLeft.isChanged = true;
                                }else{
                                    $scope.rightVersion = $scope.distInfoRight.version;
                                    $scope.contentRight.content = o.data[0].content;
                                    $scope.contentRight.isChanged = true;
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                        resize();
                    }

                    function getHistoryContent(selectedData, selectedView, callback) {
                        $scope.contentLeft.isChanged = false;
                        $scope.contentRight.isChanged = false;
                        var param = {
                            id: selectedData.id,
                            appName: selectedData.appName,
                            transactionId: selectedData.transactionId
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/get/bio.distribution-GetComponentHistory.json');
                        api.post({
                            success: function (o) {
                                if (o.data) {
                                    var distInfo = o.data[0];
                                    if ("L" === selectedView) {
                                        $scope.distInfoLeft = distInfo;
                                        if ($scope.leftVersion == null) $scope.leftVersion = distInfo.version;
                                        if ($scope.distInfoLeft.version !== $scope.leftVersion || $scope.contentLeft.content == null) {
                                            getVersionContent(selectedView, $scope.distInfoLeft.version);
                                        }
                                    } else if ("R" === selectedView) {
                                        $scope.distInfoRight = distInfo;
                                        if ($scope.rightVersion == null) $scope.rightVersion = distInfo.version;
                                        if ($scope.distInfoRight.version !== $scope.rightVersion || $scope.contentRight.content == null) {
                                            getVersionContent(selectedView, $scope.distInfoRight.version);
                                        }
                                        if (callback) {
                                            callback();
                                        }
                                    }
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    }

                    $scope.selectHistoryLeft = function (idx, selectedData) {
                        $scope.selectedNumLeft = idx;
                        $scope.selectedHistoryLeft = idx===-1 ? $scope.distHistoriesLeft[0] : selectedData;
                        getHistoryContent($scope.selectedHistoryLeft, 'L');
                    };
                    $scope.selectHistoryRight = function (idx, selectedData){
                        $scope.selectedNumRight = idx;
                        $scope.selectedHistoryRight = idx===-1 ? $scope.distHistoriesRight[0] : selectedData;
                        getHistoryContent($scope.selectedHistoryRight, 'R');
                    };

                    $scope.loadMore = function(view) {
                        pageSize = 10000;
                        $scope.getHistoryList(view);
                    };

                    $scope.getHistoryList = function(view, callback){
                        var param = {
                            id: $scope.dist.id,
                            appName: $scope.dist.appName,
                            pageSize: pageSize
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/bio.distribution-GetComponentHistoryInfoList@.json');
                        api.post({
                            success: function (o) {
                                $scope.distHistoriesLeft = o.data.data;
                                $scope.distHistoriesRight = o.data.data;

                                if (callback) {
                                    if (callback === initHistoryView) {
                                        $(".list-group").scrollTop(0);
                                        $(".content").scrollTop(0);
                                        $scope.selectedHistoryLeft = $scope.distHistoriesLeft[0];
                                        $scope.selectedHistoryRight = $scope.distHistoriesRight[0];
                                        $scope.selectedNumLeft = 0;
                                        $scope.selectedNumRight = -1;
                                    }
                                    callback();
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    };

                    $scope.getDistributionUrl = function (item) {
                        if(item){
                            var fileName = mars$util$.addFileExtension(item.name, item.extension);
                            return marsContext.contextPath + "/services/file/get/bio.distribution-DownloadDistribution@/" + item.id + "/" + fileName
                                + "?version="+item.version+"&etpl=/core/exception/ExceptionResponse.html";
                        }
                    };

                    $scope.isValueDiff = function (name) {
                        if (null != name && $scope.distInfoLeft && $scope.distInfoRight) {
                            return $scope.distInfoLeft[name] !==  $scope.distInfoRight[name];
                        }
                        return false;
                    };

                    function updateDistribution(distData, callback) {
                        var api = new AjaxService(marsContext.contextPath + '/services/file/run/bio.distribution-UpdateDistributionBase64@.json');
                        api.post({
                            success: function (o) {
                                scope.refreshList();
                                if (callback) {
                                    callback();
                                }
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, distData);
                    }

                    $scope.revert = function (view) {
                        var dist = 'L' === view ? $scope.distInfoLeft : $scope.distInfoRight;
                        var data = {
                            id: dist.id,
                            location: dist.location,
                            type: dist.type,
                            auto: dist.auto,
                            method: dist.method,
                            priority: dist.priority,
                            appName: dist.appName
                        };
                        if (dist.label) data.label = dist.label;
                        if (dist.description) data.description = dist.description;

                        if (view ==='L' && $scope.contentLeft.isChanged){
                            data.fileInputStream = $scope.contentLeft.content;
                            data.name = dist.name;
                        } else if (view ==='R' && $scope.contentRight.isChanged){
                            data.fileInputStream = $scope.contentRight.content;
                            data.name = dist.name;
                        }
                        updateDistribution(data, resetHistory);
                    };

                    function resetHistory() {
                        var callback = initHistoryView;
                        $scope.getHistoryList("ALL", callback);
                    }

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };

                    function resize() {
                        var historyContainer = document.getElementById("historyContainer");
                        var historyContentHeader = document.getElementById("historyContentHeader");
                        var historyContent = document.getElementById("historyContent");
                        if (historyContent) historyContent.style.height = (historyContainer.offsetHeight - historyContentHeader.offsetHeight) + "px";
                    }

                    $(window).on('resize', function () {
                        resize();
                    });

                    $scope.setHeight = function () {
                        setHeight();
                    };

                    function initHistoryView() {
                        var callback = getHistoryContent($scope.selectedHistoryRight, "R");
                        getHistoryContent($scope.selectedHistoryLeft, "L", callback);
                    }

                    (function () {
                        initHistoryView();
                    })();

                }
            ]
        }).result.then(function (distData) {

        }, function () {

        });
    };

    $scope.setHeight = function () {
        setHeight();
    };

    (function () {
        getAppNames();
        getDirList();
        getDistributionList(1);
    })();

}]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = height + "px";

    var historyModal = document.getElementById("historyModal");
    if (historyModal) historyModal.style.height = height + "px";
}
