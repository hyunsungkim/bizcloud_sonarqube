<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>System Distribution Management</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select2.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <script src="../../../includes/node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="../../../includes/node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../includes/node_modules/angular-file-upload/dist/angular-file-upload.min.js"></script>
    <script src="../../../includes/node_modules/ui-select/dist/select.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <!--[if lt IE 9]>
    <script>
        document.createElement('ui-select');
        document.createElement('ui-select-match');
        document.createElement('ui-select-choices');
    </script>
    <![endif]-->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("distribution-manager.js");
    </script>
    <style>
        a:not([href]):not([tabindex]), a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
            color: #444;
        }
    </style>
    <script>
        var textColor = "<%=textColor%>";
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>System Distribution Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                <div class="card-header" id="rightPanelHeader">
                    <div class="row justify-content-md-center" style="padding:10px 0 10px 0;">
                        <form class="form-inline">
                            <label for="searchApp" style="padding: 0 5px 0 2px;">Application</label>
                            <select class="form-control form-control-sm" id="searchApp" ng-enter="searchDistribution()" ng-model="search.appName" style="min-width: 100px;">
                                <option></option>
                                <option value="BIO">BIO</option>
                                <option ng-repeat="item in appNames" value="{{item}}">{{item}}</option>
                            </select>
                            <div style="padding-left: 10px;"></div>
                            <label for="searchName" style="padding: 0 5px 0 2px;">Name</label>
                            <input type="text" class="form-control form-control-sm" id="searchName" ng-enter="searchDistribution()" ng-model="search.LIKE_name_LIKE">
                            <div style="padding-left: 10px;"></div>
                            <label for="searchlabel" style="padding: 0 5px 0 2px;">Label</label>
                            <input type="text" class="form-control form-control-sm" id="searchLabel" ng-enter="searchDistribution()" ng-model="search.label">
                            <div style="padding-left: 10px;"></div>
                            <label for="searchType" style="padding: 0 5px 0 2px;">Type</label>
                            <select class="form-control form-control-sm" id="searchType" ng-enter="searchDistribution()" ng-model="search.type" style="min-width: 100px;">
                                <option></option>
                                <option ng-repeat="item in distTypeFilters" value="{{item}}">{{item}}</option>
                            </select>
                            <div style="padding-left: 5px;"></div>
                            <button type="button" class="btn btn-sm btn-secondary" ng-click="searchDistribution()">Search</button>
                            <div style="padding-left: 10px;"></div>
                            <button class="btn btn-sm btn-success" ng-click="uploadDistribution();$event.stopPropagation();">Upload</button>
                            <div style="padding-left: 10px;"></div>
                            <button class="btn btn-sm btn-secondary" ng-click="refreshList();$event.stopPropagation();">Refresh</button>
                        </form>
                    </div>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div id="DataValue" ng-if="distList">
                        <div ng-if="distList.data.length == 0">
                            <div class="text-center">There are no distributions</div>
                        </div>
                        <div class="table-responsive" style="border-bottom: 1px solid #dee2e6;">
                            <table class="table table-striped table-condensed table-font-small table-sm list text-<%=textColor%>" ng-if="distList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold">
                                    <th>#</th>
                                    <th ng-click="sortDistributionList('appName')">App</th>
                                    <th ng-click="sortDistributionList('name')">Name</th>
                                    <th ng-click="sortDistributionList('label')">Label</th>
                                    <th ng-click="sortDistributionList('type')">Type</th>
                                    <th ng-click="sortDistributionList('fileSize')" nowrap>Size</th>
                                    <th ng-click="sortDistributionList('auto')">Auto</th>
                                    <th ng-click="sortDistributionList('method')">Method</th>
                                    <th ng-click="sortDistributionList('priority')">Priority</th>
                                    <th ng-click="sortDistributionList('location')">Location</th>
                                    <th ng-click="sortDistributionList('transactionDate')">Upload Date</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat-start="item in distList.data" id="{{item.id}}" class="dist">
                                    <td rowspan="2">{{item.ROW_NUMBER}}</td>
                                    <td>{{item.appName}}</td>
                                    <td><a target="{{item.id}}" ng-href="{{getDistributionUrl(item, item.name)}}" title="{{item.name}}">{{item.name}}</a></td>
                                    <td>{{item.label}}</td>
                                    <td>{{item.type}}</td>
                                    <td>{{item.fileSize|number:0}}</td>
                                    <td>{{item.auto}}</td>
                                    <td>{{item.method ? item.method : "COPY"}}</td>
                                    <td>{{item.priority ? item.priority : "0"}}</td>
                                    <td>{{item.location}}</td>
                                    <td>{{item.transactionDate | makeDate | makeLocalDate | date:'MM/dd/yyyy HH:mm:ss'}}</td>
                                    <td rowspan="2" class="text-nowrap" style="vertical-align: middle;">
                                        <div style="padding-bottom: 5px;">
                                            <button class="btn btn-xs2 btn-info" ng-if="item.extension && item.extension.toLowerCase() == 'zip'" ng-click="showEntityList(item);$event.stopPropagation();"> Entity List</button>
                                            <button class="btn btn-xs2 btn-warning" ng-click="deployDistribution(item);$event.stopPropagation();">Deploy</button>
                                            <button class="btn btn-xs2 btn-primary" ng-click="updateDistribution(item);$event.stopPropagation();">Edit</button>
                                        </div>
                                        <div>
                                            <button class="btn btn-xs2 btn-secondary" ng-click="copyUrl(item, 'full');$event.stopPropagation();">Copy Full URL</button>
                                            <button class="btn btn-xs2 btn-danger" ng-click="deleteDistribution(item);$event.stopPropagation();">Delete</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr ng-repeat-end>
                                    <td colspan="10">
                                        <div class="row" style="padding-left: 10px; padding-right: 10px;">
                                            {{item.description}}
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="distList.totalCount > distListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="distList.totalCount" items-per-page="distListPageSize" ng-model="$parent.$parent.distListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changeDistributionListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>