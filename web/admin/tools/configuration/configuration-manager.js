var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'mars.angular.directive'
    , 'blockUI'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/data\/search\//) || config.url.match(/myTimeInfo.json/gi)) {
            return false; // ... don't block it.
        }
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$timeout', '$filter', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $timeout, $filter, $uibModal, blockUI, AjaxService, marsContext) {
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;

    if ("undefined" != typeof appName) {
        $scope.appName = appName;
    }
    var blockUIStarted = false;

    function startBlockUI(msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    function callApi2(apiUrl, callback, param) {
        var api = new AjaxService(marsContext.contextPath + apiUrl);
        api.get({
            success: function (o) {
                callback(o);
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function callApi(apiUrl, callback, param) {
        var api = new AjaxService(marsContext.contextPath + apiUrl);
        api.post({
            success: function (o) {
                callback(o);
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    function getAppNames() {
        callApi2('/services/moon/getMoonNames.json',
            function (o) {
                $scope.appNames = o.data;
                stopBlockUI();
            });
    }

    $scope.search = {};
    $scope.confListCurrentPage = 1;
    $scope.confListPageSize = 15;
    $scope.confListOrderColumn = 'appName, name';
    $scope.confListOrderAsc = true;

    $scope.setPageSize = function (size) {
        $scope.confListPageSize = size;
    };

    function callConfListApi(param) {
        getAppNames();
        callApi('/services/data/getList/bio.conf-GetConfiguration.json',
            function (o) {
                $scope.confList = o.data;
                stopBlockUI();
            }, param);
    }

    function getConfList(pageNo, filter) {
        var param = {
            pageNo: pageNo || $scope.confListCurrentPage,
            pageSize: $scope.confListPageSize,
            ORDER_BY: $scope.confListOrderColumn + ' ' + ($scope.confListOrderAsc ? 'ASC' : 'DESC'),
            appName: $scope.appName
        };

        $scope.confListCurrentPage = param.pageNo;

        if (filter) {
            angular.extend(param, filter);
        }

        callConfListApi(param);
    }

    $scope.searchConf = function () {
        if ($scope.search.appName === '') $scope.search.appName = undefined;
        getConfList(1, $scope.search);
    };

    $scope.changeConfListPage = function () {
        getConfList();
    };

    $scope.sortConfList = function (column) {
        if ($scope.confListOrderColumn === column) {
            $scope.confListOrderAsc = !$scope.confListOrderAsc;
        } else $scope.confListOrderAsc = true;
        $scope.confListOrderColumn = column;
        getConfList($scope.confListCurrentPage);
    };

    $scope.refreshList = function () {
        getConfList(undefined, $scope.search);
    };

    (function () {
        getConfList();
    })();

    function createConfiguration(conf) {
        callApi('/services/data/run/bio.conf-CreateConfiguration@.json',
            function (o) {
                $scope.refreshList();
            }, conf);
    }

    function updateConfiguration(conf, callback) {
        callApi('/services/data/run/bio.conf-UpdateConfiguration@.json',
            function (o) {
                $scope.refreshList();
                if(callback){
                    callback();
                }
            }, conf);
    }

    function deleteConfiguration(conf) {
        callApi('/services/data/run/bio.conf-DeleteConfiguration@.json',
            function (o) {
                $scope.refreshList();
            }, {
                id: conf.id,
                appName: conf.appName
            });
    }

    $scope.deleteConf = function (conf) {
        bootbox.confirm({
            message: "Are you sure to delete the configuration '" + conf.type + "/" + conf.name + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteConfiguration(conf);
                }
            }
        });
    };

    $scope.addConf = function () {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/configuration/configuration-edit.html'),
            windowClass: 'app-modal-window-8',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.mode = 'Add';
                    $scope.title = "Add Configuration";
                    $scope.appNames = scope.appNames;
                    $scope.appName = scope.appName;
                    $scope.conf = {
                        appName: scope.appName
                    };

                    $scope.checkConf = function (type) {
                        $scope.conf.invalidName = false;
                        $scope.conf.invalidType = false;
                        if ($scope.conf.type && $scope.conf.name) {
                            callApi('/services/data/get/bio.conf-GetConfiguration.json',
                                function (o) {
                                    if (type === 'type') {
                                        $scope.conf.invalidType = o && o.data && o.data.length > 0;
                                    } else {
                                        $scope.conf.invalidName = o && o.data && o.data.length > 0;
                                    }
                                    stopBlockUI();
                                }, {
                                    type: $scope.conf.type,
                                    name: $scope.conf.name
                                });
                        }
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.conf);
                    };
                }
            ]
        }).result.then(function (conf) {
            createConfiguration(conf);
        }, function () {
        });
    };

    $scope.updateConf = function (conf) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/configuration/configuration-edit.html'),
            windowClass: 'app-modal-window-8',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.mode = 'Edit';
                    $scope.title = "Edit Configuration";
                    $scope.appNames = scope.appNames;
                    $scope.appName = scope.appName;
                    $scope.conf = conf;

                    $scope.checkConf = function (type) {
                        $scope.conf.invalidName = false;
                        $scope.conf.invalidType = false;
                        if ($scope.conf.type && $scope.conf.name) {
                            callApi('/services/data/get/bio.conf-GetConfiguration.json',
                                function (o) {
                                    if (type === 'type') {
                                        $scope.conf.invalidType = o && o.data && o.data.length > 0;
                                        if (o.data.length === 1) {
                                            $scope.conf.invalidType = o.data[0].id !== conf.id;
                                        }
                                    } else {
                                        $scope.conf.invalidName = o && o.data && o.data.length > 0;
                                        if (o.data.length === 1) {
                                            $scope.conf.invalidName = o.data[0].id !== conf.id;
                                        }
                                    }
                                    stopBlockUI();
                                }, {
                                    type: $scope.conf.type,
                                    name: $scope.conf.name
                                });
                        }
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close($scope.conf);
                    };
                }
            ]
        }).result.then(function (conf) {
            delete conf['transactionId'];
            updateConfiguration(conf);
        }, function () {
        });
    };

    function getHistory(conf, callback) {
        $scope.confHistories = [];
        $scope.conf = conf;
        var param = {
            id: conf.id,
            appName: conf.appName
        };
        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/bio.conf-GetConfigurationHistoryInfoList@.json');
        api.post({
            success: function (o) {
                $scope.confHistories = o.data.data;
                if (callback) {
                    callback();
                }
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, param);
    }

    $scope.showConfHistory = function(conf) {
        var callback = $scope.loadConfHistory;
        getHistory(conf, callback);
    };

    $scope.loadConfHistory = function () {
        var scope = $scope;
        var pageSize = 10;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/apps/fbs/tools/configuration/configuration-history.html'),
            windowClass: 'app-modal-window-11',
            controller: ['$scope',
                function ($scope) {
                    $scope.scope = scope;
                    $scope.contextPath = marsContext.contextPath;
                    $scope.title = "Configuration Versioning";
                    $scope.conf = scope.conf;
                    $scope.confHistoriesLeft = scope.confHistories;
                    $scope.confHistoriesRight = scope.confHistories;
                    $scope.selectedHistoryLeft = scope.confHistories[0];
                    $scope.selectedHistoryRight = scope.confHistories[0];
                    $scope.selectedNumLeft = 0;
                    $scope.selectedNumRight = -1;

                    $scope.selectHistoryLeft = function (idx, selectedData) {
                        $scope.selectedNumLeft = idx;
                        $scope.selectedHistoryLeft = idx===-1 ? $scope.confHistoriesLeft[0] : selectedData;
                    };

                    $scope.selectHistoryRight = function (idx, selectedData){
                        $scope.selectedNumRight = idx;
                        $scope.selectedHistoryRight = idx===-1 ? $scope.confHistoriesRight[0] : selectedData;
                    };

                    $scope.loadMore = function(view) {
                        pageSize = 10000;
                        $scope.getHistoryList(view);
                    };

                    $scope.getHistoryList = function(){
                        var param = {
                            appName: $scope.conf.appName,
                            id: $scope.conf.id,
                            pageSize: pageSize
                        };
                        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/bio.conf-GetConfigurationHistoryInfoList@.json');
                        api.post({
                            success: function (o) {
                                $scope.confHistoriesLeft = o.data.data;
                                $scope.confHistoriesRight = o.data.data;

                                $(".list-group").scrollTop(0);
                                $(".content").scrollTop(0);
                                $scope.selectedHistoryLeft = $scope.confHistoriesLeft[0];
                                $scope.selectedHistoryRight = $scope.confHistoriesRight[0];
                                $scope.selectedNumLeft = 0;
                                $scope.selectedNumRight = -1;
                            },
                            error: function (o) {
                                stopBlockUI();
                                o.alertException();
                            }
                        }, param);
                    };

                    $scope.isValueDiff = function (name) {
                        if (null != name) {
                            return $scope.selectedHistoryLeft[name] !==  $scope.selectedHistoryRight[name];
                        }
                        return false;
                    };

                    function checkConf(conf) {
                        var invalidConf = false;
                        if (conf.type && conf.name) {
                            callApi('/services/data/get/bio.conf-GetConfiguration.json',
                                function (o) {
                                    invalidConf = o && o.data && o.data.length > 0;
                                    if (o.data.length === 1) {
                                        invalidConf = o.data[0].id !== conf.id;
                                    }
                                    if(!invalidConf){
                                        updateConfiguration(conf, resetHistory);
                                    }else{
                                        angularExt.getBootboxObject().alert("The configuration already exists in the list.");
                                    }
                                }, {
                                    type: conf.type,
                                    name: conf.name
                                });
                        }
                    }

                    $scope.revert = function (view) {
                        var conf = 'L' === view ? $scope.selectedHistoryLeft : $scope.selectedHistoryRight;
                        var data = {
                            id: conf.id,
                            appName: conf.appName,
                            type: conf.type,
                            name: conf.name,
                            value: conf.value,
                            description: conf.description
                        };
                        checkConf(data);
                    };

                    function resetHistory() {
                        $scope.getHistoryList();
                    }

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };

                    function resize() {
                        var historyContainer = document.getElementById("historyContainer");
                        var historyContentHeader = document.getElementById("historyContentHeader");
                        var historyContent = document.getElementById("historyContent");
                        if (historyContent) historyContent.style.height = (historyContainer.offsetHeight - historyContentHeader.offsetHeight) + "px";
                    }

                    $(window).on('resize', function () {
                        resize();
                    });

                    $scope.setHeight = function () {
                        setHeight();
                    };

                    (function () {
                        $timeout(function(){
                            resize();
                        }, 100);
                    })();
                }
            ]
        }).result.then(function (confData) {
        }, function () {
        });
    };

    $scope.setHeight = function () {
        setHeight();
    };
}]);

function setHeight() {
    var clientSize = mars$util$.getClientSize();
    var panelContainer = document.getElementById("panelContainer");
    var height = clientSize.height - panelContainer.offsetTop;
    // var leftContainer = document.getElementById("leftPanelContainer");
    // leftContainer.style.height = height + "px";
    var rightContainer = document.getElementById("rightPanelContainer");
    rightContainer.style.height = height + "px";

    var historyModal = document.getElementById("historyModal");
    if (historyModal) historyModal.style.height = height + "px";
}
