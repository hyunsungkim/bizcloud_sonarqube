<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.core.net.util.HostNameUtil" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<%
    String bioServiceName = CoreServiceConfigUtil.getServiceName().toUpperCase();
%>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Application Configuration Management</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/theme/dark/primereact/themes/theme.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bizflow/css/modeler-admin/bootstrap/custom-<%=bgColor%>.css"/>

    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-directive.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("configuration-manager.js");
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" ng-init="setHeight();" onresize="setHeight();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (showTitle) {%>
<div id="titleBox" class="text-center " style="padding: 10px 0 10px 0">
    <h1>Application Configuration Management</h1>
</div>
<%}%>
<div class="container-fluid" id="baseContainer">
    <div class="row" id="panelContainer">
        <div class="col" style="padding: 0 0 0 1px;">
            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary" id="rightPanelContainer">
                <div class="card-header" id="rightPanelHeader" style="min-height: 46px;">
                    <div class="row justify-content-md-center" style="padding:10px 0 10px 0;" ng-if="confList">
                        <form class="form-inline">
                            <label for="searchApp" style="padding: 0 5px 0 2px;">Application</label>
                            <select class="form-control form-control-sm" id="searchApp" ng-enter="searchConf()" ng-model="search.appName" style="min-width: 100px;">
                                <option></option>
                                <option value="<%=bioServiceName%>"><%=bioServiceName%></option>
                                <option ng-repeat="item in appNames" value="{{item}}">{{item}}</option>
                            </select>
                            <div style="padding-left: 10px;"></div>
                            <label for="searchType" style="padding: 0 5px 0 2px;">Type</label>
                            <input type="text" class="form-control form-control-sm" id="searchType" ng-enter="searchConf()" ng-model="search.LIKE_type_LIKE">
                            <div style="padding-left: 10px;"></div>
                            <label for="searchName" style="padding: 0 5px 0 2px;">Name</label>
                            <input type="text" class="form-control form-control-sm" id="searchName" ng-enter="searchConf()" ng-model="search.LIKE_name_LIKE">
                            <div style="padding-left: 5px;"></div>
                            <button type="button" class="btn btn-sm btn-secondary" ng-click="searchConf()">Search</button>
                            <div style="padding-left: 10px;"></div>
                            <button class="btn btn-sm btn-primary" ng-click="addConf();$event.stopPropagation();">Create</button>
                            <div style="padding-left: 10px;"></div>
                            <button class="btn btn-sm btn-secondary" ng-click="refreshList();$event.stopPropagation();">Refresh</button>
                            <div style="padding-left: 10px;"></div>
                            <span class="badge badge-pill badge-secondary" style="font-size: 12px;">
                                Current Server Name: <span class="badge badge-info"><%=HostNameUtil.getLocalHostName()%></span>
                            </span>
                        </form>
                    </div>
                </div>
                <div class="card-block panelContainer" style="overflow-x: hidden;">
                    <div id="DataValue" ng-if="confList">
                        <div ng-if="confList.data.length == 0">
                            <div class="text-center">There are no configurations</div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed table-font-small table-sm text-<%=textColor%>" ng-if="confList.data.length > 0">
                                <thead>
                                <tr style="font-weight: bold;" class="bg-info conf">
                                    <th>#</th>
                                    <th ng-click="sortConfList('appName')"><a>Application</a></th>
                                    <th ng-click="sortConfList('type')"><a>Type</a></th>
                                    <th ng-click="sortConfList('name')"><a>Name</a></th>
                                    <th ng-click="sortConfList('value')"><a>Value</a></th>
                                    <th ng-click="sortConfList('description')"><a>Description</a></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in confList.data" id="{{item.id}}" class="conf">
                                    <td>{{item.ROW_NUMBER}}</td>
                                    <td>{{item.appName}}</td>
                                    <td>{{item.type}}</td>
                                    <td>{{item.name}}</td>
                                    <td>{{item.value}}</td>
                                    <td>{{item.description}}</td>
                                    <td class="text-nowrap text-right" style="vertical-align: middle;">
                                        <button class="btn btn-xs2 btn-primary" ng-click="updateConf(item);$event.stopPropagation();">Edit</button>
                                        <button class="btn btn-xs2 btn-danger" ng-click="deleteConf(item);$event.stopPropagation();">Delete</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-md-center" style="padding-top: 10px;" ng-if="confList.totalCount > confListPageSize">
                            <ul class="uibPagination" uib-pagination total-items="confList.totalCount" items-per-page="confListPageSize" ng-model="$parent.$parent.confListCurrentPage"
                                max-size="10" boundary-links="true" rotate="false" ng-change="changeConfListPage()"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>