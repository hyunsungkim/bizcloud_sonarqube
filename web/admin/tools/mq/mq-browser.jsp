<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.io.OutputStreamWriterEx" %>
<%@ page import="com.bizflow.io.core.util.DateUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.core.xml.util.XPathUtil" %>
<%@ page import="com.bizflow.io.core.xml.util.XmlUtil" %>
<%@ page import="com.bizflow.io.services.mq.exception.MQException" %>
<%@ page import="com.bizflow.io.services.mq.exception.MQExceptionCode" %>
<%@ page import="com.bizflow.io.services.mq.exception.MQMessageWorkerException" %>
<%@ page import="com.bizflow.io.services.mq.handler.MQMessageBrowser" %>
<%@ page import="com.bizflow.io.services.mq.handler.MQMessageSender" %>
<%@ page import="com.bizflow.io.services.mq.model.MQConnectionInfo" %>
<%@ page import="com.bizflow.io.services.mq.util.MQMessageUtil" %>
<%@ page import="com.bizflow.io.services.mq.util.MQServiceConfigUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="javax.jms.JMSException" %>
<%@ page import="javax.jms.Message" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>

<%
    final String whatQueue = ServletUtil.getParameterValue(request, "Q", "IN");
    final String message = ServletUtil.getParameterValue(request, "message", "");
    final String action = ServletUtil.getParameterValue(request, "action", "");
    final String mqSource = ServletUtil.getParameterValue(request, "mqSource", "DEFAULT");
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>MQ Browser</title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <style>
        .table tr th {
            font-size: 14px;
            text-align: center;
        }

        .table tbody tr td {
            font-size: 12px;
        }

        .textbox {
            font-size: 12px;
        }
    </style>
    <script>
        function sendMessage() {
            document.forms['main'].elements['action'].value = "send";
            document.forms['main'].submit()
        }

        function refreshList() {
            document.forms['main'].submit()
        }
    </script>
</head>
<body>
<div class="page-header" style="text-align: center;">
    <h1>BizFlowSRS MQ Browser</h1>
</div>
<div class="container">
    <div class="text-right">
        <form>
            Service:&nbsp;<select name="mqSource">
            <%
                Map<String, List> map = MQServiceConfigUtil.getConfiguration().getGroupPropertyMap();
                Iterator<String> iterator = map.keySet().iterator();
                while (iterator.hasNext()) {
                    String name = iterator.next();
                    if (name.startsWith("MQService")) {
                        String type = "DEFAULT";
                        if (name.startsWith("MQService-")) {
                            type = name.substring("MQService-".length());
                        }
            %>
            <option <%=type.equalsIgnoreCase(mqSource) ? "selected" : ""%> value="<%=type%>"><%=type%>
            </option>
            <%
                    }
                }
            %>
        </select>
            <input type="submit" value="Go">
        </form>
    </div>
    <%
        String prefix = "MQService" + ((null != mqSource && !"DEFAULT".equalsIgnoreCase(mqSource)) ? "-" + mqSource : "");
        MQConnectionInfo connectionInfo = new MQConnectionInfo();
        connectionInfo.setRemoteHost(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".Host", connectionInfo.getRemoteHost()));
        connectionInfo.setRemotePort(MQServiceConfigUtil.getConfiguration().getPropertyAsInt(prefix + ".Port", connectionInfo.getRemotePort()));
        connectionInfo.setRemoteQManager(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".QManager", connectionInfo.getRemoteQManager()));
        if ("IN".equalsIgnoreCase(whatQueue)) {
            connectionInfo.setRemoteQueue(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".Queue.In", connectionInfo.getRemoteQueue()));
            connectionInfo.setRemoteChannel(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".Channel", connectionInfo.getRemoteChannel()));
        } else if ("OUT".equalsIgnoreCase(whatQueue)) {
            connectionInfo.setRemoteQueue(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".Queue.Out", connectionInfo.getRemoteQueue()));
            connectionInfo.setRemoteChannel(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".Channel", connectionInfo.getRemoteChannel()));
        } else if ("NOTIFICATION".equalsIgnoreCase(whatQueue)) {
            connectionInfo.setRemoteQueue(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".Queue.Notification", connectionInfo.getRemoteQueue()));
            connectionInfo.setRemoteChannel(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".Channel.Notification", connectionInfo.getRemoteChannel()));
        }
        connectionInfo.setUserId(MQServiceConfigUtil.getConfiguration().getProperty(prefix + ".QManager.User.Id", connectionInfo.getUserId()));
        connectionInfo.setPassword(MQServiceConfigUtil.getConfiguration().getEncryptedProperty(prefix + ".QManager.User.Password", connectionInfo.getPassword()));

        if ("send".equalsIgnoreCase(action) && null != message && message.length() > 0) {
            MQMessageSender sender = new MQMessageSender();
            sender.setConnectionInfo(connectionInfo);
            sender.sendMessage(message);
        }
    %>
    <ul class="nav nav-tabs">
        <li role="presentation" class='<%="IN".equalsIgnoreCase(whatQueue) ? "active":"inactive"%>'><a href="mq-browser.jsp?Q=IN&mqSource=<%=Encode.forUriComponent(mqSource)%>">In Queue</a></li>
        <li role="presentation" class='<%="OUT".equalsIgnoreCase(whatQueue) ? "active":"inactive"%>'><a href="mq-browser.jsp?Q=OUT&mqSource=<%=Encode.forUriComponent(mqSource)%>">Out Queue</a></li>
        <li role="presentation" class='<%="NOTIFICATION".equalsIgnoreCase(whatQueue) ? "active":"inactive"%>'><a href="mq-browser.jsp?Q=NOTIFICATION&mqSource=<%=Encode.forUriComponent(mqSource)%>">Notification Queue</a></li>
    </ul>

    <div class="panel panel-default" style="border-top: 0;">
        <div class="panel-heading">
            <label>Queue Information:</label>&nbsp;<span><%=connectionInfo.toString()%></span>
        </div>
        <div class="panel-body">

            <form action="mq-browser.jsp" method="post" name="main">
                <input type="hidden" name="Q" value="<%=Encode.forHtmlAttribute(whatQueue)%>">
                <input type="hidden" name="mqSource" value="<%=Encode.forHtmlAttribute(mqSource)%>">
                <input type="hidden" name="action" value="">
                <div class="form-group">
                    <label for="message">Message to send:</label>&nbsp;&nbsp;&nbsp;&nbsp;<a href="../test/sample/mqmessage-sample.html" target="_blank">
                    <small>Click here to view samples</small>
                </a>
                    <textarea name="message" id="message" class="textbox" placeholder="Enter a message to send" style="width: 100%;" rows="16"><%=Encode.forHtmlContent(message)%></textarea>
                </div>
                <div style="text-align: center">
                    <input type="button" class="btn btn-warning" style="width: 150px;" value="Send Message" onclick="sendMessage();">
                </div>
            </form>

            <div style="width: 100%; text-align: right;">
                <a class="btn btn-default btn-sm" href="javascript:refreshList();">Refresh</a>&nbsp;&nbsp;
            </div>

            <br>
            <table class="table table-striped table-condensed">
                <tr>
                    <th>Index</th>
                    <th>Message ID</th>
                    <th nowrap>Process ID</th>
                    <th>Date</th>
                    <th>Message</th>
                </tr>
                <%
                    final OutputStreamWriterEx htmlWriter = new OutputStreamWriterEx(response.getOutputStream());

                    MQMessageBrowser browser = new MQMessageBrowser() {
                        @Override
                        public void processMessage(Message message, int index) throws JMSException, MQException {
                            String textMessage = MQMessageUtil.getTextMessage(message);
                            String dateString = DateUtil.convert(new Date(message.getJMSTimestamp()), "yyyy-MM-dd HH:mm:ss.S");
                            String processId = "UNKNOWN";
                            try {
                                Document document = XmlUtil.createDocument(textMessage);
                                processId = XPathUtil.getXPathValue(document, "//WorkflowProcessID/text()");
                            } catch (Exception e) {
                            }

                            try {
                                htmlWriter.write("<tr class='msgRow'><td class='").write(0 == index % 2 ? "evenRow" : "oddRow").write("'>").write(index + 1).write("</td>");
                                htmlWriter.write("<td class='").write(0 == index % 2 ? "evenRow" : "oddRow").write("'>").write(message.getJMSMessageID()).write("</td>");
                                htmlWriter.write("<td class='").write(0 == index % 2 ? "evenRow" : "oddRow").write("'>").write(processId).write("</td>");
                                htmlWriter.write("<td class='").write(0 == index % 2 ? "evenRow" : "oddRow").write("'>").write(dateString).write("</td>");
                                htmlWriter.write("<td class='").write(0 == index % 2 ? "evenRow" : "oddRow").write("'>").write(HtmlUtil.getHtmlText(textMessage)).write("</td>");
                                htmlWriter.write("</tr>");
                            } catch (IOException e) {
                                throw new MQMessageWorkerException(MQExceptionCode.GeneralError, e, null);
                            }
                        }
                    };

                    browser.setConnectionInfo(connectionInfo);
                    int msgCount = browser.browse();
                    if (0 == msgCount) {
                %>
                <tr>
                    <td class="evenRow" colspan="4" style="text-align: center;">No message</td>
                </tr>
                <%}%>

            </table>
        </div>
    </div>
</div>
<%@include file="../../../copyright.html" %>
</body>
</html>