<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.file.util.FileUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.Moon" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.Map" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<%!
    private Map<String, File> getQueryFiles(ServletContext application, String subFolder) {
        Map<String, File> fileList = new LinkedHashMap();
        File webInfDir = new File(application.getRealPath(subFolder));
        File[] listFiles = webInfDir.listFiles();
        if (null != listFiles) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    fileList.putAll(getQueryFiles(application, subFolder + "/" + file.getName()));
                } else if (file.getName().endsWith(".xml") || file.getName().endsWith(".json")) {
                    fileList.put(subFolder + "/" + file.getName(), file);
                }
            }
        }
        return fileList;
    }

    private Map<String, File> getMoonQueryFiles() {
        Map<String, File> fileList = new LinkedHashMap();
        Collection<Moon> moons = MoonManager.getInstance().getMoonCollection();
        Iterator<Moon> it = moons.iterator();
        while (it.hasNext()) {
            Moon moon = it.next();
            File moonInfDir = new File(moon.getMoonDir(), MoonManager.MOON_INF_DIR_NAME);
            File moonDBDir = new File(moonInfDir, MoonManager.MOON_INF_DB_DIR_NAME);
            if (moonDBDir.exists() && moonDBDir.isDirectory()) {
                File[] files = moonDBDir.listFiles();
                for (File file : files) {
                    if (file.isFile() && (file.getName().endsWith(".xml") || file.getName().endsWith(".json"))) {
                        String relativePath = FileUtil.getUriRelativePath(moon.getMoonDir().getParentFile().getParentFile(), file);
                        fileList.put("/" + relativePath, file);
                    }
                }
            }
        }

        return fileList;
    }
%>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);

    Map<String, File> xmlFileMap = getQueryFiles(application, MoonManager.SYSTEM_SERVICES_DIR_PATH);
    xmlFileMap.putAll(getMoonQueryFiles());
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Query Management</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/select2/dist/css/select2.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css"/>

    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/select2/dist/js/select2.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
    </script>

    <script>
        function _onload() {
            $('#fileList').select2({
                theme: "bootstrap"
            });

            var titleBox = document.getElementById("titleBox");
            var height = document.body.clientHeight - titleBox.offsetHeight - 5;
            var frame = document.getElementById("frame");
            frame.style.height = height + "px";

            document.forms[0].submit();
        }

        function apply() {
            applyOk();
            var hiddenGround = document.getElementById('hiddenGround');
            hiddenGround.src = '../mgmt/reload-config.jsp?service=Moon-DB-MyBatis';
        }

        function reloadDone() {
            document.getElementById("applyAlert").style.display = '';
        }

        function applyOk() {
            document.getElementById("applyAlert").style.display = 'none';
        }
    </script>
    <style>
        .select2-container--bootstrap .select2-results > .select2-results__options {
            max-height: 600px;
        }

        .select2-results__option {
            color: black;
        }
    </style>
</head>
<body onload="_onload()" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="page-header text-center" id="titleBox">
    <h1>Query Management</h1>
</div>
<div class="container-fluid">
    <div class="alert alert-success" role="alert" id="applyAlert" style="display: none;">
        Reloading MyBatis Configuration is done successfully.
        <button type="button" class="close" aria-label="Close" onclick="applyOk()"><span aria-hidden="true">&times;</span></button>
    </div>
    <form method="post" target="frame" action="../util/code-editor.jsp?theme=<%=theme%>">
        <input type="hidden" name="t" value="">
        <input type="hidden" name="m" value="e">
        <div class="row text-<%=textColor%> bg-<%=bgColor%> border-secondary">
            <div class="col-md-9">
                <select name="f" id="fileList" class="js-example-basic-single max-width" style="height: 700px;" onchange="document.forms[0].submit();">
                    <%
                        Iterator<String> it = xmlFileMap.keySet().iterator();
                        while (it.hasNext()) {
                            String name = it.next();
                    %>
                    <option value="<%=name%>"><%=name%>
                    </option>
                    <%
                        }
                    %>
                </select>
            </div>
            <div class="col-md-3 text-nowrap">
                <input type="submit" class="btn btn-secondary btn-sm" value="Edit" style="width: 100px;">&nbsp;&nbsp;
                <input type="button" class="btn btn-danger btn-sm" value="Apply" onclick="apply();" style="width: 100px;">
            </div>
        </div>
    </form>
</div>
<iframe id="frame" name="frame" src="../blank.html" style="width: 100%;height: 95%;border: 0;padding: 0;margin: 0;" frameborder="0"></iframe>
<iframe name="hiddenGround" id="hiddenGround" src="about:blank" frameborder="0" width="5" height="5" style="visibility: hidden;"></iframe>
<%@include file="../../../copyright.html" %>
</body>
</html>