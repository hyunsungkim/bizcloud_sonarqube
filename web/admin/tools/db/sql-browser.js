var app = angular.module('angularApp', [
    'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'mars.angular.filter'
    , 'ngSanitize'
    , 'ui.select'
]).controller('angularAppCtrl', ['$scope', 'AjaxService', 'marsContext', function ($scope, AjaxService, marsContext) {
    $scope.method = "POST";
    $scope.htmlStyle = htmlStyle;
    $scope.model = {};

    var paramLoad = false;

    if ('undefined' != typeof queryName) {
        $scope.model.api = queryName;
        $scope.model.session = sessionName;
        if ('undefined' != typeof namespace) {
            $scope.model.namespace = namespace;
        }
        if ('undefined' != typeof queryParam) {
            $scope.model.param = queryParam.replace(/\\\\r/g, '\r').replace(/\\\\n/g, '\n');
        }

        paramLoad = true;
    }

    function getQueryName() {
        return $scope.model.api.replace(".", "-");
    }

    $scope.selectApi = function (item) {
        var queryName = getQueryName();
        var api = new AjaxService(marsContext.contextPath + '/services/data/context/' + $scope.model.session + '/api/' + queryName + '.json');
        api.get({
            success: function (o) {
                $scope.queryInfo = o.data;
                $scope.model.notes = $scope.queryInfo.notes;
                if ('undefined' != typeof item) {
                    $scope.model.param = undefined;
                }

                if (!paramLoad) {
                    $("#parameters").addClass("in");
                    var sqlBox = document.getElementById("sqlBox");
                    if (sqlBox) sqlBox.style.display = "none";
                    var resultBox = document.getElementById("resultBox");
                    if (resultBox) resultBox.style.display = "none";
                }

                paramLoad = false;
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.genParam = function () {
        var param = {};
        var len = $scope.queryInfo.parameterList.length;
        for (var i = 0; i < len; i++) {
            var parameter = $scope.queryInfo.parameterList[i];
            param[parameter.name] = parameter.value ? parameter.value : undefined;
        }

        $scope.model.param = JSON.stringify(param, null, 2);
    };

    $scope.changeNamespace = function () {
        var api;
        if ($scope.model.namespace === "ALL") {
            api = new AjaxService(marsContext.contextPath + '/services/data/context/' + $scope.model.session + '/queries.json');
        } else {
            api = new AjaxService(marsContext.contextPath + '/services/data/context/' + $scope.model.session + '/' + $scope.model.namespace + '/queries.json');
        }
        api.get({
            success: function (o) {
                $scope.apis = o.data;
                if (paramLoad) {
                    $scope.selectApi();
                } else {
                    $scope.model.api = undefined;
                }
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    $scope.changeSession = function () {
        var api = new AjaxService(marsContext.contextPath + '/services/data/context/' + $scope.model.session + '/namespaces.json');
        api.get({
            success: function (o) {
                $scope.namespaces = o.data;
                if (!paramLoad) {
                    $scope.namespaces.push("ALL");
                    $scope.model.namespace = "ALL";
                }
                $scope.changeNamespace();
            },
            error: function (o) {
                o.alertException();
            }
        });
    };

    var sessionsApi = new AjaxService(marsContext.contextPath + '/services/data/context/sessions.json');
    sessionsApi.get({
        success: function (o) {
            $scope.sessions = o.data;
            if (!paramLoad) {
                $scope.model.session = $scope.sessions[0];
            }
            $scope.changeSession();
        },
        error: function (o) {
            o.alertException();
        }
    });

    $scope.doSubmit = function () {
        var form = document.getElementById("runQuery");
        form.elements['s'].value = $scope.model.session;
        form.elements['n'].value = $scope.model.namespace;
        form.elements['q'].value = $scope.model.api;
        form.elements['p'].value = $scope.model.param || "{}";

        form.submit();
    };

    $scope.fixChromeScroll = function () {
        setTimeout(function () {
            var div = document.createElement("div");
            div.innerHTML = "";
            document.body.appendChild(div);
            setTimeout(function () {
                document.body.removeChild(div);
            }, 100);
        }, 100);
    };
}]);



