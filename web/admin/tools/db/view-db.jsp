<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.db.mybatis.model.SqlSessionFactoryObject" %>
<%@ page import="com.bizflow.io.core.db.mybatis.session.SqlSessionFactoryManager" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Map" %>
<%
    SqlSessionFactoryManager sqlSessionFactoryManager = SqlSessionFactoryManager.getInstance();
    Map<String, SqlSessionFactoryObject> map = sqlSessionFactoryManager.getSqlSessionFactoryObjectMap();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");
    </script>

    <script>
        function _onload() {
            var outerBox = document.getElementById("outerBox");
            var height = window.innerHeight - outerBox.offsetHeight - 10;
            var frameBox = document.getElementById("frameBox");
            frameBox.style.height = height + "px";
        }

        function viewDB() {
            var form = document.forms[0];
            var sessionName = form.s.options[form.s.selectedIndex].value;
            var url = "view-schema.jsp?s=" + encodeURIComponent(sessionName);
            var frame = document.getElementById("frameView");
            frame.src = url;
        }

    </script>
</head>
<body onload="_onload(); viewDB();">
<div id="outerBox" class="text-center" style="height: 28px;">
    <form target="frameView" action="view-schema.jsp">
        <table id="outerBoxTable">
            <tr>
                <td>&nbsp;</td>
                <td nowrap>
                    Session:
                    <select name="s" style="color:black;">
                        <%
                            Iterator<String> it = map.keySet().iterator();
                            while (it.hasNext()) {
                                String name = it.next();
                        %>
                        <option value="<%=name%>"><%=name%>
                        </option>
                        <%}%>
                    </select>
                </td>
                <td nowrap>
                    <input class="button" type="submit" value="View">
                </td>
            </tr>
        </table>
    </form>
</div>
<div class="container-fluid max-height" id="frameBox">
    <iframe id="frameView" name="frameView" src="about:blank" style="width: 100%;height: 100%;border: 0;padding: 0;margin: 0;text-align: center;" frameborder="0"></iframe>
</div>
</body>
</html>