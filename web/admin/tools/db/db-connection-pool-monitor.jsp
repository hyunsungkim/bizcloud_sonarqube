<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="javax.management.MBeanAttributeInfo" %>
<%@ page import="javax.management.MBeanInfo" %>
<%@ page import="javax.management.MBeanServer" %>
<%@ page import="javax.management.ObjectName" %>
<%@ page import="java.lang.management.ManagementFactory" %>
<%@ page import="java.util.Set" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Database Connection Pool Monitor</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/fontawesome-free/css/all.min.css"/>
    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
    </script>

    <style>
        .card-header h5 {
            margin: 0;
        }
    </style>
</head>

<body onload="mars$util$.resizeFrameWindowHeight()" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="page-header text-center" style="padding: 15px 0 10px 0;">
    <h1>Database Connection Pool Monitor</h1>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md">
            <%
                try {
                    int index = 0;
                    MBeanServer server = ManagementFactory.getPlatformMBeanServer();
                    Set<ObjectName> objectNames = server.queryNames(null, null);
                    for (ObjectName name : objectNames) {
                        MBeanInfo info = server.getMBeanInfo(name);
                        String type = name.getKeyProperty("type") != null ? name.getKeyProperty("type") : "";

                        if ((type.equals("DataSource") && info.getClassName().equals("org.apache.tomcat.util.modeler.BaseModelMBean"))) {
                            index++;
            %>

            <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
                <div class="card-header border-secondary" data-toggle="collapse" data-target="#monitor<%=index%>" aria-expanded="false" aria-controls="monitor<%=index%>">
                    <h5>
                        Connection Pool #<%=index%>
                    </h5>
                </div>
                <div class="card-block collapse show text-dark bg-white" id="monitor<%=index%>">
                    <table class="table table-striped table-sm max-width">
                        <%
                            for (MBeanAttributeInfo mf : info.getAttributes()) {
                                Object attributeValue;
                                try {
                                    attributeValue = server.getAttribute(name, mf.getName());
                                } catch (Exception ex) {
                                    attributeValue = "";
                                }

                                if (attributeValue != null) {
                        %>
                        <tr>
                            <td>
                                <%=mf.getName()%>
                            </td>
                            <td>
                                <%="password".equals(mf.getName())?"**********":attributeValue.toString()%>
                            </td>
                        </tr>
                        <%
                                }
                            }
                        %>
                    </table>
                </div>
            </div>
            <%
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            %>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<%@include file="../../../copyright.html" %>
</body>
</html>

