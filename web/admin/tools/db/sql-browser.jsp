<%@ page import="com.bizflow.io.core.db.mybatis.model.ColumnInfo" %>
<%@ page import="com.bizflow.io.core.db.mybatis.session.SqlSessionFactoryManager" %>
<%@ page import="com.bizflow.io.core.db.mybatis.util.ResultSetMetaDataKeeper" %>
<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.core.json.JSONArray" %>
<%@ page import="com.bizflow.io.core.json.JSONObject" %>
<%@ page import="com.bizflow.io.core.json.util.JSONUtil" %>
<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@ page import="com.bizflow.io.core.net.util.ParameterUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.service.AdhocService" %>
<%@ page import="com.bizflow.io.services.data.dao.util.SqlDAOUtil" %>
<%@ page import="com.bizflow.io.services.data.dao.util.SqlParamUtil" %>
<%@ page import="org.apache.ibatis.mapping.BoundSql" %>
<%@ page import="org.apache.ibatis.mapping.MappedStatement" %>
<%@ page import="org.apache.ibatis.session.Configuration" %>
<%@ page import="org.apache.ibatis.session.SqlSessionFactory" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@include file="../../auth-admin.jsp" %>

<%
    AdhocService.getInstance().initializeRequest(request, response);

    String sessionName = request.getParameter("s");
    String namespace = request.getParameter("n");
    String queryName = request.getParameter("q");
    String queryParam = request.getParameter("p");
    String option = request.getParameter("o");
    String title = ServletUtil.getParameterValue(request, "t", "SQL Browser");
    String htmlStyle = ServletUtil.getParameterValue(request, "st", "bootstrap");
    boolean readOnly = "true".equalsIgnoreCase(request.getParameter("ro"));
    Object sqlResult = null;
    String errorMessage = null;
    String sql = null;
    long taken = 0;

    if (!"ignore".equals(option) && StringUtil.isNotNullBlank(sessionName) && StringUtil.isNotNullBlank(queryName)) {
        Map paramMap = (null != queryParam && queryParam.trim().length() > 0) ? JSONUtil.jsonStringToMap(queryParam) : ParameterUtil.convertToSingleValueMap(request.getParameterMap());
        try {
            long _s = System.currentTimeMillis();
            sqlResult = SqlDAOUtil.runSql(sessionName, queryName, SqlParamUtil.getSqlQueryParameter(sessionName, queryName, paramMap));
            taken = System.currentTimeMillis() - _s;
        } catch (Exception e) {
            errorMessage = ExceptionUtil.exceptionToString(e);
        }

        SqlSessionFactory sqlSessionFactory = SqlSessionFactoryManager.getInstance().getUserSqlSessionFactory(sessionName);
        Configuration configuration = sqlSessionFactory.getConfiguration();

        MappedStatement ms = configuration.getMappedStatement(queryName);
        BoundSql boundSql = ms.getBoundSql(SqlParamUtil.getSqlQueryParameter(sessionName, queryName, paramMap));
        sql = boundSql.getSql();
    }
%>

<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>SQL Browser <%=null != sessionName ? "for " + sessionName : ""%>.<%=null != queryName ? queryName : ""%>
    </title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <%if ("bootstrap".equals(htmlStyle)) {%>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <%} else {%>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.<%=htmlStyle%>.min.css" type="text/css"/>
    <%}%>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/jsonformatter/dist/json-formatter.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/ui-select/dist/select2.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../includes/node_modules/ui-select/dist/select.min.js"></script>

    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>

    <!--[if lt IE 9]>
    <script>
        document.createElement('ui-select');
        document.createElement('ui-select-match');
        document.createElement('ui-select-choices');
    </script>
    <![endif]-->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("sql-browser.js");
    </script>

    <style>
        .collapse.in {
            overflow: scroll;
        }

        .table {
            margin-bottom: 0;
        }
    </style>

    <script>
        <%if(StringUtil.isNotNullBlank(queryName)) {%>
        var sessionName = '<%=Encode.forJavaScriptBlock(sessionName)%>';
        var namespace = '<%=Encode.forJavaScriptBlock(namespace)%>';
        var queryName = '<%=Encode.forJavaScriptBlock(queryName)%>';
        var queryParam = '<%=Encode.forJavaScriptBlock(queryParam.replaceAll("\r", "\\\\r").replaceAll("\n", "\\\\n"))%>';
        <%}%>

        var htmlStyle = "<%=Encode.forJavaScriptBlock(htmlStyle)%>";

        function changeStyle() {
            var style = document.getElementById("htmlStyle");
            var styleName = style.options[style.selectedIndex].value;
            this.location.href = "sql-browser.jsp?st=" + styleName;
        }

        function changeSession(form) {
            form.o.value = "ignore";
            form.submit();
        }
    </script>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true">
<div style="text-align: center;">
    <table class="max-width">
        <tr>
            <td class="max-width text-center" style="padding-left: 100px;">
                <h1><%=title%>
                </h1>
            </td>
            <td style="text-align: right;padding-right: 10px;">
                <select id="htmlStyle" name="htmlStyle" ng-model="htmlStyle" style="color:black;" onchange="changeStyle()">
                    <option value="admin">admin</option>
                    <option value="bootstrap">bootstrap</option>
                    <option value="cerulean">cerulean</option>
                    <option value="cosmo">cosmo</option>
                    <option value="cyborg">cyborg</option>
                    <option value="darkly">darkly</option>
                    <option value="flatly">flatly</option>
                    <option value="journal">journal</option>
                    <option value="lumen">lumen</option>
                    <option value="readable">readable</option>
                    <option value="sandstone">sandstone</option>
                    <option value="simplex">simplex</option>
                    <option value="slate">slate</option>
                    <option value="spacelab">spacelab</option>
                    <option value="superhero">superhero</option>
                    <option value="united">united</option>
                    <option value="yeti">yeti</option>
                </select>
            </td>
        </tr>
    </table>
</div>
<div id="box" class="container-fluid">
    <%if (!readOnly) { %>
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools"><h3 class="panel-title">SQL</h3></div>
        <div class="panel-body">
            <div class="form-group col-md-3">
                <label for="sessionNames">Session Name</label>
                <ui-select id="sessionNames" ng-model="model.session" title="Choose a session" theme="select2" class="max-width" on-select="changeSession($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a session in the list or search name...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in sessions | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-3">
                <label for="namespaces">Type</label>
                <ui-select id="namespaces" ng-model="model.namespace" title="Choose a type" theme="select2" class="max-width" on-select="changeNamespace($item)" ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a type in the list or search name...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in namespaces | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-6">
                <label for="queryNames">Query Name</label> (* Only Select queries can be executed)
                <ui-select id="queryNames" ng-model="model.api" title="Choose a URL" theme="select2" class="max-width" on-select="selectApi($item)" autofocus ng-click="fixChromeScroll();">
                    <ui-select-match placeholder="Select a query in the list or search name...">{{$select.selected}}</ui-select-match>
                    <ui-select-choices repeat="item in apis | filter: $select.search">
                        <div ng-bind-html="item | highlight: $select.search"></div>
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group col-md-12">
                <label for="param">Parameter (JSON String)</label>
                <textarea name="param" id="param" ng-model="model.param" placeholder="Enter json parameter" class="form-control" style="width: 100%; height: 200px;"></textarea>
            </div>
            <div class="text-center col-md-12">
                <input class="btn btn-primary btn-xs" type="button" value="Run" ng-click="doSubmit()">
            </div>
        </div>
    </div>
    <div class="panel panel-info" ng-show="queryInfo">
        <div class="panel-heading" data-toggle='collapse' data-target="#parameters">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Parameters</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button class="btn btn-primary btn-xs" style="width: 200px;" ng-click="genParam();$event.stopPropagation()">Generate Parameter</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="parameters" class='collapse <%=null == sql ? "in" : ""%>'>
            <span ng-if="queryInfo.parameterList.length == 0" style="padding-left: 10px;">None</span>
            <table class="table table-bordered table-striped table-condensed" ng-if="queryInfo.parameterList.length > 0">
                <tr>
                    <th>Name</th>
                    <th>Mandatory</th>
                    <th>Type</th>
                    <th class="max-width">Value</th>
                </tr>
                <tr ng-repeat="param in queryInfo.parameterList | filter: {queryParameter: true}">
                    <td>{{param.name}}</td>
                    <td nowrap>{{param.mandatory ? 'Yes' : 'No'}}</td>
                    <td nowrap>{{param.type | toUpperCase}}<span ng-show="param.type && param.type.indexOf('char') != -1">({{param.maxLength}})</span></td>
                    <td><textarea ng-model="param.value" class="max-width" rows="1" ng-change="genParam()"></textarea></td>
                </tr>
            </table>
        </div>
    </div>
    <%}%>

    <%if (null != errorMessage) {%>
    <div id="alertError" class="alert alert-danger"
         role="alert"><%=HtmlUtil.getHtmlText(errorMessage).replaceAll("\\n", "\n<br>")%>
    </div>
    <%}%>

    <% if (null != sql) {%>
    <div class="panel panel-primary" id="sqlBox">
        <div class="panel-heading" data-toggle="collapse" data-target="#ExecSQ">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">Executed SQL Query</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <% if (taken > 0) {%>
                        <span>(<%=taken%> ms)</span>
                        <%}%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="collapse in" id="ExecSQ">
            <span style="padding: 10px;">
            <%=sql%>
            </span>
        </div>
    </div>
    <%}%>

    <% if (sqlResult instanceof JSONArray) {
        JSONArray jsonArray = (JSONArray) sqlResult;
    %>
    <div class="panel panel-success" id="resultBox">
        <div class="panel-heading" data-toggle="collapse" data-target="#resultTable">
            <h3 class="panel-title">
                Result of <%=sessionName%>-<%=queryName%>
            </h3>
        </div>
        <div class="collapse in" id="resultTable">
            <table class="table table-striped table-condensed" id="tableBox"
                   data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true"
                   data-filters="true"
                   data-show-toggle="true" data-show-refresh="true" data-show-filter="true" data-show-columns="true"
                   data-show-pagination-switch="showPaginationSwitch">
                <%
                    int resultLen = jsonArray.length();
                    if (resultLen > 0) {
                %>
                <thead>
                <tr>
                    <th>#</th>
                    <%
                        List<ColumnInfo> columnInfoList = ResultSetMetaDataKeeper.getColumnInfo();
                        for (ColumnInfo columnInfo : columnInfoList) {
                            String name = columnInfo.getColumnName();
                    %>
                    <th class="header" data-field='<%=name%>' data-sortable='true'><%=name%>
                    </th>
                    <%}%>
                </tr>
                </thead>
                <%
                    for (int i = 0; i < resultLen; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                %>
                <tr>
                    <td><%=i + 1%>
                    </td>
                    <%
                        for (ColumnInfo columnInfo : columnInfoList) {
                            String name = columnInfo.getColumnName();
                            Object value = jsonObject.opt(name);
                    %>
                    <td><%=null != value ? value : ""%>
                    </td>
                    <%
                        }
                    %>
                </tr>
                <%
                    }
                } else {
                %>
                <tr>
                    <td>No result</td>
                </tr>
                <%
                    }
                %>
            </table>
        </div>
    </div>
    <%
    } else if (null != sqlResult) {
    %>
    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-target="#result2">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">API Result (Text)</h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <% if (taken > 0) {%>
                        <span>(<%=taken%> ms)</span>
                        <%}%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="result2" class="collapse in panel-body" style="padding: 0;">
            <textarea class="form-control" style="width: 100%; height: 600px;" readonly><%=sqlResult.toString()%></textarea>
        </div>
    </div>
    <%
        }
    %>
</div>
<form id="runQuery" action="sql-browser.jsp" method="POST">
    <input type="hidden" name="s">
    <input type="hidden" name="n">
    <input type="hidden" name="q">
    <input type="hidden" name="p">
    <input type="hidden" name="st" value="<%=htmlStyle%>">
</form>
<%@include file="../../../copyright.html" %>
</body>
</html>
