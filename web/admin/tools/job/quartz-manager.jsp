<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.util.DateUtil" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlDAO" %>
<%@ page import="com.bizflow.io.services.job.util.JobServiceUtil" %>
<%@ page import="net.redhogs.cronparser.CronExpressionDescriptor" %>
<%@ page import="net.redhogs.cronparser.Options" %>
<%@ page import="org.quartz.JobDetail" %>
<%@ page import="org.quartz.JobKey" %>
<%@ page import="org.quartz.Scheduler" %>
<%@ page import="org.quartz.Trigger" %>
<%@ page import="org.quartz.impl.StdSchedulerFactory" %>
<%@ page import="org.quartz.impl.matchers.GroupMatcher" %>
<%@ page import="org.quartz.impl.triggers.CronTriggerImpl" %>
<%@ page import="java.util.*" %>

<%!
    private String getJobServerName(String serviceName) {
        Map<String, String> paramMap = new HashMap();
        paramMap.put("serviceName", serviceName);
        paramMap.put("TOP", "1");

        Map resultMap = (Map) SqlDAO.selectOne("quartz", "quartz.GetQuartzJobHistory", paramMap);
        return (String) resultMap.get("serverName");
    }

    public String convertDate(Date date) {
        return null != date ? DateUtil.convert(date, "yyyy/MM/dd HH:mm:ss z") : "";
    }
%>

<%
    List<String> jobFileList = JobServiceUtil.getJobDescriptionFilePaths();
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Quartz State</title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>

    <script>
        function showQuartzHistory(serviceName) {
            var form = document.getElementById("showQuartzHistory");
            form.serviceName.value = serviceName;
            form.t.value = "Quartz History";
            form.submit();
        }

        function editQuartzDataXML() {
            var form = document.getElementById('editQuartzDataXML');
            var sel = document.getElementById('jobFile');
            var file = sel.options[sel.selectedIndex].value;
            form.f.value = file;
            form.submit();
        }
    </script>
</head>
<body>
<form id="editQuartzDataXML" action="../util/text-editor.jsp" method="POST" target="EditQuartzJobs">
    <input type="hidden" name="m" value="e">
    <input type="hidden" name="f" value="/WEB-INF/classes/quartz_data.xml">
</form>
<form id="showQuartzHistory" action="../db/sql-browser.jsp" method="POST" target="_blank">
    <input type="hidden" name="s" value="quartz">
    <input type="hidden" name="q" value="quartz.GetQuartzJobHistory">
    <input type="hidden" name="serviceName" value="">
    <input type="hidden" name="t" value="">
</form>
<div class="page-header text-center no-margin">
    <h1>Quartz Job State</h1>
</div>
<div class="container-fluid">
    <%
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        List<String> groupNameList = new ArrayList();

        for (String groupName : scheduler.getJobGroupNames()) {
            groupNameList.add(groupName);
        }

        String[] groupNames = new String[groupNameList.size()];
        groupNames = groupNameList.toArray(groupNames);
        Arrays.sort(groupNames);
    %>
    <div class="text-right">
        Server Time <span class="badge"><%= DateUtil.convert(new Date(), "yyyy/MM/dd HH:mm:ss z")%></span>
    </div>
    <div class="text-center">
        <div class="btn-group" role="group" aria-label="...">
            <%
                for (String groupName : groupNames) {
                    String groupNameId = groupName.replaceAll(" ", "_");
            %>
            <div type="button" class="btn btn-default btn-xs"><a href="#<%=groupNameId%>Panel"><%=groupName%> Job Group</a></div>
            <%
                }
            %>
        </div>
        <a class="btn btn-default btn-xs" href="quartz-manager.jsp">Refresh</a>
        <select name="jobFile" id="jobFile">
            <%
                for (String path : jobFileList) {
            %>
            <option value="<%=path%>"><%=path%>
            </option>
            <%
                }
            %>
        </select>
        <a class="btn btn-default btn-xs" href="javascript:editQuartzDataXML();">Edit Quartz Jobs</a>
    </div>
    <br>
    <%
        for (String groupName : groupNames) {
            String groupNameId = groupName.replaceAll(" ", "_");
    %>
    <div class="panel panel-primary" id="<%=groupNameId%>Panel">
        <div class="panel-heading" data-toggle="collapse" data-target="#QuartzJobs<%=groupNameId%>"><h3 class="panel-title">Job Group [<%=groupName%>]</h3>
        </div>
        <div id="QuartzJobs<%=groupNameId%>" class="collapse in">
            <div class="panel-body">Jobs that are running in the Quartz Group <%=groupName%>
            </div>
            <table class="table table-striped table-condensed"
                   data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-toggle="true" data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <th data-field='index' data-sortable='true' class="header" style="width: 20px;">#</th>
                    <th data-field='jobName' data-sortable='true' class="header">Job Name</th>
                    <th data-field='desc' data-sortable='true' class="header">Description</th>
                    <th data-field='cronExpression' data-sortable='true' class="header">Cron Expression</th>
                    <th data-field='startTime' data-sortable='true' class="header">Start Time</th>
                    <th data-field='server' data-sortable='true' class="header">Last Run Job Server</th>
                    <th data-field='previousTIme' data-sortable='true' class="header-sm">Previous Fire Time</th>
                    <th data-field='nextTime' data-sortable='true' class="header-sm">Next Fire Time</th>
                </tr>
                </thead>
                <%
                    List<String> jobNameList = new ArrayList();
                    Map<String, JobKey> jobKeyMap = new HashMap();

                    for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
                        String jobName = jobKey.getName();
                        jobNameList.add(jobName);
                        jobKeyMap.put(jobName, jobKey);
                    }

                    String[] jobNames = new String[jobNameList.size()];
                    jobNames = jobNameList.toArray(jobNames);
                    Arrays.sort(jobNames);
                    int index = 0;
                    for (String jobName : jobNames) {
                        JobKey jobKey = jobKeyMap.get(jobName);
                        List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
                        Trigger trigger = triggers.get(0);
                        Date startTime = trigger.getStartTime();
                        Date prevFireTime = trigger.getPreviousFireTime();
                        Date nextFireTime = trigger.getNextFireTime();

                        String cronExpression = "";
                        String cronDesc = "";
                        if (trigger instanceof CronTriggerImpl) {
                            cronExpression = ((CronTriggerImpl) trigger).getCronExpression();
                            Options cronDescOptions = new Options();
                            cronDescOptions.setVerbose(true);
                            cronDesc = CronExpressionDescriptor.getDescription(cronExpression, cronDescOptions);
                        }

                        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
                        String description = jobDetail.getDescription();

                        String jobServer = "";
                        try {
                            jobServer = getJobServerName(jobName);
                        } catch (Exception e) {
                        }

                        index++;
                %>
                <tr>
                    <td><%=index%>
                    </td>
                    <td><a href="javascript:showQuartzHistory('<%=jobName%>')"><%=jobName%>
                    </a></td>
                    <td><%=null != description ? description : ""%>
                    </td>
                    <td class="small"><%=null != cronExpression ? cronExpression : ""%>
                        <div><em><%=null != cronDesc ? cronDesc : ""%>
                        </em></div>
                    </td>
                    <td><%=convertDate(startTime)%>
                    </td>
                    <td><%=jobServer%>
                    </td>
                    <td><%=convertDate(prevFireTime)%>
                    </td>
                    <td><%=convertDate(nextFireTime)%>
                    </td>
                </tr>
                <%
                    }
                %>
            </table>
        </div>
    </div>
    <%
        }
    %>
</div>
<br>
<br>
<br>
<%@include file="../../../copyright.html" %>
</body>
</html>