var app = angular.module('angularApp', [
    'ui.bootstrap'
    , 'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        return true;
    };
}).controller('angularAppCtrl', ['$scope', '$interval', '$uibModal', 'blockUI', 'AjaxService', 'marsContext', function ($scope, $interval, $uibModal, blockUI, AjaxService, marsContext) {
    $scope.bgColor = themeBgColor;
    $scope.textColor = themeTextColor;
    var blockUIStarted = false;

    function startBlockUI(msg) {
        blockUI.start(msg);
        blockUIStarted = true;
    }

    function stopBlockUI() {
        blockUI.stop();
        blockUIStarted = false;
    }

    $scope.startBlockUI = function (msg) {
        startBlockUI(msg);
    }

    $scope.stopBlockUI = function () {
        $scope.$apply(function () {
            stopBlockUI();
        });
    }

    $scope.reloadConf = function (service, serviceTitle, msg, appName) {
        startBlockUI(msg);
        reloadConf(service, serviceTitle, "../mgmt/", appName ? "moon=" + encodeURIComponent(appName) : appName);
    }

    $scope.reloadApp = function (msg, appName) {
        $scope.reloadConf('Moons', 'Reloading Applications', msg, appName)
    }

    $scope.refresh = function () {
        location.reload();
    }

    function adjustAppName(appName) {
        return appName ? appName.replace(/\s/g, '').replace(/-/g, '').replace(/\./g, '') : appName;
    }

    $scope.uploadApp = function (dist) {
        var scope = $scope;
        $uibModal.open({
            backdrop: false,
            templateUrl: marsContext.getTemplateUrl('/admin/tools/application/application-upload.html'),
            size: 'xl',
            controller: ['$scope',
                function ($scope) {
                    var config = $scope.config = {
                        title: "Upload Application",
                        uploadUrl: marsContext.contextPath + '/services/file/run/bio.distribution-UploadApplication@.html?tpl=/core/ServiceResultCallback.html'
                    };

                    $scope.scope = scope;
                    $scope.baseUrl = location.protocol + "://" + location.host + marsContext.contextPath;
                    $scope.item = dist || {
                        type: "APPLICATION",
                        auto: "N",
                        method: "UNZIP"
                    }

                    $scope.adjustAppName = function (item) {
                        item.appName = adjustAppName(item.appName);
                    }

                    $scope.isReadyToUpload = function () {
                        return $scope.item.appName;
                    };

                    $scope.upload = function () {
                        var f = document.getElementById("uploadFile");
                        if (f.value && f.value != "") {
                            $scope.adjustAppName($scope.item);
                            mars$util$.createHiddenFrame(1);
                            $scope.uploading = true;
                            var form = document.forms[0];
                            form.location.value = "/apps/" + $scope.item.appName;
                            form.action = config.uploadUrl;
                            startBlockUI('Uploading...');
                            form.submit();
                        } else {
                            angularExt.getBootboxObject().alert("Please select a zip file that have all files of an application");
                        }
                    };

                    $scope.serviceExceptionCallback = function () {
                        stopBlockUI();
                        $scope.uploading = false;
                        $scope.cancel();
                    }

                    $scope.serviceResultCallback = function () {
                        stopBlockUI();
                        $scope.uploading = false;
                        $scope.cancel();
                        scope.reloadApp("Deploying applications", $scope.item.appName);
                        angularExt.getBootboxObject().alert("The application has been uploaded and is being deployed.");
                    };

                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                    $scope.ok = function () {
                        $scope.$close();
                    };
                }
            ]
        }).result.then(function () {
        }, function () {
        });
    };

    function deleteApp(appName) {
        var api = new AjaxService(marsContext.contextPath + '/services/file/run/bio.application-DeleteApplication@.json');
        api.post({
            success: function (o) {
                stopBlockUI();
                $scope.reloadApp("Undeploying " + appName + " application", appName);
                angularExt.getBootboxObject().alert(appName + "application has been deleted and is being undeployed.");
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        }, {
            appName: appName
        });
    }

    $scope.deleteApp = function (appName) {
        bootbox.confirm({
            message: "Are you sure to delete the application '" + appName + "'?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn btn-sm btn-secondary'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn btn-sm btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    deleteApp(appName)
                }
            }
        });
    }

    function backupApp(appName) {
        var api = new AjaxService(marsContext.contextPath + '/services/dist/backup/app/' + appName + '.json');
        startBlockUI("The application '" + appName + "' backup is in progress. It may take some time.")
        api.get({
            success: function (o) {
                stopBlockUI();
                angularExt.getBootboxObject().alert("The application '" + appName + "' backup is completed.");
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    $scope.backupApp = function (appName) {
        backupApp(appName);
    }

    function backupSystemApplication() {
        var api = new AjaxService(marsContext.contextPath + '/services/dist/backup/system.json');
        startBlockUI("System backup is in progress. It may take some time.")
        api.get({
            success: function (o) {
                stopBlockUI();
                angularExt.getBootboxObject().alert("System application backup is completed.");
            },
            error: function (o) {
                stopBlockUI();
                o.alertException();
            }
        });
    }

    $scope.backupSystemApplication = function () {
        backupSystemApplication();
    }

    $scope.updateApp = function (appName) {
        var api = new AjaxService(marsContext.contextPath + '/services/data/get/bio.distribution-GetDistribution.json');
        api.post({
            success: function (o) {
                if (o.data && o.data.length === 1) {
                    $scope.uploadApp(o.data[0]);
                } else {
                    $scope.uploadApp({
                        appName: appName,
                        type: "APPLICATION",
                        auto: "N",
                        method: "UNZIP"
                    });
                }
            },
            error: function (o) {
                o.alertException();
            }
        }, {
            appName: appName,
            type: "APPLICATION"
        });
    }

    // Download file {
    var _fileName = null;
    var _reqKey;

    function isWaitingFile() {
        var cookieName = "DOWNLOAD-FILE-DONE" + _reqKey;
        var loadState = mars$util$.getCookie(cookieName);
        console.log("cookieName", cookieName, loadState, _fileName);
        if (loadState === _fileName) {
            mars$util$.deleteCookie(cookieName, marsContext.contextPath);
            _fileName = null;
            $interval.cancel(_timer);
            stopBlockUI();
        }

        cookieName = "BIO-EXCEPTION" + _reqKey;
        var bioException = mars$util$.getCookie(cookieName);
        if (bioException && -1 !== bioException.indexOf(_fileName)) {
            mars$util$.deleteCookie(cookieName, marsContext.contextPath);
            $interval.cancel(_timer);
            stopBlockUI();
            var frameDoc = mars$util$.getFrameDocument("hiddenGround");
            var errorMsg = JSON.parse(frameDoc.body.innerText);
            angularExt.getBootboxObject().alert(errorMsg.faultMessage || errorMsg.faultString);
        }
    }

    var _timer = null;
    $scope.downloadApp = function (appName) {
        $scope.startBlockUI("Download...");
        _fileName = appName ? appName + "-application.zip" : marsContext.contextPath.substring(1) + ".war";
        _reqKey = "-" + new Date().getTime();
        document.getElementById('hiddenGround').src = marsContext.contextPath
            + (appName ? '/services/dist/download/application/' + appName + '/' : '/services/dist/download/application/')
            + encodeURIComponent(_fileName) + "?$RK=" + _reqKey;
        _timer = $interval(isWaitingFile, 1000);
    }
    // } Download File
}]);

function uploadFileChanged(id) {
    try {
        var scope = angularExt.getAngularScope("#application-upload");
        var input = document.getElementById(id);
        var name = input.files[0].name;
        var idx = name.indexOf("-");
        if (-1 != idx) {
            var appName = name.substring(0, idx);
            scope.item.appName = appName;
            document.getElementById("appName").value = appName;
            scope.$apply();
        }
    } catch (e) {
    }
}

function reloadPage() {
    setTimeout(function () {
        this.location = location.href;
    }, 2000);
}
