<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.Moon" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Properties" %>
<%@include file="../../auth-admin.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<%!
    boolean hasProperties(Properties properties) {
        if (null != properties) {
            Iterator nameIt = properties.keySet().iterator();
            while (nameIt.hasNext()) {
                String propertyName = (String) nameIt.next();
                if (!"version".equals(propertyName) && !"title".equals(propertyName)) {
                    return true;
                }
            }
        }

        return false;
    }
%>

<%
    Collection<Moon> moonCollection = MoonManager.getInstance().getMoonCollection();
%>
<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Application Management</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/ui-bootstrap4/dist/ui-bootstrap-tpls-3.0.6.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");
        mars$require$.link("../../../tools/include/tool-common.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("../mgmt/reload-config.js");
        mars$require$.script("application-manager.js");
    </script>
    <style>
        .label {
            padding-left: 25px;
        }

        .bootbox.modal {
            z-index: 1000000 !important;
        }
    </style>
</head>
<body ng-controller="angularAppCtrl" ng-cloak="true" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<div class="page-header" style="text-align: center;">
    <h1>Application Management</h1>
</div>
<div class="container">
    <div class="alert alert-success" role="alert" id="applyAlert" style="display: none;">
        <span id="serviceName"></span> is done successfully.
        <button type="button" class="close" aria-label="Close" onclick="applyOk();reloadPage();"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="text-right" style="padding-bottom: 10px;">
        <button type="button" class="btn btn-sm btn-success" ng-click="uploadApp();$event.stopPropagation();">Upload & Deploy New Application</button>
        <button type="button" class="btn btn-warning btn-xs" ng-click="reloadApp();$event.stopPropagation();">Reload All Applications</button>
        <button type="button" class="btn btn-secondary btn-sm" ng-click="refresh();$event.stopPropagation();">Refresh</button>
    </div>

    <%
        Moon mars = MoonManager.getInstance().getMars();
        String bioName = mars.getName();
    %>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#<%=bioName%>" aria-expanded="false" aria-controls="#<%=bioName%>">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            <%=mars.getTitle()%>
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-info" ng-click="downloadApp();$event.stopPropagation();">Download</button>
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="backupSystemApplication();$event.stopPropagation();">Backup</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="<%=bioName%>">
            <div>
                <div class="row">
                    <label class="col-sm-2 label">Application Name</label>
                    <div class="col-sm-10">
                        <%=bioName%>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">Title</label>
                    <div class="col-sm-10">
                        <%=mars.getTitle()%>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">Version</label>
                    <div class="col-sm-10">
                        <%=StringUtil.toString(mars.getVersion(), "--UNKNOWN--")%>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">URI</label>
                    <div class="col-sm-10">
                        <a target="<%=bioName%>" href="<%=request.getContextPath()%>">
                            <%=request.getContextPath()%>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">Path</label>
                    <div class="col-sm-10">
                        <%=mars.getMoonDir()%>
                    </div>
                </div>
            </div>
            <div>
                <div class="row">
                    <label class="col-sm-2 label">Properties</label>
                    <div class="col-sm-10">
                        <%
                            Properties properties = mars.getProperties();
                            if (hasProperties(properties)) {
                        %>
                        <table class="table table-sm table-striped table-condensed table-font-small">
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <%
                                    Iterator nameIt = properties.keySet().iterator();
                                    while (nameIt.hasNext()) {
                                        String propertyName = (String) nameIt.next();
                                        if (!"version".equals(propertyName) && !"title".equals(propertyName)) {
                                            String value = properties.getProperty(propertyName);
                                %>
                                <td class="property" style="width: 100px;white-space: nowrap;">
                                    <label id="<%=propertyName%>"><%=propertyName%>
                                    </label>
                                </td>
                                <td class="property" width="50%"><%=value%>
                                </td>
                            </tr>
                            <%
                                    }
                                }
                            %>
                        </table>
                        <%
                        } else {
                        %>
                        None
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <% if (null != moonCollection) {
        for (Moon moon : moonCollection) {
    %>
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
        <div class="card-header card-header-xs border-secondary" data-toggle="collapse" data-target="#<%=moon.getName()%>" aria-expanded="false" aria-controls="#<%=moon.getName()%>">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="card-title">
                            <%=moon.getTitle()%>
                        </h3>
                    </td>
                    <td width="95%" style="text-align: right;">
                        <button type="button" class="btn btn-xs2 btn-primary" ng-click="updateApp('<%=moon.getName()%>');$event.stopPropagation();">Update</button>
                        <button type="button" class="btn btn-xs2 btn-info" ng-click="downloadApp('<%=moon.getName()%>');$event.stopPropagation();">Download</button>
                        <button type="button" class="btn btn-xs2 btn-warning" ng-click="reloadApp('Reloading application','<%=moon.getName()%>');$event.stopPropagation();">Reload</button>
                        <button type="button" class="btn btn-xs2 btn-secondary" ng-click="backupApp('<%=moon.getName()%>');$event.stopPropagation();">Backup</button>
                        <button type="button" class="btn btn-xs2 btn-danger" ng-click="deleteApp('<%=moon.getName()%>');$event.stopPropagation();">Delete</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-block collapse show text-dark bg-white" id="<%=moon.getName()%>">
            <div>
                <div class="row">
                    <label class="col-sm-2 label">Application Name</label>
                    <div class="col-sm-10">
                        <%=moon.getName()%>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">Title</label>
                    <div class="col-sm-10">
                        <%=moon.getTitle()%>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">Version</label>
                    <div class="col-sm-10">
                        <%=StringUtil.toString(moon.getVersion(), "--UNKNOWN--")%>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">URI</label>
                    <div class="col-sm-10">
                        <a target="<%=moon.getName()%>" href="<%=request.getContextPath()%>/<%=moon.getMoonUri()%>">
                            <%=request.getContextPath()%>/<%=moon.getMoonUri()%>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 label">Path</label>
                    <div class="col-sm-10">
                        <%=moon.getMoonDir()%>
                    </div>
                </div>
            </div>
            <div>
                <div class="row">
                    <label class="col-sm-2 label">Properties</label>
                    <div class="col-sm-10">
                        <%
                            properties = moon.getProperties();
                            if (hasProperties(properties)) {
                        %>
                        <table class="table table-sm table-striped table-condensed table-font-small">
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <%
                                    Iterator nameIt = properties.keySet().iterator();
                                    while (nameIt.hasNext()) {
                                        String propertyName = (String) nameIt.next();
                                        if (!"version".equals(propertyName) && !"title".equals(propertyName)) {
                                            String value = properties.getProperty(propertyName);
                                %>
                                <td class="property" style="width: 100px;white-space: nowrap;">
                                    <label id="<%=propertyName%>"><%=propertyName%>
                                    </label>
                                </td>
                                <td class="property" width="50%"><%=value%>
                                </td>
                            </tr>
                            <%
                                    }
                                }
                            %>
                        </table>
                        <%
                        } else {
                        %>
                        None
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%
        }
    } else {
    %>
    None
    <%
        }
    %>
</div>
<iframe name="hiddenGround" id="hiddenGround" src="../blank.html" frameborder="0" width="5" height="5" style="visibility: hidden;"></iframe>
<%@include file="../../../copyright.html" %>
</body>
</html>