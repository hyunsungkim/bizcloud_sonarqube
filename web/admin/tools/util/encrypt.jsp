<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.security.util.SecurityEncryptUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String valueToEncrypt = request.getParameter("valueToEncrypt");

    if (valueToEncrypt == null)
        valueToEncrypt = "";

    String encryptedValue = null;

    if (valueToEncrypt != null && valueToEncrypt.length() > 0) {
        encryptedValue = SecurityEncryptUtil.encryptToBase64(valueToEncrypt);
    }

%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Encrypt Tool</title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="page-header" style="text-align: center;">
    <h1>Encrypt Tool</h1>
</div>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools"><h3 class="panel-title">Encrypt</h3></div>
        <div class="panel-body">
            <form method="POST">
                <div class="form-group">
                    <label for="valueToEncrypt">Source Value</label>
                    <input type="text" name="valueToEncrypt" id="valueToEncrypt" value="<%=Encode.forHtmlAttribute(valueToEncrypt)%>" placeholder="Enter value to encrypt" class="form-control" autofocus>
                </div>
                <div style="text-align: center;">
                    <input class="btn btn-default" type="submit" value="Encrypt">
                </div>
                <% if (encryptedValue != null) {%>
                <label>Encrypted Value</label>&nbsp;
                <small>(Copy the text inside the box below)</small>
                <div class="well">
                    <span style="border: 1px #c2c2c2 solid; padding: 2px;"><%=encryptedValue%></span>
                </div>
                <% } %>
            </form>
        </div>
    </div>
</div>
<%@include file="../../../copyright.html" %>
</body>
</html>