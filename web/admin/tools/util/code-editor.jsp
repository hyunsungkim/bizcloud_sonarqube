<%@ page import="com.bizflow.io.core.file.util.FileIOUtil" %>
<%@ page import="com.bizflow.io.core.net.util.MimeUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.File" %>
<%@include file="../../../auth/auth.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);

    String filename = request.getParameter("f");
    if ("POST".equalsIgnoreCase(request.getMethod()) && null != filename) {
        if (filename.startsWith("/WEB-INF/") || filename.startsWith("/apps/")) {
            if (!AcmManager.getInstance().isValidAdminRequest(request)) {
                response.sendRedirect(request.getContextPath() + "/admin/access-denied.jsp");
            }
        }
        String filePath = application.getRealPath(filename);
        File file = new File(filePath);
        if (null != file && file.exists()) {
            boolean saved = false;
            String contents = request.getParameter("c");
            if (null != contents) {
                FileIOUtil.write(file, contents.getBytes("UTF-8"));
                saved = true;
            }
            contents = FileIOUtil.getContents(file);
            boolean editMode = "e".equalsIgnoreCase(request.getParameter("m"));
            boolean hideTittle = "".equalsIgnoreCase(request.getParameter("t"));

            String codeMode = MimeUtil.getContentType(filename);
%>
<!DOCTYPE html>
<html id="ng-app">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HTML Editor</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/codemirror/lib/codemirror.css">

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/codemirror/lib/codemirror.js"></script>
    <script src="../../../includes/node_modules/codemirror/mode/xml/xml.js"></script>
    <script src="../../../includes/node_modules/codemirror/mode/javascript/javascript.js"></script>
    <script src="../../../includes/node_modules/codemirror/mode/htmlmixed/htmlmixed.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/bizflow/css/theme/<%=theme%>/bootstrap/bootstrap.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("../../../includes/node_modules-ext/mars/mars.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("code-editor.js");
    </script>
    <style>
        #Editor .CodeMirror {
            height: 100%;
        }
    </style>
    <script>
        var _mode = "<%=codeMode%>";
    </script>
</head>

<body onload="_onload();" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">
<%if (!hideTittle) {%>
<div class="page-header text-center">
    <h1>Edit <%=Encode.forHtmlContent(filename)%>
    </h1>
    <%if (editMode && "/WEB-INF/classes/quartz_data.xml".equalsIgnoreCase(filename)) {%>
    <span class="alert-danger">&nbsp;&nbsp;<strong>Be careful!!</strong> Quartz Jobs will be also applied as well as saving the changes when you click Save button&nbsp;&nbsp;</span>
    <br><span>Use <a target="_blank" href="http://www.cronmaker.com/">Cron Maker</a> to make a cron expression</span>
    and <span>consult <a target="_blank" href="http://www.quartz-scheduler.org/documentation/quartz-1.x/tutorials/crontrigger">Quartz Tutorial</a> to make a cron expression&nbsp;&nbsp;</span>
    <%}%>
</div>
<%}%>
<form method="post">
    <div class="container-fluid" id="box">
        <% if (saved) {%>
        <div class="alert alert-success alert-dismissible" role="alert" id="saveAlert">
            <%=Encode.forHtmlContent(filename)%> has been saved successfully.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <%}%>
        <div class="card fill-height text-<%=textColor%> bg-<%=bgColor%> border-secondary">
            <div class="card-header" id="heading">
                <div class="row">
                    <div class="col-md-10">
                        <h4><%=Encode.forHtmlContent(filename)%>
                        </h4>
                    </div>
                    <div class="col-md-2 text-right">
                        <%if (editMode) {%>
                        <button class="btn btn-secondary btn-sm" type="submit" value="Save" style="width: 100px;" accesskey="s">Save</button>
                        <%}%>
                    </div>
                </div>
            </div>
            <div id="Editor" class="fill-height">
                <textarea class="fill" name="c" id="textarea" <%=editMode ? "" : "readonly"%>><%=HtmlUtil.getHtmlText(contents)%></textarea>
            </div>
        </div>
        <input type="hidden" name="f" value="<%=Encode.forHtmlAttribute(filename)%>">
    </div>
    <input type="hidden" name="t" value="<%=Encode.forHtmlAttribute(request.getParameter("t"))%>">
    <input type="hidden" name="m" value="<%=Encode.forHtmlAttribute(request.getParameter("m"))%>">
</form>
<%@include file="../../../copyright.html" %>
</body>
</html>
<%
        }
    }
%>
</body>
</html>
