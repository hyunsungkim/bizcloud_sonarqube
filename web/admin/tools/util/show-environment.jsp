<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil,org.owasp.encoder.Encode" %>
<%@ page import="java.io.UnsupportedEncodingException" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.Map" %>

<%!
    /**
     * Print Application Property
     *
     * @param application ServletContext Object
     * @param out JspWriter Object
     * @since 3.4.4
     */
    private void prtApplicationProperty(ServletContext application, JspWriter out) {
        try {
            out.println("<TABLE class='table table-striped table-condensed'  id='SystemProperty'>");

            out.print("<TR><TD class='header'>Application Version</TD><TD class='value'>");
            out.print(application.getServerInfo());
            out.println("</TD></TR>");

            out.print("<TR><TD class='header'>Servlet Version</TD><TD class='value'>");
            out.print(application.getMajorVersion());
            out.println(".");
            out.print(application.getMinorVersion());
            out.println("</TD></TR>");

            out.print("<TR><TD class='header'>JSP Version</TD><TD class='value'>");
            out.print(JspFactory.getDefaultFactory().getEngineInfo().getSpecificationVersion());
            out.println("</TD></TR>");

            out.print("<TR><TD class='header'>Servlet Context Name</TD><TD class='value'>");
            out.print(application.getServletContextName());
            out.println("</TD></TR>");

            out.print("<TR><TD class='header'>Virtual Server Name</TD><TD class='value'>");
            out.print(application.getVirtualServerName());
            out.println("</TD></TR>");

            Enumeration enuM = application.getAttributeNames();
            while (enuM.hasMoreElements()) {
                String name = (String) enuM.nextElement();
                out.print("<TR><TD class='header'>");
                out.print(name);
                out.print("</TD><TD class='value'>");
                out.print(application.getAttribute(name));
                out.println("</TD></TR>");
            }

            out.println("</TABLE>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Print System Property
     */
    private void prtSystemProperty(JspWriter out) throws Exception {
        try {
            out.println("<TABLE class='table table-striped table-condensed'  id='SystemProperty'>");
            Enumeration enuM = System.getProperties().propertyNames();
            while (enuM.hasMoreElements()) {
                String name = (String) enuM.nextElement();
                out.print("<TR><TD class='header'>" + name + "</TD><TD class='value'>" + System.getProperties().getProperty(name));
                out.println("</TD></TR>");
            }
            out.println("</TABLE>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Print Request Property
     */
    private void prtReqProperty(JspWriter out, HttpServletRequest request) throws Exception {
        try {
            out.println("<TABLE class='table table-striped table-condensed'  id='RequestProperty'>");
            out.print("<TR><TD class='header'>encoding type</TD><TD class='value'>" + System.getProperty("file.encoding"));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Content-Type</TD><TD class='value'>" + request.getContentType());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Content-Length</TD><TD class='value'>" + request.getContentLength());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Request Method</TD><TD class='value'>" + request.getMethod());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Remote Address</TD><TD class='value'>" + request.getRemoteAddr());
            out.println(" (" + ServletUtil.getRemoteAddress(request) + ")");
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Client IP Address</TD><TD class='value'>" + ServletUtil.getClientIPAddress(request));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Remote Host</TD><TD class='value'>" + request.getRemoteHost());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Remote User</TD><TD class='value'>" + request.getRemoteUser());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Authentication Type</TD><TD class='value'>" + request.getAuthType());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Schme</TD><TD class='value'>" + request.getScheme());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Protocol</TD><TD class='value'>" + request.getProtocol());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>CharacterEncoding</TD><TD class='value'>" + request.getCharacterEncoding());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Server Name</TD><TD class='value'>" + request.getServerName());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Server Port</TD><TD class='value'>" + request.getServerPort());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Translated Request Path Info</TD><TD class='value'>" + request.getPathTranslated());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Servlet Path</TD><TD class='value'>" + request.getServletPath());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Path Info</TD><TD class='value'>" + request.getPathInfo());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Request URI</TD><TD class='value'>" + request.getRequestURI());
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>Request URL</TD><TD class='value'>" + request.getRequestURL());
            out.println("</TD></TR>");
            String szTemp = (String) request.getQueryString();
            if (szTemp != null) {
                out.print("<TR><TD class='header'>Origin Request Query String</TD><TD class='value'>" + new String(szTemp.getBytes("8859_1"), "KSC5601"));
                out.println("</TD></TR>");
            }
            out.println("</TABLE>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Print QueryString Convert
     */
    private void prtStringCodeConvert(JspWriter out, String str) throws Exception, NullPointerException, UnsupportedEncodingException {
        try {
            out.println("<TABLE class='table table-striped table-condensed'  id='ParameterEncoded'>");
            out.print("<TR><TD class='header'>8859_1</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("8859_1"))));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>KSC5601</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("KSC5601"))));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>UTF8</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("UTF8"))));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>8859_1->8859_1</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("8859_1"), "8859_1")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>8859_1->KSC5601</TD><TD>" + Encode.forHtmlContent(new String(str.getBytes("8859_1"), "KSC5601")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>8859_1->UTF8</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("8859_1"), "UTF8")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>KSC5601->8859_1</TD><TD>" + Encode.forHtmlContent(new String(str.getBytes("KSC5601"), "8859_1")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>KSC5601->KSC5601</TD><TD>" + Encode.forHtmlContent(new String(str.getBytes("KSC5601"), "KSC5601")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>KSC5601->UTF8</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("KSC5601"), "UTF8")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>UTF8->8859_1</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("UTF8"), "8859_1")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>UTF8->KSC5601</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("UTF8"), "KSC5601")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>UTF8->UTF8</TD><TD class='value'>" + Encode.forHtmlContent(new String(str.getBytes("UTF8"), "UTF8")));
            out.println("</TD></TR>");
            out.print("<TR><TD class='header'>8859_1 Remove All Space Char</TD><TD>" + Encode.forHtmlContent((new String(str.getBytes("8859_1"))).trim()));
            out.println("</TD></TR>");
            out.println("</TABLE>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Print Request Header
     */
    private void prtReqHeader(JspWriter out, HttpServletRequest request) throws Exception {
        try {
            out.println("<TABLE class='table table-striped table-condensed' id='RequestHeader'>");
            Enumeration enuM = request.getHeaderNames();
            while (enuM.hasMoreElements()) {
                String name = (String) enuM.nextElement();
                out.print("<TR><TD class='header'>" + name + "</TD><TD class='value'>" + request.getHeader(name));
                out.println("</TD></TR>");
            }
            out.println("</TABLE>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Servlet? Parameter ?? Hashtable? ????.
     */
    private Map getSvtParam(HttpServletRequest request) throws Exception {
        Map hashtable = new LinkedHashMap();

        try {
            Enumeration enuM = request.getParameterNames();

            while (enuM.hasMoreElements()) {
                String name = (String) enuM.nextElement();
                String[] values = request.getParameterValues(name);
                if (values != null) {
                    if (values.length == 1) {
                        hashtable.put(name, values[0]);
                    } else {
                        for (int i = 0; i < values.length; i++) {
                            hashtable.put(name + " [" + i + "]", values[i]);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return hashtable;
    }

    /**
     * Print Servlet Parameter
     */
    private void prtSvtParam(JspWriter out, Map hash) throws Exception {
        try {
            out.println("<TABLE class='table table-striped table-condensed'  id='Parameter'>");
            Iterator enuM = hash.keySet().iterator();
            boolean isParam = false;
            while (enuM.hasNext()) {
                String name = (String) enuM.next();
                out.print("<TR><TD class='header'>" + Encode.forHtmlContent(name) + "</TD><TD class='value'>" + Encode.forHtmlContent((String) hash.get(name)));
                out.println("</TD></TR>");
                isParam = true;
            }
            if (!isParam) {
                out.print("<TR><TD style='text-align: center'>No parameter</TD></TR>");
            }
            out.println("</TABLE>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Print attributes stored at session
     */
    private void prtSessionAttributes(JspWriter out, HttpServletRequest request) throws Exception {
        try {
            out.println("<TABLE class='table table-striped table-condensed'  id='SessionAttribute'>");
            HttpSession session = request.getSession();
            Enumeration enuM = session.getAttributeNames();
            boolean exist = false;
            while (enuM.hasMoreElements()) {
                String name = (String) enuM.nextElement();
                out.print("<TR><TD class='header'>" + name + "<TD><textarea style='width:100%' rows='1'>" + session.getAttribute(name) + "</textarea>");
                out.println("</TD></TR>");
                exist = true;
            }

            if (!exist) {
                out.println("<TR><TD style='text-align:center;'>No session attribute</TD></TR>");
            }
            out.println("</TABLE>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

%>

<%
    try {
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Environment Information</title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <style>
        td.header {
            width: 150px;
            white-space: nowrap;
            border-right: 1px solid #c0c0c0;
        }

        .collapse.in {
            overflow: scroll;
        }

        .table {
            margin-bottom: 0px;
        }

        textarea {
            border: transparent !important;
        }

    </style>
    <script>
        function openNewWin() {
            window.open("show-environment.jsp");
        }
    </script>
</head>
<body>
<div class="page-header" style="text-align: center;">
    <h1>System Environment Information</h1>
</div>
<div class="container-fluid">
    <div class="text-right" style="padding-bottom: 5px">
        <span>
            Web Session <span class="badge"><%=request.getSession().getId()%></span>
        </span>
    </div>

    <div style="text-align: center;">
        <div class="btn-group" role="group" aria-label="...">
            <div type="button" class="btn btn-default btn-xs"><a href="#ApplicationPropertyPanel">Application Property</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#SystemPropertyPanel">System Property</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#RequestHeaderPanel">Request Header</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#RequestPropertyPanel">Request Property</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#SessionAttributePanel">Session Attributes</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#ParameterPanel">Request Parameters</a></div>
        </div>
        <a class="btn btn-default btn-xs" href="show-environment.jsp">Refresh</a>
    </div>
    <br>

    <div class="panel panel-primary" id="ApplicationPropertyPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#ApplicationPropertyDiv">
            <h3 class="panel-title">Application Property</h3>
        </div>
        <div class="collapse in" id="ApplicationPropertyDiv">
            <%
                prtApplicationProperty(application, out);
            %>
        </div>
    </div>
    <div class="panel panel-primary" id="SystemPropertyPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#SystemPropertyDiv">
            <h3 class="panel-title">System Property</h3>
        </div>
        <div class="collapse in" id="SystemPropertyDiv">
            <%
                prtSystemProperty(out);
            %>
        </div>
    </div>
    <div class="panel panel-primary" id="RequestHeaderPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#RequestHeaderDiv">
            <h3 class="panel-title">Request Header</h3>
        </div>
        <div class="collapse in" id="RequestHeaderDiv">
            <%
                prtReqHeader(out, request);
            %>
        </div>
    </div>
    <div class="panel panel-primary" id="RequestPropertyPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#RequestPropertyDiv">
            <h3 class="panel-title">Request Property</h3>
        </div>
        <div class="collapse in" id="RequestPropertyDiv">
            <%
                prtReqProperty(out, request);
            %>
        </div>
    </div>
    <div class="panel panel-primary" id="SessionAttributePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#SessionAttributeDiv">
            <h3 class="panel-title">Session Attribute</h3>
        </div>
        <div class="collapse in" id="SessionAttributeDiv">
            <%
                prtSessionAttributes(out, request);
            %>
        </div>
    </div>
    <div class="panel panel-primary" id="ParameterPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#ParameterDiv">
            <h3 class="panel-title">Request Parameter</h3>
        </div>
        <div class="collapse in" id="ParameterDiv">
            <%
                String str = null;
                {
                    Map hash = getSvtParam(request);
                    prtSvtParam(out, hash);

                    Iterator enumK = hash.keySet().iterator();
                    if (enumK.hasNext()) {
                        str = (String) hash.get(enumK.next());
                    }
                }
            %>
        </div>
    </div>
    <%
        if (str != null) {
    %>
    <div class="panel panel-primary" id="ParameterEncodedPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#ParameterEncodedDiv">
            <h3 class="panel-title">Parameter Encode</h3>
        </div>
        <div id="ParameterEncodedDiv" class="collapse in">
            <div class="panel-body">
                <%
                    out.println("<FONT CLASS=\"tabletitle\">QueryString[<I>" + Encode.forHtmlContent(str) + "</I>] Code Convert</FONT>");
                %>
            </div>
            <%
                prtStringCodeConvert(out, str);
            %>
        </div>
    </div>
    <%
        }
    %>
</div>
<%@include file="../../../copyright.html" %>
</body>
</html>
<%
        out.flush();

    } catch (Exception e) {
        e.printStackTrace();
    }
%>
