<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.file.util.FileIOUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.File" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", -1);

    String filename = request.getParameter("f");
    if ("POST".equalsIgnoreCase(request.getMethod()) && null != filename && (filename.startsWith("/WEB-INF/") || filename.startsWith("/apps/"))) {
        String filePath = application.getRealPath(filename);
        File file = new File(filePath);
        if (null != file && file.exists()) {
            boolean saved = false;
            String contents = request.getParameter("c");
            if (null != contents) {
                FileIOUtil.write(file, contents.getBytes("UTF-8"));
                saved = true;
            }
            contents = FileIOUtil.getContents(file);
            boolean editMode = "e".equalsIgnoreCase(request.getParameter("m"));
            boolean hideTittle = "".equalsIgnoreCase(request.getParameter("t"));
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Editor <%=file.getName()%>
    </title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules-ext/mars/mars-util.js"></script>
    <script>
        function page_onload() {
            var obj = document.getElementById("box");
            if (obj) {
                var size = mars$util$.getClientSize();
                obj.style.height = (size.height - 200) + "px";

                var heading = document.getElementById("heading");
                var editor = document.getElementById("Editor");
                editor.style.paddingBottom = heading.clientHeight + "px";
            }
        }
    </script>
</head>
<body onload="page_onload();">
<%if (!hideTittle) {%>
<div class="page-header text-center">
    <h1>Edit <%=Encode.forHtmlContent(filename)%>
    </h1>
    <%if (editMode && "/WEB-INF/classes/quartz_data.xml".equalsIgnoreCase(filename)) {%>
    <span class="alert-danger">&nbsp;&nbsp;<strong>Be careful!!</strong> Quartz Jobs will be also applied as well as saving the changes when you click Save button&nbsp;&nbsp;</span>
    <br><span>Use <a target="_blank" href="http://www.cronmaker.com/">Cron Maker</a> to make a cron expression</span>
    and <span>consult <a target="_blank" href="http://www.quartz-scheduler.org/documentation/quartz-1.x/tutorials/crontrigger">Quartz Tutorial</a> to make a cron expression&nbsp;&nbsp;</span>
    <%}%>
</div>
<%}%>
<form method="post">
    <div class="container-fluid" id="box">
        <% if (saved) {%>
        <div class="alert alert-success alert-dismissible" role="alert" id="saveAlert">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <%=Encode.forHtmlContent(filename)%> has been saved successfully.
                    </td>
                    <td>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </td>
                </tr>
            </table>
        </div>
        <%}%>
        <div class="panel panel-primary fill-height">
            <div class="panel-heading" id="heading">
                <div class="row">
                    <div class="col-md-10"><h3 class="panel-title"><%=Encode.forHtmlContent(filename)%>
                    </h3></div>
                    <div class="col-md-2 text-right">
                        <%if (editMode) {%>
                        <button class="btn btn-default btn-xs" type="submit" value="Save" style="width: 100px;" accesskey="s">Save</button>
                        <%}%>
                    </div>
                </div>
            </div>
            <div id="Editor" class="fill-height">
                <textarea class="fill" name="c" id="textarea" <%=editMode ? "" : "readonly"%>><%=HtmlUtil.getHtmlText(contents)%></textarea>
            </div>
        </div>
        <input type="hidden" name="f" value="<%=Encode.forHtmlAttribute(filename)%>">
    </div>
    <input type="hidden" name="t" value="<%=Encode.forHtmlAttribute(request.getParameter("t"))%>">
    <input type="hidden" name="m" value="<%=Encode.forHtmlAttribute(request.getParameter("m"))%>">
</form>
<%@include file="../../../copyright.html" %>
</body>
</html>
<%
        }
    }
%>