<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../../auth/auth.jsp" %>
<%@include file="../../../tools/include/tool-common.jsp" %>

<!DOCTYPE html>
<html id="ng-app" ng-app="htmlEditorApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HTML Editor</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/fontawesome-free/css/all.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/summernote/dist/summernote.css">
    <%@include file="../../../tools/include/tool-common-css.jsp" %>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="../../../includes/node_modules/summernote/dist/summernote.min.js"></script>
    <script src="../../../includes/node_modules/angular-summernote/dist/angular-summernote.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap4/css/bootstrap-ext.css");
        mars$require$.link("../../../includes/node_modules-ext/mars/css/mars.css");

        mars$require$.script("html-editor.js");
    </script>
    <style>
        .note-popover.popover {
            max-width: none;
            display: none;
        }

        .note-toolbar {
            background-color: #f8f9fa;
            border-bottom: 1px solid #6c757d;
        }
    </style>
</head>

<body ng-controller="htmlEditorAppController" class="bioTool text-<%=textColor%> bg-<%=bgColor%>">

<div class="page-header text-center">
    <h1 class="no-margin">HTML Editor</h1>
</div>
<div class="container-fluid">
    <div class="card text-<%=textColor%> bg-<%=bgColor%> border-secondary">
        <div class="card-header  card-header-sm border-secondary bg-info"><h3><i class="fa fa-edit fa-fw" aria-hidden="true"></i> Compose</h3></div>
        <div class="card-body">
            <div class="collapse show max-width">
                <summernote ng-model="contents" config="options" class="html-contents"></summernote>
            </div>
            <div style="padding-top: 5px;"><h5>HTML Output</h5></div>
            <div class="text-center col-md-12 no-padding no-margin">
                <textarea style="width: 100%;height: 300px;">{{contents}}</textarea>
            </div>
        </div>
    </div>
</div>

<%@include file="../../../copyright.html" %>
</body>
</html>
