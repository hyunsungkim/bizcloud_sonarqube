'use strict';

angular.module('htmlEditorApp', [
    'ngSanitize'
    , 'summernote'
]).controller('htmlEditorAppController', ['$scope', function ($scope) {

    $scope.options = {
        height: 300,
        focus: true
    };

}]);

