<%@ page import="com.bizflow.io.core.db.util.DBUtil" %>
<%@ page import="com.bizflow.io.core.file.exception.NoMoreRecordProcessException" %>
<%@ page import="com.bizflow.io.core.file.processor.AccessTableProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.RecordProcessor" %>
<%@ page import="com.bizflow.io.core.file.util.AccessDatabaseUtil" %>
<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@ page import="com.bizflow.io.core.web.MultipartFile" %>
<%@ page import="com.bizflow.io.core.web.MultipartFormData" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.data.processor.RecordDBProcessor" %>
<%@ page import="com.healthmarketscience.jackcess.Column" %>
<%@ page import="com.healthmarketscience.jackcess.Database" %>
<%@ page import="com.healthmarketscience.jackcess.DatabaseBuilder" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@include file="../../auth-admin.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Data - Select</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("view-access-file.js");
    </script>
    <style>
        small.db-col-name {
            color: #808080;
            font-weight: normal;
        }

        small.db-col-type {
            color: #a0a0a0;
            font-weight: normal;
        }
    </style>
</head>

<%
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        MultipartFile multipartFile = null;
        String tableNameParam = request.getParameter("tn");
        long _maxRecNumber = -1;
        if (null != tableNameParam) {
            String _filename = request.getParameter("filename");
            String _file = request.getParameter("file");
            multipartFile = new MultipartFile(_filename, new File(_file));
        } else {
            MultipartFormData formData = new MultipartFormData(request, 1024 * 1024 * 5, 1024 * 1024 * 100, 1024 * 1024 * 100);
            multipartFile = formData.getFile("uploadFileToRepository");
            _maxRecNumber = formData.getParameterIntValue("maxRecNumber", 10);
        }

        String filename = multipartFile.getName();
        Database database = DatabaseBuilder.open(multipartFile.getFile());
        try {
            final long maxRecNumber = _maxRecNumber;
            Set<String> tableNames = database.getTableNames();
%>
<body onload="_onload();">
<div class="container-fluid">
    <div class="page-header text-center">
        <h1>Access File Information<br>
            <small><%=filename%>
                <%
                    if (null != tableNameParam) {
                %>
                / <%=tableNameParam%>
                <%
                    }
                %>
            </small>
        </h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title"><span class="glyphicon glyphicon-th-large"></span>&nbsp;<%=filename%> File Information</h1>
        </div>
        <div class="panel-body">
            <%
                Iterator<String> iterator = null;
                if (null != tableNameParam) {
                    List<String> list = new ArrayList();
                    list.add(tableNameParam);
                    iterator = list.iterator();
                } else {
                    iterator = tableNames.iterator();
                }

                while (iterator.hasNext()) {
                    String tableName = iterator.next();
                    AccessTableProcessor processor = new AccessTableProcessor(database, tableName);
            %>
            <div class="panel panel-primary">
                <div class="panel-heading" data-toggle="collapse" data-target="#<%=tableName%>">
                    <h1 class="panel-title">
                        &nbsp;<%=tableName%>
                    </h1>
                </div>
                <div class="panel-body">
                    <form target="<%=tableName%>" method="post">
                        Total Number of Record: <%=processor.getTotalRecordNumber()%>
                        <%if (!tableName.equalsIgnoreCase(tableNameParam)) {%>
                        <input type="hidden" name="file" value="<%=multipartFile.getFile().getAbsolutePath()%>">
                        <input type="hidden" name="filename" value="<%=filename%>">
                        <input type="hidden" name="tn" value="<%=tableName%>">
                        <button type="submit" class="btn btn-xs btn-info">View all</button>
                        <%}%>
                    </form>
                </div>
                <div class="collapse in" id="<%=tableName%>">
                    <table class="table table-striped table-condensed table-bordered">
                        <%
                            final JspWriter writer = out;
                            final List<Column> columnList = processor.getColumnList();

                            processor.process(new RecordProcessor() {
                                int columnSize = 0;

                                @Override
                                public void initializeProcess(long totalRecordNumber, List columnNameList) {
                                    try {
                                        columnSize = columnNameList.size();
                                        writer.print("<tr>");
                                        writer.print("<th>#</th>");
                                        int columnSize = columnNameList.size();
                                        for (int i = 0; i < columnSize; i++) {
                                            writer.print("<th>");
                                            writer.print(columnNameList.get(i));
                                            writer.print("<br><small class='db-col-name'>[");
                                            writer.print(RecordDBProcessor.makeTableColumnName((String) columnNameList.get(i)));
                                            writer.print("]</small> <small class='db-col-type'>(");
                                            try {
                                                writer.print(StringUtil.toString(AccessDatabaseUtil.getDataTypeString(columnList.get(i).getSQLType()), "unknown"));
                                                writer.print("/");
                                                writer.print(StringUtil.toString(DBUtil.getJdbcTypeName(columnList.get(i).getSQLType()), "unknown"));
                                            } catch (SQLException e) {
                                                writer.print("unknown");
                                            }
                                            writer.print(")</small></th>");
                                        }
                                        writer.print("</tr>");
                                    } catch (IOException e) {
                                    }
                                }

                                @Override
                                public void preprocessRecord(long recordNumber, long totalRecordNumber, int columnSize) {
                                    try {
                                        writer.print("<tr><td>");
                                        writer.print(recordNumber);
                                        writer.print("</td>");
                                    } catch (IOException e) {
                                    }
                                }

                                @Override
                                public void processColumn(long recordNumber, int columnIndex, int columnSize, Object value) {
                                    try {
                                        writer.print("<td>");
                                        writer.print(HtmlUtil.getHtmlText(StringUtil.toString(value, "")));
                                        writer.print("</td>");
                                    } catch (IOException e) {
                                    }
                                }

                                @Override
                                public void processRecord(long recordNumber, long totalRecordNumber) {
                                    try {
                                        writer.print("</tr>");
                                    } catch (IOException e) {
                                    }

                                    if (maxRecNumber > 0 && recordNumber == maxRecNumber) {
                                        if (recordNumber < totalRecordNumber) {
                                            try {
                                                writer.print("<tr>");
                                                writer.print("<td colspan=\"");
                                                writer.print(columnSize + 1);
                                                writer.print("\" class='text-center'>...</tr>");
                                            } catch (IOException e) {
                                            }
                                        }
                                        throw new NoMoreRecordProcessException();
                                    }
                                }

                                @Override
                                public void finalizeProcess() {
                                }
                            });


                        %>
                    </table>
                </div>
            </div>
            <%
                }
            %>
        </div>
    </div>
</div>
<br><br><br>
</body>
<%
    } finally {
        database.close();
        multipartFile.deleteOnExit();
    }
} else {
%>
<body onload="_onload();">
<div class="container">
    <div class="page-header text-center">
        <h1>Select Access File</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title"><span class="glyphicon glyphicon-th-large"></span>&nbsp;Select Access File<br>
                <small style="color: #7a7a7a;">Please select an access file.</small>
            </h1>
        </div>
        <div class="panel-body">
            <form method="post" enctype="multipart/form-data" accept-charset="UTF-8" class="form-horizontal">
                <div class="form-group">
                    <label for="uploadFile" class="col-sm-3 control-label">File:</label>
                    <div class="col-sm-9">
                        <input type="file" name="uploadFile" id="uploadFile" style="padding-bottom: 40px;" accept=".mdb,.accdb" class="form-control btn btn-default max-width" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="maxRecNumber" class="col-sm-3 control-label">Max Record Number to display:</label>
                    <div class="col-sm-9">
                        <input type="number" name="maxRecNumber" id="maxRecNumber" value="10" class="form-control" required/>
                    </div>
                </div>
                <div class="text-center" style="padding-top: 10px;">
                    <button type="submit" class="btn btn-default">Upload</button>
                    <button id="btnGroup1" type="button" class="btn btn-default" style="display: none;" onclick="closeWindow()">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<br><br><br>
</body>
<%
    }
%>
</html>
