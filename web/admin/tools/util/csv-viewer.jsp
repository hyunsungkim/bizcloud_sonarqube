<%@include file="../../auth-admin.jsp" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.bizflow.io.core.lang.util.StringGetter,
                 com.bizflow.io.core.web.util.HtmlUtil,
                 com.bizflow.io.core.web.util.ServletUtil,
                 org.apache.commons.csv.CSVFormat,
                 org.apache.commons.csv.CSVParser,
                 org.apache.commons.csv.CSVRecord"
%>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.util.Iterator" %>

<%!
    private String getRecordValue(CSVRecord record, int index, String defaultValue) {
        if (index >= 0 && index < record.size()) {
            String v = record.get(index);
            if (v.startsWith("=\"") && v.endsWith("\"")) {
                v = StringGetter.get(v, "=\"", "\"");
            }

            return v;
        } else return defaultValue;
    }
%>
<%
    String file = request.getParameter("f");
    String title = ServletUtil.getParameterValue(request, "t", "CSV Viewer");
    boolean noTitle = ServletUtil.getParameterBooleanValue(request, "nt", false);
    boolean pageOn = ServletUtil.getParameterBooleanValue(request, "po", false);
    if (null != file) {
        File csvFile = new File(application.getRealPath(file));
        if (csvFile.exists()) {

            FileReader csvFileReader = new FileReader(csvFile);
            CSVParser csvParser = null;

            try {
                csvParser = new org.apache.commons.csv.CSVParser(csvFileReader, CSVFormat.EXCEL.withDelimiter(','));
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>CSV Viewer</title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>

    <script>
        function page_onload() {
            try {
                var size = new Object();
                size.width = document.body.clientWidth;
                size.height = document.body.clientHeight;
                parent.onloadCsvViewer(size);
            } catch (e) {
            }
        }
    </script>
</head>
<body onload="page_onload();">
<%if (!noTitle) {%>
<div class="page-header" style="text-align: center;">
    <h1><%=Encode.forHtmlContent(title)%>
    </h1>
</div>
<%}%>
<div class='<%=noTitle ? "" : "container-fluid"%>'>
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#GenTools"><h3 class="panel-title"><%=Encode.forHtmlContent(title)%>
        </h3></div>
        <table class="table table-striped table-condensed" id="tableBox"
               data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
               data-show-export="true" <%=pageOn ? "data-pagination=\"true\"" : ""%>
               data-show-toggle="true" data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
            <%
                long recordNumber = 0;
                int headerColumnSize = 0;
                Iterator<CSVRecord> csvRecordIterator = csvParser.iterator();
                while (csvRecordIterator.hasNext()) {
                    CSVRecord csvRecord = csvRecordIterator.next();
                    recordNumber = csvRecord.getRecordNumber();
                    int columnSize = csvRecord.size();
                    if (1 == recordNumber) {  // header
                        headerColumnSize = csvRecord.size();
            %>
            <thead>
            <tr>
                <th style='text-align:right' data-field='sequence' data-sortable='true'>#</th>
                <%
                    for (int c = 0; c < columnSize; c++) {
                        String header = csvRecord.get(c);
                        if (null != header && header.length() > 0) {
                %>
                <th data-field='data<%=c+1%>' data-sortable='true'><%=header%>
                </th>
                <%
                        }
                    }
                %>
            </tr>
            <tbody>
            <%
            } else {    // body
                long indexNumber = recordNumber - 1;
            %>
            <tr>
                <td><%=indexNumber%>
                </td>
                <%
                    for (int c = 0; c < headerColumnSize; c++) {
                        String columnValue = getRecordValue(csvRecord, c, "");
                %>
                <td data-field='data<%=c+1%>' data-sortable='true'>
                    <%=HtmlUtil.getHtmlText(columnValue)%>
                </td>
                <%}%>
            </tr>
            <%
                    }
                }
            %>
            </tbody>
        </table>
    </div>
    </form>
</div>
<%@include file="../../../copyright.html" %>
</body>
</html>
<%
            } finally {
                if (null != csvParser) {
                    csvParser.close();
                }

                if (null != csvFileReader) {
                    csvFileReader.close();
                }
            }
        }
    }
%>