'use strict';

function setHeight() {
    var obj = document.getElementById("box");
    if (obj) {
        var size = mars$util$.getClientSize();
        obj.style.height = (size.height - 80) + "px";

        var heading = document.getElementById("heading");
        var editor = document.getElementById("Editor");
        editor.style.paddingBottom = heading.clientHeight + "px";
    }
}

function _onload() {
    setHeight();
    CodeMirror.fromTextArea(document.getElementById("textarea"), {
        mode: _mode
    });
}

