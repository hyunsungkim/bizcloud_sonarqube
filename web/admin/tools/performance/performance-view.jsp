<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@ page import="com.bizflow.io.core.log.performlog.PerformanceLog" %>
<%@ page import="com.bizflow.io.core.log.performlog.PerformanceLogAnalyzer" %>
<%@ page import="com.bizflow.io.core.log.performlog.PerformanceStatisticLog" %>
<%@ page import="com.bizflow.io.core.util.DateUtil" %>
<%@ page import="com.bizflow.io.core.util.LogUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="java.util.*" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>

<%!
    public String convertDate(Date date) {
        return null != date ? DateUtil.convert(date, "yyyy/MM/dd HH:mm:ss z") : "";
    }
%>
<%
    String type = ServletUtil.getParameterValue(request, "type", "Data");
    String prefix = ServletUtil.getParameterValue(request, "prefix", "data-performance");
    String status = ServletUtil.getParameterValue(request, "status", "OK");
    String logKey = ServletUtil.getParameterValue(request, "logKey", PerformanceLog.RequestPathLogKey);
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title><%=type%> Service Performance</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
    <script>
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
    </script>
    <script>
        function reload() {
            this.location.reload(true);
        }

        function reloadBy(type) {
            var url = mars$util$.replaceUrlParamValue(this.location.href, "logKey", type);
            this.location.href = url;
        }
    </script>
</head>
<body>
<div class="page-header" style="text-align: center;">
    <h1><%=type%> Service Performance</h1>
</div>

<%
    String performFileName = null;

    File toolDir = new File(application.getRealPath("/admin"));
    File rootDir = toolDir.getParentFile();
    File docDir = new File(rootDir, "doc");
    if (!docDir.exists()) {
        docDir.mkdirs();
    }

    File logFile = LogUtil.getLogFile(prefix);
    if (null != logFile) {
        File logDir = logFile.getParentFile();

        File file = new File(docDir, prefix + ".csv");
        performFileName = file.getName();

        PerformanceLogAnalyzer logAnalyzer = new PerformanceLogAnalyzer();
        logAnalyzer.setLogKeyName(logKey);
        logAnalyzer.setOkStatusOnly("OK".equalsIgnoreCase(status));

        if (file.exists()) file.delete();

        FileOutputStream fos = new FileOutputStream(file, true);

        logAnalyzer.setPerformanceOutputStream(fos);
%>
<div class="container-fluid">
    <div class="text-center">
        <div class="btn-group" role="group" aria-label="...">
            <div type="button" class="btn btn-default btn-xs"><a href="#AverageTimePanel">The Average Time</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#MinTimePanel">The Least Time</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#MaxTimePanel">The Greatest Time</a></div>
        </div>
        <a class="btn btn-default btn-xs" href="#" onclick="reload();">Refresh</a>
        <a class="btn btn-default btn-xs" target="_blank" href="<%=request.getContextPath()%>/doc/<%=performFileName%>">Download <%=performFileName%>
        </a>
        <%if (PerformanceLog.RequestPathLogKey.equals(logKey)) {%>
        <a class="btn btn-default btn-xs" href="#" onclick="reloadBy('operationName');">By Operator</a>
        <%} else {%>
        <a class="btn btn-default btn-xs" href="#" onclick="reloadBy('requestPath');">By URL</a>
        <%}%>
    </div>
    <br>

    <div class="panel panel-default" id="ProcessingPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#Processing"><h3 class="panel-title">Processing Logs</h3></div>
        <div class="panel-body collapse out overflow-auto" id="Processing">
            <%
                try {
                    File[] files = logDir.listFiles();
                    for (int i = 0; i < files.length; i++) {
                        File _file = files[i];
                        if (_file.isFile()) {
                            String filePath = _file.getAbsolutePath();
                            String fileName = _file.getName();
                            if (fileName.indexOf(prefix) != -1) {
                                logAnalyzer.analyze(filePath);
            %>
            <div>Processing <%=filePath%>
            </div>
            <%
                            }
                        }
                    }
                } finally {
                    fos.close();
                }
            %>
        </div>
    </div>
    <%
        Map<String, PerformanceStatisticLog> map = logAnalyzer.getPerformMap();
        Iterator it = map.keySet().iterator();
        List<String> nameList = new ArrayList<String>();

        while (it.hasNext()) {
            nameList.add((String) it.next());
        }

        Collections.sort(nameList);
    %>
    <div class="panel panel-primary" id="AverageTimePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#AverageTime"><h3 class="panel-title">The Average Time</h3></div>
        <div id="AverageTime" class="collapse in overflow-auto">
            <%if (null != logAnalyzer.getFirstLog()) {%>
            <div class="panel-body">From <strong><%=convertDate(logAnalyzer.getFirstLog().getDate())%>
            </strong> To <strong><%=convertDate(logAnalyzer.getLastLog().getDate())%>
            </strong></div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <%if (PerformanceLog.RequestPathLogKey.equalsIgnoreCase(logKey)) {%>
                    <th data-field='requestPath' data-sortable='true' class="header text-nowrap">URI</th>
                    <%} else {%>
                    <th data-field='operator' data-sortable='true' class="header text-nowrap">Operator</th>
                    <%}%>
                    <th data-field='count' data-sortable='true' class="header">Count</th>
                    <th data-field='totalTime' data-sortable='true' class="header">Total Time (ms)</th>
                    <th data-field='averageTime' data-sortable='true' class="header">Average Response Time (ms)</th>
                    <th data-field='leastTime' data-sortable='true' class="header">Least Response Time (ms)</th>
                    <th data-field='greatestTime' data-sortable='true' class="header">Greatest Response Time (ms)</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (String name : nameList) {
                        PerformanceStatisticLog staticsLog = map.get(name);
                        PerformanceLog minLog = staticsLog.getMinLog();
                        PerformanceLog maxLog = staticsLog.getMaxLog();
                %>
                <tr>
                    <%if (PerformanceLog.RequestPathLogKey.equalsIgnoreCase(logKey)) {%>
                    <td><%=StringUtil.toString(staticsLog.getRequestPath(), staticsLog.getOperationName())%>
                            <%} else {%>
                    <td><%=staticsLog.getOperationName()%>
                        <%}%>
                    </td>
                    <td><%=staticsLog.getCount()%>
                    </td>
                    <td><%=staticsLog.getTotalTime()%>
                    </td>
                    <td><%=staticsLog.getAverageResponseTime()%>
                    </td>
                    <td><%=minLog.getResponseTime()%>
                    </td>
                    <td><%=maxLog.getResponseTime()%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
            <%} else { %>
            <div class="panel-body">No Logs</div>
            <%}%>
        </div>
    </div>

    <div class="panel panel-primary" id="MinTimePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#MinTime"><h3 class="panel-title">The Least Request</h3></div>
        <div id="MinTime" class="collapse in overflow-auto">
            <%if (null != logAnalyzer.getFirstLog()) {%>
            <div class="panel-body">From <strong><%=convertDate(logAnalyzer.getFirstLog().getDate())%>
            </strong> To <strong><%=convertDate(logAnalyzer.getLastLog().getDate())%>
            </strong></div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <%if (PerformanceLog.RequestPathLogKey.equalsIgnoreCase(logKey)) {%>
                    <th data-field='requestPath' data-sortable='true' class="header text-nowrap">URI</th>
                    <%} else {%>
                    <th data-field='operator' data-sortable='true' class="header text-nowrap">Operator</th>
                    <%}%>
                    <th data-field='requestId' data-sortable='true' class="header">Request ID</th>
                    <th data-field='date' data-sortable='true' class="header">Date</th>
                    <th data-field='logUser' data-sortable='true' class="header">Log User</th>
                    <th data-field='userAddress' data-sortable='true' class="header">User Address</th>
                    <th data-field='responseTime' data-sortable='true' class="header text-nowrap">Response Time (ms)</th>
                    <th data-field='parameter' data-sortable='true' class="header">Parameter</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (String name : nameList) {
                        PerformanceStatisticLog staticsLog = map.get(name);
                        PerformanceLog minLog = staticsLog.getMinLog();
                %>
                <tr>
                    <%if (PerformanceLog.RequestPathLogKey.equalsIgnoreCase(logKey)) {%>
                    <td><%=StringUtil.toString(staticsLog.getRequestPath(), staticsLog.getOperationName())%>
                            <%} else {%>
                    <td><%=staticsLog.getOperationName()%>
                        <%}%>
                    </td>
                    <td><%=minLog.getRequestId()%>
                    </td>
                    <td class="text-nowrap"><%=convertDate(minLog.getDate())%>
                    </td>
                    <td><%=StringUtil.toString(minLog.getLogUser(), "")%>
                    </td>
                    <td><%=minLog.getLogUserIp()%>
                    </td>
                    <td><%=minLog.getResponseTime()%>
                    </td>
                    <td><%=HtmlUtil.getHtmlText(minLog.getParameter())%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
            <%} else {%>
            <div class="panel-body">No Logs</div>
            <%}%>
        </div>
    </div>

    <div class="panel panel-primary" id="MaxTimePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#MaxTime"><h3 class="panel-title">The Greatest Request</h3></div>
        <div id="MaxTime" class="collapse in overflow-auto">
            <%if (null != logAnalyzer.getFirstLog()) {%>
            <div class="panel-body">From <strong><%=convertDate(logAnalyzer.getFirstLog().getDate())%>
            </strong> To <strong><%=convertDate(logAnalyzer.getLastLog().getDate())%>
            </strong></div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <%if (PerformanceLog.RequestPathLogKey.equalsIgnoreCase(logKey)) {%>
                    <th data-field='requestPath' data-sortable='true' class="header text-nowrap">URI</th>
                    <%} else {%>
                    <th data-field='operator' data-sortable='true' class="header text-nowrap">Operator</th>
                    <%}%>
                    <th data-field='requestId' data-sortable='true' class="header">Request ID</th>
                    <th data-field='date' data-sortable='true' class="header">Date</th>
                    <th data-field='logUser' data-sortable='true' class="header">Log User</th>
                    <th data-field='userAddress' data-sortable='true' class="header">User Address</th>
                    <th data-field='responseTime' data-sortable='true' class="header text-nowrap">Response Time (ms)</th>
                    <th data-field='parameter' data-sortable='true' class="header">Parameter</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (String name : nameList) {
                        PerformanceStatisticLog staticsLog = map.get(name);
                        PerformanceLog maxLog = staticsLog.getMaxLog();
                %>
                <tr>
                    <%if (PerformanceLog.RequestPathLogKey.equalsIgnoreCase(logKey)) {%>
                    <td><%=StringUtil.toString(staticsLog.getRequestPath(), staticsLog.getOperationName())%>
                            <%} else {%>
                    <td><%=staticsLog.getOperationName()%>
                        <%}%>
                    </td>
                    <td><%=maxLog.getRequestId()%>
                    </td>
                    <td class="text-nowrap"><%=convertDate(maxLog.getDate())%>
                    </td>
                    <td><%=StringUtil.toString(maxLog.getLogUser(), "")%>
                    </td>
                    <td><%=maxLog.getLogUserIp()%>
                    </td>
                    <td><%=maxLog.getResponseTime()%>
                    </td>
                    <td><%=HtmlUtil.getHtmlText(maxLog.getParameter())%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
            <%} else {%>
            <div class="panel-body">No Logs</div>
            <%}%>
        </div>
    </div>
    <%}%>
</div>
<br><br>
<%@include file="../../../copyright.html" %>
</body>
</html>
