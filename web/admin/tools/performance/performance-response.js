var app = angular.module('angularApp', [
    'nvd3'
    , 'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'bio.angular.report'
]).config(function (blockUIConfig) {
    blockUIConfig.requestFilter = function (config) {
        if (config.url.match(/\/sys\/status\//)) {
            return false; // ... don't block it.
        }
        return true;
    };
});

app.controller('angularAppCtrl', ['$scope', '$timeout', 'AjaxService', 'marsContext', function ($scope, $timeout, AjaxService, marsContext) {
    $scope.param = {
        pageSize: 144,
        ORDER_BY: 'id desc'
    };

    $scope.status = {
        interval: '60'
    };
    $scope.refreshInterval = 5 * (60 * 1000);
    $scope.chartCallback = {
        onReady: function (chart) {
        }
    };

    function getSystemStatusInfo() {
        var api = new AjaxService(marsContext.contextPath + '/services/sys/status/simple.json');
        api.get({
            success: function (o) {
                $scope.status.data = o.data;
            },
            error: function (o) {
                o.alertException();
            }
        });

        var interval = mars$util$.parseInt($scope.status.interval, -1);
        if (interval > 0) {
            $timeout(function () {
                getSystemStatusInfo();
            }, interval * 1000);
        }
    }

    $scope.getSystemStatusInfo = function () {
        getSystemStatusInfo();
    };

    (function () {
        getSystemStatusInfo();
    })();
}]);
