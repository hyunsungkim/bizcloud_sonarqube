angular.module('bio.angular.report', [
    'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
]).directive('performanceResponseLineChart', ['marsContext', function (marsContext) {
    return {
        restrict: 'EA',
        transclude: true,
        scope: {
            id: "=",
            param: "<",
            refreshInterval: "<",
            height: "<",
            margin: "<",
            callback: "="
        },
        link: function (scope, element, attrs) {
        },
        template: '<nvd3 id="id" options="options" data="data" class="with-3d-shadow with-transitions"></nvd3>',
        controller: 'performanceResponseLineChartController'
    };
}]).controller('performanceResponseLineChartController', ['$scope', '$timeout', 'AjaxService', 'marsContext', function ($scope, $timeout, AjaxService, marsContext) {
    if ("undefined" === typeof $scope.margin) {
        $scope.margin = {};
    }

    $scope.options = {
        chart: {
            type: 'lineChart',
            height: $scope.height || 450,
            margin: {
                top: $scope.margin.top || 50,
                right: $scope.margin.right || 35,
                bottom: $scope.margin.bottom || 50,
                left: $scope.margin.left || 70
            },
            x: function (d) {
                return d.x;
            },
            y: function (d) {
                return d.y;
            },
            useInteractiveGuideline: false,
            tooltip: {
                contentGenerator: function (e) {
                    var series = e.series[0];
                    var item = e.point.item;
                    var rows =
                        "<tr style='border-top: 1px solid #cecece;'><td class='key'>" + 'Date Slot: ' + "</td><td class='x-value'>" + item.dateKey + "</td></tr>" +
                        "<tr><td class='key'>" + 'Request Count: ' + "</td><td class='x-value'>" + item.count + "</td></tr>" +
                        "<tr><td class='key'>" + 'Average Response: ' + "</td><td class='x-value'>" + d3.format('.02f')(item.avgResponseTime) + " ms</td></tr>" +
                        "<tr style='border-top: 1px solid #cecece;'><td class='key'>" + 'Greatest Response: ' + "</td><td class='x-value'>" + d3.format('.02f')(item.greatestResponseTime) + " ms</td></tr>" +
                        "<tr><td class='key'>" + 'Greatest Response Path: ' + "</td><td class='x-value'>" + item.greatestResponsePath + "</td></tr>" +
                        "<tr style='border-top: 1px solid #cecece;'><td class='key'>" + 'Least Response: ' + "</td><td class='x-value'>" + d3.format('.02f')(item.leastResponseTime) + " ms</td></tr>" +
                        "<tr><td class='key'>" + 'Least Response Path: ' + "</td><td class='x-value'>" + item.leastResponsePath + "</td></tr>";

                    var header =
                        "<tr>" +
                        "<td class='legend-color-guide'><div style='background-color: " + series.color + ";'></div></td>" +
                        "<td class='key'><strong>" + series.key + "</strong></td>" +
                        "</tr>";

                    return "<table><thead>" + header + "</thead><tbody>" + rows + "</tbody>" + "</table>";
                }
            },
            dispatch: {
                stateChange: function (e) {
                    if ($scope.callback && $scope.callback.stateChange) {
                        $scope.callback.stateChange(e);
                    }
                },
                changeState: function (e) {
                    if ($scope.callback && $scope.callback.changeState) {
                        $scope.callback.changeState(e);
                    }
                },
                tooltipShow: function (e) {
                    if ($scope.callback && $scope.callback.tooltipShow) {
                        $scope.callback.tooltipShow(e);
                    }
                },
                tooltipHide: function (e) {
                    if ($scope.callback && $scope.callback.tooltipHide) {
                        $scope.callback.tooltipHide(e);
                    }
                },
                renderEnd: function (e) {
                    if ($scope.callback && $scope.callback.renderEnd) {
                        $scope.callback.renderEnd(e);
                    }
                }
            },
            xAxis: {
                axisLabel: 'Date & Time',
                tickFormat: function (d) {
                    return d3.time.format('%m/%d %H:%M')(mars$util$.convertUTCDateToLocalDate(new Date(d)));
                }
            },
            yAxis: {
                axisLabel: 'Count',
                tickFormat: function (d) {
                    return d;
                },
                axisLabelDistance: 5
            },
            callback: function (chart) {
                if ($scope.callback && $scope.callback.onReady) {
                    $scope.callback.onReady(chart);
                }
            }
        },
        title: {
            enable: true,
            text: 'Response Performance Line Chart'
        }
    };

    function makeReportData(response) {
        var series = [];
        var servers = [];
        var data = response.data;
        for (var i = data.length - 1; i >= 0; i--) {
            var item = data[i];
            var server = item.server;
            var serverData = servers[server];
            if (null == serverData || "undefined" === serverData) {
                serverData = [];
                servers[server] = serverData;
                series.push({
                    values: serverData,
                    key: server,
                    color: mars$util$.getColor(series.length)
                })
            }
            serverData.push({
                x: mars$util$.makeDate(item.logDate),
                y: item.count,
                item: item
            })
        }

        return series;
    }

    $scope.getReportData = function () {
        var api = new AjaxService(marsContext.contextPath + '/services/data/getList/bio.monitor-GetPerformanceLog.json');
        api.post({
            success: function (o) {
                $scope.data = makeReportData(o.data);
            },
            error: function (o) {
                o.alertException();
            }
        }, $scope.param);

        if ($scope.refreshInterval) {
            $timeout(function () {
                $scope.getReportData();
            }, $scope.refreshInterval);
        }
    };

    (function () {
        $scope.getReportData();
    })();
}]);
