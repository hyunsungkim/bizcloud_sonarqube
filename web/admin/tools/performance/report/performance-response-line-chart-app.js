var app = angular.module('reportApp', [
    'nvd3'
    , 'blockUI'
    , 'mars.angular.ajax'
    , 'mars.angular.context'
    , 'bio.angular.report'
]);

app.controller('reportAppCtrl', ['$scope', 'AjaxService', 'marsContext', function ($scope, AjaxService, marsContext) {
    $scope.param = {
        pageSize: 144,
        ORDER_BY: 'id desc'
    };

    $scope.refreshInterval = 5 * (60 * 1000);
}]);
