<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@include file="../../../auth-admin.jsp" %>
<!DOCTYPE html>
<html ng-app="reportApp">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Realtime Response Performance Line Chart Report</title>
    <script src="../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>

    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../../includes/node_modules/nvd3/build/nv.d3.min.css"/>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.js"></script>
    <script src="../../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../../includes/node_modules/d3/d3.min.js"></script>
    <script src="../../../../includes/node_modules/nvd3/build/nv.d3.js"></script>
    <script src="../../../../includes/node_modules/angular-nvd3/dist/angular-nvd3.min.js"></script>

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("performance-response-line-chart.js");
        mars$require$.script("performance-response-line-chart-app.js");
    </script>
</head>

<body ng-controller="reportAppCtrl">

<performance-response-line-chart id="'lineChart1'" param="param" refresh-interval="refreshInterval"/>

</body>
</html>
