<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.util.DateUtil" %>
<%@ page import="com.bizflow.io.core.util.LogUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.ws.performlog.WebServicePerformLog" %>
<%@ page import="com.bizflow.io.services.ws.performlog.WebServicePerformance" %>
<%@ page import="com.bizflow.io.services.ws.performlog.WebServicePerformanceStatisticLog" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="java.util.*" %>

<%!
    public String convertDate(Date date) {
        return null != date ? DateUtil.convert(date, "yyyy/MM/dd HH:mm:ss z") : "";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>DB Service Performance</title>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
</head>
<body>
<div class="page-header" style="text-align: center;">
    <h1>Web Service Performance</h1>
</div>
<%
    String performFileName = null;
    String performErrorFileName = null;
    String prefix = ServletUtil.getParameterValue(request, "prefix", "");
    String status = ServletUtil.getParameterValue(request, "status", "OK");

    File toolDir = new File(application.getRealPath("/admin"));
    File rootDir = toolDir.getParentFile();
    File docDir = new File(rootDir, "doc");
    if (!docDir.exists()) {
        docDir.mkdirs();
    }

    File logFile = LogUtil.getLogFile("ws-performance");
    if (null != logFile) {
        File logDir = logFile.getParentFile();

        File file = new File(docDir, prefix + "ws-performance.csv");
        File fileErr = new File(docDir, prefix + "ws-performance-error.csv");
        performFileName = file.getName();
        performErrorFileName = fileErr.getName();

        WebServicePerformance performance = new WebServicePerformance();
        performance.setOkStatusOnly("OK".equalsIgnoreCase(status));

        if (file.exists()) file.delete();
        if (fileErr.exists()) fileErr.delete();

        FileOutputStream fos = new FileOutputStream(file, true);
        FileOutputStream fosErr = new FileOutputStream(fileErr, true);

        performance.setFilePerformance(fos);
        performance.setFilePerformanceForError(fosErr);
%>
<div class="container-fluid">
    <div class="text-center">
        <div class="btn-group" role="group" aria-label="...">
            <div type="button" class="btn btn-default btn-xs"><a href="#AverageTimePanel">The Average Time</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#MinTimePanel">The Least Time</a></div>
            <div type="button" class="btn btn-default btn-xs"><a href="#MaxTimePanel">The Greatest Time</a></div>
        </div>
        <a class="btn btn-default btn-xs" href="performance-ws.jsp">Refresh</a>
        <a class="btn btn-default btn-xs" target="_blank" href="../util/csv-viewer.jsp?t=Web Service Error&f=/doc/ws-performance-error.csv">View Web Service Errors</a>
        <a class="btn btn-default btn-xs" target="_blank" href="<%=request.getContextPath()%>/doc/<%=performFileName%>">Download <%=performFileName%>
        </a>
        <a class="btn btn-default btn-xs" target="_blank" href="<%=request.getContextPath()%>/doc/<%=performErrorFileName%>">Download <%=performErrorFileName%>
        </a>
    </div>
    <br>
    <div class="panel panel-default" id="ProcessingPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#Processing"><h3 class="panel-title">Processing Logs</h3></div>
        <div class="panel-body collapse out" id="Processing">
            <%
                try {
                    File[] files = logDir.listFiles();
                    for (int i = 0; i < files.length; i++) {
                        File _file = files[i];
                        if (_file.isFile()) {
                            String filePath = _file.getAbsolutePath();
                            String fileName = _file.getName();
                            if (fileName.indexOf("ws-performance") != -1) {
                                performance.perform(filePath);
            %>
            <div>Processing <%=filePath%>
            </div>
            <%
                            }
                        }
                    }
                } finally {
                    fos.close();
                    fosErr.close();
                }
            %>
        </div>
    </div>
        <%
        Map<String, WebServicePerformanceStatisticLog> map = performance.getPerformMap();
        Iterator it = map.keySet().iterator();
        List<String> nameList = new ArrayList<String>();

        while (it.hasNext()) {
            nameList.add((String) it.next());
        }

        Collections.sort(nameList);
    %>

    <div class="panel panel-primary" id="AverageTimePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#AverageTime"><h3 class="panel-title">The Average Time</h3></div>
        <div id="AverageTime" class="collapse in">
            <%if (null != performance.getFirstLog()) { %>
            <div class="panel-body">From <strong><%=convertDate(performance.getFirstLog().getDate())%>
            </strong> To <strong><%=convertDate(performance.getLastLog().getDate())%>
            </strong></div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-toggle="true" data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <th data-field='operationName' data-sortable='true' class="header">Operation Name</th>
                    <th data-field='count' data-sortable='true' class="header">Count</th>
                    <th data-field='totalTime' data-sortable='true' class="header">Total Time (ms)</th>
                    <th data-field='averageTime' data-sortable='true' class="header">Average Response Time (ms)</th>
                    <th data-field='leastTime' data-sortable='true' class="header">Least Response Time (ms)</th>
                    <th data-field='greatestTime' data-sortable='true' class="header">Greatest Response Time (ms)</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (String name : nameList) {
                        WebServicePerformanceStatisticLog staticsLog = map.get(name);
                        WebServicePerformLog minLog = staticsLog.getMinLog();
                        WebServicePerformLog maxLog = staticsLog.getMaxLog();
                %>
                <tr>
                    <td><%=staticsLog.getOperationName()%>
                    </td>
                    <td><%=staticsLog.getCount()%>
                    </td>
                    <td><%=staticsLog.getTotalTime()%>
                    </td>
                    <td><%=staticsLog.getAverageResponseTime()%>
                    </td>
                    <td><%=minLog.getResponseTime()%>
                    </td>
                    <td><%=maxLog.getResponseTime()%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
            <%} else {%>
            <div class="panel-body">No Logs</div>
            <%}%>
        </div>
    </div>

    <div class="panel panel-primary" id="MinTimePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#MinTime"><h3 class="panel-title">The Least Request</h3></div>
        <div id="MinTime" class="collapse in">
            <%if (null != performance.getFirstLog()) { %>
            <div class="panel-body">From <strong><%=convertDate(performance.getFirstLog().getDate())%>
            </strong> To <strong><%=convertDate(performance.getLastLog().getDate())%>
            </strong></div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-toggle="true" data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <th data-field='operationName' data-sortable='true' class="header">Operation Name</th>
                    <th data-field='requestId' data-sortable='true' class="header">Request ID</th>
                    <th data-field='date' data-sortable='true' class="header">Date</th>
                    <th data-field='responseTime' data-sortable='true' class="header text-nowrap">Response Time (ms)</th>
                    <th data-field='status' data-sortable='true' class="header">Status</th>
                    <th data-field='parameter' data-sortable='true' class="header">Parameter</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (String name : nameList) {
                        WebServicePerformanceStatisticLog staticsLog = map.get(name);
                        WebServicePerformLog wsLog = staticsLog.getMinLog();
                %>
                <tr id="<%=wsLog.getRequestId()%>">
                    <td><%=staticsLog.getOperationName()%>
                    </td>
                    <td><%=wsLog.getRequestId()%>
                    </td>
                    <td class="text-nowrap"><%=convertDate(wsLog.getDate())%>
                    </td>
                    <td><%=wsLog.getResponseTime()%>
                    </td>
                    <td><%=wsLog.getStatus()%>
                    </td>
                    <td><%=HtmlUtil.getHtmlText(wsLog.getParameter())%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
            <%} else {%>
            <div class="panel-body">No Logs</div>
            <%}%>
        </div>
    </div>

    <div class="panel panel-primary" id="MaxTimePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#MaxTime"><h3 class="panel-title">The Greatest Request</h3></div>
        <div id="MaxTime" class="collapse in">
            <%if (null != performance.getFirstLog()) { %>
            <div class="panel-body">From <strong><%=convertDate(performance.getFirstLog().getDate())%>
            </strong> To <strong><%=convertDate(performance.getLastLog().getDate())%>
            </strong></div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-toggle="true" data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <th data-field='operationName' data-sortable='true' class="header">Operation Name</th>
                    <th data-field='requestId' data-sortable='true' class="header">Request ID</th>
                    <th data-field='date' data-sortable='true' class="header">Date</th>
                    <th data-field='responseTime' data-sortable='true' class="header text-nowrap">Response Time (ms)</th>
                    <th data-field='status' data-sortable='true' class="header">Status</th>
                    <th data-field='parameter' data-sortable='true' class="header">Parameter</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (String name : nameList) {
                        WebServicePerformanceStatisticLog staticsLog = map.get(name);
                        WebServicePerformLog wsLog = staticsLog.getMaxLog();
                %>
                <tr id="<%=wsLog.getRequestId()%>">
                    <td><%=staticsLog.getOperationName()%>
                    </td>
                    <td><%=wsLog.getRequestId()%>
                    </td>
                    <td class="text-nowrap"><%=convertDate(wsLog.getDate())%>
                    </td>
                    <td><%=wsLog.getResponseTime()%>
                    </td>
                    <td><%=wsLog.getStatus()%>
                    </td>
                    <td><%=HtmlUtil.getHtmlText(wsLog.getParameter())%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
            <%} else {%>
            <div class="panel-body">No Logs</div>
            <%}%>
        </div>
    </div>
        <%}%>
    <br> <br>
    <%@include file="../../../copyright.html" %>
</body>
</html>
