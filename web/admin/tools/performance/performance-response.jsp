<%@include file="../../auth-admin.jsp" %>
<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@ page import="com.bizflow.io.core.util.DateUtil" %>
<%@ page import="com.bizflow.io.services.core.model.Member" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.system.SystemManager" %>
<%@ page import="com.bizflow.io.services.core.system.performance.SystemPerformanceLog" %>
<%@ page import="com.bizflow.io.services.core.system.performance.SystemPerformanceLogManager" %>
<%@ page import="com.bizflow.io.services.core.system.performance.SystemPerformanceSlot" %>
<%@ page import="com.bizflow.io.services.core.system.performance.SystemPerformanceTotal" %>
<%@ page import="java.util.*" %>

<%!
    public String convertDate(Date date) {
        return null != date ? DateUtil.convert(date, "yyyy/MM/dd HH:mm:ss z") : "";
    }

    public String getRequestPath(SystemPerformanceLog performanceLog) {
        return null != performanceLog ? performanceLog.getRequestPath() : "";
    }
%>
<!DOCTYPE html>
<html ng-app="angularApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Realtime Response Performance</title>
    <script src="../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>"></script>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap.admin.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.css"/>
    <link rel="stylesheet" href="../../../includes/node_modules/nvd3/build/nv.d3.min.css"/>

    <script src="../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../includes/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
    <script src="../../../includes/node_modules/angular/angular.min.js"></script>
    <script src="../../../includes/node_modules/angular-block-ui/dist/angular-block-ui.min.js"></script>
    <script src="../../../includes/node_modules/d3/d3.min.js"></script>
    <script src="../../../includes/node_modules/nvd3/build/nv.d3.js"></script>
    <script src="../../../includes/node_modules/angular-nvd3/dist/angular-nvd3.min.js"></script>
    <script src="../../../includes/node_modules/bootbox/bootbox.min.js"></script>
    <style>
        .bootstrap-table .fixed-table-container .table thead th .th-inner {
            white-space: normal !important;
        }
    </style>
    <script>
        mars$require$.link("../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../includes/node_modules-ext/angular/angular-ext-filter.js");
        mars$require$.script("../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("report/performance-response-line-chart.js");
        mars$require$.script("performance-response.js");
    </script>
</head>
<body ng-controller="angularAppCtrl">
<div class="page-header" style="text-align: center;">
    <h1>Realtime Response Performance</h1>
</div>

<div class="container-fluid">
    <div class="text-center">
        <a class="btn btn-default btn-xs" href="performance-response.jsp">Refresh</a>
    </div>
    <br>
    <div class="panel panel-primary" id="ReportPanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#Report">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">The Response Time Line Chart</h3>
                    </td>
                    <td style="text-align: right;">
                        <div ng-if="status.data">
                            <span class="badge badge-id">Running Count: {{status.data.currentRunningCount}}/{{status.data.maxConcurrentCount > 0 ? status.data.maxConcurrentCount:'Unlimit'}}</span>
                            <span class="badge badge-id">Highest Running Count: {{status.data.highestRunningCount}}</span>
                            <span class="badge badge-id">Total Service Count: {{status.data.totalServiceCount}}</span>
                            <span class="badge badge-id">Response Average Time: {{status.data.responseAverageTime | number:2}} ms</span>
                            <span class="badge badge-id">Process Average Time: {{status.data.processAverageTime | number:2}} ms</span>
                        </div>
                    </td>
                    <td style="width: 20px; white-space: nowrap;padding-left: 5px;">
                        <span for="statusInterval">Interval:</span>
                    </td>
                    <td>
                        <select id="statusInterval" ng-model="status.interval" ng-click="$event.stopPropagation();" ng-change="getSystemStatusInfo();" style="color:black;">
                            <option value="3">3s</option>
                            <option value="5">5s</option>
                            <option value="10">10s</option>
                            <option value="30">30s</option>
                            <option value="60">1m</option>
                            <option value="120">2m</option>
                            <option value="300">5m</option>
                            <option value="600">10m</option>
                            <option value="-1">Stop</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div id="Report" class="collapse in overflow-auto">
            <div class="panel-body no-padding no-margin">
                <performance-response-line-chart id="'lineChart1'" param="param" refresh-interval="refreshInterval" callback="chartCallback"/>
            </div>
        </div>
    </div>
    <%
        SystemPerformanceLogManager responsePerformanceManager = SystemManager.getInstance().getSystemPerformanceLogManager();
    %>
    <div class="panel panel-primary" id="AverageTimePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#AverageTime">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">The Response Time</h3>
                    </td>
                    <td style="text-align: right;">
                        <span class="badge">Current Time: <%=convertDate(new Date())%></span>
                        <span class="badge">System Up Time: <%=convertDate(SystemManager.getInstance().getSystemUpDate())%></span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="AverageTime" class="collapse in overflow-auto">
            <div class="panel-body">
                From <strong><%=convertDate(responsePerformanceManager.getMeasureStartDate())%>
            </strong>
                To <strong><%=convertDate(new Date())%>
            </strong>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <b>Running Count</b>: <%=SystemManager.getInstance().getResponseProcessorManager().getRunningCount()%>
                <br>
                <%
                    SystemPerformanceTotal total = responsePerformanceManager.getAllPerformanceTotal();
                %>
                <b>Total Count</b>: <%=total.getCount()%> &nbsp;&nbsp;
                <b>Response Average</b>: <%=String.format("%.2f", total.getResponseAverageTime())%> ms&nbsp;&nbsp;
                <b>Process Average</b>: <%=String.format("%.2f", total.getProcessAverageTime())%> ms &nbsp;&nbsp;
                <b>Least Response Time</b>: <%=total.getLeastResponseTime()%> ms &nbsp;&nbsp;
                <b>Greatest Response Time</b>: <%=total.getGreatestResponseTime()%> ms &nbsp;&nbsp;
                <b>Least Process Time</b>: <%=total.getLeastProcessTime()%> ms &nbsp;&nbsp;
                <b>Greatest Process Time</b>: <%=total.getGreatestProcessTime()%> ms
            </div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <th>#</th>
                    <th data-field='operationName' data-sortable='true' class="header">Request Path</th>
                    <th data-field='count' data-sortable='true' class="header">Count</th>
                    <th data-field='totalTime' data-sortable='true' class="header">Response Total Time (ms)</th>
                    <th data-field='totalTime' data-sortable='true' class="header">Process Total Time (ms)</th>
                    <th data-field='averageTime' data-sortable='true' class="header">Average Response Time (ms)</th>
                    <th data-field='averageTime' data-sortable='true' class="header">Average Process Time (ms)</th>
                    <th data-field='leastTime' data-sortable='true' class="header">Least Response Time (ms)</th>
                    <th data-field='greatestTime' data-sortable='true' class="header">Greatest Response Time (ms)</th>
                    <th data-field='leastTime' data-sortable='true' class="header">Least Process Time (ms)</th>
                    <th data-field='greatestTime' data-sortable='true' class="header">Greatest Process Time (ms)</th>
                </tr>
                </thead>
                <tbody>
                <%
                    Map<String, SystemPerformanceTotal> map = responsePerformanceManager.getRequestPathPerformanceTotalMap();
                    List<String> keyList = new ArrayList(map.keySet());
                    Collections.sort(keyList);
                    int idx = 1;

                    for (String key : keyList) {
                        SystemPerformanceTotal performanceTotal = map.get(key);
                %>
                <tr>
                    <td><%=idx++%>
                    </td>
                    <td><%=StringUtil.toString(performanceTotal.getRequestPath(), performanceTotal.getName())%>
                    </td>
                    <td><%=performanceTotal.getCount()%>
                    </td>
                    <td><%=performanceTotal.getResponseTotalTime()%>
                    </td>
                    <td><%=performanceTotal.getProcessTotalTime()%>
                    </td>
                    <td><%=String.format("%.2f", performanceTotal.getResponseAverageTime())%>
                    </td>
                    <td><%=String.format("%.2f", performanceTotal.getProcessAverageTime())%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getLeastResponsePerformanceLog())%>, identifier=<%=performanceTotal.getLeastResponseIdentifier()%>">
                        <%=performanceTotal.getLeastResponseTime()%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getGreatestResponsePerformanceLog())%>, identifier=<%=performanceTotal.getGreatestResponseIdentifier()%>">
                        <%=performanceTotal.getGreatestResponseTime()%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getLeastProcessPerformanceLog())%>, identifier=<%=performanceTotal.getLeastProcessIdentifier()%>">
                        <%=performanceTotal.getLeastProcessTime()%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getGreatestProcessPerformanceLog())%>, identifier=<%=performanceTotal.getGreatestProcessIdentifier()%>">
                        <%=performanceTotal.getGreatestProcessTime()%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-primary" id="IntervalPerformancePanel">
        <div class="panel-heading" data-toggle="collapse" data-target="#IntervalPerformance">
            <table style="width:100%;">
                <tr>
                    <td nowrap>
                        <h3 class="panel-title">The Interval Performance</h3>
                    </td>
                    <td style="text-align: right;">
                        <span class="badge">Current Time: <%=convertDate(new Date())%></span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="IntervalPerformance" class="collapse in overflow-auto">
            <div class="panel-body">
            </div>
            <table class="table table-striped table-condensed" data-toggle="table" data-toolbar="#filter-bar" data-show-toggle="true" data-search="true" data-filters="true"
                   data-show-refresh="true" data-show-filter="true" data-show-columns="true" data-show-pagination-switch="showPaginationSwitch">
                <thead>
                <tr>
                    <th>#</th>
                    <th data-field='time' data-sortable='true' class="header">Local Time</th>
                    <th data-field='timeKey' data-sortable='true' class="header">Time Key</th>
                    <th data-field='count' data-sortable='true' class="header">Count</th>
                    <th data-field='respondTotalTime' data-sortable='true' class="header">Response Total Time (ms)</th>
                    <th data-field='processTotalTime' data-sortable='true' class="header">Process Total Time (ms)</th>
                    <th data-field='averageResponseTime' data-sortable='true' class="header">Average Response Time (ms)</th>
                    <th data-field='averageProcessTime' data-sortable='true' class="header">Average Process Time (ms)</th>
                    <th data-field='leastResponseTime' data-sortable='true' class="header">Least Response Time (ms)</th>
                    <th data-field='greatestResponseTime' data-sortable='true' class="header">Greatest Response Time (ms)</th>
                    <th data-field='leastProcessTime' data-sortable='true' class="header">Least Process Time (ms)</th>
                    <th data-field='greatestProcessTime' data-sortable='true' class="header">Greatest Process Time (ms)</th>
                    <th data-field='archived' data-sortable='true' class="header">Log to DB</th>
                </tr>
                </thead>
                <tbody>
                <%
                    Member member = AcmManager.getInstance().getUserSessionMember(session);
                    Map<String, SystemPerformanceSlot> slotMap = responsePerformanceManager.getPerformanceSlotMap();
                    List<SystemPerformanceSlot> slotList = new ArrayList(slotMap.values());
                    Collections.sort(slotList, new Comparator<SystemPerformanceSlot>() {
                        @Override
                        public int compare(SystemPerformanceSlot o1, SystemPerformanceSlot o2) {
                            return (int) (o2.getSlotDate().getTime() - o1.getSlotDate().getTime());
                        }
                    });
                    int idx2 = 1;

                    for (SystemPerformanceSlot performanceSlot : slotList) {
                        String key = performanceSlot.getSlotKey();
                        SystemPerformanceTotal performanceTotal = performanceSlot.getResponsePerformanceTotal();
                        Date slotDate = null != member ? member.getClientDate(performanceSlot.getSlotDate()) : performanceSlot.getSlotDate();
                %>
                <tr>
                    <td><%=idx2++%>
                    </td>
                    <td><%=DateUtil.convertDateToStandDateFormat(slotDate)%>
                    </td>
                    <td title="Archived=<%=performanceSlot.isArchived()%>"><%=key%>
                    </td>
                    <td><%=performanceTotal.getCount()%>
                    </td>
                    <td><%=performanceTotal.getResponseTotalTime()%>
                    </td>
                    <td><%=performanceTotal.getProcessTotalTime()%>
                    </td>
                    <td><%=String.format("%.2f", performanceTotal.getResponseAverageTime())%>
                    </td>
                    <td><%=String.format("%.2f", performanceTotal.getProcessAverageTime())%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getLeastResponsePerformanceLog())%>, identifier=<%=performanceTotal.getLeastResponseIdentifier()%>">
                        <%=performanceTotal.getLeastResponseTime()%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getGreatestResponsePerformanceLog())%>, identifier=<%=performanceTotal.getGreatestResponseIdentifier()%>">
                        <%=performanceTotal.getGreatestResponseTime()%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getLeastProcessPerformanceLog())%>, identifier=<%=performanceTotal.getLeastProcessIdentifier()%>">
                        <%=performanceTotal.getLeastProcessTime()%>
                    </td>
                    <td title="requestPath=<%=getRequestPath(performanceTotal.getGreatestProcessPerformanceLog())%>, identifier=<%=performanceTotal.getGreatestProcessIdentifier()%>">
                        <%=performanceTotal.getGreatestProcessTime()%>
                    </td>
                    <td><%=performanceSlot.isArchived() ? "Yes" : "No"%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
</div>
<br><br>
<%@include file="../../../copyright.html" %>
</body>
</html>
