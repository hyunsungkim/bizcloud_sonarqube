<%@ page import="com.bizflow.io.core.db.util.DBUtil" %>
<%@ page import="com.bizflow.io.core.file.exception.NoMoreRecordProcessException" %>
<%@ page import="com.bizflow.io.core.file.processor.AccessTableProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.RecordProcessor" %>
<%@ page import="com.bizflow.io.core.file.util.AccessDatabaseUtil" %>
<%@ page import="com.bizflow.io.core.lang.util.StringUtil" %>
<%@ page import="com.bizflow.io.core.web.MultipartFile" %>
<%@ page import="com.bizflow.io.core.web.MultipartFormData" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.data.processor.RecordDBProcessor" %>
<%@ page import="com.bizflow.io.services.data.util.DataServiceConfigUtil" %>
<%@ page import="com.healthmarketscience.jackcess.Column" %>
<%@ page import="com.healthmarketscience.jackcess.Database" %>
<%@ page import="com.healthmarketscience.jackcess.DatabaseBuilder" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@include file="../../../auth-admin.jsp" %>

<%
    MultipartFile multipartFile = null;
    String importKey = request.getParameter("ik");
    String importTitle = request.getParameter("it");
    String tableNameParam = request.getParameter("tn");
    long _maxRecNumber = -1;
    if (null != tableNameParam) {
        String _filename = request.getParameter("filename");
        String _file = request.getParameter("file");
        multipartFile = new MultipartFile(_filename, new File(_file));
    } else {
        MultipartFormData formData = new MultipartFormData(request, 1024 * 1024 * 5, 1024 * 1024 * 1000, 1024 * 1024 * 1000);
        multipartFile = formData.getFile("uploadFile");
        _maxRecNumber = formData.getParameterIntValue("maxRecNumber", 10);
        importKey = formData.getParameterValue("ik", null);
        importTitle = formData.getParameterValue("it", "");
    }

    String importUrl = DataServiceConfigUtil.getProperty(importKey + ".Import.Access.import-data-url", "import-data.jsp");
    importUrl = importUrl.replaceAll("\\{contextPath\\}", request.getContextPath());

    String filename = multipartFile.getName();
    Database database = DatabaseBuilder.open(multipartFile.getFile());
    try {
        final long maxRecNumber = _maxRecNumber;
        Set<String> tableNames = database.getTableNames();
%>

<!DOCTYPE html>
<html ng-app="importApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Access - Review</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");
        mars$require$.link("import-access.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("review-file.js");
    </script>
    <style>
        .collapse.in {
            overflow: scroll;
        }

        .table {
            margin-bottom: 0px;
        }
    </style>

</head>

<body ng-controller="importAppCtrl" onload="mars$util$.resizeFrameWindowHeight();">
<form name="form" action="<%=importUrl%>" method="post" enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
    <input type="hidden" name="file" value="<%=multipartFile.getFile().getAbsolutePath()%>">
    <input type="hidden" name="filename" value="<%=Encode.forHtmlAttribute(filename)%>">
    <input type="hidden" name="it" value="<%=Encode.forHtmlAttribute(importTitle)%>">
    <input type="hidden" name="ik" value="<%=Encode.forHtmlAttribute(importKey)%>">
    <div class="container-fluid">
        <div class="page-header text-center">
            <h1>"<%=StringUtil.toString(Encode.forHtmlContent(importTitle), "")%>" Review and Set Import Parameters<br>
                <small>
                    <%=filename%>
                    <%
                        if (null != tableNameParam) {
                    %>
                    / <%=tableNameParam%>
                    <%
                        }
                    %>
                </small>
            </h1>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title"><span class="glyphicon glyphicon-th-large"></span>&nbsp;<%=filename%>
                </h1>
            </div>
            <div class="panel-body">
                <%
                    Iterator<String> iterator = null;
                    if (null != tableNameParam) {
                        List<String> list = new ArrayList();
                        list.add(tableNameParam);
                        iterator = list.iterator();
                    } else {
                        iterator = tableNames.iterator();
                    }

                    while (iterator.hasNext()) {
                        String tableName = iterator.next();
                        final String safeTableName = DBUtil.makeSafeName(tableName);
                        String dbSession = DataServiceConfigUtil.getProperty(importKey + ".Import.Access." + tableName + ".dbSession", "mars");
                        String dbQuery = DataServiceConfigUtil.getProperty(importKey + ".Import.Access." + tableName + ".dbQuery", "InsertDummy");
                        String inputType = DataServiceConfigUtil.getProperty(importKey + ".Import.Access." + tableName + ".type", "text");
                        AccessTableProcessor processor = new AccessTableProcessor(database, tableName);
                %>
                <div class="panel panel-primary">
                    <div class="panel-heading" data-toggle="collapse" data-target="#<%=tableName%>">
                        <h1 class="panel-title">
                            &nbsp;<%=tableName%>
                        </h1>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            Total Number of Record: <%=processor.getTotalRecordNumber()%>
                            <%if (!tableName.equalsIgnoreCase(tableNameParam)) {%>
                            <button type="button" class="btn btn-xs btn-info" ng-click="viewTableData('<%=HtmlUtil.getHtmlParam(tableName)%>');">View all records</button>
                            <%}%>
                        </div>
                        <%
                            if (null == tableNameParam) {
                                if ("hidden".equalsIgnoreCase(inputType)) {
                        %>
                        <input type="<%=inputType%>" class="form-control" name="<%=safeTableName%>.dbSession" required ng-model="<%=safeTableName%>.dbSession" value="<%=dbSession%>" ng-init="<%=safeTableName%>.dbSession = '<%=dbSession%>'">
                        <input type="<%=inputType%>" class="form-control" name="<%=safeTableName%>.dbQuery" required ng-model="<%=safeTableName%>.dbQuery" value="<%=dbQuery%>" ng-init="<%=safeTableName%>.dbQuery= '<%=dbQuery%>'">
                        <%} else { %>
                        <div class="col-md-12" style="height: 5px;"></div>

                        <div class="form-group col-md-6">
                            <div class="input-group max-width" ng-class="{ 'has-error': !<%=tableName%>.dbSession}">
                                <div class="input-group-addon title">Session Name</div>
                                <input type="<%=inputType%>" class="form-control" name="<%=safeTableName%>.dbSession" required ng-model="<%=safeTableName%>.dbSession" ng-init="<%=safeTableName%>.dbSession = '<%=dbSession%>'">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group max-width" ng-class="{ 'has-error': !<%=tableName%>.dbQuery}">
                                <div class="input-group-addon">Query Name</div>
                                <input type="<%=inputType%>" class="form-control" name="<%=safeTableName%>.dbQuery" required ng-model="<%=safeTableName%>.dbQuery" ng-init="<%=safeTableName%>.dbQuery= '<%=dbQuery%>'">
                            </div>
                        </div>
                        <%
                                }
                            }
                        %>
                    </div>
                    <div class="collapse in" id="<%=tableName%>">
                        <table class="table table-striped table-condensed table-bordered">
                            <%
                                final JspWriter writer = out;
                                final List<Column> columnList = processor.getColumnList();

                                processor.process(new RecordProcessor() {
                                    int columnSize = 0;

                                    @Override
                                    public void initializeProcess(long totalRecordNumber, List columnNameList) {
                                        try {
                                            columnSize = columnNameList.size();
                                            writer.print("<tr>");
                                            writer.print("<th>#</th>");
                                            int columnSize = columnNameList.size();
                                            for (int i = 0; i < columnSize; i++) {
                                                String tableColName = RecordDBProcessor.makeTableColumnName((String) columnNameList.get(i));
                                                writer.print("<th>");
                                                writer.print(columnNameList.get(i));
                                                writer.print("<br><input class='db-col-name' name=\"");
                                                writer.print(safeTableName);
                                                writer.print(".");
                                                writer.print(tableColName);
                                                writer.print("\" value=\"");
                                                writer.print(tableColName);
                                                writer.print("\"><br><small class='db-col-type'>(");
                                                try {
                                                    writer.print(StringUtil.toString(AccessDatabaseUtil.getDataTypeString(columnList.get(i).getSQLType()), "unknown"));
                                                    writer.print("/");
                                                    writer.print(StringUtil.toString(DBUtil.getJdbcTypeName(columnList.get(i).getSQLType()), "unknown"));
                                                } catch (SQLException e) {
                                                    writer.print("unknown");
                                                }
                                                writer.print(")</small></th>");
                                            }
                                            writer.print("</tr>");
                                        } catch (IOException e) {
                                        }
                                    }

                                    @Override
                                    public void preprocessRecord(long recordNumber, long totalRecordNumber, int columnSize) {
                                        try {
                                            writer.print("<tr><td>");
                                            writer.print(recordNumber);
                                            writer.print("</td>");
                                        } catch (IOException e) {
                                        }
                                    }

                                    @Override
                                    public void processColumn(long recordNumber, int columnIndex, int columnSize, Object value) {
                                        try {
                                            writer.print("<td>");
                                            writer.print(HtmlUtil.getHtmlText(StringUtil.toString(value, "")));
                                            writer.print("</td>");
                                        } catch (IOException e) {
                                        }
                                    }

                                    @Override
                                    public void processRecord(long recordNumber, long totalRecordNumber) {
                                        try {
                                            writer.print("</tr>");
                                        } catch (IOException e) {
                                        }

                                        if (maxRecNumber > 0 && recordNumber == maxRecNumber) {
                                            if (recordNumber < totalRecordNumber) {
                                                try {
                                                    writer.print("<tr>");
                                                    writer.print("<td colspan=\"");
                                                    writer.print(columnSize + 1);
                                                    writer.print("\" class='text-center'>...</tr>");
                                                } catch (IOException e) {
                                                }
                                            }
                                            throw new NoMoreRecordProcessException();
                                        }
                                    }

                                    @Override
                                    public void finalizeProcess() {
                                    }
                                });


                            %>
                        </table>
                    </div>
                </div>
                <%
                    }

                    if (null == tableNameParam) {
                %>
                <div class="text-center" style="padding-top: 10px;">
                    <button type="button" class="btn btn-default" ng-click="goBack();">Go back</button>
                    <button type="submit" class="btn btn-default" ng-disabled="form.$invalid">Import</button>
                </div>
                <%}%>
            </div>
        </div>
    </div>
</form>
<br><br><br>
<form target="_blank" method="post" id="viewDataForm">
    <input type="hidden" name="file" value="<%=multipartFile.getFile().getAbsolutePath()%>">
    <input type="hidden" name="filename" value="<%=filename%>">
    <input type="hidden" name="tn" value="">
</form>

</body>
</html>
<%
    } finally {
        database.close();
        multipartFile.deleteOnExit();
    }
%>
