<%@ page import="com.bizflow.io.core.db.mybatis.session.SqlSessionFactoryManager" %>
<%@ page import="com.bizflow.io.core.db.util.DBUtil" %>
<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.core.file.processor.AccessTableProcessor" %>
<%@ page import="com.bizflow.io.core.json.util.JSONUtil" %>
<%@ page import="com.bizflow.io.core.lang.LongData" %>
<%@ page import="com.bizflow.io.core.lang.ObjectData" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.services.core.model.Member" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.service.AdhocService" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlTransactionDAO" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlTransactionDAOManager" %>
<%@ page import="com.bizflow.io.services.data.processor.RecordDBProcessor" %>
<%@ page import="com.healthmarketscience.jackcess.Database" %>
<%@ page import="com.healthmarketscience.jackcess.DatabaseBuilder" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<%@include file="../../../auth-admin.jsp" %>

<%
    AdhocService.getInstance().initializeRequest(request, response);
%>

<!DOCTYPE html>
<html ng-app="importApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Access - Importing</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("import-data.js");
    </script>
</head>
<body ng-controller="importAppCtrl" onload="_onload()">
<div class="container">
    <%
        String filePath = request.getParameter("file");
        File file = new File(filePath);
        if (file.exists()) {
            Database database = DatabaseBuilder.open(file);
            try {
    %>
    <div class="page-header text-center">
        <h1>Import Data</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Importing Data</div>
        <div class="panel-body">
            <%
                Set<String> tableNames = database.getTableNames();
                Iterator<String> _iterator = tableNames.iterator();
                while (_iterator.hasNext()) {
                    final String tableName = _iterator.next();
                    final String safeTableName = DBUtil.makeSafeName(tableName);
                    AccessTableProcessor processor = new AccessTableProcessor(database, tableName);
            %>
            <div class="col-md-12">
                <div class="col-md-4"><%=tableName%> (<%=processor.getTotalRecordNumber()%>)</div>
                <div class="col-md-8 " style="margin-bottom: 0;">
                    <div id="progress<%=safeTableName%>" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;">0</div>
                </div>
            </div>
            <%
                }
            %>
            <%
                final Member userInfo = AcmManager.getInstance().getSessionMember(session);
                final JspWriter writer = out;

                String errorMessage = null;
                Date date_s = new Date();

                SqlTransactionDAOManager daoManager = new SqlTransactionDAOManager();
                try {
                    Iterator<String> iterator = tableNames.iterator();

                    while (iterator.hasNext()) {
                        final String tableName = iterator.next();
                        final String safeTableName = DBUtil.makeSafeName(tableName);
                        final String dbSession = request.getParameter(safeTableName + ".dbSession");
                        if (SqlSessionFactoryManager.getInstance().isExistSessionFactory(dbSession)) {
                            AccessTableProcessor processor = new AccessTableProcessor(database, tableName);
                            final String dbQuery = request.getParameter(safeTableName + ".dbQuery");
                            final Map<String, String[]> paramMap = request.getParameterMap();
                            final LongData currentRecordNumber = new LongData();
                            final ObjectData objectData = new ObjectData();
                            final SqlTransactionDAO sqlDao = daoManager.getSqlTransactionDAO(dbSession);

                            try {
                                processor.process(new RecordDBProcessor(false) {
                                    int oldPercent = 0;

                                    private String getParamValue(String name) {
                                        String value = name;
                                        String[] values = paramMap.get(name);
                                        if (null != values) {
                                            value = values[0];
                                        }

                                        return value;
                                    }

                                    @Override
                                    public String getMapColumnName(String name) {
                                        return getParamValue(safeTableName + "." + DBUtil.makeSafeName(name));
                                    }

                                    @Override
                                    public void processRecord(long recordNumber, long totalRecordNumber) {
                                        currentRecordNumber.setLongData(recordNumber);
                                        objectData.setObject(getRecordDataMap());

                                        if (!containsKey("creatorId")) {
                                            addColumn("creatorId", userInfo.getId());
                                            addColumn("creatorName", userInfo.getName());
                                        }

                                        sqlDao.insert(dbQuery, objectData.getObject());

                                        int percent = (int) ((recordNumber * 100) / totalRecordNumber);
                                        if (oldPercent != percent) {
                                            oldPercent = percent;

                                            try {
                                                writer.print("<script>");
                                                writer.print("$('#progress");
                                                writer.print(safeTableName);
                                                writer.print("').attr('aria-valuenow', ");
                                                writer.print(percent);
                                                writer.print(").css('width', '");
                                                writer.print(percent);
                                                writer.print("%').html('");
                                                writer.print(percent);
                                                writer.print("%');");
                                                writer.print("</script>");
                                                writer.flush();
                                            } catch (IOException e) {
                                            }
                                        }
                                    }
                                });


                            } catch (Exception e) {
                                StringBuilder builder = new StringBuilder();
                                builder.append("RecordNumber=").append(currentRecordNumber.getLongData()).append("\r\n, ");
                                builder.append("Record=").append(null != objectData.getObject() ? JSONUtil.mapToJsonString((Map) objectData.getObject()) : "").append("\r\n, ");
                                builder.append(ExceptionUtil.getOriginalExceptionString(e));
                                errorMessage = builder.toString();
                                throw e;
                            }
                        } else {
                            errorMessage = "The DB Session \"" + dbSession + "\" does not exist";
                            throw new RuntimeException(errorMessage);
                        }
                    }

                    daoManager.commitAllSessions();
                } catch (Exception e) {
                    daoManager.rollbackAllSessions();
                } finally {
                    daoManager.closeAllSessions();
                }
            %>
            <div class="col-md-12" style="height: 10px;"></div>
            <div class="col-md-12">
                <div id="alertSuccess" class="alert alert-success" role="alert" style="display: none;margin:0;">All records have been imported. It took <span id="howLong"></span>ms.</div>
            </div>
        </div>
    </div>

    <%if (null != errorMessage) {%>
    <div id="alertError" class="alert alert-danger" role="alert"><%=HtmlUtil.getHtmlText(errorMessage)%>
    </div>
    <%} else {%>
    <script>
        $("#howLong").html(<%=System.currentTimeMillis() - date_s.getTime()%>);
        setTimeout(function () {
            $("#alertSuccess").css("display", "");
        }, 1000);
    </script>
    <%
            }
        } finally {
            database.close();
            file.delete();
        }
    } else { %>
    <div class="page-header text-center">
        <h1>Import Data</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Importing Data</div>
        <div class="panel-body">
            <div class="alert alert-danger" role="alert">File <%=file.getName()%> does not exist.</div>
        </div>
    </div>
    <% }%>

    <div class="col-md-12 text-center">
        <div id="btnGroup1" style="display: none;">
            <button type="button" class="btn btn-default" onclick="done()">Ok</button>
        </div>
        <div id="btnGroup2" style="display: none;">
            <button type="button" class="btn btn-default" onclick="closeWindow()">Close</button>
        </div>
    </div>
</div>
<br><br><br>
</body>
</html>
