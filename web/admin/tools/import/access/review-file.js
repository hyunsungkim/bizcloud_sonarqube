var app = angular.module('importApp', []).controller('importAppCtrl', ['$scope', function ($scope) {

    $scope.goBack = function () {
        window.location.href = "select-file.jsp";
    };

    $scope.viewTableData = function(tableName) {
        var form = document.getElementById("viewDataForm");
        form.tn.value = tableName;
        form.target = tableName;
        form.submit();
    };
}]);

