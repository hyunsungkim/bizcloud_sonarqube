<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.data.util.DataServiceConfigUtil" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="../../../auth-admin.jsp" %>

<%
    String importKey = request.getParameter("ik");
    String importTitle = DataServiceConfigUtil.getProperty(importKey + ".Import.Access.title", "Title");
%>

<!DOCTYPE html>
<html ng-app="importApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Access - Select</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("select-file.js");
    </script>
</head>
<body ng-controller="importAppCtrl" onload="_onload();">
<div class="container">
    <div class="page-header text-center">
        <h1>Select Access File</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title"><span class="glyphicon glyphicon-th-large"></span>&nbsp;Select Access File<br>
                <small style="color: #7a7a7a;">Please select an access file.</small>
            </h1>
        </div>
        <div class="panel-body">
            <form name="form" method="post" action="review-file.jsp" enctype="multipart/form-data" accept-charset="UTF-8" class="form-horizontal">
                <%if (null != importKey) {%>
                <input type="hidden" name="ik" value="<%=Encode.forHtmlAttribute(importKey)%>">
                <%}%>
                <div class="form-group">
                    <label for="maxRecNumber" class="col-sm-3 control-label"><%=importTitle%>:</label>
                    <div class="col-sm-9" ng-class="{ 'has-error': !data.importTitle}">
                        <input type="text" name="it" class="form-control" required ng-model="data.importTitle"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="uploadFile" class="col-sm-3 control-label">File:</label>
                    <div class="col-sm-9">
                        <input type="file" name="uploadFile" id="uploadFile" style="padding-bottom: 40px;" accept=".mdb,.accdb" class="form-control btn btn-default max-width" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="maxRecNumber" class="col-sm-3 control-label">Max Record Number to display:</label>
                    <div class="col-sm-9">
                        <input type="number" name="maxRecNumber" id="maxRecNumber" value="5" class="form-control" required/>
                    </div>
                </div>
                <div class="text-center" style="padding-top: 10px;">
                    <button type="submit" class="btn btn-default" ng-disabled="form.$invalid">Upload</button>
                    <button id="btnGroup1" type="button" class="btn btn-default" style="display: none;" onclick="closeWindow()">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<br><br><br>
</body>
</html>
