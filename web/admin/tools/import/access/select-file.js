var app = angular.module('importApp', []).controller('importAppCtrl', ['$scope', function ($scope) {
}]);

function resizeWindowFrameHeight() {
    mars$util$.resizeFrameWindowHeight();
}

function closeWindow() {
    angularExt.closeModalWindow();
}

function _onload() {
    resizeWindowFrameHeight();
    if (window.frameElement && window.frameElement.contentWindow) {
        document.getElementById("btnGroup1").style.display = "";
    }
}