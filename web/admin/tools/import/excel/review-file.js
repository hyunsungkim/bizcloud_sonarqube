var app = angular.module('importApp', []).controller('importAppCtrl', ['$scope', function ($scope) {
    $scope.isValid = function () {
        return $scope.data && $scope.data.dbSession && $scope.data.dbQuery;
    };

    $scope.goBack = function () {
        window.location.href = "select-file.jsp";
    }
}]);

