<%@ page import="com.bizflow.io.core.db.mybatis.session.SqlSessionFactoryManager" %>
<%@ page import="com.bizflow.io.core.exception.util.ExceptionUtil" %>
<%@ page import="com.bizflow.io.core.file.processor.CsvProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.ExcelProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.FileProcessor" %>
<%@ page import="com.bizflow.io.core.json.util.JSONUtil" %>
<%@ page import="com.bizflow.io.core.lang.LongData" %>
<%@ page import="com.bizflow.io.core.lang.ObjectData" %>
<%@ page import="com.bizflow.io.core.net.util.ParameterUtil" %>
<%@ page import="com.bizflow.io.core.util.NumberUtil" %>
<%@ page import="com.bizflow.io.core.web.util.HtmlUtil" %>
<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.model.Member" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.service.AdhocService" %>
<%@ page import="com.bizflow.io.services.data.dao.SqlTransactionDAO" %>
<%@ page import="com.bizflow.io.services.data.processor.RecordDBProcessor" %>
<%@ page import="org.apache.commons.csv.CSVFormat" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@include file="../../../auth-admin.jsp" %>

<!DOCTYPE html>
<html ng-app="importApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Excel - Importing</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("import-data.js");
    </script>
</head>
<%
    AdhocService.getInstance().initializeRequest(request, response);

    String mode = ServletUtil.getParameterValue(request, "mode", "");

    final String filename = request.getParameter("filename");
    String filePath = request.getParameter("file");
    File file = new File(filePath);
    if (file.exists()) {
        String lowercaseFilename = filename.toLowerCase();

        FileProcessor fileProcessor = null;
        String csvFormat = ServletUtil.getParameterValue(request, "csvFormat", "DEFAULT");
        if (lowercaseFilename.endsWith(".csv")) {
            if ("TDF".equalsIgnoreCase(csvFormat)) {
                fileProcessor = new CsvProcessor(file, CSVFormat.TDF);
            } else {
                fileProcessor = new CsvProcessor(file, CSVFormat.DEFAULT);
            }
        } else {
            fileProcessor = new ExcelProcessor(file);
        }

        try {
%>
<body ng-controller="importAppCtrl" onload="_onload()">
<div class="container">
    <div class="page-header text-center">
        <h1>Import Data</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Importing Data</div>
        <div class="progress" style="margin-bottom: 0;">
            <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;">0</div>
        </div>
        <div id="alertSuccess" class="alert alert-success" role="alert" style="display: none;margin:0;">All data have been imported. It took <span id="howLong"></span>ms.</div>
        <%
            String errorMessage = null;
            Date date_s = new Date();
            final LongData currentRecordNumber = new LongData();
            final ObjectData objectData = new ObjectData();
            final String dbSession = request.getParameter("dbSession");
            final String dbQuery = request.getParameter("dbQuery");
            final Map<String, String[]> paramMap = request.getParameterMap();
            if (SqlSessionFactoryManager.getInstance().isExistSessionFactory(dbSession)) {
                final SqlTransactionDAO sqlDao = new SqlTransactionDAO(dbSession);

                try {
                    long _transactionId = 0;
                    if ("LookupData".equalsIgnoreCase(mode)) {
                        Map param = new HashMap();
                        param.put("moduleName", "LookupDataImport");
                        param.put("action", "C");
                        sqlDao.insert("app.CreateTransactionLog", param);
                        _transactionId = NumberUtil.parseLong(param.get("transactionId"), -1);
                    }
                    final long transactionId = _transactionId;
                    final Member userInfo = AcmManager.getInstance().getSessionMember(session);
                    final JspWriter writer = out;

                    fileProcessor.process(new RecordDBProcessor(false) {
                        int oldPercent = 0;

                        private String getParamValue(String name) {
                            String value = name;
                            String[] values = paramMap.get(name);
                            if (null != values) {
                                value = values[0];
                            }

                            return value;
                        }

                        @Override
                        public String getMapColumnName(String name) {
                            return getParamValue(name);
                        }

                        @Override
                        public void processRecord(long recordNumber, long totalRecordNumber) {
                            currentRecordNumber.setLongData(recordNumber);
                            objectData.setObject(getRecordDataMap());

                            if (transactionId > 0) {
                                addColumn("transactionId", transactionId);
                            }

                            if (!containsKey("uploadFilename")) {
                                addColumn("uploadFilename", filename);
                            }

                            if (!containsKey("creatorId")) {
                                addColumn("creatorId", userInfo.getId());
                                addColumn("creatorName", userInfo.getName());
                            }

                            for (int i = 0; i < 12; i++) {
                                String customParam = getParamValue("customParam" + i);
                                if (null != customParam && customParam.length() > 0) {
                                    addColumn(customParam, ParameterUtil.parseVariableSyntaxes(getParamValue("customValue" + i), getRecordDataMap()));
                                }
                            }

                            sqlDao.insert(dbQuery, objectData.getObject());

                            int percent = (int) ((recordNumber * 100) / totalRecordNumber);
                            if (oldPercent != percent) {
                                oldPercent = percent;

                                try {
                                    writer.print("<script>");
                                    writer.print("$('#progress').attr('aria-valuenow', ");
                                    writer.print(percent);
                                    writer.print(").css('width', '");
                                    writer.print(percent);
                                    writer.print("%').html('");
                                    writer.print(percent);
                                    writer.print("%');");
                                    writer.print("</script>");
                                    writer.flush();
                                } catch (IOException e) {
                                }
                            }
                        }
                    });

                    sqlDao.commit();
                } catch (Exception e) {
                    sqlDao.rollback();
                    StringBuilder builder = new StringBuilder();
                    builder.append("RecordNumber=").append(currentRecordNumber.getLongData()).append("\r\n, ");
                    builder.append("Record=").append(null != objectData.getObject() ? JSONUtil.mapToJsonString((Map) objectData.getObject()) : "").append("\r\n, ");
                    builder.append(ExceptionUtil.getOriginalExceptionString(e));
                    errorMessage = builder.toString();
                } finally {
                    sqlDao.close();
                }
            } else {
                errorMessage = "The DB Session \"" + dbSession + "\" does not exist";
            }
        %>
    </div>

    <%if (null != errorMessage) {%>
    <div id="alertError" class="alert alert-danger" role="alert"><%=HtmlUtil.getHtmlText(errorMessage)%>
    </div>
    <%} else {%>
    <script>
        $("#howLong").html(<%=System.currentTimeMillis() - date_s.getTime()%>);
        setTimeout(function () {
            $("#alertSuccess").css("display", "");
        }, 1000);
    </script>
    <%}%>

    <div class="col-md-12 text-center">
        <div id="btnGroup1" style="display: none;">
            <button type="button" class="btn btn-default" onclick="done()">Ok</button>
        </div>
        <div id="btnGroup2" style="display: none;">
            <button type="button" class="btn btn-default" onclick="closeWindow()">Close</button>
        </div>
    </div>
</div>
<br><br><br>
</body>
<%
    } finally {
        fileProcessor.close();
        file.delete();
    }
} else {
%>
<body onload="_onload()">
<div class="container">
    <div class="page-header text-center">
        <h1>Import Data</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Importing Data</div>
        <div class="panel-body">
            <div class="alert alert-danger" role="alert">File <%=file.getName()%> does not exist.</div>
        </div>
    </div>

    <div class="col-md-12 text-center">
        <div id="btnGroup1" style="display: none;">
            <button type="button" class="btn btn-default" onclick="done()">Ok</button>
        </div>
        <div id="btnGroup2" style="display: none;">
            <button type="button" class="btn btn-default" onclick="closeWindow()">Close</button>
        </div>
    </div>
</div>
<br><br><br>
</body>
<%
    }
%>
</html>