var validateExcelFile = function (showAlert) {
    var isValid = true;

    //Validation
    var btnExcelFile = $("#uploadFile");
    var fileLength = 0;
    var fileName = "";
    var fileExtension = "";
    if (null != btnExcelFile) {
        fileLength = $("#uploadFile").get(0).files.length;
        if (0 < fileLength) {
            fileName = $("#uploadFile").get(0).files[0].name;
            if (null != fileName) {
                var idx = fileName.lastIndexOf(".");
                if (-1 != idx) {
                    fileExtension = fileName.substr(idx + 1);
                }
                if (null != fileExtension) {
                    fileExtension = fileExtension.toUpperCase();
                }
            }
        }
    }

    if (0 >= fileLength) {
        if (showAlert) alert("Please select a csv or an excel file");
        isValid = false;
    } else if ("XLSX" != fileExtension && "XLS" != fileExtension && "CSV" != fileExtension) {
        if (showAlert) alert("Selected file is not a csv or an excel file. Please select a csv or an excel file again.");
        isValid = false;
    }

    return isValid;
};

function isReadExcelFile() {
    $("#uploadButton").css("display", validateExcelFile(true) ? "" : "none");
}

function cancel() {
    location.href = '../request-file-list';
}

function resizeWindowFrameHeight() {
    mars$util$.resizeFrameWindowHeight();
}

function closeWindow() {
    angularExt.closeModalWindow();
}

function _onload() {
    if (window.frameElement && window.frameElement.contentWindow) {
        document.getElementById("btnGroup1").style.display = "";
    }
}