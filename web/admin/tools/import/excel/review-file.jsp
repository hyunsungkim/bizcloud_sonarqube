<%@ page import="com.bizflow.io.core.file.processor.CsvProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.ExcelProcessor" %>
<%@ page import="com.bizflow.io.core.file.processor.FileProcessor" %>
<%@ page import="com.bizflow.io.core.util.IDUtil" %>
<%@ page import="com.bizflow.io.core.web.MultipartFile" %>
<%@ page import="com.bizflow.io.core.web.MultipartFormData" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.apache.commons.csv.CSVFormat" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@include file="../../../auth-admin.jsp" %>

<%
    MultipartFormData formData = new MultipartFormData(request);
    MultipartFile multipartFile = formData.getFile("uploadFile");
    String filename = multipartFile.getName();
    File file = multipartFile.getFile();
    String mode = formData.getParameterValue("mode", "");

    FileProcessor fileProcessor = null;
    String lowercaseFilename = filename.toLowerCase();
    String csvFormat = formData.getParameterValue("csvFormat", "DEFAULT");
    if (lowercaseFilename.endsWith(".csv")) {
        if ("TDF".equalsIgnoreCase(csvFormat)) {
            fileProcessor = new CsvProcessor(file, CSVFormat.TDF);
        } else {
            fileProcessor = new CsvProcessor(file, CSVFormat.DEFAULT);
        }
    } else {
        fileProcessor = new ExcelProcessor(file);
    }
    try {
%>

<!DOCTYPE html>
<html ng-app="importApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Excel - Review</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("review-file.js");
    </script>
    <style>
        div.title {
            min-width: 200px;
        }
    </style>
</head>
<body ng-controller="importAppCtrl">
<form name="form" action="import-data.jsp" method="post" enctype="application/x-www-form-urlencoded" accept-charset="UTF-8">
    <input type="hidden" name="file" value="<%=file.getAbsolutePath()%>">
    <input type="hidden" name="filename" value="<%=Encode.forHtmlAttribute(filename)%>">
    <input type="hidden" name="csvFormat" value="<%=Encode.forHtmlAttribute(csvFormat)%>">
    <input type="hidden" name="mode" value="<%=Encode.forHtmlAttribute(mode)%>">
    <div class="container">
        <div class="page-header text-center">
            <h1>Review and Set Import Parameters</h1>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title"><span class="glyphicon glyphicon-th-large"></span>&nbsp;Review and Set<br>
                    <small style="color: #7a7a7a;">Please set DB and parameters</small>
                </h1>
            </div>
            <div class="panel-body">
                <div class="panel">
                    <div>
                        <h5>Database Setting</h5>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-md-6">
                            <div class="input-group max-width" ng-class="{ 'has-error': !data.dbSession}">
                                <div class="input-group-addon title">Session Name</div>
                                <input type="text" class="form-control" name="dbSession" id="dbSession" required ng-model="data.dbSession">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group max-width" ng-class="{ 'has-error': !data.dbSession}">
                                <div class="input-group-addon">Query Name</div>
                                <input type="text" class="form-control" name="dbQuery" id="dbQuery" required ng-model="data.dbQuery">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div>
                        <h5>Parameter Setting</h5>
                    </div>
                    <div class="panel-body">
                        <%
                            List<String> columnNameList = fileProcessor.getColumnNameList();
                            for (String columnName : columnNameList) {
                                String value = IDUtil.makeSafeLowerCaseId(columnName);
                        %>
                        <div class="form-group col-md-6">
                            <div class="input-group max-width">
                                <div class="input-group-addon title"><%=columnName%>
                                </div>
                                <input type="text" class="form-control max-width" id="<%=value%>" name="<%=columnName%>" value="<%=value%>">
                            </div>
                        </div>
                        <%
                            }
                            for (int i = 0; i < 12; i++) {
                        %>
                        <div class="form-group col-md-6">
                            <div class="col-md-6 no-padding">
                                <input type="text" class="form-control title" name="customParam<%=i%>" value="" placeholder="Constant Param<%=i%> Name">
                            </div>
                            <div class="col-md-6 no-padding">
                                <input type="text" class="form-control max-width" name="customValue<%=i%>" value="" placeholder="Constant Param<%=i%> Value">
                            </div>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>

                <div class="text-center" style="padding-top: 10px;">
                    <button type="button" class="btn btn-default" ng-click="goBack();">Go back</button>
                    <button type="submit" class="btn btn-default" ng-disabled="!isValid()">Import</button>
                </div>
            </div>
        </div>
    </div>
</form>
<br><br><br>
</body>
</html>
<%
    } finally {
        fileProcessor.close();
    }
%>