<%@ page import="com.bizflow.io.core.web.util.ServletUtil" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@include file="../../../auth-admin.jsp" %>

<%
    String mode = ServletUtil.getParameterValue(request, "mode", "");
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Excel - Select</title>
    <script src='../../../../includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="../../../../includes/node_modules/bootstrap/dist/css/bootstrap.min.css"/>

    <script src="../../../../includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../../../../includes/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../../includes/node_modules/angular/angular.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../../../../includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../../../includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Fix for old browsers -->
    <script src="../../../../includes/node_modules/es5-shim/es5-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es5-shim/es5-sham.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="../../../../includes/node_modules/es6-shim/es6-sham.min.js"></script>
    <!-- Fix for old browsers -->

    <script>
        mars$require$.link("../../../../includes/node_modules-ext/bootstrap/css/bootstrap-ext.css");

        mars$require$.script("../../../../includes/node_modules-ext/angular/angular-ext.js");
        mars$require$.script("../../../../includes/node_modules-ext/mars/mars-util.js");
        mars$require$.script("select-file.js");
    </script>
</head>
<body onload="_onload();">
<div class="container">
    <div class="page-header text-center">
        <h1>Import Data</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title"><span class="glyphicon glyphicon-th-large"></span>&nbsp;Select File<br>
                <small style="color: #7a7a7a;">Please select a csv with comma separated format or an excel file that has records on the first work sheet. The first row of the file must be column headers.</small>
            </h1>
        </div>
        <div class="panel-body">
            <div class="max-width no-border">
                <form action="review-file.jsp" method="post" enctype="multipart/form-data" accept-charset="UTF-8">
                    <input type="file" name="uploadFile" id="uploadFile" style="padding-bottom: 40px;" accept=".xlsx,.xls,.csv" class="form-control btn btn-default max-width" onchange="isReadExcelFile();" required/>
                    <input type="hidden" name="mode" value="<%=Encode.forHtmlAttribute(mode)%>">
                    <div style="padding: 5px"></div>
                    <div>
                        CSV Format:
                        <select name="csvFormat">
                            <option>Select one for CSV</option>
                            <option value="DEFAULT">Comma</option>
                            <option value="TDF">Tab</option>
                        </select>
                    </div>
                    <div class="text-center" style="padding-top: 10px;">
                        <button type="submit" class="btn btn-default">Upload</button>
                        <button id="btnGroup1" type="button" class="btn btn-default" style="display: none;" onclick="closeWindow()">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br><br><br>
</body>
</html>