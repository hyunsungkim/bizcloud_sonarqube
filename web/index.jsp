<%@ page import="com.bizflow.io.services.core.moon.Moon" %>
<%@ page import="com.bizflow.io.services.core.moon.MoonManager" %>
<%@ page import="com.bizflow.io.services.core.util.CoreServiceConfigUtil" %>
<%@ page import="java.util.Map" %>

<%
    String systemUrl = CoreServiceConfigUtil.getSystemUrl();
    if (null != systemUrl && systemUrl.trim().length() > 0) {
        if (systemUrl.startsWith("/")) response.sendRedirect(request.getContextPath() + systemUrl);
        else response.sendRedirect(systemUrl);
        return;
    } else {
        Map<String, Moon> map = MoonManager.getInstance().getMoonMap();
        if (map.size() == 1) {  // if there is only one application, use it as default application
            Moon moon = map.get(map.keySet().iterator().next());
            String home = request.getContextPath() + "/" + moon.getMoonUri();

            response.sendRedirect(home);
            return;
        }
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <title>Welcome!</title>
    <script src='includes/node_modules-ext/mars/mars-require.js?v=<%=MoonManager.getInstance().getMars().getVersion()%>'></script>
    <link rel="stylesheet" href="includes/node_modules/bootstrap4/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="includes/node_modules/components-font-awesome/css/font-awesome.min.css">

    <script src="includes/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="includes/node_modules/bootstrap4/dist/js/bootstrap.min.js"></script>

    <!--[if lt IE 9]>
    <script src="includes/node_modules/html5shiv/dist/html5shiv.min.js"></script>
    <script src="includes/node_modules/respond/dest/respond.min.js"></script>
    <![endif]-->

    <script>
        mars$require$.link("includes/node_modules-ext/mars/css/mars-shadow.css");
        mars$require$.link("includes/node_modules-ext/mars/css/mars.css");
        mars$require$.script("includes/node_modules-ext/mars/mars-util.js");
    </script>

</head>

<body class="bg-dark">
<div class="container text-center" style="padding-top: 30%;">
    <div class="justify-content-md-center">
        <% if (CoreServiceConfigUtil.isExistFile("/includes/node_modules-ext/bizflow/img/logo_white.png")) { %>
        <div css="no-padding no-margin" id="logoBox">
            <a href="http://www.bizflow.com">
                <img src="includes/node_modules-ext/bizflow/img/logo_white.png" width="217" height="37" alt="BizFlow"></div>
        </a>
    </div>
    <%}%>
</div>
</div>
</body>
</html>
