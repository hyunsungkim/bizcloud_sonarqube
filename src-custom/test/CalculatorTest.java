package test;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
   @Test
    public void testSum(){
       Calculator c = new Calculator();
       assertEquals(30, c.sum(10, 20));
   }

   @Test
    public void testSum2() {
       Calculator c = new Calculator();
       assertNotEquals(30, c.sum(20,20));
   }
}
