package notuse;//
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SetConfigToAppDev {

    private CloseableHttpClient httpClient = null;
    private CloseableHttpResponse response = null;
    private HttpClientContext context = null;
    private Cookie cookie = null;


    public HttpClientContext getContext() {
        return this.context;
    }


    public CloseableHttpResponse getResponse() {
        return this.response;
    }

    public boolean call(String method, String url, Object content) throws IOException {
        boolean success = false;
        if (method.equals("GET")) {
            if (content != null) {
                if (content instanceof BasicCookieStore) {
                    success = this.get(url, (BasicCookieStore)content);
                }
            } else {
                this.context = HttpClientContext.create();
                success = this.get(url, (BasicCookieStore)null);
            }
        } else {
            success = this.post(url, this.getContentEntity(content));
        }

        return success;
    }


    private HttpEntity getContentEntity(Object content) throws UnsupportedEncodingException {
        HttpEntity entity = null;
        if (null != content) {
            if (content instanceof List) {
                entity = new UrlEncodedFormEntity((List)content, "UTF-8");
            } else if (content instanceof String) {
                entity = new StringEntity((String)content);
            } else if (content instanceof File) {
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                builder.addBinaryBody("file", (File)content, ContentType.APPLICATION_OCTET_STREAM, ((File)content).getName());
                if ("" != null) {
                    builder.addTextBody("projectId", "", ContentType.DEFAULT_BINARY);
                }

                entity = builder.build();
            }
        }

        return (HttpEntity)entity;
    }

    private boolean post(String url, HttpEntity entity) throws IOException {
        int timeout = 1200;
        RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(0).build();
        this.httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Cookie", "JSESSIONID=" + this.getCookie());
        httpPost.setEntity(entity);
        Date date = new Date();
        this.response = this.httpClient.execute(httpPost);
        return this.isSuccess();
    }

    private boolean get(String url, BasicCookieStore cookieStore) throws IOException {
        this.httpClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
        HttpGet httpGet = new HttpGet(url);
        if (cookieStore == null) {
            this.response = this.httpClient.execute(httpGet, this.context);
        } else {
            this.response = this.httpClient.execute(httpGet);
        }

        return this.isSuccess();
    }

    public boolean isSuccess() {
        return this.response != null && this.response.getStatusLine().getStatusCode() == 200;
    }

    public String getCookie() {
        return this.cookie.getValue();
    }

    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }

    public boolean login(String url, String usesrId, String password) throws URISyntaxException, IOException {
        URI uri = (new URIBuilder()).setScheme("http").setHost(url.replace("http://", ""))
                .setPath("/services/acm/login.json").setParameter("app", "fbs").setParameter("i", usesrId)
                .setParameter("p", password).build();
        if (this.call("GET", uri.toString(), (Object)null)) {
            try {
                CookieStore cookieStore = this.getContext().getCookieStore();
                List<Cookie> cookies = cookieStore.getCookies();
                if (cookies != null) {
                    Iterator var4 = cookies.iterator();
                    if (var4.hasNext()) {
                        Cookie cookie = (Cookie)var4.next();
                        boolean var6;
                        if (cookie.getName().equals("JSESSIONID")) {
                            this.setCookie(cookie);
                            var6 = true;
                            return var6;
                        }

                        System.out.println("JSESSIONID does not exist.");
                        var6 = false;
                        return var6;
                    }
                }
            } finally {
                this.getResponse().close();
            }
        } else {
            System.out.println(EntityUtils.toString(this.getResponse().getEntity()));
            this.getResponse().close();
        }

        return false;
    }

    public static void main(String[] args){
        String serverIp = "bizcloud.bizflow.com";
        String url = "http://%SERVERIP%/bizflowappdev";
        String userId = "hkim";
        String passWord = "1";

        String getUrl = "http://%SERVERIP%/bizflowappdev/services/data/getList/bio.conf-GetConfiguration.json";
        String putUrl = "http://%SERVERIP%/bizflowappdev/services/data/run/bio.conf-CreateConfiguration@.json";

        String configJson = "{\"appName\":\"fbs\",\"name\":\"BizFlowAuthServerBaseURL\",\"description\":\"The base URL of BizFlow Authentication Server\",\"type\":\"Server\",\"value\":\"http://%SERVERIP%\"}";
        SetConfigToAppDev ins = new SetConfigToAppDev();

        if(args.length != 3) {
            System.out.println("Please Enter valid Server IP or domain as the 1st argument. ");
            System.out.println("  ex1) 123.32.45.121, 10.2.1.123:8080");
            System.exit(1);
        } else if(args[0].length() < 8){
            System.out.println("Please Enter valid Server IP or domain as the 1st argument. args[0]=" + args[0] + "args[0].length()="+args[0].length());
            System.out.println("  ex1) 123.32.45.121, 10.2.1.123:8080");
            System.exit(1);
        } else if(args.length < 3){
            System.out.println("Please Enter 3 arguments as below.");
            System.out.println("  ex1) 'Executable java' 'server ip:port' 'loginid ' 'password'");
            System.exit(2);
        }

        try {
            serverIp = args[0];
            userId = args[1];
            passWord = args[2];
            url = url.replaceAll("%SERVERIP%", serverIp);
            getUrl = getUrl.replaceAll("%SERVERIP%", serverIp);
            putUrl = putUrl.replaceAll("%SERVERIP%", serverIp);
            configJson = configJson.replaceAll("%SERVERIP%", serverIp);
            if(ins.login(url, userId, passWord)){
                if(ins.call("POST", getUrl,  (Object)null)){
                    CloseableHttpResponse response = ins.getResponse();
                    System.out.println("[GET]response:" + response.toString());
                    HttpEntity ent = response.getEntity();
                    System.out.println("[GET]entity:" + ent.toString());
                    String log = EntityUtils.toString(response.getEntity());
                    System.out.println("[GET]log:" + log);
                } else {
                    throw new Exception("Fail to get configuration info. url:" + getUrl);
                }
                if(ins.call("POST", putUrl,  configJson)){
                    CloseableHttpResponse response = ins.getResponse();
                    System.out.println("[PUT]response:" + response.toString());
                    HttpEntity ent = response.getEntity();
                    System.out.println("[PUT]entity:" + ent.toString());
                    String log = EntityUtils.toString(response.getEntity());
                    System.out.println("[PUT]log:" + log);
                } else {
                    throw new Exception("Fail to get configuration info. url:" + getUrl);
                }
            } else {
                throw new Exception("Fail to Login this AppDev:" + url);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}
