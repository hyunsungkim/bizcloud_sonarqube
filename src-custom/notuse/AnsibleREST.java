package notuse;//
import com.bizflow.io.core.lang.util.StringUtil;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import org.apache.hc.client5.http.async.methods.*;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManager;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManagerBuilder;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.client5.http.ssl.ClientTlsStrategyBuilder;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.nio.ssl.TlsStrategy;
import org.apache.hc.core5.io.CloseMode;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.ssl.TrustStrategy;
import org.quartz.DisallowConcurrentExecution;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.Future;

@DisallowConcurrentExecution
public class AnsibleREST {

    private String POST_RESPONSE = null;
    private String GET_RESPONSE = null;

    public AnsibleREST() {
    }

    public String callPOSTJob(String hostname, int port, String uri, String json) throws Exception {
        //"/api/v1/playbooks/maini.yml", "{\"id\":1,\"name\":\"John\"}"
        final String requestUri = uri;
        //json = "{\"id\":1,\"name\":\"John\"}";

        POST_RESPONSE = null;

        // Trust standard CA and those trusted by our custom strategy
        final SSLContext sslcontext = SSLContexts.custom()
                .loadTrustMaterial(new TrustStrategy() {

                    @Override
                    public boolean isTrusted(
                            final X509Certificate[] chain,
                            final String authType) throws CertificateException {
                        final X509Certificate cert = chain[0];
                        return "CN=httpbin.org".equalsIgnoreCase(cert.getSubjectDN().getName());
                    }

                })
                .build();
        final TlsStrategy tlsStrategy = ClientTlsStrategyBuilder.create()
                .setSslContext(sslcontext)
                // IMPORTANT uncomment the following method when running Java 9 or older
                // in order for ALPN support to work and avoid the illegal reflective
                // access operation warning
                /*
                .setTlsDetailsFactory(new Factory<SSLEngine, TlsDetails>() {

                    @Override
                    public TlsDetails create(final SSLEngine sslEngine) {
                        return new TlsDetails(sslEngine.getSession(), sslEngine.getApplicationProtocol());
                    }
                })
                */
                .build();

        final PoolingAsyncClientConnectionManager cm = PoolingAsyncClientConnectionManagerBuilder.create()
                .setTlsStrategy(tlsStrategy)
                .build();
        try (final CloseableHttpAsyncClient client = HttpAsyncClients.custom()
                .setConnectionManager(cm)
                .build()) {

            String returnBody = null;
            client.start();

            final HttpHost target = new HttpHost("https", hostname, port);
            final HttpClientContext clientContext = HttpClientContext.create();

            final SimpleHttpRequest request = SimpleHttpRequests.post(target, requestUri);
            request.setBody(json, ContentType.APPLICATION_JSON);
            request.setHeader("Accept", "application/json");

            final Future<SimpleHttpResponse> future = client.execute(
                    SimpleRequestProducer.create(request),
                    SimpleResponseConsumer.create(),
                    clientContext,
                    new FutureCallback<SimpleHttpResponse>() {

                        @Override
                        public void completed(final SimpleHttpResponse response) {
                            System.out.println(requestUri + "->" + response.getCode());
                            System.out.println(response.getBody());
                            System.out.println("bodyText-----" + response.getBodyText());
                            POST_RESPONSE = response.getBodyText();
                            final String bt = response.getBodyText();

                            final SSLSession sslSession = clientContext.getSSLSession();
                            if (sslSession != null) {
                                System.out.println("--SSL protocol " + sslSession.getProtocol());
                                System.out.println("--SSL cipher suite " + sslSession.getCipherSuite());
                            }
                        }

                        @Override
                        public void failed(final Exception ex) {
                            System.out.println(requestUri + "-- ->" + ex);
                        }

                        @Override
                        public void cancelled() {
                            System.out.println(requestUri + "-- cancelled");
                        }
                    });
            future.get();

            System.out.println("---Shutting down");
            client.close(CloseMode.GRACEFUL);
        }
        return POST_RESPONSE;
    }

    public String callGETJob(String uri) throws Exception {
        final String requestUri = uri;

        GET_RESPONSE = null;
        System.out.println("--------- call JSP File for Initializing AppDev. ---------------");
        System.out.println("--------- uri = "+ uri);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(uri);
        httpGet.addHeader("User-Agent", "Mozila/5.0");
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
        System.out.println("::GET Response Status::");
        System.out.println(httpResponse.getCode());
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                httpResponse.getEntity().getContent()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine);
        }
        GET_RESPONSE = response.toString();
        reader.close();
        System.out.println(GET_RESPONSE);
        httpClient.close();

        return GET_RESPONSE;
    }

    public static void main(final String[] args) throws Exception {

        AnsibleREST asyncAPI = new AnsibleREST();
//        asyncAPI.callPOSTJob("bizcloud.bizflow.com", 443, "/api/v1/playbooks/maini.yml", "{\"id\":1,\"name\":\"John\"}");
        //       asyncAPI.callGETJob("bizcloud.bizflow.com", 443, "/api/v1/playbooks");

//        String get_response = asyncAPI.callGETJob("bizcloud.bizflow.com", 443, "/api/v1/playbooks");
        String uri = "https://35.172.139.216/bizflowappdev/apps/sample/initData.jsp?serverIP=34.226.204.23&loginID=appdev&password=xxxx&wsName=Workspace&auth9=111000001";
        String get_response = asyncAPI.callGETJob(uri);

        System.out.println("CALL_GET_JOB RESPONSE=" + get_response);
        /*
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://bizcloud.bizflow.com:443/api/v1/playbooks");
        httpGet.addHeader("User-Agent", "Mozila/5.0");
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
        System.out.println("::GET Response Status::");
        System.out.println(httpResponse.getCode());
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                httpResponse.getEntity().getContent()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine);
        }
        reader.close();
        System.out.println(response.toString());
        httpClient.close();

         */
    }

}
