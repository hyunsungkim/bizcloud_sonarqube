package notuse;//
import com.hs.bf.importexport.BPMExport;
import com.hs.bf.importexport.env.*;
import com.hs.bf.importexport.obj.BizCoveWorkAreas;
import com.hs.bf.importexport.obj.BizCoves;
import com.hs.bf.importexport.obj.Menus;
import com.hs.bf.importexport.obj.ProcessDefinitions;
import com.hs.bf.plugin.bfmodel.internals.ServerModel;
import com.hs.bf.plugin.bfmodel.internals.SessionManager;
import com.hs.bf.plugin.importexport.Importor;
import com.hs.bf.web.beans.*;
import org.apache.log4j.PropertyConfigurator;

import com.hs.bf.web.xmlrs.XMLResultSet;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Properties;

public class CloudImportExport {
    static Logger logger = Logger.getLogger(CloudImportExport.class);

    public static void main(String[] args) {
        String action = "export";
        String propertiesFile = "bizflow.properties";
        try {
            PropertyConfigurator.configure("log4j.properties");
            if(0<args.length){
                action = args[0];
                propertiesFile = args[1];
            }else{
                System.out.println("export or import?");
                System.exit(0);
            }

            Properties props = new Properties();
            FileInputStream in = new FileInputStream(propertiesFile);
            props.load(in);
            in.close();

            HWSessionFactory factory = new HWSessionFactory();
            factory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
            HWSession session = factory.newInstance();

            String sessionInfoXML = null;
            HWSessionInfo sessionInfo = new HWSessionInfo();
            if("export".equals(action)){
                sessionInfoXML = session.logIn(props.getProperty("export.server"), Integer.parseInt(props.getProperty("export.port")), props.getProperty("export.login.id"), props.getProperty("export.login.pwd"), true);
                logger.info("login");
                sessionInfo.setSessionInfo(sessionInfoXML);

                boolean includeAllMenu = false;
                ArrayList<String> menuNames = new ArrayList<String>();
                for(int i=1; i<100; i++){
                    String name = props.getProperty("export.object.menu."+i);
                    if(name != null){
                        if("*".equals(name)){
                            includeAllMenu = true;
                            break;
                        }else{
                            menuNames.add(name.trim());
                        }
                    }
                }

                ArrayList<String> bizcoveviewNames = new ArrayList<String>();
                for(int i=1; i<100; i++){
                    String name = props.getProperty("export.object.bizcoveview."+i);
                    if(name != null){
                        bizcoveviewNames.add(name.trim());
                    }
                }

                ArrayList<String> processDefinitionNames = new ArrayList<String>();
                for(int i=1; i<100; i++){
                    String name = props.getProperty("export.object.processdefinition."+i);
                    if(name != null){
                        processDefinitionNames.add(name.trim());
                    }
                }

                BPMExport bpmExport = new BPMExport(sessionInfoXML, false);
                if("false".equals(props.getProperty("export.object.user_group.participant", "true"))){
                    bpmExport.options.setProperty("user_group.participant", "false");
                }

                OperationEnvironment opEnv = bpmExport.getOperationEnvironment();
                ArrayList<String> arrayList = new ArrayList<String>();

                if (includeAllMenu || 0<menuNames.size()) {
                    arrayList = new ArrayList<String>();
                    XMLResultSet xrs = Menus.getAllMenus(session, sessionInfo, Menus.XRS_ETCINFO_FIELD_NAME);
                    for (int i = 0; i < xrs.getRowCount(); i++) {
                        String name = xrs.getFieldValueAt(i, "NAME");
                        if (includeAllMenu || menuNames.contains(name)) {
                            arrayList.add(xrs.getFieldValueAt(i, "ID"));
                        }
                    }
                    String[] ids = arrayList.toArray(new String[0]);
                    opEnv.setMenuIds(ids);
                }

                if (0<bizcoveviewNames.size()) {
                    arrayList = new ArrayList<String>();
                    XMLResultSet xrs = BizCoveWorkAreas.getAllWorkAreas(session, sessionInfo, Menus.XRS_ETCINFO_FIELD_NAME);
                    for (int i = 0; i < xrs.getRowCount(); i++) {
                        String name = xrs.getFieldValueAt(i, "NAME");
                        String type = xrs.getFieldValueAt(i, "TYPE");
                        if (bizcoveviewNames.contains(name) && "K".equals(type) ) {
                            arrayList.add(xrs.getFieldValueAt(i, "ID"));
                        }
                    }
                    String[] ids = arrayList.toArray(new String[0]);
                    opEnv.setBizCoveWorkAreaIds(ids);
                }

                if (0<processDefinitionNames.size()) {
                    arrayList = new ArrayList<String>();
                    HWString folderID = new HWString();
                    HWString checkOutUserId = new HWString();

                    for (String fullPath : processDefinitionNames) {
                        int p = fullPath.lastIndexOf("/");
                        String path = fullPath.substring(0, p);
                        String name = fullPath.substring(p+1);
                        String id = ProcessDefinitions.getProcDefIDByPath(session, sessionInfo, sessionInfo.getServerID(), path, name, null, true, folderID, checkOutUserId );
                        arrayList.add(id);
                    }

                    String[] ids = arrayList.toArray(new String[0]);
                    opEnv.setProcessDefinitionIds(ids);
                }

                bpmExport.doExport(props.getProperty("export.filename"), props.getProperty("export.name"));
            }else if("import".equals(action)){
                sessionInfoXML = session.logIn(props.getProperty("import.server"), Integer.parseInt(props.getProperty("import.port")), props.getProperty("import.login.id"), props.getProperty("import.login.pwd"), true);
                logger.info("login");
                sessionInfo.setSessionInfo(sessionInfoXML);

                com.hs.bf.wf.jo.HWSessionFactory hwSessionFactory = new com.hs.bf.wf.jo.HWSessionFactory();
                com.hs.bf.wf.jo.HWSession hwSession = hwSessionFactory.newInstance();

                hwSession.setServerIP(props.getProperty("import.server"));
                hwSession.setServerPort(Integer.parseInt(props.getProperty("import.port")));
                hwSession.setSessionInfoXML(sessionInfoXML);

                ServerModel serverModel = new ServerModel();
                serverModel.setServerURL(props.getProperty("import.serverURL"));
                com.hs.bf.plugin.bfmodel.internals.Session bfSession = SessionManager.getInstance().getSession(hwSession);
                serverModel.connect(bfSession);
                Importor importor = new Importor(hwSession);
                importor.init(props.getProperty("import.filename"), serverModel);
                importor.importObject();
            }
            // logout
            session.logOut(sessionInfoXML);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("", e);
        }
    }
}
