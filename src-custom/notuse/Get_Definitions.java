package notuse;//
// Get_Definitions.java
import java.io.*;

import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import com.hs.bf.web.beans.*;
import com.hs.bf.web.xmlrs.*;

/**
 * Get process definitions on a folder.
 * <p>
 * @see HWSession
 *
 */
public class Get_Definitions
{
    static { org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.WARN); }

    // ---Modify the following variables with proper information. ---
    static String serverIP = "bizcloud.bizflow.com";
    static int serverPort = 7201;
    static String loginID = "administrator";
    static String password = "xxxx";

    public static JSONObject getDefinitions()
    {
        JSONObject result = null;
        try
        {
            HWSessionFactory hwSessionFactory = new HWSessionFactory();
            hwSessionFactory.setDefaultImplClassName(HWSessionFactory.TCPImpl);

            HWSession hwSession = hwSessionFactory.newInstance();
            String sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, password,true);

            // You can store the sessionInfoXML to HWSessionInfo class.
            // HWSessionInfo provides easy method to get necessary information such as
            // "ServerID".
            HWSessionInfo hwSessionInfo = new HWSessionInfo();
            hwSessionInfo.reset (sessionInfoXML);

            HWFilter hwFilter = new HWFilter();
            hwFilter.setName("HWProcessDefinition");
            hwFilter.addFilter("FolderServerID", "E", hwSessionInfo.get("ServerID"));
//            hwFilter.addFilter("FolderID", "E", Integer.toString(folderID));
//            hwFilter.addFilter("FolderID", "N", "72");
//            hwFilter.addFilter("ID", "E", "100");
            InputStream resultStream =
                    hwSession.getProcessDefinitions(hwSessionInfo.toString(), hwFilter.toByteArray());

//            saveStreamToFile (resultStream, outputFile);

            XMLResultSet xrs = new XMLResultSetImpl();
            xrs.parse(resultStream);
            int rowCount = xrs.getRowCount();

            resultStream.close();

            JSONObject recJson = JSONUtil.xmlToJsonObject(xrs.toString());

            if(recJson != null && recJson.has("HWRECORDSET")){
//                result = recJson.getJSONObject("HWRECORDSET").getJSONObject("HWRECBODY");
//                result.put("COUNT",rowCount);
            }

            System.out.println("process count is " + rowCount);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return result;
        }
    }

    private static void saveStreamToFile (InputStream is, String path)
    {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            int b;
            while (-1 != (b = is.read())) {
                fos.write(b);
            }
            fos.close();
        } catch (Exception e) { e.printStackTrace(); }
    }

    public String getDownloadFilePath(String appType, String versionStr) throws Exception{

//        String rootPath = CustomServiceConfigUtil.getProperty("upgrade.repository.file.path");
        String rootPath = "D:/Project/BIOCloud/";
        String downPath = rootPath;
        if(appType == null || versionStr == null){
            throw new Exception("The value of appType or versionStr parameter should not be null");
        }
        if(appType.equals("B")){
            downPath += "bpm/";
        } else if (appType.equals("A")){
            downPath += "appdev/";
        }

        if(versionStr.indexOf('.') < 0){
            throw new Exception("The format of the value of versionStr parameter is incorrect. Period point doesn't Exist");
        }
        String majorVersionStr = null;
        int majorVersion = -1;
        majorVersionStr = versionStr.substring(0, versionStr.indexOf('.'));
        if(!isInteger(majorVersionStr)){
            throw new Exception("The format of the value of versionStr parameter is incorrect: "+ majorVersionStr);
        }
        majorVersion = Integer.parseInt(majorVersionStr);
        downPath += majorVersion + File.separator ;
        System.out.println("download Path = " + downPath);
        return downPath;

    }

    public boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isExistForDownloadFile(String appType, String versionStr, String fileName) throws Exception {
        String downFilePath = null;
        try{
            downFilePath = this.getDownloadFilePath(appType, versionStr);
            downFilePath += fileName;
            if(new File(downFilePath).exists()){
                return true;
            } else {
                return false;
            }
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String args[])
    {
//        JSONObject rjson = (new Get_Definitions()).getDefinitions();

        Get_Definitions getPath = new Get_Definitions();
        String downloadPath = null;
        String fileName = "demo.zip";
        try{
            downloadPath = getPath.getDownloadFilePath("B", "15.0.001.00");
            if(getPath.isExistForDownloadFile("B", "15.0.001.00", fileName)){
                System.out.println("Exist File OK = " + downloadPath + fileName);
            } else {
                System.out.println("Doesn't Exist File = " + downloadPath + fileName);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        /*
        String aaa = "002";
        int iii = Integer.parseInt(aaa);
        System.out.println("int value = " + iii);
        */
    }
}
