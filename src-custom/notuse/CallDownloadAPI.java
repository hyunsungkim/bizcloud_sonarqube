package notuse;//
import com.bizflow.io.core.db.mybatis.model.QueryActionConstant;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.custom.services.bizcloud.ansible.AnsibleREST;
import com.bizflow.io.services.data.dao.util.SqlDAOUtil;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import jdk.internal.org.xml.sax.InputSource;
import jdk.nashorn.internal.codegen.CompilerConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.imageio.ImageIO;
import javax.net.ssl.*;
import javax.xml.parsers.DocumentBuilder;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CallDownloadAPI {
    private static final Logger logger = LogManager.getLogger(CallDownloadAPI.class);


    public static Object callDownloadPatch(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;
        String serverIP = null;

        String systemUserID = null;
        String systemUserPW = null;

        String url = null;

        int pageNo = -1;
        int pageSize = -1;
        int totalCount = -1;

        try{
            logger.debug("AppDevMigParam.callAppDevAPI_ForSelect Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

//            systemUserID = CustomServiceConfigUtil.getProperty("appdev.client.system.user.id", "appdev");
//            systemUserPW = CustomServiceConfigUtil.getProperty("appdev.client.system.user.pw", "");

//            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);

            systemUserID = "hkim";
            systemUserPW = "1";

            String appType = null;
            String mVersion = null;
            String fileName = null;

            if(!jsonParam.has("APPTYPE") || jsonParam.get("APPTYPE").equals("")){
                throw new Exception("Cannot Find the parameter, 'APPTYPE' ");
            }
            if(!jsonParam.has("MAJORVERSION") || jsonParam.get("MAJORVERSION").equals("")){
                throw new Exception("Cannot Find the parameter, 'MAJORVERSION' ");
            }
            if(!jsonParam.has("FILENAME") || jsonParam.get("FILENAME").equals("")){
                throw new Exception("Cannot Find the parameter, 'FILENAME' ");
            }
            appType = jsonParam.getString("APPTYPE");
            if(jsonParam.get("MAJORVERSION") instanceof String) {
                mVersion = jsonParam.getString("MAJORVERSION");
            } else if(jsonParam.get("MAJORVERSION") instanceof Integer) {
                mVersion = jsonParam.getInt("MAJORVERSION") + "";
            }
            fileName = jsonParam.getString("FILENAME");

            serverIP = jsonParam.getString("CLOUDSERVER");
            String protocol = "https";
            if(protocol.equals("https")) {

                url = "https://%CLOUDSERVER%/bizflowappdev/services/acm/login.json?app=fbs&i=" + systemUserID + "&p=" + systemUserPW;
//                url = "http://localhost:8080/bizflowappdev/services/acm/login.json?app=fbs&i=" + systemUserID + "&p=" + systemUserPW;
                url = url.replaceAll("%CLOUDSERVER%", serverIP);

                AnsibleREST restAPI = new AnsibleREST();
                String res = restAPI.login_get(url);

                String jsessionid = restAPI.getCookie().getValue();

                url = "/bizflowappdev/services/custom/cloud/file/download/patch/" + appType + "/" + mVersion + "/" + fileName;
                String theUrl = "https://bizcloud.bizflow.com" + url;
//                String theUrl = "http://localhost:8080" + url;

                String spec = theUrl;
                String outputDir = "D:/Backup";

                InputStream is = null;
                FileOutputStream os = null;
                try{
                    URL urlx = new URL(spec);
                    HttpURLConnection conn = (HttpURLConnection) urlx.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Cookie", "JSESSIONID="+jsessionid);
                    int responseCode = conn.getResponseCode();

                    System.out.println("responseCode " + responseCode);

                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        String fileNamex = fileName;
                        String disposition = conn.getHeaderField("Content-Disposition");
                        String contentType = conn.getContentType();

                        if (disposition != null) {
                            String target = "filename=";
                            int index = disposition.indexOf(target);
                            if (index != -1) {
                                fileNamex = disposition.substring(index + target.length() + 1);
                            }
                        } else {
                            fileNamex = spec.substring(spec.lastIndexOf("/") + 1);
                        }

                        System.out.println("Content-Type = " + contentType);
                        System.out.println("Content-Disposition = " + disposition);
                        System.out.println("fileName = " + fileNamex);

                        is = conn.getInputStream();
                        os = new FileOutputStream(new File(outputDir, fileNamex));

                        final int BUFFER_SIZE = 4096;
                        int bytesRead;
                        byte[] buffer = new byte[BUFFER_SIZE];
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        os.close();
                        is.close();
                        System.out.println("File downloaded");
                    } else {
                        System.out.println("No file to download. Server replied HTTP code: " + responseCode);
                    }
                    conn.disconnect();
                } catch (Exception e){
                    System.out.println("An error occurred while trying to download a file.");
                    e.printStackTrace();
                    try {
                        if (is != null){
                            is.close();
                        }
                        if (os != null){
                            os.close();
                        }
                    } catch (IOException e1){
                        e1.printStackTrace();
                    }
                }
            } else {
            }

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
//            e.printStackTrace();
            result = new JSONObject();
            result.put("count",0);
            result.put("result","false");
            result.put("faultString",e.getMessage());
            result.put("data", new JSONArray());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "callAppDevAPI_ForSelect", (System.currentTimeMillis() - timeS));
            return result;
        }
    }

    public static Object getServiceBPMSVersionInfo(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;
        String serverIP = null;

        String serviceID = null;
        String systemUserID = null;
        String systemUserPW = null;

        String url = null;

        try{
            logger.debug("getServiceBPMSVersionInfo Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

//            systemUserID = CustomServiceConfigUtil.getProperty("appdev.client.system.user.id", "appdev");
//            systemUserPW = CustomServiceConfigUtil.getProperty("appdev.client.system.user.pw", "");

//            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);

            systemUserID = "hkim";
            systemUserPW = "1";


            serviceID = jsonParam.getInt("SSERVICEID") + "";
            JSONObject idJson = new JSONObject();

/*
            String queryName = "service.selectIPsByServiceID";
            String queryAction =  QueryActionConstant.SelectQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, (HashMap)JSONUtil.jsonStringToMap(jsonParam.toString()));

            String webIP = null;
            if(resultObject instanceof JSONArray){
                JSONArray jArr = (JSONArray) resultObject;
                if(jArr == null || jArr.length() < 1){
                    throw new Exception("Cannot find network information by serviceID: " + serviceID);
                }
                JSONObject jSon = jArr.getJSONObject(0);
                webIP = jSon.getString("WEBIP");
            } else if(resultObject instanceof JSONObject){
                JSONObject jSon = (JSONObject) resultObject;
                webIP = jSon.getString("WEBIP");
            }
*/          String webIP = "bizcloud.bizflow.com";

            if(webIP == null || webIP.length() < 4){
                throw new Exception("Cannot find WebIP and network information by serviceID: " + serviceID);
            }
            String protocol = "https";
            if(protocol.equals("https")) {

                url = "https://%CLOUDSERVER%/bizflowappdev/services/acm/login.json?app=fbs&i=" + systemUserID + "&p=" + systemUserPW;
//                url = "http://localhost:8080/bizflowappdev/services/acm/login.json?app=fbs&i=" + systemUserID + "&p=" + systemUserPW;
                url = url.replaceAll("%CLOUDSERVER%", webIP);

                AnsibleREST restAPI = new AnsibleREST();
                String res = restAPI.login_get(url);

                String jsessionid = restAPI.getCookie().getValue();

                url = "/bizflowappdev/services/data/get/update.version-getAppDevVersion.json";
                String theUrl = "https://bizcloud.bizflow.com" + url;
//                String theUrl = "http://localhost:8080" + url;

                String spec = theUrl;
                JSONObject prdVersionJson = new JSONObject();
                prdVersionJson = getServiceVersionInfoUsingConnection(spec, jsessionid, prdVersionJson);

                url = "/bizflowappdev/services/data/get/update.version-getBizFlowVersion.json";
                url = "https://bizcloud.bizflow.com" + url;

                prdVersionJson = getServiceVersionInfoUsingConnection(url, jsessionid, prdVersionJson);
                result = new JSONObject(prdVersionJson.toString());
                result.put("result", "true");
            } else {
            }

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
//            e.printStackTrace();
            result = new JSONObject();
            result.put("count",0);
            result.put("result","false");
            result.put("faultString",e.getMessage());
            result.put("data", new JSONArray());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "callAppDevAPI_ForSelect", (System.currentTimeMillis() - timeS));
            return result;
        }
    }

    private static JSONObject getServiceVersionInfoUsingConnection(String fullUrl, String jsessionid, JSONObject responseJson) throws Exception{
        try{
            URL sUrl = new URL(fullUrl);
            HttpURLConnection subConn = (HttpURLConnection) sUrl.openConnection();
            subConn.setDoInput(true);
            subConn.setDoOutput(true);
            subConn.setRequestMethod("POST");
            subConn.setRequestProperty("Cookie", "JSESSIONID="+jsessionid);
            int responseCode = subConn.getResponseCode();

            System.out.println("responseCode " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(subConn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                JSONArray responseJsonArr = new JSONArray(sb.toString());
                System.out.println(responseJsonArr);

                for (Object o : responseJsonArr) {
                    JSONObject jsonLineItem = (JSONObject) o;
                    String prd = jsonLineItem.getString("PRODUCT");
                    if("AppDev".equals(prd)){
                        String version = jsonLineItem.getString("VERSION");
                        responseJson.put("APPDEV_VERSION", version);
                    } else if ("BIO".equals(prd)){
                        String version = jsonLineItem.getString("VERSION");
                        responseJson.put("BIO_VERSION", version);
                    } else if ("bizflowserver".equals(prd)){
                        String version = jsonLineItem.getString("VERSION");
                        responseJson.put("BIZFLOW_VERSION", version);
                    }
                }

                System.out.println("File downloaded");
            } else {
                System.out.println("No file to download. Server replied HTTP code: " + responseCode);
            }
            subConn.disconnect();
        } catch (Exception e){
            System.out.println("An error occurred while trying to download a file.");
            e.printStackTrace();
        }
        return responseJson;
    }
    public static void main(String args[]) {
        String jsonStr = "{\n" +
                "  \"CLOUDSERVER\": \"bizcloud.bizflow.com\",\n" +
                "  \"APPTYPE\": \"bpm\",\n" +
                "  \"MAJORVERSION\": 12,\n" +
                "  \"SSERVICEID\": 12,\n" +
                "  \"FILENAME\": \"12.6.0.0003.00-Windows.zip\"\n" +
                "}";
        String jsonStr2 = "{\n" +
                "   \"loginId\":\"newuser13\",\n" +
                "   \"password\":\"Newuser13\",\n" +
                "   \"userDetailYn\":\"Y\"\n" +
                "}";
//        String imgUrl = "https://www.bizflow.com/wp-content/uploads/elementor/thumbs/main_Keyvisual_01-1-oi1wflpv5hhcb4bjyzx32hnqu61zx4r6i0yi1acyxw.jpg";
//        String imgUrl = "https://54.197.187.151/bizflowappdev/services/acm/login.json?app=fbs&i=appdev&p=st1ceqef";
        String imgUrl = "https://bizcloud.bizflow.com/bizflowauth/services/auth/login.json";
        JSONObject resultJson = null;
        JSONObject incomingJson = new JSONObject(jsonStr2);
        try {
//            resultJson = CallDownloadAPI.executeSSLAPI_Without_Certificate(imgUrl, true, "POST", incomingJson);
//            System.out.println("resultJson1 = " + resultJson.toString());
//            resultJson = CallDownloadAPI.executeSSLAPI_Without_Certificate(imgUrl, false, "GET", incomingJson);
//            CallDownloadAPI.imageUrlDown(imgUrl, "xxxxxxx.jpg", 0, "");
//            CallDownloadAPI.callDownloadPatch("district", "sessionName", (HashMap) JSONUtil.jsonStringToMap(jsonStr));
//            CallDownloadAPI.getServiceBPMSVersionInfo("district", "sessionName", (HashMap) JSONUtil.jsonStringToMap(jsonStr));

String urlLogin = "https://3.80.239.166/bizflowappdev/services/acm/login.json?app=fbs&i=appdev&p=st1ceqef";
            CallDownloadAPI.executeLogin_Without_Cert(urlLogin, true,"GET", incomingJson);

            System.out.println("resultJson2 = " + resultJson.toString());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static JSONObject executeSSLAPI_Without_Certificate(String callUrl, boolean needReturnValue, String method, JSONObject jsonObj) throws Exception {
        JSONObject object = null;
        if (StringUtils.isNotBlank(callUrl)) {

            String contents = null;
            if (logger.isDebugEnabled()) {
                logger.debug("Call URL = " + callUrl);
            }

            String contentType = null;
            try {
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {return null;}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session){
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL url = new URL(callUrl);
                URLConnection con = url.openConnection();
                HttpURLConnection urlConn = (HttpURLConnection) con;
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                urlConn.setRequestMethod(method);
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("Accept", "application/json");

                OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
                wr.write(jsonObj.toString());
                wr.flush();

                if (urlConn.getResponseCode() == 200) {

                    String resMessage = urlConn.getResponseMessage();
                    logger.debug("resMessage = " + resMessage);

                    contentType = urlConn.getContentType();
                    logger.debug("contentType=" + contentType);
                } else {
                    logger.debug("responseCode=" + urlConn.getResponseCode());
                    logger.debug("responseMessage=" + urlConn.getResponseMessage());
/* --------------------------- */
                    contents = "";
                    object = new JSONObject();
                    object = object.put("result", "false");
                    for (Map.Entry<String, List<String>> header : urlConn.getHeaderFields().entrySet()) {
                        for (String value : header.getValue()) {
                            System.out.println(header.getKey() + " : " + value);
                            contents += header.getKey() + " : " + value + "\n";
                        }
                    }
                    object = object.put("errorMessage", contents);
                    return object;
                }

                if(needReturnValue){
                    contents = getContentFromConnection(urlConn);
                    if(contents != null){
                        object = new JSONObject(contents);
                    }
                    object.put("result","true");
                } else {
                    object = new JSONObject();
                    object = object.put("result", "true");
                }
            } catch (Exception e) {
                object = new JSONObject();
                object = object.put("result","false");
                object = object.put("errorMessage",e.getMessage());
                e.printStackTrace();
                logger.error("Error URL : " + callUrl);
                logger.error("Error Message : " + e.getMessage());
            }
        }
        return object;
    }


    public static JSONObject executeLogin_Without_Cert(String callUrl, boolean needReturnValue, String method, JSONObject jsonObj) throws Exception {
        JSONObject object = null;
        if (StringUtils.isNotBlank(callUrl)) {

            String contents = null;
            if (logger.isDebugEnabled()) {
                logger.debug("Call URL = " + callUrl);
            }

            String contentType = null;
            try {
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {return null;}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session){
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL url = new URL(callUrl);
                URLConnection con = url.openConnection();
                HttpURLConnection urlConn = (HttpURLConnection) con;
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                urlConn.setRequestMethod(method);
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("Accept", "application/json");

                if (urlConn.getResponseCode() == 200) {

                    String resMessage = urlConn.getResponseMessage();
                    logger.debug("resMessage = " + resMessage);

                    contentType = urlConn.getContentType();
                    logger.debug("contentType=" + contentType);
                } else {
                    logger.debug("responseCode=" + urlConn.getResponseCode());
                    logger.debug("responseMessage=" + urlConn.getResponseMessage());
                    /* --------------------------- */
                    contents = "";
                    object = new JSONObject();
                    object = object.put("result", "false");
                    for (Map.Entry<String, List<String>> header : urlConn.getHeaderFields().entrySet()) {
                        for (String value : header.getValue()) {
                            System.out.println(header.getKey() + " : " + value);
                            contents += header.getKey() + " : " + value + "\n";
                        }
                    }
                    object = object.put("errorMessage", contents);
                    return object;
                }

                String sessionId = null;
                if(needReturnValue){
                    List<String> cookies = urlConn.getHeaderFields().get("Set-Cookie");
                    if (cookies != null) {
                        for (String cookie : cookies) {
                            sessionId = cookie.split(";\\s*")[0];
                        }
                    }

                    contents = getContentFromConnection(urlConn);
                    if(contents != null){
                        object = new JSONObject(contents);
                    }
                    object.put("result","true");
                } else {
                    object = new JSONObject();
                    object = object.put("result", "true");
                }
            } catch (Exception e) {
                object = new JSONObject();
                object = object.put("result","false");
                object = object.put("errorMessage",e.getMessage());
                e.printStackTrace();
                logger.error("Error URL : " + callUrl);
                logger.error("Error Message : " + e.getMessage());
            }
        }
        return object;
    }

    private static String getContentFromConnection(HttpURLConnection hUrlCon){
        BufferedReader br = null;
        StringBuilder sb = null;
        try {
            br = new BufferedReader(new InputStreamReader((hUrlCon.getInputStream())));
            sb = new StringBuilder();
            String response = null;
            while ((response = br.readLine()) != null) {
                sb.append(response);
            }
            logger.debug("Content=" + sb.toString());
            br.close();
            hUrlCon.disconnect();
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException");
        } catch (IOException e) {
            logger.error("IOException!");
        } finally {
            if(sb != null){
                return sb.toString();
            } else {
                logger.debug("Cannot get the contents from API");
                return null;
            }
        }
    }

    public static JSONObject imageUrlDown_SSLAPI_Without_Certificate(String imageUrl, String fileName, int count, String currentTime) throws Exception {

        BufferedImage bi = null;
        JSONObject object = new JSONObject();

        if (StringUtils.isNotBlank(imageUrl)) {

            if (logger.isDebugEnabled()) {
                logger.debug("Download URL = " + imageUrl);
            }

            String contentType = null;
            try {
                //ignore SSL
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {return null;}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());
                //end

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session){
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL url = new URL(imageUrl);
                URLConnection con = url.openConnection();
                HttpURLConnection urlConn = (HttpURLConnection) con;

                if (urlConn.getResponseCode() == 200) {

                    String resMessage = urlConn.getResponseMessage();
                    logger.debug("resMessage = " + resMessage);

                    contentType = urlConn.getContentType().split("/")[1];
                    if ("jpeg".equalsIgnoreCase(contentType)) {
                        contentType = "jpg";
                    }
                } else {
                    logger.debug("responseCode=" + urlConn.getResponseCode());
                    logger.debug("responseMessage=" + urlConn.getResponseMessage());
                    Object content = urlConn.getContent();
                    logger.debug("content.toString=" + content.toString());

                }

                File saveFile = new File("D:\\Backup\\Image");
                saveFile.getParentFile().mkdirs();

                if (logger.isDebugEnabled()) {
                    logger.debug("contentType = " + contentType);
                    logger.debug("saveFile = " + fileName + "." + contentType);
                }

                if (saveFile.isDirectory()) {
                    saveFile.mkdirs();
                } else {
                    try {
                        bi = ImageIO.read(url);
                        ImageIO.write(bi, contentType, saveFile);

                        if (logger.isDebugEnabled()) {
                            logger.debug("saveFile.getAbsolutePath() = " + saveFile.getAbsolutePath());
                            logger.debug("saveFile.getPath() = " + saveFile.getPath());
                        }

                    } catch (MalformedURLException e) {
                        logger.error("Cannot Found Image");
                    } catch (IOException e) {
                        logger.error("File IOException");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error URL : " + imageUrl);
                logger.error("Error Message : " + e.getMessage());
            }
        }
        return object;
    }

}
