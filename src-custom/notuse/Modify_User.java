package notuse;//
// Modify_User.java
import com.hs.bf.wf.jo.*;
import com.hs.bf.web.beans.HWException;
import com.hs.bf.web.beans.HWInteger;

import java.io.BufferedInputStream;

/**
 * Modify a state of existing user as 'CLOSED'.
 * <p>
 * @see HWSession
 * @see HWUser
 *
 */
public class Modify_User
{
//    static { org.apache.log4j.Category.getRoot().setPriority(org.apache.log4j.Priority.WARN); }

    // ---Modify the following variables with proper information. ---
    // --------------------------------------------------------------

    public void modifyUser()
    {
        String serverIP = "bizcloud.bizflow.com";
        int serverPort = 7201;
        String loginID = "administrator";
        String password = "xxxx";

        String userID = "0000000226";	// User ID to close.
        boolean forceCheckOut = true;
        try
        {
            // Login
            HWSessionFactory hwSessionFactory = new HWSessionFactory();
            HWSession hwSession = hwSessionFactory.newInstance();

            hwSession.setServerIP(serverIP);
            hwSession.setServerPort(serverPort);
            String sessionInfoXML = hwSession.login(loginID, password, true);

            HWInteger reasonNumber = new HWInteger();

            // Get the user by User ID.
            HWUser user = hwSession.getUserByID(userID);

            if (null != user)
            {


/*
                // Get all users of the user's organizational unit for updateOrg().
                HWUsers orgUsers = hwSession.getUsersByOrgUnitID(user.getOrgUnitID());

                // Check out the parent organizational unit.
                hwSession.checkOutOrg(forceCheckOut, user.getOrgUnitID() , reasonNumber);
                if (reasonNumber.getValue() != 0)
                {
                    System.out.println("checkOutOrg error: " + reasonNumber);
                    return;
                }

                HWUser	orgUser = null;
                int	i;

                for (i = 0; i < orgUsers.getCount(); i++)
                {
                    orgUser = orgUsers.getItem(i);
                    if (orgUser.getID().equals(userID))
                    {
                        // Change the user status into 'Closed'.
                        orgUser.setState(HWConstant.ORGSTATE_CLOSED);

                        hwSession.updateOrg(null, orgUsers.toByteArray(), null, null, null);
                    }
                }
*/
                // Check out the parent organizational unit.
                hwSession.checkOutOrg(forceCheckOut, user.getOrgUnitID() , reasonNumber);
                if (reasonNumber.getValue() != 0)
                {
                    System.out.println("checkOutOrg error: " + reasonNumber);
                    return;
                }
                user.setPassword("3");
                // Check in
                hwSession.checkInOrg(user.getOrgUnitID());

                System.out.println("The user password was changed.");
            }
            else
            {
                System.out.println("There is no such user: " + userID);
            }

            hwSession.logout();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void main(String args[])
    {
        (new Modify_User()).modifyUser();
    }
}
