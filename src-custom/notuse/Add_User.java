package notuse;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
// Add_User.java
import java.io.*;

import com.bizflow.io.custom.services.bizcloud.appdev.Initialize;
import com.hs.bf.wf.jo.*;
import com.hs.bf.web.beans.HWException;
import com.hs.bf.web.beans.HWInteger;

/**
 * Add a new user with 'User' license and authority group to 'Root'.
 * <p>
 * @see HWSession
 * @see HWUser
 * @see HWGroupParticipant
 * @see HWAuthorityGroup
 *
 */
public class Add_User
{
    static { org.apache.log4j.Category.getRoot().setPriority(org.apache.log4j.Priority.WARN); }

    // ---Modify the following variables with proper information. ---
//    String serverIP = "3.94.255.173";

    // --------------------------------------------------------------

    public void addUser(String serverIP, String newLoginID, String password)
    {
        int serverPort = 7201;
        String admLoginID = "administrator";
        String orgUnitID = "9000000000";	// Root

        String userName = "AppDev User";
        String shortName = "appdev user";
        boolean forceCheckOut = true;
        try
        {
            // Login
            HWSessionFactory hwSessionFactory = new HWSessionFactory();
            HWSession hwSession = hwSessionFactory.newInstance();

            hwSession.setServerIP(serverIP);
            hwSession.setServerPort(serverPort);
            String sessionInfoXML = hwSession.login(admLoginID, password, true);

            // Get the target user group.
            HWOrgUnit ou = null;
            HWOrgUnits ous = hwSession.getOrgUnits();
            for(int i=0; i<ous.getCount(); i++)
            {
                ou = ous.getItem(i);
                if(ou.getID().equals(orgUnitID))
                {
                    break;
                }
            }

            HWInteger reasonNumber = new HWInteger();

            // Check out
            hwSession.checkOutOrg(forceCheckOut, orgUnitID, reasonNumber);
            if (reasonNumber.getValue() != 0)
            {
                System.out.println("checkOutOrg error: " + reasonNumber);
                return;
            }

            // Add new user
            HWUsers users = ou.getUsers();
            HWUser user = users.add();
            user.setName(userName);
            user.setLoginID(newLoginID);
            user.setPassword(password);
            user.setShortName(shortName);

            HWJobTitles jobTitles = hwSession.getJobTitles();

            if (jobTitles.getCount() > 0)
            {
                HWJobTitle jobTitle = jobTitles.getItem(0);
                user.setJobTitleID(jobTitle.getID());
            }
            else
            {
                user.setJobTitleID("9999999999");
            }

            user.setLicenseType(HWConstant.LICENSETYPE_USER);
            user.setDispOrder(100);
            user.setServerID("0000001001");
            user.setOrgUnitID("9000000000");
            user.setOrgUnitName("Root");
            user.setManagerID("0000000000");
            user.setManagerName("system");

            // License Group
            HWGroupParticipants licenseGroups = new HWGroupParticipantsImpl();
            HWGroupParticipant licenseGroup = licenseGroups.add();
            licenseGroup.setGroupID("licsgrp004");			// 'User' license
            licenseGroup.setParticipantID(user.getID());
            licenseGroup.setType(HWConstant.PRTCPTYPE_HUMAN);

            HWGroupParticipant licenseGroup2 = licenseGroups.add();
            licenseGroup2.setGroupID("licsgrp001");			// 'User' license
            licenseGroup2.setParticipantID(user.getID());
            licenseGroup2.setType(HWConstant.PRTCPTYPE_HUMAN);

            // Authority Group
            HWAuthorityGroups authGroups = new HWAuthorityGroupsImpl();
            HWAuthorityGroup authGroup = authGroups.add();
            HWGroupParticipants authGroupPrtcps = authGroup.getGroupParticipants();
            HWGroupParticipant authGroupPrtcp = authGroupPrtcps.add();
            authGroupPrtcp.setGroupID("authgrp005");			// 'User' license
            authGroupPrtcp.setParticipantID(user.getID());
            authGroupPrtcp.setType(HWConstant.PRTCPTYPE_HUMAN);

            // Add user
            hwSession.updateOrg(null,
                    users.toByteArray(),
                    authGroupPrtcps.toByteArray(),
                    licenseGroups.toByteArray(),
                    null);

            // Check in
            hwSession.checkInOrg(orgUnitID);

            System.out.println("Test user was successfully added.");

            hwSession.logout();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String args[])
    {
        /*
        if(args.length != 3) {
            System.out.println("Please Enter valid public BPM Server IP as the 1st argument. ");
            System.out.println("  ex1) 123.32.45.121");
            System.exit(1);
        } else if(args[0].length() < 8){
            System.out.println("Please Enter valid public BPM Server IP as the 1st argument. args[0]=" + args[0] + "args[0].length()="+args[0].length());
            System.out.println("  ex1) 123.32.45.121");
            System.exit(1);
        } else if(args.length < 3){
            System.out.println("Please Enter 3 arguments as below.");
            System.out.println("  ex1) 'Executable java' 'bpm server ip' 'loginid to be added' 'password'");
            System.exit(2);
        }
*/
        try {
        } catch (Exception e){
            e.printStackTrace();
        }

        (new Add_User()).addUser("3.94.255.173", "aaa1", "xxxx");
    }
}
