package notuse;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import com.bizflow.io.core.expression.ExpressionEvaluator;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.lang.util.StringUtil;
import com.bizflow.io.core.message.mail.AttachmentPart;
import com.bizflow.io.core.message.mail.MessagePart;
import com.bizflow.io.core.message.parser.InlineContentParser;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.services.file.util.FileServiceUtil;
import com.bizflow.io.services.message.exception.MessageServiceException;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import com.bizflow.io.services.message.sender.MessageSender;
import com.bizflow.io.services.message.util.MessageServiceConfigUtil;
import com.bizflow.io.services.message.util.MessageServiceUtil;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

public class _VPSendMail {
    private static final Logger logger = LogManager.getLogger(_VPSendMail.class);
    private static final String SEND_MAIL_URL = "/services/msg/send/mail.json";
    private static final String CONTRACTOR_NAME_RWORD = "$$UserName";
    private static final String TYPENAME_NAME_RWORD = "$$TypeName";
    private static final String TODAY_NAME_RWORD = "$$Today";
    private static final String LOGIN_INFO_RWORD = "$$LoginInfo";
    private static final String PW_INFO_RWORD = "$$PasswordInfo";
    private static final String URL_INFO_RWORD = "$$UrlInfo";
    private static final String AUTHCODE_INFO_RWORD = "$$AuthCodeInfo";
    private static final String ATTACH_INFO_RWORD = "$$AttachInfo";
    private static final String CUSTOMFIELD_INFO_RWORD = "$$CustomFieldInfo";
    private static String appName = null;

    public _VPSendMail() {
    }

    public static Object sendMailNotify(String district, String sessionName, HashMap parameter) {
        logger.debug("VPSendMail.sendMailNotify is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try {
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String subject = null;
            String sender = null;
            String contents = null;
            subject = MessageServiceConfigUtil.getProperty("mail.template.to.usercreation.subject");
            sender = MessageServiceConfigUtil.getProperty("mail.template.to.usercreation.sender");
            if (jsonParam.has("subject")) {
                subject = jsonParam.getString("subject");
            }

            if (jsonParam.has("sender")) {
                sender = jsonParam.getString("sender");
            }

            if (jsonParam.has("contents")) {
                contents = jsonParam.getString("contents");
            }

            JSONArray indInfoArr = jsonParam.getJSONArray("individualInfos");

            for(int i = 0; i < indInfoArr.length(); ++i) {
                JSONObject jobj = indInfoArr.getJSONObject(i);
                String theEmail = jobj.getString("email");
                String theName = jobj.getString("name");
                String password = jobj.getString("password");
                String userid = jobj.getString("userid");
                String linkUrl = MessageServiceConfigUtil.getProperty("mail.template.to.usercreation.url");
                String contentsFile = null;
                String loginInfo = "";
                String pwInfo = "";
                String linkInfo = "";
                contentsFile = MessageServiceConfigUtil.getProperty("mail.template.to.usercreation.contents");
                contents = FileUtils.readFileToString(new File(contentsFile), "utf-8");
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$Today", getToday());
                linkInfo = "Url: <a href='" + linkUrl + "'>" + linkUrl + "</a><br>";
                loginInfo = "Username: " + userid + "<br>";
                pwInfo = "Password: " + password + "<br>";
                contents = contents.replace("$$UrlInfo", linkInfo);
                contents = contents.replace("$$LoginInfo", loginInfo);
                contents = contents.replace("$$PasswordInfo", pwInfo);
                jsonParam.put("sender", sender);
                jsonParam.put("contents", contents);
                jsonParam.put("subject", subject);
                JSONArray newToArr = new JSONArray();
                newToArr.put(theEmail);
                jsonParam.put("to", newToArr);
                jsonParam.put("quillMessage", true);
                result = sendMail(request, JSONUtil.jsonStringToMap(jsonParam.toString()));
                System.out.println("result:" + result);
            }

            logger.debug("All jobs are done!");
        } catch (Exception var27) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var27);
            var27.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var27.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendMailNotify", System.currentTimeMillis() - timeS);
            return result;
        }
    }

    public static Object sendAuthCode(String district, String sessionName, HashMap parameter) {
        logger.debug("VPSendMail.sendAuthCode is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try {
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String subject = null;
            String sender = null;
            String contents = null;
            subject = MessageServiceConfigUtil.getProperty("mail.template.to.verifyAuthCode.subject");
            sender = MessageServiceConfigUtil.getProperty("mail.template.to.verifyAuthCode.sender");
            if (jsonParam.has("subject")) {
                subject = jsonParam.getString("subject");
            }

            if (jsonParam.has("sender")) {
                sender = jsonParam.getString("sender");
            }

            if (jsonParam.has("contents")) {
                contents = jsonParam.getString("contents");
            }

            JSONArray indInfoArr = jsonParam.getJSONArray("individualInfos");

            for(int i = 0; i < indInfoArr.length(); ++i) {
                JSONObject jobj = indInfoArr.getJSONObject(i);
                String theEmail = jobj.getString("email");
                String theName = jobj.getString("name");
                String authcode = jobj.getString("authCode");
                String contentsFile = null;
                String authCodeInfo = "";
                contentsFile = MessageServiceConfigUtil.getProperty("mail.template.to.verifyAuthCode.contents");
                contents = FileUtils.readFileToString(new File(contentsFile), "utf-8");
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$Today", getToday());
                authCodeInfo = "AuthCode: " + authcode + "<br>";
                contents = contents.replace("$$AuthCodeInfo", authCodeInfo);
                jsonParam.put("sender", sender);
                jsonParam.put("contents", contents);
                jsonParam.put("subject", subject);
                JSONArray newToArr = new JSONArray();
                newToArr.put(theEmail);
                jsonParam.put("to", newToArr);
                jsonParam.put("quillMessage", true);
                result = sendMail(request, JSONUtil.jsonStringToMap(jsonParam.toString()));
                System.out.println("result:" + result);
            }

            logger.debug("All jobs are done!");
        } catch (Exception var23) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var23);
            var23.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var23.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendAuthCode", System.currentTimeMillis() - timeS);
            return result;
        }
    }


    public static Object sendAuthCodeForVisn2(String district, String sessionName, HashMap parameter) {
        logger.debug("VPSendMail.sendAuthCodeForVisn2 is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try {
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String subject = null;
            String sender = null;
            String contents = null;
            subject = MessageServiceConfigUtil.getProperty("mail.template.to.verifyAuthCode.visn2.subject");
            sender = MessageServiceConfigUtil.getProperty("mail.template.to.verifyAuthCode.visn2.sender");
            if (jsonParam.has("subject")) {
                subject = jsonParam.getString("subject");
            }

            if (jsonParam.has("sender")) {
                sender = jsonParam.getString("sender");
            }

            if (jsonParam.has("contents")) {
                contents = jsonParam.getString("contents");
            }

            JSONArray indInfoArr = jsonParam.getJSONArray("individualInfos");

            for(int i = 0; i < indInfoArr.length(); ++i) {
                JSONObject jobj = indInfoArr.getJSONObject(i);
                String theEmail = jobj.getString("email");
                String theName = jobj.getString("name");
                String authcode = jobj.getString("authCode");
                String contentsFile = null;
                String authCodeInfo = "";
                contentsFile = MessageServiceConfigUtil.getProperty("mail.template.to.verifyAuthCode.visn2.contents");
                contents = FileUtils.readFileToString(new File(contentsFile), "utf-8");
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$Today", getToday());
                authCodeInfo = "AuthCode: " + authcode + "<br>";
                contents = contents.replace("$$AuthCodeInfo", authCodeInfo);
                jsonParam.put("sender", sender);
                jsonParam.put("contents", contents);
                jsonParam.put("subject", subject);
                JSONArray newToArr = new JSONArray();
                newToArr.put(theEmail);
                jsonParam.put("to", newToArr);
                jsonParam.put("quillMessage", true);
                result = sendMail(request, JSONUtil.jsonStringToMap(jsonParam.toString()));
                System.out.println("result:" + result);
            }

            logger.debug("All jobs are done!");
        } catch (Exception var23) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var23);
            var23.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var23.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendAuthCodeForVisn2", System.currentTimeMillis() - timeS);
            return result;
        }
    }

    public static Object sendCompleteNotification(String district, String sessionName, HashMap parameter) {
        logger.debug("VPSendMail.sendCompleteNotification is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try {
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String subject = null;
            String sender = null;
            String contents = null;
            subject = MessageServiceConfigUtil.getProperty("mail.template.to.requestCompletion.subject");
            sender = MessageServiceConfigUtil.getProperty("mail.template.to.requestCompletion.sender");
            if (jsonParam.has("subject")) {
                subject = jsonParam.getString("subject");
            }

            if (jsonParam.has("sender")) {
                sender = jsonParam.getString("sender");
            }

            if (jsonParam.has("contents")) {
                contents = jsonParam.getString("contents");
            }

            JSONArray indInfoArr = jsonParam.getJSONArray("individualInfos");

            for(int i = 0; i < indInfoArr.length(); ++i) {
                JSONObject jobj = indInfoArr.getJSONObject(i);
                String theEmail = jobj.getString("email");
                String theName = jobj.getString("name");
                String customField1 = jobj.getString("customField1");
                String contentsFile = null;
                String customFieldInfo = "";
                contentsFile = MessageServiceConfigUtil.getProperty("mail.template.to.requestCompletion.contents");
                contents = FileUtils.readFileToString(new File(contentsFile), "utf-8");
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$UserName", theName);
                contents = contents.replace("$$Today", getToday());
                customFieldInfo = "<h2>" + customField1 + "</h2>";
                contents = contents.replace("$$CustomFieldInfo", customFieldInfo);
                jsonParam.put("sender", sender);
                jsonParam.put("contents", contents);
                jsonParam.put("subject", subject);
                JSONArray newToArr = new JSONArray();
                newToArr.put(theEmail);
                jsonParam.put("to", newToArr);
                jsonParam.put("quillMessage", true);
                result = sendMail(request, JSONUtil.jsonStringToMap(jsonParam.toString()));
                System.out.println("result:" + result);
            }

            logger.debug("All jobs are done!");
        } catch (Exception var23) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var23);
            var23.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var23.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendCompleteNotification", System.currentTimeMillis() - timeS);
            return result;
        }
    }

    public static Object sendRejectMail(String district, String sessionName, HashMap parameter) {
        logger.debug("VPSendMail.sendRejectMail is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try {
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String subject = null;
            String sender = null;
            String contents = null;
            String contractorEMail = null;
            String contractorName = null;
            subject = MessageServiceConfigUtil.getProperty("mail.template.to.attachReject.subject");
            sender = MessageServiceConfigUtil.getProperty("mail.template.to.attachReject.sender");
            if (jsonParam.has("subject")) {
                subject = jsonParam.getString("subject");
            }

            if (jsonParam.has("sender")) {
                sender = jsonParam.getString("sender");
            }

            if (jsonParam.has("contents")) {
                contents = jsonParam.getString("contents");
            }

            if (!jsonParam.has("contractorEMail")) {
                throw new MessageServiceException("There is no contractorEMail field.");
            } else {
                contractorEMail = jsonParam.getString("contractorEMail");
                if (!jsonParam.has("contractorName")) {
                    throw new MessageServiceException("There is no contractorName field.");
                } else {
                    contractorName = jsonParam.getString("contractorName");
                    JSONArray indInfoArr = jsonParam.getJSONArray("attachInfos");
                    String attachInfo = "";

                    for(int i = 0; i < indInfoArr.length(); ++i) {
                        JSONObject jobj = indInfoArr.getJSONObject(i);
                        String formattedComments = "";
                        String theName = null;
                        String theComment = null;
                        if (!jobj.has("name")) {
                            throw new MessageServiceException("There is no name field.");
                        }

                        theName = jobj.getString("name");
                        if (!jobj.has("comment")) {
                            throw new MessageServiceException("There is no comment field.");
                        }

                        theComment = jobj.getString("comment");
                        String contentsFile = null;
                        contentsFile = MessageServiceConfigUtil.getProperty("mail.template.to.attachReject.contents");
                        contents = FileUtils.readFileToString(new File(contentsFile), "utf-8");

                        Scanner scanner;
                        String line;
                        for(scanner = new Scanner(theComment); scanner.hasNextLine(); formattedComments = formattedComments + line + "<br>") {
                            line = scanner.nextLine();
                        }

                        scanner.close();
                        attachInfo = attachInfo + "<li><span style=\"font-size: 14px\"><strong>" + theName + ":</strong><br />";
                        attachInfo = attachInfo + formattedComments + "</span><br /></li>\n";
                    }

                    contents = contents.replace("$$UserName", contractorName);
                    contents = contents.replace("$$AttachInfo", attachInfo);
                    JSONArray newToArr = new JSONArray();
                    newToArr.put(contractorEMail);
                    jsonParam.put("to", newToArr);
                    jsonParam.put("quillMessage", true);
                    jsonParam.put("sender", sender);
                    jsonParam.put("contents", contents);
                    jsonParam.put("subject", subject);
                    result = sendMail(request, JSONUtil.jsonStringToMap(jsonParam.toString()));
                    System.out.println("result:" + result);
                    logger.debug("All jobs are done!");
                }
            }
        } catch (Exception var26) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var26);
            var26.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var26.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendRejectMail", System.currentTimeMillis() - timeS);
            return result;
        }
    }

    private static JSONObject getFileJsonObject(String fileName, String fileFullPath) {
        JSONObject fileJson = null;
        fileJson = new JSONObject();
        fileJson.put("name", fileName);
        fileJson.put("path", fileFullPath);

        Scanner scanner;
        String var4;
        for(scanner = new Scanner("aaaa"); scanner.hasNextLine(); var4 = scanner.nextLine()) {
        }

        scanner.close();
        return fileJson;
    }

    private static JSONObject sendMail(HttpServletRequest request, Map parameter) {
        JSONObject result = new JSONObject();
        Message message = null;
        JSONObject jsonParam = null;

        try {
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String sender = jsonParam.optString("sender", (String)null);
            if (StringUtil.isNullBlank(sender)) {
                sender = MessageServiceConfigUtil.getProperty("Mail.Sender", MessageServiceConfigUtil.getProperty("mail.smtp.user"));
            }

            if (!StringUtil.isNotNullBlank(sender)) {
                result.put("result", false);
                result.put("faultMessage", "There is no sender email address");
                throw new MessageServiceException("There is no sender email address");
            }

            InternetAddress[] toRecipients = null;
            if (jsonParam.has("to")) {
                toRecipients = MessageServiceUtil.getInternetAddresses(removeCharInJSONArray(jsonParam.getJSONArray("to")));
            }

            InternetAddress[] ccRecipients = null;
            if (jsonParam.has("cc")) {
                ccRecipients = MessageServiceUtil.getInternetAddresses(removeCharInJSONArray(jsonParam.getJSONArray("cc")));
            }

            if (null == toRecipients && null == ccRecipients) {
                throw new MessageServiceException("There is no mail recipients");
            }

            InternetAddress[] bccRecipients = null;
            InternetAddress[] replyTos = jsonParam.has("replyTo") ? MessageServiceUtil.getInternetAddresses(jsonParam.getJSONArray("replyTo")) : MessageServiceUtil.getInternetAddresses(MessageServiceConfigUtil.getProperty("Mail.ReplyTo"), ";");
            InternetAddress senderIA = jsonParam.has("senderName") ? new InternetAddress(sender, jsonParam.getString("senderName")) : new InternetAddress(sender);
            String subject = jsonParam.optString("subject", "No subject");
            String contextType = jsonParam.optString("contentType", "text/html; charset=UTF-8");
            String mailContents = jsonParam.getString("contents");
            List<AttachmentPart> attachmentPartList = null;
            if (jsonParam.getBoolean("quillMessage")) {
                InlineContentParser inlineContentParser = new InlineContentParser(mailContents);
                inlineContentParser.parse();
                mailContents = inlineContentParser.getContents();
                attachmentPartList = inlineContentParser.getAttachmentPartList();
            }

            String contents = ExpressionEvaluator.evaluateExpressions(mailContents);
            MessagePart[] messageParts = new MessagePart[]{new MessagePart("body", contextType, contents)};
            if (jsonParam.has("attachments")) {
                JSONArray jsonArray = jsonParam.getJSONArray("attachments");
                int len = jsonArray.length();
                if (null == attachmentPartList) {
                    attachmentPartList = new ArrayList(len);
                }

                for(int i = 0; i < len; ++i) {
                    JSONObject json = jsonArray.getJSONObject(i);
                    String path = FileServiceUtil.instance.getRepositoryFileFullPath(json.getString("path"));
                    ((List)attachmentPartList).add(new AttachmentPart(json.getString("name"), path));
                }
            }

            AttachmentPart[] attachmentParts = null;
            if (null != attachmentPartList) {
                attachmentParts = (AttachmentPart[])((List)attachmentPartList).toArray(new AttachmentPart[((List)attachmentPartList).size()]);
            }

            MessageSender.getInstance().sendMail(senderIA, toRecipients, subject, messageParts, attachmentParts, replyTos, ccRecipients, (InternetAddress[])bccRecipients);
            result.put("result", true);
        } catch (Throwable var22) {
            result.put("result", false);
            result.put("faultMessage", var22.getMessage());
            var22.printStackTrace();
        }

        return result;
    }

    private static JSONArray removeCharInJSONArray(JSONArray inArray) {
        if (inArray != null && inArray.length() >= 1) {
            for(int i = 0; i < inArray.length(); ++i) {
                Object item = inArray.get(i);
                if (item instanceof String) {
                    String itemStr = (String)item;
                    if (itemStr.indexOf(",") > -1) {
                        itemStr = itemStr.replaceAll(",", " ");
                        inArray.put(i, itemStr);
                    }
                }
            }

            return inArray;
        } else {
            return inArray;
        }
    }

    public static String getToday() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleformat = new SimpleDateFormat("MM/dd/yyyy");
        return simpleformat.format(cal.getTime());
    }

    public static void main(String[] args) {
        String paramString = "{\n\"procid\":123,\n\"reqType\":\"IH\", \n\"individualInfos\":[ \n{ \n\"userNumber\":1, \n\"name\":\"Hyunsung\", \n\"position\":\"dd\", \n\"email\":\"hkim@bizflow.com\", \n\"phone\":\"(111) 111 - 1111\" \n}, \n{ \n\"userNumber\":2, \n\"name\":\"Yoo\", \n\"position\":\"dd\", \n\"email\":\"hkim@bizflow.com\", \n\"phone\":\"(111) 111 - 1111\" \n}\n], \n\"reqData\":{ \n\"title\":\"Optiplex\", \n\"instructTheUser\":\"Y\", \n\"verifyTheUserAD\":\"Y\", \n\"verifyDATFile\":\"Y\", \n\"verifyRecent\":\"Y\", \n\"machineType\":\"machineType\", \n\"computerName\":\"computerName\", \n\"machineSerial\":\"machineSerial\", \n\"assetTag\":\"assetTag\", \n\"nsDescription\":\"nsDescription\", \n\"computerName2\":\"computerName2\", \n\"location\":\"location\", \n\"serviceDesk\":\"serviceDesk\", \n\"powerOn\":\"powerOn\", \n\"recoveredEquipment\":\"recoveredEquipment\", \n\"modelTag\":\"modelTag\", \n\"assetTag2\":\"assetTag2\" \n} \n}";
        System.out.println("paramString=" + paramString);
        JSONObject job = new JSONObject(paramString);
        sendMailNotify("", "", (HashMap)job.toMap());
    }
}
