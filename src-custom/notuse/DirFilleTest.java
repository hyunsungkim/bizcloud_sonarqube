package notuse;//
import com.bizflow.io.custom.services.bizcloud.util.InfoFileFilter;

import java.io.File;

public class DirFilleTest {

    public static void main(String[] args) {
        // create a file object pointing to the directory
        File directory = new File("D:\\tmp\\results\\results");
        // pass an object of our file filter while listing files
        File[] listOfMatchingFiles  = directory.listFiles(new InfoFileFilter(1161, 10561));
        // iterate over the list of files
        for (File file : listOfMatchingFiles) {
            // print their path
            System.out.println(file.getAbsolutePath());
        }
        System.out.println("File Array1.length="+listOfMatchingFiles.length);

        File directory2 = new File("D:\\tmp\\results\\results\\1161");
        // pass an object of our file filter while listing files
        File[] listOfMatchingFiles2  = directory2.listFiles(new InfoFileFilter(1161, 10561));
        // iterate over the list of files
        for (File file2 : listOfMatchingFiles2) {
            // print their path
            System.out.println(file2.getAbsolutePath());
        }
        System.out.println("File Array2.length="+listOfMatchingFiles2.length);

        File[] sumFile = addFileArray(listOfMatchingFiles, listOfMatchingFiles2);
        System.out.println("File ArrayAll.length="+sumFile.length);

        for (File file3 : sumFile) {
            // print their path
            System.out.println(file3.getAbsolutePath());
        }

    }

    public static File[] addFileArray(File[] arr1, File[] arr2){
        File[] sArr = null;
        int sum1 = -1;
        int sum2 = -1;
        int ind = 0;
        if(arr1 == null && arr2 == null){
            return null;
        } else if (arr1 == null){
            return arr2;
        } else if (arr2 == null){
            return arr1;
        }
        sum1 = arr1.length;
        sum2 = arr2.length;
        sArr = new File[sum1 + sum2];
        for(ind = 0;ind < arr1.length; ind++){
            sArr[ind] = arr1[ind];
        }

        for(int i = 0;i < arr2.length; i++){
            sArr[ind++] = arr2[i];
        }

        return sArr;
    }
}
