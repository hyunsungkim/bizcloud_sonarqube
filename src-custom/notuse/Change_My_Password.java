package notuse;//
// Change_My_Password.java
import java.io.*;
import com.hs.bf.web.beans.*;
import com.hs.bf.web.xmlrs.*;


public class Change_My_Password
{
    static { org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.WARN); }

    // ---Modify the following variables with proper information. ---
    String serverIP = "3.80.178.29";
    int serverPort = 7201;
    String loginID = "hkim";
    String oldpassword = "1";
    String newpassword = "2";


    // --------------------------------------------------------------

    public void Change_My_Password()
    {
        try
        {
            HWSessionFactory hwSessionFactory = new HWSessionFactory();
            hwSessionFactory.setDefaultImplClassName(HWSessionFactory.TCPImpl);

            HWSession hwSession = hwSessionFactory.newInstance();

//            String sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, oldpassword,true);
            String sessionInfoXML = hwSession.logIn(serverIP, serverPort, "administrator", "xxxx",true);

            boolean checkPassword = false;
//            boolean checkPassword = hwSession.checkUserPassword(sessionInfoXML, oldpassword);
//            boolean checkPassword = hwSession.checkUserPassword(sessionInfoXML, "aa");
            System.out.println("checkUserPassword is called with his/her current password.");
            System.out.println("Is the password " + oldpassword + "? " + checkPassword);

//            hwSession.changePassword(loginID, oldpassword, newpassword);
            hwSession.changePassword(loginID, "", newpassword);
            System.out.println("Password changed as " + newpassword);

            checkPassword = hwSession.checkUserPassword(sessionInfoXML, newpassword);
            System.out.println("checkUserPassword is called with his/her changed password.");
            System.out.println("Is the password " + newpassword + "? " + checkPassword);
            hwSession.logOut(sessionInfoXML);

            sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, newpassword, true);
            hwSession.changePassword(loginID, newpassword, oldpassword);
            System.out.println("Password changed as " + oldpassword);





            hwSession.logOut(sessionInfoXML);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String args[])
    {

        (new Change_My_Password()).Change_My_Password();
    };
};
