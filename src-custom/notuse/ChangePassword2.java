package notuse;//
// Change_My_Password.java
import java.io.*;
import java.net.*;
import java.time.LocalDateTime;

import com.bizflow.io.core.json.JSONObject;
import com.hs.bf.web.beans.*;
import com.hs.bf.web.xmlrs.*;
import com.hs.frmwk.xml.dom.*;
import com.hs.frmwk.xml.dom.util.*;
import com.hs.frmwk.web.util.*;
import sun.security.ssl.SSLSocketFactoryImpl;

/**
 * Change my password
 * <p>
 * @see HWSession
 *
 */
public class ChangePassword2
{
    static { org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.WARN); }

    // ---Modify the following variables with proper information. ---
    String serverIP = "bizcloud.bizflow.com";
    int serverPort = 7201;
    String loginID = "administrator";
    String oldpassword = "xxxx";
    String newpassword = "newpassword";

    String userID = "0000000226";	// User ID to close.


    // --------------------------------------------------------------

    public void Change_My_Password()
    {
        try
        {
            HWSessionFactory hwSessionFactory = new HWSessionFactory();
            hwSessionFactory.setDefaultImplClassName(HWSessionFactory.TCPImpl);

            HWSession hwSession = hwSessionFactory.newInstance();

            String sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, oldpassword,true);

            HWSessionInfo hwSessionInfo = new HWSessionInfo();
            hwSessionInfo.reset (sessionInfoXML);
/**
            boolean checkPassword = hwSession.checkUserPassword(sessionInfoXML, oldpassword);
            System.out.println("checkUserPassword is called with his/her current password.");
            System.out.println("Is the password " + oldpassword + "? " + checkPassword);

            hwSession.changePassword(loginID, oldpassword, newpassword);
            System.out.println("Password changed as " + newpassword);

            checkPassword = hwSession.checkUserPassword(sessionInfoXML, newpassword);
            System.out.println("checkUserPassword is called with his/her changed password.");
            System.out.println("Is the password " + newpassword + "? " + checkPassword);
            hwSession.logOut(sessionInfoXML);

            sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, newpassword, true);
            hwSession.changePassword(loginID, newpassword, oldpassword);
            System.out.println("Password changed as " + oldpassword);
*/
            // --------------------------------------------------------------------
            XMLResultSet xrsLicGrps = null;
            xrsLicGrps = new XMLResultSetImpl();

            com.hs.bf.web.beans.HWFilter filter = new com.hs.bf.web.beans.HWFilter();
            filter.setName("HWGROUPPARTICIPANT");
            filter.addFilter("TYPE", "E", "L");
            filter.addFilter("MEMBERID", "E", userID);
            BufferedInputStream bis = new BufferedInputStream(hwSession.getParticipants(hwSessionInfo.toString(), filter.toByteArray()));
            xrsLicGrps.setLookupField("USERGROUPID");
            xrsLicGrps.parse(bis);
            bis.close();

            JSONObject licInfo = getLicenseInfo(hwSessionFactory, hwSessionInfo);
// --------------------------------------------------------------------

            hwSession.logOut(sessionInfoXML);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    static String eValue = "VALUE";
    private static JSONObject getLicenseInfo(HWSessionFactory hwSessionFactory, HWSessionInfo hwSessionInfo){
        String eName = "NAME";
        String eCompanyName = "COMPANY_NAME";
        String eProductName = "PRODUCT_NAME";
        String eExpirationDate = "EXPIRATION_DATE";
        String eIP = "IP";
        String eProcessDefinition = "PROCESS_DEFINITION";
        String eUsedProcessDefinition = "USED_PROCESS_DEFINITION";
        String eSystemAdmin = "SYSTEM_ADMIN";
        String eGeneralAdmin = "GENERAL_ADMIN";
        String eDesigner = "DESIGNER";
        String eUser = "USER";
        String eConcUser = "CONC_USER";
        String eOfficeEngineUser= "OFFICEENGINE_USER";
        String eUsedSystemAdmin = "USED_SYSTEM_ADMIN";
        String eUsedGeneralAdmin = "USED_GENERAL_ADMIN";
        String eUsedDesigner = "USED_DESIGNER";
        String eUsedUser = "USED_USER";
        String eUsedConcUser = "USED_CONC_USER";
        String eUsedOfficeEngineUser= "USED_OFFICEENGINE_USER";
        String eConcSessionUser = "CONC_SESSION_USER";
        String eConcLicenseModel = "CONC_LICENSE_MODEL";
        String eLoginUsedSystemAdmin = "LOGIN_USED_SYSTEM_ADMIN";
        String eLoginUsedGeneralAdmin = "LOGIN_USED_GENERAL_ADMIN";
        String eLoginUsedDesigner = "LOGIN_USED_DESIGNER";
        String eLoginUsedUser = "LOGIN_USED_USER";
        String eLoginUsedConcUser = "LOGIN_USED_CONC_USER";
        String eLoginUsedOfficeEngineUser= "LOGIN_USED_OFFICEENGINE_USER";
        String eReport= "REPORTING";
        String eReportDesigner= "REPORT_DESIGNER";
        String eUsedReportDesigner= "USED_REPORT_DESIGNER";
        String eLoginUsedReportDesigner= "LOGIN_USED_REPORT_DESIGNER";
        String eReportUser= "REPORT_USER";
        String eUsedReportUser= "USED_REPORT_USER";
        String eLoginUsedReportUser= "LOGIN_USED_REPORT_USER";
        String eWebMaker= "WEBMAKER_DESIGNER";
        String eUsedWebMaker= "USED_WEBMAKER_DESIGNER";
        String eLoginUsedWebMaker= "LOGIN_USED_WEBMAKER_DESIGNER";
        String eAppDev= "APPDEV_DESIGNER";
        String eUsedAppDev= "USED_APPDEV_DESIGNER";
        String eLoginUsedAppDev= "LOGIN_USED_APPDEV_DESIGNER";
        String isUpdate = "false";

        String msgError = "";
        int errorNumber = 0;
        int definition_total = 0, definition_used = 0;
        int sysadmin_total = 0, sysadmin_used = 0, sysadmin_login=0;
        int genadmin_total = 0, genadmin_used = 0, genadmin_login=0;
        int designer_total = 0, designer_used = 0, designer_login=0;
        int user_total = 0, user_used = 0, user_login=0;
        int officeengine_user_total = 0, officeengine_user_used = 0, officeengine_user_login=0;
        boolean enableReport = false;
        int report_designer_total = 0, report_designer_used = 0, report_designer_login=0;
        int report_user_total = 0, report_user_used = 0, report_user_login=0;
        int webmaker_total = 0, webmaker_used = 0, webmaker_login=0;
        int appdev_total = 0, appdev_used = 0, appdev_login=0;
        int concurrent_session_Upurchased = 0;
        int concurrent_session_LUpurchased = 0;
        int concurrent_session_login = 0;
        int concurrent_users_Uallowed = 0;
        int concurrent_users_LUallowed = 0;
        int concurrent_users_Uused = 0;
        int concurrent_users_LUused = 0;
        String viewModel = "true";
        Element ipElem = null, companyElem = null, elemERA = null, productElem = null, expirationDateElem=null;
        JSONObject licInfo = new JSONObject();

        try
        {
            Element root = null, elem = null;
            HWSession hwSession = hwSessionFactory.newInstance();

            InputStream is = hwSession.getLicenseInfoXML(hwSessionInfo.toString());
//    com.hs.ja.file.FileIOUtil.write(is, "C:\\x.xml");
//    is = new FileInputStream("C:\\x.xml");

            Parser parser = new ParserImpl();
            parser.parse(is);
            root = parser.getDocumentElement().childAt(0);

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eCompanyName + "']");
            companyElem = elem.getParent();

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eProductName + "']");
            productElem = elem.getParent();

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eExpirationDate + "']");
            expirationDateElem = elem.getParent();

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eIP + "']");
            ipElem = elem.getParent();

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eProcessDefinition + "']");
            elem = elem.getParent();
            definition_total = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("definition_total", definition_total);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUsedProcessDefinition + "']");
            elem = elem.getParent();
            definition_used = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("definition_used", definition_used);

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eSystemAdmin + "']");
            elem = elem.getParent();
            sysadmin_total = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("sysadmin_total", sysadmin_total);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUsedSystemAdmin + "']");
            elem = elem.getParent();
            sysadmin_used = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("sysadmin_used", sysadmin_used);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eLoginUsedSystemAdmin + "']");
            if(null != elem)
            {
                elem = elem.getParent();
                sysadmin_login = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("sysadmin_login", sysadmin_login);
            }

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eGeneralAdmin + "']");
            elem = elem.getParent();
            genadmin_total = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("genadmin_total", genadmin_total);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUsedGeneralAdmin + "']");
            elem = elem.getParent();
            genadmin_used = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("genadmin_used", genadmin_used);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eLoginUsedGeneralAdmin + "']");
            if(null != elem)
            {
                elem = elem.getParent();
                genadmin_login = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("genadmin_login", genadmin_login);
            }

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eDesigner + "']");
            elem = elem.getParent();
            designer_total = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("designer_total", designer_total);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUsedDesigner + "']");
            elem = elem.getParent();
            designer_used = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("designer_used", designer_used);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eLoginUsedDesigner + "']");
            if(null != elem)
            {
                elem = elem.getParent();
                designer_login = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("designer_login", designer_login);
            }

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUser + "']");
            elem = elem.getParent();
            user_total = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("user_total", user_total);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUsedUser + "']");
            elem = elem.getParent();
            user_used = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("user_used", user_used);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eLoginUsedUser + "']");
            if(null != elem)
            {
                elem = elem.getParent();
                user_login = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("user_login", user_login);
            }

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eOfficeEngineUser+ "']");
            if(null != elem)
            {
                elem = elem.getParent();
                officeengine_user_total = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("officeengine_user_total", officeengine_user_total);
                elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUsedOfficeEngineUser+ "']");
                elem = elem.getParent();
                officeengine_user_used = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("officeengine_user_used", officeengine_user_used);
                elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eLoginUsedOfficeEngineUser+ "']");
                elem = elem.getParent();
                officeengine_user_login = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("officeengine_user_login", officeengine_user_login);
            }

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eConcUser + "']");
            elem = elem.getParent();
            concurrent_users_Uallowed = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("concurrent_users_Uallowed", concurrent_users_Uallowed);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eUsedConcUser + "']");
            elem = elem.getParent();
            concurrent_users_Uused = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("concurrent_users_Uused", concurrent_users_Uused);

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eConcSessionUser + "']");
            elem = elem.getParent();
            concurrent_session_Upurchased = getIntegerFromString(elem.getChildText(eValue));
            licInfo.put("concurrent_session_Upurchased", concurrent_session_Upurchased);
            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eLoginUsedConcUser + "']");
            if(null != elem)
            {
                elem = elem.getParent();
                concurrent_session_login = getIntegerFromString(elem.getChildText(eValue));
                licInfo.put("concurrent_session_login", concurrent_session_login);
            }

            elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='" + eConcLicenseModel + "']");

            if (elem != null)
            {
                elem = elem.getParent();
                viewModel = elem.getChildText(eValue);
            }
            else
            {
                viewModel = "true";
            }

            if ("true".equalsIgnoreCase(isUpdate))
            {
                elem = XPathUtil.selectSingleElement(root,  "*/" + eName + "[string(.)='ENA']");
                elemERA = elem.getParent();

                if (null != elemERA)
                {
                    String licenseENA = elemERA.getChildText(eValue);
//                    session.setAttribute("licenseENA", licenseENA);
                }
            }

            elem = XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eReport + "']");
            if (null != elem)
            {
                elem = elem.getParent();

                if (null != elem)
                {
                    enableReport = "True".equalsIgnoreCase(elem.getChildText(eValue));
                }
            }

            if (enableReport)
            {
                elem = XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eReportDesigner + "']");
                if (null != elem)
                {
                    report_designer_total = getCount(elem);
                    report_designer_used = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eUsedReportDesigner + "']"));
                    report_designer_login = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eLoginUsedReportDesigner + "']"));
                    licInfo.put("report_designer_total", report_designer_total);
                    licInfo.put("report_designer_used", report_designer_used);
                    licInfo.put("report_designer_login", report_designer_login);
                }

                elem = XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eReportUser + "']");
                if (null != elem)
                {
                    report_user_total = getCount(elem);
                    report_user_used = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eUsedReportUser + "']"));
                    report_user_login = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eLoginUsedReportUser + "']"));
                    licInfo.put("report_user_total", report_user_total);
                    licInfo.put("report_user_used", report_user_used);
                    licInfo.put("report_user_login", report_user_login);
                }
            }

            elem = XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eWebMaker + "']");
            if (null != elem)
            {
                webmaker_total = getCount(elem);
                webmaker_used = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eUsedWebMaker + "']"));
                webmaker_login = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eLoginUsedWebMaker + "']"));
                licInfo.put("webmaker_total", webmaker_total);
                licInfo.put("webmaker_used", webmaker_used);
                licInfo.put("webmaker_login", webmaker_login);
            }

            elem = XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eAppDev + "']");
            if (null != elem)
            {
                appdev_total = getCount(elem);
                appdev_used = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eUsedAppDev + "']"));
                appdev_login = getCount(XPathUtil.selectSingleElement(root, "*/" + eName + "[string(.)='" + eLoginUsedAppDev + "']"));
                licInfo.put("appdev_total", appdev_total);
                licInfo.put("appdev_used", appdev_used);
                licInfo.put("appdev_login", appdev_login);
            }

            System.out.println("the End = end");
        }
        catch (HWException e)
        {
            e.printStackTrace();
            errorNumber = e.getNumber();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            msgError = e.toString();
        }
        finally {
            return licInfo;
        }
    }

    private static int getIntegerFromString(String s){
        int n = 0;

        try{
            int len = s.length();
            StringBuffer sb = new StringBuffer(len);

            if ('-' == s.charAt(0) || '+' == s.charAt(0)) {
                sb.append(s.charAt(0));
            }

            for (int i = 0; i != len; ++i) {
                char ch = s.charAt(i);

                if (Character.isDigit(ch)) {
                    sb.append(ch);
                }
            }

            n = Integer.parseInt(sb.toString());
        }
        catch (Exception e){
        }

        return n;
    }

    private static int getCount(Element elem)
    {
        int count = 0;

        if(null != elem)
        {
            elem = elem.getParent();
            count = getIntegerFromString(elem.getChildText(eValue));
        }

        return count;
    }

    public static boolean checkServerAvailability(String host, int port, int timeout) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), timeout);
            return true;
        } catch (IOException e) {
            return false; // time is out , or the server is unreachable
        }
    }

    public static String checkCurlOutputForSSLServer(String serverIP) throws Exception{
        String retValue = null;
        String cmd = "curl -k -s https://" + serverIP.trim() + "/bizflow/index.html | grep '<HTML'";
        Process proc = Runtime.getRuntime().exec(cmd);

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(proc.getInputStream()));

        String line = "";
        while ((line = reader.readLine()) != null) {
//            System.out.print(line + "\n");
            retValue = line;
            break;
        }
        proc.waitFor();
        return retValue;
    }


    public static void main(String args[]) throws Exception
    {
        int min = LocalDateTime.now().getMinute();
        System.out.println("LocalDateTime.now().getMinute();=" + min);
        System.out.println("Current Min % 5 = " + min % 5);


        String appServerIP = "107.22.55.11";
        String webServerIP = "54.164.149.121";
        String appServerURL = "http://"+appServerIP+":8080/bizflow";
        String webServerURL = "https://"+webServerIP+"/bizflow";

        boolean isLiveBizFlow = checkServerAvailability(appServerIP, 7201, 3000);
        boolean isLiveApp = checkServerAvailability(appServerIP, 8080, 3000);
        boolean isLiveWeb = checkServerAvailability(webServerIP, 443, 3000);

        System.out.println("BizFlow Live=" + isLiveBizFlow);
        System.out.println("AppServer Live=" + isLiveApp);
        System.out.println("WebServer Live=" + isLiveWeb);


//        boolean appServerReachable = InetAddress.getByName(appServerIP).isReachable(3000);
//        boolean webServerReachable = InetAddress.getByName(webServerIP).isReachable(3000);

//        System.out.println("AppServer Reachable=" + appServerReachable);
//        System.out.println("WebServer Reachable=" + webServerReachable);
//        System.out.println("AppServer InetAddress.getByName(appServerIP).toString()=" + InetAddress.getByName(appServerIP).toString());

//        System.out.println("App Response Code="+checkServerResponse(appServerURL));
//        System.out.println("Web Response Code="+checkSSL(webServerURL));

        System.out.println("SSL Check = " + checkCurlOutputForSSLServer(webServerIP));


        /*
        String rds = "prd-1586-database.c5odjulcwn5h.us-east-1.rds.amazonaws.com";
        int idPos = rds.indexOf(".");
        String dbId = rds.substring(0, idPos);
        System.out.println("dbId="+ dbId);


         */
        // (new ChangePassword2()).Change_My_Password();
    };
};
