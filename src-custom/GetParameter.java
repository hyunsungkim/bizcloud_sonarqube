
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.SsmClientBuilder;
import software.amazon.awssdk.services.ssm.model.*;

import java.util.ArrayList;
import java.util.List;

public class GetParameter {

    public static void main(String[] args) {
/*
        final String USAGE = "\n" +
                "Usage:\n" +
                "    GetParameter <paraName>\n\n" +
                "Where:\n" +
                "    paraName - the name of the parameter\n";

        if (args.length < 1) {
            System.out.println(USAGE);
            System.exit(1);
        }
*/
        // Get args
        String paraName = null;
        if(args.length == 1) {
            paraName = args[0];
        } else if (args.length == 0){
            paraName = "service.create.sqs.queue.name";
        }

        Region region = Region.US_EAST_1;
        SsmClient ssmClient = SsmClient.builder()
//                .region(region)
                .build();

        try {
            GetParameterRequest parameterRequest = GetParameterRequest.builder()
                    .name(paraName)
                    .build();

            GetParameterResponse parameterResponse = ssmClient.getParameter(parameterRequest);
            String paramValue = parameterResponse.parameter().value();
            System.out.println("The parameter value is " + paramValue);

        } catch (SsmException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        try{
            paraName = "bpm.system.user.id";
            GetParameterRequest parameterRequest = GetParameterRequest.builder()
                    .name(paraName).withDecryption(true)
                    .build();

            GetParameterResponse parameterResponse = ssmClient.getParameter(parameterRequest);
            String paramValue = parameterResponse.parameter().value();
            System.out.println("The parameter value is " + paramValue);
        } catch(SsmException e1) {
            e1.printStackTrace();
        }

        try{

            Tag tag = Tag.builder().key("Tag1").value("tagvalue").build();

            paraName = "bpm.yyyyyyyyyyyy";
            PutParameterRequest parameterRequest = PutParameterRequest.builder()
                    .name(paraName).type("SecureString").description("This is description...")
                    .value("1234567890").dataType("text").tags(tag)
                    .build();

            PutParameterResponse parameterResponse = ssmClient.putParameter(parameterRequest);

            SsmResponseMetadata sm = parameterResponse.responseMetadata();
            System.out.println("The parameter value is " + sm.toString());
        } catch(SsmException e1) {
            e1.printStackTrace();
        }
    }


}
