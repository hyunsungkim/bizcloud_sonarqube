package com.bizflow.io.services.custom;

import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.services.core.model.ServiceMode;
import com.bizflow.io.services.core.system.response.async.AsyncManager;
import com.bizflow.io.services.core.system.response.async.AsyncResponseProcessor;
import com.bizflow.io.services.file.util.FileServiceConfigUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Enumeration;
import java.util.Properties;

/**
 * File Custom Service
 *
 * @author Hwansoon Park
 * @version 1.0
 * @since 1.0
 */
@Path("/custom")
public class FileCustomService extends FileCustomServiceCore {
    private static final Logger logger = LogManager.getLogger(FileCustomService.class);
    /**
     * Constructor
     */
    public FileCustomService() {
        super(ServiceMode.Async);
    }

    /**
     * Gets a file
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    @POST
    @Path("/file/properties")
    public void getFileProperties(@Context HttpServletRequest request, @Context HttpServletResponse response, @Suspended AsyncResponse asyncResponse) {
        AsyncManager.getInstance().run(new AsyncResponseProcessor(this, asyncResponse, request, "FileCustomService.getFileProperties:POST") {
            public void processResponse() {
                Properties fileProperties = FileServiceConfigUtil.getConfiguration().getProperties();
                Enumeration propertyNames = fileProperties.propertyNames();
                JSONObject propertyJSON = new JSONObject();
                while (propertyNames.hasMoreElements()) {
                    String propertyName = (String)propertyNames.nextElement();
                    propertyJSON.put(propertyName, fileProperties.getProperty(propertyName));
                }
                Response.ResponseBuilder responseBuilder = Response.ok();
                responseBuilder.type("application/json");
                asyncResponse.resume(responseBuilder.entity(propertyJSON.toString()).build());
            }
        });
    }

    /**
     * Runs a virtual service by multipart form data
     *
     * @param repositoryName  a repository name
     * @param sessionName  a session name
     * @param serviceName   a service name
     * @param format        output format, json or xml
     * @param multiPart     multipart form data
     * @param request       HttpServletRequest
     * @param response      HttpServletResponse
     * @param asyncResponse Asynchronous Response
     */
    @POST
    @Path("/file/upload/{repositoryName}/{sessionName}.{serviceName}.{format}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void fileUploadAndRunVirtualService(@PathParam("repositoryName") String repositoryName, @PathParam("sessionName") String sessionName, @PathParam("serviceName") String serviceName, @PathParam("format") String format,
                                  FormDataMultiPart multiPart, @Context HttpServletRequest request, @Context HttpServletResponse response, @Suspended AsyncResponse asyncResponse) {
        AsyncManager.getInstance().run(new AsyncResponseProcessor(this, asyncResponse, request, "FileCustomService.fileUploadAndRunVirtualService:POST:multipart/form-data") {
            public void processResponse() {
                asyncResponse.resume(fileUploadAndRunVirtualService(repositoryName, sessionName, serviceName, format, multiPart, request, response, "FileCustomService.fileUploadAndRunVirtualService:POST:multipart/form-data"));
            }
        });
    }

    /**
     * Runs a virtual service by multipart form data
     *
     * @param fileID a fileID
     * @param repositoryName  a repository name
     * @param sessionName  a session name
     * @param serviceName   a service name
     * @param format        output format, json or xml
     * @param multiPart     multipart form data
     * @param request       HttpServletRequest
     * @param response      HttpServletResponse
     * @param asyncResponse Asynchronous Response
     */
    @POST
    @Path("/file/update/{repositoryName}/{fileID}/{sessionName}.{serviceName}.{format}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void fileUpdateAndRunVirtualService(@PathParam("repositoryName") String repositoryName, @PathParam("fileID") String fileID, @PathParam("sessionName") String sessionName, @PathParam("serviceName") String serviceName, @PathParam("format") String format,
                                               FormDataMultiPart multiPart, @Context HttpServletRequest request, @Context HttpServletResponse response, @Suspended AsyncResponse asyncResponse) {
        AsyncManager.getInstance().run(new AsyncResponseProcessor(this, asyncResponse, request, "FileCustomService.fileUpdateAndRunVirtualService:POST:multipart/form-data") {
            public void processResponse() {
                asyncResponse.resume(fileUpdateAndRunVirtualService(repositoryName, fileID, sessionName, serviceName, format, multiPart, request, response, "FileCustomService.fileUpdateAndRunVirtualService:POST:multipart/form-data"));
        }
        });
    }

    /**
     * Gets a file
     *
     * @param repository
     * @param category
     * @param orgName
     * @param displayName
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    @POST
    @Path("/file/download/{repository}/{category}/{orgName}/{displayName}")
    public void downloadNetSubFile(@PathParam("repository") String repository, @PathParam("category") String category,
                                   @PathParam("orgName") String orgName, @PathParam("displayName") String displayName,
                                   @Context HttpServletRequest request, @Context HttpServletResponse response, @Suspended AsyncResponse asyncResponse) {
        AsyncManager.getInstance().run(new AsyncResponseProcessor(this, asyncResponse, request, "FileCustomService.downloadFile:POST") {
            public void processResponse() {
                asyncResponse.resume(downloadNetSubFile(repository, category, orgName, displayName, false, request, response, "FileCustomService.downloadNetSubFile:POST"));
            }
        });
    }


    @POST
    @Path("/cloud/file/download/{repository}/{appType}/{majorVersion}/{displayName}")
    public void downloadCloudFile(@PathParam("repository") String repository, @PathParam("appType") String appType,
                                   @PathParam("majorVersion") String majorVersion, @PathParam("displayName") String displayName,
                                   @Context HttpServletRequest request, @Context HttpServletResponse response, @Suspended AsyncResponse asyncResponse) {
        AsyncManager.getInstance().run(new AsyncResponseProcessor(this, asyncResponse, request, "FileCustomService.downloadCloudFile:POST") {
            public void processResponse() {
                logger.debug("START---- processResponse()");
                asyncResponse.resume(downloadPatchFile(repository, appType, majorVersion, displayName, false, request, response, "FileCustomService.downloadPatchFile:POST"));
            }
        });
    }

    /**
     * Removes a file
     *
     * @param fileID a file name
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    @POST
    @Path("/file/remove/{repository}/{category}/{orgName}/{fileID}/{sessionName}.{serviceName}.{format}")
    public void removeFile(@PathParam("repository") String repository, @PathParam("category") String category, @PathParam("orgName") String orgName, @PathParam("fileID") String fileID, @PathParam("sessionName") String sessionName, @PathParam("serviceName") String serviceName, @PathParam("format") String format,
                           @Context HttpServletRequest request, @Context HttpServletResponse response, @Suspended AsyncResponse asyncResponse) {
        AsyncManager.getInstance().run(new AsyncResponseProcessor(this, asyncResponse, request, "FileCustomService.removeFile:POST") {
            public void processResponse() {
                asyncResponse.resume(fileRemoveAndRunVirtualService(repository, category, orgName, fileID, sessionName, serviceName, format, request, response, "FileCustomService.removeFile:POST"));
            }
        });
    }

    /**
     * Upload Private File From NetSub and Send Email To TriWest
     *
     * @param multiPart     multipart form data
     * @param request       HttpServletRequest
     * @param response      HttpServletResponse
     * @param asyncResponse Asynchronous Response
     */
    @POST
    @Path("/file/sendemail/{repository}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void sendEmailWithAttachment(@PathParam("repository") String repository,
                                               FormDataMultiPart multiPart, @Context HttpServletRequest request, @Context HttpServletResponse response, @Suspended AsyncResponse asyncResponse) {
        AsyncManager.getInstance().run(new AsyncResponseProcessor(this, asyncResponse, request, "FileCustomService.fileUploadAndRunVirtualService:POST:multipart/form-data") {
            public void processResponse() {
                asyncResponse.resume(sendEmailWithAttachment(repository, multiPart, request, response, "FileCustomService.sendEmailWithAttachment:POST:multipart/form-data"));
            }
        });
    }
}
