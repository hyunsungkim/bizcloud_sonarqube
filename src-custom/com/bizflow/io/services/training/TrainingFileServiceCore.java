package com.bizflow.io.services.training;

import com.bizflow.io.core.conf.ServiceType;
import com.bizflow.io.core.db.mybatis.model.FileBinaryStream;
import com.bizflow.io.core.db.mybatis.model.QueryConstant;
import com.bizflow.io.core.db.mybatis.util.QueryParamUtil;
import com.bizflow.io.core.expression.ExpressionEvaluator;
import com.bizflow.io.core.file.exception.FileNotAvailableException;
import com.bizflow.io.core.file.util.FileIOUtil;
import com.bizflow.io.core.file.util.FileUtil;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.lang.util.StringReplacer;
import com.bizflow.io.core.lang.util.StringUtil;
import com.bizflow.io.core.message.mail.AttachmentPart;
import com.bizflow.io.core.message.mail.MessagePart;
import com.bizflow.io.core.net.util.FormDataMultiPartUtil;
import com.bizflow.io.core.net.util.ParameterUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.core.util.SequenceGenerator;
import com.bizflow.io.core.util.UUIDGenerator;
import com.bizflow.io.core.web.util.ServletUtil;
import com.bizflow.io.services.core.message.OutputFormat;
import com.bizflow.io.services.core.message.util.OutputFormatKeeper;
import com.bizflow.io.services.core.model.ServiceMode;
import com.bizflow.io.services.core.service.CoreService;
import com.bizflow.io.services.core.util.CipherFileUtil;
import com.bizflow.io.services.data.dao.util.SqlTransactionDAOUtil;
import com.bizflow.io.services.file.exception.FileServiceException;
import com.bizflow.io.services.file.model.FileServiceConstant;
import com.bizflow.io.services.file.performlog.PerformanceLoggerUtil;
import com.bizflow.io.services.file.session.FileServiceSessionManager;
import com.bizflow.io.services.file.util.FileServiceConfigUtil;
import com.bizflow.io.services.file.util.FileServiceUtil;
import com.bizflow.io.services.message.exception.MessageServiceException;
import com.bizflow.io.services.message.sender.MessageSender;
import com.bizflow.io.services.message.util.MessageServiceConfigUtil;
import com.bizflow.io.services.message.util.MessageServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.bizflow.io.services.file.model.FileServiceConstant.EncodedFileNameParam;
import static com.bizflow.io.services.file.model.FileServiceConstant.FileParam;

/**
 * Training File Service Core
 *
 * @author Hwansoon Park
 * @version 1.0
 */
public class TrainingFileServiceCore extends CoreService {
    private static final Logger logger = LogManager.getLogger(TrainingFileServiceCore.class);
    private static SequenceGenerator sequenceGenerator = new SequenceGenerator();

    /**
     * Constructor
     *
     * @param serviceMode Service Mode
     */
    protected TrainingFileServiceCore(ServiceMode serviceMode) {
        super(ServiceType.File, serviceMode);
    }

    /**
     * Gets the parameter of a virtual service
     *
     * @param virtualService a virtual service option
     * @param multiPart      a multipart form data
     * @return a map that has query parameters
     * @since 1.0
     */
    private Map getVirtualServiceParameter(JSONObject virtualService, FormDataMultiPart multiPart) {
        Object _queryParam = virtualService.opt(QueryConstant.QueryParamKey);
        Map queryParam = null != _queryParam ? ParameterUtil.getParameterMap(multiPart, _queryParam) : new HashMap();

        JSONObject _hiddenParam = virtualService.optJSONObject(QueryConstant.HiddenQueryParamKey);
        if (null != _hiddenParam) {
            queryParam.putAll(ParameterUtil.getParameterMap(multiPart, _hiddenParam.toMap()));
        }

        JSONObject _runtimeParam = virtualService.optJSONObject(QueryConstant.RuntimeQueryParamKey);
        if (null != _runtimeParam) {
            for (String key : _runtimeParam.keySet()) {
                queryParam.put(key, QueryParamUtil.getParamValue(multiPart, _runtimeParam.getJSONObject(key)));
            }
        }

        JSONObject _adjustParam = virtualService.optJSONObject(QueryConstant.AdjustQueryParamKey);
        if (null != _adjustParam) {
            for (String key : _adjustParam.keySet()) {
                queryParam.put(key, QueryParamUtil.getAdjustParamValue(multiPart, _adjustParam.getString(key)));
            }
        }

        String mandatoryParam = virtualService.optString(QueryConstant.MandatoryQueryParamKey, null);
        if (null != mandatoryParam) {
            ParameterUtil.checkMandatoryParameters(queryParam, Arrays.asList(mandatoryParam.split(",")));
        }

        return queryParam;
    }

    /**
     * Runs a file upload and virtual service
     * @param repositoryName a repository name
     * @param sessionName    the session name of a virtual service
     * @param virtualService a virtual service
     * @param multiPart  multipart form data
     * @return result
     * @throws IOException
     */
    public Object fileUploadAndRunVirtualService(String repositoryName, String sessionName, JSONObject virtualService, FormDataMultiPart multiPart) throws IOException {
        OutputFormat outputFormat = OutputFormatKeeper.getOutputFormat();
        OutputFormat.MessageTemplate mt = outputFormat.setMessageTemplatesIfExist(virtualService.optString(QueryConstant.ReturnMessageTemplateKey, null),
                virtualService.optString(QueryConstant.ErrorMessageTemplateKey, null));

        String treeId = FormDataMultiPartUtil.getStringValue(multiPart, "TreeID");
        String description = FormDataMultiPartUtil.getStringValue(multiPart, "Description");

        FormDataBodyPart filePart = multiPart.getField(FileParam);
        File file = filePart.getValueAs(File.class);
        FileInputStream fileInputStream = null;
        String filename = null;
        String systemFileName = UUIDGenerator.newUUID();
        File baseRepositoryDir = FileServiceUtil.getInstance().getRepositoryDir(repositoryName);
        File repositoryDir = getFileRepositoryDir(repositoryName, baseRepositoryDir, multiPart);

        try {
            if (null != repositoryDir && repositoryDir.exists()) {
                filename = getFileName(filePart, FormDataMultiPartUtil.getStringValue(multiPart, EncodedFileNameParam));
                File oldFile = getOldFile(multiPart);
                String folderName = treeId;
                if (StringUtil.isNotNullBlank(filename)) {
                    File targetDir = new File(repositoryDir, folderName);
                    targetDir.mkdirs();

                    file = new File(targetDir.getAbsolutePath() + "/" + systemFileName + "_" + filename);

                    InputStream fileInputStream2 = filePart.getValueAs(InputStream.class);
                    try {
                        FileIOUtil.saveFile(fileInputStream2, file);
                    } finally {
                        fileInputStream2.close();
                    }
                }
                String savedFileName = StringUtil.isNotNullBlank(filename) ? filename : FormDataMultiPartUtil.getStringValue(multiPart, FileServiceConstant.OldNameParam);
                File savedFile = null != file ? file : oldFile;

                Map<String, Object> parameter = new HashMap<>();
                JSONObject reservedParamObj = virtualService.optJSONObject(FileServiceConstant.ReservedParamKey);
                if (null != reservedParamObj) {
                    String fileName = getFileName(filePart, FormDataMultiPartUtil.getStringValue(multiPart, EncodedFileNameParam));
                    long fileSize = file.length();
                    if (fileSize >= 0 && StringUtil.isNotNullBlank(fileName)) {
                        Iterator<String> it = reservedParamObj.keys();
                        while (it.hasNext()) {
                            String param = it.next();
                            String key = reservedParamObj.getString(param);
                            if (FileServiceConstant.ReservedFileNameParam.equals(key)) {
                                parameter.put(param, fileName);
                            } else if (FileServiceConstant.ReservedFileExtParam.equals(key)) {
                                parameter.put(param, FileUtil.getFileExtension(fileName));
                            } else if (FileServiceConstant.ReservedFileSizeParam.equals(key)) {
                                parameter.put(param, fileSize);
                            } else if (FileServiceConstant.ReservedFileInputStreamParam.equals(key)) {
                                fileInputStream = new FileInputStream(file);
                                parameter.put(param, fileInputStream);
                            } else if (FileServiceConstant.ReservedFileParam.equals(key)) {
                                parameter.put(param, file);
                            } else if (FileServiceConstant.ReservedFileJsonContent.equals(key) || FileServiceConstant.ReservedFileContent.equals(key)) {
                                String contents = CipherFileUtil.getFileContents(file, fileName);

                                if (FileServiceConstant.ReservedFileJsonContent.equals(key)) {
                                    parameter.put(param, JSONUtil.stringToJson(contents, true));
                                } else {
                                    parameter.put(param, contents);
                                }
                            } else if (FileServiceConstant.ReservedFileBinaryStream.equals(key)) {
                                parameter.put(param, new FileBinaryStream(file, false));
                            }
                        }
                    }
                }

                Map serviceParameter = getVirtualServiceParameter(virtualService, multiPart);
                if (null != serviceParameter && serviceParameter.size() > 0) {
                    parameter.putAll(serviceParameter);
                }

                parameter.put("treeId", treeId);
                parameter.put("description", description);
                parameter.put("path", savedFile.getPath());
                parameter.put("orgName", systemFileName + "_" + filename);
                Object result = SqlTransactionDAOUtil.runVirtualQuery(sessionName, virtualService, parameter);

                outputFormat.setMessageTemplates(mt);
                return result;
            } else {
                throw new IOException();
            }
        } catch (IOException e) {
            file.delete();
            logger.error("{}, {}: repositoryName={}, filename={}", RequestIdentifierKeeper.getRequestIdLogString(), repositoryName, treeId, repositoryDir, filename, e);
            throw new FileServiceException(e);
        } finally {
            if (null != fileInputStream) {
                fileInputStream.close();
            }
        }
    }

    /**
     * Runs a file update and virtual service
     * @param fileID a file key
     * @param repositoryName a repository name
     * @param sessionName  the session name of a virtual service
     * @param virtualService a virtual service
     * @param multiPart      multipart form data
     * @return result
     * @throws IOException
     * @since 1.0
     */
    public Object fileUpdateAndRunVirtualService(String repositoryName, String fileID, String sessionName, JSONObject virtualService, FormDataMultiPart multiPart) throws IOException {
        OutputFormat outputFormat = OutputFormatKeeper.getOutputFormat();
        OutputFormat.MessageTemplate mt = outputFormat.setMessageTemplatesIfExist(virtualService.optString(QueryConstant.ReturnMessageTemplateKey, null),
                virtualService.optString(QueryConstant.ErrorMessageTemplateKey, null));
        String treeId = FormDataMultiPartUtil.getStringValue(multiPart, "TreeID");
        String description = FormDataMultiPartUtil.getStringValue(multiPart, "Description");
        String filePath = FormDataMultiPartUtil.getStringValue(multiPart, "FilePath");

        FormDataBodyPart filePart = multiPart.getField(FileParam);

        File orgFile = null;
        FileInputStream fileInputStream = null;
        Map<String, Object> parameter = new HashMap<>();

        if (filePart != null) {
            File file = filePart.getValueAs(File.class);

            String filename = null;
            String systemFileName = UUIDGenerator.newUUID();
            File baseRepositoryDir = FileServiceUtil.getInstance().getRepositoryDir(repositoryName);
            File repositoryDir = getFileRepositoryDir(repositoryName, baseRepositoryDir, multiPart);

            orgFile = new File(filePath); // For Delete
            try {
                if (null != repositoryDir && repositoryDir.exists()) {
                    filename = getFileName(filePart, FormDataMultiPartUtil.getStringValue(multiPart, EncodedFileNameParam));
                    File oldFile = getOldFile(multiPart);
                    String folderName = treeId;
                    if (StringUtil.isNotNullBlank(filename)) {
                        File targetDir = new File(repositoryDir, folderName);
                        targetDir.mkdirs();

                        file = new File(targetDir.getAbsolutePath() + "/" + systemFileName + "_" + filename);

                        InputStream fileInputStream2 = filePart.getValueAs(InputStream.class);
                        try {
                            FileIOUtil.saveFile(fileInputStream2, file);
                        } finally {
                            fileInputStream2.close();
                        }
                    }
                    String savedFileName = StringUtil.isNotNullBlank(filename) ? filename : FormDataMultiPartUtil.getStringValue(multiPart, FileServiceConstant.OldNameParam);

                    File savedFile = null != file ? file : oldFile;

                    JSONObject reservedParamObj = virtualService.optJSONObject(FileServiceConstant.ReservedParamKey);
                    if (null != reservedParamObj) {
                        String fileName = getFileName(filePart, FormDataMultiPartUtil.getStringValue(multiPart, EncodedFileNameParam));
                        long fileSize = file.length();
                        if (fileSize >= 0 && StringUtil.isNotNullBlank(fileName)) {
                            Iterator<String> it = reservedParamObj.keys();
                            while (it.hasNext()) {
                                String param = it.next();
                                String key = reservedParamObj.getString(param);
                                if (FileServiceConstant.ReservedFileNameParam.equals(key)) {
                                    parameter.put(param, fileName);
                                } else if (FileServiceConstant.ReservedFileExtParam.equals(key)) {
                                    parameter.put(param, FileUtil.getFileExtension(fileName));
                                } else if (FileServiceConstant.ReservedFileSizeParam.equals(key)) {
                                    parameter.put(param, fileSize);
                                } else if (FileServiceConstant.ReservedFileInputStreamParam.equals(key)) {
                                    fileInputStream = new FileInputStream(file);
                                    parameter.put(param, fileInputStream);
                                } else if (FileServiceConstant.ReservedFileParam.equals(key)) {
                                    parameter.put(param, file);
                                } else if (FileServiceConstant.ReservedFileJsonContent.equals(key) || FileServiceConstant.ReservedFileContent.equals(key)) {
                                    String contents = CipherFileUtil.getFileContents(file, fileName);

                                    if (FileServiceConstant.ReservedFileJsonContent.equals(key)) {
                                        parameter.put(param, JSONUtil.stringToJson(contents, true));
                                    } else {
                                        parameter.put(param, contents);
                                    }
                                } else if (FileServiceConstant.ReservedFileBinaryStream.equals(key)) {
                                    parameter.put(param, new FileBinaryStream(file, false));
                                }
                            }
                        }
                    }
                    Map serviceParameter = getVirtualServiceParameter(virtualService, multiPart);
                    if (null != serviceParameter && serviceParameter.size() > 0) {
                        parameter.putAll(serviceParameter);
                    }
                    parameter.put("path", savedFile.getPath());
                    parameter.put("orgName", systemFileName + "_" + filename);
                } else {
                    throw new IOException();
                }
            } catch (IOException e) {
                file.delete();
                logger.error("{}, {}: repositoryName={}, filename={}", RequestIdentifierKeeper.getRequestIdLogString(), repositoryName, treeId, repositoryDir, filename, e);
                throw new FileServiceException(e);
            }
        }
        try {
            parameter.put("description", description);
            parameter.put("id", fileID);
            Object result = SqlTransactionDAOUtil.runVirtualQuery(sessionName, virtualService, parameter);

            outputFormat.setMessageTemplates(mt);
            return result;
        } catch (Exception e) {
            logger.error("{}, {}", RequestIdentifierKeeper.getRequestIdLogString(), repositoryName, treeId, e);
            throw new FileServiceException(e);
        }
        finally {
            if (null != fileInputStream) {
                fileInputStream.close();
            }
            if (null != orgFile && orgFile.exists()) {
                orgFile.delete();
            }
        }
    }

    /**
     * Runs a file remove and virtual service
     * @param fileID file ID
     * @param sessionName  the session name of a virtual service
     * @param virtualService a virtual service
     * @return result
     * @throws IOException
     * @since 1.0
     */
    public Object fileRemoveAndRunVirtualService(String repository, String category, String orgName, String fileID, String sessionName, JSONObject virtualService, HttpServletRequest request) throws IOException {
        OutputFormat outputFormat = OutputFormatKeeper.getOutputFormat();
        OutputFormat.MessageTemplate mt = outputFormat.setMessageTemplatesIfExist(virtualService.optString(QueryConstant.ReturnMessageTemplateKey, null),
                virtualService.optString(QueryConstant.ErrorMessageTemplateKey, null));
        File orgFile = null;
        try {
            orgFile = new File(FileServiceUtil.getInstance().getRepositoryDirPath(repository) +  "\\" + category + "\\" + orgName); // For Delete

            Map<String, Object> parameter = new HashMap<>();
            parameter.put("id", fileID);
            Object result = SqlTransactionDAOUtil.runVirtualQuery(sessionName, virtualService, parameter);

            outputFormat.setMessageTemplates(mt);
            return result;
        } catch (Exception e) {
            logger.error("{}, {}: repositoryName={}, filename={}", RequestIdentifierKeeper.getRequestIdLogString(), fileID, e);
            throw new FileServiceException(e);
        } finally {
            if (null != orgFile && orgFile.exists()) {
                orgFile.delete();
            }
        }
    }

    /**
     * Uploads a file by a virtual service and multipart form data
     *
     * @param repositoryName a repository name
     * @param sessionName a session name
     * @param serviceName a service name
     * @param format      output format, json or xml
     * @param multiPart   multipart form data
     * @param request     HttpServletRequest
     * @param response    HttpServletResponse
     * @return the path of a file that is saved
     * @since 1.0
     */
    public Response fileUploadAndRunVirtualService(String repositoryName, String sessionName, String serviceName, String format, FormDataMultiPart multiPart, HttpServletRequest request, HttpServletResponse response, String callerName) {
        long timeS = System.currentTimeMillis();
        try {
            OutputFormatKeeper.setOutputFormat(new OutputFormat(format, response, request));
            initializeRequest(request, response);

            JSONObject virtualService = FileServiceSessionManager.getInstance().getVirtualService(sessionName, serviceName);
            if (null != virtualService) {
                return buildResponse(fileUploadAndRunVirtualService(repositoryName, sessionName, virtualService, multiPart));
            } else {
                throw new FileServiceException(sessionName + "." + serviceName + " not found");
            }
        } catch (Throwable e) {
            logger.error("{}, {}", RequestIdentifierKeeper.getRequestIdLogString(), callerName, e);
            throw new FileServiceException(e);
        } finally {
            PerformanceLoggerUtil.logger.log("{}: sessionName={}, serviceName={}, RequestResponseTime={}", callerName, sessionName, serviceName, (System.currentTimeMillis() - timeS));
        }
    }

    /**
     * Update a file by a virtual service and multipart form data
     *
     * @param fileID a file key
     * @param repositoryName a repository name
     * @param sessionName a session name
     * @param serviceName a service name
     * @param format      output format, json or xml
     * @param multiPart   multipart form data
     * @param request     HttpServletRequest
     * @param response    HttpServletResponse
     * @return the path of a file that is saved
     * @since 1.0
     */
    public Response fileUpdateAndRunVirtualService(String repositoryName, String fileID, String sessionName, String serviceName, String format, FormDataMultiPart multiPart, HttpServletRequest request, HttpServletResponse response, String callerName) {
        long timeS = System.currentTimeMillis();
        try {
            OutputFormatKeeper.setOutputFormat(new OutputFormat(format, response, request));
            initializeRequest(request, response);

            JSONObject virtualService = FileServiceSessionManager.getInstance().getVirtualService(sessionName, serviceName);
            if (null != virtualService) {
                return buildResponse(fileUpdateAndRunVirtualService(repositoryName, fileID, sessionName, virtualService, multiPart));
            } else {
                throw new FileServiceException(sessionName + "." + serviceName + " not found");
            }
        } catch (Throwable e) {
            logger.error("{}, {}", RequestIdentifierKeeper.getRequestIdLogString(), callerName, e);
            throw new FileServiceException(e);
        } finally {
            PerformanceLoggerUtil.logger.log("{}: sessionName={}, serviceName={}, RequestResponseTime={}", callerName, sessionName, serviceName, (System.currentTimeMillis() - timeS));
        }
    }

    /**
     * Update a file by a virtual service and multipart form data
     *
     * @param fileID a file key
     * @param sessionName a session name
     * @param serviceName a service name
     * @param format      output format, json or xml
     * @param request     HttpServletRequest
     * @param response    HttpServletResponse
     * @return the path of a file that is saved
     * @since 1.0
     */
    public Response fileRemoveAndRunVirtualService(String repository, String category, String orgName, String fileID, String sessionName, String serviceName, String format, HttpServletRequest request, HttpServletResponse response, String callerName) {
        long timeS = System.currentTimeMillis();
        try {
            OutputFormatKeeper.setOutputFormat(new OutputFormat(format, response, request));
            initializeRequest(request, response);
            JSONObject virtualService = FileServiceSessionManager.getInstance().getVirtualService(sessionName, serviceName);
            if (null != virtualService) {
                return buildResponse(fileRemoveAndRunVirtualService(repository, category, orgName, fileID, sessionName, virtualService, request));
            } else {
                throw new FileServiceException(sessionName + "." + serviceName + " not found");
            }
        } catch (Throwable e) {
            logger.error("{}, {}", RequestIdentifierKeeper.getRequestIdLogString(), callerName, e);
            throw new FileServiceException(e);
        } finally {
            PerformanceLoggerUtil.logger.log("{}: sessionName={}, serviceName={}, RequestResponseTime={}", callerName, sessionName, serviceName, (System.currentTimeMillis() - timeS));
        }
    }

    /**
     * Gets a file
     *
     * @param repository repository
     * @param category category
     * @param orgName orgName
     * @param displayName displayName
     * @param isAttachment true if a file is an attachment
     * @param request      HttpServletRequest
     * @param response     HttpServletResponse
     * @return the contents of a file
     */
    protected Response downloadNetSubFile(String repository, String category, String orgName, String displayName, boolean isAttachment, HttpServletRequest request, HttpServletResponse response, String callerName) {
        long timeS = System.currentTimeMillis();
        try {
            initializeRequest(request, response);
            if (logger.isDebugEnabled()) {
                logger.debug("{}, {}: repository={}, category={}, orgName={}, displayName={}", RequestIdentifierKeeper.getRequestIdLogString(), callerName, repository, category, orgName, displayName);
            }

            File file = new File(FileServiceUtil.getInstance().getRepositoryDirPath(repository) +  "\\" + category + "\\" + orgName);

            if (file.exists()) {
                if (null == displayName) {
                    displayName = file.getName();
                }
                return buildResponseForDownload(file, displayName, null, true, request);
            } else {
                throw new FileNotAvailableException(file.getAbsolutePath());
            }
        } catch (Throwable e) {
            logger.error("{}, {}: repository={}, category={}, orgName={}, displayName={}, {}", RequestIdentifierKeeper.getRequestIdLogString(), callerName, repository, category, orgName, displayName, e);
            throw new FileServiceException(e);
        } finally {
            PerformanceLoggerUtil.logger.log("{}: repository={}, category={}, orgName={}, displayName={}, RequestResponseTime={}", callerName, repository, category, orgName, displayName, (System.currentTimeMillis() - timeS));
        }
    }

    protected Response buildResponseForDownload(File file, String filename, String contentType, boolean attachment, HttpServletRequest request) {
        Response.ResponseBuilder responseBuilder;

        responseBuilder = Response.ok(file);
        responseBuilder.type("application/octet-stream");
        responseBuilder.header("Content-Disposition", "attachment; ");
        responseBuilder.header("Content-Length", file.length());
        return responseBuilder.build();
    }

    /**
     * Upload Private File From NetSub and Send Email To TriWest
     *
     * @param multiPart   multipart form data
     * @param request     HttpServletRequest
     * @param response    HttpServletResponse
     * @return the path of a file that is saved
     * @since 1.0
     */
    public Response sendEmailWithAttachment(String repository, FormDataMultiPart multiPart, HttpServletRequest request, HttpServletResponse response, String callerName) {
        long timeS = System.currentTimeMillis();
        try {
            OutputFormatKeeper.setOutputFormat(new OutputFormat("json", response, request));
            initializeRequest(request, response);

            return buildResponse(sendEmailWithAttachment(repository, multiPart));
        } catch (Throwable e) {
            logger.error("{}, {}", RequestIdentifierKeeper.getRequestIdLogString(), callerName, e);
            throw new FileServiceException(e);
        } finally {
            PerformanceLoggerUtil.logger.log("{}: RequestResponseTime={}", callerName, (System.currentTimeMillis() - timeS));
        }
    }

    /**
     * Upload Private File From NetSub and Send Email To TriWest
     * @param multiPart  multipart form data
     * @return result
     * @throws IOException
     */
    public Object sendEmailWithAttachment(String repository, FormDataMultiPart multiPart) throws IOException {

        FormDataBodyPart filePart = multiPart.getField(FileParam);
        File file = filePart.getValueAs(File.class);
        FileInputStream fileInputStream = null;
        String filename = null;
        String systemFileName = UUIDGenerator.newUUID();
        String repositoryDirPath = FileServiceConfigUtil.getConfiguration().getProperty("Repository." + repository);
        File baseRepositoryDir = FileServiceUtil.getInstance().getRepositoryDir(repository);

        File repositoryDir = getFileRepositoryDir(repository, baseRepositoryDir, multiPart);

        String name = FormDataMultiPartUtil.getStringValue(multiPart, "Name");
        String category = FormDataMultiPartUtil.getStringValue(multiPart, "NetSubCategory");
        String netSubID = FormDataMultiPartUtil.getStringValue(multiPart, "NetSubID");
        String description = FormDataMultiPartUtil.getStringValue(multiPart, "Description");
        String userInfoJson = FormDataMultiPartUtil.getStringValue(multiPart, "UserInfo");

        JSONObject userInfo = JSONUtil.stringToJsonObject(userInfoJson, false);
        JSONObject result = new JSONObject();

        try {
            if (null != repositoryDir && repositoryDir.exists()) {
                filename = getFileName(filePart, FormDataMultiPartUtil.getStringValue(multiPart, EncodedFileNameParam));
                File oldFile = getOldFile(multiPart);
                JSONObject group = userInfo.getJSONObject("group");

                String folderName = "sendfile/" + (group != null ? group.getString("name") : "undefined");
                if (StringUtil.isNotNullBlank(filename)) {
                    File targetDir = new File(repositoryDir, folderName);
                    targetDir.mkdirs();
                    file = new File(targetDir.getAbsolutePath() + "/" + name);
                    InputStream fileInputStream2 = filePart.getValueAs(InputStream.class);
                    try {
                        file.delete();
                        FileIOUtil.saveFile(fileInputStream2, file);
                    } finally {
                        fileInputStream2.close();
                    }
                }
                String savedFileName = StringUtil.isNotNullBlank(filename) ? filename : FormDataMultiPartUtil.getStringValue(multiPart, FileServiceConstant.OldNameParam);
                File savedFile = null != file ? file : oldFile;

                sendMail(userInfo, name, description, savedFile);
                result.put("result", true);
            } else {
                throw new IOException();
            }
        } catch (Exception e) {
            result.put("result", false);
            file.delete();
            logger.error("{}, {}: repositoryName={}, filename={}", RequestIdentifierKeeper.getRequestIdLogString(), repository, category, netSubID, repositoryDir, filename, e);
            throw new FileServiceException(e);
        }
        if (null != fileInputStream) {
            fileInputStream.close();
        }
        return result;
    }

    private static void sendMail(JSONObject userInfo, String orgName, String description,  File attachment) throws Exception {
        JSONObject result = new JSONObject();
        Message message = null;

            String sender = MessageServiceConfigUtil.getProperty("mail.netsub.template.report.sender");
            String recipientsProperty = MessageServiceConfigUtil.getProperty("mail.netsub.template.report.recipients");
            String ccRecipientsProperty = MessageServiceConfigUtil.getProperty("mail.netsub.template.report.ccRecipients");
            String subject = MessageServiceConfigUtil.getProperty("mail.netsub.template.report.subject");
            String contextType = "text/html; charset=UTF-8";
            String mailContents = MessageServiceConfigUtil.getProperty("mail.netsub.template.report.contents");

            if (!StringUtil.isNotNullBlank(sender)) {
                result.put("result", false);
                result.put("faultMessage", "There is no sender email address");
                throw new MessageServiceException("There is no sender email address");
            }
            InternetAddress[] toRecipients = null;
            JSONArray recipientArray = new JSONArray();

            if (recipientsProperty != null) {
                StringTokenizer st = new StringTokenizer(recipientsProperty, ";");
                while(st.hasMoreTokens()) {
                    recipientArray.put(st.nextToken());
                }
            }
            toRecipients = MessageServiceUtil.getInternetAddresses(recipientArray);
            InternetAddress[] ccRecipients = null;
            JSONArray ccRecipientArray = new JSONArray();

            if (ccRecipientsProperty != null) {
                StringTokenizer st = new StringTokenizer(ccRecipientsProperty, ";");
                while(st.hasMoreElements()) {
                    ccRecipientArray.put(st.nextToken().trim());
                }
            }
            ccRecipients = MessageServiceUtil.getInternetAddresses(ccRecipientArray);
            if (null == toRecipients && null == ccRecipients) {
                throw new MessageServiceException("There is no mail recipients");
            }

            InternetAddress[] bccRecipients = null;

            InternetAddress[] replyTos = null;
            InternetAddress senderIA = new InternetAddress(sender);
            List<AttachmentPart> attachmentPartList = null;
            JSONObject group = userInfo.getJSONObject("group");

            subject = subject.replace("$$netsubName$$", group != null ? group.getString("name") : "");

            String contents = ExpressionEvaluator.evaluateExpressions(mailContents);
            contents = contents.replace("$$netsubName$$", group != null ? group.getString("name") : "");
            contents = contents.replace("$$fileName$$", orgName);
            contents = contents.replace("$$senderUserName$$", userInfo.getString("name"));
            contents = contents.replace("$$sendDate$$", getToday());
            contents = contents.replace("$$fileDescription$$", description);
            contents = contents.replace("$$sharedDrivePath$$", attachment.getPath());

            MessagePart[] messageParts = new MessagePart[]{new MessagePart("body", contextType, contents)};

            JSONArray jArr = new JSONArray();
            JSONObject attachReferralJson = new JSONObject();
            attachReferralJson.put("name", orgName);
            attachReferralJson.put("path", attachment.getPath());

            jArr.put(attachReferralJson);

            if (null == attachmentPartList) {
                attachmentPartList = new ArrayList();
            }

            for(int i = 0; i < jArr.length(); ++i) {
                JSONObject json = jArr.getJSONObject(i);
                String path = FileServiceUtil.instance.getRepositoryFileFullPath(json.getString("path"));
                ((List)attachmentPartList).add(new AttachmentPart(json.getString("name"), path));
            }

            AttachmentPart[] attachmentParts = null;
            if (null != attachmentPartList) {
                attachmentParts = (AttachmentPart[])((List)attachmentPartList).toArray(new AttachmentPart[((List)attachmentPartList).size()]);
            }
            MessageSender.getInstance().sendMail(senderIA, toRecipients, subject, messageParts, attachmentParts, replyTos, ccRecipients, (InternetAddress[])bccRecipients);
    }

    public static String getToday() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleformat = new SimpleDateFormat("MM/dd/yyyy");
        return simpleformat.format(cal.getTime());
    }

    private static JSONArray removeCharInJSONArray(JSONArray inArray) {
        if (inArray != null && inArray.length() >= 1) {
            for(int i = 0; i < inArray.length(); ++i) {
                Object item = inArray.get(i);
                if (item instanceof String) {
                    String itemStr = (String)item;
                    if (itemStr.indexOf(",") > -1) {
                        itemStr = itemStr.replaceAll(",", " ");
                        inArray.put(i, itemStr);
                    }
                }
            }

            return inArray;
        } else {
            return inArray;
        }
    }

    protected Response buildResponseForSendEmail(File file, String filename, String contentType, boolean attachment, HttpServletRequest request) {
        Response.ResponseBuilder responseBuilder;

        responseBuilder = Response.ok(file);
        responseBuilder.type("application/octet-stream");
        responseBuilder.header("Content-Disposition", "attachment; ");
        responseBuilder.header("Content-Length", file.length());
        return responseBuilder.build();
    }

    /**
     * Gets the old file
     *
     * @param multiPart a multipart data
     * @return the old file if exists,
     */
    private File getOldFile(FormDataMultiPart multiPart) {
        String oldPath = FormDataMultiPartUtil.getStringValue(multiPart, FileServiceConstant.OldPathParam);
        File oldFile = null;
        if (null != oldPath) {
            oldFile = new File(FileServiceUtil.getInstance().getRepositoryFileFullPath(oldPath));
        }

        return oldFile;
    }

    /**
     * Gets the file name of a file uploaded
     *
     * @param filePart        form data file part
     * @param encodedFileName encoded file name
     * @return the name of a file uploaded
     * @throws UnsupportedEncodingException
     */
    private String getFileName(FormDataBodyPart filePart, String encodedFileName) throws UnsupportedEncodingException {
        String filename;
        if (null == encodedFileName) {
            ContentDisposition headerOfFilePart = filePart.getContentDisposition();
            filename = new String(FileUtil.getFilename(headerOfFilePart.getFileName()).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        } else {
            filename = ServletUtil.decodeUrl(encodedFileName);
        }

        return filename;
    }

    /**
     * Resolves a repository path with keys and values
     * The keys are set to the parameter resolveKeys and the keys use @@ as the delimiter of each key. for example, key1@@key2@@key3.
     * The values  are set to the parameter resolveValues and the values use @@ as the delimiter of each value. for example, value1@@value2@@value3
     *
     * @param repositoryPath a repository path
     * @param multiPart      a multipart data
     * @return a resolved repository path
     */
    private String resolveRepositoryPath(String repositoryPath, FormDataMultiPart multiPart) {
        String resolveKeys = FormDataMultiPartUtil.getStringValue(multiPart, FileServiceConstant.ResolveKeysParam, null);
        if (null != resolveKeys) {
            String resolveValues = FormDataMultiPartUtil.getStringValue(multiPart, FileServiceConstant.ResolveValuesParam, null);
            String[] keys = resolveKeys.split(FileServiceConstant.ResolveKeyValueDelimiter);
            String[] values = resolveValues.split(FileServiceConstant.ResolveKeyValueDelimiter);
            if (keys.length == values.length) {
                repositoryPath = StringReplacer.replace(repositoryPath, keys, values, true, false);
            } else {
                throw new FileServiceException("The number of " + FileServiceConstant.ResolveKeysParam + "and " + FileServiceConstant.ResolveValuesParam + "does not match");
            }
        }

        return repositoryPath;
    }

    /**
     * Gets the directory where a file is stored.
     *
     * @param repositoryName repository name
     * @param repositoryDir  repository directory
     * @param multiPart      multipart form data
     * @return the directory
     * @since 3.0
     */
    private File getFileRepositoryDir(String repositoryName, File repositoryDir, FormDataMultiPart multiPart) {
        String subRepositoryName = FormDataMultiPartUtil.getStringValue(multiPart, FileServiceConstant.SubRepositoryNameParam);
        String repositoryDirPath = null != subRepositoryName ? FileServiceUtil.getInstance().getRepositoryDirPath(repositoryName + "." + subRepositoryName) : repositoryDir.getAbsolutePath();
        return (null != repositoryDirPath) ? new File(resolveRepositoryPath(repositoryDirPath, multiPart)) : null;
    }
}
