package com.bizflow.io.services.training;

import com.bizflow.io.core.db.mybatis.model.QueryActionConstant;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.custom.services.bizcloud.util.CommonUtil;
import com.bizflow.io.services.data.dao.util.SqlDAOUtil;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class TrainingService {
    private TrainingService() {
        throw new IllegalStateException("TrainingService class");
    }

    private static final Logger logger = LogManager.getLogger(TrainingService.class);

    public static Object insertTrainingMaterial(String district, String sessionName, HashMap parameter) {
        logger.debug("TrainingService.insertTrainingMaterial is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        HashMap dataMap = new HashMap();
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            int parentId = -1;
            int depth = -1;

            if(jsonParam.has("data") ) {
                Object dataObj = jsonParam.get("data");
                JSONObject dataJson = null;
                if (dataObj instanceof JSONObject) {
                    dataJson = (JSONObject) dataObj;
                } else if (dataObj instanceof JSONArray) {
                    JSONArray dataArr = (JSONArray) dataObj;
                    dataJson = dataArr.getJSONObject(0);
                }
                if (dataJson == null || !dataJson.has("PARENTID")) {
                    throw new Exception("Cannot find the parameter 'PARENTID' or have the null value.");
                }
                if (dataJson == null || !dataJson.has("TITLE")) {
                    throw new Exception("Cannot find the parameter 'TITLE' or have the null value.");
                }
                if (dataJson == null || !dataJson.has("CRTUSERID")) {
                    throw new Exception("Cannot find the parameter 'CRTUSERID' or have the null value.");
                }
                parentId = dataJson.getInt("PARENTID");
                dataMap = (HashMap) dataJson.toMap();
            }
            String queryName = "material.insertMaterial";
            String queryAction =  QueryActionConstant.InsertQuery;
            Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, dataMap);

            if(dataMap.containsKey("DEPTH")){
                Object depthObj = dataMap.get("DEPTH");
                if(depthObj instanceof BigDecimal){
                    depth = ((BigDecimal)dataMap.get("DEPTH")).intValue();
                } else if(depthObj instanceof Integer){
                    depth = ((Integer)dataMap.get("DEPTH")).intValue();
                }
            }
            if(depth == 4){
                resultObject = updateCourseEST(dataMap);
                logger.debug("updateCourseEST: {}", resultObject);
            }

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "insertTrainingMaterial", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static Object updateCourseEST(HashMap hmap){
        Object resultObject = null;
        String queryName = "material.updateCourseSumOfChildEST";
        String queryAction =  QueryActionConstant.UpdateQuery;
        resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, hmap);

        return resultObject;
    }

    public static Object deleteTrainingMaterial(String district, String sessionName, HashMap parameter) {
        logger.debug("TrainingService.deleteTrainingMaterial is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has("data") ){
                Object dataObj = jsonParam.get("data");
                JSONObject dataJson = null;
                if(dataObj instanceof JSONObject) {
                    dataJson = (JSONObject) dataObj;
                } else if(dataObj instanceof JSONArray) {
                    JSONArray dataArr = (JSONArray)dataObj;
                    dataJson = dataArr.getJSONObject(0);
                }
                if(dataJson == null || !dataJson.has("TNODEID")){
                    throw new Exception("Cannot find the parameter 'TNODEID'");
                }
                jsonParam.put("TNODEID", CommonUtil.getNumberFromJsonParam(dataJson, "TNODEID"));
                parameter = (HashMap)jsonParam.toMap();
            }

            if(!jsonParam.has("TNODEID")){
                throw new Exception("Need parameter TNODEID!");
            }
            JSONObject theMaterial = null;
            theMaterial = getMaterialInfoByTNodeID(parameter);
            if(theMaterial == null){
                throw new Exception("Cannot find the material information by TNODEID:"+jsonParam.getInt("TNODEID"));
            }

            int depth = -1;
            int childCount = -1;

            if(theMaterial.has("DEPTH")){
                depth = theMaterial.getInt("DEPTH");
            }
            if(theMaterial.has("CHILDCOUNT")){
                childCount = theMaterial.getInt("CHILDCOUNT");
            }
            if(childCount > 0) {
                throw new Exception("Cannot Delete the Material because the children of the Material exist");
            }
            String queryName = "material.deleteMaterial";
            String queryAction =  QueryActionConstant.DeleteQuery;
            Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, parameter);

            if(depth == 4){
                resultObject = updateCourseEST((HashMap)JSONUtil.jsonStringToMap(theMaterial.toString()));
            }
            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("deleteTrainingMaterial job is done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "deleteTrainingMaterial", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static JSONObject getMaterialInfoByTNodeID(Map parameter){
        String queryName = "material.selectMaterialInfo";
        String queryAction =  QueryActionConstant.SelectQuery;
        Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, parameter);
        JSONArray matInfos = null;
        JSONObject theMaterial = null;

        if(resultObject == null){
            return null;
        }
        if(resultObject instanceof JSONArray){
            matInfos = (JSONArray)resultObject;
            if(matInfos.length() < 1){
                return null;
            }
            theMaterial = matInfos.getJSONObject(0);
        }else if(resultObject instanceof JSONObject){
            theMaterial = (JSONObject)resultObject;
        }

        return theMaterial;
    }

    public static Object updateTrainingMaterial(String district, String sessionName, HashMap parameter) {
        logger.debug("TrainingService.updateTrainingMaterial is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            boolean needUpdateCourseMIN = false;
            if(jsonParam.has("data")){
                if(jsonParam.get("data") instanceof JSONObject) {
                    jsonParam = jsonParam.getJSONObject("data");
                } else if(jsonParam.get("data") instanceof JSONArray) {
                    jsonParam = jsonParam.getJSONArray("data").getJSONObject(0);
                }
                parameter = (HashMap)jsonParam.toMap();
            }

            if(!jsonParam.has("TNODEID")){
                throw new Exception("Need parameter TNODEID!");
            }
            if(!jsonParam.has("UPDUSERID")){
                throw new Exception("Need parameter UPDUSERID!");
            }

            // if PARENTID ESTMIN USEFLAG is changed, the Estimated Minutes of the parent Course needs to be updated.
            if(jsonParam.has("PARENTID") || jsonParam.has("ESTMIN") || jsonParam.has("USEFLAG")){
                needUpdateCourseMIN = true;
            }

            int depth = -1;

            JSONObject theMaterial = null;
            theMaterial = getMaterialInfoByTNodeID(parameter);
            if(theMaterial == null){
                throw new Exception("Cannot find the material information by TNODEID:"+jsonParam.getInt("TNODEID"));
            }
            if(theMaterial.has("DEPTH")){
                depth = theMaterial.getInt("DEPTH");
            }

            String queryName = "material.updateMaterial";
            String queryAction =  QueryActionConstant.UpdateQuery;
            Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, parameter);

            if(depth == 4){
                resultObject = updateCourseEST((HashMap)JSONUtil.jsonStringToMap(theMaterial.toString()));
            }
            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("updateTrainingMaterial job is done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "updateTrainingMaterial", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object insertUserProgress(String district, String sessionName, HashMap parameter) {
        logger.debug("TrainingService.insertUserProgress is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        HashMap dataMap = new HashMap();
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            int parentId = -1;
            int depth = -1;
            String matType = null;
            String status = null;
            if(jsonParam.has("data") ) {
                Object dataObj = jsonParam.get("data");
                JSONObject dataJson = null;
                if (dataObj instanceof JSONObject) {
                    dataJson = (JSONObject) dataObj;
                } else if (dataObj instanceof JSONArray) {
                    JSONArray dataArr = (JSONArray) dataObj;
                    dataJson = dataArr.getJSONObject(0);
                }
                if (dataJson == null || !dataJson.has("TRAINEEID")) {
                    throw new Exception("Cannot find the parameter 'TRAINEEID' or have the null value.");
                }
                if (dataJson == null || !dataJson.has("TNODEID")) {
                    throw new Exception("Cannot find the parameter 'TNODEID' or have the null value.");
                }
                if (dataJson == null || !dataJson.has("STATUS")) {
                    throw new Exception("Cannot find the parameter 'STATUS' or have the null value.");
                }
                if (dataJson == null || !dataJson.has("MATTYPE")) {
                    throw new Exception("Cannot find the parameter 'MATTYPE' or have the null value.");
                }
                matType = dataJson.getString("MATTYPE");
                if (matType == null || (!matType.equals("C") && !matType.equals("L") && !matType.equals("S"))) {
                    throw new Exception("The parameter MATTYPE should have the value among 'C', 'L', or 'S'.");
                }
                status = dataJson.getString("STATUS");
//                parentId = dataJson.getInt("PARENTID");
                dataMap = (HashMap) dataJson.toMap();
            }
            String queryName = "progress.insertProgress";
            String queryAction =  QueryActionConstant.InsertQuery;
            Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, dataMap);

            int tNodeId = -1;
            tNodeId = jsonParam.getInt("TNODEID");

            if(dataMap.containsKey("DEPTH")){
                Object depthObj = dataMap.get("DEPTH");
                if(depthObj instanceof BigDecimal){
                    depth = ((BigDecimal)dataMap.get("DEPTH")).intValue();
                } else if(depthObj instanceof Integer){
                    depth = ((Integer)dataMap.get("DEPTH")).intValue();
                }
            }

            int estMin = -1;
            if(dataMap.containsKey("ESTMIN")){
                Object depthObj = dataMap.get("ESTMIN");
                if(depthObj instanceof BigDecimal){
                    estMin = ((BigDecimal)dataMap.get("ESTMIN")).intValue();
                } else if(depthObj instanceof Integer){
                    estMin = ((Integer)dataMap.get("ESTMIN")).intValue();
                }
            }



            if(depth == 4){
                resultObject = updateCourseEST(dataMap);
                logger.debug("updateCourseEST: %s", resultObject.toString());
            }

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("insertUserProgress job is done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "insertUserProgress", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object updateUserProgressForAll(String district, String sessionName, HashMap parameter) {
        logger.debug("TrainingService.updateUserProgressForAll is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        HashMap dataMap = new HashMap();
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            int parentId = -1;
            int depth = -1;
            String matType = null;
            String status = null;
            int childCount = -1;
            int estMin = -1;
            int finishExFlag = 0;
            Object resultObject = null;

            if(jsonParam.has("data") ) {
                Object dataObj = jsonParam.get("data");
                JSONObject dataJson = null;
                if (dataObj instanceof JSONObject) {
                    dataJson = (JSONObject) dataObj;
                } else if (dataObj instanceof JSONArray) {
                    JSONArray dataArr = (JSONArray) dataObj;
                    dataJson = dataArr.getJSONObject(0);
                }
                if (dataJson == null) {
                    throw new Exception("Cannot find the JSON Parameters.");
                }
                if (!dataJson.has("TRAINEEID")) {
                    throw new Exception("Cannot find the parameter 'TRAINEEID' or have the null value.");
                }
                if (!dataJson.has("TNODEID")) {
                    throw new Exception("Cannot find the parameter 'TNODEID' or have the null value.");
                }
                if (!dataJson.has("MATTYPE")) {
                    throw new Exception("Cannot find the parameter 'MATTYPE' or have the null value.");
                }
                matType = dataJson.getString("MATTYPE");
                if (matType == null || (!matType.equals("C") && !matType.equals("L") && !matType.equals("E"))) {
                    throw new Exception("The parameter MATTYPE should have the value among 'C', 'L', or 'E'.");
                }
                if (dataJson.has("STATUS")){
                    status = dataJson.getString("STATUS");
                }
                if (dataJson.has("CHILDCOUNT")){
                    childCount = dataJson.getInt("CHILDCOUNT");
                }
                if (dataJson.has("ESTMIN")){
                    estMin = dataJson.getInt("ESTMIN");
                }
                if (dataJson.has("FINISHEXERCISE_FLAG")){
                    finishExFlag = dataJson.getInt("FINISHEXERCISE_FLAG");
                }
                if (dataJson.has("PARENTID")){
                    parentId = dataJson.getInt("PARENTID");
                }
                dataMap = (HashMap) dataJson.toMap();
            }
            if(matType.equals("C") && status != null && status.equals("C")){
                throw new Exception("There cannot be the case that Course Node would be completed('C') directly by the user'");
            }
            if(matType.equals("C")){                // For Course Node
                dataMap.put("STATUS", "R");
                dataMap.put("UPDDATETIME", "1");
                Object obj = updateProgressAny(dataMap);
            } else if (matType.equals("L")){        // For Lesson Node
                if(childCount < 0){
                    throw new Exception("The parameter CHILDCOUNT should exist if the selected node is Lesson case.");
                }
                if(estMin < 0){
                    throw new Exception("The parameter ESTMIN should exist if the selected node is Lesson case.");
                }
                if(childCount > 0){
                    dataMap.put("FINESTMIN", estMin/2);
                    dataMap.put("STATUS", "R");
                    dataMap.put("UPDDATETIME", "1");
                } else {    // childCount = 0
                    dataMap.put("FINESTMIN", estMin);
                    dataMap.put("STATUS", "C");
                    dataMap.put("COMPDATETIME", "1");
                }
                Object obj = updateProgressAny(dataMap);

            } else if (matType.equals("E")){        // For Exercise Node        to update only lesson node
                if(finishExFlag > 0){
                    dataMap.put("STATUS", "C");
                    dataMap.put("COMPDATETIME", "1");
                    // dataMap.put("FINESTMIN", estMin);   // estMin of the Lesson
                    dataMap.put("TNODEID", parentId);
                    Object obj = updateProgressAny(dataMap);
                }
            }
            Object obj = calibrateLessonEstMinIfCompleted(dataMap);
            obj = updateCourseEstMin(dataMap);
            obj = updateCourseStatusAsCompleted(dataMap);

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("updateUserProgressForAll job is done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "updateUserProgressForAll", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static Object updateProgressAny(HashMap hashMap){
        String queryName = "progress.updateProgressAny";
        String queryAction =  QueryActionConstant.UpdateQuery;
        Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, hashMap);

        return resultObject;
    }

    private static Object calibrateLessonEstMinIfCompleted(HashMap hashMap){
        String queryName = "progress.calibrateLessonEstMinIfCompleted";
        String queryAction =  QueryActionConstant.UpdateQuery;
        Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, hashMap);

        return resultObject;
    }

    private static Object updateCourseEstMin(HashMap hashMap){
        String queryName = "progress.updateCourseEstMin";
        String queryAction =  QueryActionConstant.UpdateQuery;
        Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, hashMap);

        return resultObject;
    }

    private static Object updateCourseStatusAsCompleted(HashMap hashMap){
        String queryName = "progress.updateCourseStatusAsCompleted";
        String queryAction =  QueryActionConstant.UpdateQuery;
        Object resultObject = SqlDAOUtil.runSql("training", queryName, queryAction, hashMap);

        return resultObject;
    }

}
