package com.bizflow.io.custom.services.portal.util;

import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Map;

public class PortalFormFlowUtil {
    private PortalFormFlowUtil() {
        throw new IllegalStateException("PortalFormFlowUtil class");
    }

    private static final Logger logger = LogManager.getLogger(PortalFormFlowUtil.class);
    private static final String SRC_JSON_FORM               = "form";
    private static final String SRC_JSON_FORMFLOW           = "formFlow";
    private static final String SRC_JSON_TOFORMID           = "toFormId";
    private static final String SRC_JSON_COMPONENT_LABEL    = "componentLabel";
    private static final String SRC_PROJECTID               = "projectId";
    private static final String SRC_FORMID                  = "formId";
    private static final String SRC_ISFI                    = "isfi";
    private static final String SRC_NAME                    = "name";
    private static final String SRC_ID                      = "id";
    private static final String TARGET_NODES                = "nodes";
    private static final String TARGET_EDGES                = "edges";
    private static final String TARGET_ID                   = "id";
    private static final String TARGET_LABEL                = "label";
    private static final String TARGET_FROM                 = "from";
    private static final String TARGET_TO                   = "to";
    private static final String TARGET_ISFI                 = "isfi";


    /**
     * Sends a mail with project adx file
     *
     * Incoming Parameter: parameter
     * {
     *     projectId: aa145276d922449e0b059df1f48663e7f,
     *     formId: rd3408ae551e040ca82461dc7387ec8e8,
     *     isfi: YWExNDUyNzZkOTIyNDQ5ZTBiMDU5ZGYxZjQ4NjYzZTdmfGsyMDc1MTQzY2JmNDg0MTdlYTA5MzQzZTcyMjgxYzg3ZnxmYTliNzEzMTZmM2YyNDNiNDliNWVkODlkYjBmYTYzN2Z8bGlnaHQ=
     * }
     *
     *
     * Outgoing Parameter: Response
     * {
     *     nodes:[{
     *         id: xxxxx,
     *         label: yyyyy,
     *         isfi: xyzabcdefg
     *     },
     *     {
     *         id: xxxxx,
     *         label: yyyyy,
     *         isfi: xyzabcdefg
     *     }],
     *     edges:[{
     *         from: aaaaaaa,
     *         to: bbbbbb,
     *         label: asdfsdaf
     *     },
     *     {
     *         from: aaaaaaa,
     *         to: bbbbbb,
     *         label: asdfsdaf
     *     }]
     * }
     * @return Response
     */
    public static Object getAllLinkedForms(String district, String sessionName, HashMap parameter){
        logger.debug("PortalFormFlowUtil.getAllLinkedForms is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            JSONObject formsInfoJson = null;
            JSONObject formListJson = null;
            JSONObject formFlowListJson = null;

            JSONObject out_linkedFormsJson = new JSONObject();
            JSONArray out_nodesJArr = new JSONArray();
            JSONArray out_edgesJArr = new JSONArray();

            out_linkedFormsJson.put(TARGET_NODES,out_nodesJArr);
            out_linkedFormsJson.put(TARGET_EDGES,out_edgesJArr);

            String projectId = jsonParam.optString(SRC_PROJECTID);
            String formId = jsonParam.optString(SRC_FORMID);
            String isfi = jsonParam.optString(SRC_ISFI);

            Map<String, Object> map = new HashMap<>();
//            map.put(SRC_PROJECTID, projectId);
//            map.put(SRC_FORMID, formId);
            map.put(SRC_ISFI, isfi);

            formsInfoJson = PortalUtil.getSingleJSONObjectFromFileService("fbs","formFlow-GetPublicationFormFlowList@", map);
            if(formsInfoJson == null || !formsInfoJson.has(SRC_JSON_FORM)) {
                throw new Exception("Failed to get formFlow data: Form.");
            }
            formListJson = formsInfoJson.getJSONObject(SRC_JSON_FORM);

            if (!formsInfoJson.has(SRC_JSON_FORMFLOW) || formsInfoJson.getJSONObject(SRC_JSON_FORMFLOW).length() < 1){
                if(formListJson.has(formId)){
                    JSONObject srcForm = formListJson.getJSONObject(formId);
                    JSONObject outForm = new JSONObject();
                    outForm.put(TARGET_ID, formId);
                    outForm.put(TARGET_LABEL, srcForm.getString(SRC_NAME));
                    outForm.put(TARGET_ISFI, isfi);
                    out_nodesJArr.put(outForm);
                    out_linkedFormsJson.put("result","true");

                    return out_linkedFormsJson;
                } else {
                    throw new Exception("Failed to get form data: formId=" + formId);
                }
            }
            formFlowListJson = formsInfoJson.getJSONObject(SRC_JSON_FORMFLOW);

            JSONArray temp_nodesArr = new JSONArray();
            JSONArray temp_edgesArr = new JSONArray();

            JSONArray formFlowNames = formFlowListJson.names();
            if(formFlowNames == null || formFlowNames.length() < 1){
                throw new Exception("Failed to get formFlow data");
            }
            for(int i = 0 ; i < formFlowNames.length(); i++){
                String formName = formFlowNames.getString(i); // formid

                JSONObject theFormFromList = formListJson.getJSONObject(formName);
                JSONObject formNameFromKey = createNodeJsonObject(theFormFromList);

                if(!hasJSONObject(temp_nodesArr,formNameFromKey)) {
                    temp_nodesArr.put(formNameFromKey);
                }

                JSONArray flowValueArray = formFlowListJson.getJSONArray(formName);
                for(int j = 0 ; j < flowValueArray.length(); j++){
                    JSONObject toFlowForm = flowValueArray.getJSONObject(j);
                    JSONObject theForm = formListJson.getJSONObject(toFlowForm.getString(SRC_JSON_TOFORMID)); // "toFormId"

                    JSONObject outForm = createNodeJsonObject(theForm);
                    if(!hasJSONObject(temp_nodesArr,outForm)) {
                        temp_nodesArr.put(outForm);
                    }

                    JSONObject outToForm = new JSONObject();
                    outToForm.put(TARGET_FROM, formName);
                    outToForm.put(TARGET_TO, toFlowForm.getString(SRC_JSON_TOFORMID));
                    outToForm.put(TARGET_LABEL, toFlowForm.getString(SRC_JSON_COMPONENT_LABEL));

                    temp_edgesArr.put(outToForm);
                }
            }

            getEdgesArray(temp_edgesArr, out_edgesJArr, TARGET_FROM, formId);
            logger.debug("Successful for getting the edges array from temp_edgesArr.");

            if(out_edgesJArr.length() < 1){
                JSONObject oneFormJson = getJSONObjectByFormId(temp_nodesArr, TARGET_ID, formId);
                if(oneFormJson == null) {
                    JSONObject srcForm = formListJson.getJSONObject(formId);
                    JSONObject outForm = new JSONObject();
                    outForm.put(TARGET_ID, formId);
                    outForm.put(TARGET_LABEL, srcForm.getString(SRC_NAME));
                    outForm.put(TARGET_ISFI, isfi);
                    out_nodesJArr.put(outForm);
                    out_linkedFormsJson.put("result","true");
                    return out_linkedFormsJson;
                }

                if (!hasJSONObject(out_nodesJArr, oneFormJson)) {
                    out_nodesJArr.put(oneFormJson);
                }

            } else {
                for (int i = 0; i < out_edgesJArr.length(); i++) {
                    JSONObject jobj = out_edgesJArr.getJSONObject(i);

                    String fromId = jobj.getString(TARGET_FROM);
                    String toId = jobj.getString(TARGET_TO);

                    JSONObject fromFormJson = getJSONObjectByFormId(temp_nodesArr, TARGET_ID, fromId);
                    if (!hasJSONObject(out_nodesJArr, fromFormJson)) {
                        out_nodesJArr.put(fromFormJson);
                    }

                    JSONObject toFormJson = getJSONObjectByFormId(temp_nodesArr, TARGET_ID, toId);
                    if (!hasJSONObject(out_nodesJArr, toFormJson)) {
                        out_nodesJArr.put(toFormJson);
                    }
                }
            }
            String resultJsonString = out_linkedFormsJson.toString();
            logger.debug("All jobs are done!");
            logger.debug("-------------------------------------------------------------------------------------------");
            logger.debug(resultJsonString);
            logger.debug("-------------------------------------------------------------------------------------------");
            out_linkedFormsJson.put("result","true");

            return out_linkedFormsJson;
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
            return result;
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "getAllLinkedForms", (System.currentTimeMillis() - timeS));
        }
    }


    private static JSONArray getEdgesArray(JSONArray srcArray, JSONArray targetArray, String searchName, String searchValue){
        JSONArray theArray = getJSONArrayByFormId(srcArray, searchName, searchValue);

        for(int i = 0; i < theArray.length(); i++ ){
            JSONObject jobj = theArray.getJSONObject(i);
            // prevent targetArray from getting duplicated edge object.
            if(hasJSONObject(targetArray,jobj)){
                continue;
            }
            targetArray.put(jobj);
            getEdgesArray(srcArray, targetArray, searchName, jobj.getString(TARGET_TO));
        }

        return targetArray;
    }

    private static JSONArray getJSONArrayByFormId(JSONArray srcArray, String searchName, String searchValue){
        JSONArray theArray = new JSONArray();
        for(int i = 0; i < srcArray.length(); i++ ){
            JSONObject jobj = srcArray.getJSONObject(i);
            if(searchValue.equals(jobj.getString(searchName))){
                theArray.put(jobj);
            }
        }

        return theArray;
    }

    private static JSONObject getJSONObjectByFormId(JSONArray srcArray, String searchName, String searchValue){
        for(int i = 0; i < srcArray.length(); i++ ){
            JSONObject jobj = srcArray.getJSONObject(i);
            if(searchValue.equals(jobj.getString(searchName))){
                return jobj;
            }
        }

        return null;
    }

    private static boolean hasJSONObject(JSONArray srcArray, JSONObject jobj){
        for(int i = 0; i < srcArray.length(); i++ ){
            JSONObject theObj = srcArray.getJSONObject(i);
            if(jobj.similar(theObj)){
                return true;
            }
        }
        return false;
    }

    /**
     * @param inJson
     * incoming:{
     *     id: xxxx,
     *     name: cccc,
     *     isfi: yyyy
     * }
     * * @return outForm
     * output: {
     *     id: xxxx,
     *     label: cccc,
     *     isfi: yyyy
     * }
     */
    private static JSONObject createNodeJsonObject(JSONObject inJson){
        JSONObject outForm = new JSONObject();
        outForm.put(TARGET_ID, inJson.getString(SRC_ID));
        outForm.put(TARGET_LABEL, inJson.getString(SRC_NAME));
        outForm.put(TARGET_ISFI, inJson.getString(SRC_ISFI));
        return outForm;
    }
}
