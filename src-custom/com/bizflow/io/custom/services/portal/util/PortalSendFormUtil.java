package com.bizflow.io.custom.services.portal.util;

import com.bizflow.io.core.file.util.FileUtil;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.core.rs.client.RsClient;
import com.bizflow.io.core.security.ContentCipher;
import com.bizflow.io.services.file.util.FileServiceUtil;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.Cookie;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class PortalSendFormUtil {
    private PortalSendFormUtil() {
        throw new IllegalStateException("PortalSendFormUtil class");
    }

    private static final Logger logger = LogManager.getLogger(PortalSendFormUtil.class);
    private static final String REPOSITORYDIR  = FileServiceUtil.instance.getRepositoryDirPath("formShare");
    private static final String SEND_MAIL_URL = "/services/msg/send/mail.json";

    /**
     * Sends a mail with project adx file
     *
     * <pre>
     * {
     *   "subject": "This is test subject",
     *   "contents": "<p>This is test email body contents</p>",
     *   "to": [
     *     "touser1@mail.com",
     *     "touser2@mail.com"
     *   ]
     * }
     * </pre>
     *
     * @return Response
     */
    public static Object sendMailAndProject(String district, String sessionName, HashMap parameter){
        logger.debug("PortalSendFormUtil.sendMailAndProject is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        try{
            boolean isCreated = FileUtil.createDirIfNotExist(REPOSITORYDIR);
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();

            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String sender = jsonParam.optString("sender", null);
            String projectId = jsonParam.optString("projectId");
            String projectName = jsonParam.optString("projectName");
            Map<String, Object> map = new HashMap<>();
            map.put("projectId", projectId);

            // ------------- sudo code for testing
//            map.put("id", projectId);
//            JSONObject projectJson = new JSONObject();
//            projectJson.put("workspaceId","a3b99bc336297413cbff62804d8125997");
//            map.put("project", projectJson);
            //map.put("project.workspaceId","a3b99bc336297413cbff62804d8125997");
            // ------------- sudo code for testing

            logger.debug("This class will export and get data for project.");
            JSONObject exportAndGetProjectData = PortalUtil.getSingleJSONObjectFromFileService("fbs","export-ProjectPublication@", map);

            logger.debug("will create raw file and adx file.");
            JSONObject adxAttach = createAndGetAdxJsonObject(exportAndGetProjectData, projectName);

            Object attachs = jsonParam.get("attachments");
            JSONArray jArr = null;
            if(attachs instanceof JSONArray){
                jArr = (JSONArray)attachs;
            } else {
                jArr = new JSONArray();
                jsonParam.put("attachments",jArr);
            }
            jArr.put(adxAttach);

            Object tos = jsonParam.get("to");
            JSONArray jToArr = null;
            if(tos instanceof JSONArray){
//                jToArr = (JSONArray)tos;
            } else if(tos instanceof JSONObject){
                jToArr = new JSONArray();
                jToArr.put(tos);
                jsonParam.put("to",jToArr);
            } else if(tos instanceof String){
                jToArr = new JSONArray((String)tos);
                jsonParam.put("to",jToArr);
            } else {
                jToArr = new JSONArray();
                jsonParam.put("to",jToArr);
            }
            logger.debug("will send mail with adx file");

            result = sendMail(request, JSONUtil.jsonStringToMap(jsonParam.toString()));
            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendMailAndProject", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static JSONObject sendMail(HttpServletRequest request, Map parameter){
        JSONObject result = null;

        Cookie cookie[] = request.getCookies();
        String jSessionID = PortalUtil.getJSessionID(cookie);

        String serverURL =  request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()  + request.getContextPath();
        RsClient client2 = new RsClient(serverURL + SEND_MAIL_URL);
        client2.setCookie(new javax.ws.rs.core.Cookie(PortalUtil.JSESSIONID_STR,jSessionID));

        if (client2.call("POST", parameter, MediaType.APPLICATION_JSON)){
            String responseStr = client2.getResponseString();
            logger.debug(responseStr);
            result = new JSONObject(responseStr);
        } else {
            String faultCode = client2.getResponseStatusCode() + "";
            String faultMessage = client2.getResponseString();
            logger.debug("ERROR statusCode= {}", faultCode);
            logger.debug(faultMessage);
            result = new JSONObject();
            result.put("result",false);
            result.put("faultCode", faultCode);
            result.put("faultMessage", faultMessage);
        }

        return result;
    }

    private static JSONObject createAndGetAdxJsonObject(JSONObject exportAndGetProjectData, String projectName){
        JSONObject adxAttach = null;
        ZipOutputStream out = null;

        try {
            String rawFileName = FileUtil.makeSafeFileName(projectName) + ".raw";
            File rawFile = null;

            String projectDataStr = JSONUtil.toJsonString(exportAndGetProjectData);
            projectDataStr = ContentCipher.getInstance().encipherContent(projectDataStr);

            // ------------------- create raw file
            rawFile = FileUtil.getUniqueFile(new File(REPOSITORYDIR), rawFileName);
            String rawFullFileName = REPOSITORYDIR + File.separator + rawFile.getName();
            java.nio.file.Path path = Paths.get(rawFullFileName);
            byte[] rawBytes = projectDataStr.getBytes(StandardCharsets.UTF_8);
            Files.write(path, rawBytes);

            // ------------------ create adx file
            String adxFileName = FileUtil.makeSafeFileName(projectName) + ".adx";
            File adxFile = FileUtil.getUniqueFile(new File(REPOSITORYDIR), adxFileName);
            String adxFullFileName = REPOSITORYDIR +File.separator + adxFile.getName();

            // archive adx file to zip file
            File f = new File(adxFullFileName);     // target zip file
            out = new ZipOutputStream(new FileOutputStream(f));
            ZipEntry e = new ZipEntry(rawFullFileName);
            out.putNextEntry(e);

            out.write(rawBytes, 0, rawBytes.length);
            adxAttach = new JSONObject();
            adxAttach.put("name", f.getName());
            adxAttach.put("path", f.getCanonicalPath());

        } catch(Exception e){
            e.printStackTrace();
        } finally {
            try {
                out.closeEntry();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return adxAttach;
    }
}
