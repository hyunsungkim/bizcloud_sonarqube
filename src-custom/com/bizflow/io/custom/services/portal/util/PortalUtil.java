package com.bizflow.io.custom.services.portal.util;

import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.services.data.dao.util.SqlTransactionDAOUtil;
import com.bizflow.io.services.data.exception.MoreThanOneRecordException;
import com.bizflow.io.services.file.session.FileServiceSessionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Map;

public class PortalUtil {
    private PortalUtil() {
        throw new IllegalStateException("ProtalUtil class");
    }

    private static final Logger logger = LogManager.getLogger(PortalUtil.class);
    protected static final String JSESSIONID_STR = "JSESSIONID";

    protected static JSONObject getSingleJSONObjectFromFileService(String sessionName, String queryName, Map parameter) throws Exception{
        JSONObject data = null;

        JSONObject virtualService = FileServiceSessionManager.getInstance().getVirtualService(sessionName, queryName);
        Object returnObject = SqlTransactionDAOUtil.runVirtualQuery(sessionName, virtualService, parameter);

        if (returnObject instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) returnObject;
            if (jsonArray.length() > 1) {
                logger.error("Only one record must exist but there are more than one record for {}, parameter={}", queryName, parameter);
                throw new MoreThanOneRecordException("Only one record must exist but there are more than one record for " + queryName + ", parameter=" + parameter);
            } else if (jsonArray.length() == 1) {
                data = jsonArray.getJSONObject(0);
            }
        } else if (returnObject instanceof JSONObject) {
            data = (JSONObject) returnObject;
        } else if (returnObject instanceof HashMap) {
            data = new JSONObject(JSONUtil.mapToJsonString((Map)returnObject));
        }

        return data;
    }

    protected static String getJSessionID(Cookie cookies[]){
        String jsessionId = null;
        if(cookies == null || cookies.length < 1){
            return null;
        }

        for(Cookie c: cookies){
            String name = c.getName();
            if(name != null && JSESSIONID_STR.equals(name)){
                jsessionId = c.getValue();
                break;
            }
        }
        return jsessionId;
    }

}
