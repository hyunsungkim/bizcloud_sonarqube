package com.bizflow.io.custom.services.train.util;//


import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.core.security.SecurityCipher;
import com.bizflow.io.services.bizflow.util.BizFlowServiceConfigUtil;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import com.bizflow.io.services.data.dao.util.SqlDAOUtil;
import com.bizflow.io.services.message.exception.MessageServiceException;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import com.hs.bf.web.beans.*;
import com.hs.bf.web.xmlrs.XMLResultSet;
import com.hs.bf.web.xmlrs.XMLResultSetImpl;
import com.hs.bf.wf.jo.HWAuthorityGroupsImpl;
import com.hs.bf.wf.jo.HWException;
import com.hs.bf.wf.jo.HWFolderMemberImpl;
import com.hs.bf.wf.jo.HWFolderMembers;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class BPMUserManage {
    private static final Logger logger = LogManager.getLogger(BPMUserManage.class);

    public BPMUserManage() {
    }

    private static boolean checkDomainValidate(String domainOrIP) {
        return DomainValidator.getInstance().isValid(domainOrIP);
    }

    private static boolean checkEmailValidate(String mailAddr) {
        return EmailValidator.getInstance().isValid(mailAddr);
    }

    public static boolean isBinary(String str) {
        char[] var1 = str.toCharArray();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            char c = var1[var3];
            if (c != '0' && c != '1') {
                return false;
            }
        }

        return true;
    }

    public static Object addManyUsers(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        String serverIP = null;
        int serverPort = 7201;
        String loginID = null;
        String password = null;
        String fullUserName = null;
        String shortUserName = null;
        String email = null;
        String empID = null;
        String authFlag = null;
        String jobTitleName = "Administrator";
        String jobTitleID = "9999999999";
        String deptID = "9000000000";
        String deptName = "Root";
        boolean forceCheckOut = true;
        String systemUserID = null;
        String systemUserPW = null;

        try {
            logger.debug("BPMUserManage.addManyUsers Started!");
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            systemUserID = CustomServiceConfigUtil.getProperty("bpm.system.user.id", "administrator");
            systemUserPW = CustomServiceConfigUtil.getProperty("bpm.system.user.pw", "");
            logger.debug("encoded systemUserPW= %s", systemUserPW);
            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);
            logger.debug("decoded systemUserPW= %s", systemUserPW);
            if (jsonParam.has("SERVER_IP") && !jsonParam.isNull("SERVER_IP")) {
                serverIP = jsonParam.getString("SERVER_IP");
            } else {
                serverIP = BizFlowServiceConfigUtil.getBizFlowServer();
            }

            if (jsonParam.has("SERVER_PORT") && !jsonParam.isNull("SERVER_PORT")) {
                serverPort = jsonParam.getInt("SERVER_PORT");
            }

            if (serverIP != null && !serverIP.equals("")) {
                if (!checkDomainValidate(serverIP)) {
                    throw new MessageServiceException("SERVER_IP value is not valid.");
                } else {
                    if (serverPort < 1) {
                        serverPort = 7201;
                    }

                    HWSessionFactory hwSessionFactory = new HWSessionFactory();
                    HWSessionFactory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
                    HWSession hwSession = hwSessionFactory.newInstance();
                    String sessionInfoXML = hwSession.logIn(serverIP, serverPort, systemUserID, systemUserPW, true);
                    HWSessionInfo hwSessionInfo = new HWSessionInfo();
                    hwSessionInfo.reset(sessionInfoXML);
                    HWInteger reasonNumber = new HWInteger();
                    hwSession.checkOutOrg(sessionInfoXML, forceCheckOut, deptID, reasonNumber);
                    if (reasonNumber.getValue() != 0) {
                        logger.debug("checkOutOrg Error reasonNumber= %s", reasonNumber);
                        throw new MessageServiceException("Fail to CheckOutOrg. ReasonNumber:" + reasonNumber);
                    } else {
                        JSONArray indInfoArr = jsonParam.getJSONArray("userInfos");
                        int ind = 0;

                        while(ind < indInfoArr.length()) {
                            JSONObject jobj = indInfoArr.getJSONObject(ind);
                            loginID = jobj.getString("LOGINID");
                            password = jobj.getString("PASSWORD");
                            if (jobj.has("NAME") && !jobj.isNull("NAME")) {
                                fullUserName = jobj.getString("NAME");
                            }

                            if (jobj.has("SHORTNAME") && !jobj.isNull("SHORTNAME")) {
                                shortUserName = jobj.getString("SHORTNAME");
                            }

                            if (jobj.has("EMAIL") && !jobj.isNull("EMAIL")) {
                                email = jobj.getString("EMAIL");
                            }

                            if (jobj.has("EMPLOYEENUMBER") && !jobj.isNull("EMPLOYEENUMBER")) {
                                empID = jobj.getString("EMPLOYEENUMBER");
                            }

                            authFlag = jobj.getString("AUTH_FLAG");
                            if (loginID != null && !loginID.equals("")) {
                                if (password != null && !password.equals("")) {
                                    if (fullUserName == null || fullUserName.equals("")) {
                                        fullUserName = "AppDev User";
                                    }

                                    if (shortUserName == null || shortUserName.equals("")) {
                                        shortUserName = "AppDev User";
                                    }

                                    if (email == null) {
                                        email = "";
                                    } else if (!checkEmailValidate(email)) {
                                        throw new MessageServiceException("EMAIL value is not valid.");
                                    }

                                    if (empID == null) {
                                        empID = "";
                                    }

                                    if (authFlag != null && !authFlag.equals("") && authFlag.length() == 9 && isBinary(authFlag)) {
                                        HWString encryptedPassword = new HWString();
                                        hwSession.encryptString(password, encryptedPassword);
                                        logger.debug("Non encoded UserPW= %s", password);
                                        logger.debug("encoded UserPW= %s", encryptedPassword.getValue());
                                        XMLResultSetImpl xrsUsers = new XMLResultSetImpl();
                                        xrsUsers.createResultSet("HWUsers", "HWUSER");
                                        xrsUsers.add();
                                        xrsUsers.setFieldValueAt(0, "ID", "_USER00001");
                                        xrsUsers.setFieldValueAt(0, "LICENSETYPE", "N");
                                        xrsUsers.setFieldValueAt(0, "INHERITTYPE", "P");
                                        xrsUsers.setFieldValueAt(0, "NAME", fullUserName);
                                        xrsUsers.setFieldValueAt(0, "MANAGERID", "0000000000");
                                        xrsUsers.setFieldValueAt(0, "EMAIL", email);
                                        xrsUsers.setFieldValueAt(0, "EMPLOYEENUMBER", empID);
                                        xrsUsers.setFieldValueAt(0, "JOBTITLEID", jobTitleID);
                                        xrsUsers.setFieldValueAt(0, "JOBTITLEIDA", "");
                                        xrsUsers.setFieldValueAt(0, "JOBTITLEIDB", "");
                                        xrsUsers.setFieldValueAt(0, "DEPTID", deptID);
                                        xrsUsers.setFieldValueAt(0, "SERVERID", hwSessionInfo.get("ServerID"));
                                        xrsUsers.setFieldValueAt(0, "ISABSENT", "F");
                                        xrsUsers.setFieldValueAt(0, "STATE", "N");
                                        xrsUsers.setFieldValueAt(0, "JOBTITLENAME", jobTitleName);
                                        xrsUsers.setFieldValueAt(0, "JOBTITLENAMEA", "");
                                        xrsUsers.setFieldValueAt(0, "JOBTITLENAMEB", "");
                                        xrsUsers.setFieldValueAt(0, "DEPTNAME", deptName);
                                        xrsUsers.setFieldValueAt(0, "SHORTNAME", shortUserName);
                                        xrsUsers.setFieldValueAt(0, "LOGINID", loginID);
                                        xrsUsers.setFieldValueAt(0, "TYPE", "U");
                                        xrsUsers.setFieldValueAt(0, "DISPORDER", "0");
                                        xrsUsers.setFieldValueAt(0, "PASSWORD", encryptedPassword.getValue());
                                        xrsUsers.setFieldValueAt(0, "CHANGEPASSWD", "F");
                                        xrsUsers.setFieldValueAt(0, "PASSWDFAILCOUNT", "0");
                                        XMLResultSetImpl xrsLicenseGroups = new XMLResultSetImpl();
                                        xrsLicenseGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");


                                        int objectRow;
                                        for(int i = 0; i < authFlag.length(); ++i) {
                                            char theAuth = authFlag.charAt(i);
                                            if (theAuth != '0') {
                                                objectRow = xrsLicenseGroups.add();
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "USERGROUPID", "licsgrp00" + (i + 1));
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                            }
                                        }

                                        XMLResultSetImpl xrsAuthGroups = new XMLResultSetImpl();
                                        xrsAuthGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");

                                        for(int i = 0; i < authFlag.length(); ++i) {
                                            char theAuth = authFlag.charAt(i);
                                            if (theAuth != '0') {
                                                if (i < 4) {
                                                    objectRow = xrsAuthGroups.add();
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 1));
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                                }

                                                if (i > 2) {
                                                    objectRow = xrsAuthGroups.add();
                                                    if (i < 8) {
                                                        xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 2));
                                                    } else {
                                                        xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp0" + (i + 2));
                                                    }

                                                    xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                                }
                                            }
                                        }

                                        hwSession.updateOrg(sessionInfoXML, (byte[])null, xrsUsers.toByteArray(), xrsAuthGroups.toByteArray(), xrsLicenseGroups.toByteArray(), (byte[])null);
                                        String masterUserInfoQueryName = "usermng.CreateUserAdditionalInfo";
                                        String queryAction = "insert";
                                        Map<String, Object> paramMap = new HashMap();
                                        HWFilter hwFilterUser = new HWFilter();
                                        hwFilterUser.setName("HWUser");
                                        hwFilterUser.addFilter("LOGINID", "E", loginID);
                                        InputStream resultStream = hwSession.getUsers(sessionInfoXML, hwFilterUser.toByteArray());
                                        XMLResultSet xrs = new XMLResultSetImpl();
                                        xrs.parse(resultStream);
                                        paramMap.put("MEMBERID", xrs.getFieldValueAt(0, "ID"));
                                        resultStream.close();
                                        paramMap.put("SHORTNAME", jobj.get("SHORTNAME"));
                                        paramMap.put("NAME", jobj.get("NAME"));
                                        paramMap.put("LOGINID", jobj.get("LOGINID"));
                                        paramMap.put("EMPLOYEENUMBER", jobj.get("EMPLOYEENUMBER"));
                                        paramMap.put("EMAIL", jobj.get("EMAIL"));
                                        paramMap.put("AUTH_FLAG", jobj.get("AUTH_FLAG"));
                                        paramMap.put("INVESTIGATION", jobj.get("Investigation"));
                                        paramMap.put("CONTRACTORCOMPANY", jobj.get("ContractorCompany"));
                                        paramMap.put("CONTRACTORADDRESS", jobj.get("ContractorAddress"));
                                        paramMap.put("CONTRACTORSTATE", jobj.get("ContractorState"));
                                        paramMap.put("CONTRACTORCITY", jobj.get("ContractorCity"));
                                        paramMap.put("CONTRACTORZIP", jobj.get("ContractorZip"));
                                        paramMap.put("CONTRACTORDUNSNUMBER", jobj.get("ContractorDUNSNumber"));
                                        paramMap.put("CONTRACTNUMBER", jobj.get("ContractNumber"));
                                        paramMap.put("SOCIALSECURITYNUMBER", jobj.get("SocialSecurityNumber"));
                                        paramMap.put("LASTNAME", jobj.get("LastName"));
                                        paramMap.put("FIRSTNAME", jobj.get("FirstName"));
                                        paramMap.put("MIDDLENAME", jobj.get("MiddleName"));
                                        paramMap.put("EMAILADDRESS", jobj.get("EmailAddress"));
                                        paramMap.put("PHONENUMBER", jobj.get("PhoneNumber"));
                                        paramMap.put("STREETADDRESS", jobj.get("StreetAddress"));
                                        paramMap.put("STATE", jobj.get("State"));
                                        paramMap.put("CITY", jobj.get("City"));
                                        paramMap.put("ZIP", jobj.get("Zip"));
                                        paramMap.put("GRADE", jobj.get("Grade"));
                                        paramMap.put("SERIES", jobj.get("Series"));
                                        paramMap.put("POSITIONTITLE", jobj.get("PositionTitle"));
                                        paramMap.put("DUTYLOCATIONSTATE", jobj.get("DutyLocationState"));
                                        paramMap.put("DUTYLOCATIONCITY", jobj.get("DutyLocationCity"));
                                        Object resultObject = SqlDAOUtil.runSql("train", masterUserInfoQueryName, queryAction, paramMap);
                                        logger.debug("############### resultObject : %s", resultObject);
                                        ++ind;
                                        continue;
                                    }

                                    throw new MessageServiceException("AUTH_FLAG value is not valid. The value is 9 digit number which is consisted of 0 or 1.");
                                }

                                throw new MessageServiceException("Need parameter PASSWORD");
                            }

                            throw new MessageServiceException("Need parameter LOGINID");
                        }

                        hwSession.checkInOrg(sessionInfoXML, deptID);
                        logger.debug("The BPM Users was successfully added. The login ID is %s", loginID );
                        hwSession.logOut(sessionInfoXML);
                        result = new JSONObject();
                        result.put("result", true);
                    }
                }
            } else {
                throw new MessageServiceException("Need parameter SERVER_IP");
            }
        } catch (Exception var47) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var47);
            var47.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var47.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "addManyUsers", System.currentTimeMillis() - timeS);
        }
        return result;
    }

    public static Object addBPMUser(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        String serverIP = null;
        int serverPort = 7201;
        String loginID = null;
        String password = null;
        String fullUserName = null;
        String shortUserName = null;
        String email = null;
        String empID = null;
        String authFlag = null;
        String jobTitleName = "Administrator";
        String jobTitleID = "9999999999";
        String deptID = "9000000000";
        String deptName = "Root";
        boolean forceCheckOut = true;
        String systemUserID = null;
        String systemUserPW = null;

        try {
            logger.debug("BPMUserManage.addBPMUser Started!");
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            if (jsonParam.has("SERVER_IP") && !jsonParam.isNull("SERVER_IP")) {
                serverIP = jsonParam.getString("SERVER_IP");
            } else {
                serverIP = BizFlowServiceConfigUtil.getBizFlowServer();
            }

            if (jsonParam.has("SERVER_PORT") && !jsonParam.isNull("SERVER_PORT")) {
                serverPort = jsonParam.getInt("SERVER_PORT");
            }

            loginID = jsonParam.getString("LOGINID");
            password = jsonParam.getString("PASSWORD");
            if (jsonParam.has("NAME") && !jsonParam.isNull("NAME")) {
                fullUserName = jsonParam.getString("NAME");
            }

            if (jsonParam.has("SHORTNAME") && !jsonParam.isNull("SHORTNAME")) {
                shortUserName = jsonParam.getString("SHORTNAME");
            }

            if (jsonParam.has("EMAIL") && !jsonParam.isNull("EMAIL")) {
                email = jsonParam.getString("EMAIL");
            }

            if (jsonParam.has("EMPLOYEENUMBER") && !jsonParam.isNull("EMPLOYEENUMBER")) {
                empID = jsonParam.getString("EMPLOYEENUMBER");
            }

            authFlag = jsonParam.getString("AUTH_FLAG");
            systemUserID = CustomServiceConfigUtil.getProperty("bpm.system.user.id", "administrator");
            systemUserPW = CustomServiceConfigUtil.getProperty("bpm.system.user.pw", "");
            logger.debug("encoded systemUserPW= %s", systemUserPW);
            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);
            logger.debug("decoded systemUserPW= %s", systemUserPW);
            if (serverIP != null && !serverIP.equals("")) {
                if (!checkDomainValidate(serverIP)) {
                    throw new MessageServiceException("SERVER_IP value is not valid.");
                } else {
                    if (serverPort < 1) {
                        serverPort = 7201;
                    }

                    if (loginID != null && !loginID.equals("")) {
                        if (password != null && !password.equals("")) {
                            if (fullUserName == null || fullUserName.equals("")) {
                                fullUserName = "AppDev User";
                            }

                            if (shortUserName == null || shortUserName.equals("")) {
                                shortUserName = "AppDev User";
                            }

                            if (email == null) {
                                email = "";
                            } else if (!checkEmailValidate(email)) {
                                throw new MessageServiceException("EMAIL value is not valid.");
                            }

                            if (empID == null) {
                                empID = "";
                            }

                            if (authFlag != null && !authFlag.equals("") && authFlag.length() == 9 && isBinary(authFlag)) {
                                HWSessionFactory hwSessionFactory = new HWSessionFactory();
                                HWSessionFactory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
                                HWSession hwSession = hwSessionFactory.newInstance();
                                String sessionInfoXML = hwSession.logIn(serverIP, serverPort, systemUserID, systemUserPW, true);
                                HWSessionInfo hwSessionInfo = new HWSessionInfo();
                                hwSessionInfo.reset(sessionInfoXML);
                                HWInteger reasonNumber = new HWInteger();
                                hwSession.checkOutOrg(sessionInfoXML, forceCheckOut, deptID, reasonNumber);
                                if (reasonNumber.getValue() != 0) {
                                    logger.debug("checkOutOrg Error reasonNumber= %s", reasonNumber);
                                    throw new MessageServiceException("Fail to CheckOutOrg. ReasonNumber:" + reasonNumber);
                                } else {
                                    HWString encryptedPassword = new HWString();
                                    hwSession.encryptString(password, encryptedPassword);
                                    logger.debug("Non encoded UserPW= {}", password);
                                    logger.debug("encoded UserPW= {}", encryptedPassword.getValue());
                                    XMLResultSetImpl xrsUsers = new XMLResultSetImpl();
                                    xrsUsers.createResultSet("HWUsers", "HWUSER");
                                    xrsUsers.add();
                                    xrsUsers.setFieldValueAt(0, "ID", "_USER00001");
                                    xrsUsers.setFieldValueAt(0, "LICENSETYPE", "N");
                                    xrsUsers.setFieldValueAt(0, "INHERITTYPE", "P");
                                    xrsUsers.setFieldValueAt(0, "NAME", fullUserName);
                                    xrsUsers.setFieldValueAt(0, "MANAGERID", "0000000000");
                                    xrsUsers.setFieldValueAt(0, "EMAIL", email);
                                    xrsUsers.setFieldValueAt(0, "EMPLOYEENUMBER", empID);
                                    xrsUsers.setFieldValueAt(0, "JOBTITLEID", jobTitleID);
                                    xrsUsers.setFieldValueAt(0, "JOBTITLEIDA", "");
                                    xrsUsers.setFieldValueAt(0, "JOBTITLEIDB", "");
                                    xrsUsers.setFieldValueAt(0, "DEPTID", deptID);
                                    xrsUsers.setFieldValueAt(0, "SERVERID", hwSessionInfo.get("ServerID"));
                                    xrsUsers.setFieldValueAt(0, "ISABSENT", "F");
                                    xrsUsers.setFieldValueAt(0, "STATE", "N");
                                    xrsUsers.setFieldValueAt(0, "JOBTITLENAME", jobTitleName);
                                    xrsUsers.setFieldValueAt(0, "JOBTITLENAMEA", "");
                                    xrsUsers.setFieldValueAt(0, "JOBTITLENAMEB", "");
                                    xrsUsers.setFieldValueAt(0, "DEPTNAME", deptName);
                                    xrsUsers.setFieldValueAt(0, "SHORTNAME", shortUserName);
                                    xrsUsers.setFieldValueAt(0, "LOGINID", loginID);
                                    xrsUsers.setFieldValueAt(0, "TYPE", "U");
                                    xrsUsers.setFieldValueAt(0, "DISPORDER", "0");
                                    xrsUsers.setFieldValueAt(0, "PASSWORD", encryptedPassword.getValue());
                                    xrsUsers.setFieldValueAt(0, "CHANGEPASSWD", "F");
                                    xrsUsers.setFieldValueAt(0, "PASSWDFAILCOUNT", "0");
                                    XMLResultSetImpl xrsLicenseGroups = new XMLResultSetImpl();
                                    xrsLicenseGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");


                                    int objectRow;
                                    for(int i = 0; i < authFlag.length(); ++i) {
                                        char theAuth = authFlag.charAt(i);
                                        if (theAuth != '0') {
                                            objectRow = xrsLicenseGroups.add();
                                            xrsLicenseGroups.setFieldValueAt(objectRow, "USERGROUPID", "licsgrp00" + (i + 1));
                                            xrsLicenseGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");
                                            xrsLicenseGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                            xrsLicenseGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                        }
                                    }

                                    XMLResultSetImpl xrsAuthGroups = new XMLResultSetImpl();
                                    xrsAuthGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");

                                    for(int i = 0; i < authFlag.length(); ++i) {
                                        char theAuth = authFlag.charAt(i);
                                        if (theAuth != '0') {
                                            if (i < 4) {
                                                objectRow = xrsAuthGroups.add();
                                                xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 1));
                                                xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");
                                                xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                            }

                                            if (i > 2) {
                                                objectRow = xrsAuthGroups.add();
                                                if (i < 8) {
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 2));
                                                } else {
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp0" + (i + 2));
                                                }

                                                xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", "_USER00001");
                                                xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                            }
                                        }
                                    }

                                    hwSession.updateOrg(sessionInfoXML, (byte[])null, xrsUsers.toByteArray(), xrsAuthGroups.toByteArray(), xrsLicenseGroups.toByteArray(), (byte[])null);
                                    hwSession.checkInOrg(sessionInfoXML, deptID);
                                    logger.debug("The BPM User was successfully added. login ID is : %s", loginID);
                                    HWFilter hwFilterUser = new HWFilter();
                                    hwFilterUser.setName("HWUser");
                                    hwFilterUser.addFilter("LOGINID", "E", loginID);
                                    InputStream resultStream = hwSession.getUsers(sessionInfoXML, hwFilterUser.toByteArray());
                                    XMLResultSet xrs = new XMLResultSetImpl();
                                    xrs.parse(resultStream);
                                    logger.debug("    ID: %s" , xrs.getFieldValueAt(0, "ID"));
                                    resultStream.close();
                                    hwSession.logOut(sessionInfoXML);
                                    result = new JSONObject();
                                    result.put("result", true);
                                }
                            } else {
                                throw new MessageServiceException("AUTH_FLAG value is not valid. The value is 9 digit number which is consisted of 0 or 1.");
                            }
                        } else {
                            throw new MessageServiceException("Need parameter PASSWORD");
                        }
                    } else {
                        throw new MessageServiceException("Need parameter LOGINID");
                    }
                }
            } else {
                throw new MessageServiceException("Need parameter SERVER_IP");
            }
        } catch (Exception var40) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var40);
            var40.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var40.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "addBPMUser", System.currentTimeMillis() - timeS);
        }
        return result;
    }

    public static Object delBPMUser(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        String serverIP = null;
        int serverPort = 7201;
        String loginID = null;
        boolean forceCheckOut = true;
        String systemUserID = null;
        String systemUserPW = null;

        try {
            logger.debug("BPMUserManage.delBPMUser Started!");
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            logger.debug("All jobs are done!");
            if (jsonParam.has("SERVER_IP") && !jsonParam.isNull("SERVER_IP")) {
                serverIP = jsonParam.getString("SERVER_IP");
            } else {
                serverIP = BizFlowServiceConfigUtil.getBizFlowServer();
            }

            if (jsonParam.has("SERVER_PORT") && !jsonParam.isNull("SERVER_PORT")) {
                serverPort = jsonParam.getInt("SERVER_PORT");
            }

            loginID = jsonParam.getString("LOGINID");
            systemUserID = CustomServiceConfigUtil.getProperty("bpm.system.user.id", "administrator");
            systemUserPW = CustomServiceConfigUtil.getProperty("bpm.system.user.pw", "");
            logger.debug("encoded systemUserPW= %s", systemUserPW);
            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);
            logger.debug("decoded systemUserPW= %s", systemUserPW);
            if (serverIP != null && !serverIP.equals("")) {
                if (!checkDomainValidate(serverIP)) {
                    throw new MessageServiceException("SERVER_IP value is not valid.");
                } else {
                    if (serverPort < 1) {
                        serverPort = 7201;
                    }

                    if (loginID != null && !loginID.equals("")) {
                        HWSessionFactory hwSessionFactory = new HWSessionFactory();
                        HWSessionFactory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
                        HWSession hwSession = hwSessionFactory.newInstance();
                        String sessionInfoXML = hwSession.logIn(serverIP, serverPort, systemUserID, systemUserPW, true);
                        HWSessionInfo hwSessionInfo = new HWSessionInfo();
                        hwSessionInfo.reset(sessionInfoXML);
                        HWFilter hwFilter = new HWFilter();
                        hwFilter.setName("HWUser");
                        hwFilter.addFilter("LOGINID", "E", loginID);
                        InputStream inStream = hwSession.getUsers(sessionInfoXML, hwFilter.toByteArray());
                        XMLResultSet xrsUser = new XMLResultSetImpl();
                        xrsUser.parse(inStream);
                        if (xrsUser.getRowCount() >= 1) {
                            String deptID = xrsUser.getFieldValueAt(0, "DEPTID");
                            HWInteger reasonNumber = new HWInteger();
                            hwSession.checkOutOrg(sessionInfoXML, forceCheckOut, deptID, reasonNumber);
                            if (reasonNumber.getValue() != 0) {
                                throw new MessageServiceException("checkOutOrg Error reasonNumber=" + reasonNumber);
                            } else {
                                xrsUser.remove(0);
                                xrsUser.setFilter(8);
                                ByteArrayOutputStream os = new ByteArrayOutputStream(500 * xrsUser.getRowCount());
                                xrsUser.serialize(os);
                                hwSession.updateOrg(sessionInfoXML, (byte[])null, xrsUser.toByteArray(), (byte[])null, (byte[])null, (byte[])null);
                                hwSession.checkInOrg(sessionInfoXML, deptID);
                                logger.debug("The user was removed. id: %s", loginID);
                                result = new JSONObject();
                                result.put("result", true);
                            }
                        } else {
                            throw new MessageServiceException("There is no such loginID id: " + loginID);
                        }
                    } else {
                        throw new MessageServiceException("Need parameter LOGINID");
                    }
                }
            } else {
                throw new MessageServiceException("Need parameter SERVER_IP");
            }
        } catch (Exception var27) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var27);
            var27.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var27.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "delBPMUser", System.currentTimeMillis() - timeS);
        }
        return result;
    }

    public static Object modBPMUser(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        String serverIP = null;
        int serverPort = 7201;
        String loginID = null;
        String password = null;
        String fullUserName = null;
        String shortUserName = null;
        String email = null;
        String empID = null;
        String lockYN = null;
        String authFlag = null;
        String deptID = "9000000000";
        String deptName = "Root";
        boolean forceCheckOut = true;
        String systemUserID = null;
        String systemUserPW = null;

        try {
            logger.debug("BPMUserManage.modBPMUser Started!");
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            if (jsonParam.has("SERVER_IP") && !jsonParam.isNull("SERVER_IP")) {
                serverIP = jsonParam.getString("SERVER_IP");
            } else {
                serverIP = BizFlowServiceConfigUtil.getBizFlowServer();
            }

            if (jsonParam.has("SERVER_PORT") && !jsonParam.isNull("SERVER_PORT")) {
                serverPort = jsonParam.getInt("SERVER_PORT");
            }

            loginID = jsonParam.getString("LOGINID");
            if (jsonParam.has("PASSWORD") && !jsonParam.isNull("PASSWORD")) {
                password = jsonParam.getString("PASSWORD");
            }

            if (jsonParam.has("NAME") && !jsonParam.isNull("NAME")) {
                fullUserName = jsonParam.getString("NAME");
            }

            if (jsonParam.has("SHORTNAME") && !jsonParam.isNull("SHORTNAME")) {
                shortUserName = jsonParam.getString("SHORTNAME");
            }

            if (jsonParam.has("EMAIL") && !jsonParam.isNull("EMAIL")) {
                email = jsonParam.getString("EMAIL");
            }

            if (jsonParam.has("EMPLOYEENUMBER") && !jsonParam.isNull("EMPLOYEENUMBER")) {
                empID = jsonParam.getString("EMPLOYEENUMBER");
            }

            if (jsonParam.has("AUTH_FLAG") && !jsonParam.isNull("AUTH_FLAG")) {
                authFlag = jsonParam.getString("AUTH_FLAG");
            }

            if (jsonParam.has("LOCK_YN") && !jsonParam.isNull("LOCK_YN")) {
                lockYN = jsonParam.getString("LOCK_YN");
            }

            systemUserID = CustomServiceConfigUtil.getProperty("bpm.system.user.id", "administrator");
            systemUserPW = CustomServiceConfigUtil.getProperty("bpm.system.user.pw", "");
            logger.debug("encoded systemUserPW= %s", systemUserPW);
            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);
            logger.debug("decoded systemUserPW= %s", systemUserPW);
            if (serverIP != null && !serverIP.equals("")) {
                if (!checkDomainValidate(serverIP)) {
                    throw new MessageServiceException("SERVER_IP value is not valid.");
                } else {
                    if (serverPort < 1) {
                        serverPort = 7201;
                    }

                    if (loginID != null && !loginID.equals("")) {
                        if (password == null && fullUserName == null && shortUserName == null && email == null && empID == null && authFlag == null) {
                            throw new MessageServiceException("At least more than 1 item is needed to be changed in the user info");
                        } else if (email != null && !checkEmailValidate(email)) {
                            throw new MessageServiceException("EMAIL value is not valid.");
                        } else if (authFlag == null || authFlag.length() == 9 && isBinary(authFlag)) {
                            HWSessionFactory hwSessionFactory = new HWSessionFactory();
                            HWSessionFactory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
                            HWSession hwSession = hwSessionFactory.newInstance();
                            String sessionInfoXML = hwSession.logIn(serverIP, serverPort, systemUserID, systemUserPW, true);
                            HWSessionInfo hwSessionInfo = new HWSessionInfo();
                            hwSessionInfo.reset(sessionInfoXML);
                            HWString encryptedPassword = new HWString();
                            if (password != null) {
                                logger.debug("Non encoded UserPW= %s", password);
                                hwSession.encryptString(password, encryptedPassword);
                                logger.debug("encoded UserPW= %s", password);
                            }

                            HWFilter hwFilter = new HWFilter();
                            hwFilter.setName("HWUser");
                            hwFilter.addFilter("LOGINID", "E", loginID);
                            InputStream inStream = hwSession.getUsers(sessionInfoXML, hwFilter.toByteArray());
                            XMLResultSetImpl xrsUser = new XMLResultSetImpl();
                            xrsUser.parse(inStream);
                            if (xrsUser.getRowCount() >= 1) {
                                String id = xrsUser.getFieldValueAt(0, "ID");
                                deptID = xrsUser.getFieldValueAt(0, "DEPTID");
                                HWInteger reasonNumber = new HWInteger();
                                hwSession.checkOutOrg(sessionInfoXML, forceCheckOut, deptID, reasonNumber);
                                if (reasonNumber.getValue() != 0) {
                                    logger.debug("checkOutOrg Error reasonNumber= %s", reasonNumber);
                                    throw new MessageServiceException("Fail to CheckOutOrg. ReasonNumber:" + reasonNumber);
                                } else {
                                    if (password != null) {
                                        xrsUser.setFieldValueAt(0, "PASSWORD", encryptedPassword.getValue());
                                    }

                                    if (fullUserName != null) {
                                        xrsUser.setFieldValueAt(0, "NAME", fullUserName);
                                    }

                                    if (shortUserName != null) {
                                        xrsUser.setFieldValueAt(0, "SHORTNAME", shortUserName);
                                    }

                                    if (email != null) {
                                        xrsUser.setFieldValueAt(0, "EMAIL", email);
                                    }

                                    if (empID != null) {
                                        xrsUser.setFieldValueAt(0, "EMPLOYEENUMBER", empID);
                                    }

                                    if (lockYN != null) {
                                        if (lockYN.equalsIgnoreCase("Y")) {
                                            xrsUser.setFieldValueAt(0, "STATE", "C");
                                        } else {
                                            xrsUser.setFieldValueAt(0, "STATE", "N");
                                        }
                                    }

                                    XMLResultSetImpl xrsLicenseGroups = null;
                                    XMLResultSetImpl xrsAuthGroups = null;
                                    if (authFlag != null) {
                                        xrsLicenseGroups = new XMLResultSetImpl();
                                        xrsLicenseGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");


                                        int i;
                                        char theAuth;
                                        int objectRow;
                                        for(i = 0; i < authFlag.length(); ++i) {
                                            theAuth = authFlag.charAt(i);
                                            if (theAuth != '0') {
                                                objectRow = xrsLicenseGroups.add();
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "USERGROUPID", "licsgrp00" + (i + 1));
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "PARTICIPANTID", id);
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                xrsLicenseGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                            }
                                        }

                                        xrsAuthGroups = new XMLResultSetImpl();
                                        xrsAuthGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");

                                        for(i = 0; i < authFlag.length(); ++i) {
                                            theAuth = authFlag.charAt(i);
                                            if (theAuth != '0') {
                                                if (i < 4) {
                                                    objectRow = xrsAuthGroups.add();
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 1));
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", id);
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                                }

                                                if (i > 2) {
                                                    objectRow = xrsAuthGroups.add();
                                                    if (i < 8) {
                                                        xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 2));
                                                    } else {
                                                        xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp0" + (i + 2));
                                                    }

                                                    xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", id);
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                                                    xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                                                }
                                            }
                                        }
                                    }

                                    if (xrsLicenseGroups != null) {
                                        hwSession.updateOrg(sessionInfoXML, (byte[])null, xrsUser.toByteArray(), xrsAuthGroups.toByteArray(), xrsLicenseGroups.toByteArray(), (byte[])null);
                                    } else {
                                        hwSession.updateOrg(sessionInfoXML, (byte[])null, xrsUser.toByteArray(), (byte[])null, (byte[])null, (byte[])null);
                                    }

                                    hwSession.checkInOrg(sessionInfoXML, deptID);
                                    logger.debug("The User account was modified successfully.");
                                    result = new JSONObject();
                                    result.put("result", true);
                                }
                            } else {
                                throw new MessageServiceException("Fail to find the user. loginID:" + loginID);
                            }
                        } else {
                            throw new MessageServiceException("AUTH_FLAG value is not valid. The value is 9 digit number which is consisted of 0 or 1.");
                        }
                    } else {
                        throw new MessageServiceException("Need parameter LOGINID");
                    }
                }
            } else {
                throw new MessageServiceException("Need parameter SERVER_IP");
            }
        } catch (Exception var41) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var41);
            var41.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var41.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "modBPMUser", System.currentTimeMillis() - timeS);
        }
        return result;
    }

    public static Object getBPMUsers(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        String serverIP = null;
        int serverPort = 7201;
        String systemUserID = null;
        String systemUserPW = null;
        String var11 = "9000000000";

        try {
            logger.debug("BPMUserManage.getBPMUsers Started!");
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            if (jsonParam.has("SERVER_IP") && !jsonParam.isNull("SERVER_IP")) {
                serverIP = jsonParam.getString("SERVER_IP");
            } else {
                serverIP = BizFlowServiceConfigUtil.getBizFlowServer();
            }

            if (jsonParam.has("SERVER_PORT") && !jsonParam.isNull("SERVER_PORT")) {
                serverPort = jsonParam.getInt("SERVER_PORT");
            }

            systemUserID = CustomServiceConfigUtil.getProperty("bpm.system.user.id", "administrator");
            systemUserPW = CustomServiceConfigUtil.getProperty("bpm.system.user.pw", "");
            logger.debug("encoded systemUserPW= %s", systemUserPW);
            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);
            logger.debug("decoded systemUserPW= %s", systemUserPW);
            if (serverIP != null && !serverIP.equals("")) {
                if (!checkDomainValidate(serverIP)) {
                    throw new MessageServiceException("SERVER_IP value is not valid.");
                } else {
                    if (serverPort < 1) {
                        serverPort = 7201;
                    }

                    HWSessionFactory hwSessionFactory = new HWSessionFactory();
                    HWSessionFactory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
                    HWSession hwSession = hwSessionFactory.newInstance();
                    String sessionInfoXML = hwSession.logIn(serverIP, serverPort, systemUserID, systemUserPW, true);
                    HWSessionInfo hwSessionInfo = new HWSessionInfo();
                    hwSessionInfo.reset(sessionInfoXML);
                    HWFilter hwFilterUser = new HWFilter();
                    hwFilterUser.setName("HWUser");
                    InputStream resultStream = hwSession.getUsers(sessionInfoXML, hwFilterUser.toByteArray());
                    result = getTheMembersInfo(resultStream);
                    resultStream.close();
                    result.put("result", true);
                }
            } else {
                throw new MessageServiceException("Need parameter SERVER_IP");
            }
        } catch (Exception var22) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var22);
            var22.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var22.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "getBPMUsers", System.currentTimeMillis() - timeS);
        }
        return result;
    }

    private static JSONObject getTheMembersInfo(InputStream is) {
        JSONObject result = null;

        try {
            XMLResultSet xrs = new XMLResultSetImpl();
            xrs.parse(is);
            int rowCount = xrs.getRowCount();

/*            for(int i = 0; i < rowCount; ++i) {
                System.out.println("--------------------- " + (i + 1) + "/" + rowCount + " ---------------------------");
                System.out.println("    User: " + xrs.getFieldValueAt(i, "NAME"));
                System.out.println("    EMAIL: " + xrs.getFieldValueAt(i, "EMAIL"));
                System.out.println("    EMPLOYEENUMBER: " + xrs.getFieldValueAt(i, "EMPLOYEENUMBER"));
                System.out.println("    DEPTID: " + xrs.getFieldValueAt(i, "DEPTID"));
                System.out.println("    JOBTITLENAME: " + xrs.getFieldValueAt(i, "JOBTITLENAME"));
                System.out.println("    DEPTNAME: " + xrs.getFieldValueAt(i, "DEPTNAME"));
                System.out.println("    SHORTNAME: " + xrs.getFieldValueAt(i, "SHORTNAME"));
                System.out.println("    LOGINID: " + xrs.getFieldValueAt(i, "LOGINID"));
                System.out.println("    PASSWORD: " + xrs.getFieldValueAt(i, "PASSWORD"));
            }*/

            JSONObject recJson = JSONUtil.xmlToJsonObject(xrs.toString());
            if (recJson != null && recJson.has("HWRECORDSET")) {
                result = recJson.getJSONObject("HWRECORDSET").getJSONObject("HWRECBODY");
                result.put("COUNT", rowCount);
            }

            return result;
        } catch (Exception var8) {
            var8.printStackTrace();
            return result;
        } finally {
            ;
        }
    }

    public static Object getBPMUserInfo(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        String serverIP = null;
        int serverPort = 7201;
        String loginID = null;
        String systemUserID = null;
        String systemUserPW = null;
        String var12 = "9000000000";

        try {
            logger.debug("BPMUserManage.getBPMUsers Started!");
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            if (jsonParam.has("SERVER_IP") && !jsonParam.isNull("SERVER_IP")) {
                serverIP = jsonParam.getString("SERVER_IP");
            } else {
                serverIP = BizFlowServiceConfigUtil.getBizFlowServer();
            }

            if (jsonParam.has("SERVER_PORT") && !jsonParam.isNull("SERVER_PORT")) {
                serverPort = jsonParam.getInt("SERVER_PORT");
            }

            loginID = jsonParam.getString("LOGINID");
            systemUserID = CustomServiceConfigUtil.getProperty("bpm.system.user.id", "administrator");
            systemUserPW = CustomServiceConfigUtil.getProperty("bpm.system.user.pw", "");
            logger.debug("encoded systemUserPW= %s", systemUserPW);
            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);
            logger.debug("decoded systemUserPW= %s", systemUserPW);
            if (serverIP != null && !serverIP.equals("")) {
                if (!checkDomainValidate(serverIP)) {
                    throw new MessageServiceException("SERVER_IP value is not valid.");
                } else {
                    if (serverPort < 1) {
                        serverPort = 7201;
                    }

                    if (loginID != null && !loginID.equals("")) {
                        HWSessionFactory hwSessionFactory = new HWSessionFactory();
                        HWSessionFactory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
                        HWSession hwSession = hwSessionFactory.newInstance();
                        hwSession.setSSLConnection(true);
                        String sessionInfoXML = hwSession.logIn(serverIP, serverPort, systemUserID, systemUserPW, true);
                        HWSessionInfo hwSessionInfo = new HWSessionInfo();
                        hwSessionInfo.reset(sessionInfoXML);
                        HWFilter hwFilterUser = new HWFilter();
                        hwFilterUser.setName("HWUser");
                        hwFilterUser.addFilter("LOGINID", "E", loginID);
                        InputStream resultStream = hwSession.getUsers(sessionInfoXML, hwFilterUser.toByteArray());
                        result = getTheMembersInfo(resultStream);
                        if (result != null && result.has("HWUSER") && !result.isNull("HWUSER")) {
                            result = result.getJSONObject("HWUSER");
                            result.put("result", true);
                        }

                        HWAuthorityGroupsImpl hwAuthGroups = new HWAuthorityGroupsImpl(sessionInfoXML, true);
                        String userLicenseInfo = getUserLicenses(hwAuthGroups);
                        String userAuthorityInfo = getUserAuthorites(hwAuthGroups);
                        resultStream.close();
                    } else {
                        throw new MessageServiceException("Need parameter LOGINID");
                    }
                }
            } else {
                throw new MessageServiceException("Need parameter SERVER_IP");
            }
        } catch (Exception var26) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), var26);
            var26.printStackTrace();
            result = new JSONObject();
            result.put("result", "false");
            result.put("faultString", var26.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "addBPMUser", System.currentTimeMillis() - timeS);
        }
        return result;
    }

    private static String getUserLicenses(HWAuthorityGroupsImpl hwAuthGroups) throws HWException {
        char[] userLicenseChar = new char[]{'0', '0', '0', '0', '0', '0', '0', '0', '0'};
        String result = "";
        if (hwAuthGroups.getCount() < 1) {
            return "";
        } else {
            int authCount = hwAuthGroups.getCount();

            int i;
            for(i = 0; i < authCount; ++i) {
                String lid = hwAuthGroups.getItem(i).getLicenseID();
                String last2Str = lid.substring(8);
                int licenseSeq = Integer.parseInt(last2Str);
                userLicenseChar[licenseSeq - 1] = '1';
                printSelectedFunctions(hwAuthGroups.getItem(i).getFunctions());
            }

            for(i = 0; i < userLicenseChar.length; ++i) {
                result = result + userLicenseChar[i];
            }

            return result;
        }
    }

    private static String getUserAuthorites(HWAuthorityGroupsImpl hwAuthGroups) {
        char[] userAuthChar = new char[]{'0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};
        String result = "";
        if (hwAuthGroups.getCount() < 1) {
            return "";
        } else {
            int authCount = hwAuthGroups.getCount();

            int i;
            for(i = 0; i < authCount; ++i) {
                String aid = hwAuthGroups.getItem(i).getID();
                String last2Str = aid.substring(8);
                int licenseSeq = Integer.parseInt(last2Str);
                userAuthChar[licenseSeq - 1] = '1';
            }

            for(i = 0; i < userAuthChar.length; ++i) {
                result = result + userAuthChar[i];
            }

            return result;
        }
    }

    private static void printSelectedFunctions(HWFolderMembers functions) {
        logger.debug("GroupFunctions=");

        for(int i = 0; i < functions.getCount(); ++i) {
            HWFolderMemberImpl m = (HWFolderMemberImpl)functions.getItem(i);
            if (null != m.getDisplayResID()) {
                logger.debug("getDisplayRESID() %s", m.getDisplayResID());
            }
        }

    }

    public static void main(String[] args) {
        String inputJson = "{\"SERVER_IP\":\"bizcloud.bizflow.com\",\"SERVER_PORT\":\"7201\",\"LOGINID\":\"hskim_01\",\"PASSWORD\":\"xxxx\",\"NAME\":\"Hyunsung Kim\",\"SHORTNAME\":\"Harris\",\"EMAIL\":\"hkim@bizflow.com\",\"EMPLOYEENUMBER\":\"BIZ_01\",\"AUTH_FLAG\":\"111000001\"}";
    }
}
