package com.bizflow.io.custom.services.train.util;

import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.hs.bf.web.beans.HWException;
import com.hs.bf.web.beans.HWSession;
import com.hs.bf.web.beans.HWSessionFactory;
import com.hs.bf.web.beans.HWString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;

public class VPAuthUtil {
    private VPAuthUtil() {
        throw new IllegalStateException("VPAuthUtil class");
    }

    private static final Logger logger = LogManager.getLogger(VPAuthUtil.class);

    public static JSONObject encryptPwd(HashMap parameter) {
        JSONObject result = new JSONObject();
        HWString encryptedPassword = null;
        String userId = null;
        String password = null;

        try {
            JSONObject jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            if (jsonParam.has("userId") && !jsonParam.isNull("userId")) {
                userId = jsonParam.getString("userId");
            }

            if (jsonParam.has("password") && !jsonParam.isNull("password")) {
                password = jsonParam.getString("password");
            }

            HWSessionFactory hwSessionFactory = new HWSessionFactory();
            HWSessionFactory.setDefaultImplClassName("com.hs.bf.web.beans.HWSessionTCPImpl");
            HWSession hwSession = hwSessionFactory.newInstance();
            encryptedPassword = new HWString();
            hwSession.encryptString(password, encryptedPassword);
            if (logger.isDebugEnabled()) {
                logger.debug("===========================================");
                logger.debug("userId: %s", userId);
                logger.debug("password: %s", password);
                logger.debug("encryptedPassword: %s", encryptedPassword.getValue());
            }

            result.put("encryptedPassword", encryptedPassword.getValue());
        } catch (HWException var8) {
            logger.error("{}: parameter={} %s", RequestIdentifierKeeper.getRequestIdLogString(), parameter.toString(), var8);
            result.put("result", "false");
            result.put("faultString", var8.getMessage());
        } catch (IOException var9) {
            logger.error("{}: parameter={} %S", RequestIdentifierKeeper.getRequestIdLogString(), parameter.toString(), var9);
            result.put("result", "false");
            result.put("faultString", var9.getMessage());
        }

        return result;
    }
}
