package com.bizflow.io.custom.services.bizcloud.license;

public interface CodeCrypt {
	public abstract String Encode(String szSrc) throws Exception;
	public abstract String Decode(String szSrc) throws Exception;
}
