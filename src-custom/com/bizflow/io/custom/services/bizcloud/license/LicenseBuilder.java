package com.bizflow.io.custom.services.bizcloud.license;

import java.io.StringWriter;

/**
 * License Generation class
 * 
 * 
 * Lecense Generation Method
 * <p>
 * 1. Shuffling source string
 * <blockquote><pre>
 *      The length of Source should be odd number. 
 *      If it is even number, it will append one line sperator, and make the length odd number.
 *      Check shuffle()
 * </pre></blockquote>
 * <p>
 * 2. encrypt key
 * <p>
 * 
 * 
 * @version 3.0, 01/16/02
 * @author  Park, Chan Hoon
 */


public class LicenseBuilder
{
	private LicenseSourceInfo licSource;
	
	public LicenseBuilder(LicenseSourceInfo licSource)
	{
		this.licSource = licSource;
	}
	
	public String buildLicense()
	{
		int srcLength;
		
		String licInfoText = licSource.getLicenseSource();
		String licCreated = null;
		// *. Normalize
		// In the new LicenseGenerator, there would be no normalization on input field.
		// It will just take the string as it is.
		
		// *. ClearKey
		// No clear key generation
		
		HDIdeaCipher cipherer = new HDIdeaCipher();
		String encrpytedString = cipherer.encryptKeyShuffled(licInfoText);
		
		licCreated = hexEncode(encrpytedString);

		return licCreated;
	}
	
	public void translateLicense(String rawLicenseData) throws Exception 
	{
		String hexDecoded = hexDecode(rawLicenseData);
		
		HDIdeaCipher cipherer = new HDIdeaCipher();
		String decrypted = cipherer.decryptKeyShuffled(hexDecoded);
		
		licSource.setLicenseSource(decrypted);
	}

	private String hexEncode(String strSource)
	{
		int nSize = strSource.length();
		char[] charArray = new char[nSize];
		strSource.getChars(0, nSize, charArray, 0);

		StringWriter writer = new StringWriter();
		int mark = 0;
		for(int i = 0;i < nSize; i++) 
		{
			int t = (charArray[i] & 0xff) ;
			String hexString = Integer.toHexString(t).toUpperCase();
			if(hexString.length() == 1) 
			{
				writer.write("0");
			}
			writer.write(hexString);
			mark++;

			if(mark == 4 && i < nSize - 1) 
			{
				writer.write("-");
				mark = 0;
			}
		}
		return writer.toString();		
	}
	
	private String hexDecode(String text) throws InvalidLicenseSourceException
	{
		StringWriter writer = new StringWriter();
		int mark = 0;
		int t1, t2;
		try 
		{
			for(int i=0;i<text.length();)
			{
				if(text.charAt(i) == '-')
				{
					i++;
					continue;
				}
				String str = text.substring(i, i + 2).toLowerCase();
				int x = Integer.parseInt(str, 16);
				byte b = new Integer(x).byteValue();
				char c = (char)b;
				writer.write(c);
				i += 2;

			}
		}
		catch(NumberFormatException nfe)
		{
			nfe.printStackTrace();			
			throw new InvalidLicenseSourceException("license is corrupt");

		}
		return writer.toString();

	}
}
