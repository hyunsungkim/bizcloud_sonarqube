package com.bizflow.io.custom.services.bizcloud.license;

import com.bizflow.io.core.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

public class LicenseManager {
	private static final Logger logger = LogManager.getLogger(LicenseManager.class);

	public  String licFileRepository = "";
	private String ENV_PROPERTIES_NAME = "env.properties";
	private static String OS = System.getProperty("os.name").toLowerCase();
	private String licenseDataFile = ""; // bfLicData.json
	private String licenseTemplateFile = ""; // bfLicTemplate.lic

	public LicenseManager() {
		try {
			loadProperties();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public LicenseManager(String licenseTemplateFile, String licFileRepository) {
		try {
			this.licenseTemplateFile = licenseTemplateFile;
			this.licFileRepository = licFileRepository;
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public LicenseManager(String licFilePath) {
		try {
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	private void loadProperties() throws Exception{
		InputStream input = LicenseManager.class.getClassLoader().getResourceAsStream(ENV_PROPERTIES_NAME) ;
		Properties prop = new Properties();

		// load a properties file
		prop.load(input);

		licenseDataFile = prop.getProperty("license.info.file.name");
		licenseTemplateFile = prop.getProperty("license.template.file.name");

		if(OS.indexOf("win") >= 0){
			licFileRepository = prop.getProperty("admin.on.window.file.repository");
		} else if(OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0){
			licFileRepository = prop.getProperty("admin.on.linux.file.repository");
		} else {
			throw new Exception("What type of os is this? OS=" + OS);
		}
	}

	private JSONObject loadLicenseInfoJSON() throws Exception
	{
//		InputStream input = LicenseGen.class.getClassLoader().getResourceAsStream("config.properties")) {
		StringBuffer text = new StringBuffer();

		String lineRead = null;
		FileReader fr = new FileReader(licenseDataFile);
		BufferedReader bufReader = new BufferedReader(fr);

		while((lineRead = bufReader.readLine()) != null)
		{
			text.append(lineRead);
			text.append("\n");
		}

		bufReader.close();
		JSONObject json = new JSONObject(text.toString());

		return json;
	}

	private String loadLBfTemplate() throws Exception
	{
//		InputStream input = LicenseGen.class.getClassLoader().getResourceAsStream("config.properties")) {

		StringBuffer text = new StringBuffer();

		String lineRead = null;
		FileReader fr = new FileReader(licenseTemplateFile);
		BufferedReader bufReader = new BufferedReader(fr);

		while((lineRead = bufReader.readLine()) != null)
		{
			text.append(lineRead);
			text.append("\n");
		}

		return text.toString();
	}

	private boolean runApplication() throws Exception
	{
		try
		{
			String licTemplateStr = loadLBfTemplate();
			JSONObject json = loadLicenseInfoJSON();
			JSONObject serverJson = json.getJSONObject("Server");
			JSONObject userLicenseJson = json.getJSONObject("User License");
			licTemplateStr = updateJsonToTemplate(licTemplateStr, serverJson);
			licTemplateStr = updateJsonToTemplate(licTemplateStr, userLicenseJson);
			if(licTemplateStr.indexOf("=%") > -1){
				throw new Exception("Not enough information to create license file. :" + getErrorKey(licTemplateStr,"=%"));
			}

			String encodedLicense = createEncodedLicense(licTemplateStr);

			String fullFilePath = licFileRepository + File.separator + getLicenseFileName(serverJson);
			return createLicenseFile(new File(fullFilePath), encodedLicense);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
	}

	public String generateLicenseFile(JSONObject json) throws Exception
	{
		boolean isCreated = false;
		try
		{
			String licTemplateStr = loadLBfTemplate();

			JSONObject serverJson = json.getJSONObject("Server");
			JSONObject userLicenseJson = json.getJSONObject("User License");
			licTemplateStr = updateJsonToTemplate(licTemplateStr, serverJson);
			licTemplateStr = updateJsonToTemplate(licTemplateStr, userLicenseJson);
			if(licTemplateStr.indexOf("=%") > -1){
				throw new Exception("Not enough information to create license file. :" + getErrorKey(licTemplateStr,"=%"));
			}
			String encodedLicense = createEncodedLicense(licTemplateStr);
			String fullFilePath = licFileRepository + File.separator + getLicenseFileName(serverJson);
			File f = new File(fullFilePath);
			isCreated = createLicenseFile(f, encodedLicense);
			if(isCreated != true){
				throw new Exception("Can't create license file");
			}
			f.setReadable(true,false);
			if(!".".equals(licFileRepository))
				return f.getAbsolutePath();
			else
				return f.getAbsolutePath();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	private String getErrorKey(String licData, String searchDelim) throws Exception {
		BufferedReader reader = new BufferedReader(new StringReader(licData));
		String line = reader.readLine();
		while(line != null){
			if(line.indexOf(searchDelim) > -1)
				return line.substring(0,line.indexOf(searchDelim));
			line = reader.readLine();
		}

		return "something wrong!";
	}

	// (2020_08_08)_0000000000_ABC Corp._(2011_01_01).lic
	// (CRT_DATE)_LICENSE_NO_COMPANY_NAME_(EXPIRATION_DATE).lic
	private String getLicenseFileName(JSONObject infoJson) throws Exception
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String CRT_DATE = df.format(new Date());

		String LICENSE_NO = infoJson.getString("LICENSE_NO");
		String COMPANY_NAME = infoJson.getString("COMPANY_NAME");
		COMPANY_NAME = COMPANY_NAME.replaceAll("[^a-zA-Z0-9\\.\\-]", "_");

		String EXPIRATION_DATE = infoJson.getString("EXPIRATION_DATE");
		EXPIRATION_DATE = EXPIRATION_DATE.replace("/","_");
		EXPIRATION_DATE = EXPIRATION_DATE.replace("/","_");EXPIRATION_DATE = EXPIRATION_DATE.replace("/","_");

		String physicalFileName = "(" + CRT_DATE + ")_"+ LICENSE_NO + "_" + COMPANY_NAME + "_(" + EXPIRATION_DATE + ").lic";
//		String physicalFileName = "license.lic";
		return physicalFileName;
	}

	private String createEncodedLicense(String updatedlicStr) throws Exception
	{
		// generate license from loaded text
		String encodedLicStr = null;
		LicenseSourceInfo source = new LicenseSourceInfo();
		source.setLicenseSource(updatedlicStr);
		try
		{
			source.validateLicenseSource();

			LicenseBuilder builder = new LicenseBuilder(source);
			encodedLicStr = builder.buildLicense();

			logger.debug("generated length : %s", encodedLicStr.length());
		}
		catch(InvalidLicenseSourceException ex)
		{
			logger.error("Invalid license input text: %n", ex.toString());
		}
		return encodedLicStr;
	}

	private String updateJsonToTemplate(String licTemplate, JSONObject targetJson) throws Exception
	{
		Iterator<String> iter = targetJson.keys();
		while(iter.hasNext()){
			String theKey = iter.next();
			Object valueObj = targetJson.get(theKey);
			String theValue = null;
			if(valueObj instanceof String){
				theValue = targetJson.getString(theKey);
			} else if(valueObj instanceof Integer){
				theValue = "" + targetJson.getInt(theKey);
			} else {
				throw new Exception("The value of '" + theKey + "' is not supported:" + valueObj);
			}

//System.out.println("theKey="+theKey);
//System.out.println("theValue="+theValue);
			String keyInLic = "%" + theKey + "%";
			licTemplate = licTemplate.replaceAll(keyInLic, theValue);
		}
		return licTemplate;
	}

	private boolean createLicenseFile(File file, String licGenerated) throws Exception
	{
		/*FileOutputStream fosLicense = null;
		PrintWriter posLicense = null;*/

		try(FileOutputStream fosLicense = new FileOutputStream(file);
			 PrintWriter posLicense = new PrintWriter(fosLicense))
		{
			if(file.exists()){
				throw new Exception("This license File already exists! fileName="+ file.getAbsolutePath());
			}
			posLicense.write(licGenerated);

			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
	}

	public String displayFromLicenseFile(File file) throws Exception
	{
		logger.debug("FILE PATH : {}", file.getAbsolutePath());

		String licenseLoaded = null;
		String decodedLicenseData = null;

		if (file.length() == 0)
			throw new Exception("The License file is null.");

		try(FileReader reader = new FileReader(file))
		{
			char buffer[] = new char[1024];
			StringBuffer tmp = new StringBuffer();
			int read = 0;
			while((read = reader.read(buffer)) > 0)
			{
				tmp.append(buffer, 0, read);
			}
			licenseLoaded = tmp.toString();
//System.out.println("license loaded length = " + licenseLoaded.length());
		}
		catch (IOException ignore)
		{
			logger.error("ERROR : %s", ignore.getMessage());
		}
		/*finally
		{
			try
			{
				if(reader != null) reader.close();
			}
			catch (IOException ignore2)
			{}
		}*/
		// get license information
		try
		{
			LicenseSourceInfo sourceInfo = new LicenseSourceInfo();
			LicenseBuilder builder = new LicenseBuilder(sourceInfo);
			logger.debug("Raw data length : %s" , licenseLoaded.length());
			builder.translateLicense(licenseLoaded);

			sourceInfo.validateLicenseSource();
			decodedLicenseData = sourceInfo.getLicenseSource();

			logger.debug("decodedLicenseData : %s", decodedLicenseData);
		}
		catch(Exception ex)
		{
			logger.error("ERROR : %s", ex.getMessage());
		}
		return decodedLicenseData;
	}

	public static void main(String[] args)
	{
		LicenseManager licenseGen = new LicenseManager();
		try {
			if (!licenseGen.runApplication()) {
				logger.debug("Something is wrong!");
				System.exit(-1);
			}
			logger.debug("License File Generation OK!");
		} catch (Exception e){
			logger.error("ERROR : %s", e.getMessage());
		}
	}
}
