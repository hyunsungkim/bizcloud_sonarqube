package com.bizflow.io.custom.services.bizcloud.license;

/**
 * License source info class.
 * This class will validate license source information
 * 
 * 
 * @version 3.0, 01/16/02
 * @author  Park, Chan Hoon
 */

public class LicenseSourceInfo
{
	private String licSource;
	private boolean validated = false;
	
	public LicenseSourceInfo()
	{
		
	}
	
	public LicenseSourceInfo(String source)
	{
		licSource = source;	
	}
	
	public void validateLicenseSource() throws InvalidLicenseSourceException
	{
		//todo validation
		// MAX_USER, EXPIRATION_DATE and IP can��t be unlimited concurrently.
		//Super key (which has unlimited MAX_USER, EXPIRATION_DATE, IP) should not be created.
		
		

		
		
		validated =true;
	}
	
	
	private String getPropertyFromInfo(String key)
	{
		
		return null;
	}
	
	public String getLicenseSource() 
	{
		return licSource;	
	}
	
	public void setLicenseSource(String newSource)
	{
		licSource = newSource;		
	}
	
	public String toString()
	{
		return licSource;	
	}
}
