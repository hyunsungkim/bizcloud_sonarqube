package com.bizflow.io.custom.services.bizcloud.license;

import java.io.File;

 public class LicenseFilterUtils {
     private LicenseFilterUtils() {
         throw new IllegalStateException("LicenseFilterUtils class");
     }

    public final static String LICENSE = "license";
	public final static String LIC = "lic";

    /*
     * Get the extension of a file.
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
}
