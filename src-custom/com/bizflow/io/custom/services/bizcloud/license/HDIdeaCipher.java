package com.bizflow.io.custom.services.bizcloud.license;

import com.bizflow.io.custom.services.bizcloud.aws.ssm.SSM;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HDIdeaCipher
{
	private static final Logger logger = LogManager.getLogger(HDIdeaCipher.class);

	private static final int CIPHERBLOCKLEN = 8;
	private static final int CIPHERKEYLEN = 16;
	private static final int CIPHERENCODINGBLOCKLEN = 11;

	private static int[] encryptKeys;
	private static int[] decryptKeys;

	static
	{
		decryptKeys = new int[52];
		encryptKeys = new int[52];
		//setKey("jihgfedcba");			//New York
		//setKey("90-R&DLKcS");			//Paris
		//setKey("901R&DLKcS");			//Riviera
		//setKey("Seoul-lkCs");			//Seoul
		setKey("11CoRioLis");			//Coriolis
	}

    public static void setKey( byte[] key )
    {
		int k1, k2, j;
		int t1, t2, t3;
	
		for (k1 = 0; k1 < 8; ++k1)
		{
		    encryptKeys[k1] = ((key[2 * k1] & 0xff) << 8) | (key[ 2 * k1 + 1] & 0xff);
		}
		
		int offset = 0;
	    
	    for (int i = 0; k1 < 52; k1++)
	    {
	        i++;
	        encryptKeys[i+7+offset] = (encryptKeys[(i & 7) + offset] << 9 | encryptKeys[(i+1 & 7) + offset] >> 7) & 0xffff;
	        offset += i & 8;
	        i &= 7;
	    }
	
		k1 = 0;
		k2 = 51;
		t1 = mulinv( encryptKeys[k1++] );
		t2 = -encryptKeys[k1++] & 0x0000ffff;
		t3 = -encryptKeys[k1++] & 0x0000ffff;
		decryptKeys[k2--] = mulinv( encryptKeys[k1++] );
		decryptKeys[k2--] = t3;
		decryptKeys[k2--] = t2;
		decryptKeys[k2--] = t1;
	
		for (j = 1; j < 8; ++j)
		{
		    t1 = encryptKeys[k1++];
		    decryptKeys[k2--] = encryptKeys[k1++];
		    decryptKeys[k2--] = t1;
		    t1 = mulinv( encryptKeys[k1++] );
			t2 = -encryptKeys[k1++] & 0x0000ffff;
		    t3 = -encryptKeys[k1++] & 0x0000ffff;
		    decryptKeys[k2--] = mulinv( encryptKeys[k1++] );
		    decryptKeys[k2--] = t2;
		    decryptKeys[k2--] = t3;
		    decryptKeys[k2--] = t1;
		}
	
		t1 = encryptKeys[k1++];
		decryptKeys[k2--] = encryptKeys[k1++];
		decryptKeys[k2--] = t1;
		t1 = mulinv( encryptKeys[k1++] );
		t2 = -encryptKeys[k1++] & 0x0000ffff;
		t3 = -encryptKeys[k1++] & 0x0000ffff;
		decryptKeys[k2--] = mulinv( encryptKeys[k1++] );
		decryptKeys[k2--] = t3;
		decryptKeys[k2--] = t2;
		decryptKeys[k2--] = t1;
	}

    private int[] tempShorts = new int[4];
    
	private byte[] encryptBlock(byte[] clearText)
	{
		byte[] cipherText = new byte[CIPHERBLOCKLEN + 1];
		for(int i=0;i<CIPHERBLOCKLEN + 1;i++) cipherText[i]=(byte)0;

		squashBytesToShorts( clearText, 0, tempShorts, 0, 4 );
		idea( tempShorts, tempShorts, encryptKeys );
		spreadShortsToBytes( tempShorts, 0, cipherText, 0, 4 );

		return encoding(cipherText);
	}

	private byte[] decryptBlock(byte[] cipherTextStr)
	{
		byte[] cipherText = decoding(cipherTextStr);
		byte[] clearText = new byte[CIPHERBLOCKLEN];

		squashBytesToShorts( cipherText, 0, tempShorts, 0, 4 );
		idea( tempShorts, tempShorts, decryptKeys );
		spreadShortsToBytes( tempShorts, 0, clearText, 0, 4 );

		int i = 0;
		for(; i < CIPHERBLOCKLEN ; i++ )
		{
			if(clearText[i] == 0) break;			
		}
		
		byte[] returnByte = new byte[i];
		System.arraycopy(clearText, 0, returnByte, 0, i);
		
		return returnByte;
	}

	private byte[] encoding(byte[] text)
	{
		byte[] ret = new byte[CIPHERENCODINGBLOCKLEN];

		int shift = 2;
		int j = 0;
		int i = 0;
		ret[j] = (byte)( ( ( text[i] & 0xff )  >>> shift ) + (byte) 48 );
		i++;

		for(j=1; j<CIPHERENCODINGBLOCKLEN; j++) 
		{
			shift += 2;

			if(shift > 8)
			{
				shift = 2;
				i--;
			}

			ret[j] = (byte)(((((text[i - 1] & 0xff) << (8 - shift)) | ((text[i] & 0xff) >>> shift)) & 0x3f));
			ret[j] += (byte) 48;
			i++;
		}
		
		return ret;
	}

	private byte[] decoding(byte[] text)
	{
		byte[] ret = new byte[CIPHERBLOCKLEN];

		for(int l=0;l<CIPHERENCODINGBLOCKLEN;l++) {
			text[l] = (byte)(text[l] - (byte)48);
		}
		
		int i = 0;
		int shift = 0;
		for(int j=0;j<CIPHERBLOCKLEN;j++) {
			shift += 2;
			
			if(shift > 7)
			{
				shift = 2;
				i++;
			}

			ret[j] = (byte)((text[i] << shift) | (text[i + 1] >>> (6 - shift)) );
			i++;
		}
		
		return ret;
	}

	private void shuffle(byte[] charArray)
	{
		byte tmp;
		int nRIndex = 0;
		int nSourceLen = charArray.length;
		
		for (int i = 0; i < nSourceLen / 2; i++)
		{
			tmp = charArray[i];
			nRIndex = nSourceLen - i - 1;
			charArray[i] = charArray[nRIndex];
			charArray[nRIndex] = tmp;
		}
	}

    private void idea( int[] inShorts, int[] outShorts, int[] keys )
	{
		int x1, x2, x3, x4, k, t1, t2, t3;
	
		x1 = inShorts[0];
		x2 = inShorts[1];
		x3 = inShorts[2];
		x4 = inShorts[3];
	
		k = 0;
		for (int round = 0; round < 8; ++round)
		{
		    x1 = mul( x1 & 0xffff, keys[k++] );
		    x2 = (x2 + keys[k++]) & 0xffff;
		    x3 = (x3 + keys[k++]) & 0xffff;
		    x4 = mul( x4 & 0xffff, keys[k++] );
	
			t3 = x3;
			x3 = x3 ^ x1;
			x3 = mul(x3 & 0xffff, keys[k++]);
			t2 = x2;
			x2 = x2 ^ x4;
			x2 = (x2 + x3) & 0xffff;
			x2 = mul(x2 & 0xffff, keys[k++]);
			x3 = (x2 + x3) & 0xffff;

			x1 = x1 ^ x2;
			x4 = x4 ^ x3;
			x2 = x2 ^ t3;
			x3 = x3 ^ t2;
	    }

		outShorts[0] = mul( x1 & 0xffff, keys[k++] ) & 0xffff;
		outShorts[1] = ( x3 + keys[k++] ) & 0xffff;
		outShorts[2] = ( x2 + keys[k++] ) & 0xffff;
		outShorts[3] = mul( x4 & 0xffff, keys[k++] ) & 0xffff;
	}

    private static int mul( int a, int b )
	{
		int ab = a * b;
		if ( ab != 0 )
		    {
		    int lo = ab & 0xffff;
		    int hi = ab >>> 16;
		    return ( ( lo - hi ) + ( lo < hi ? 1 : 0 ) ) & 0xffff;
		    }
		if ( a != 0 )
		    return ( 1 - a ) & 0xffff;
		return ( 1 - b ) & 0xffff;
	}
    
    private static int mulinv( int x )
	{
		int t0, t1, q, y;

		if ( x <= 1 )
		    return x;		// 0 and 1 are self-inverse

		t0 = 1;
		t1 = 0x10001 / x;	// since x >= 2, this fits into 16 bits
		y = ( 0x10001 % x ) & 0xffff;
		for (;;)
	    {
		    if (y == 1)
				return (1 - t1) & 0xffff;
				
		    q = x / y;
		    x = x % y;
		    t0 = ( t0 + q * t1 ) & 0xffff;
		    
		    if ( x == 1 )
				return t0;

		    q = y / x;
		    y = y % x;
		    t1 = ( t1 + q * t0 ) & 0xffff;
	    }
	}

    public static void setKey(String keyStr)
	{
		byte[] key = new byte[CIPHERKEYLEN];
		byte[] suppliedKey = keyStr.getBytes();
		int len = suppliedKey.length;
		
		if(len > CIPHERKEYLEN)
			len = CIPHERKEYLEN;
		
		System.arraycopy(suppliedKey, 0, key, 0, len);
		
		for(int i=len;i<CIPHERKEYLEN;i++)
		{
			key[i] = (byte)0;
		}

		setKey( key );
	}

	private void squashBytesToShorts( byte[] inBytes, int inOff, int[] outShorts, int outOff, int shortLen )
	{
		for (int i = 0; i < shortLen; ++i)
		{
	    	outShorts[outOff + i] = ((inBytes[inOff + i * 2 + 1 ] & 0xff) << 8) | (inBytes[inOff + i * 2] & 0xff);
	    }
	}

    private static void spreadShortsToBytes(int[] inShorts, int inOff, byte[] outBytes, int outOff, int shortLen)
	{
		for ( int i = 0; i < shortLen; ++i )
	    {
	    	outBytes[outOff + i * 2 + 1 ] = (byte) (( inShorts[inOff + i] >>> 8 ) & 0xff);
	    	outBytes[outOff + i * 2 ] = (byte)   (inShorts[inOff + i] & 0xff);
	    }
	}

	public String encrypt(String strSource)
	{
		byte[] byteSource = strSource.getBytes();
		
		int block = byteSource.length / 8;
		int remaining = byteSource.length % 8;
		int resultByteLength = block * 11;
		if (remaining > 0)
			resultByteLength += 11;
		
		byte[] resultByte = new byte[resultByteLength];
		
		for(int i=0;i<block;i++) 
		{
			byte[] partClearByte = new byte[8];
			System.arraycopy(byteSource, i*8, partClearByte, 0, 8);
			byte[] partCipherByte = encryptBlock(partClearByte);
			System.arraycopy(partCipherByte, 0, resultByte, i*11, 11);
		}
		
		if (remaining > 0)
		{
			byte[] partClearByte = new byte[8];
			System.arraycopy(byteSource, block*8, partClearByte, 0, remaining);
			for (int i = remaining; i < 8; i++)
				partClearByte[i] = (byte) 0;
			
			byte[] partCipherByte = encryptBlock(partClearByte);
			System.arraycopy(partCipherByte, 0, resultByte, block*11, 11);
		}

		return new String(resultByte);
	}
	
	public String encryptKeyShuffled(String strSource)
	{
		byte[] byteSource = strSource.getBytes();
		shuffle(byteSource);
		
		int block = byteSource.length / 8;
		int remaining = byteSource.length % 8;
		int resultByteLength = block * 11;
		if (remaining > 0)
			resultByteLength += 11;
		
		byte[] resultByte = new byte[resultByteLength];
		
		for(int i=0;i<block;i++) 
		{
			byte[] partClearByte = new byte[8];
			System.arraycopy(byteSource, i*8, partClearByte, 0, 8);
			byte[] partCipherByte = encryptBlock(partClearByte);
			System.arraycopy(partCipherByte, 0, resultByte, i*11, 11);
		}
		
		if (remaining > 0)
		{
			byte[] partClearByte = new byte[8];
			System.arraycopy(byteSource, block*8, partClearByte, 0, remaining);
			for (int i = remaining; i < 8; i++)
				partClearByte[i] = (byte) 0;
			
			byte[] partCipherByte = encryptBlock(partClearByte);
			System.arraycopy(partCipherByte, 0, resultByte, block*11, 11);
		}

		return new String(resultByte);
	}
	
	public String decrypt(String strSource)
	{
		byte[] byteSource = strSource.getBytes();
		int block = byteSource.length / 11;
		byte[] resultByte = new byte[block*8];
		int finalLength = 0;

		for(int i=0;i<block;i++)
		{
			byte[] partCipherByte = new byte[11];
			System.arraycopy(byteSource, i*11, partCipherByte, 0, 11);
			byte[] partClearByte = decryptBlock(partCipherByte);
			System.arraycopy(partClearByte, 0, resultByte, i*8, partClearByte.length);
			finalLength += partClearByte.length;
		}
		
		byte[] returnByte = new byte[finalLength];
		System.arraycopy(resultByte, 0, returnByte, 0, finalLength);
		
		return new String(returnByte);
	}

	public String decryptKeyShuffled(String strSource)
	{
		byte[] byteSource = strSource.getBytes();
		int block = byteSource.length / 11;
		byte[] resultByte = new byte[block*8];
		int finalLength = 0;

		for(int i=0;i<block;i++)
		{
			byte[] partCipherByte = new byte[11];
			System.arraycopy(byteSource, i*11, partCipherByte, 0, 11);
			byte[] partClearByte = decryptBlock(partCipherByte);
			System.arraycopy(partClearByte, 0, resultByte, i*8, partClearByte.length);
			finalLength += partClearByte.length;
		}
		
		byte[] returnByte = new byte[finalLength];
		System.arraycopy(resultByte, 0, returnByte, 0, finalLength);
		shuffle(returnByte);
		
		return new String(returnByte);
	}

	private final static int ENCRYPTION_MODE = 0;
	private final static int DECRYPTION_MODE = 1;

	public String transform(int mode, String text) throws Exception {
		String result = null;
		try {
			switch(mode) {
				case ENCRYPTION_MODE :
					result = encrypt(text);
					break;
				case DECRYPTION_MODE :
					result = decrypt(text);
					break;
				default :
			}
		} catch(Exception e) {
			logger.error("ERROR : %s", e.getMessage());
		}
		return result;
	}
}
