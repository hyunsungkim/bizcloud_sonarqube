package com.bizflow.io.custom.services.bizcloud.license;

public interface Constants
{
	//MAX_USER, EXPIRATION_DATE and IP 
	public static final String MAX_USER = "MAX_USER";
	public static final String EXPIRATION_DATE = "EXPIRATION_DATE";
	public static final String IP = "IP";
		
}
