package com.bizflow.io.custom.services.bizcloud.license;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class LicenseFilter extends FileFilter {
    
    // Accept all directories and all gif, jpg, or tiff files.
    public boolean accept(File f) 
	{
        if (f.isDirectory())
		{
            return true;
        }

        String extension = LicenseFilterUtils.getExtension(f);
	if (extension != null)
	{
            if (extension.equals(LicenseFilterUtils.LICENSE)
				|| extension.equals(LicenseFilterUtils.LIC))
			{
                   
				return true;
            } else 
			{
                return false;
            }
    	}

        return false;
    }
    
    // The description of this filter
    public String getDescription() {
        return "BizFlow Cloud License File.(*.lic, *.license)";
    }
}
