package com.bizflow.io.custom.services.bizcloud.license;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;

public class HDIdeaCipherIni   implements CodeCrypt {
	private static final Logger logger = LogManager.getLogger(HDIdeaCipherIni.class);

	private static final int CIPHERBLOCKLEN = 8;
	private static final int CIPHERKEYLEN = 16;
	private static final int CIPHERENCODINGBLOCKLEN = 11;

	// key values
	private static int[] encryptKeys;
	private static int[] decryptKeys;


	// key initialization 
	static {
		decryptKeys = new int[52];
		encryptKeys = new int[52];
		setKey("90-R&DLKcS");

	}

    public static void setKey( byte[] key )	{
	int k1, k2, j;
	int t1, t2, t3;

	// Encryption keys.  The first 8 key values come from the 16
	// user-supplied key bytes.
	for ( k1 = 0; k1 < 8; ++k1 )
	    encryptKeys[k1] =
		( ( key[2 * k1] & 0xff ) << 8 ) | ( key[ 2 * k1 + 1] & 0xff );
/*
	// Subsequent key values are the previous values rotated to the
	// left by 25 bits.
	for ( ; k1 < 52; ++k1 )
	    encryptKeys[k1] =
		( ( encryptKeys[k1 - 8] << 9 ) |
		  ( encryptKeys[k1 - 7] >>> 7 ) ) & 0xffff;
*/
		int offset = 0;
    for (int i=0; k1<52; k1++)
    {
        i++;
        encryptKeys[i+7+offset] = (encryptKeys[(i & 7) + offset] << 9 | encryptKeys[(i+1 & 7) + offset] >> 7) & 0xffff;
        offset += i & 8;
        i &= 7;
    }

/*
	for(k1 = 0;k1 < 52; k1++) {
		System.out.println("ek[" + k1 + "] = " + encryptKeys[k1]);
	}
*/
	// Decryption keys.  These are the encryption keys, inverted and
	// in reverse order.
	k1 = 0;
	k2 = 51;
	t1 = mulinv( encryptKeys[k1++] );
	t2 = -encryptKeys[k1++] & 0x0000ffff;
	t3 = -encryptKeys[k1++] & 0x0000ffff;
	decryptKeys[k2--] = mulinv( encryptKeys[k1++] );
	decryptKeys[k2--] = t3;
	decryptKeys[k2--] = t2;
	decryptKeys[k2--] = t1;

	for ( j = 1; j < 8; ++j ) {
	    t1 = encryptKeys[k1++];
	    decryptKeys[k2--] = encryptKeys[k1++];
	    decryptKeys[k2--] = t1;
	    t1 = mulinv( encryptKeys[k1++] );
/*
	    t2 = -encryptKeys[k1++];
	    t3 = -encryptKeys[k1++];
*/
			t2 = -encryptKeys[k1++] & 0x0000ffff;
	    t3 = -encryptKeys[k1++] & 0x0000ffff;
	    decryptKeys[k2--] = mulinv( encryptKeys[k1++] );
	    decryptKeys[k2--] = t2;
	    decryptKeys[k2--] = t3;
	    decryptKeys[k2--] = t1;
  }

	t1 = encryptKeys[k1++];
	decryptKeys[k2--] = encryptKeys[k1++];
	decryptKeys[k2--] = t1;
	t1 = mulinv( encryptKeys[k1++] );
	t2 = -encryptKeys[k1++] & 0x0000ffff;
	t3 = -encryptKeys[k1++] & 0x0000ffff;
	decryptKeys[k2--] = mulinv( encryptKeys[k1++] );
	decryptKeys[k2--] = t3;
	decryptKeys[k2--] = t2;
	decryptKeys[k2--] = t1;
/*
	for(k1 = 0;k1 < 52; k1++) {
		System.out.println("dk[" + k1 + "] = " + decryptKeys[k1]);
	}
*/
	}


    // Block encryption routines.

	public String encrypt(String clearTextStr)
	{
		StringBuffer buffer = new StringBuffer();

		byte[]	clearTextBytes= clearTextStr.getBytes() ;
		// get loop count
		int blockLen = ( clearTextBytes.length - 1 ) / CIPHERBLOCKLEN + 1;
		for(int i = 0; i < blockLen; i++) 
		{
			if( i + 1 == blockLen )
			{
				buffer.append(encryptBlock(subBytes(clearTextBytes, i*CIPHERBLOCKLEN, 0 ) ));
			}
			else 
			{
				buffer.append(encryptBlock(subBytes(clearTextBytes, i*CIPHERBLOCKLEN , (i+1)*CIPHERBLOCKLEN)));
			}
		}
		return buffer.toString();
	}
	private byte[] subBytes ( byte[] byteArray , int nStart, int nEnd ) 
	{
		byte[]	retBytes	= null ;
		if ( nEnd == 0)
		{
			retBytes	= new byte [ byteArray.length - nStart ] ;
		}
		else
		{
			retBytes	= new byte [ nEnd - nStart ] ;
		}
		System.arraycopy(byteArray, nStart, retBytes, 0, retBytes.length);
		return retBytes ;
	}
	public String decrypt(String cipherTextStr) {
		byte[]	byteArray	= cipherTextStr.getBytes() ;
		ByteArrayOutputStream	bao	= new ByteArrayOutputStream () ;

		// get loop count
		int blockLen = byteArray.length / CIPHERENCODINGBLOCKLEN;
		byte[]	byteDecode	= null ;
		for(int i = 0; i < blockLen; i++) 
		{
			byteDecode	= decryptBlock(subBytes(byteArray,i*CIPHERENCODINGBLOCKLEN, (i + 1)*CIPHERENCODINGBLOCKLEN )) ;
			bao.write(byteDecode , 0 , byteDecode.length);
		}
		return bao.toString();		
	}

    private int[] tempShorts = new int[4];

	public String encryptBlock(byte[] clearTextBytes) {
		// make quadruple of clearTextStr
		byte[] clearText = makeCipherBlockBytes(clearTextBytes);
//		System.out.println ( "encryptBlock::clearTextBytes : " + new String ( clearTextBytes ) ) ;
		byte[] cipherText = new byte[CIPHERBLOCKLEN + 1];
		for(int i=0;i<CIPHERBLOCKLEN + 1;i++) cipherText[i]=(byte)0;

		squashBytesToShorts( clearText, 0, tempShorts, 0, 4 );
		idea( tempShorts, tempShorts, encryptKeys );
/*
		for(int i=0;i<4;i++) {
			System.out.println("tempShorts[" + i*2 + "] = " + (tempShorts[i] & 0xff));
			System.out.println("tempShorts[" + (i*2+1) + "] = " + (tempShorts[i] >>> 8));
		}
*/
		spreadShortsToBytes( tempShorts, 0, cipherText, 0, 4 );

		return encoding(cipherText);
	}

	public byte[] decryptBlock(byte[] cipherByte) {
		byte[] cipherText = decoding(cipherByte);
		byte[] clearText = new byte[CIPHERBLOCKLEN];

		squashBytesToShorts( cipherText, 0, tempShorts, 0, 4 );
		idea( tempShorts, tempShorts, decryptKeys );
/*
		for(int i=0;i<4;i++) {
			System.out.println("tempShorts[" + i*2 + "] = " + (tempShorts[i] & 0xff));
			System.out.println("tempShorts[" + (i*2+1) + "] = " + (tempShorts[i] >>> 8));
		}
*/
		spreadShortsToBytes( tempShorts, 0, clearText, 0, 4 );

		int i = 0;
		for(; i < CIPHERBLOCKLEN ; i++ ) {
			if(clearText[i] == 0) break;			
		}
		return clearText ;
	}

	/** 
	 *  allocate CIPHERBLOCKLEN bytes
	 **/
	private byte[] makeCipherBlockBytes(byte[] notQuadrupleTextBytes) {
		int len = notQuadrupleTextBytes.length;
		byte[] ret = new byte[CIPHERBLOCKLEN];	
		if(len >= CIPHERBLOCKLEN) len = CIPHERBLOCKLEN;
		System.arraycopy(notQuadrupleTextBytes, 0, ret, 0, len);
		for(int i=len;i<CIPHERBLOCKLEN;i++) {
			ret[i] = (byte)0;
		}
		return ret;
	}


	private String encoding(byte[] text) {
		byte[] ret = new byte[CIPHERENCODINGBLOCKLEN];

		int shift = 2;
		int j = 0;
		int i = 0;
		ret[j] = (byte)( ( ( text[i] & 0xff )  >>> shift ) + (byte) 48 );
		i++;

		for(j=1; j<CIPHERENCODINGBLOCKLEN; j++) {
			shift += 2;
			if(shift > 8) {
				shift = 2;
				i--;
			}
			ret[j] = (byte)( ( ( ( (text[i - 1] & 0xff ) << (8 - shift)) | ( ( text[ i ] & 0xff ) >>> shift ) ) & 0x3f));
			ret[j] += (byte) 48;
			i++;
		}
		String retStr = new String(ret);
		return retStr;
	}

	private byte[] decoding(byte[] textByte) {
		byte[] ret = new byte[CIPHERBLOCKLEN];
		byte[] text = textByte ;

		for(int l=0;l<CIPHERENCODINGBLOCKLEN;l++) {
			text[l] = (byte)(text[l] - (byte)48);
		}
		
		int i = 0;
		int shift = 0;
		for(int j=0;j<CIPHERBLOCKLEN;j++) {
			shift += 2;
			if(shift > 7) {
				shift = 2;
				i++;
			}
			ret[j] = (byte)( ( text[i] << shift ) | ( text[i+1] >>> ( 6-shift) ) );
			i++;
		}
		return ret;
	}

    // Run IDEA on one block.
    private void idea( int[] inShorts, int[] outShorts, int[] keys )
	{
		int x1, x2, x3, x4, k, t1, t2, t3;

		x1 = inShorts[0];
		x2 = inShorts[1];
		x3 = inShorts[2];
		x4 = inShorts[3];

		k = 0;
		for ( int round = 0; round < 8; ++round ) 
		{
				// algorithm changed by dbin to adjust to KOTRA encryption sources
			x1 = mul( x1 & 0xffff, keys[k++] );
			x2 = (x2 + keys[k++]) & 0xffff;
			x3 = (x3 + keys[k++]) & 0xffff;
			x4 = mul( x4 & 0xffff, keys[k++] );

				t3 = x3;
				x3 = x3 ^ x1;
				x3 = mul(x3 & 0xffff, keys[k++]);
				t2 = x2;
				x2 = x2 ^ x4;
				x2 = (x2 + x3) & 0xffff;
				x2 = mul(x2 & 0xffff, keys[k++]);
				x3 = (x2 + x3) & 0xffff;

				x1 = x1 ^ x2;
				x4 = x4 ^ x3;
				x2 = x2 ^ t3;
				x3 = x3 ^ t2;
		}
		// outShorts assignment ordering changed by dbin
		outShorts[0] = mul( x1 & 0xffff, keys[k++] ) & 0xffff;
		outShorts[1] = ( x3 + keys[k++] ) & 0xffff;
		outShorts[2] = ( x2 + keys[k++] ) & 0xffff;
		outShorts[3] = mul( x4 & 0xffff, keys[k++] ) & 0xffff;

	}

    // Multiplication modulo 65537.
    private static int mul( int a, int b )
	{
	int ab = a * b;
	if ( ab != 0 )
	    {
	    int lo = ab & 0xffff;
	    int hi = ab >>> 16;
	    return ( ( lo - hi ) + ( lo < hi ? 1 : 0 ) ) & 0xffff;
	    }
	if ( a != 0 )
	    return ( 1 - a ) & 0xffff;
	return ( 1 - b ) & 0xffff;
	}
    
    // The multiplicative inverse of x, modulo 65537.  Uses Euclid's
    // GCD algorithm.  It is unrolled twice to avoid swapping the
    // meaning of the registers each iteration, and some subtracts
    // of t have been changed to adds.
    private static int mulinv( int x )
	{
		int t0, t1, q, y;
		if ( x <= 1 )
			return x;		// 0 and 1 are self-inverse
		t0 = 1;
		t1 = 0x10001 / x;	// since x >= 2, this fits into 16 bits
		y = ( 0x10001 % x ) & 0xffff;
		for (;;)
		{
			if ( y == 1 )
				return ( 1 - t1 ) & 0xffff;
			q = x / y;
			x = x % y;
			t0 = ( t0 + q * t1 ) & 0xffff;
			if ( x == 1 )
				return t0;
			q = y / x;
			y = y % x;
			t1 = ( t1 + q * t0 ) & 0xffff;
	    }
	}

    public static void setKey( String keyStr )
	{
/*
		byte[] key = {(byte)97, (byte)98, (byte)99, (byte)100, (byte)101, (byte)102, (byte)103, (byte)104, (byte)105, (byte)106, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0};
*/

		byte[] key = new byte[CIPHERKEYLEN];
		byte[] suppliedKey = keyStr.getBytes();
		//System.out.println("supplied key len = " + suppliedKey.length);
		int len = suppliedKey.length;
		if(len > CIPHERKEYLEN) len = CIPHERKEYLEN;
		System.arraycopy(suppliedKey, 0, key, 0, len);
		for(int i=len;i<CIPHERKEYLEN;i++) {
			key[i] = (byte)0;
		}

		setKey( key );
	}

   private void squashBytesToShorts( byte[] inBytes, int inOff, int[] outShorts, int outOff, int shortLen )
	{
	for ( int i = 0; i < shortLen; ++i )
	    outShorts[outOff + i] =
		( ( inBytes[inOff + i * 2 + 1 ] & 0xff ) << 8 ) |
		  ( inBytes[inOff + i * 2 ] & 0xff );
	}

    private static void spreadShortsToBytes( int[] inShorts, int inOff, byte[] outBytes, int outOff, int shortLen )
	{
	for ( int i = 0; i < shortLen; ++i )
	    {
	    outBytes[outOff + i * 2 + 1 ] = (byte) (( inShorts[inOff + i] >>> 8 ) & 0xff);
	    outBytes[outOff + i * 2 ] = (byte)   (inShorts[inOff + i] & 0xff);
	    }
	}

	public String Encode(String clearText) throws Exception {
		try {
			return encrypt(clearText);
		} catch(Exception e) {
			throw e;
		}
	}

	public String Decode(String cipherText) throws Exception {
		try {
			return decrypt(cipherText);
		} catch(Exception e) {
			throw e;
		}
	}

	private final static int ENCRYPTION_MODE = 0;
	private final static int DECRYPTION_MODE = 1;
	public String transform(int mode, String text) throws Exception {
		String result = null;
		try {
			switch(mode) {
				case ENCRYPTION_MODE :
					result = encrypt(text);
					break;
				case DECRYPTION_MODE :
					result = decrypt(text);
					break;
				default :
			}
		} catch(Exception e) {
			logger.error("ERROR %s", e.getMessage());
		}
		return result;
	}

	public static void main(String[] args) throws Exception {
		if(args.length < 2) {
			logger.debug("java com.hs.hip.common.HDIdeaCipherIni mode text");
			logger.debug("mode = 0(encryption) | 1(decyption)");
			logger.debug("text = clearText | cipherText");
			System.exit(1);
		}

		int mode = Integer.parseInt(args[0]);
		if(mode != 0 && mode != 1) {
			logger.debug("mode is invalid. mode must be between 0 and 1. current mode value is %s", mode);
		}
		String text = args[1];
		HDIdeaCipher cipher = new HDIdeaCipher();
		String result = cipher.transform(mode, text);

		if(result == null) {
			logger.error("key is invalid");
			System.exit(1);
		}

		switch(mode) {
			case ENCRYPTION_MODE :
				logger.debug("clear text ===> %s", text);
				logger.debug("====================================");
				logger.debug("cipher text ===> %s", result);
				break;
			case DECRYPTION_MODE :
				logger.debug("cipher text ===> %s", result);
				logger.debug("====================================");
				logger.debug("clear text ===> %s", text);
				break;
		}
	}
}
