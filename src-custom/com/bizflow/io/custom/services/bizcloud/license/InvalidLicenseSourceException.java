package com.bizflow.io.custom.services.bizcloud.license;

public class InvalidLicenseSourceException extends Exception 
{
	public InvalidLicenseSourceException(String s) {
		super(s);
	}

	public InvalidLicenseSourceException() {
		super("Invalid Input information");
	}	
}
