package com.bizflow.io.custom.services.bizcloud.aws.ssm;

import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SSM {
    private SSM(){
        throw new IllegalStateException("SSM class");
    }

    private static final Logger logger = LogManager.getLogger(SSM.class);
    private static final String PARAMNAME = "PARAMNAME";
    private static final String DATATYPE = "DATATYPE";

    public static Object createSSMParameter(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String type = "SecureString";
        String dataType = "text";
        try{
            logger.debug("createSSMParameter Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(!jsonParam.has(PARAMNAME)){
                throw new Exception("Cannot find the parameter PARAMNAME");
            }
            if(!jsonParam.has("PARAMVALUE")){
                throw new Exception("Cannot find the parameter PARAMVALUE");
            }
            if(jsonParam.has("TYPE")){
                String tempType = jsonParam.getString("TYPE");
                if(tempType.equals("String") || tempType.equals("StringList") || tempType.equals("SecureString")) {
                    type = tempType;
                } else {
                    throw new Exception("The parameter TYPE should have the value 'text', 'aws:ec2:image', or 'SecureString'.");
                }
            }
            if(jsonParam.has(DATATYPE)){
                String tempDataType = jsonParam.getString(DATATYPE);
                if(tempDataType.equals("text") || tempDataType.equals("aws:ec2:image")) {
                    dataType = tempDataType;
                } else {
                    throw new Exception("The parameter DATATYPE should have the value 'text' or 'aws:ec2:image'.");
                }
            }

            String theValue = createParameter(jsonParam);

            result = new JSONObject();
            result.put("result","true");
            result.put("debugMsg", theValue);

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "createSSMParameter", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object getSSMParameter(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String paramName = null;
        boolean needDecrypt = true;
        try{
            logger.debug("getSSMParameter Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(!jsonParam.has(PARAMNAME)){
                throw new Exception("Cannot find the parameter PARAMNAME");
            }
            paramName = jsonParam.getString(PARAMNAME);

            if(jsonParam.has("NEEDDECRYPT")){
                String needDec = jsonParam.getString("NEEDDECRYPT");
                if(needDec.equalsIgnoreCase("true") || needDec.equalsIgnoreCase("y")) {
                    needDecrypt = true;
                } else if (needDec.equalsIgnoreCase("false") || needDec.equalsIgnoreCase("n")){
                    needDecrypt = false;
                } else {
                    throw new Exception("The parameter NEEDDECRYPT should have the value 'true', or 'false'.");
                }
            }

            String theValue = getStringParameter(paramName, needDecrypt);

            result = new JSONObject();
            result.put("result","true");
            result.put("data", theValue);

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "getSSMParameter", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static String getStringParameter(String paramName, boolean needDecrypt) throws Exception {
        String paramValue = null;
        Region region = Region.US_EAST_1;
        SsmClient ssmClient = SsmClient.builder()
//                .region(region)
                .build();
        try {
            GetParameterRequest parameterRequest = GetParameterRequest.builder()
                    .name(paramName).withDecryption(needDecrypt)
                    .build();

            GetParameterResponse parameterResponse = ssmClient.getParameter(parameterRequest);
            paramValue = parameterResponse.parameter().value();
            logger.debug("Param: {}", paramName);
            logger.debug("Value: {}", paramValue);

        } catch (SsmException e) {
            logger.debug("ERROR : {}",e.getMessage());
        }
        return paramValue;
    }

    private static String createParameter(JSONObject iJson) throws Exception {
        String paraName = iJson.getString(PARAMNAME);
        String paraValue = iJson.getString("PARAMVALUE");
        String paraDesc = null;
        List<Tag> tags = getTagsFromJson(iJson);
        String type = "SecureString";
        String dataType = "text";

        if(iJson.has("PARAMDESC")){
            paraDesc = iJson.getString("PARAMDESC");
        }
        if(iJson.has("TYPE")){
            type = iJson.getString("TYPE");
        }
        if(iJson.has(DATATYPE)){
            dataType = iJson.getString(DATATYPE);
        }

        return createParameter(paraName, paraValue, paraDesc, tags, type, dataType);
    }

    private static String createParameter(String paraName, String paraVal, String desc
                                        , List<Tag> tags, String type, String dataType) throws Exception {
        String paramValue = null;
        Region region = Region.US_EAST_1;
        SsmClient ssmClient = SsmClient.builder()
//                .region(region)
                .build();

        try{
            PutParameterRequest parameterRequest = PutParameterRequest.builder()
                    .name(paraName).type(type).description(desc)
                    .value(paraVal).dataType(dataType).tags(tags)
                    .build();

            PutParameterResponse parameterResponse = ssmClient.putParameter(parameterRequest);

            String val = getStringParameter(paraName, true);
            logger.debug("The parameter value is %s", val);
        } catch(SsmException e1) {
            logger.error("ERROR : %s", e1.getMessage());
        }
        return paramValue;
    }

    private static List<Tag> getTagsFromJson(JSONObject iJson) throws Exception {
        ArrayList<Tag> tags = new ArrayList<Tag>();

        Tag tag1 = getTagPair(iJson, "TAGNAME1", "TAGVALUE1");
        if(tag1 != null){
            tags.add(tag1);
        }
        Tag tag2 = getTagPair(iJson, "TAGNAME2", "TAGVALUE2");
        if(tag2 != null){
            tags.add(tag2);
        }
        Tag tag3 = getTagPair(iJson, "TAGNAME3", "TAGVALUE3");
        if(tag3 != null){
            tags.add(tag3);
        }

        return tags;
    }

    private static Tag getTagPair(JSONObject iJson, String nameTag, String valueTag) throws Exception {
        Tag tag = null;
        if(iJson.has(nameTag) && iJson.has(valueTag)){
            String tagName = iJson.getString(nameTag);
            String tagVal = iJson.getString(valueTag);
            tag = Tag.builder().key(tagName).value(tagVal).build();
        }

        return tag;
    }
}
