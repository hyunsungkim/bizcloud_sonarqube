package com.bizflow.io.custom.services.bizcloud.aws.sqs;

import com.amazon.sqs.javamessaging.AmazonSQSMessagingClientWrapper;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.bizflow.io.custom.services.bizcloud.appdev.RemoteAppDevAPI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jms.JMSException;

public class SQSConfiguration {
    private static final Logger logger = LogManager.getLogger(SQSConfiguration.class);
    public static final String DEFAULT_QUEUE_NAME = "SQSJMSClientExampleQueue";
//    public static final String DEFAULT_QUEUE_NAME = "BizflowQueTest3";

    public static final Region DEFAULT_REGION = Region.getRegion(Regions.US_EAST_1);

    public static SQSConfiguration parseConfig(String app, String queueName, String regionName) {
        try {
            return new SQSConfiguration(queueName, regionName);
        } catch (IllegalArgumentException e) {
            logger.debug("ERROR : %s", e.getMessage());
            logger.debug("queueName : %s", queueName);
            logger.debug("regionName %s", regionName);
            return null;
        }
    }

    private SQSConfiguration(String queueName, String regionName){
        setQueueName(queueName);
        setRegion(Region.getRegion(Regions.fromName(regionName)));
    }

    private String queueName = DEFAULT_QUEUE_NAME;
    private Region region = DEFAULT_REGION;
    private AWSCredentialsProvider credentialsProvider = new DefaultAWSCredentialsProviderChain();

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public AWSCredentialsProvider getCredentialsProvider() {
        return credentialsProvider;
    }

    public void setCredentialsProvider(AWSCredentialsProvider credentialsProvider) {
        credentialsProvider.getCredentials();
        this.credentialsProvider = credentialsProvider;
    }

    public boolean queueExist(SQSConnection connection, String queueName) throws JMSException {
        AmazonSQSMessagingClientWrapper client = connection.getWrappedAmazonSQSClient();

        return client.queueExists(queueName);
    }
}
