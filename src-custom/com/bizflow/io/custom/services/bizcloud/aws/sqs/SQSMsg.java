package com.bizflow.io.custom.services.bizcloud.aws.sqs;

import com.amazon.sqs.javamessaging.AmazonSQSMessagingClientWrapper;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.util.Base64;

import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;

import javax.jms.*;
import java.lang.IllegalStateException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class SQSMsg {

    private SQSMsg() {
        throw new IllegalStateException("SQS class");
    }

    private static final Logger logger = LogManager.getLogger(SQSMsg.class);
    private static final long TIME_OUT_SECONDS = 1;
    private static final String QUEUENAME = "QUEUENAME";
    private static final String REGIONNAME = "REGIONNAME";
    private static final String REASON = "us-east-1";

    public static Object listQueues(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String msg = null;
        try{
            logger.debug("listQueues Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            JSONArray queueListArrr = listQueuesJSON();
            result = new JSONObject();

            result.put("result","true");
            result.put("queuelists", queueListArrr);

        } catch (NullPointerException ne) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), ne);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",ne.getMessage());
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally{
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "listQueues", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object recvSyncQMessage(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String queueName = null;
        String regionName = null;
        boolean acknoledge = true;
        String msg = null;
        try{
            logger.debug("recvSyncQMessage Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has(QUEUENAME)){
                queueName = jsonParam.getString(QUEUENAME);
            }

            if(jsonParam.has(REGIONNAME)){
                regionName = jsonParam.getString(REGIONNAME);
            }

            if(jsonParam.has("ACKNOWLEDGE")){
                acknoledge = jsonParam.getBoolean("ACKNOWLEDGE");
            }

            msg = recvSyncSQSMessage(queueName, regionName, acknoledge);
            result = new JSONObject();

            result.put("result","true");
            result.put("msg", msg);

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "recvSyncQMessage", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object sendQMessage(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String qCategory = null;
        String qMessage = null;
        String queueName = null;
        String regionName = null;
        try{
            logger.debug("sendQMessage Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(!jsonParam.has("QMESSAGE")){
                throw new Exception("Cannot find the parameter QMESSAGE");
            }
            qMessage = jsonParam.getString("QMESSAGE");

            if(!jsonParam.has("QCATEGORY")){
                throw new Exception("Cannot find the parameter QCATEGORY");
            }
            qCategory = jsonParam.getString("QCATEGORY");

            logger.debug("QCategory: %s" ,qCategory);

            if(jsonParam.has(QUEUENAME)){
                queueName = jsonParam.getString(QUEUENAME);
            }

            if(jsonParam.has(REGIONNAME)){
                regionName = jsonParam.getString(REGIONNAME);
            }
            sendSQSMessage(qCategory, qMessage, queueName, regionName);
            result = new JSONObject();

            result.put("result","true");

        } catch (NullPointerException ne) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), ne);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",ne.getMessage());
        } catch(Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally{
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendQMessage", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object deleteSQSQueue(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String queueName = null;
        String regionName = null;

        try{
            logger.debug("deleteSQSQueue Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(!jsonParam.has(QUEUENAME)){
                throw new Exception("Cannot find the parameter QUEUENAME");
            }
            queueName = jsonParam.getString(QUEUENAME);

            logger.debug("queueName: %s" ,queueName);

            if(jsonParam.has(REGIONNAME)){
                regionName = jsonParam.getString(REGIONNAME);
            }

            deleteQueue(queueName, regionName);

            result = new JSONObject();
            result.put("result","true");

        } catch (NullPointerException ne) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), ne);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",ne.getMessage());
        } catch(Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "deleteSQSQueue", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object createSQSQueue(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String queueName = null;
        String regionName = null;
        boolean needEncrypt = true;
        try{
            logger.debug("createSQSQueue Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(!jsonParam.has(QUEUENAME)){
                throw new Exception("Cannot find the parameter QUEUENAME");
            }
            queueName = jsonParam.getString(QUEUENAME);

            logger.debug("queueName: %s", queueName);

            if(jsonParam.has(REGIONNAME)){
                regionName = jsonParam.getString(REGIONNAME);
            }

            if(jsonParam.has("NEEDENCRYPT")){
                String needEnc = jsonParam.getString("NEEDENCRYPT");
                if(needEnc.equalsIgnoreCase("true") || needEnc.equalsIgnoreCase("y")) {
                    needEncrypt = true;
                } else if (needEnc.equalsIgnoreCase("false") || needEnc.equalsIgnoreCase("n")){
                    needEncrypt = false;
                } else {
                    throw new Exception("The parameter NEEDENCRYPT should have the value 'true', or 'false'.");
                }
            }

            HashMap<String, String> tags = getTagsFromJson(jsonParam);
            creatQueue(queueName, regionName, tags, needEncrypt);

            result = new JSONObject();
            result.put("result","true");

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "createSQSQueue", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static boolean creatQueue(String queueName, String regionName, HashMap<String, String> tags, boolean needEncrypt) throws Exception {

        boolean retBool = false;
        String queueRegionName = null;

        if(regionName != null){
            queueRegionName = regionName;
        } else {
            queueRegionName = CustomServiceConfigUtil.getProperty("service.create.sqs.region.name", REASON);
        }

        SQSConfiguration config = SQSConfiguration.parseConfig("SQSMsg", queueName, queueRegionName);
        SQSConnection connection = getSQSConnection(config);

        AmazonSQSMessagingClientWrapper client = connection.getWrappedAmazonSQSClient();
        if( client.queueExists(queueName) ) {
            throw new Exception("already Existed Queue Name: " + queueName);
        }

        HashMap hm = new HashMap();
        if(queueName.endsWith(".fifo")){
            hm.put("FifoQueue", "true");
        }
        if(needEncrypt) {
            final String kmsMasterKeyAlias = "alias/aws/sqs";  // the alias of the AWS managed CMK for Amazon SQS.
            hm.put("KmsMasterKeyId", kmsMasterKeyAlias);
            hm.put("KmsDataKeyReusePeriodSeconds", "140");
        }
        CreateQueueRequest cqr = new CreateQueueRequest(queueName);
        if(hm.size() > 0) {
            cqr.setAttributes(hm);
        }
        if(tags != null && tags.size() > 0) {
            cqr.setTags(tags);
        }
        client.createQueue(cqr);
        Thread.sleep(TimeUnit.SECONDS.toMillis(TIME_OUT_SECONDS));
        if( !client.queueExists(queueName) ) {
            throw new Exception("Cannot create the Queue Name: " + queueName);
        }
        retBool = true;
        return retBool;
    }


    private static boolean deleteQueue(String queueName, String regionName) throws Exception {

        boolean retBool = false;
        String queueRegionName = null;

        if(regionName != null){
            queueRegionName = regionName;
        } else {
            queueRegionName = CustomServiceConfigUtil.getProperty("service.create.sqs.region.name", REASON);
        }

        SQSConfiguration config = SQSConfiguration.parseConfig("SQSMsg", queueName, queueRegionName);
        SQSConnection connection = getSQSConnection(config);

        AmazonSQSMessagingClientWrapper client = connection.getWrappedAmazonSQSClient();
        if( !client.queueExists(queueName) ) {
            throw new Exception("Cannot find the queue: " + queueName);
        }

        DeleteQueueRequest dqr = new DeleteQueueRequest(queueName);
        client.getAmazonSQSClient().deleteQueue(dqr);

        Thread.sleep(TimeUnit.SECONDS.toMillis(TIME_OUT_SECONDS));
        if( client.queueExists(queueName) ) {
            throw new Exception("Cannot delete the Queue: " + queueName);
        }
        retBool = true;
        return retBool;
    }

    private static String recvSyncSQSMessage(String qName, String regionName, boolean acknowledge) throws Exception {

        String queueName = null;
        String queueRegionName = null;
        String msg = null;

        if(qName != null){
            queueName = qName;
        } else {
            queueName = CustomServiceConfigUtil.getProperty("service.create.sqs.queue.name", "queueName");
        }
        if(regionName != null) {
            queueRegionName = regionName;
        } else {
            queueRegionName = CustomServiceConfigUtil.getProperty("service.create.sqs.region.name", REASON);
        }

        SQSConfiguration config = SQSConfiguration.parseConfig("SQSMsg", queueName, queueRegionName);

        SQSConnection connection = getSQSConnection(config);
        if(!config.queueExist(connection, config.getQueueName())){
            throw new Exception("Cannot find the queue: " + queueName);
        }

        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

        MessageConsumer consumer = session.createConsumer( session.createQueue( config.getQueueName() ) );

        connection.start();
        msg = receiveSyncMessage(consumer, acknowledge);
        connection.close();

        return msg;
    }

    private static void receiveAsyncMessages(Session session, MessageConsumer consumer ) {
        try {
            while( true ) {
                logger.debug("Waiting for messages");
                Message message = consumer.receive(TimeUnit.MINUTES.toMillis(1));
                if( message == null ) {
                    logger.debug("Shutting down after 1 minute of silence");
                    break;
                }
                if( message instanceof TextMessage ) {
                    TextMessage txtMessage = (TextMessage) message;
                    logger.debug("\t %s", txtMessage.getText() );
                }
//                handleMessage(message);
                message.acknowledge();
                logger.debug("Acknowledged message %s", message.getJMSMessageID() );
            }
        } catch (JMSException e) {
            logger.debug("Error receiving from SQS: %s" , e.getMessage() );
        }
    }

    private static void handleMessage(Message message) throws JMSException {
        logger.debug("Got message %s", message.getJMSMessageID() );
        logger.debug( "Content: ");
        if( message instanceof TextMessage ) {
            TextMessage txtMessage = (TextMessage) message;
            logger.debug( "\t %s", txtMessage.getText() );
        } else if( message instanceof BytesMessage ){
            BytesMessage byteMessage = (BytesMessage) message;
            // Assume the length fits in an int - SQS only supports sizes up to 256k so that
            // should be true
            byte[] bytes = new byte[(int)byteMessage.getBodyLength()];
            byteMessage.readBytes(bytes);
            logger.debug("\t %s",  Base64.encodeAsString( bytes ) );
        } else if( message instanceof ObjectMessage ) {
            ObjectMessage objMessage = (ObjectMessage) message;
            logger.debug("\t %s", objMessage.getObject() );
        }
    }

    private static boolean sendSQSMessage(String category, String qMessage, String qName, String regionName) throws Exception {

        boolean retBool = false;
        String queueName = null;
        String queueRegionName = null;

        if(qName != null){
            queueName = qName;
        } else {
            queueName = CustomServiceConfigUtil.getProperty("service.create.sqs.queue.name", "queueName");
        }
        if(regionName != null) {
            queueRegionName = regionName;
        } else {
            queueRegionName = CustomServiceConfigUtil.getProperty("service.create.sqs.region.name", REASON);
        }

        SQSConfiguration config = SQSConfiguration.parseConfig("SQSMsg", queueName, queueRegionName);

        SQSConnection connection = getSQSConnection(config);
        if(!config.queueExist(connection, config.getQueueName())){
            throw new Exception("Cannot find the queue: " + queueName);
        }

        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        MessageProducer producer = session.createProducer(session.createQueue(config.getQueueName()));

        connection.start();
        sendMessage(producer, session, qMessage);
        connection.close();
        retBool = true;

        return retBool;
    }

    private static void sendMessage(MessageProducer producer, Session session, String messageText) throws JMSException {
        // Create a text message and send it
        producer.send(session.createTextMessage(messageText));
    }

    private static String receiveSyncMessage(MessageConsumer consumer, boolean acknowledge) throws JMSException {
        // Receive a message
        String msgStr = null;
        Message message = consumer.receive(TimeUnit.SECONDS.toMillis(TIME_OUT_SECONDS));

        if (message == null) {
            logger.debug("Queue is empty!");
        } else {
            // Since this queue has only text messages, cast the message object and print the text
            msgStr = ((TextMessage) message).getText();
            logger.debug("Received: %s", msgStr);

            // Acknowledge the message if asked
            if (acknowledge) message.acknowledge();
        }
        return msgStr;
    }

    private static void ensureQueueExists(SQSConnection connection, String queueName) throws JMSException {
        AmazonSQSMessagingClientWrapper client = connection.getWrappedAmazonSQSClient();

        if( !client.queueExists(queueName) ) {
            client.createQueue( queueName );
        }
    }

    private static JSONArray listQueuesJSON() throws JMSException {
        JSONArray qArr = new JSONArray();
        AmazonSQS sqs = AmazonSQSClientBuilder.standard().build();

        for (String queryUrl : sqs.listQueues().getQueueUrls()) {
            JSONObject qJson = new JSONObject();
            logger.debug(" QueueUrl: %s", queryUrl);
            String qName = null;
            if(queryUrl.lastIndexOf("/") > -1){
                qName = queryUrl.substring(queryUrl.lastIndexOf("/") + 1);
            } else {
                qName = queryUrl;
            }
            qJson.put("QUERYURL", queryUrl);
            qJson.put(QUEUENAME, qName);
            qArr.put(qJson);
        }

        return qArr;
    }

    public static void processingMessage() throws Exception{
        String queueName = null;
        String queueRegionName = null;

        SQSConnection connection = null;

        try {
            queueName = CustomServiceConfigUtil.getProperty("service.create.sqs.queue.name", "queueName");
            queueRegionName = CustomServiceConfigUtil.getProperty("service.create.sqs.region.name", REASON);

            SQSConfiguration config = SQSConfiguration.parseConfig("AsyncMessageReceiver", queueName, queueRegionName);

            connection = getSQSConnection(config);

            AmazonSQSMessagingClientWrapper client = connection.getWrappedAmazonSQSClient();
            if(!client.queueExists(queueName) ) {
                throw new Exception("Cannot find the Queue: " + queueName);
            }

            Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            MessageConsumer consumer = session.createConsumer(session.createQueue(config.getQueueName()));

            ReceiverCallback callback = new ReceiverCallback();
            consumer.setMessageListener(callback);

            connection.start();

            callback.waitForOneMinuteOfSilence();
            logger.debug("Returning after one minute of silence");

        }
        catch (InterruptedException ie){
            logger.debug("Interrupted!", ie);
            Thread.currentThread().interrupt();
        }
        catch(Exception e) {
            e.printStackTrace();
        } finally{
            connection.close();
        }
    }

    private static SQSConnection getSQSConnection(SQSConfiguration config) throws JMSException{
        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.standard()
                        .withRegion(config.getRegion().getName())
                        .withCredentials(config.getCredentialsProvider())
        );

        return connectionFactory.createConnection();

    }
    private static class ReceiverCallback implements MessageListener {
        private volatile long timeOfLastMessage = System.nanoTime();

        public void waitForOneMinuteOfSilence() throws InterruptedException {
            for(;;) {
                long timeSinceLastMessage = System.nanoTime() - timeOfLastMessage;
                long remainingTillOneMinuteOfSilence =
                        TimeUnit.SECONDS.toNanos(59) - timeSinceLastMessage;
                if( remainingTillOneMinuteOfSilence < 0 ) {
                    break;
                }
                TimeUnit.NANOSECONDS.sleep(remainingTillOneMinuteOfSilence);

            }
        }

        @Override
        public void onMessage(Message message) {
            String msgStr = null;
            try {
                if( message instanceof TextMessage ) {
                    TextMessage txtMessage = (TextMessage) message;
                    msgStr = txtMessage.getText();
                    logger.debug( "--**--: %s", msgStr);
                } else {
                    throw new JMSException("Cannot recognize the instance type of Message object: " + message.toString());
                }
                message.acknowledge();
                timeOfLastMessage = System.nanoTime();
            } catch (JMSException e) {
                logger.error( "Error processing message: {}", e.getMessage() );
                e.printStackTrace();
            }
        }
    }


    private static HashMap<String, String> getTagsFromJson(JSONObject iJson) throws Exception {
        HashMap<String, String> hm = new HashMap<>();

        String nameTag = "TAGNAME1";
        String valueTag = "TAGVALUE1";
        if(iJson.has(nameTag) && iJson.has(valueTag)){
            String tagName = iJson.getString(nameTag);
            String tagVal = iJson.getString(valueTag);
            hm.put(tagName, tagVal);
        }
        nameTag = "TAGNAME2";
        valueTag = "TAGVALUE2";
        if(iJson.has(nameTag) && iJson.has(valueTag)){
            String tagName = iJson.getString(nameTag);
            String tagVal = iJson.getString(valueTag);
            hm.put(tagName, tagVal);
        }
        nameTag = "TAGNAME3";
        valueTag = "TAGVALUE3";
        if(iJson.has(nameTag) && iJson.has(valueTag)){
            String tagName = iJson.getString(nameTag);
            String tagVal = iJson.getString(valueTag);
            hm.put(tagName, tagVal);
        }
        return hm;
    }

}
