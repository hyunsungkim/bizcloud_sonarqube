package com.bizflow.io.custom.services.bizcloud.ansible;

import com.bizflow.io.core.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.hc.client5.http.async.methods.*;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.cookie.Cookie;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.cookie.BasicClientCookie;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManager;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManagerBuilder;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.client5.http.ssl.ClientTlsStrategyBuilder;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.nio.ssl.TlsStrategy;
import org.apache.hc.core5.io.CloseMode;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.ssl.TrustStrategy;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

@DisallowConcurrentExecution
public class AnsibleREST {
    private static final Logger logger = LogManager.getLogger(AnsibleREST.class);
    private static final String ERRORMSG502 =  "NEED_RESTART_502";
    private static X509Certificate[] EMPTY_ARRAY = {};

    private String postResponse = null;
    private String getResponse = null;

    private Cookie cookie = null;
    public AnsibleREST() {
    }

    public String callPOSTJob(String hostname, int port, String uri, String json) throws Exception {
        //"/api/v1/playbooks/maini.yml", "{\"id\":1,\"name\":\"John\"}"
        final String requestUri = uri;
        // json = "{\"id\":1,\"name\":\"John\"}";

        postResponse = null;

        // Trust standard CA and those trusted by our custom strategy
        final SSLContext sslcontext = SSLContexts.custom()
                .loadTrustMaterial(new TrustStrategy() {

                    @Override
                    public boolean isTrusted(
                            final X509Certificate[] chain,
                            final String authType) throws CertificateException {
                        final X509Certificate cert = chain[0];
                        return "CN=httpbin.org".equalsIgnoreCase(cert.getSubjectDN().getName());
                    }

                })
                .build();
        final TlsStrategy tlsStrategy = ClientTlsStrategyBuilder.create()
                .setSslContext(sslcontext)
                // IMPORTANT uncomment the following method when running Java 9 or older
                // in order for ALPN support to work and avoid the illegal reflective
                // access operation warning
                /*
                .setTlsDetailsFactory(new Factory<SSLEngine, TlsDetails>() {

                    @Override
                    public TlsDetails create(final SSLEngine sslEngine) {
                        return new TlsDetails(sslEngine.getSession(), sslEngine.getApplicationProtocol());
                    }
                })
                */
                .build();

        final PoolingAsyncClientConnectionManager cm = PoolingAsyncClientConnectionManagerBuilder.create()
                .setTlsStrategy(tlsStrategy)
                .build();
        try (final CloseableHttpAsyncClient client = HttpAsyncClients.custom()
                .setConnectionManager(cm)
                .build()) {

            // String returnBody = null;
            client.start();

            final HttpHost target = new HttpHost("https", hostname, port);

            final HttpClientContext clientContext = HttpClientContext.create();

            final SimpleHttpRequest request = SimpleHttpRequests.post(target, requestUri);
            request.setBody(json, ContentType.APPLICATION_JSON);
            request.setHeader("Accept", "application/json");

            if(this.cookie != null){
                request.setHeader("Cookie", "JSESSIONID=" + this.cookie.getValue());
            }

            final Future<SimpleHttpResponse> future = client.execute(
                    SimpleRequestProducer.create(request),
                    SimpleResponseConsumer.create(),
                    clientContext,
                    new FutureCallback<SimpleHttpResponse>() {

                        @Override
                        public void completed(final SimpleHttpResponse response) {
                            logger.debug(requestUri + "->" + response.getCode());
                            logger.debug(response.getBody());
                            logger.debug("bodyText-----" + response.getBodyText());
                            if( response.getCode() == 502){
                                postResponse = ERRORMSG502;
                            } else {
                                postResponse = response.getBodyText();
                            }
                            /*
                            final SSLSession sslSession = clientContext.getSSLSession();
                            if (sslSession != null) {
                                //logger.debug("--SSL protocol " + sslSession.getProtocol());
                                //logger.debug("--SSL cipher suite " + sslSession.getCipherSuite());
                            }
                            */
                        }

                        @Override
                        public void failed(final Exception ex) {
                            logger.error(requestUri + "-- ->" + ex);
                        }

                        @Override
                        public void cancelled() {
                            logger.debug(requestUri + "-- cancelled");
                        }
                    });
            future.get();

            logger.debug("---SSL Connection Close");
            client.close(CloseMode.GRACEFUL);
        }
        return postResponse;
    }


    public String callPOSTJob2(String hostname, int port, String uri, String json) throws Exception {
        //"/api/v1/playbooks/maini.yml", "{\"id\":1,\"name\":\"John\"}"
        final String requestUri = uri;
        //json = "{\"id\":1,\"name\":\"John\"}";

        postResponse = null;

        final PoolingAsyncClientConnectionManager cm = PoolingAsyncClientConnectionManagerBuilder.create()
                .build();
        try (final CloseableHttpAsyncClient client = HttpAsyncClients.custom()
                .setConnectionManager(cm)
                .build()) {

            // String returnBody = null;
            client.start();

            final HttpHost target = new HttpHost("http", hostname, port);
            final HttpClientContext clientContext = HttpClientContext.create();

            final SimpleHttpRequest request = SimpleHttpRequests.post(target, requestUri);
            request.setBody(json, ContentType.APPLICATION_JSON);
            request.setHeader("Accept", "application/json");

            final Future<SimpleHttpResponse> future = client.execute(
                    SimpleRequestProducer.create(request),
                    SimpleResponseConsumer.create(),
                    clientContext,
                    new FutureCallback<SimpleHttpResponse>() {

                        @Override
                        public void completed(final SimpleHttpResponse response) {
                            logger.debug(requestUri + "->" + response.getCode());
                            logger.debug(response.getBody());
                            logger.debug("bodyText-----" + response.getBodyText());
                            if( response.getCode() == 502){
                                postResponse = ERRORMSG502;
                            } else {
                                postResponse = response.getBodyText();
                            }
/*
                            final String bt = response.getBodyText();

                            final SSLSession sslSession = clientContext.getSSLSession();
                            if (sslSession != null) {
                                //logger.debug("--SSL protocol " + sslSession.getProtocol());
                                //logger.debug("--SSL cipher suite " + sslSession.getCipherSuite());
                            }

 */
                        }

                        @Override
                        public void failed(final Exception ex) {
                            logger.error(requestUri + "-- ->" + ex);
                        }

                        @Override
                        public void cancelled() {
                            logger.debug(requestUri + "-- cancelled");
                        }
                    });
            future.get();

            logger.debug("---Shutting down callPOSTJob2");
            client.close(CloseMode.GRACEFUL);
        }
        return postResponse;
    }


    public String callPOSTDownload(String hostname, int port, String uri, String json) throws Exception {
        //"/api/v1/playbooks/maini.yml", "{\"id\":1,\"name\":\"John\"}"
        final String requestUri = uri;
        //json = "{\"id\":1,\"name\":\"John\"}";

        postResponse = null;

        // Trust standard CA and those trusted by our custom strategy
        final SSLContext sslcontext = SSLContexts.custom()
                .loadTrustMaterial(new TrustStrategy() {

                    @Override
                    public boolean isTrusted(
                            final X509Certificate[] chain,
                            final String authType) throws CertificateException {
                        final X509Certificate cert = chain[0];
                        return "CN=httpbin.org".equalsIgnoreCase(cert.getSubjectDN().getName());
                    }

                })
                .build();
        final TlsStrategy tlsStrategy = ClientTlsStrategyBuilder.create()
                .setSslContext(sslcontext)
                // IMPORTANT uncomment the following method when running Java 9 or older
                // in order for ALPN support to work and avoid the illegal reflective
                // access operation warning
                /*
                .setTlsDetailsFactory(new Factory<SSLEngine, TlsDetails>() {

                    @Override
                    public TlsDetails create(final SSLEngine sslEngine) {
                        return new TlsDetails(sslEngine.getSession(), sslEngine.getApplicationProtocol());
                    }
                })
                */
                .build();

        final PoolingAsyncClientConnectionManager cm = PoolingAsyncClientConnectionManagerBuilder.create()
                .setTlsStrategy(tlsStrategy)
                .build();
        try (final CloseableHttpAsyncClient client = HttpAsyncClients.custom()
                .setConnectionManager(cm)
                .build()) {

            // String returnBody = null;
            client.start();

            final HttpHost target = new HttpHost("https", hostname, port);

            final HttpClientContext clientContext = HttpClientContext.create();

            final SimpleHttpRequest request = SimpleHttpRequests.post(target, requestUri);
            request.setBody(json, ContentType.APPLICATION_JSON);
            request.setHeader("Accept", "application/json");

            if(this.cookie != null){
                request.setHeader("Cookie", "JSESSIONID=" + this.cookie.getValue());
            }

            final Future<SimpleHttpResponse> future = client.execute(
                    SimpleRequestProducer.create(request),
                    SimpleResponseConsumer.create(),
                    clientContext,
                    new FutureCallback<SimpleHttpResponse>() {

                        @Override
                        public void completed(final SimpleHttpResponse response) {



                            logger.debug(requestUri + "->" + response.getCode());
                            logger.debug(response.getBody());
                            logger.debug("bodyText-----" + response.getBodyText());
                            if( response.getCode() == 502){
                                postResponse = ERRORMSG502;
                            } else {
                                postResponse = response.getBodyText();
                            }
                            /*
                            final SSLSession sslSession = clientContext.getSSLSession();
                            if (sslSession != null) {
                                //logger.debug("--SSL protocol " + sslSession.getProtocol());
                                //logger.debug("--SSL cipher suite " + sslSession.getCipherSuite());
                            }
                            */
                        }

                        @Override
                        public void failed(final Exception ex) {
                            logger.error(requestUri + "-- ->" + ex);
                        }

                        @Override
                        public void cancelled() {
                            logger.debug(requestUri + "-- cancelled");
                        }
                    });
            future.get();

            logger.debug("---SSL Connection Close");
            client.close(CloseMode.GRACEFUL);
        }
        return postResponse;
    }

/*
    public String callGETJob(String hostname, String appPublicIP, int port, String uri) throws Exception {
        if(uri.indexOf("<WEB_HOST>") > 0){
            uri = StringUtil.replace(uri, "<WEB_HOST>", hostname);
        }
        if(uri.indexOf("<APP_HOST>") > 0){
            uri = StringUtil.replace(uri, "<APP_HOST>", appPublicIP);
        }
        if(uri.indexOf("<PORT>") > 0){
            uri = StringUtil.replace(uri, "<PORT>", port + "");
        }

        return callGETJob(uri);
    }
*/
/*
    public String callGETJob(String hostname, String appPublicIP, int port) throws Exception {
        String uri = CustomServiceConfigUtil.getProperty("appdev.init.url.after.install");

        return callGETJob(hostname, appPublicIP, port, uri);
    }
*/
    public String callGETJob(String uri) throws Exception {
        // final String requestUri = uri;

        getResponse = null;
logger.debug("--------- call JSP File for Initializing AppDev. ---------------");
logger.debug("--------- uri = "+ uri);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(uri);
        httpGet.addHeader("User-Agent", "Mozila/5.0");
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
        try( BufferedReader reader = new BufferedReader(new InputStreamReader(
                httpResponse.getEntity().getContent()))) {
            logger.debug("::GET Response Status::");
            logger.debug(httpResponse.getCode());
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            getResponse = response.toString();
            logger.debug(getResponse);
        } catch (Exception e) {
            logger.error("Error Message : %s", e.getMessage());
        } finally {
            httpClient.close();
        }
        return getResponse;
    }

    public String login_get(String uri) throws Exception {
        getResponse = null;
        logger.debug("uri %s", uri);

        HttpClientContext context = HttpClientContext.create();
        CloseableHttpClient httpClient = null; //HttpClients.createDefault();
        httpClient = org.apache.hc.client5.http.impl.classic.HttpClientBuilder.create().setDefaultCookieStore(null)
                .build();
        HttpGet httpGet = new HttpGet(uri);
        httpGet.addHeader("User-Agent", "Mozila/5.0");
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet, context);

        org.apache.hc.client5.http.cookie.CookieStore cookieStore =  context.getCookieStore();
        List<Cookie> cookies = cookieStore.getCookies();
        if (cookies != null) {

           Iterator var4 = cookies.iterator();
           if (var4.hasNext()) {
              Cookie theCookie = (Cookie)var4.next();
              if (theCookie.getName().equals("JSESSIONID")) {
                  logger.debug("JSESSIONID = %s" ,theCookie.getValue());
                  this.cookie = theCookie;
              } else {
                  logger.debug("JSESSIONID does not exist.");
              }
           }
        }

        logger.debug("::GET Response Status::");
        logger.debug(httpResponse.getCode());
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                httpResponse.getEntity().getContent()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine);
        }
        getResponse = response.toString();
        reader.close();
        logger.debug(getResponse);
        httpClient.close();

        return getResponse;
    }

    public JSONObject executeLogin_Without_Cert(String callUrl, String method) throws Exception {
        JSONObject object = null;
        if (StringUtils.isNotBlank(callUrl)) {

            String contents = null;
            if (logger.isDebugEnabled()) {
                logger.debug("Call URL = %s" , callUrl);
            }

            String contentType = null;
            try {
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {return EMPTY_ARRAY;}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session){
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL url = new URL(callUrl);
                URLConnection con = url.openConnection();
                HttpURLConnection urlConn = (HttpURLConnection) con;
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                urlConn.setRequestMethod(method);
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("Accept", "application/json");

                if (urlConn.getResponseCode() == 200) {

                    String resMessage = urlConn.getResponseMessage();
                    logger.debug("resMessage = %s" ,resMessage);

                    contentType = urlConn.getContentType();
                    logger.debug("contentType= %s"  ,contentType);
                } else {
                    object = new JSONObject();
                    object = object.put("result", "false");
                    object = object.put("errorMessage", urlConn.getResponseCode());
                    return object;
                }

                String sessionId = null;

                List<String> cookies = urlConn.getHeaderFields().get("Set-Cookie");
                if (cookies != null) {
                    for (String cookie : cookies) {
                        sessionId = cookie.split(";\\s*")[0];
                        sessionId = sessionId.split("=\\s*")[1];
                    }
                }
                this.cookie = new BasicClientCookie("JSESSIONID", sessionId);
                contents = getContentFromConnection(urlConn);
                if(contents != null){
                    object = new JSONObject(contents);
                }
                object.put("result","true");

            } catch (NullPointerException nullPointerException) {
                object = new JSONObject();
                object = object.put("result","false");
                object = object.put("errorMessage",nullPointerException.getMessage());
                nullPointerException.printStackTrace();
                logger.error("Error URL : %s" ,callUrl);
                logger.error("Error Message : %s" ,nullPointerException.getMessage());
            } catch (Exception e) {
                object = new JSONObject();
                object = object.put("result","false");
                object = object.put("errorMessage",e.getMessage());
                e.printStackTrace();
                logger.error("Error URL : %s" ,callUrl);
                logger.error("Error Message : %s" , e.getMessage());
            }
        }
        return object;
    }


    private static String getContentFromConnection(HttpURLConnection hUrlCon){
        BufferedReader br = null;
        StringBuilder sb = null;
        try {
            br = new BufferedReader(new InputStreamReader((hUrlCon.getInputStream())));
            sb = new StringBuilder();
            String response = null;
            while ((response = br.readLine()) != null) {
                sb.append(response);
            }
            logger.debug("Content= {}", sb);
            br.close();
            hUrlCon.disconnect();
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException");
        } catch (IOException e) {
            logger.error("IOException!");
        }
        if(sb != null){
            return sb.toString();
        } else {
            logger.debug("Cannot get the contents from API");
            return null;
        }
    }
    public Cookie getCookie(){
        return this.cookie;
    }
/*
    public String callGETJobAndSetCookie(String uri) throws Exception {
        final String requestUri = uri;

        getResponse = null;
        logger.debug("--------- call JSP File for Initializing AppDev. ---------------");
        logger.debug("--------- uri = "+ uri);

        HttpClientContext context = HttpClientContext.create();
        context.setAttribute(HttpClientContext.COOKIE_STORE, createCustomCookieStore());

        success = get(url, (BasicCookieStore)null);

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(uri);
        httpGet.addHeader("User-Agent", "Mozila/5.0");
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);

        CookieStore cookieStore = context.getCookieStore();
        Cookie customCookie = cookieStore.getCookies()
                .stream()
                .peek(cookie -> log.info("cookie name:{}", cookie.getName()))
                .filter(cookie -> "custom_cookie".equals(cookie.getName()))
                .findFirst()
                .orElseThrow(IllegalStateException::new);

        assertEquals("test_value", customCookie.getValue());

        logger.debug("::GET Response Status::");
        logger.debug(httpResponse.getCode());
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                httpResponse.getEntity().getContent()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine);
        }
        getResponse = response.toString();
        reader.close();
        logger.debug(getResponse);
        httpClient.close();

        return getResponse;
    }
*/


    public static void main(final String[] args) throws Exception {

        AnsibleREST asyncAPI = new AnsibleREST();
//        asyncAPI.callGETJob("https://bizcloud.bizflow.com/bizflowappdev/services/acm/login.json?app=fbs&i=hkim&p=xxx");
//        asyncAPI.callGETJob("https://54.81.225.123/bizflowappdev/services/acm/login.json?app=fbs&i=appdev&p=xxx");
//        asyncAPI.callPOSTJob("bizcloud.bizflow.com", 443, "/api/v1/playbooks/maini.yml", "{\"id\":1,\"name\":\"John\"}");
 //       asyncAPI.callGETJob("bizcloud.bizflow.com", 443, "/api/v1/playbooks");

//        String getResponse = asyncAPI.callGETJob("bizcloud.bizflow.com", 443, "/api/v1/playbooks");
//        String uri2 = "http://3.80.178.29:8080/bizflowappdev/apps/sample/initData.jsp?serverIP=3.80.178.29&loginID=appdev6&password=xxx&wsName=Workspace&auth9=111000001";

        String domain="3.80.178.29";
        int port = 8080;
        String uri = "/bizflowappdev/apps/sample/initDataTest.jsp?serverIP=3.80.178.29&loginID=appdev4&password=xxx&wsName=Workspace&auth9=111000001";
        String postResponse = asyncAPI.callPOSTJob2(domain, port, uri, ""); // http
//        String postResponse = asyncAPI.callPOSTJob(domain, port, uri, ""); // https

        logger.debug("CALL_postResponse= %s" ,postResponse);
        /*
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://bizcloud.bizflow.com:443/api/v1/playbooks");
        httpGet.addHeader("User-Agent", "Mozila/5.0");
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
        logger.debug("::GET Response Status::");
        logger.debug(httpResponse.getCode());
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                httpResponse.getEntity().getContent()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine);
        }
        reader.close();
        logger.debug(response.toString());
        httpClient.close();
         */
    }
}
