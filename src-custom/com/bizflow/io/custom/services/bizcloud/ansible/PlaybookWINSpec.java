package com.bizflow.io.custom.services.bizcloud.ansible;

import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.custom.services.bizcloud.util.FileUtil;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;

public class PlaybookWINSpec {
    private static final Logger logger = LogManager.getLogger(PlaybookWINSpec.class);

    private static JSONArray playbookJson = null;
    private static String[] EMPTY_STRING_ARRAY = {};
    private final static String SAAS_PLAYBOOK_INFO_WIN = "playbook.win.info.spec.file.for.saas";
    private final static String STEPNO = "StepNo";
    private final static String INPUTJSON = "InputJSON";
    private final static String TASKFOR = "TaskFor";

    private static boolean loadPlaybookSpecWINFile(){
        if(playbookJson == null){
            logger.debug("playbookJson is null, so load the spec file.");
            String specFilePath = CustomServiceConfigUtil.getConfiguration().getProperty(SAAS_PLAYBOOK_INFO_WIN);
            String contentsInFile = FileUtil.getFileContents(new File(specFilePath));
            playbookJson = new JSONArray(contentsInFile);
        }
        logger.debug("playbookJson="+playbookJson.toString());
        return true;
    }

    public static String[] getUriAndInputJson(int stepNo) throws Exception{

        String[] uriAndJson = null;
        if (playbookJson == null) {
            loadPlaybookSpecWINFile();
        }

        for (int i=0; i < playbookJson.length(); i++) {
            JSONObject onePlaybook = playbookJson.getJSONObject(i);
            if(onePlaybook.has(STEPNO) && onePlaybook.getInt(STEPNO) == stepNo){
                uriAndJson = new String[2];
                uriAndJson[0] = onePlaybook.getString("URI");
                uriAndJson[1] = onePlaybook.getJSONObject(INPUTJSON).toString();
                return uriAndJson;
            }
        }
        if(uriAndJson == null){
            throw new Exception("Cannot found the playbook spec for the stepNo:"+ stepNo);
        }

        return EMPTY_STRING_ARRAY;
    }

    public static String[] getUriAndInputJson(int stepNo, String serviceType) throws Exception{

        String[] uriAndJson = null;
        if (playbookJson == null) {
            loadPlaybookSpecWINFile();
        }

        for (int i=0; i < playbookJson.length(); i++) {
            JSONObject onePlaybook = playbookJson.getJSONObject(i);
            if(onePlaybook.has(STEPNO) && onePlaybook.getInt(STEPNO) == stepNo){
                uriAndJson = new String[2];
                uriAndJson[0] = onePlaybook.getString("URI");
                uriAndJson[1] = onePlaybook.getJSONObject(INPUTJSON).toString();
                return uriAndJson;
            }
        }
        if(serviceType.equals("P")){
            for (int i=0; i < playbookJson.length(); i++) {
                JSONObject onePlaybook = playbookJson.getJSONObject(i);
                if(onePlaybook.has(TASKFOR) && onePlaybook.getString(TASKFOR).equals("AccessForPaaS")){
                    uriAndJson = new String[2];
                    uriAndJson[0] = onePlaybook.getString("URI");
                    uriAndJson[1] = onePlaybook.getJSONObject(INPUTJSON).toString();
                    return uriAndJson;
                }
            }
        }
        if(uriAndJson == null){
            throw new Exception("Cannot found the playbook spec for the stepNo:"+ stepNo);
        }

        return EMPTY_STRING_ARRAY;
    }

    /**
     *
     * @return uriAndJson[0] = URI
     *         uriAndJson[1] = InputJSON
     * @throws Exception
     */
    public static String[] getLicenseURIAndInput() throws Exception{

        String[] uriAndJson =  null;
        if (playbookJson == null) {
            loadPlaybookSpecWINFile();
        }

        for (int i=0; i < playbookJson.length(); i++) {
            JSONObject onePlaybook = playbookJson.getJSONObject(i);
            if(onePlaybook.has(TASKFOR) && onePlaybook.getString(TASKFOR).equals("UpdateLicense")) {
                uriAndJson = new String[2];
                uriAndJson[0] = onePlaybook.getString("URI");
                uriAndJson[1] = onePlaybook.getJSONObject(INPUTJSON).toString();
                return uriAndJson;
            }
        }
        if(uriAndJson == null){
            throw new Exception("Cannot found the WINDOWS playbook spec using Creating License");
        }

        return EMPTY_STRING_ARRAY;
    }

    /**
     *
     * @return uriAndJson[0] = URI
     *         uriAndJson[1] = InputJSON
     * @throws Exception
     */
    public static String[] getRemoveServiceURIAndInput() throws Exception{

        String[] uriAndJson =  null;
        if (playbookJson == null) {
            loadPlaybookSpecWINFile();
        }

        for (int i=0; i < playbookJson.length(); i++) {
            JSONObject onePlaybook = playbookJson.getJSONObject(i);
            if(onePlaybook.has(TASKFOR) && onePlaybook.getString(TASKFOR).equals("RemoveService")) {
                uriAndJson = new String[2];
                uriAndJson[0] = onePlaybook.getString("URI");
                uriAndJson[1] = onePlaybook.getJSONObject(INPUTJSON).toString();
                return uriAndJson;
            }
        }
        if(uriAndJson == null){
            throw new Exception("Cannot found the WINDOWS playbook spec using Removing Service");
        }

        return EMPTY_STRING_ARRAY;
    }

    public static int getLastStepNo() throws Exception {
        if (playbookJson == null) {
            loadPlaybookSpecWINFile();
        }
        int topStepNo = -1;
        for (int i=0; i < playbookJson.length(); i++) {
            JSONObject onePlaybook = playbookJson.getJSONObject(i);
            if(onePlaybook.has(STEPNO)) {
                int aStepNo = onePlaybook.getInt(STEPNO);
                if (aStepNo > topStepNo) {
                    topStepNo = aStepNo;
                }
            }
        }
        return topStepNo;
    }

    public static int getLastStepNo(String serviceType) throws Exception {
        if (playbookJson == null) {
            loadPlaybookSpecWINFile();
        }
        boolean hasGuacomoli = false;
        int topStepNo = -1;
        for (int i=0; i < playbookJson.length(); i++) {
            JSONObject onePlaybook = playbookJson.getJSONObject(i);
            if(onePlaybook.has(STEPNO)) {
                int aStepNo = onePlaybook.getInt(STEPNO);
                if (aStepNo > topStepNo) {
                    topStepNo = aStepNo;
                }
            }
            if(onePlaybook.has(TASKFOR) && onePlaybook.getString(TASKFOR).equals("AccessForPaaS")){
                hasGuacomoli = true;
            }
        }
        if((topStepNo != -1 && serviceType.equals("P")) && hasGuacomoli){
                topStepNo += 1;
        }
        return topStepNo;
    }

    public static String[] getUriAndInputJson2(int stepNo) throws Exception{

        String[] uriAndJson = null;
        if (playbookJson == null) {
            loadPlaybookSpecFile2();
        }

        for (int i=0; i < playbookJson.length(); i++) {
            JSONObject onePlaybook = playbookJson.getJSONObject(i);
            if(onePlaybook.has(STEPNO) && onePlaybook.getInt(STEPNO) == stepNo){
                uriAndJson = new String[2];
                uriAndJson[0] = onePlaybook.getString("URI");
                uriAndJson[1] = onePlaybook.getJSONObject(INPUTJSON).toString();
                return uriAndJson;
            }
        }
        if(uriAndJson == null){
            throw new Exception("Cannot found the playbook spec for the stepNo:"+ stepNo);
        }

        return EMPTY_STRING_ARRAY;
    }

    private static boolean loadPlaybookSpecFile2(){
        if(playbookJson == null){
            logger.debug("playbookJson = null");
            String specFilePath = "D:/Project/BIOCloud/web/WEB-INF/templates/cloud/SaaSPlaybookInfo.json";
            String contentsInFile = FileUtil.getFileContents(new File(specFilePath));
            playbookJson = new JSONArray(contentsInFile);
        }
        logger.debug("playbookJson= {}", playbookJson.toString());
        return true;
    }

    public static void main(String[] argss){
        try {
            String[] ts = PlaybookSpec.getUriAndInputJson2(1);
            logger.debug("ts= {}", ts[0] + "-" + ts[1]);
        }catch (Exception e){
            logger.error("ERROR : {}", e.getMessage());
        }
    }
}
