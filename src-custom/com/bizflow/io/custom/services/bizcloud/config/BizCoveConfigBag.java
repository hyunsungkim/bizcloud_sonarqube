package com.bizflow.io.custom.services.bizcloud.config;

import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.*;

import com.hs.bf.adk.config.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BizCoveConfigBag extends HttpServlet implements Serializable {
    private static final Logger logger = LogManager.getLogger(BizCoveConfigBag.class);
//	private static final String BIZFLOW_PIPELINE = "bizflowpipe";
    public static XMLResourceBundles hwres = new XMLResourceBundles();
    public static boolean isLoaded = false;
    //	private static String defaultPipeline = null;
    private static URL resURL = null;

    public BizCoveConfigBag() {}

    public static void init(String propsPath) {
        try {
            /*
             * ServletContext context = getServletContext(); String
             * resourceXmlURI = getInitParameter("resource-xml-uri"); File
             * resourceXmlFile = new File(context.getRealPath(resourceXmlURI));
             * setResourceInfo(resourceXmlFile.toURL()); boolean loaded =
             * parse(); if(!loaded) System.out.println(
             * "[INFO] com.hs.bf.adk.config couldn't read resource from : " +
             * resourceXmlURI);
             */

            File resourceXmlFile = new File(propsPath);
            setResourceInfo(resourceXmlFile.toURI().toURL());
            boolean loaded = parse();
            if (!loaded)
                logger.debug("[INFO] com.bizflow.io.custom.services.bizcloud.config.ConfigBag couldn't read resource from : %s", propsPath);
        } catch (Exception e) {
            logger.error("ERROR : %s", e.getMessage());
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        String cmd = req.getParameter("reload");
        if (cmd != null && "true".compareToIgnoreCase(cmd) == 0) {
            isLoaded = false;
            parse();
        }
        String cmd2 = req.getParameter("action");
        if (cmd2 != null && "dump".compareToIgnoreCase(cmd2) == 0) {
            Properties props = getProperties();
            try {
                String name;
                ServletOutputStream out;
                for (Enumeration propNames = props.propertyNames(); propNames
                        .hasMoreElements(); out.println("[" + name + "]["
                        + props.getProperty(name) + "]<br>")) {
                    name = (String) propNames.nextElement();
                    out = res.getOutputStream();
                }

            } catch (Exception e) {
                logger.error("ERROR : %s", e.getMessage());
            }
        }
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        doGet(req, res);
    }

    public static void setResourceInfo(URL resourceURL) {
        resURL = resourceURL;
    }

    public static String getDefaultPipeName() {
        String pipeline = getProperty("defaultPipeName");
        if (pipeline == null || "".equals(pipeline.trim()))
            pipeline = "bizflowpipe";
        return pipeline;
    }

    public static boolean parse() {
        if (!isLoaded)
            try {
                if (resURL != null) {
                    hwres.setResourceInfo(resURL);
                    isLoaded = true;
                }
            } catch (Exception e) {
                isLoaded = false;
            }
        return isLoaded;
    }

    public String getString(String itemid) {
        return getProperty(itemid);
    }

    public static String getProperty(String itemid) {
        String msg = "";
        try {
            msg = hwres.getString(itemid);
        } catch (Exception e) {
            msg = "";
        }
        return msg;
    }

    public static Properties getProperties() {
        return hwres.getProperties();
    }

}
