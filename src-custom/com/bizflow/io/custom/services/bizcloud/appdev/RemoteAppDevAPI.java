package com.bizflow.io.custom.services.bizcloud.appdev;

import com.hs.bf.web.xmlrs.*;
import com.hs.bf.web.beans.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.*;


public class RemoteAppDevAPI {
    private static final Logger logger = LogManager.getLogger(RemoteAppDevAPI.class);

    private HWSessionFactory hwSessionFactory = null;
    private HWSession hwSession = null;
    private String sessionInfoXML = null;

    public boolean initializeAppDev(String webIP, String bizflowIP, int serverPort, String loginID, String password, String workspaceName, String auth9) throws Exception{
        logger.debug("******* START RemoteAppDevAPI.initializeAppDev ******** ");
        logger.debug("webIP= %s",webIP);
        logger.debug("bizflowIP= %s",bizflowIP);
        logger.debug("serverPort= %s",serverPort);
        logger.debug("loginID= %s",loginID);
        logger.debug("password= ********");
        logger.debug("workspaceName= %s",workspaceName);
        logger.debug("auth9= %s" ,auth9);

        hwSessionFactory = new HWSessionFactory();
        hwSessionFactory.setDefaultImplClassName(HWSessionFactory.TCPImpl);

        login(bizflowIP, serverPort, loginID, password);

        logger.debug("******* END RemoteAppDevAPI.initializeAppDev ********");
        return true;
    }

    public boolean initializeAppDev(String bizflowIP, String loginID, String password) throws Exception {
        return initializeAppDev(null, bizflowIP, 7201, loginID, password, "Sample Workspace", "111111111");
    }

    private void formatTimeZone(Calendar cal, StringBuilder buf) {
        TimeZone tz = cal.getTimeZone();

        if (tz == null) {
            return;
        }

        // otherwise print out normally.
        int offset = tz.getOffset(cal.getTime().getTime());

        if (offset == 0) {
            buf.append('Z');
            return;
        }

        if (offset >= 0) {
            buf.append('+');
        } else {
            buf.append('-');
            offset *= -1;
        }

        offset /= 60 * 1000; // offset is in milli-seconds

        formatTwoDigits(offset / 60, buf);
        formatTwoDigits(offset % 60, buf);
    }

    private void formatTwoDigits(int offset, StringBuilder buf){
        if(offset < 10){
            buf.append("0");
        }
        buf.append(offset);
    }

    public String login(String serverIP, int serverPort, String loginID, String password) throws Exception{
        hwSession = hwSessionFactory.newInstance();
        logger.debug("Login appdev user to create sample workspaces");
        hwSessionFactory = new HWSessionFactory();
        hwSession = hwSessionFactory.newInstance();
        logger.debug("serverIP %s",serverIP);
        logger.debug("serverPort= %s",serverPort);
        logger.debug("loginID= %s",loginID);
        sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, password,true);
        return sessionInfoXML;
    }

    public static void main(String[] args) {
        RemoteAppDevAPI ini = new RemoteAppDevAPI();
        if(args.length != 3 || args[0].length() < 8){
            logger.debug("Please Enter valid public BPM Server IP as the 1st argument. args[0]= %s" , args[0]  );
            logger.debug("args[0].length()= %s" , args[0].length());
            logger.debug("  ex1) 123.32.45.121");
            System.exit(1);
        } else if(args.length < 3){
            logger.debug("Please Enter 3 arguments as below.");
            logger.debug("  ex1) 'Executable java' 'bpm server ip' 'loginid to be added' 'password'");
            System.exit(2);
        }

        try {
            ini.initializeAppDev(args[0], args[1], args[2]);
        } catch (Exception e){
            logger.debug("ERROR MESSAGE %s", e.getMessage());
        }
    }
}
