package com.bizflow.io.custom.services.bizcloud.appdev;

import com.hs.bf.web.xmlrs.*;
import com.hs.bf.web.beans.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.*;


public class Initialize {
    private static final Logger logger = LogManager.getLogger(Initialize.class);

    private HWSessionFactory hwSessionFactory = null;
    private HWSession hwSession = null;
    private String sessionInfoXML = null;
    private static final String INITALDEFID = "9000000000";
    private static final String PARTICIPANTID = "_USER00001";

    public boolean initializeAppDev(String webIP, String bizflowIP, int serverPort, String loginID, String password, String workspaceName, String auth9) throws Exception{
        logger.debug("******* START initializeAppDev ********");
        logger.debug("webIP= %s",webIP);
        logger.debug("bizflowIP= %s",bizflowIP);
        logger.debug("serverPort= %s",serverPort);
        logger.debug("loginID= %s",loginID);
        logger.debug("password= %s","********");
        logger.debug("workspaceName= %s",workspaceName);
        logger.debug("auth9= %s",auth9);

        hwSessionFactory = new HWSessionFactory();
        hwSessionFactory.setDefaultImplClassName(HWSessionFactory.TCPImpl);

        addUser(bizflowIP, serverPort, loginID, password, auth9);

        login(bizflowIP, serverPort, loginID, password);

        logger.debug("******* END initializeAppDev ********");
        return true;
    }

    public boolean initializeAppDev(String bizflowIP, String loginID, String password) throws Exception {
        return initializeAppDev(null, bizflowIP, 7201, loginID, password, "Sample Workspace", "111111111");
    }

    private void formatTimeZone(Calendar cal, StringBuilder buf) {
        TimeZone tz = cal.getTimeZone();

        if (tz == null) {
            return;
        }

        // otherwise print out normally.
        int offset = tz.getOffset(cal.getTime().getTime());

        if (offset == 0) {
            buf.append('Z');
            return;
        }

        if (offset >= 0) {
            buf.append('+');
        } else {
            buf.append('-');
            offset *= -1;
        }

        offset /= 60 * 1000; // offset is in milli-seconds

        formatTwoDigits(offset / 60, buf);
        formatTwoDigits(offset % 60, buf);
    }

    private void formatTwoDigits(int offset, StringBuilder buf){
        if(offset < 10){
            buf.append("0");
        }
        buf.append(offset);
    }

    public int addUser(String serverIP, int serverPort, String loginID, String password, String auth9) throws Exception{
        boolean forceCheckOut = true;

        hwSession = hwSessionFactory.newInstance();
        sessionInfoXML = hwSession.logIn(serverIP, serverPort, "administrator", password,true);

        HWSessionInfo hwSessionInfo = new HWSessionInfo();
        hwSessionInfo.reset (sessionInfoXML);

        if(hasUserInBPM(hwSession, sessionInfoXML, loginID)){
            logger.debug("******* Already exists loginId in BPM. loginID= %s", loginID);
            return 0;
        }
        // Check out
        HWInteger reasonNumber = new HWInteger();
        hwSession.checkOutOrg(sessionInfoXML, forceCheckOut, INITALDEFID, reasonNumber);

        // User info
        XMLResultSetImpl xrsUsers = new XMLResultSetImpl();
        xrsUsers.createResultSet("HWUsers", "HWUSER");

        xrsUsers.add();
        xrsUsers.setFieldValueAt(0, "ID", PARTICIPANTID);    // temp id. this should have the prefix '_USER'.
        xrsUsers.setFieldValueAt(0, "LICENSETYPE", "N");    // Named
        xrsUsers.setFieldValueAt(0, "INHERITTYPE", "P");    // InheritP
        xrsUsers.setFieldValueAt(0, "NAME", "AppDev User");
        xrsUsers.setFieldValueAt(0, "MANAGERID", "0000000000");
        xrsUsers.setFieldValueAt(0, "EMAIL", "");
        xrsUsers.setFieldValueAt(0, "EMPLOYEENUMBER", "");
        xrsUsers.setFieldValueAt(0, "JOBTITLEID", "9999999999");
        xrsUsers.setFieldValueAt(0, "JOBTITLEIDA", "");
        xrsUsers.setFieldValueAt(0, "JOBTITLEIDB", "");
        xrsUsers.setFieldValueAt(0, "DEPTID", INITALDEFID);
        xrsUsers.setFieldValueAt(0, "SERVERID", (String) hwSessionInfo.get("ServerID"));
        xrsUsers.setFieldValueAt(0, "ISABSENT", "F");
        xrsUsers.setFieldValueAt(0, "STATE", "N");  // Normal
        xrsUsers.setFieldValueAt(0, "JOBTITLENAME", "Administrator");
        xrsUsers.setFieldValueAt(0, "JOBTITLENAMEA", "");
        xrsUsers.setFieldValueAt(0, "JOBTITLENAMEB", "");
        xrsUsers.setFieldValueAt(0, "DEPTNAME", "Root");
        xrsUsers.setFieldValueAt(0, "SHORTNAME", "AppDev User");
        xrsUsers.setFieldValueAt(0, "LOGINID", loginID);
        xrsUsers.setFieldValueAt(0, "TYPE", "U");
        xrsUsers.setFieldValueAt(0, "DISPORDER", "0");
        xrsUsers.setFieldValueAt(0, "PASSWORD", "224155018038064014231021037079227131083195211000");
        xrsUsers.setFieldValueAt(0, "CHANGEPASSWD", "F");
        xrsUsers.setFieldValueAt(0, "PASSWDFAILCOUNT", "0");

        // License Group
        XMLResultSetImpl xrsLicenseGroups = new XMLResultSetImpl();
        xrsLicenseGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");

        // licsgrp001: System Administrator , licsgrp002: General Administrator , licsgrp003: Designer,
        // licsgrp004: User                 , licsgrp005: OfficeEngine User     , licsgrp006: Report Designer,
        // licsgrp007: Report User          , licsgrp008: WebMaker Designer     , licsgrp009: AppDev Designer

        int objectRow = 0;
        for(int i=0 ; i < auth9.length(); i ++){
            char theAuth = auth9.charAt(i);
            if(theAuth != '0'){
                objectRow = xrsLicenseGroups.add();
                xrsLicenseGroups.setFieldValueAt(objectRow, "USERGROUPID", "licsgrp00" + (i+1));   // 'System Administrator ' license
                xrsLicenseGroups.setFieldValueAt(objectRow, "PARTICIPANTID", PARTICIPANTID); // temp id. This should have the prefix '_USER'.
                xrsLicenseGroups.setFieldValueAt(objectRow, "TYPE", "U");
                xrsLicenseGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
            }
        }

        // Authority Group
        XMLResultSetImpl xrsAuthGroups = new XMLResultSetImpl();
        xrsAuthGroups.createResultSet("HWGroupParticipants", "HWGROUPPARTICIPANT");

        for(int i=0 ; i < auth9.length(); i ++) {
            char theAuth = auth9.charAt(i);
            if (theAuth != '0') {
                if( i < 4 ) {
                    objectRow = xrsAuthGroups.add();
                    xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 1));
                    xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", PARTICIPANTID);    // temp id. This should have the prefix '_USER'.
                    xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                    xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                }

                if( i > 2 ){
                    objectRow = xrsAuthGroups.add();
                    if(i < 8){
                        xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp00" + (i + 2));
                    } else {
                        xrsAuthGroups.setFieldValueAt(objectRow, "USERGROUPID", "authgrp0" + (i + 2));
                    }
                    xrsAuthGroups.setFieldValueAt(objectRow, "PARTICIPANTID", PARTICIPANTID);    // temp id. This should have the prefix '_USER'.
                    xrsAuthGroups.setFieldValueAt(objectRow, "TYPE", "U");
                    xrsAuthGroups.setFieldValueAt(objectRow, "DISPORDER", "100");
                }
            }
        }

        // Add user
        hwSession.updateOrg(sessionInfoXML, null, xrsUsers.toByteArray(),
                xrsAuthGroups.toByteArray(), xrsLicenseGroups.toByteArray(), null);
        hwSession.checkInOrg(sessionInfoXML, INITALDEFID);
        logger.debug("Test user was successfully added. The Added Login Id is : {}", loginID);
        hwSession.logOut(sessionInfoXML);

        return 0;
    }

    public String login(String serverIP, int serverPort, String loginID, String password) throws Exception{
        hwSession = hwSessionFactory.newInstance();
        logger.debug("Login appdev user to create sample workspaces");
        hwSessionFactory = new HWSessionFactory();
        hwSession = hwSessionFactory.newInstance();
        logger.debug("serverIP= %s",serverIP);
        logger.debug("serverPort %s",serverPort);
        logger.debug("loginID= %s",loginID);
        sessionInfoXML = hwSession.logIn(serverIP, serverPort, loginID, password,true);
        return sessionInfoXML;
    }


    public static boolean hasUserInBPM(HWSession hwSession, String sessionInfoXML, String loginID) {
        boolean ret = false;
//        long timeS = System.currentTimeMillis();

        try{
            HWFilter hwFilterUser = new HWFilter();
            hwFilterUser.setName("HWUser");
            hwFilterUser.addFilter("LOGINID", "E", loginID);

            InputStream resultStream = hwSession.getUsers (	sessionInfoXML,
                    hwFilterUser.toByteArray());

            XMLResultSet xrs = new XMLResultSetImpl();
            xrs.parse(resultStream);
            int rowCount = xrs.getRowCount();
            if(rowCount == 1) {
                ret = true;
            }
            resultStream.close();
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        } finally {
//            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "hasUserInBPM", (System.currentTimeMillis() - timeS));
        }
        return ret;
    }

    // HTTP POST request
    /*
    private void sendPost(String targetUrl, String parameters) throws Exception {

        URL url = new URL(targetUrl);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setDoOutput(true);

        // Send post request
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(parameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // print result
        System.out.println("HTTP 응답 코드 : " + responseCode);
        System.out.println("HTTP body : " + response.toString());

    }
*/

    public static void main(String[] args) {
        Initialize ini = new Initialize();
        if(args.length != 3 || args[0].length() < 8){
            logger.debug("Please Enter valid public BPM Server IP as the 1st argument. args[0]= {}", args[0] );
            logger.debug("args[0].length()= {}", args[0].length());
            logger.debug("  ex1) 123.32.45.121");
            System.exit(1);
        } else if(args.length < 3){
            logger.debug("Please Enter 3 arguments as below.");
            logger.debug("  ex1) 'Executable java' 'bpm server ip' 'loginid to be added' 'password' ");
            System.exit(2);
        }

        try {
            ini.initializeAppDev(args[0], args[1], args[2]);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
