package com.bizflow.io.custom.services.bizcloud.util;

import com.bizflow.io.core.db.mybatis.model.QueryActionConstant;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.core.security.SecurityCipher;
import com.bizflow.io.custom.services.bizcloud.ansible.AnsibleREST;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import com.bizflow.io.services.data.dao.util.SqlDAOUtil;
import com.bizflow.io.services.message.exception.MessageServiceException;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.net.ssl.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpgradeInfo {
    private UpgradeInfo() {
        throw new IllegalStateException("UpgradeInfo class");
    }

    private static final Logger logger = LogManager.getLogger(UpgradeInfo.class);
    private static final int LIMIT_OF_CHECKED = 30;
    private static X509Certificate[] EMPTY_X509CERTIFICATE = {};

    public static Object insertUpgradeDefInfo(String district, String sessionName, HashMap parameter) {
        logger.debug("UpgradeInfo.insertUpgradeDefInfo is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        HashMap dataMap = new HashMap();
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has("data") && jsonParam.has("queryString")){
                if(jsonParam.get("data") instanceof JSONObject) {
                    jsonParam = jsonParam.getJSONObject("data");
                } else if(jsonParam.get("data") instanceof JSONArray) {
                    jsonParam = jsonParam.getJSONArray("data").getJSONObject(0);
                }
                parameter = (HashMap)jsonParam.toMap();
            }

            if(jsonParam == null || !jsonParam.has("APPTYPE")){
                throw new Exception("Cannot find the parameter 'APPTYPE' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("OSTYPE")){
                throw new Exception("Cannot find the parameter 'OSTYPE' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("VERSIONSTR")){
                throw new Exception("Cannot find the parameter 'VERSIONSTR' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("RELEASEDATE")){
                throw new Exception("Cannot find the parameter 'RELEASEDATE' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("FILENAME")){
                throw new Exception("Cannot find the parameter 'FILENAME' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("CRTUSERID")){
                throw new Exception("Cannot find the parameter 'CRTUSERID' or have the null value.");
            }

            String queryName = "upgrade.insertUpgradeDefInfo";
            String queryAction = QueryActionConstant.InsertQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, parameter);

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("result After insertUpgradeDefInfo: %s", resultObject);
            logger.debug("All jobs are done!");

        } catch (Exception e) {
            logger.error("{}:  jsonString={} %s", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "insertUpgradeDefInfo", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static String getToday(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleformat = new SimpleDateFormat("MM/dd/yyyy");
        return simpleformat.format(cal.getTime());
    }

    private static String getCurrentDateTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleformat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        return simpleformat.format(cal.getTime());
    }

    private static String getDatePlusDays(int days){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, days);
        SimpleDateFormat simpleformat = new SimpleDateFormat("MM/dd/yyyy");
        return simpleformat.format(cal.getTime());
    }

    public static Object selectUpgradeDefList(String district, String sessionName, HashMap parameter) {
        logger.debug("UpgradeInfo.selectUpgradeDefList is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            int pageNo = -1;
            int pageSize = -1;
            if(jsonParam.has("pageNo") && jsonParam.get("pageNo") != null){
                pageNo = jsonParam.getInt("pageNo");
            }
            if(jsonParam.has("pageSize") && jsonParam.get("pageSize") != null){
                pageSize = jsonParam.getInt("pageSize");
            }

            if(jsonParam.has("filters") && jsonParam.has("queryString")){
                if(jsonParam.get("filters") instanceof JSONObject) {
                    jsonParam = jsonParam.getJSONObject("filters");
                } else if(jsonParam.get("filters") instanceof JSONArray) {
                    jsonParam = CommonUtil.getFieldAndValue(jsonParam.getJSONArray("filters"), jsonParam);
                }
//                jsonParam = CommonUtil.addStartNumEndNum(jsonParam);
                parameter = (HashMap)jsonParam.toMap();
            }

            if(jsonParam == null || !jsonParam.has("APPTYPE")){
                throw new Exception("Cannot find the parameter 'APPTYPE' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("OSTYPE")){
                throw new Exception("Cannot find the parameter 'OSTYPE' or have the null value.");
            }

            jsonParam.put("STARTNUM", 0);
            jsonParam.put("ENDNUM", 100000);
            String queryName = "upgrade.selectUpgradeDefList";
            String queryAction =  QueryActionConstant.SelectQuery;
            int totalCount = CommonUtil.getTotalCount(queryName, jsonParam);

            if(pageNo < 0)
                pageNo = 1;
            if(pageSize < 0)
                pageSize = 10;

            int STARTNUM = pageSize * (pageNo - 1);
            int ENDNUM = pageSize * (pageNo);
            jsonParam.put("STARTNUM", STARTNUM);
            jsonParam.put("ENDNUM", ENDNUM);
            parameter = (HashMap)jsonParam.toMap();

            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, parameter);

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");
            result.put("totalCount", totalCount);
            result.put("pageNo", pageNo);
            result.put("pageSize", pageSize);

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "selectTrialUserInfoList", (System.currentTimeMillis() - timeS));
        }
        return result;
    }
    private static String getDownloadFilePath(String appType, String versionStr) throws Exception{

        String rootPath = CustomServiceConfigUtil.getProperty("web.root.physical.file.path")
                + CustomServiceConfigUtil.getProperty("upgrade.repository.relative.file.path") ;
        String downPath = rootPath;
        if(appType == null || versionStr == null){
            throw new Exception("The value of appType or versionStr parameter should not be null");
        }
        if(appType.equals("B")){
            downPath += "bpm/";
        } else if (appType.equals("A")){
            downPath += "appdev/";
        }

        if(versionStr.indexOf('.') < 0){
            throw new Exception("The format of the value of versionStr parameter is incorrect. Period point doesn't Exist");
        }
        String majorVersionStr = null;
        int majorVersion = -1;
        majorVersionStr = versionStr.substring(0, versionStr.indexOf('.'));
        if(!CommonUtil.isInteger(majorVersionStr)){
            throw new Exception("The format of the value of versionStr parameter is incorrect: "+ majorVersionStr);
        }
        majorVersion = Integer.parseInt(majorVersionStr);
        downPath += majorVersion + File.separator ;
        logger.debug("download Path = %s", downPath);
        return downPath;

    }

    public static boolean isExistForDownloadFile(String appType, String versionStr, String fileName) throws Exception {
        String downFilePath = null;
        try{
            downFilePath = getDownloadFilePath(appType, versionStr);
            downFilePath += fileName;
            if(new File(downFilePath).exists()){
                return true;
            } else {
                return false;
            }
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private static String getDownFileFullPath(String appType, String versionStr, String fileName) throws Exception {
        String downFilePath = null;
        try{
            downFilePath = getDownloadFilePath(appType, versionStr);
            downFilePath += fileName;
            return downFilePath;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static Object existForDownloadFile(String district, String sessionName, HashMap parameter) {
        logger.debug("UpgradeInfo.existForDownloadFile is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        HashMap dataMap = new HashMap();
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has("data") && jsonParam.has("queryString")){
                if(jsonParam.get("data") instanceof JSONObject) {
                    jsonParam = jsonParam.getJSONObject("data");
                } else if(jsonParam.get("data") instanceof JSONArray) {
                    jsonParam = jsonParam.getJSONArray("data").getJSONObject(0);
                }
                parameter = (HashMap)jsonParam.toMap();
            }

            if(jsonParam == null || !jsonParam.has("APPTYPE")){
                throw new Exception("Cannot find the parameter 'APPTYPE' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("VERSIONSTR")){
                throw new Exception("Cannot find the parameter 'VERSIONSTR' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("FILENAME")){
                throw new Exception("Cannot find the parameter 'FILENAME' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("CRTUSERID")){
                throw new Exception("Cannot find the parameter 'CRTUSERID' or have the null value.");
            }
            String appType = jsonParam.getString("APPTYPE");
            String versionStr = jsonParam.getString("VERSIONSTR");
            String fileName = jsonParam.getString("FILENAME");

            if(!appType.equals("A") && !appType.equals("B")){
                throw new Exception("Incorrect Value of APPTYPE parameter: "+appType);
            }
            if(fileName.indexOf("..") >= 0 || fileName.indexOf(File.separator) >= 0){
                throw new Exception("FILENAME should not have '..' or a file separator.: "+fileName);
            }
            boolean exists = isExistForDownloadFile(appType, versionStr, fileName);
            result = new JSONObject();
            result.put("result","" + exists);
            if(!exists){
                result.put("faultString","Cannot find the file. Contact your administrator.");
            }

            logger.debug("result After existForDownloadFile:" + result.toString());
            logger.debug("All jobs are done!");

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "existForDownloadFile", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object getDownFilePath(String district, String sessionName, HashMap parameter) {
        logger.debug("UpgradeInfo.getDownFilePath is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        HashMap dataMap = new HashMap();
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has("data") && jsonParam.has("queryString")){
                if(jsonParam.get("data") instanceof JSONObject) {
                    jsonParam = jsonParam.getJSONObject("data");
                } else if(jsonParam.get("data") instanceof JSONArray) {
                    jsonParam = jsonParam.getJSONArray("data").getJSONObject(0);
                }
                parameter = (HashMap)jsonParam.toMap();
            }

            if(jsonParam == null || !jsonParam.has("APPTYPE")){
                throw new Exception("Cannot find the parameter 'APPTYPE' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("VERSIONSTR")){
                throw new Exception("Cannot find the parameter 'VERSIONSTR' or have the null value.");
            }
            if(jsonParam == null || !jsonParam.has("CRTUSERID")){
                throw new Exception("Cannot find the parameter 'CRTUSERID' or have the null value.");
            }
            String appType = jsonParam.getString("APPTYPE");
            if(!appType.equals("A") && !appType.equals("B")){
                throw new Exception("Incorrect Value of APPTYPE parameter: "+appType);
            }
            String versionStr = jsonParam.getString("VERSIONSTR");
            String filePath = getDownloadFilePath(appType, versionStr);
            result = new JSONObject();
            String webRoot = CustomServiceConfigUtil.getProperty("web.root.physical.file.path");
            if(new File(filePath).exists() && webRoot != null && webRoot.length() > 0){
                filePath = filePath.substring(webRoot.length());
                result.put("result","true");
                result.put("path", filePath);
            } else {
                result.put("result","false");
                result.put("faultString","Cannot find the file path. Contact your administrator.");
            }

            logger.debug("result After getDownFilePath:" + result.toString());
            logger.debug("All jobs are done!");

        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "getDownFilePath", (System.currentTimeMillis() - timeS));
        }
        return result;
    }


    public static int goUpdate() {
        logger.debug(" ------------------ START goUpdate  ------------------");
        String[] osType = {"nux", "win"};

        try {
            JSONObject theOSInfo = new JSONObject();
            String currentOS = null;
            String currentAPP = null;
            for (int i = 0; i < osType.length; i++) {
                int updatingCount = getCountsForUpdatingNow(osType[i]);
                if(updatingCount > 0){
                    continue;
                }
                theOSInfo = theOSInfo.put("OSTYPE", osType[i]);
                String queryName = "upgrade.getUpgradePlanInfoToGo";
                String queryAction = QueryActionConstant.SelectQuery;
                Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(theOSInfo.toString()));
                logger.debug("resultObject = %s", resultObject.toString());

                JSONArray planInfoArr = null;
                JSONObject upgradeDefJson = null;
                if (resultObject instanceof JSONArray) {
                    planInfoArr = (JSONArray) resultObject;
                    if(planInfoArr.length() < 1){
                        continue;
                    }
                    upgradeDefJson = planInfoArr.getJSONObject(0);
                } else if (resultObject instanceof JSONObject){
                    upgradeDefJson = (JSONObject) resultObject;
                }

                if( upgradeDefJson == null || upgradeDefJson.length() < 1){
                    continue;
                }

                if( upgradeDefJson.has("GONOW") && upgradeDefJson.getInt("GONOW") == 0){
                    continue;
                }
                if(osType[i].equals("nux")){
                    currentOS = "Linux";
                } else {
                    currentOS = "Windows";
                }

                if(upgradeDefJson.has("APPTYPE") && upgradeDefJson.getString("APPTYPE").equals("A")){
                    currentAPP = "AppDev";
                } else {
                    currentAPP = "BizFLow";
                }
                queryName = "service.selectIPsForUpdating";
                queryAction = QueryActionConstant.SelectQuery;
                Object serviceIp = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(theOSInfo.toString()));
                logger.debug("serviceIp = %s", serviceIp.toString());
                JSONArray serviceInfoArr = null;
                JSONObject serviceDefJson = null;
                String webIP = null;
                if (serviceIp instanceof JSONArray) {
                    serviceInfoArr = (JSONArray) serviceIp;
                    if(serviceInfoArr.length() < 1){
                        continue;
                    }
                    serviceDefJson = serviceInfoArr.getJSONObject(0);
                } else if (serviceIp instanceof JSONObject){
                    serviceDefJson = (JSONObject) serviceIp;
                    serviceInfoArr = new JSONArray();
                    serviceInfoArr = serviceInfoArr.put(serviceDefJson);
                }
                if ( serviceDefJson == null || serviceDefJson.length() < 1){
                    logger.debug("Cannot find services information based on: %s", osType[i]);
                    continue;
                }

                upgradeDefJson = upgradeDefJson.put("STATE", "U");
                upgradeDefJson = upgradeDefJson.put("INITIATEDDATETIME", "true");
                upgradeDefJson = upgradeDefJson.put("UPDUSERID", "100001");
                String logValue = "\n-- START UPDATE: " + getCurrentDateTime() + "";
                logValue += "\n-- OS: " + currentOS + ", APP: " + currentAPP + ", Number of Services:" + serviceInfoArr.length() + "";
                Object objReturn = updateUpgradeDefInfo(upgradeDefJson, "SYSTEMLOG" , logValue);

                //                String urlForUpdate = "https://bizcloud.bizflow.com/bizflowauth/services/auth/login.json";
                JSONObject updDefForLog = null;
                String urlForUpdate = "https://%SERVERIP%/bizflowauth/services/auth/login.json";
                urlForUpdate = CustomServiceConfigUtil.getProperty("update.initiate.rest.api.url");
                for(i = 0; i < serviceInfoArr.length(); i++){
                    serviceDefJson = serviceInfoArr.getJSONObject(i);
                    webIP = serviceDefJson.getString("WEBIP");
                    String theUrl = urlForUpdate;
                    int serviceId = serviceDefJson.getInt("SSERVICEID");
                    String serviceName = serviceDefJson.getString("INSTANCENAME");
                    updDefForLog = new JSONObject();

                    theUrl = theUrl.replace("%SERVERIP%", webIP);

                    Object retObj = executeSSLAPI_Without_Certificate(theUrl, false, "POST", upgradeDefJson);
                    JSONObject retJson = null;
                    if(retObj instanceof JSONObject){
                        retJson = (JSONObject) retObj;
                    } else if(retObj instanceof JSONArray){
                        JSONArray jArr = (JSONArray) retObj;
                        retJson = jArr.getJSONObject(0);
                    }

                    if(!retJson.has("result")){
                        retJson = retJson.put("result", "false");
                    }

                    if(retJson.getString("result").equals("true")){
                        upgradeDefJson = upgradeDefJson.put("UPDSTATUS", "U");
                    } else {
                        upgradeDefJson = upgradeDefJson.put("UPDSTATUS", "E");
                        upgradeDefJson = upgradeDefJson.put("TOUPGRADEDEFID", upgradeDefJson.getInt("UPGRADEDEFID"));
                    }

                    logValue += "\n- Called #: " + (i+1) + ", sName: " + serviceName + ", sID: " + serviceId + ", Result:" + retJson.getString("result") ;

                    updDefForLog = updDefForLog.put("UPGRADEDEFID", upgradeDefJson.getInt("UPGRADEDEFID"));
                    updDefForLog = updDefForLog.put("UPDUSERID", upgradeDefJson.getInt("UPDUSERID"));
                    objReturn = updateUpgradeDefLog(updDefForLog, "SYSTEMLOG" , logValue);
                    logValue = "";

                    upgradeDefJson = upgradeDefJson.put("SSERVICEID", serviceId);
                    upgradeDefJson = upgradeDefJson.put("TOUPGRADEDEFID", upgradeDefJson.getInt("UPGRADEDEFID"));


                    if(retJson.getString("result").equals("true")){
                        int upgradeID = insertServiceUpgradeInfo(upgradeDefJson, "MSG", "Initiated at " + getCurrentDateTime());
                    } else {
                        logValue = "Initiated but Error at " + getCurrentDateTime();
                        logValue += "\n ErrorMessage=" + retJson.getString("errorMessage");
                        int upgradeID = insertServiceUpgradeInfo(upgradeDefJson, "MSG", logValue);
                    }

                }
                logValue = "\n--- END UPDATE: " + getCurrentDateTime() ;
                updateUpgradeDefLog(upgradeDefJson, "SYSTEMLOG" , logValue);

                logger.debug("serviceDefJson = " + serviceDefJson.toString());

                // upgradeDefJson

            } // for
        } catch (Exception e){
            e.printStackTrace();
        }

                logger.debug(" ------------------ START goUpdate() UpdCount = " + 0 + " ------------------");

        return -1;
    }


    public static int applyFinishedUpdatedJobs() {
        logger.debug(" ------------------ START applyFinishedUpdatedJobs  ------------------");
        String[] osType = {"nux", "win"};

        try {
            JSONObject theOSInfo = new JSONObject();
            String currentOS = null;
            String currentAPP = null;
            for (int i = 0; i < osType.length; i++) {
                theOSInfo = theOSInfo.put("OSTYPE", osType[i]);
                String queryName = "upgrade.getFinishingUpgradeInfo";
                String queryAction = QueryActionConstant.SelectQuery;
                Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(theOSInfo.toString()));
                logger.debug("resultObject = " + resultObject);

                JSONArray planInfoArr = null;
                JSONObject upgradeDefJson = null;
                if (resultObject instanceof JSONArray) {
                    planInfoArr = (JSONArray) resultObject;
                    if(planInfoArr.length() < 1){
                        continue;
                    }
                    upgradeDefJson = planInfoArr.getJSONObject(0);
                } else if (resultObject instanceof JSONObject){
                    upgradeDefJson = (JSONObject) resultObject;
                }

                if( upgradeDefJson == null || upgradeDefJson.length() < 1){
                    continue;
                }

                if(osType[i].equals("nux")){
                    currentOS = "Linux";
                } else {
                    currentOS = "Windows";
                }

                if(upgradeDefJson.has("APPTYPE") && upgradeDefJson.getString("APPTYPE").equals("A")){
                    currentAPP = "AppDev";
                } else {
                    currentAPP = "BizFLow";
                }

                queryName = "upgrade.getFinishingServiceInfo";
                queryAction = QueryActionConstant.SelectQuery;
                Object serviceIp = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(upgradeDefJson.toString()));
                logger.debug("serviceIp = {}" + serviceIp);
                JSONArray serviceInfoArr = null;
                JSONObject serviceDefJson = null;
                String webIP = null;
                String updStatus = null;
                if (serviceIp instanceof JSONArray) {
                    serviceInfoArr = (JSONArray) serviceIp;
                    if(serviceInfoArr.length() < 1){
                        continue;
                    }
                    serviceDefJson = serviceInfoArr.getJSONObject(0);
                } else if (serviceIp instanceof JSONObject){
                    serviceDefJson = (JSONObject) serviceIp;
                    serviceInfoArr = new JSONArray();
                    serviceInfoArr = serviceInfoArr.put(serviceDefJson);
                }
                if ( serviceDefJson == null || serviceDefJson.length() < 1){
                    logger.debug("Cannot find CURRENT UPDATING Service information based on: {}" + osType[i]);
                    continue;
                }
/////////////////////////////////////////////////////
                upgradeDefJson = upgradeDefJson.put("UPDUSERID", "100001");
                String logValue = "\n-- START APPLYING: " + getCurrentDateTime() + "";
                logValue += "\n-- OS: " + currentOS + ", APP: " + currentAPP + ", Number of Services:" + serviceInfoArr.length() ;
                Object objReturn = updateUpgradeDefLog(upgradeDefJson, "SYSTEMLOG" , logValue);

                //                String urlForUpdate = "https://bizcloud.bizflow.com/bizflowauth/services/auth/login.json";

                int ongoingCount = 0;
                int successfulCount = 0;
                int failCount = 0;
                JSONObject updDefForLog = null;
                String urlForUpdate = "";
                urlForUpdate = CustomServiceConfigUtil.getProperty("update.statuscheck.rest.api.url");
                for(i = 0; i < serviceInfoArr.length(); i++) {
                    serviceDefJson = serviceInfoArr.getJSONObject(i);
                    webIP = serviceDefJson.getString("WEBIP");
                    String theUrl = urlForUpdate;
                    int serviceId = serviceDefJson.getInt("SSERVICEID");
                    String serviceName = serviceDefJson.getString("INSTANCENAME");
                    updStatus = serviceDefJson.getString("UPDSTATUS");
                    updDefForLog = new JSONObject();

                    theUrl = theUrl.replace("%SERVERIP%", webIP);

                    if (updStatus.equals("E")) {

                    } else if (updStatus.equals("U")) {
                        Object retObj = executeSSLAPI_Without_Certificate(theUrl, false, "POST", upgradeDefJson);
                        JSONObject retJson = null;
                        if (retObj instanceof JSONObject) {
                            retJson = (JSONObject) retObj;
                        } else if (retObj instanceof JSONArray) {
                            JSONArray jArr = (JSONArray) retObj;
                            retJson = jArr.getJSONObject(0);
                        }

                        if (!retJson.has("result")) {
                            retJson = retJson.put("result", "false");
                        }

                        if (retJson.getString("result").equals("successful")) {
                            upgradeDefJson = upgradeDefJson.put("UPDSTATUS", "S");
                            successfulCount++;
                        } else if (retJson.getString("result").equals("fail")){
                            upgradeDefJson = upgradeDefJson.put("UPDSTATUS", "E");
                            failCount++;
                        } else {
                            logger.debug("\n-- Applying Called Number#: " + (i + 1) + ", serviceName: " + serviceName + ", serviceID: " + serviceId + ", Result:" + retJson.getString("result"));
                            upgradeDefJson = upgradeDefJson.put("UPDSTATUS", "U");
                            ongoingCount++;
                        }

                        logValue += "\n-- Applying Called Number#: " + (i + 1) + ", serviceName: " + serviceName + ", serviceID: " + serviceId + ", Result:" + retJson.getString("result");

                        updDefForLog = updDefForLog.put("UPGRADEDEFID", upgradeDefJson.getInt("UPGRADEDEFID"));
                        updDefForLog = updDefForLog.put("UPDUSERID", upgradeDefJson.getInt("UPDUSERID"));
                        objReturn = updateUpgradeDefLog(updDefForLog, "SYSTEMLOG", logValue);
                        logValue = "";

                        upgradeDefJson = upgradeDefJson.put("SSERVICEID", serviceId);
                        upgradeDefJson = upgradeDefJson.put("TOUPGRADEDEFID", upgradeDefJson.getInt("UPGRADEDEFID"));

                        Object updObj = updateServiceUpgradeInfo(upgradeDefJson, "MSG", "Finished and Checked at " + getCurrentDateTime());
                    }
                }
                if(ongoingCount == 0 && serviceInfoArr.length() > 0) {
                    upgradeDefJson = upgradeDefJson.put("UPGRADEDEFID", upgradeDefJson.getInt("FROMUPGRADEDEFID"));
                    upgradeDefJson = upgradeDefJson.put("STATE", "O");
                    logValue = "\n--- END Applying: " + getCurrentDateTime();
                    updateUpgradeDefLog(upgradeDefJson, "SYSTEMLOG", logValue);

                    upgradeDefJson = upgradeDefJson.put("UPGRADEDEFID", upgradeDefJson.getInt("TOUPGRADEDEFID"));
                    upgradeDefJson = upgradeDefJson.put("STATE", "A");
                    updateUpgradeDefLog(upgradeDefJson, "SYSTEMLOG", logValue);
                }
                logger.debug("serviceDefJson = " + serviceDefJson.toString());

                // upgradeDefJson

            } // for
        } catch (Exception e){
            e.printStackTrace();
        }

        logger.debug(" ------------------ START applyFinishedUpdatedJobs()  ------------------");

        return -1;
    }


    public static int applyServiceStatus_WhileUpting() {
        logger.debug(" ------------------ START applyServiceStatus_WhileUpting  ------------------");

        try {
            JSONObject resultJson = new JSONObject();
            String currentOS = null;
            String currentAPP = null;

            String queryName = "upgrade.getServicesListUpdatingNow";
            String queryAction = QueryActionConstant.SelectQuery;
            Object serviceListObj = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(resultJson.toString()));

            logger.debug("serviceIp = {}", serviceListObj);
            JSONArray serviceInfoArr = null;
            JSONObject serviceDefJson = null;
            String webIP = null;
            String updStatus = null;
            if (serviceListObj instanceof JSONArray) {
                serviceInfoArr = (JSONArray) serviceListObj;
                if(serviceInfoArr.length() < 1){
                    return 0;
                }
                serviceDefJson = serviceInfoArr.getJSONObject(0);
            } else if (serviceListObj instanceof JSONObject){
                serviceDefJson = (JSONObject) serviceListObj;
                serviceInfoArr = new JSONArray();
                serviceInfoArr = serviceInfoArr.put(serviceDefJson);
            }
            if ( serviceDefJson == null || serviceDefJson.length() < 1){
                logger.debug("There are no updating services!");
                return 0;
            }

            String logValue = "\n-- START APPLYING: " + getCurrentDateTime() + ", Number of Services:"  + serviceInfoArr.length() ;
            logger.debug(logValue);
            int ongoingCount = 0;
            int successfulCount = 0;
            int failCount = 0;

            int checkedCount = 0;
            String urlForUpdate = "";
            urlForUpdate = CustomServiceConfigUtil.getProperty("update.statuscheck.rest.api.url");
            for(int i = 0; i < serviceInfoArr.length(); i++) {
                serviceDefJson = serviceInfoArr.getJSONObject(i);
                webIP = serviceDefJson.getString("WEBIP");
                String theUrl = urlForUpdate;
                int serviceId = serviceDefJson.getInt("SSERVICEID");
                String serviceName = serviceDefJson.getString("INSTANCENAME");

                theUrl = theUrl.replace("%SERVERIP%", webIP);
                serviceDefJson = serviceDefJson.put("UPDUSERID", "100001");
                checkedCount = serviceDefJson.getInt("CHECKEDCOUNT");
                if(checkedCount > LIMIT_OF_CHECKED){
                    logger.debug("Checked Count is %s", checkedCount);
                    logger.debug("so never check the status any more for this service: %s", webIP);
                    continue;
                }
                Object retObj = executeSSLAPI_Without_Certificate(theUrl, true, "POST", serviceDefJson);

                JSONObject retJson = null;
                if (retObj instanceof JSONObject) {
                    retJson = (JSONObject) retObj;
                } else if (retObj instanceof JSONArray) {
                    JSONArray jArr = (JSONArray) retObj;
                    retJson = jArr.getJSONObject(0);
                }

                if (!retJson.has("result")) {
                    retJson = retJson.put("result", "fail");
                }

                if (retJson.getString("result").equals("successful")) {
                    serviceDefJson = serviceDefJson.put("UPDSTATUS", "S");
                    serviceDefJson = serviceDefJson.put("APPLIEDDATE", "SYSDATE");
                    successfulCount++;
                } else if (retJson.getString("result").equals("fail")) {
                    serviceDefJson = serviceDefJson.put("UPDSTATUS", "F");
                    serviceDefJson = serviceDefJson.put("ERRORDATETIME", "SYSDATE");
                    failCount++;
                } else {
                    logger.debug("\n-- Applying Called Number#: " + (i + 1) + ", serviceName: " + serviceName + ", serviceID: " + serviceId + ", Result:" + retJson.getString("result"));
                    serviceDefJson = serviceDefJson.put("UPDSTATUS", "U");
                    ongoingCount++;
                }

                logValue = "-- Applying Called Number#: " + (i + 1) + ", serviceName: " + serviceName + ", serviceID: " + serviceId + ", Result:" + retJson.getString("result");
                logValue += " - " + getCurrentDateTime();
                serviceDefJson.remove("INITIATEDDATETIME");

                int msgCount = 0;
                if(serviceDefJson.has("MSG")){
                    msgCount = serviceDefJson.getString("MSG").length();
                }
                if(msgCount + logValue.length() > 3950){
                    logValue = "";
                }
                serviceDefJson.put("CHECKEDCOUNT", checkedCount++);
                Object updObj = updateServiceUpgradeInfo(serviceDefJson, "MSG", logValue);
            }
            logger.debug("serviceDefJson = " + serviceDefJson.toString());

        } catch (Exception e){
            e.printStackTrace();
        }

        logger.debug(" ------------------ START applyFinishedUpdatedJobs()  ------------------");

        return -1;
    }

    public static Object updateUpgradeDefInfo(JSONObject jsonObj, String logKey, String logValue) throws Exception {
        logger.debug("--START updateUpgradeDefInfo---");

        jsonObj = jsonObj.put(logKey, logValue);
        String queryName = "upgrade.updateUpgradeDefInfo";
        String queryAction = QueryActionConstant.UpdateQuery;

        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(jsonObj.toString()));
        int ret = -1;
        if(resultObject instanceof Integer){
            ret = ((Integer)resultObject).intValue();
        }
        logger.debug("--END updateUpgradeDefInfo--- %s", ret);
        return resultObject;
    }

    public static Object updateUpgradeDefLog(JSONObject jsonObj, String logKey, String logValue) throws Exception {
        logger.debug("--START updateUpgradeDefLog---");

        jsonObj = jsonObj.put(logKey, logValue);
        String queryName = "upgrade.updateUpgradeDefSystemLog";
        String queryAction = QueryActionConstant.UpdateQuery;

        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(jsonObj.toString()));
        int ret = -1;
        if(resultObject instanceof Integer){
            ret = ((Integer)resultObject).intValue();
        }
        logger.debug("--END updateUpgradeDefLog--- %s" , ret);
        return resultObject;
    }


    public static Object updateServiceUpgradeInfo(JSONObject jsonObj, String logKey, String logValue) throws Exception {
        logger.debug("--START updateServiceUpgradeInfo---");

        if(logKey != null){
            jsonObj = jsonObj.put(logKey, logValue);
        }
        String queryName = "upgrade.updateServiceUpgradeInfo";
        String queryAction = QueryActionConstant.UpdateQuery;

        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(jsonObj.toString()));
        int ret = -1;
        if(resultObject instanceof Integer){
            ret = ((Integer)resultObject).intValue();
        }
        logger.debug("--END updateServiceUpgradeInfo--- %s", ret );
        return resultObject;
    }

    public static int insertServiceUpgradeInfo(JSONObject jsonObj, String logKey, String logValue) throws Exception {
        logger.debug("--START insertServiceUpgradeInfo---");
        int upgradeID = -1;
        if(logKey != null){
            jsonObj = jsonObj.put(logKey, logValue);
        }
        String queryName = "upgrade.insertServiceUpgradeInfo";
        String queryAction = QueryActionConstant.InsertQuery;

        HashMap map = (HashMap) JSONUtil.jsonStringToMap(jsonObj.toString());
        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, map);


        upgradeID = ((Integer)map.get("UPGRADEID")).intValue();
        logger.debug("--END insertServiceUpgradeInfo--- %s", upgradeID );
        return upgradeID;
    }

    public static int getCountsForUpdatingNow(String osType) throws Exception {
        String queryName = "upgrade.getCountsForUpdatingNow";
        String queryAction = QueryActionConstant.SelectQuery;

        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("OSTYPE", osType);
        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, parameter);
        JSONObject retJson = null;
        if(resultObject instanceof JSONArray){
            JSONArray jArr = (JSONArray) resultObject;
            if(jArr.length() < 1)
                return -1;
            retJson = jArr.getJSONObject(0);
        } else if(resultObject instanceof JSONObject){
            retJson = (JSONObject) resultObject;
        }

        if(!retJson.has("UCOUNT"))
            return -1;

        int uCount = retJson.getInt("UCOUNT");

        return uCount;
    }

    public static JSONObject executeSSLAPI_Without_Certificate(String callUrl, boolean needReturnValue, String method, JSONObject jsonObj) throws Exception {
        JSONObject object = null;
        if (StringUtils.isNotBlank(callUrl)) {

            String contents = null;
            if (logger.isDebugEnabled()) {
                logger.debug("Call URL = %s", callUrl);
            }

            String contentType = null;
            try {
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {return EMPTY_X509CERTIFICATE;}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session){
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL url = new URL(callUrl);
                URLConnection con = url.openConnection();
                HttpURLConnection urlConn = (HttpURLConnection) con;
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                urlConn.setRequestMethod(method);
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("Accept", "application/json");

                OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
                wr.write(jsonObj.toString());
                wr.flush();

                if (urlConn.getResponseCode() == 200) {

                    String resMessage = urlConn.getResponseMessage();
                    logger.debug("resMessage = %s", resMessage);

                    contentType = urlConn.getContentType();
                    logger.debug("contentType= %s", contentType);
                } else {
                    logger.debug("responseCode= %s", urlConn.getResponseCode());
                    logger.debug("responseMessage= %s", urlConn.getResponseMessage());
                    /* --------------------------- */
                    contents = "";
                    object = new JSONObject();
                    object = object.put("result", "false");
                    for (Map.Entry<String, List<String>> header : urlConn.getHeaderFields().entrySet()) {
                        for (String value : header.getValue()) {
                            logger.debug(header.getKey() + " : " + value);
                            contents += header.getKey() + " : " + value + "\n";
                        }
                    }
                    object = object.put("errorMessage", contents);
                    return object;
                }

                if(needReturnValue){
                    contents = getContentFromConnection(urlConn);
                    if(contents != null){
                        object = new JSONObject(contents);
                    }
                } else {
                    object = new JSONObject();
                }
                if(!object.has("result")){
                    object.put("result","true");
                }
            } catch (Exception e) {
                object = new JSONObject();
                object = object.put("result","false");
                object = object.put("errorMessage",e.getMessage());
                e.printStackTrace();
                logger.error("Error URL : %s", callUrl);
                logger.error("Error Message : %s", e.getMessage());
            }
        }
        return object;
    }


    public static JSONObject executeSSLAPI_No_Response_Without_Cert(String callUrl, boolean needReturnValue, String method, JSONObject jsonObj) throws Exception {
        JSONObject object = null;
        if (StringUtils.isNotBlank(callUrl)) {

            String contents = null;
            if (logger.isDebugEnabled()) {
                logger.debug("Call URL = %s", callUrl);
            }

            String contentType = null;
            try {
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {return null;}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session){
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL url = new URL(callUrl);
                URLConnection con = url.openConnection();
                HttpURLConnection urlConn = (HttpURLConnection) con;
                urlConn.setDoInput(needReturnValue);
                urlConn.setDoOutput(true);
                urlConn.setRequestMethod(method);
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("Accept", "application/json");

                OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
                wr.write(jsonObj.toString());
                wr.flush();

                if (urlConn.getResponseCode() == 200) {

                    String resMessage = urlConn.getResponseMessage();
                    logger.debug("resMessage = %s", resMessage);

                    contentType = urlConn.getContentType();
                    logger.debug("contentType= %s", contentType);
                } else {
                    logger.debug("responseCode= %s", urlConn.getResponseCode());
                    logger.debug("responseMessage= %s", urlConn.getResponseMessage());
                    /* --------------------------- */
                    contents = "";
                    object = new JSONObject();
                    object = object.put("result", "false");
                    for (Map.Entry<String, List<String>> header : urlConn.getHeaderFields().entrySet()) {
                        for (String value : header.getValue()) {
                            logger.debug(header.getKey() + " : " + value);
                            contents += header.getKey() + " : " + value + "\n";
                        }
                    }
                    object = object.put("errorMessage", contents);
                    return object;
                }

                if(needReturnValue){
                    contents = getContentFromConnection(urlConn);
                    if(contents != null){
                        object = new JSONObject(contents);
                    }
                    object.put("result","true");
                } else {
                    object = new JSONObject();
                    object = object.put("result", "true");
                }
            } catch (Exception e) {
                object = new JSONObject();
                object = object.put("result","false");
                object = object.put("errorMessage",e.getMessage());
                e.printStackTrace();
                logger.error("Error URL : %s", callUrl);
                logger.error("Error Message : %s", e.getMessage());
            }
        }
        return object;
    }

    private static String getContentFromConnection(HttpURLConnection hUrlCon){
        //BufferedReader br = null;
        StringBuilder sb = null;
        try(BufferedReader br = new BufferedReader(new InputStreamReader((hUrlCon.getInputStream())))) {
            sb = new StringBuilder();
            String response = null;
            while ((response = br.readLine()) != null) {
                sb.append(response);
            }
            logger.debug("Content= {}" + sb);
            hUrlCon.disconnect();
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException");
        } catch (IOException e) {
            logger.error("IOException!");
        }
        if(sb != null){
            return sb.toString();
        } else {
            logger.debug("Cannot get the contents from API");
            return null;
        }
    }

    public static JSONObject imageUrlDown_SSLAPI_Without_Certificate(String imageUrl, String fileName, int count, String currentTime) throws Exception {

        BufferedImage bi = null;
        JSONObject object = new JSONObject();

        if (StringUtils.isNotBlank(imageUrl)) {

            if (logger.isDebugEnabled()) {
                logger.debug("Download URL = %s", imageUrl);
            }

            String contentType = null;
            try {
                //ignore SSL
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {return EMPTY_X509CERTIFICATE;}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());
                //end

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session){
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL url = new URL(imageUrl);
                URLConnection con = url.openConnection();
                HttpURLConnection urlConn = (HttpURLConnection) con;

                if (urlConn.getResponseCode() == 200) {

                    String resMessage = urlConn.getResponseMessage();
                    logger.debug("resMessage = %s", resMessage);

                    contentType = urlConn.getContentType().split("/")[1];
                    if ("jpeg".equalsIgnoreCase(contentType)) {
                        contentType = "jpg";
                    }
                } else {
                    logger.debug("responseCode= %s", urlConn.getResponseCode());
                    logger.debug("responseMessage= %s", urlConn.getResponseMessage());
                    Object content = urlConn.getContent();
                    logger.debug("content.toString= {}", content);

                }

                File saveFile = new File("D:\\Backup\\Image");
                saveFile.getParentFile().mkdirs();

                if (logger.isDebugEnabled()) {
                    logger.debug("contentType = %s", contentType);
                    logger.debug("saveFile = %s" , fileName + "." + contentType);
                }

                if (saveFile.isDirectory()) {
                    saveFile.mkdirs();
                } else {
                    try {
                        bi = ImageIO.read(url);
                        ImageIO.write(bi, contentType, saveFile);

                        if (logger.isDebugEnabled()) {
                            logger.debug("saveFile.getAbsolutePath() = %s", saveFile.getAbsolutePath());
                            logger.debug("saveFile.getPath() = %s", saveFile.getPath());
                        }

                    } catch (MalformedURLException e) {
                        logger.error("Cannot Found Image");
                    } catch (IOException e) {
                        logger.error("File IOException");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error URL : %s", imageUrl);
                logger.error("Error Message : %s", e.getMessage());
            }
        }
        return object;
    }

    public static Object getServiceBPMSVersionInfo(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String serviceID = null;
        String systemUserID = null;
        String systemUserPW = null;

        String url = null;

        try{
            logger.debug("getServiceBPMSVersionInfo Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            systemUserID = CustomServiceConfigUtil.getProperty("appdev.client.system.user.id", "appdev");
            systemUserPW = CustomServiceConfigUtil.getProperty("appdev.client.system.user.pw", "");

            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);

            boolean debug = false;
            if(jsonParam.has("TEST")){
                if(jsonParam.getString("TEST").equalsIgnoreCase("Y")){
                    debug = true;
                    systemUserID = "hkim";
                    systemUserPW = "1";
                }
            }

            serviceID = jsonParam.getInt("SSERVICEID") + "";
            JSONObject idJson = new JSONObject();

            if(getUpdatingServiceCount(jsonParam) > 0){
                return getErrJSON("Updating Now!~~");
            }
            String queryName = "service.selectIPsByServiceID";
            String queryAction =  QueryActionConstant.SelectQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, (HashMap)JSONUtil.jsonStringToMap(jsonParam.toString()));

            String webIP = null;
            String osType = null;
            JSONObject ipsJson = null;
            if(resultObject instanceof JSONArray){
                JSONArray jArr = (JSONArray) resultObject;
                if(jArr == null || jArr.length() < 1){
                    return getErrJSON("Cannot find network information by serviceID: " + serviceID);
                }
                if(jArr.length() > 0) {
                    ipsJson = jArr.getJSONObject(0);
                } else {
                    ipsJson = new JSONObject();
                }
            } else if(resultObject instanceof JSONObject){
                ipsJson = (JSONObject) resultObject;
            }
            if(ipsJson.length() < 1){
                return getErrJSON("Cannot find the service running");
            }
            webIP = ipsJson.getString("WEBIP");
            osType = ipsJson.getString("OSTYPE");

            if(debug){
                webIP = "bizcloud.bizflow.com";
            }
            if(webIP == null || webIP.length() < 4){
                return getErrJSON("Cannot find WebIP and network information by serviceID: " + serviceID);
            }
            String protocol = "https";
            if(protocol.equals("https")) {

                url = "https://%CLOUDSERVER%/bizflowappdev/services/acm/login.json?app=fbs&i=" + systemUserID + "&p=" + systemUserPW;
                url = url.replaceAll("%CLOUDSERVER%", webIP);

                AnsibleREST restAPI = new AnsibleREST();
                JSONObject rJson = restAPI.executeLogin_Without_Cert(url,"GET");

                if(restAPI.getCookie() == null) {
                    return getErrJSON("Cannot login the customer service:" + webIP);
                }
                String jsessionid = restAPI.getCookie().getValue();

                url = "/bizflowappdev/services/data/get/update.version-getAppDevVersion.json";
                String theUrl = "https://" + webIP + url;


                String spec = theUrl;
                JSONObject prdVersionJson = new JSONObject();
                prdVersionJson = getServiceVersionInfoUsingConnection(spec, jsessionid, prdVersionJson);

                url = "/bizflowappdev/services/data/get/update.version-getBizFlowVersion.json";
                url = "https://" + webIP + url;

                prdVersionJson = getServiceVersionInfoUsingConnection(url, jsessionid, prdVersionJson);
                result = new JSONObject(prdVersionJson.toString());
                if(prdVersionJson == null || prdVersionJson.length() < 3){
                    result.put("result", "false");
                } else {
                    result.put("result", "true");
                    result.put("OSTYPE", osType);
                    result.put("SSERVICEID", serviceID);
                    result = getNextScheduledVersion(result);
                }
            }
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("count",0);
            result.put("result","false");
            result.put("faultString",e.getMessage());
            result.put("data", new JSONArray());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={} %s", "callAppDevAPI_ForSelect", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static JSONObject getErrJSON(String errMsg) throws Exception {
        JSONObject errJson = new JSONObject();
        errJson.put("result","false");
        errJson.put("faultString",errMsg);
        return errJson;
    }

    private static JSONObject getNextScheduledVersion(JSONObject verJson) throws Exception{
        JSONObject nextInfoJson = new JSONObject();
        if(verJson.has("APPDEV_VERSION")){
            String currentAppDevVersion = verJson.getString("APPDEV_VERSION");
            String appType = "A";
            String osType = verJson.getString("OSTYPE");
            nextInfoJson.put("PREVERSIONSTR", currentAppDevVersion);
            nextInfoJson.put("OSTYPE", osType);
            nextInfoJson.put("APPTYPE", appType);

            String queryName = "upgrade.getNextScheduledExistAppDevVersion";
            String queryAction =  QueryActionConstant.SelectQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, (HashMap)JSONUtil.jsonStringToMap(nextInfoJson.toString()));

            JSONObject rJson = null;
            if(resultObject instanceof JSONArray){
                JSONArray rArr = (JSONArray) resultObject;
                if(rArr.length() > 0){
                    rJson = rArr.getJSONObject(0);
                } else {
                    rJson = new JSONObject();
                }
            } else if (resultObject instanceof JSONObject){
                rJson = (JSONObject) resultObject;
            }

            if(rJson.has("TO_APPDEV_VERSION")){
                verJson.put("TO_APPDEV_VERSION", rJson.getString("TO_APPDEV_VERSION"));
            }

        }
        if(verJson.has("BIZFLOW_VERSION")){
            String currentAppDevVersion = verJson.getString("BIZFLOW_VERSION");
            String appType = "B";
            String osType = verJson.getString("OSTYPE");
            nextInfoJson.put("PREVERSIONSTR", currentAppDevVersion);
            nextInfoJson.put("OSTYPE", osType);
            nextInfoJson.put("APPTYPE", appType);

            String queryName = "upgrade.getNextScheduledExistBizFlowVersion";
            String queryAction =  QueryActionConstant.SelectQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, (HashMap)JSONUtil.jsonStringToMap(nextInfoJson.toString()));

            JSONObject rJson = null;
            if(resultObject instanceof JSONArray){
                JSONArray rArr = (JSONArray) resultObject;
                if(rArr.length() > 0){
                    rJson = rArr.getJSONObject(0);
                } else {
                    rJson = new JSONObject();
                }
            } else if (resultObject instanceof JSONObject){
                rJson = (JSONObject) resultObject;
            }

            if(rJson.has("TO_BIZFLOW_VERSION")){
                verJson.put("TO_BIZFLOW_VERSION", rJson.getString("TO_BIZFLOW_VERSION"));
            }

        }
        return verJson;
    }

    private static JSONObject getServiceVersionInfoUsingConnection(String fullUrl, String jsessionid, JSONObject responseJson) throws Exception{
        try{
            URL sUrl = new URL(fullUrl);
            HttpURLConnection subConn = (HttpURLConnection) sUrl.openConnection();
            subConn.setDoInput(true);
            subConn.setDoOutput(true);
            subConn.setRequestMethod("POST");
            subConn.setRequestProperty("Cookie", "JSESSIONID="+jsessionid);
            int responseCode = subConn.getResponseCode();

            logger.debug("responseCode %s", responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(subConn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                JSONArray responseJsonArr = new JSONArray(sb.toString());
                logger.debug(responseJsonArr);
                String versionStr = null;
                for (Object o : responseJsonArr) {
                    JSONObject jsonLineItem = (JSONObject) o;
                    if(jsonLineItem.has("VERSION")){
                        versionStr = jsonLineItem.getString("VERSION");
                    }

                    if(!jsonLineItem.has("PRODUCT")){
                        continue;
                    }
                    String prd = jsonLineItem.getString("PRODUCT");
                    if("AppDev".equals(prd)){
                        String version = jsonLineItem.getString("VERSION");
                        responseJson.put("APPDEV_VERSION", version);
                    } else if ("BIO".equals(prd)){
                        String version = jsonLineItem.getString("VERSION");
                        responseJson.put("BIO_VERSION", version);
                    } else if ("bizflowserver".equals(prd)){
                        String version = jsonLineItem.getString("VERSION");
                        responseJson.put("BIZFLOW_VERSION", version);
                    }
                }
                if(responseJsonArr.toString().indexOf("PRODUCT") < 0 && versionStr != null){
                    responseJson.put("BIZFLOW_VERSION", "14.0.0.0000.00");
                }
                logger.debug("Url Call OK!: {}", fullUrl);
            } else {
                throw new Exception("Fail to Call Url, responseCode = " + responseCode + ", Url = " + fullUrl);
            }
            subConn.disconnect();
        } catch (Exception e){
            logger.error("An error occurred while trying to call API: %s", fullUrl);
            e.printStackTrace();
        }
        return responseJson;
    }

    private static int preInsertToUpgradeInfo(String serviceID, String msg) throws Exception {
        JSONObject updInfoJson = new JSONObject();
        updInfoJson.put("SSERVICEID", serviceID);
        updInfoJson.put("UPDSTATUS", "U");
        updInfoJson.put("FROMUPGRADEDEFID", 0);
        updInfoJson.put("TOUPGRADEDEFID", 0);

        int upgradeID = insertServiceUpgradeInfo(updInfoJson, "MSG", msg);

        return upgradeID;
    }

    public static Object goUpdateNextVersion(String district, String sessionName, HashMap parameter) {
        JSONObject result = null;
        long timeS = System.currentTimeMillis();

        JSONObject jsonParam = null;

        String serviceID = null;
        String systemUserID = null;
        String systemUserPW = null;

        String appType = null;
        String url = null;

        try{
            logger.debug("goUpdateNextVersion Started!");
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            systemUserID = CustomServiceConfigUtil.getProperty("appdev.client.system.user.id", "appdev");
            systemUserPW = CustomServiceConfigUtil.getProperty("appdev.client.system.user.pw", "");

            systemUserPW = SecurityCipher.getInstance().decipherBase64(systemUserPW);

            boolean debug = false;
            if(jsonParam.has("TEST")){
                if(jsonParam.getString("TEST").equalsIgnoreCase("Y")){
                    debug = true;
                    systemUserID = "hkim";
                    systemUserPW = "1";
                }
            }

            if(!jsonParam.has("SSERVICEID")){
                throw new Exception("Cannot find the parameter SSERVICEID");
            }
            if(!jsonParam.has("APPTYPE")){
                throw new Exception("Cannot find the parameter APPTYPE");
            }

            serviceID = jsonParam.getInt("SSERVICEID") + "";
            appType = jsonParam.getString("APPTYPE");
            if(!appType.equals("A") && !appType.equals("B")){
                throw new Exception("The parameter value APPTYPE must be 'A' or 'B' but the real value is "+ appType);
            }

            String queryName = "service.selectIPsByServiceID";
            String queryAction =  QueryActionConstant.SelectQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, (HashMap)JSONUtil.jsonStringToMap(jsonParam.toString()));

            String webIP = null;
            String osType = null;
            JSONObject ipsJson = null;
            if(resultObject instanceof JSONArray){
                JSONArray jArr = (JSONArray) resultObject;
                if(jArr == null || jArr.length() < 1){
                    throw new Exception("Cannot find network information by serviceID: " + serviceID);
                }
                ipsJson = jArr.getJSONObject(0);
            } else if(resultObject instanceof JSONObject){
                ipsJson = (JSONObject) resultObject;
            }
            webIP = ipsJson.getString("WEBIP");
            osType = ipsJson.getString("OSTYPE");

            if(debug){
                webIP = "bizcloud.bizflow.com";
            }
            if(webIP == null || webIP.length() < 4){
                throw new Exception("Cannot find WebIP and network information by serviceID: " + serviceID);
            }

            int upgradeID = preInsertToUpgradeInfo(serviceID, "Initiated at " + getCurrentDateTime());

            String protocol = "https";
            if(protocol.equals("https")) {

                url = "https://%CLOUDSERVER%/bizflowappdev/services/acm/login.json?app=fbs&i=" + systemUserID + "&p=" + systemUserPW;
                url = url.replaceAll("%CLOUDSERVER%", webIP);

                AnsibleREST restAPI = new AnsibleREST();
                JSONObject rJson = restAPI.executeLogin_Without_Cert(url,"GET");

                String jsessionid = restAPI.getCookie().getValue();

                JSONObject prdVersionJson = new JSONObject();
                JSONObject nextInfoJson = new JSONObject();
                JSONObject nextVersionInfoJson = null;
                JSONObject returnAPIOutJson = null;
                String theUrl = null;
                if(appType.equals("A")){
                    url = "/bizflowappdev/services/data/get/update.version-getAppDevVersion.json";
                    theUrl = "https://" + webIP + url;

                    prdVersionJson = getServiceVersionInfoUsingConnection(theUrl, jsessionid, prdVersionJson);

                    String currentAppDevVersion = prdVersionJson.getString("APPDEV_VERSION");

                    nextInfoJson.put("PREVERSIONSTR", currentAppDevVersion);
                    nextInfoJson.put("OSTYPE", osType);
                    nextInfoJson.put("APPTYPE", appType);

                    queryName = "upgrade.getNextScheduledExistAppDevVersion";
                    queryAction =  QueryActionConstant.SelectQuery;
                    resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, (HashMap)JSONUtil.jsonStringToMap(nextInfoJson.toString()));

                } else if(appType.equals("B")){
                    url = "/bizflowappdev/services/data/get/update.version-getBizFlowVersion.json";
                    url = "https://" + webIP + url;

                    prdVersionJson = getServiceVersionInfoUsingConnection(url, jsessionid, prdVersionJson);

                    String currentBizFlowVersion = prdVersionJson.getString("BIZFLOW_VERSION");

                    nextInfoJson.put("PREVERSIONSTR", currentBizFlowVersion);
                    nextInfoJson.put("OSTYPE", osType);
                    nextInfoJson.put("APPTYPE", appType);

                    queryName = "upgrade.getNextScheduledExistBizFlowVersion";
                    queryAction =  QueryActionConstant.SelectQuery;
                    resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, (HashMap)JSONUtil.jsonStringToMap(nextInfoJson.toString()));

                }

                if(resultObject instanceof JSONArray){
                    JSONArray rArr = (JSONArray) resultObject;
                    nextVersionInfoJson = rArr.getJSONObject(0);
                } else if (resultObject instanceof JSONObject){
                    nextVersionInfoJson = (JSONObject) resultObject;
                }
                nextVersionInfoJson = nextVersionInfoJson.put("SSERVICEID", serviceID);
                if(getUpdatingServiceCount(nextVersionInfoJson) > 0){
                    throw new Exception("Current updating now!");
                }

                String urlForUpdate = null;
                urlForUpdate = CustomServiceConfigUtil.getProperty("update.initiate.rest.api.url");

                urlForUpdate = urlForUpdate.replace("%SERVERIP%", webIP);

                JSONObject installJson = new JSONObject(nextVersionInfoJson.toString());
                Object retObj = executeSSLAPI_Without_Certificate(urlForUpdate, false, "POST", installJson);
                if(retObj instanceof JSONObject){
                    returnAPIOutJson = (JSONObject) retObj;
                } else if(retObj instanceof JSONArray){
                    JSONArray jArr = (JSONArray) retObj;
                    returnAPIOutJson = jArr.getJSONObject(0);
                }

                if(!returnAPIOutJson.has("result")){
                    returnAPIOutJson = returnAPIOutJson.put("result", "false");
                }
                nextVersionInfoJson = nextVersionInfoJson.put("SSERVICEID", serviceID);
                if(returnAPIOutJson.getString("result").equals("true")){
                    nextVersionInfoJson = nextVersionInfoJson.put("UPDSTATUS", "U");
                } else {
                    nextVersionInfoJson = nextVersionInfoJson.put("UPDSTATUS", "F");
                }
                nextVersionInfoJson = nextVersionInfoJson.put("UPGRADEID", upgradeID);

                String logValue = null;
                if(returnAPIOutJson.getString("result").equals("true")){
                    Object insObj = updateServiceUpgradeInfo(nextVersionInfoJson, "MSG", "\nCall Install API at " + getCurrentDateTime());
                } else {
                    logValue = "\nCall Install API but Error at " + getCurrentDateTime();
                    if(returnAPIOutJson.has("errorMessage")) {
                        logValue += "\n ErrorMessage=" + returnAPIOutJson.getString("errorMessage");
                    }
                    Object insObj = updateServiceUpgradeInfo(nextVersionInfoJson, "MSG", logValue);
                }

            }
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            result = new JSONObject();
            result.put("count",0);
            result.put("result","false");
            result.put("faultString",e.getMessage());
            result.put("data", new JSONArray());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "goUpdateNextVersion", (System.currentTimeMillis() - timeS));
        }
        return result;
    }


    public static int getUpdatingServiceCount(JSONObject jsonObj) throws Exception {
        logger.debug("--START getUpdatingServiceCount---");

        String queryName = "upgrade.getCustomerServiceUpdatingCount";
        String queryAction = QueryActionConstant.SelectQuery;

        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, JSONUtil.jsonStringToMap(jsonObj.toString()));
        int ret = -1;
        JSONObject jSon = null;
        if(resultObject instanceof JSONArray){
            JSONArray jArr = (JSONArray) resultObject;
            jSon = jArr.getJSONObject(0);
        } else if(resultObject instanceof JSONObject){
            jSon = (JSONObject) resultObject;
        }
        if(jSon.has("UCNT")){
            ret = jSon.getInt("UCNT");
        }
        logger.debug("--END getUpdatingServiceCount--- {}", ret);
        return ret;
    }

}
