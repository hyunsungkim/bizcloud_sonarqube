package com.bizflow.io.custom.services.bizcloud.util;

import com.bizflow.io.core.db.mybatis.model.QueryActionConstant;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import com.bizflow.io.services.data.dao.util.SqlDAOUtil;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Admin {
    private Admin() {
        throw new IllegalStateException("Admins class");
    }

    private static final Logger logger = LogManager.getLogger(Admin.class);
    public static Object selectCompanyInfoList(String district, String sessionName, HashMap parameter) {
        logger.debug("Admin.selectCompanyInfoList is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has("filters") && jsonParam.has("queryString")){
                if(jsonParam.get("filters") instanceof JSONObject) {
                    jsonParam = jsonParam.getJSONObject("filters");
                } else if(jsonParam.get("filters") instanceof JSONArray) {
                    jsonParam = CommonUtil.getFieldAndValue(jsonParam.getJSONArray("filters"), jsonParam);
                }
                jsonParam = CommonUtil.addStartNumEndNum(jsonParam);
                parameter = (HashMap)jsonParam.toMap();
            }

            String queryName = "admin.selectCompanyInfoList";
            String queryAction =  QueryActionConstant.SelectQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, parameter);

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "selectCompanyInfoList", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object deleteCompany(String district, String sessionName, HashMap parameter) {
        logger.debug("Admin.deleteCompany is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has("data") ){
                Object dataObj = jsonParam.get("data");
                JSONObject dataJson = null;
                if(dataObj instanceof JSONObject) {
                    dataJson = (JSONObject) dataObj;
                } else if(dataObj instanceof JSONArray) {
                    JSONArray dataArr = (JSONArray)dataObj;
                    dataJson = dataArr.getJSONObject(0);
                }
                if(dataJson == null || !dataJson.has("COMPID")){
                    throw new Exception("Cannot find the parameter 'COMPID'");
                }
                jsonParam.put("COMPID", CommonUtil.getNumberFromJsonParam(dataJson, "COMPID"));
                parameter = (HashMap)jsonParam.toMap();
            }

            if(!jsonParam.has("COMPID")){
                throw new Exception("Need parameter COMPID!");
            }
            String queryName = "admin.selectUserIdsByCompId";
            String queryAction =  QueryActionConstant.SelectQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, parameter);
            JSONArray userInfos = null;
            if(resultObject instanceof JSONArray){
                userInfos = (JSONArray)resultObject;
            }else if(resultObject instanceof JSONObject){
                JSONObject oneUser = (JSONObject)resultObject;
                userInfos = new JSONArray(oneUser);
            }
            for(int i =0;i<userInfos.length();i++){
                JSONObject userInfo = userInfos.getJSONObject(i);
                deleteUserOnAuthServer(userInfo);
            }

            queryName = "admin.deleteCompanyInfo";
            queryAction =  QueryActionConstant.UpdateQuery;
            resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, parameter);

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "deleteCompany", (System.currentTimeMillis() - timeS));
        }
        return result;
    }


    public static Object insertCompanyInfo(String district, String sessionName, HashMap parameter) {
        logger.debug("Admin.insertCompanyInfo is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        HashMap dataMap = new HashMap();
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));

            if(jsonParam.has("data") ){
                Object dataObj = jsonParam.get("data");
                JSONObject dataJson = null;
                if(dataObj instanceof JSONObject) {
                    dataJson = (JSONObject) dataObj;
                } else if(dataObj instanceof JSONArray) {
                    JSONArray dataArr = (JSONArray)dataObj;
                    dataJson = dataArr.getJSONObject(0);
                }
                if(dataJson == null || !dataJson.has("COMPNAME")){
                    throw new Exception("Cannot find the parameter 'COMPNAME' or have the null value.");
                }
                if(dataJson == null || !dataJson.has("ADDRLINE1")){
                    throw new Exception("Cannot find the parameter 'ADDRLINE1' or have the null value.");
                }
                if(dataJson == null || !dataJson.has("CITY")){
                    throw new Exception("Cannot find the parameter 'CITY' or have the null value.");
                }
                if(dataJson == null || !dataJson.has("STATEREGION")){
                    throw new Exception("Cannot find the parameter 'STATEREGION' or have the null value.");
                }
                if(dataJson == null || !dataJson.has("POSTALCODE")){
                    throw new Exception("Cannot find the parameter 'POSTALCODE' or have the null value.");
                }
                if(dataJson == null || !dataJson.has("PHONENUMBER")){
                    throw new Exception("Cannot find the parameter 'PHONENUMBER' or have the null value.");
                }
                dataMap = (HashMap)dataJson.toMap();
            }

            String queryName = "admin.insertCompanyInfo";
            String queryAction =  QueryActionConstant.InsertQuery;
            Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, dataMap);

            result = new JSONObject();
            result.put("data", resultObject);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "insertCompanyInfo", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static String deleteUserOnAuthServer(JSONObject runInfoJson) throws Exception {
        String retValue = "";

        if(!runInfoJson.has("USERID")){
            throw new Exception("Cannot find USERID");
        }
// curl -k -H "ClientId: zd4d9c8d433a34aaead125c79e15f4861" -X POST \
// https://bizcloud.bizflow.com/bizflowauth/services/data/run/auth.user-DeleteUserInfo.json \
// -d ' {"deleteFields":[{"userId":	%USERID% }]}'

        int userId = CommonUtil.getNumberFromJsonParam(runInfoJson, "USERID");
        String curlCmd = CustomServiceConfigUtil.getProperty("delete.authserver.user.curl.api");
        curlCmd = curlCmd.replaceAll("%USERID%", userId+"");
        logger.debug("Curl Cmd to delete the User= {}", curlCmd);
        Process proc = Runtime.getRuntime().exec(curlCmd);

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(proc.getInputStream()));

        String line = "";
        while ((line = reader.readLine()) != null) {
            logger.debug("%n", line);
            retValue += line;
        }

        proc.waitFor();

        return retValue;
    }

}
