package com.bizflow.io.custom.services.bizcloud.util;

import com.bizflow.io.core.db.mybatis.model.QueryActionConstant;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.custom.services.bizcloud.config.BizCoveConfigBag;
import com.bizflow.io.services.data.dao.util.SqlDAOUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CommonUtil {
    private CommonUtil() {
        throw new IllegalStateException("CommonUtil class");
    }

    public static JSONObject getCompanyInfo(int compId) throws Exception{
        JSONObject data = null;
        String queryName = "admin.selectCompanyInfo";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("COMPID", compId);

        String queryAction =  QueryActionConstant.SelectQuery;
        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, paramMap);

        if (isResultExist(resultObject) ) {
            if (resultObject instanceof Integer) {
                int res = ((Integer) resultObject).intValue();
                data = new JSONObject();
                data.put("result",false);
            } else if (resultObject instanceof JSONObject) {
                data = (JSONObject) resultObject;
                data.put("result", true);
            } else if (resultObject instanceof JSONArray) {
                data = ((JSONArray) resultObject).getJSONObject(0);
                data.put("result", true);
            } else if (resultObject instanceof HashMap) {
                data = new JSONObject(JSONUtil.mapToJsonString((Map)resultObject));
                data.put("result",false);
            } else {
                data = (JSONObject) resultObject;
                data.put("result",false);
                throw new Exception("Cannot Get Company Info:"+ resultObject.toString());
            }
        }

        return data;
    }

    public static int getTotalCount(String selectQuery, JSONObject jsonParam) throws Exception{
        JSONObject data = null;
        String queryName = selectQuery;
        Map<String, Object> paramMap = null;
        paramMap = jsonParam.toMap();
        String queryAction =  QueryActionConstant.SelectQuery;
        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, paramMap);
        int len = -1;
        if (resultObject instanceof Collection) {
            len = ((Collection) resultObject).size();
        } else if (resultObject instanceof JSONObject) {
            len = 1;
        } else if (resultObject instanceof JSONArray) {
            JSONArray jArr = (JSONArray)resultObject;
            len = jArr.length();
        } else {
            throw new Exception("Cannot know the type of results:"+resultObject.toString());
        }

        return len;
    }

    protected static boolean isResultExist(Object resultObject) {
        boolean exist = true;
        if (resultObject instanceof Collection) {
            exist = ((Collection) resultObject).size() > 0;
        } else if (resultObject instanceof Integer) {
            exist = ((int) resultObject) > 0;
        }

        return exist;
    }


    public static String getStringFromJsonParam(JSONObject jsonParam, String paramName) throws Exception {
        String paramValue = null;
        if(jsonParam.has(paramName) && !jsonParam.get(paramName).equals(null)) {
            paramValue = jsonParam.getString(paramName);
        } else {
            throw new Exception(paramName + " information is not recognized. : " + paramValue);
        }

        return paramValue;
    }

    public static int getNumberFromJsonParam(JSONObject jsonParam, String paramName) throws Exception {
        int paramValue = -1;
        if(jsonParam.has(paramName) && !jsonParam.get(paramName).equals(null)) {
            paramValue = jsonParam.getInt(paramName);
        } else {
            throw new Exception(paramName + " information is not recognized. : " + paramValue);
        }

        return paramValue;
    }

    protected static String padLeftZeros(String inputString, int length) {
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append('0');
        }
        sb.append(inputString);

        return sb.toString();
    }

    public static JSONObject getFieldAndValue(JSONArray filters, JSONObject jsonParam){
        for(int i=0;i < filters.length(); i++){
            JSONObject aFilter = filters.getJSONObject(i);
            String filterName = null;
            String filterValue = null;
            if(aFilter.has("field") && !aFilter.get("field").equals("null")){
                filterName = aFilter.getString("field");

                if(aFilter.has("value") && !aFilter.get("value").equals("null")){
                    Object valObj = aFilter.get("value");
                    if(valObj instanceof Integer){
                        int val = aFilter.getInt("value");
                        jsonParam.put(filterName, val);
                    } else if(valObj instanceof String){
                        filterValue = aFilter.getString("value");
                        jsonParam.put(filterName, filterValue);
                    }
                }
            }
        }
        return jsonParam;
    }

    public static JSONObject addStartNumEndNum(JSONObject jsonParam){
        int pageNo = -1;
        int pageSize = -1;
        int startNum = -1;
        int endNum = -1;
        if(jsonParam.has("pageNo") && !jsonParam.get("pageNo").equals(null)){
            pageNo = jsonParam.getInt("pageNo");
        }
        if(jsonParam.has("pageSize") && !jsonParam.get("pageSize").equals(null)){
            pageSize = jsonParam.getInt("pageSize");
        }
        if(pageNo == -1 || pageSize == -1){
            startNum = 1;
            endNum = 10;
        } else {
            startNum = (pageNo - 1) * pageSize + 1;
            endNum = pageNo * pageSize;
        }
        jsonParam.put("STARTNUM", startNum);
        jsonParam.put("ENDNUM", endNum);
        return jsonParam;
    }

    public static String getToday(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleformat = new SimpleDateFormat("MMddyyyy_HHmm" );
        return simpleformat.format(cal.getTime());
    }

    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
