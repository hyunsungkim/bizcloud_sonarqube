package com.bizflow.io.custom.services.bizcloud.util;

import java.io.File;
import java.io.FilenameFilter;

public class InfoFileFilter  implements FilenameFilter {
    private int compId = -1;
    private int serviceId = -1;

    public InfoFileFilter(int compId, int sServiceId){
        this.compId = compId;
        this.serviceId = sServiceId;
    }
    public boolean accept(File dir, String name) {
        return (name.startsWith("" + this.serviceId) || name.startsWith("" + this.compId) ) && name.endsWith(".info");
    }
}
