package com.bizflow.io.custom.services.bizcloud.util;

import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class FileNavi {
    private static final Logger logger = LogManager.getLogger(FileNavi.class);

    public static void subDirList(String source){
        File dir = new File(source);
        File[] fileList = dir.listFiles();
        try{
            for(int i = 0 ; i < fileList.length ; i++){
                File file = fileList[i];
                if(file.isFile()){

                    logger.debug("File Name =  %t", file.getName());
                    logger.debug("File Name = %t", file.lastModified());
                }else if(file.isDirectory()){
                    logger.debug("Dir Name = %t", file.getName());
                    subDirList(file.getCanonicalPath().toString());
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }


    public static void main(String[] args){
        String networkJsonStr = FileUtil.getFileContents(new File("D:/Project/BIOCloud/web/WEB-INF/templates/cloud/SaaSFileToNetworkParam.json"));
        networkJsonStr = StringUtils.replace(networkJsonStr, "$SERVICE_ID$", "" + 23432);
        logger.debug("networkJsonStr= %s", networkJsonStr);

//        FileNavi.subDirList(args[0]);
    }

}
