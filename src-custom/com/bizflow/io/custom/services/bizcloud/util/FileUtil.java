package com.bizflow.io.custom.services.bizcloud.util;

import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Vector;

public class FileUtil {
    private static final Logger logger = LogManager.getLogger(FileNavi.class);
    private static File[] EMPTY_FILE_ARRAY = {};

    public static String getFileContents(File file){
        try {
            String strings = FileUtils.readFileToString(file, "UTF-8");
            logger.debug(strings);
            return strings;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getArtifactFileContents(String osType, String uuid, String fileType){
        String filePath = null;
        try {
            String artifactPath = null;
            if(osType.equals("win")) {
                artifactPath = CustomServiceConfigUtil.getProperty("ansible.win.service.runner.artifact.path");
            } else {
                artifactPath = CustomServiceConfigUtil.getProperty("ansible.service.runner.artifact.path");
            }
            filePath = artifactPath + File.separator + uuid + File.separator + fileType;
            String strings = FileUtils.readFileToString(new File(filePath), "UTF-8");
            logger.debug("Artifact File Path: %s", filePath);
            logger.debug(strings);
            return strings;
        } catch (FileNotFoundException fne) {
            logger.debug("File Not Found, That is running now : %s", filePath);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        if (!destFile.exists()) {
            destFile.createNewFile();
        }
        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();

        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
    }

    public static void copyDirectory(File sourceLocation , File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }

            String[] children = sourceLocation.list();
            for (int i=0; i<children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    public static void writeStrToFile(File targetFile, String contents){
        logger.debug("Write String to file %s" ,targetFile.getAbsolutePath());
        logger.debug("contents= %s", contents);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile))) {
            writer.write(contents);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static File[] addFileArray(File[] arr1, File[] arr2) throws Exception{
        File[] sArr = null;
        int sum1 = -1;
        int sum2 = -1;
        int ind = 0;
        if(arr1 == null && arr2 == null){
            return EMPTY_FILE_ARRAY;
        } else if (arr1 == null){
            return arr2;
        } else if (arr2 == null){
            return arr1;
        }
        sum1 = arr1.length;
        sum2 = arr2.length;
        sArr = new File[sum1 + sum2];
        for(ind = 0;ind < arr1.length; ind++){
            sArr[ind] = arr1[ind];
        }

        for(int i = 0;i < arr2.length; i++){
            sArr[ind++] = arr2[i];
        }

        return sArr;
    }

    public static void main(String[] args) {
        // create a file object pointing to the directory
        File directory = new File("D:\\tmp\\results\\1");
        // pass an object of our file filter while listing files
        File[] listOfMatchingFiles  = directory.listFiles(new InfoFileFilter(1161,10320));
        // iterate over the list of files
        for (File file : listOfMatchingFiles) {
            // print their path
            logger.debug(file.getAbsolutePath());
        }
    }

}
