package com.bizflow.io.custom.services.bizcloud.util;

import com.bizflow.io.core.db.mybatis.model.QueryActionConstant;
import com.bizflow.io.core.json.JSONArray;
import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.services.custom.util.CustomServiceConfigUtil;
import com.bizflow.io.services.data.dao.util.SqlDAOUtil;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.bizflow.io.custom.services.bizcloud.license.LicenseManager;

public class BizCloudLicense {
    private static final Logger logger = LogManager.getLogger(BizCloudLicense.class);
    private static final String OS = System.getProperty("os.name").toLowerCase();

    private static LicenseManager licMan =null;
    public static Object createLicenseFile(String district, String sessionName, HashMap parameter){
        logger.debug("BizCloudLicense.createLicenseFile is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        String licenseTemplateFile = null;
        String licFileRepository = null;

        try{
            licenseTemplateFile = CustomServiceConfigUtil.getProperty("license.template.file.name");

            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String osType = CommonUtil.getStringFromJsonParam(jsonParam,"OSTYPE");
            if(OS.indexOf("win") >= 0){
                licFileRepository = CustomServiceConfigUtil.getProperty("admin.on.window.file.repository");
            } else if(OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0){
                if(osType.equals("win")){
                    licFileRepository = CustomServiceConfigUtil.getProperty("cloud.on.window.file.repository");
                } else {
                    licFileRepository = CustomServiceConfigUtil.getProperty("admin.on.linux.file.repository");
                }
            } else {
                throw new Exception("What type of os is this? OS=" + OS);
            }

            JSONObject licenseInfoJson = new JSONObject();
            JSONObject serverJson = new JSONObject();
            JSONObject userLicenseJson = new JSONObject();

            String ip = CommonUtil.getStringFromJsonParam(jsonParam, "IP");
            String instanceName = CommonUtil.getStringFromJsonParam(jsonParam, "INSTANCENAME");
            String expirationDate = "unlimited";

            int updUserId = CommonUtil.getNumberFromJsonParam(jsonParam, "UPDUSERID");
            int licAppDevNum = CommonUtil.getNumberFromJsonParam(jsonParam, "APPDEVLICNUM");
            int licBPMNum = CommonUtil.getNumberFromJsonParam(jsonParam, "BPMLICNUM");
            int licUserNum = CommonUtil.getNumberFromJsonParam(jsonParam, "USERLICNUM");
            int serviceId = CommonUtil.getNumberFromJsonParam(jsonParam, "SSERVICEID");
            int compId = CommonUtil.getNumberFromJsonParam(jsonParam, "COMPID");
            String COMPANY_NAME = null;

            JSONObject compInfo = CommonUtil.getCompanyInfo(compId);
            if(compInfo != null && compInfo.has("COMPNAME") && !compInfo.getString("COMPNAME").equals("null")){
                COMPANY_NAME = compInfo.getString("COMPNAME");
                jsonParam.put("COMPANY_NAME", COMPANY_NAME);
            } else {
                throw new Exception("Cannot find the Company Information by COMPID you gave, ? COMPID=" + compId);
            }
            serverJson.put("COMPANY_NAME", COMPANY_NAME);
            serverJson.put("EXPIRATION_DATE",expirationDate);
            serverJson.put("IP", ip);

            userLicenseJson.put("APPDEVLICNUM", licBPMNum);
            userLicenseJson.put("BPMLICNUM", licBPMNum);
            userLicenseJson.put("USERLICNUM", licUserNum);

            int licEventId = insertLicenseInfo(updUserId, serviceId, COMPANY_NAME, instanceName);

            String licenseNo = CommonUtil.padLeftZeros(""+licEventId, 10);
            serverJson.put("LICENSE_NO", licenseNo);

            jsonParam.put("LICEVENTID", licEventId);
            jsonParam.put("LICNO", licenseNo);

            licenseInfoJson.put("Server", serverJson);
            licenseInfoJson.put("User License", userLicenseJson);

            jsonParam.put("LICENSE_INFO", licenseInfoJson);

            licMan = new LicenseManager(licenseTemplateFile, licFileRepository);
            JSONObject lic_info = jsonParam.getJSONObject("LICENSE_INFO");

            String fileName = licMan.generateLicenseFile(lic_info);
            if(fileName == null) {
                throw new Exception("Fail to create the license file. File Path:" + fileName);
            }

            int ret = updateLicenseInfo(licEventId, ip,expirationDate, licAppDevNum, licBPMNum, licUserNum);
            result = new JSONObject();
            result.put("_LICENSE_FILE", fileName);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendMailToContractors", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static String createLicenseFileForTemp(String district, String sessionName, JSONObject parameter){
        logger.debug("BizCloudLicense.createLicenseFileForTemp is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        String licenseTemplateFile = null;
        String licFileRepository = null;

        String fileName= null;
        try{
            licenseTemplateFile = createTempFile();

            if(OS.indexOf("win") >= 0){
                licFileRepository = ".";
            } else if(OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0){
                licFileRepository = ".";
            } else {
                throw new Exception("What type of os is this? OS=" + OS);
            }
            licMan = new LicenseManager(licenseTemplateFile, licFileRepository);
            jsonParam = parameter;
            JSONObject lic_info = jsonParam.getJSONObject("LICENSE_INFO");

            fileName = licMan.generateLicenseFile(lic_info);

            if(district != null && district.equals("REST")) {

                HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
                HttpServletResponse response = RequestIdentifierKeeper.getRequestIdentifier().getResponse();
                File file = FileUtils.getFile(fileName);

                String destFileOnWebPath = CustomServiceConfigUtil.getProperty("web.root.physical.file.path")
                        + File.separator + "lic" + File.separator + file.getName();

                File fileToMove = FileUtils.getFile(destFileOnWebPath);
                FileUtils.moveFile(file, fileToMove);


                String webUrl = CustomServiceConfigUtil.getProperty("bizcloud.web.root.url", "whaturl");
                webUrl += "/sw/lic/" + file.getName();
                fileName = webUrl;
            }
            result = new JSONObject();
            result.put("_LICENSE_FILE", fileName);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
//            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendMailToContractors", (System.currentTimeMillis() - timeS));
        }
        return fileName;
    }


    public static Object createLicToWeb(String district, String sessionName, HashMap parameter){
        logger.debug("BizCloudLicense.createLicToWeb is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        String ipString = null;
        String passPhrase = null;
        String fileName= null;
        try{

            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            if(!jsonParam.has("IP") || jsonParam.getString("IP").length() < 7){
                throw new Exception("No valid IP or IP String(delimeter: ,)");
            }
            ipString = jsonParam.getString("IP");
            if(!jsonParam.has("PASSPHRASE") || jsonParam.getString("PASSPHRASE").length() < 7){
                throw new Exception("No valid PASSPHRASE value:" + jsonParam.getString("PASSPHRASE"));
            }
            passPhrase = jsonParam.getString("PASSPHRASE");

            JSONObject job = new JSONObject(paramString);
            JSONObject serverJson = job.getJSONObject("LICENSE_INFO").getJSONObject("Server");
            serverJson.put("IP", ipString);
//System.out.println("License Info="+job.toString());
            fileName = BizCloudLicense.createLicenseFileForTemp("REST","", job);

            result = new JSONObject();
            result.put("FILE_PATH", fileName);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
//            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "createLicToWeb", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    public static Object displayFromLicenseFile(String district, String sessionName, HashMap parameter){
        logger.debug("BizCloudLicense.displayFromLicenseFile is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;

        String LICENSE_FILE_PATH = null;
        File licenseFile = null;
        try{
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            LICENSE_FILE_PATH = jsonParam.getString("LICENSE_FILE_PATH");
            licMan = new LicenseManager(LICENSE_FILE_PATH);

            licenseFile = new File(LICENSE_FILE_PATH);
            String licInfo = licMan.displayFromLicenseFile(licenseFile);

            result = new JSONObject();
            result.put("licenseInfo", licInfo);
            result.put("result","true");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sendMailToContractors", (System.currentTimeMillis() - timeS));
        }
        return result;
    }

    private static JSONObject getFileJsonObject(String fileName, String fileFullPath){
        JSONObject fileJson = null;

        fileJson = new JSONObject();
        fileJson.put("name", fileName);
        fileJson.put("path", fileFullPath);

        return fileJson;
    }

    private static JSONArray removeCharInJSONArray(JSONArray inArray) {
        if(inArray == null || inArray.length() < 1)
            return inArray;
        for(int i=0; i < inArray.length();i++){
            Object item = inArray.get(i);
            if(item instanceof String){
                String itemStr = (String)item;
                if(itemStr.indexOf(",") > -1){
                    itemStr = itemStr.replaceAll(",", " ");
                    inArray.put(i, itemStr);
                }
            }
        }

        return inArray;
    }

    public static String getToday(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleformat = new SimpleDateFormat("MM/dd/yyyy");
        return simpleformat.format(cal.getTime());
    }

    public static boolean isValidInet4Address(String ip){
        String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

        return ip.matches(PATTERN);
    }


    private static int insertLicenseInfo(int updUserId, int serviceId, String companyName, String instanceName) throws Exception{
        JSONObject data = null;
        int ret = -1;
        String queryName = "license.insertLicenseInfo";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("INSTANCENAME", instanceName);
        paramMap.put("COMPNAME", companyName);
        paramMap.put("SSERVICEID", serviceId);
        paramMap.put("UPDUSERID", updUserId);

        String queryAction =  QueryActionConstant.InsertQuery;
        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, paramMap);
        if(resultObject instanceof Integer){
            ret = (Integer) paramMap.get("LICEVENTID");
        } else {
            throw new Exception("Should check the return value of insertLicenseInfo query");
        }
        return ret;
    }

    private static int updateLicenseInfo(int licEventId, String ip, String expirationDate, int licAppDev, int licBPM, int licUser) throws Exception{
        JSONObject data = null;
        int ret = -1;
        String queryName = "license.updateLicenseInfo";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("LICEVENTID", licEventId);
        paramMap.put("IP", ip);
        paramMap.put("EXPIRATION_DATE", expirationDate);
        paramMap.put("APPDEVLICNUM", licAppDev);
        paramMap.put("BPMLICNUM", licBPM);
        paramMap.put("USERLICNUM", licUser);

        String queryAction =  QueryActionConstant.UpdateQuery;
        Object resultObject = SqlDAOUtil.runSql("bizcloud", queryName, queryAction, paramMap);
        if(resultObject instanceof Integer){
            ret = ((Integer)resultObject).intValue();
        } else {
            throw new Exception("Should check the return value of updateLicenseInfo query");
        }
        return ret;
    }

    private static boolean isResultExist(Object resultObject) {
        boolean exist = true;
        if (resultObject instanceof Collection) {
            exist = ((Collection) resultObject).size() > 0;
        } else if (resultObject instanceof Integer) {
            exist = ((int) resultObject) > 0;
        }

        return exist;
    }

    public static String createTempFile(){
        String licTemplteContents = null;
        BufferedWriter writer = null;
        licTemplteContents = "[Server]\n" +
                "; Specify LICENSE_NO. This is for only management. There is no regulation about this.\n" +
                "LICENSE_NO=%LICENSE_NO%\n" +
                "COMPANY_NAME=%COMPANY_NAME%\n" +
                "\n" +
                ";Max. Count of CPU (if unspecified, unlimited)\n" +
                ";The option \"CPU=\" and \"CPU=0\" are identical.\n" +
                ";CPU license is optional. Therefore if the customer does not purchase CPU license, do not modify this option.\n" +
                "CPU=%CPU%\n" +
                "\n" +
                "; Let the value as BizFlow for BizFlow.\n" +
                "PRODUCT_NAME=%PRODUCT_NAME%\n" +
                "\n" +
                "; Multiple IPs are possible (delimiter is ',') and IPs can't be empty. \n" +
                "IP=%IP%\n" +
                "\n" +
                "; YYYY/MM/DD (if unspecified, unlimited)\n" +
                "EXPIRATION_DATE=%EXPIRATION_DATE%\n" +
                "\n" +
                "; Maximum process definition number (if unspecified, unlimited)\n" +
                "PROCESS_DEFINITION=%PROCESS_DEFINITION%\n" +
                "\n" +
                "; Maximum number of user group hierarchy\n" +
                "; Default is 10 and maximum is 50\n" +
                "USER_GROUP_HIERARCHY=%USER_GROUP_HIERARCHY%\n" +
                "\n" +
                "; Maximum number of report domain.\n" +
                "; Default is 2\n" +
                "REPORT_DOMAIN=%REPORT_DOMAIN%\n" +
                "\n" +
                "; Configurable UI\n" +
                "BIZCOVE=%BIZCOVE%\n" +
                "\n" +
                ";BizEform Designer\n" +
                "BizEform=False\n" +
                "\n" +
                ";EJB Application\n" +
                "EJBApplication=False\n" +
                "\n" +
                ";Web Service Application\n" +
                "WebServiceApplication=%WebServiceApplication%\n" +
                "\n" +
                "; Templates ?????\n" +
                "APS=%APS%\n" +
                "\n" +
                "; Templates Kit (False/True) ?????\n" +
                "APS_DEFINITIONS=%APS_DEFINITIONS%\n" +
                "\n" +
                "; Government Forms (False/True) ?????\n" +
                "APS_GOVERNMENT=%APS_GOVERNMENT%\n" +
                "\n" +
                "; ERA\n" +
                "ENA=%ENA%\n" +
                "\n" +
                "; Utilities\n" +
                "PROC_IMP_EXP=%PROC_IMP_EXP%\n" +
                "\n" +
                "; MS Project Integration\n" +
                "MSPROJECT=False\n" +
                "\n" +
                "; MS Visio Integration\n" +
                "MSVISIO=False\n" +
                "\n" +
                "; Process Simulator\n" +
                "PROCESS_SIMULATOR=False\n" +
                "\n" +
                "; EDMS Adapter\n" +
                "EDMS_HUMMINGBIRD=%EDMS_HUMMINGBIRD%\n" +
                "EDMS_DOCUMENTUM=%EDMS_DOCUMENTUM%\n" +
                "EDMS_SHAREPOINT=%EDMS_SHAREPOINT%\n" +
                "EDMS_CYBERDIGM=False\n" +
                "EDMS_ALFRESCO=%EDMS_ALFRESCO%\n" +
                "\n" +
                "\n" +
                "; PKI Adapter\n" +
                "PKI_ENTRUST=%PKI_ENTRUST%\n" +
                "PKI_VERISIGN=%PKI_VERISIGN%\n" +
                "\n" +
                ";EAI\n" +
                "EAI_BIZTALK=%EAI_BIZTALK%\n" +
                "EAI_QUEUE=%EAI_QUEUE%\n" +
                "\n" +
                ";LDAP\n" +
                "LDAP=%LDAP%\n" +
                "\n" +
                ";OfficeEngine\n" +
                "OfficeEngine=%OfficeEngine%\n" +
                "\n" +
                "; Advanced Reporting\n" +
                "REPORTING=%REPORTING%\n" +
                "\n" +
                "; WebMaker\n" +
                "WEBMAKER=False\n" +
                "\n" +
                "; AppDev\n" +
                "APPDEV=%APPDEV%\n" +
                "\n" +
                "; Light User License\n" +
                "LIGHT_USER_LICENSE=%LIGHT_USER_LICENSE%\n" +
                "\n" +
                "[User License]\n" +
                "SYSTEM_ADMIN=%SYSTEM_ADMIN%\n" +
                "GENERAL_ADMIN=%GENERAL_ADMIN%\n" +
                "DESIGNER=%DESIGNER%\n" +
                "; Number of report designer. Should be equal to or less than DESIGNER.\n" +
                "REPORT_DESIGNER=%REPORT_DESIGNER%\n" +
                "\n" +
                "; Number of WebMaker designer.\n" +
                "WEBMAKER_DESIGNER=0\n" +
                "\n" +
                "; Number of AppDev designer.\n" +
                "APPDEV_DESIGNER=%APPDEV_DESIGNER%\n" +
                "\n" +
                "; Number of named normal user licenses\n" +
                "USER=%USER%\n" +
                "\n" +
                "; Number of named report user. Should be equal to or less than USER.\n" +
                "REPORT_USER=%REPORT_USER%\n" +
                "\n" +
                "; Number of OfficeEngine users licenses\n" +
                "OFFICEENGINE_USER=%OFFICEENGINE_USER%\n" +
                "\n" +
                "; Number of concurrent normal users licenses\n" +
                "CONC_USER=%CONC_USER%\n" +
                "\n" +
                "; Number of simultaneous sessions that can be created by a concurrent normal user\n" +
                "CONC_SESSION_USER=%CONC_SESSION_USER%\n" +
                "\n" +
                "; No more logins are allowed if the # of concurrent users exceeds 105% of CONC_SESSION_USER.\n" +
                "CONC_SESSION_USER_ALLOWANCE=%CONC_SESSION_USER_ALLOWANCE%\n" +
                "\n" +
                ";Flag to Indicate If a Customer Can See Concurrent License Model. (True/False)\n" +
                "CONC_LICENSE_MODEL=%CONC_LICENSE_MODEL%\n" +
                "\n" +
                "; Number of light users\n" +
                "LIGHT_USER=%LIGHT_USER%\n";
        try{
            File f = File.createTempFile("pre","suf");
            writer = new BufferedWriter(new FileWriter(f));
            writer.write(licTemplteContents);
            return f.getAbsolutePath();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]){
        if(args.length != 1 || args[0].length() < 8){
            System.exit(1);
        } else{
            String[] ips = args[0].split(",");
            for(int i = 0 ; i < ips.length ; i++){
                if(!isValidInet4Address(ips[i])){
                    System.exit(1);
                }
            }
        }

        JSONObject job = new JSONObject(paramString);
        JSONObject serverJson = job.getJSONObject("LICENSE_INFO").getJSONObject("Server");
        serverJson.put("IP", args[0]);
        String fileName = BizCloudLicense.createLicenseFileForTemp("","", job);
/*
        File file = new File(fileName);
        String newFilePath = file.getParent() + File.separator + args[0]+".lic";
        boolean success = file.renameTo(new File(newFilePath));
        if (!success) {
            System.out.println("Failed to rename to "+newFilePath);
        }
        try {
            System.out.println("File Created: " + newFilePath);
        } catch (Exception e){}
*/
        logger.debug("Created FileName= %s", fileName);
    }

    private static String paramString = "{\n" +
            "  \"LICENSE_NO\": \"0000000001\",\n" +
            "  \"LICENSE_INFO\": {\n" +
            "    \"Server\": {\n" +
            "        \"LICENSE_NO\": \"0000000000\",\n" +
            "        \"COMPANY_NAME\": \"BizCloud Server\",\n" +
            "        \"CPU\": 0,\n" +
            "        \"PRODUCT_NAME\": \"BizFlow Cloud\",\n" +
            "        \"IP\": \"10.10.10.10\",\n" +
            "        \"EXPIRATION_DATE\": \"2021/12/31\",\n" +
            "        \"PROCESS_DEFINITION\": 9999,\n" +
            "        \"USER_GROUP_HIERARCHY\": 50,\n" +
            "        \"REPORT_DOMAIN\": 10,\n" +
            "        \"BIZCOVE\": \"True\",\n" +
            "        \"WebServiceApplication\": \"True\",\n" +
            "        \"APS\": \"False\",\n" +
            "        \"APS_DEFINITIONS\": \"False\",\n" +
            "        \"APS_GOVERNMENT\": \"False\",\n" +
            "        \"ENA\": \"True\",\n" +
            "        \"PROC_IMP_EXP\": \"True\",\n" +
            "        \"PROCESS_SIMULATOR\": \"False\",\n" +
            "        \"EDMS_HUMMINGBIRD\": \"False\",\n" +
            "        \"EDMS_DOCUMENTUM\": \"False\",\n" +
            "        \"EDMS_SHAREPOINT\": \"False\",\n" +
            "        \"EDMS_ALFRESCO\": \"False\",\n" +
            "        \"PKI_ENTRUST\": \"False\",\n" +
            "        \"PKI_VERISIGN\": \"False\",\n" +
            "        \"EAI_BIZTALK\": \"False\",\n" +
            "        \"EAI_QUEUE\": \"False\",\n" +
            "        \"LDAP\": \"True\",\n" +
            "        \"OfficeEngine\": \"True\",\n" +
            "        \"REPORTING\": \"True\",\n" +
            "        \"APPDEV\": \"True\",\n" +
            "        \"LIGHT_USER_LICENSE\": \"False\"\n" +
            "    },\n" +
            "    \"User License\": {\n" +
            "        \"SYSTEM_ADMIN\": 100,\n" +
            "        \"GENERAL_ADMIN\": 100,\n" +
            "        \"DESIGNER\": 100,\n" +
            "        \"REPORT_DESIGNER\": 100,\n" +
            "        \"APPDEV_DESIGNER\": 400,\n" +
            "        \"USER\": 400,\n" +
            "        \"REPORT_USER\": 400,\n" +
            "        \"OFFICEENGINE_USER\": 400,\n" +
            "        \"CONC_USER\": 0,\n" +
            "        \"CONC_SESSION_USER\": 0,\n" +
            "        \"CONC_SESSION_USER_ALLOWANCE\": 105,\n" +
            "        \"CONC_LICENSE_MODEL\": \"True\",\n" +
            "        \"LIGHT_USER\": 0\n" +
            "    }\n" +
            "}\n" +
            ",\n" +
            "  \"COMPANY_NAME\": \"BizFlow\",\n" +
            "  \"UPDUSERID\": \"54321\"\n" +
            "}\n";
}
