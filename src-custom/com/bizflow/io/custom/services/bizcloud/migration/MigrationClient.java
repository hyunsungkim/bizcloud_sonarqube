package com.bizflow.io.custom.services.bizcloud.migration;

public class MigrationClient {
    public MigrationClient() {
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            (new MigrationClientInterp()).client();
        } else {
            (new MigrationClientParam()).client(args);
        }

    }
}
