package com.bizflow.io.custom.services.bizcloud.migration;

import com.bizflow.io.custom.services.bizcloud.migration.model.Workspace;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

public class MigrationClientParam extends MigrationClientCore {
    private static final Logger logger = LogManager.getLogger(MigrationClientParam.class);

    private static Options options = new Options();
    private static HelpFormatter formatter = new HelpFormatter();

    public MigrationClientParam() {
    }

    public Workspace getWorkspace() throws IOException, JSONException {
        String url = this.getBaseUrl() + "/services/data/get/fbs.workspace-GetWorkspace@.json/";
        String jsonParams = "{\n  \"projectId\": \"" + this.getProjectId() + "\"\n}";
        Workspace ws = new Workspace();
        if (this.call("POST", url, jsonParams)) {
            String json = EntityUtils.toString(this.getResponse().getEntity());
            JSONArray jsonArray = new JSONArray(json);
            String workspaceId = jsonArray.getJSONObject(0).getString("id");
            String workspaceName = jsonArray.getJSONObject(0).getString("name");
            ws.setId(workspaceId);
            ws.setName(workspaceName);
        } else {
            String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
            logger.debug("EntityUtils To String", EntityUtilsToString);
            this.getResponse().close();
        }

        return ws;
    }

    public void copyProject(String name, String description) throws IOException, JSONException {
        String url = this.getBaseUrl() + "/services/data/run/fbs.project-CopyProject@.html";
        Workspace ws = this.getWorkspace();
        String jsonParams = "{\n  \"sourceId\": \"" + this.getProjectId() + "\",\n  \"name\": \"" + name + "\",\n  \"description\": \"" + description + "\",\n  \"workspaceId\": \"" + ws.getId() + "\"\n}";
        logger.debug("Copying... {}");
        logger.debug("Server: {}", this.getBaseUrl());
        logger.debug("New Project Name: {}", name);
        if (this.call("POST", url, jsonParams)) {
            logger.debug("Copied in the workspace successfully. workspace name is : {}",  ws.getName());
            EntityUtils.consume(this.getResponse().getEntity());
        } else {
            String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
            logger.debug("EntityUtils To String", EntityUtilsToString);
            this.getResponse().close();
        }

    }

    public void importData(String type, File file) throws IOException {
        if (file.exists()) {
            this.setImportValue(type);
            logger.debug("Importing...");
            logger.debug("Server: {}", this.getBaseUrl());
            logger.debug("Type: {}", this.getType());
            if (this.call("POST", this.getUrl(), file)) {
                this.importing();
            } else {
                String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
                logger.debug("EntityUtils To String", EntityUtilsToString);
                this.getResponse().close();
            }
        } else {
            logger.debug("File does not exist. Please check your file.");
        }

    }

    public void exportData(String type, String file) throws IOException {
        File f = new File(file);
        String fileName = f.getName();
        List<BasicNameValuePair> params = this.setExportValue(type, fileName);
        logger.debug("Exporting...");
        logger.debug("Server: ", this.getBaseUrl());
        logger.debug("Type: ", this.getType());

        List<BasicNameValuePair> params2 = new ArrayList();
        params2.add(new BasicNameValuePair("projectId", this.getProjectId()));


//        if (this.call("POST", this.getUrl(), params)) {
        String url = "http://bizcloud.bizflow.com/bizflowappdev/services/custom/cloud/file/download/patch/bpm/12/12.6.0.0001.00-Windows.zip";
        if (this.call("POST", url, (List<BasicNameValuePair>)null)) {
            try {
                this.download(file);
                logger.debug("Success.");
            } finally {
                this.getResponse().close();
            }
        } else {
            String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
            logger.debug("EntityUtils To String", EntityUtilsToString);
            this.getResponse().close();
        }

    }

    public void helper(int helper) {
        formatter.setWidth(150);
        if (helper == 1) {
            logger.debug("=== Bizflow AppDev Migration Management Helper [Parameter Mode] ===");
            logger.debug("EXPORT -export -url <arg> -i <arg> -p <arg> [-pid <arg>] -t <arg> [-isfi <arg>] -f <arg>");
            logger.debug(" To export data model, project, project publication, global lookup data or file.");
            logger.debug(" If district is a project, set pid. If export type is project publication, add '-isfi <arg>'");
            logger.debug("IMPORT -import -url <arg> -i <arg> -p <arg> [-pid <arg>] -t <arg> -f <arg>");
            logger.debug(" To import the exported data model, project, project publication, lookup data or file.");
            logger.debug(" If district is a project, set pid.");
            logger.debug("COPY -copy -url <arg> -i <arg> -p <arg> -pid <arg> -n <arg> -d <arg>");
            logger.debug(" Copy the existing project by entering new name(n) and description(d).");
            logger.debug("DESCRIPTION");
            formatter.printHelp("options", options);
        } else {
            formatter.printHelp("options", options);
            logger.debug("Use 'appdev-mm.bat help' (Windows) or 'sh appdev-mm.sh help' (Linux) for an overview of the commands.");
        }
    }

    public void setOptions() {
        options.addOption(new Option("export", false, "For exporting"));
        options.addOption(new Option("import", false, "For importing"));
        options.addOption(new Option("copy", false, "For copying"));
        Option host = new Option("url", true, "Enter the site URL you want to access. ex) -url http://localhost/bizflowappdev");
        host.setRequired(true);
        options.addOption(host);
        Option id = new Option("i", "userId", true, "User id");
        id.setRequired(true);
        options.addOption(id);
        Option pwd = new Option("p", "pwd", true, "User password");
        pwd.setRequired(true);
        options.addOption(pwd);
        options.addOption(new Option("pid", "projectId", true, "Project id"));
        options.addOption(new Option("isfi", true, "In the case of exporting project publication, set the unique value 'isfi'."));
        options.addOption(new Option("t", "type", true, "Select data type - Model, Project, Publication, Lookup, or File. ex) -t Model"));
        options.addOption(new Option("f", "file", true, "File path with file name what you will export or import. ex) -f D:/file.adx"));
        options.addOption(new Option("n", "name", true, "Set a new project name. If it is not a single word, put it in quotation marks. ex) -n \"Test project\""));
        options.addOption(new Option("d", "description", true, "Set a new project description. If it is not a single word, put it in quotation marks. ex) -d \"Test project description.\""));
    }

    public void checkPid() {
        if (this.getProjectId() == null) {
            logger.debug("Missing required options: pid");
            this.helper(2);
        }

    }

    public void client(String[] args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        this.setOptions();
        if (args.length == 1 && args[0].toLowerCase().equals("help")) {
            this.helper(1);
        }

        try {
            cmd = parser.parse(options, args);
            if (!cmd.hasOption("export") && !cmd.hasOption("import") && !cmd.hasOption("copy")) {
                logger.debug("Missing required options: export, import or copy");
                this.helper(2);
            }

            if (cmd.hasOption("export") || cmd.hasOption("import")) {
                if (!cmd.hasOption("type")) {
                    logger.debug("Missing required options: t");
                    this.helper(2);
                }

                if (!cmd.hasOption("file")) {
                    logger.debug("Missing required options: f");
                    this.helper(2);
                }
            }
        } catch (ParseException var5) {
            logger.debug(var5.getMessage());
            this.helper(2);
        }

        this.setBaseUrl(cmd.getOptionValue("url"));
        this.setUserId(cmd.getOptionValue("userId"));
        this.setPassword(cmd.getOptionValue("pwd"));
        this.setProjectId(cmd.getOptionValue("projectId"));
        this.setIsfi(cmd.getOptionValue("isfi"));
        if (cmd.hasOption("type")) {
            this.setType(cmd.getOptionValue("type").toLowerCase());
        }

        if (this.login()) {
            if (cmd.hasOption("export")) {
                if (this.getType().equals("publication") && this.getIsfi() == null) {
                    logger.debug("Missing required options: isfi");
                    this.helper(2);
                } else if (this.getType().equals("model") || this.getType().equals("project") || this.getType().equals("publication")) {
                    this.checkPid();
                }

                this.exportData(this.getType(), cmd.getOptionValue("file"));
            } else if (cmd.hasOption("import")) {
                if (this.getType().equals("model")) {
                    this.checkPid();
                }

                this.importData(this.getType(), new File(cmd.getOptionValue("file")));
            } else if (cmd.hasOption("copy")) {
                if (!cmd.hasOption("name") || !cmd.hasOption("description")) {
                    logger.debug("Missing required options: n or d");
                    this.helper(2);
                }

                this.checkPid();
                this.copyProject(cmd.getOptionValue("name"), cmd.getOptionValue("description"));
            }
        } else {
            logger.debug("Login Again.");
        }

    }
}
