package com.bizflow.io.custom.services.bizcloud.migration.model;

import org.json.JSONArray;

public class Workspace {
    public String id;
    public String name;
    public int projectCount;
    public JSONArray children;

    public Workspace() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setProjectCount(int projectCount) {
        this.projectCount = projectCount;
    }

    public int getProjectCount() {
        return this.projectCount;
    }

    public void setChildren(JSONArray children) {
        this.children = children;
    }

    public JSONArray getChildren() {
        return this.children;
    }
}
