package com.bizflow.io.custom.services.bizcloud.migration;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.bizflow.io.custom.services.bizcloud.license.LicenseManager;
import org.apache.http.HttpEntity;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AppDevMigCore {
    public AppDevMigCore() {

    }
    private static final Logger logger = LogManager.getLogger(AppDevMigCore.class);

    private String baseUrl;
    private String url;
    private String userId;
    private String password;
    private String id;
    private static String projectId;
    private String transactionId;
    private String isfi;
    private String type;
    private static Cookie cookie = null;
    private static long postTime;
    private static CloseableHttpClient httpClient = null;
    private static CloseableHttpResponse response = null;
    private static HttpClientContext context = null;
    private static String PATH = "/services/acm/login.json";
    private static String PROJECTPUBLICATIONURL = "/services/file/download/fbs.export-ProjectPublication@.json/";

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static String getProjectId() {
        return AppDevMigCore.projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getIsfi() {
        return this.isfi;
    }

    public void setIsfi(String isfi) {
        this.isfi = isfi;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static String getCookie() {
        return AppDevMigCore.cookie.getValue();
    }

    public static void setCookie(Cookie cookie) {
        AppDevMigCore.cookie = cookie;
    }

    public static long getPostTime() {
        return postTime;
    }

    public static void setPostTime(long postTime) {
        AppDevMigCore.postTime = postTime;
    }

    public static CloseableHttpResponse getResponse() {
        return response;
    }

    public static HttpClientContext getContext() {
        return context;
    }

    public static boolean isSuccess() {
        return response != null && response.getStatusLine().getStatusCode() == 200;
    }

    private static HttpEntity getContentEntity(Object content) throws UnsupportedEncodingException {
        HttpEntity entity = null;
        if (null != content) {
            if (content instanceof List) {
                entity = new UrlEncodedFormEntity((List)content, "UTF-8");
            } else if (content instanceof String) {
                entity = new StringEntity((String)content);
            } else if (content instanceof File) {
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                builder.addBinaryBody("file", (File)content, ContentType.APPLICATION_OCTET_STREAM, ((File)content).getName());
                if (getProjectId() != null) {
                    builder.addTextBody("projectId", getProjectId(), ContentType.DEFAULT_BINARY);
                }

                entity = builder.build();
            }
        }

        return (HttpEntity)entity;
    }

    private static HttpEntity getContentEntity(Object content, String projectId) throws UnsupportedEncodingException {
        HttpEntity entity = null;
        if (null != content) {
            if (content instanceof List) {
                entity = new UrlEncodedFormEntity((List)content, "UTF-8");
            } else if (content instanceof String) {
                entity = new StringEntity((String)content);
            } else if (content instanceof File) {
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                builder.addBinaryBody("file", (File)content, ContentType.APPLICATION_OCTET_STREAM, ((File)content).getName());
                if (projectId != null) {
                    builder.addTextBody("projectId", projectId, ContentType.DEFAULT_BINARY);
                }

                entity = builder.build();
            }
        }

        return (HttpEntity)entity;
    }

    private static boolean post(String url, HttpEntity entity) throws IOException {
        int timeout = 1200;
        RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(0).build();
        httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Cookie", "JSESSIONID=" + AppDevMigCore.getCookie());
        httpPost.setEntity(entity);
        Date date = new Date();
        setPostTime(date.getTime());
        response = httpClient.execute(httpPost);
        return isSuccess();
    }

    private static boolean get(String url, BasicCookieStore cookieStore) throws IOException {
        httpClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
        HttpGet httpGet = new HttpGet(url);
        if (cookieStore == null) {
            AppDevMigCore.response = httpClient.execute(httpGet, context);
        } else {
            AppDevMigCore.response = httpClient.execute(httpGet);
        }

        return isSuccess();
    }

    public static boolean call(String method, String url, Object content) throws IOException {
        boolean success = false;
        if (method.equals("GET")) {
            if (content != null) {
                if (content instanceof BasicCookieStore) {
                    success = get(url, (BasicCookieStore)content);
                }
            } else {
                context = HttpClientContext.create();
                success = get(url, (BasicCookieStore)null);
            }
        } else {
            success = post(url, getContentEntity(content));
        }

        return success;
    }

    public static boolean call(String method, String url, Object content, String projectId) throws IOException {
        boolean success = false;
        if (method.equals("GET")) {
            if (content != null) {
                if (content instanceof BasicCookieStore) {
                    success = get(url, (BasicCookieStore)content);
                }
            } else {
                context = HttpClientContext.create();
                success = get(url, (BasicCookieStore)null);
            }
        } else {
            success = post(url, getContentEntity(content, projectId));
        }

        return success;
    }

    public boolean login() throws URISyntaxException, IOException {
        URI uri = (new URIBuilder()).setScheme("http").setHost(this.getBaseUrl().replace("http://", "")).setPath(PATH).setParameter("app", "fbs").setParameter("i", this.getUserId()).setParameter("p", this.getPassword()).build();
        if (this.call("GET", uri.toString(), (Object)null)) {
            try {
                CookieStore cookieStore = this.getContext().getCookieStore();
                List<Cookie> cookies = cookieStore.getCookies();
                if (cookies != null) {
                    Iterator var4 = cookies.iterator();
                    if (var4.hasNext()) {
                        Cookie cookie = (Cookie)var4.next();
                        boolean var6;
                        if (cookie.getName().equals("JSESSIONID")) {
                            setCookie(cookie);
                            var6 = true;
                            return var6;
                        }
                        logger.debug("JSESSIONID does not exist.");
                        var6 = false;
                        return var6;
                    }
                }
            } finally {
                this.getResponse().close();
            }
        } else {
            logger.debug("EntityUtils {}", EntityUtils.toString(this.getResponse().getEntity()));
            this.getResponse().close();
        }

        return false;
    }


    public static boolean login(String url, String userId, String userPw) throws URISyntaxException, IOException {
        URI uri = null;
        if(url.startsWith("https://")) {
            uri = (new URIBuilder()).setScheme("https").setHost(url.replace("https://", "")).setPath(PATH).setParameter("app", "fbs").setParameter("i", userId).setParameter("p", userPw).build();
        } else {
            uri = (new URIBuilder()).setScheme("http").setHost(url.replace("http://", "")).setPath(PATH).setParameter("app", "fbs").setParameter("i", userId).setParameter("p", userPw).build();
        }
        if (call("GET", uri.toString(), (Object)null)) {
            try {
                CookieStore cookieStore = getContext().getCookieStore();
                List<Cookie> cookies = cookieStore.getCookies();
                if (cookies != null) {
                    Iterator var4 = cookies.iterator();
                    if (var4.hasNext()) {
                        Cookie cookie = (Cookie)var4.next();
                        boolean var6;
                        if (cookie.getName().equals("JSESSIONID")) {
                            setCookie(cookie);
                            var6 = true;
                            return var6;
                        }

                        logger.debug("JSESSIONID does not exist.");
                        var6 = false;
                        return var6;
                    }
                }
            } finally {
                getResponse().close();
            }
        } else {
            getResponse().close();
        }

        return false;
    }


    public static boolean login_https(String url, String userId, String userPw) throws URISyntaxException, IOException {
        URI uri = (new URIBuilder()).setScheme("http").setHost(url.replace("http://", "")).setPath(PATH).setParameter("app", "fbs").setParameter("i", userId).setParameter("p", userPw).build();
        if (call("GET", uri.toString(), (Object)null)) {
            try {
                CookieStore cookieStore = getContext().getCookieStore();
                List<Cookie> cookies = cookieStore.getCookies();
                if (cookies != null) {
                    Iterator var4 = cookies.iterator();
                    if (var4.hasNext()) {
                        Cookie cookie = (Cookie)var4.next();
                        boolean var6;
                        if (cookie.getName().equals("JSESSIONID")) {
                            setCookie(cookie);
                            var6 = true;
                            return var6;
                        }

                        logger.debug("JSESSIONID does not exist.");
                        var6 = false;
                        return var6;
                    }
                }
            } finally {
                getResponse().close();
            }
        } else {
            getResponse().close();
        }

        return false;
    }

    public static void download(String file) throws IOException {
        int progress = 0;
        long actual = getResponse().getEntity().getContentLength();
        long bytesDownloaded = 0L;

        try(  BufferedInputStream bis = new BufferedInputStream(getResponse().getEntity().getContent());
              BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(file)));) {

            int inByte;
            while((inByte = bis.read()) != -1) {
                bos.write(inByte);
                ++bytesDownloaded;
                int rate = (int)(100L * bytesDownloaded / actual);
                if (progress != rate) {
                    //logger.debug(rate + "% (" + bytesDownloaded + "/" + actual + ")\r");
                    logger.debug("rate : %s", rate);
                    logger.debug("% %s(", bytesDownloaded);
                    logger.debug("/ %s", actual);
                    progress = rate;
                }
            }
        } catch (IOException var15) {
            logger.error("ERROR : %s",var15.getMessage());
        }
    }

    public static String encodeURIComponent(String s) {
        String result;
        try {
            result = URLEncoder.encode(s, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!").replaceAll("\\%27", "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException var3) {
            result = s;
        }

        return result;
    }

    public List<BasicNameValuePair> setExportValue(String type, String fileName) {
        String url = this.getBaseUrl();
        byte var5 = -1;
        switch(type.hashCode()) {
            case -1097094790:
                if (type.equals("lookup")) {
                    var5 = 3;
                }
                break;
            case -1078222292:
                if (type.equals("publication")) {
                    var5 = 2;
                }
                break;
            case -309310695:
                if (type.equals("project")) {
                    var5 = 1;
                }
                break;
            case 3143036:
                if (type.equals("file")) {
                    var5 = 4;
                }
                break;
            case 104069929:
                if (type.equals("model")) {
                    var5 = 0;
                }
        }

        switch(var5) {
            case 0:
                url = url + "/services/file/download/fbs.export-ProjectDataModel@.json/";
                break;
            case 1:
                url = url + "/services/file/download/fbs.export-Project@.json/";
                break;
            case 2:
                url = url + PROJECTPUBLICATIONURL;
                break;
            case 3:
                url = url + "/services/file/download/fbs.export-LookupData@.json/";
                break;
            case 4:
                url = url + "/services/file/download/fbs.export-File@.json/";
        }

        url = url + encodeURIComponent(fileName);
        this.setUrl(url);
        List<BasicNameValuePair> params = new ArrayList();
        if (!type.equals("model") && !type.equals("project")) {
            if (type.equals("publication")) {
                params.add(new BasicNameValuePair("projectId", this.getProjectId()));
                if (this.getTransactionId() != null) {
                    params.add(new BasicNameValuePair("id", this.getId()));
                    params.add(new BasicNameValuePair("transactionId", this.getTransactionId()));
                    logger.debug("*used transaction id");
                } else {
                    params.add(new BasicNameValuePair("isfi", this.getIsfi()));
                    logger.debug("*used isfi");
                }
            } else {
                params.add(new BasicNameValuePair("district", "Application"));
            }
        } else {
            params.add(new BasicNameValuePair("projectId", this.getProjectId()));
        }

        return params;
    }


    public static List<BasicNameValuePair> setExportValue(String url, String type, String fileName, String projectId, int transactionId) {
        byte var5 = -1;
        switch(type.hashCode()) {
            case -1097094790:
                if (type.equals("lookup")) {
                    var5 = 3;
                }
                break;
            case -1078222292:
                if (type.equals("publication")) {
                    var5 = 2;
                }
                break;
            case 1295582298:
                if (type.equals("prjFromPublication")) {
                    var5 = 5;
                }
                break;
            case -309310695:
                if (type.equals("project")) {
                    var5 = 1;
                }
                break;
            case 3143036:
                if (type.equals("file")) {
                    var5 = 4;
                }
                break;
            case 104069929:
                if (type.equals("model")) {
                    var5 = 0;
                }
        }

        switch(var5) {
            case 0:
                url = url + "/services/file/download/fbs.export-ProjectDataModel@.json/";
                break;
            case 1:
                url = url + "/services/file/download/fbs.export-Project@.json/";
                break;
            case 2:
                url = url + PROJECTPUBLICATIONURL;
                break;
            case 3:
                url = url + "/services/file/download/fbs.export-LookupData@.json/";
                break;
            case 4:
                url = url + "/services/file/download/fbs.export-File@.json/";
                break;
            case 5:
                url = url + PROJECTPUBLICATIONURL;
                break;
        }

        List<BasicNameValuePair> params = new ArrayList();

        url = url + encodeURIComponent(fileName);
        params.add(new BasicNameValuePair("url", url));
        if (!type.equals("model") && !type.equals("project")) {
            if (type.equals("publication")||type.equals("prjFromPublication")) {
                params.add(new BasicNameValuePair("projectId", projectId));
                if (transactionId != -1) {
                    params.add(new BasicNameValuePair("transactionId", transactionId + ""));
                    logger.debug("*used transaction id");
                } else {
                    logger.debug("*used isfi");
                }
            } else {
                params.add(new BasicNameValuePair("district", "Application"));
            }
        } else {
            params.add(new BasicNameValuePair("projectId", projectId));
        }

        return params;
    }

    public static BufferedReader readProgress(BufferedReader br) throws IOException {
        String sb = "";
        StringBuilder percent = new StringBuilder();
        boolean isProgressbarStart = false;
        boolean isProcessing = false;

        while(true) {
            int inByte;
            while((inByte = br.read()) != -1) {
                char ch = (char)inByte;
                if (isProgressbarStart) {
                    sb = sb + ch;
                    if (sb.contains("\"importSuccess\"")) {
                        return br;
                    }
                }

                if ('0' <= ch && ch <= '9') {
                    isProgressbarStart = true;
                    percent.append(ch);
                } else if (0 < percent.length() && percent.length() < 3) {
                    isProcessing = true;
                    logger.debug("percent %s", percent);
                    percent.setLength(0);
                } else if (percent.length() == 3) {
                    if (isProcessing) {
                        logger.debug("percent %s", percent);
                        isProcessing = false;
                    }

                    percent.setLength(0);
                }
            }

            return br;
        }
    }

    public static void importing() throws IOException {
        BufferedInputStream bis = null;
        BufferedReader br = null;

        try {
            bis = new BufferedInputStream(getResponse().getEntity().getContent());
            br = new BufferedReader(new InputStreamReader(bis));

            String line;
            while((line = br.readLine()) != null) {
                if (line.contains("class=\"progress-bar\"")) {
                    br = readProgress(br);
                    break;
                }
            }

            EntityUtils.consume(getResponse().getEntity());
        } catch (IOException var8) {
            var8.printStackTrace();
        } finally {
            if (bis != null) {
                bis.close();
            }

            if (br != null) {
                br.close();
            }

        }

        Date eDate = new Date();
        long howLong = eDate.getTime() - getPostTime();
        logger.debug("Importing has been done. It took %s", howLong);
    }

    public void setImportValue(String type) {
        String url = this.getBaseUrl();
        byte var4 = -1;
        switch(type.hashCode()) {
            case -1097094790:
                if (type.equals("lookup")) {
                    var4 = 3;
                }
                break;
            case -1078222292:
                if (type.equals("publication")) {
                    var4 = 2;
                }
                break;
            case -309310695:
                if (type.equals("project")) {
                    var4 = 1;
                }
                break;
            case 3143036:
                if (type.equals("file")) {
                    var4 = 4;
                }
                break;
            case 104069929:
                if (type.equals("model")) {
                    var4 = 0;
                }
        }

        switch(var4) {
            case 0:
                url = url + "/services/file/run/fbs.import-ProjectDataModel@.html";
                break;
            case 1:
                url = url + "/services/file/run/fbs.import-Project@.html";
                break;
            case 2:
                url = url + "/services/file/run/fbs.import-ProjectPublication@.html";
                break;
            case 3:
                url = url + "/services/file/run/fbs.import-LookupData@.html";
                break;
            case 4:
                url = url + "/services/file/run/fbs.import-File@.html";
        }

        this.setUrl(url);
    }

    public static List<BasicNameValuePair> setImportValue(String url, String type, String importProjectId) {
        byte var4 = -1;
        String appUrl = null;
        switch(type.hashCode()) {
            case -1097094790:
                if (type.equals("lookup")) {
                    var4 = 3;
                }
                break;
            case -1078222292:
                if (type.equals("publication")) {
                    var4 = 2;
                }
                break;
            case 1295582298:
                if (type.equals("prjFromPublication")) {
                    var4 = 5;
                }
                break;
            case -309310695:
                if (type.equals("project")) {
                    var4 = 1;
                }
                break;
            case 3143036:
                if (type.equals("file")) {
                    var4 = 4;
                }
                break;
            case 104069929:
                if (type.equals("model")) {
                    var4 = 0;
                }
        }

        switch(var4) {
            case 0:
                appUrl = url + "/services/file/run/fbs.import-ProjectDataModel@.html";
                break;
            case 1:
                appUrl = url + "/services/file/run/fbs.import-Project@.html";
                break;
            case 2:
                appUrl = url + "/services/file/run/fbs.import-ProjectPublication@.html";
                break;
            case 3:
                appUrl = url + "/services/file/run/fbs.import-LookupData@.html";
                break;
            case 4:
                appUrl = url + "/services/file/run/fbs.import-File@.html";
                break;
            case 5:
                appUrl = url + "/services/file/run/fbs.import-ProjectFromPublication@.html";
                break;
        }

        List<BasicNameValuePair> params = new ArrayList();
        params.add(new BasicNameValuePair("url", appUrl));
        if (type.equals("model")) {
            params.add(new BasicNameValuePair("projectId", importProjectId));
        }

        return params;
    }
}
