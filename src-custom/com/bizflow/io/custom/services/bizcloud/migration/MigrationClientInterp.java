package com.bizflow.io.custom.services.bizcloud.migration;

import com.bizflow.io.custom.services.bizcloud.migration.model.Workspace;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.TimeZone;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MigrationClientInterp extends MigrationClientCore {
    String[] typeList = new String[]{"model", "project", "publication", "lookup", "file"};

    public MigrationClientInterp() {
    }
    private static final Logger logger = LogManager.getLogger(MigrationClientInterp.class);

    public boolean logIn() throws URISyntaxException, IOException {
        Console console = System.console();
        String userId = console.readLine("\nUser Name: ");
        String password = new String(console.readPassword("Password: ", new Object[0]));
        this.setUserId(userId);
        this.setPassword(password);
        return this.login();
    }

    private void logOut() throws IOException {
        BasicCookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie("JSESSIONID", this.getCookie());
        cookieStore.addCookie(cookie);
        String url = this.getBaseUrl() + "/services/acm/logout.json";
        if (this.call("GET", url, cookieStore)) {
            this.getResponse().close();
            logger.debug("Logged out successfully.");
        } else {
            this.getResponse().close();
            logger.debug("Logout error. Please contact the administrator.");
        }
    }

    public void homeOrLogout() throws IOException, JSONException, ParseException {
        Scanner sc = new Scanner(System.in);
        String select = sc.next();
        if (select.toLowerCase().equals("n")) {
            this.rootOrWorkspace();
        } else {
            this.logOut();
        }
        sc.close();
    }

    public void copyProject(String workspaceId, String workspaceName) throws IOException, JSONException, ParseException {
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        String description = sc.nextLine();
        String url = this.getBaseUrl() + "/services/data/run/fbs.project-CopyProject@.html";
        String jsonParams = "{\n  \"sourceId\": \"" + this.getProjectId() + "\",\n  \"name\": \"" + name + "\",\n  \"description\": \"" + description + "\",\n  \"workspaceId\": \"" + workspaceId + "\"\n}";
        if (this.call("POST", url, jsonParams)) {
            logger.debug("Copied in the workspace %s", workspaceName);
            logger.debug("successfully.");
            this.homeOrLogout();
        } else {
            String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
            logger.debug("EntityUtils To String", EntityUtilsToString);
            this.getResponse().close();
        }
        sc.close();
    }

    public void importData(String type) throws IOException, JSONException, ParseException {
        Scanner sc = new Scanner(System.in);
        String f = sc.next();
        File file = new File(f);
        if (file.exists()) {
            this.setImportValue(type);
            if (this.call("POST", this.getUrl(), file)) {
                this.importing();
                this.homeOrLogout();
            } else {
                String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
                logger.debug("EntityUtils To String", EntityUtilsToString);
                this.getResponse().close();
            }
        } else {
            logger.debug("File does not exist. Please check your file.");
            this.rootOrWorkspace();
        }
        sc.close();
    }

    public void selectTransactionId() throws IOException, JSONException, ParseException {
        Scanner sc = new Scanner(System.in);
        String jsonParams = "{\n  \"projectId\": \"" + this.getProjectId() + "\",\n  \"ORDER_BY\": \"transactionId DESC\"\n}";
        String url = this.getBaseUrl() + "/services/data/get/fbs.publication-GetPublicationProjectInfoSimple.json";
        if (this.call("POST", url, jsonParams)) {
            String json = EntityUtils.toString(this.getResponse().getEntity());
            JSONArray jsonarray = new JSONArray(json);
            if (jsonarray.length() == 0) {
                logger.debug("Publication does not exist.");
                this.homeOrLogout();
            } else {
                logger.debug("\n======================================Publication Info=======================================");
                logger.debug(String.format("    %-10s %-20s %-28s %-10s %-10s", "Publisher", "Publish Date", "Tag", "Version", "Transaction Id"));

                int i;
                String userName;
                for(i = 0; i < jsonarray.length(); ++i) {
                    JSONObject publicationInfo = jsonarray.getJSONObject(i);
                    userName = publicationInfo.getString("userName");
                    Date date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")).parse(publicationInfo.getString("actionDate"));
                    String timeZone = Calendar.getInstance().getTimeZone().getID();
                    Date local = new Date(date.getTime() + (long)TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
                    String actionDate = (new SimpleDateFormat("MM/dd/yyyy HH:mm")).format(local);
                    String tag = "";
                    if (publicationInfo.has("tag")) {
                        tag = publicationInfo.getString("tag");
                    }

                    int version = publicationInfo.getInt("version");
                    int transactionId = publicationInfo.getInt("transactionId");
                    logger.debug(String.format("%-3d %-10s %-20s %-30s %-10s %-10s %s",  i + 1, userName, actionDate, tag, version, transactionId));
                }
                logger.debug("=============================================================================================");
                i = sc.nextInt();
                int tid = jsonarray.getJSONObject(i - 1).getInt("transactionId");
                userName = jsonarray.getJSONObject(i - 1).getString("id");
                this.setTransactionId(Integer.toString(tid));
                this.setId(userName);
            }
        } else {
           logger.debug(this.getResponse().getStatusLine());
            this.getResponse().close();
        }

    }

    public void exportData(String type) throws IOException, JSONException, ParseException {
        if (type.equals("publication")) {
            this.selectTransactionId();
        }

        Scanner sc = new Scanner(System.in);
        String file = sc.next();
        File f = new File(file);
        String fileName = f.getName();
        List<BasicNameValuePair> params = this.setExportValue(type, fileName);
        if (this.call("POST", this.getUrl(), params)) {
            this.download(file);
            this.homeOrLogout();
        } else {
            String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
            logger.debug("EntityUtils To String", EntityUtilsToString);
            this.getResponse().close();
        }

    }

    public void showProject(Workspace ws) throws IOException, JSONException, ParseException {
        Scanner sc = new Scanner(System.in);
        JSONArray children = ws.getChildren();

        int projectNum;
        JSONObject exportObject;
        for(projectNum = 0; projectNum < ws.getProjectCount(); ++projectNum) {
            exportObject = children.getJSONObject(projectNum);
        }

        projectNum = sc.nextInt();
        if (projectNum <= ws.getProjectCount()) {
            exportObject = children.getJSONObject(projectNum - 1);
            this.setProjectId(exportObject.getString("id"));
            int type = sc.nextInt();
            if (type == 1) {
                this.copyProject(ws.getId(), ws.name);
            } else if (1 < type && type <= 4) {
                this.setType(this.typeList[type - 2]);
                this.exportData(this.typeList[type - 2]);
            } else if (type == 5) {
                this.setType(this.typeList[0]);
                this.importData(this.typeList[0]);
            } else {
                this.showProject(ws);
            }
        } else {
            this.showWorkspace();
        }

        sc.close();
    }

    public void showRoot() throws IOException, JSONException, ParseException {
        this.setProjectId((String)null);
        Scanner sc = new Scanner(System.in);
        int type = sc.nextInt();
        if (type <= 2) {
            this.setType(this.typeList[type + 2]);
            this.exportData(this.typeList[type + 2]);
        } else if (type < 7) {
            this.setType(this.typeList[type - 2]);
            this.importData(this.typeList[type - 2]);
        } else {
            this.rootOrWorkspace();
        }

        sc.close();
    }

    public void showWorkspace() throws IOException, JSONException, ParseException {
        Scanner sc = new Scanner(System.in);
        String url = this.getBaseUrl() + "/services/data/get/fbs.workspace-GetWorkspaceProjectTree@.json";
        if (this.call("POST", url, (Object)null)) {
            String json = EntityUtils.toString(this.getResponse().getEntity());
            JSONArray jsonarray = new JSONArray(json);
            List<Workspace> ws = new ArrayList();
            int length = jsonarray.length();

            int i;
            for(i = 0; i < length; ++i) {
                Workspace workspace = new Workspace();
                JSONObject workspaceObject = jsonarray.getJSONObject(i);
                workspace.setName(workspaceObject.getString("name"));
                workspace.setProjectCount(workspaceObject.getInt("projectCount"));
                workspace.setId(workspaceObject.getString("id"));
                if (workspace.getProjectCount() > 0) {
                    workspace.setChildren(workspaceObject.getJSONArray("children"));
                } else {
                    workspace.setChildren((JSONArray)null);
                }

                ws.add(workspace);
                this.getResponse().close();
            }

            i = sc.nextInt();
            if (i <= length) {
                this.showProject((Workspace)ws.get(i - 1));
            } else {
                this.rootOrWorkspace();
            }
        } else {
            String EntityUtilsToString = EntityUtils.toString(this.getResponse().getEntity());
            logger.debug("EntityUtils To String", EntityUtilsToString);
            this.getResponse().close();
        }

        sc.close();
    }

    public void rootOrWorkspace() throws IOException, JSONException, ParseException {
        Scanner sc = new Scanner(System.in);
        int select = sc.nextInt();
        if (select == 1) {
            this.showRoot();
        } else {
            this.showWorkspace();
        }

        sc.close();
    }

    public void setServer() {
        Scanner sc = new Scanner(System.in);
        String url = sc.next();
        this.setBaseUrl(url);
    }

    public void client() throws URISyntaxException, JSONException, IOException, ParseException {
        this.setServer();
        if (this.logIn()) {
            this.rootOrWorkspace();
        } else {
            logger.debug("Login again..");
        }

    }
}
