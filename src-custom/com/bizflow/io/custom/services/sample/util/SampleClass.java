package com.bizflow.io.custom.services.sample.util;

import com.bizflow.io.core.json.JSONObject;
import com.bizflow.io.core.json.util.JSONUtil;
import com.bizflow.io.core.net.util.RequestIdentifierKeeper;
import com.bizflow.io.services.message.performlog.PerformanceLoggerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class SampleClass {
    private SampleClass() {
        throw new IllegalStateException("SampleClass class");
    }

    private static final Logger logger = LogManager.getLogger(SampleClass.class);

    public static Object sampleMethod(String sessionName, HashMap parameter){
        logger.debug("SampleClass.sampleMethod is started!");
        long timeS = System.currentTimeMillis();
        JSONObject jsonParam = null;
        JSONObject result = null;
        try{
            HttpServletRequest request = RequestIdentifierKeeper.getHttpServletRequest();
            jsonParam = new JSONObject(JSONUtil.mapToJsonString(parameter));
            String subject = null;
            String contents = null;
            subject = jsonParam.getString("subject");
            contents = jsonParam.getString("contents");

            result = new JSONObject();
            result.put("result","true");
            result.put("message","you called this class and method successfully!");

            logger.debug("All jobs are done!");
        } catch (Exception e) {
            logger.error("{}:  jsonString={}", RequestIdentifierKeeper.getRequestIdLogString(), jsonParam.toString(), e);
            e.printStackTrace();
            result = new JSONObject();
            result.put("result","false");
            result.put("faultString",e.getMessage());
        } finally {
            PerformanceLoggerUtil.logger.log("parameterString={\"subject\":\"{}\"}, RequestResponseTime={}", "sampleMethod", (System.currentTimeMillis() - timeS));
        }
        return result;
    }
}
