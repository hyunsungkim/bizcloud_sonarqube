# BizFlow Application Container Server

BizFlow Application Container Service is designed to run applications and provide APIs and related back-end services to the applications

An application must be deployed under moons folder as follows.

    /-bizflowio
        |-apps
            |- app1
                |- APP-INF
                |      |- db
                |           |- app1Configuration.xml
                |           |- app1SqlMapper.xml
                |- application.inf
